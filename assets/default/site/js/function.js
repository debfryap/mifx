


$(document).ready(function(){			
		
	
	//flexslider
	$('.flexslider').flexslider({
        animation: "fade"        
    });	
	
	//Set default open/close settings
	$('.acc_container').hide(); //Hide/close all containers
	//$('.acc_trigger:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container
    $('.acc_trigger.auto:first').addClass('active').next().show(); //Add "active" class to first trigger, then show/open the immediate next container

	//On Click
	$('.acc_trigger').click(function(){
		if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
			$('.acc_trigger').removeClass('active').next().slideUp(); //Remove all .acc_trigger classes and slide up the immediate next container
			$(this).toggleClass('active').next().slideDown(); //Add .acc_trigger class to clicked trigger and slide down the immediate next container
		}
		return false; //Prevent the browser jump to the link anchor
	});	
	
	//language	 								 
	$('.lang2').hide();
	$('.lang1').click(function(){		
		$(".lang2").fadeToggle(100);		
	}); 		
	$(document).mouseup(function() {
		if($(".lang2").parent(".lang1").length==0) {			
			$(".lang2").hide();
		}
	});	
	
	//search
	$(".form_search").hide();	
	$(".btn_search").click(function(){				
		$(".form_search").slideToggle(130);		
	});	
	
	//socMed mobile
	$(".sosMedia_mobile").hide();
	$(".share_show").click(function(){
		$(".sosMedia_mobile").toggle();
	});
	$(".sosMedia_mobile").mouseup(function(){
		return false
	});
	$(document).mouseup(function(){
		if($(".sosMedia_mobile").parent(".share_show").length==0){
			$(".sosMedia_mobile").fadeOut();
		}
	});
	
	
	//schild toggle
	
	/*$("#desktop li.parent").hover(function(){
		$(this).toggleClass("selected");		
	});*/
		
	$("li.have_child").click(function(){
		$(this).toggleClass("open");
		$(this).children(".child02").toggleClass("opened");		

    return false;
	});
		
  // add script 05 Mei 2014
  $(".child02 a").each(function(){
      $(this).click(function(){
          console.log('return true');
          window.location = $(this).attr('href');
          return true;
      })  
  })
 // end here
	
	$(".inline").colorbox({inline:true, width:"700px"});
	$(".flash").colorbox({inline:true, width:"720px"});
  
	/** Tutorial, Invesment Clinic **/
    if ($("#other_menu").length > 0) {
        
        $("#other_menu").click(function(){
        	$(".list_menu").toggleClass("openList");
        });        
        	
        $(".list_menu .parent").click(function(){
        	$(this).toggleClass("arr_down");
        	$(this).children("ul").toggleClass("opened");	
        	return false;
        });
        
        $(".list_menu .parent a").click(function(){
        	liParent = $(this).parent('li');
        	liParent.toggleClass("arr_down");
        	liParent.children("ul").toggleClass("opened"); 
        
        	if ($(this).next('ul').length > 0)
        		return false;
        	else
                window.location.href = $(this).attr('href');
        });
    }
	/** Tutorial, Invesment Clinic **/
	
	$(".trade_parent").click(function(){
		$(this).children("ul").slideToggle(100);	
		return false;
	});
	
	
	//$(".child_h").click(function(){
	//	$(this).children("ul").toggleClass("show_child");
	//	return false;
	//});
	
	
	//toggle ebook
	$('#form_ebook').hide();
	$('.full').click(function(){
		$('#form_ebook').slideToggle(400);
	});
	
	
	//tooltip ebook
	  $('.tool_action').popover();

			  


function detect_device() {  
    	// Get browser width
    	var w = $(window).width();
    			  
    	if ( w <= 768 ) {                    
			$('#mobile').show().siblings('#desktop').hide();
		} else {                    
			$('#desktop').show().siblings('#mobile').hide();
		}    
	}            
	$(window).bind('resize',detect_device).trigger('resize');
		
  //drop down ipad
  $("#btn_term").click(function(){
    $(".drop-ipad").slideToggle(400);
    return false;
  })

  $('#tutup').click(function(){
        $(".drop-ipad").slideUp(400);
    }); 

  //fancybox
  $("#popuphadiah").fancybox({
    'transitionIn' : 'fade',
    'transitionOut' : 'fade',
    'showNavArrows' : 'true',
    'titlePosition' : 'over',
    'speedIn' : 600,
    'speedOut' : 200
  }); 

   $(".youtube").colorbox({iframe:true, innerWidth:725, innerHeight:644});
  

});



var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    	// If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
    	// If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
    	// If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
} ();

// add WEBARQ
$(window).load(function(){

  $(".slide_little3").jCarouselLite({
    start: 0,
    hoverPause:true,
    visible: 3,
    auto:6000,
    speed:800,
    btnNext: "a.bt_prev3",
    btnPrev: "a.bt_next3"
  });
  $(".slide_little").jCarouselLite({
		start: 0,
		hoverPause:true,
		visible: 3,
		auto:4000,
		speed:800,
		btnNext: "a.bt_prev2",
		btnPrev: "a.bt_next2"
	});

	$(".slide_little2").jCarouselLite({
		start: 0,
		hoverPause:true,
		visible: 4,
		scroll: 4,
		auto:false,
		speed:800,
		btnNext: "a.bt_prev2",
		btnPrev: "a.bt_next2"
	});

  $("header .navbar").click(function(){
  	$(".nav-collapse.collapse").fadeIn(500);
  	$(".nav-collapse.collapse ul.nav").delay(500).animate({ marginRight : '0px'},500);  	
  });
  $(".nav-collapse.collapse").click(function(){  	
  	$(".nav-collapse.collapse ul.nav").animate({ marginRight : '-250px'},500);
  	$(".nav-collapse.collapse").delay(500).fadeOut(500,function(){
  		$(".nav-collapse.collapse ul.nav,.nav-collapse.collapse").removeAttr('style');
  	});
  });
  $("ul.nav").click(function(e){
  	e.stopPropagation();
  });
  $("ul.nav li a").click(function(){
    var $this = $(this);
      $(".nav-collapse.collapse").addClass("loading");
      $(".nav-collapse.collapse ul.nav").animate({ marginRight : '-250px'},500,function(){
        window.location.href = $this.attr("href");

      });
      return false;
  });
  /*$("ul.nav li a").click(function(){
  	var $this = $(this);
  	if($this.siblings('ul').length > 0)
  	{
  		$("ul.nav li ul").slideUp(500);
  		$this.siblings('ul').delay(500).slideDown(500);  		
  		return false;
  	}
  	else
  	{
  		return true;
  	}
  });*/
  $(".side_menu_mobile").click(function() {
  	if($(".middle_page .container .left_wrap").attr('style'))
  	{
  		$(".middle_page .container .left_wrap").animate({
  			left : '-250px'
  		},300,function(){
  			$(this).removeAttr("style");
  		});
  	}
  	else
  	{
  		$(".middle_page .container .left_wrap").animate({
  			left : '0px'
  		},300);

  	}
  	
  });
  $(".form_search .close").click(function(){
  		$(this).parent().fadeOut(300);
  });
  /*$("ul.nav li a").click(function(){
  	if($(this).siblings("ul").length > 0)
  	{
  		return false;
  	}
  })*/

  
});



