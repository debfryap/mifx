// Google Map Magic
// Please use google map api v.3

var mymap;
var mygeocoder;
var mymarker;
var lastMarker = null;
var currentMarker = null;
var markersArray = [];
var overlay;
var mylatlng = {};

// Map overlays ....
// Define the overlay, derived from google.maps.OverlayView
function Label(opt_options) {
    // Initialization
    this.setValues(opt_options);
    
    // Here go the label styles
    
    
    var span = this.span_ = document.createElement('div');
    span.style.cssText = 'font-size: 12px;color:#fff;font-weight:normal;padding:2px 0px;display:block;min-height:100px;width:180px;margin:0 auto;overflow:auto;white-space:pre-wrap;word-wrap:break-word;margin-left:20px;';
                      
    var div_top = document.createElement('div');
    div_top.style.cssText = 'position: relative; left: 6px; margin-top: -48px; ' +
                            'white-space: nowrap;color:#000;' +
                            'padding: 2px;font-family: Arial; font-weight: bold;font-size: 12px;' +
                            "background-image : url('http://client.webarq.com/panin/images/map/label_top.png');"+
                            'background-color:transparent;background-repeat:no-repeat;min-height:130px;background-position:top right;';
    var div_bottom = document.createElement('div');
    div_bottom.style.cssText = //'position: relative; left: -50%; top: -10px; ' +
                            'white-space: nowrap;color:#000;margin-top:20px;margin-left:-2px;' +
                            'padding: 2px;font-family: Arial; font-weight: bold;font-size: 12px;' +
                            "background-image : url('http://client.webarq.com/panin/images/map/label_bottom.png');"+
                            'background-color:transparent;background-repeat:no-repeat;background-position:bottom left;';
    
    //div_top.appendChild(span);
    div_bottom.appendChild(span);
    div_top.appendChild(div_bottom);
    
    var div = this.div_ = document.createElement('div');
    div.appendChild(div_top);
    div.style.cssText = 'position: absolute; display: none;width:217px;';
    div.setAttribute('class','overlays');
    div.style.visibility = 'hidden';
};
 
//Label.prototype = new google.maps.OverlayView;
 
Label.prototype.onAdd = function() {
     var pane = this.getPanes().overlayImage;
     pane.appendChild(this.div_);
 
     // Ensures the label is redrawn if the text or position is changed.
     var me = this;
     this.listeners_ = [
          google.maps.event.addListener(this, 'position_changed',
               function() { me.draw(); }),
          google.maps.event.addListener(this, 'text_changed',
               function() { me.draw(); }),
          google.maps.event.addListener(this, 'clickable_changed', 
               function() { me.draw(); }),
          google.maps.event.addListener(this, 'zindex_changed',
               function() { me.draw(); })
     ];
};

Label.prototype.hide = function() {
  if (this.div_) {
    this.div_.style.visibility = "hidden";
  }
}

Label.prototype.show = function() {
  if (this.div_) {
    this.div_.style.visibility = "visible";
  }
}

Label.prototype.toggle = function() {
    if (this.div_) {
        if (this.div_.style.visibility == "hidden") {
          this.show();
        } else {
          this.hide();
        }
    }
}

Label.prototype.toggleDOM = function() {
  if (this.getMap()) {
    this.setMap(null);
  } else {
    this.setMap(this.map_);
  }
}
 
// Implement onRemove
Label.prototype.onRemove = function() {
     this.div_.parentNode.removeChild(this.div_);
 
     // Label is removed from the map, stop updating its position/text.
     for (var i = 0, I = this.listeners_.length; i < I; ++i) {
          google.maps.event.removeListener(this.listeners_[i]);
     }
};
 
// Implement draw
Label.prototype.draw = function() {
     var projection = this.getProjection();
     var position = projection.fromLatLngToDivPixel(this.get('position'));
     var div = this.div_;
     div.style.left = position.x + 'px';
     div.style.top = position.y + 'px';
     div.style.display = 'block';
     div.style.zIndex = this.get('zIndex'); //ALLOW LABEL TO OVERLAY MARKER
     this.span_.innerHTML = this.get('text').toString();
};

// End of Overlays

function custommarker(latlng,txt_overlays,marker_id){
    mymarker = new google.maps.Marker({
        position: latlng,
        map: mymap,
        icon: rooturl+'images/map/icon-map.png' /** Marker Icon Purpose. **/
    });
    mymarker.id = marker_id;
    /**
    markersArray.push(mymarker);
    hideMarkers();
    showlastMarker();
    **/
    
    /** Marker Click Listener **/
    var label = new Label({
       map: mymap
     });
    google.maps.event.addListener(mymarker, 'click', function() {
        label.toggle();
        currentMarker = true;
      });
    google.maps.event.addListener(mymarker, 'mouseout', function() {
        if (currentMarker === true) {
            //label.toggle();
            currentMarker = false;    
        }
      });
    label.bindTo('position', mymarker, 'position');
    txt_overlays = typeof txt_overlays == 'undefined' ? '' : txt_overlays;
    label.set('text',txt_overlays);
}

function addmarker(latlng,marker_id){
    marker_id = typeof id == 'undefined' ? 'marker_id' : marker_id;
    
    mymarker = new google.maps.Marker({
        position: latlng,
        map: mymap,
    });
    
    mymarker.id = marker_id;
    
    markersArray.push(mymarker);
    hideMarkers();
    showlastMarker();
}

function hideMarkers() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    }
}

function showlastMarker() {
    if (markersArray) {      
        lastMarker = markersArray.length - 1;      
        markersArray[lastMarker].setMap(mymap);
    }
}

function set_map(latitude,longitude,elem){
    latitude  = typeof latitude  == 'undefined' ? -6.1927792131184916 : latitude;
    longitude = typeof longitude == 'undefined' ? 106.79783284664154 : longitude;
    elem      = typeof elem      == 'undefined' ? 'mymap' : elem;
    
    WQ = new google.maps.LatLng(latitude,longitude);

    var mymapoption = {
        zoom: 15,
        center: WQ,
        mapTypeId: google.maps.MapTypeId.ROADMAP /** ROADMAP or HYBRID **/
        };
    mymap        = new google.maps.Map(document.getElementById(elem),mymapoption);
    mygeocoder   = new google.maps.Geocoder();
        
    google.maps.event.addListener(mymap,'click',function(event){
        set_latlng(event.latLng);
        
        addmarker(event.latLng);
        
    });
    /** **/
}

function get_lat_lng() {
    return mylatlng;
}

function set_latlng(loc) {
    mylatlng.lat = loc.lat();
    mylatlng.lng = loc.lng();
    
    if (typeof lat_frm_input !== 'undefined' && typeof lng_frm_input !== 'undefined') {
        $(lat_frm_input).val(mylatlng.lat);
        $(lng_frm_input).val(mylatlng.lng);
    }
}