/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config )
{   
    // General editor configuration
    config.uiColor = '#158DB5';
    config.resize_enabled = false;
     
    // Toolbar Configuration
        
    config.toolbar = 'simple';
    config.toolbar_simple = 
        [
            { name: 'basicstyles', items : [ 'Bold','Italic' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'tools', items : [ 'Source' ] }
        ];
        
    config.toolbar = 'simplex';
    config.toolbar_simplex = 
        [
            { name: 'basicstyles', items : [ 'Bold','Italic' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'tools', items : [ 'Source' ] }
        ];
        
        
    config.toolbar = 'full';
    config.toolbar_full =
        [            
        	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
        	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll'] },
        	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
        	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
        	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },        	
        	'/',
        	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
        	{ name: 'insert', items : [ 'Image','Table','HorizontalRule','PageBreak'] },
        	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
        	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
        	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','Templates','Source' ] }
        ];
        
        
    config.toolbar = 'mega';
    config.toolbar_mega =
        [            
        	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
        	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll'] },
        	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
        	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
        	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
        	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','Templates','Source' ] },        	
        	'/',
        	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
        	{ name: 'insert', items : [ 'Image','Table','HorizontalRule','PageBreak'] },
        	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
        	{ name: 'colors', items : [ 'TextColor','BGColor' ] }
        ];
    
   // File Browser
   config.filebrowserBrowseUrl		= bs_root+'assets/__system/lib/kcfinder/browse.php?type=files&cms=wcms&activecms='+cms_cookie;
   config.filebrowserImageBrowseUrl = bs_root+'assets/__system/lib/kcfinder/browse.php?type=images&cms=wcms&activecms='+cms_cookie;
   config.filebrowserFlashBrowseUrl = bs_root+'assets/__system/lib/kcfinder/browse.php?type=flash&cms=wcms&activecms='+cms_cookie;
   config.filebrowserUploadUrl 		= bs_root+'assets/__system/lib/kcfinder/upload.php?type=files&cms=wcms&activecms='+cms_cookie;
   config.filebrowserImageUploadUrl = bs_root+'assets/__system/lib/kcfinder/upload.php?type=images&cms=wcms&activecms='+cms_cookie;
   config.filebrowserFlashUploadUrl = bs_root+'assets/__system/lib/kcfinder/upload.php?type=flash&cms=wcms&activecms='+cms_cookie;
   
   // Template
   config.templates_replaceContent = false;   
   config.templates_files = [ bs_root+'assets/default/site/js/ckeditor-template.js' ];
   
   // Extra content
   config.allowedContent  = true;
   config.extraAllowedContent = '*(*)';
   config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre;address;div;span';
   config.format_span = { elemen : 'span' }; 
   
   //Extra css
   config.contentsCss = bs_root+'assets/default/site/css/ckeditor.css';
};