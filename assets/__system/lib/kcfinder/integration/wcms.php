<?php
    $wcms_invalid_message = 'Invalid request';
    
    $cookie_admin         = empty($_GET['activecms']) ? null : $_COOKIE[$_GET['activecms']];
    
    if (empty($cookie_admin)) {
        die($wcms_invalid_message);
    } else {
        if (!defined('SYSPATH'))
            define('SYSPATH',TRUE);
            
        if (file_exists('../../../../__apps/config/database.php')) {
            $file_path = '../../../../__apps/config/database.php';
        } elseif(file_exists('../../../../__sys/config/database.php')) {
            $file_path = '../../../../__sys/config/database.php';    
        } else {
            die($wcms_invalid_message);
        }
        
        $wcms_configs = include ($file_path);
        if (is_array($wcms_configs) && isset($wcms_configs['default']['connection'])) {
            $wcms_con  = $wcms_configs['default']['connection'];
            $wcms_link = mysql_connect($wcms_con['hostname'], $wcms_con['username'], $wcms_con['password']);
            if (!$wcms_link) {
                die($wcms_invalid_message);
            }
            
            $wcms_session = explode('::',$cookie_admin);
            $index_session =  count($wcms_session)-1;
            $wcms_session = $wcms_session[$index_session];
            
            // make foo the current db
            $db_selected = mysql_select_db($wcms_con['database'], $wcms_link);
            if (!$db_selected) {
                die($wcms_invalid_message);
            } 
            
            $q = mysql_query("SELECT `r`.`is_admin`
                                    FROM `user_sessions` `us`
                                    LEFT JOIN `users` `u` ON `u`.`id` = `us`.`user_id`
                                    LEFT JOIN `roles` `r` ON `r`.`id` = `u`.`role_id`
                                    WHERE `us`.`session` = '".$wcms_session."' AND `us`.`logout` IS NULL");
            if (!$q) {
                die($wcms_invalid_message);    
            } else {                    
                while ($row = mysql_fetch_assoc($q)) {
                    if (empty($row->is_admin)) {
                        $admin = false;
                    } else {
                        $admin = true;
                    }    
                    return $wcms_link;
                }
                
                if (empty($admin)) {
                    die($wcms_invalid_message);
                }
            }
                
        } else {
            die($wcms_invalid_message);    
        }
        
    }

?>