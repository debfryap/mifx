<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Widget viewer
 * @Module      Language
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/  

#if (empty(Language::$active->current()->code)) return; 

/*

<span class="language">
    <?php
        foreach (Language::$active as $lang) {
            if ($lang->code == Language::$current) {
                echo '<a class="lang1" title="',$lang->label,'" class="active" href="#">',$lang->label,'</a>'; break; 
            } 
        } ?>
    <p class="lang2">
        <?php 
            foreach (Language::$active as $lang) {
                
                if ($lang->code == Language::$current) continue; 
                
                echo '<a title="',$lang->label,'"';
                if ($lang->code == Language::$current) {
                    echo ' class="active"'; $href = '#'; 
                } else {
                    $href = Translate::uri($lang->code);
                }
                echo ' href="',$href,'">',$lang->label,'</a>';
            } ?>
    </p>
</span>
*/
?>
<span class="language_new">
    <?php 
        foreach (Language::$active as $lang) {
            
            echo '<a title="',$lang->label,'"';
            
            if ($lang->code == Language::$current) {
                echo ' class="active"'; $href = '#'; 
            } else {
                $href = Translate::uri($lang->code);
                if (!empty($translate_url))
                {
                    if (!empty($translate_url['data']))
                    {
                        switch($translate_url['type'])
                        {
                            case "article" :
                                $getLink = DB::select('row_value')->from('translations')->where('row_id','=',$translate_url['data']->id)->and_where('language_code','=',$lang->code)->and_where('row_table','=','news')->and_where('row_column','=','permalink')->execute()->get('row_value');
                                if (!empty($getLink))
                                {
                                    $href = str_replace("/" . $translate_url['data']->default_permalink, "/" . $getLink , $href);
                                }
                                else
                                {
                                    $href = str_replace("/" . $translate_url['data']->permalink, "/" . $translate_url['data']->default_permalink , $href);
                                }
                            break;
                        }
                    }
                }
            }
            echo ' href="',$href,'"><img src="', URL::templateImage('content/lang/'. $lang->code .'.png'),'" width="24" height="17" /></a>';
        } 
    ?>
</span>