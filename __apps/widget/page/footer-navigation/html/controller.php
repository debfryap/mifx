<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_FooterNavigation extends Populate {
    
    private $_object;
    
    private $_active_uri;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        $this->_object = Model::factory('Navigation')->footer()->order_by('sn.parent_id')->order_by('sn.ordering')->execute();
            
        $this->_active_uri = App::tail_active_uri()->get('tail');
    }
    
    public function render() {
        if (empty($this->_object))
            return null;
        
        $html  = '<ul class="foot_nav">';        
        $html .= $this->navigation_ul($this->_object);
        $html .= '</ul>';
        
        return $html;
    }
    
    private function navigation_ul($object) {        
        $html = '';
        
        foreach ($object as $item) {
            
            $item = Translate::item('site_navigations',$item);
            
            $is_active = $item->permalink == App::$config->menu->active 
                            || $item->permalink == App::$config->menu->group
                                || (!empty($this->_active_uri) && in_array($item->permalink,$this->_active_uri));
            
            $html .= '<li>';
            $html .= HTML::anchor(Page::get("completedNavigationUri:$item->permalink"),$item->label);
            $html .= '</li>';
        }
        
        return $html;
    }
}