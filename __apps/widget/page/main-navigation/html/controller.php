<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_MainNavigation extends Populate {
    
    protected $navigation;
    
    private $_active_uri;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        if (!empty(App::$module->language))
            #$this->navigation = Translate::item('site_navigations',$this->navigation,false);
                        
        $this->_active_uri = App::tail_active_uri()->get('tail');
    }
    
    public function render() {
        if (empty($this->navigation))
            return null;
            
        $html  = '<div class="main_menu"><div class="container"><div class="navbar">
                <img src="'. URL::templateImage('material/button_menu.png').'" alt="" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" width="45" height="23" />          
            </div>
            
            <div class="mobile-accountbutton">
                <a href="'.Translate::uri(Language::$current, 'open-demo-account.php').'"><img src="'.URL::templateImage('material/demo-account-mobile.png').'" class="demo-img" /></a>
                <a href="'.Translate::uri(Language::$current, 'open-live-account.php').'"><img src="'.URL::templateImage('material/live-account-mobile.png').'" class="live-img" /></a>
                <br class="clear" />
            </div><div class="nav-collapse collapse">';        
        $html .= $this->navigation_ul($this->navigation);
        //$html .= $this->mobile_ul($this->navigation);
        $html .= '</div></div></div>';
        
        return $html;
    }
    
    private function navigation_ul($object, $parent_uri = null, $level = 0) {
        
        if ($level == 0)
            $html = '<ul id="desktop" class="nav">';
        else
            $html = '<ul class="child0'.$level.'">';
        
        foreach ($object as $idx => $item) {
            $getActive  = Page::get('navigationRoot');       
            
            $activeLink =  !empty(App::$config->menu->active) ? App::$config->menu->active : (empty($getActive) ? 'home' : $getActive);
            $is_active  = $level == 0 &&  $item->permalink == $activeLink;
            
            $uri   = !isset($parent_uri) ? $item->permalink : $parent_uri.'/'.$item->permalink;
            if ($level == 0)
                $html .= $idx == count($object)-1 ? '<li class="last parent">'  : '<li class="parent">';
            elseif (!empty($item->child))
                $html .= '<li class="have_child">';
            else
                $html .= '<li>';
            
            if (!empty(App::$module->language) && Language::$current != Language::$default)
            {
                $href = Translate::uri(Language::$current,Language::$current.'/'.$uri);
            }                
            else
            {
                $href = URL::front($uri);
            }  
            
            #$href  = empty($item->child) || $level == 0 ? Page::get("completedNavigationUri:$item->permalink") : '#';
            $href = Page::get("completedNavigationUri:$item->permalink");
            
            /**
            if ($level == 0 && $item->permalink == 'about-us' && !empty($item->child)) {
                foreach ($item->child as $child) {
                    if (!empty(App::$module->language) && Language::$current != Language::$default) {
                        $child = Translate::item('site_navigations',$child);
                    }
                    $href .= '/'. $child->permalink;
                    break;
                }    
            }
            **/    
            
            $href  = strtolower($uri) == 'home' && $level == 0 ? URL::base() : $href;
                            
            $html .= '<a href="'.$href.'"' . ($is_active ? ' class="active '.$activeLink.'"' : '') . (strtolower($item->label) =='monexnews'? ' target="_blank"' : ''). '>';
          
            $html .= $item->label;
            
            if ($level == 0)
            $html .= HTML::image(URL::templateImage('material/arr_down_menu.png'),array('alt'=>''));
            $html .= '</a>';        
                        
            if (!empty($item->child))
                $html .= $this->navigation_ul($item->child,$uri,$level+1);
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
    
    private function mobile_ul($object, $parent_uri = null, $level = 0) {
        
        if ($level == 0)
            $html = '<ul id="mobile" class="nav">';
        elseif ($level == 1)
            $html = '<ul class="dropdown-menu">';
        else
            $html = '<ul class="hide_child">';
        
        foreach ($object as $i => $item) {
            
            $is_active = $item->permalink == App::$config->menu->active 
                            || $item->permalink == App::$config->menu->group
                                || (!empty($this->_active_uri) && in_array($item->permalink,$this->_active_uri));
            
            $uri   = !isset($parent_uri) ? $item->permalink : $parent_uri.'/'.$item->permalink;
            
            if (empty($item->child)) { 
                $html .= '<li>';
            } else {    
                if ($level == 0)
                    $html .= '<li class="dropdown">';
                elseif ($level == 1)
                    $html .= '<li class="child_h">';
                else
                    $html .= '<li>';
            }
            
            if (!empty(App::$module->language) && Language::$current != Language::$default)
            {
                $href = Translate::uri(Language::$current,Language::$current.'/'.$uri);
            }                
            else
            {
                $href  = URL::front($uri);
            }
            
            $href = Page::get("completedNavigationUri:$item->permalink");
            
            if ($i == 0 && !empty($item->child)) {
                $html .= HTML::anchor($href,$item->label,array('class'=>'current_child'));
            }
            
            
            if (!empty($item->child) && $level == 0) {
                //<a class="current_child" href="about.php">About Us</a>
                $html .= HTML::anchor( $href, $item->label, array('class'=>'current_child') );
            } else {
                $html .= '<a href="'.$href.'">';
                
                $html .= $item->label;
                
                if ($level == 0)
                $html .= HTML::image(URL::templateImage('material/arr_down_menu.png'),array('alt'=>''));
                
                $html .= '</a>';
            }            
            
            if (!empty($item->child)) {
                if ($level == 0) {
                    $html .= HTML::anchor('#', $item->label . HTML::image( URL::templateImage('material/arr_down_menu.png') ), array('class'=>'parent dropdown-toggle','data-toggle'=>'dropdown'));
                }
                //<a class="parent dropdown-toggle" data-toggle="dropdown" href="#">About Us <img src="images/material/arr_down_menu.png" alt=""/></a>
                $html .= $this->mobile_ul($item->child,$uri,$level+1);
            }
                
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        return $html;
    }
}