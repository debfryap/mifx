<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_SideNavigation extends Populate {
    
    protected $active = null;
    
    protected $showNavigation;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);    
    }
    
    public function render() {
        
        $nav = Page::get('navigationObject');
        
        if (empty($nav)) return null;
                    
        $html  = '<div class="side_menu">';
        
        foreach ($nav as $n) {
            //Just continue if active URL not match
            if ($n->permalink !== Page::get('navigationRoot')) continue;
            
            //Return null if something does not have child
            if (empty($n->child)) return null;                          
            
            $html .= '<h3>' . $n->label .'</h3>';
            
            $html .= $this->navigation_ul($n->child,$n->permalink);
            
            //Does not need to check another navigation
            break;
        }
            
        $html .= '</div>';
        
        return $html;
    }
    
    private function navigation_ul($object, $parent_uri = null, $level = 0) {
        
        $html = '<ul>';
        
        foreach ($object as $idx => $item) {
            
            $tail_uri  = App::tail_active_uri();
            
            $is_active = (!empty($tail_uri->tail) && in_array($item->permalink,$tail_uri->tail)) || $this->active === $item->permalink;
            
            if (!empty($this->showNavigation) && is_string($this->showNavigation)) {
                $is_active = $this->showNavigation === $item->permalink;
            }
                        
            $uri   = !isset($parent_uri) ? $item->permalink : $parent_uri.'/'.$item->permalink;
            
            $html .= '<li' . ($is_active ? ' class="active"' : '') . '>';
            
            if (!empty(App::$module->language) && Language::$current != Language::$default)
            {
                $href = Translate::uri(Language::$current,Language::$current.'/'.$uri);
            }                
            else
            {
                $href = URL::front($uri);
            }
            
            if ($level == 0 && !empty($item->child)) {
                foreach ($item->child as $child) {
                    if (!empty(App::$module->language) && Language::$current != Language::$default) {
                        $child = Translate::item('site_navigations',$child);
                    }
                    $href .= '/'. $child->permalink;
                    break;
                }    
            }
            
            //$href  = empty($item->child) || $level == 0 ? $href : '#';
            $href  = empty($item->child) || $level == 0 ? Page::get("completedNavigationUri:$item->permalink") : '#';
                
            $html .= '<a href="'.$href.'">';
            
            $html .= $item->label;
            
            $html .= '</a>';
            
            if (!empty($item->child) && !empty($tail_uri->tail) && in_array($item->permalink,$tail_uri->tail)) {
                $html .= HTML::image(URL::templateImage('material/active_side.png'),array('alt'=>''));            
                $html .= $this->navigation_ul($item->child,$uri,$level+1);
            }
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
}