<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_ShortcutNavigation extends Populate {
    
    protected $active = null;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
    }
    
    public function render() {
        
        $nav =  Model::factory('Navigation')->shortcut()->order_by('sn.parent_id')->order_by('sn.ordering')->execute();
        
        if (empty($nav)) return null;
                    
        $html  = '<div class="menuOther">';
        $html .= '<ul>';
        
        foreach ($nav as $n) {
            $media = Model::factory('Medialibrary')->simple_application('shortcut-icon',$n->id,'site_navigations')->current();
            
            if (empty($media))
                $attrs = null;
            else
                $attrs['style'] = "background:url('" . URL::mediaImage( $media->folder . '/' . $media->file , null ) . "') left center no-repeat;";
                
            $h = Page::get("completedNavigationUri:$n->id");
            $a = array();
            
            if($n->id == 72){
                $a['onclick'] = "ga('send', 'event', { eventCategory: 'Contact Us', eventAction: 'Contact Form'});";
            }
            
            if ($n->id == 73) {
                $h = 'http://www.comm100.com/livechat/';
                //$a['onclick'] = 'comm100_Chat();return false;';
                $h = '#';
                $a['id'] = 'comm100-button-2552';
            }
            $html .= '<li ' . HTML::attributes($attrs) . '>';
            $html .= HTML::anchor($h, Page::get("simpleBreadCrumb:$n->id"), $a);    
            $html .= '</li>';
        }
        $html .= '</ul>';            
        $html .= '</div>';
        
        $html .= "<script type=\"text/javascript\">
                        var Comm100API = Comm100API || new Object;
                        Comm100API.chat_buttons = Comm100API.chat_buttons || [];
                        var comm100_chatButton = new Object;
                        comm100_chatButton.code_plan = 2552;
                        comm100_chatButton.div_id = 'comm100-button-2552';
                        Comm100API.chat_buttons.push(comm100_chatButton);
                        Comm100API.site_id = 77063;
                        Comm100API.main_code_plan = 2552;
                        var comm100_lc = document.createElement('script');
                        comm100_lc.type = 'text/javascript';
                        comm100_lc.async = true;
                        comm100_lc.src = 'https://chatserver.comm100.com/livechat.ashx?siteId=' + Comm100API.site_id;
                        var comm100_s = document.getElementsByTagName('script')[0];
                        comm100_s.parentNode.insertBefore(comm100_lc, comm100_s);
                    </script>";
        
        return $html;
    }
}