<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_AutoBanner extends Populate {
    
    protected $navigation;
    
    private $_active_uri;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
    }
    
    public function render() {
        $urlTail = App::tail_active_uri();
        $urlId   = Model::factory('Navigation')->select('id')
                                               ->from('site_navigations')
                                               ->where('permalink','=',empty($urlTail->small_uri) ? 'home' : $urlTail->small_uri)
                                               ->execute()->get('id');        
        
        $html = "</section>";
        if ($urlId == 1) {     
            $slides  = Model::factory('Medialibrary')->simple_application('slide-'. Language::$current,$urlId,'site_navigations');
            
            $html  = '<section class="banner_home">';                                           //Open  banner
            $html .= '<section class="slider"><div class="flexslider"><ul class="slides">';     //Open  slide
            
            if ($slides->valid()) {
                foreach ($slides as $slide){

                    $detail = json_decode($slide->detail);
                        
                    if (!empty($detail->banner_permalink)) {
                        if (strpos($detail->banner_permalink,'http') !== false) {
                            $bannerUri = $detail->banner_permalink;
                            //$bannerAttrs['target'] = '_blank';
                        } else {
                            $bannerUri = Translate::uri(Language::$current,$detail->banner_permalink);
                        }
                    } else {
                        $bannerUri = '#';
                    }
                    if(isset($detail->title)){
                      $titl = !empty($detail->title) ? $detail->title : Contact::$config->office->name;
                    } else {
                      $titl = Contact::$config->office->name; 
                    }
 
                    $attr = null;
                    
                    if($detail->banner_permalink == 'http://www.mifx.com/pemenang'){
                        $bannerUri = '#popup1';
                        $attr = array('id' => 'popuphadiah'); 
                    } 
					
					if($detail->banner_permalink == 'http://www.mifx.com/MIC2015'){
                        $bannerUri = '#popup2';
                        $attr = array('id' => 'popuphadiah'); 
                    } 

                    $url_image = URL::mediaImage($slide->folder . '/' . $slide->file,null);
                    list($width, $height) = getimagesize($url_image);

                    $html .= '<li>' . HTML::anchor($bannerUri, HTML::image($url_image ,array('alt' => $titl , 'title' => $titl, 'width' => $width, 'height' => $height )), $attr) . '</li>';    
                }   
            }
            
            $html .= '</ul></div></section>';                                                   //Close slide            
            
            $html .= '<div class="container account_wrap3">'; // old script
            //$html .= '<div class="container account_wrap3">';
            $html .= View::factory('general/frm-account')->set('class','homepage');
            $html .= '</div>';
        } else {
            if (empty($urlId) && !empty($urlTail->parent_uri)) {
                $urlId   = Model::factory('Navigation')->select('id')
                                       ->from('site_navigations')
                                       ->where('permalink','=',$urlTail->parent_uri)
                                       ->execute()->get('id');    
            }
            
            $banner = Model::factory('Medialibrary')->simple_application('main-banner-'. Language::$current,$urlId,'site_navigations')->current();
            if (empty($banner) && !empty($urlTail->parent_uri)) {
                $urlId  = Model::factory('Navigation')->select('id')->from('site_navigations')->where('permalink','=',$urlTail->parent_uri)->execute()->get('id');
                $banner = Model::factory('Medialibrary')->simple_application('main-banner-'. Language::$current, $urlId,'site_navigations')->current();
            }
            
            if(App::$config->menu->active !== 'errors'){
            
                $html  = '<section class="banner_page">';
                
                if (!empty($banner)) {
                    $html .= HTML::image(URL::mediaImage($banner->folder . '/' . $banner->file,null),array('alt'));    
                }
            }
        }
        
        $html .= '</section>';  //Close banner
        
        return $html;
    }
}