<?php defined('SYSPATH') or die('No direct script access.');

return array  (
    'default' => array (
        'media_library_application_types'  => array (
            'image'         => array (
            
                'slide'         => array (
                    'label'     => 'image-slider',
                    'width'     => 1275,
                    'height'    => 444,
                    'details'   => null
                ),
                
                'main-banner'   => array (
                    'width'     => 1000,
                    'height'    => 280,
                    'limit'     => 1
                ),

                 
                // banner multilang
                'main-banner-id'   => array (
                    'width'     => 1000,
                    'height'    => 280,
                    'limit'     => 1
                ),
                'main-banner-en'   => array (
                    'width'     => 1000,
                    'height'    => 280,
                    'limit'     => 1
                ),
                'main-banner-cn'   => array (
                    'width'     => 1000,
                    'height'    => 280,
                    'limit'     => 1
                ),
                'main-banner-tw'   => array (
                    'width'     => 1000,
                    'height'    => 280,
                    'limit'     => 1
                ),
                // end here //
                
                
                'page-banner'   => array (
                    'width'     => 590,
                    'height'    => 290,
                    'limit'     => 1
                ),
                
                'updates-banner'   => array (
                    'width'     => 590,
                    'height'    => 210,
                    'limit'     => 1
                ),
                
                'logo-partner'  => array (
                    'height'    => 45,
                    'limit'     => 7,
                    'details'   => array (
                        array (
                            'label' => 'banner-link',
                            'name'  => 'permalink',
                            'class' => 'url'
                        )
                    ) 
                ),
                
                'widget-icon'=> array (
                    'label'     => 'widget-icon',
                    'width'     => 48,
                    'height'    => 44,
                    'limit'     => 1
                ),
                
                'widget-account'=> array (
                    'label'     => 'widget-account',
                    'width'     => 59,
                    'height'    => 57,
                    'limit'     => 1
                ),
                
                'tool-video'    => array (
                    'width'     => 27,
                    'height'    => 27,
                    'limit'     => 1
                ),

                'banner-trading' => array(
                    'label'     => 'banner-trading',
                    'width'     => 600,
                    'height'    => 322,
                    'limit'     => 1  
                ) ,
                'banner-310x172' => array(
                    'label'     => 'banner-310x172',
                    'width'     => 310,
                    'height'    => 172,
                    'limit'     => 1

                ),
                'banner-410x375' => array(
                    'label'     => 'banner-410x375',
                    'width'     => 410,
                    'height'    => 375,
                    'limit'     => 1

                ),
                'widget-trading' => array(
                    'label'     => 'widget-trading',
                    'width'     => 100,
                    'height'    => 66,
                    'limit'     => 1  
                ),
                'banner-account' => array (
                    'width'      => 640,
                    'height'     => 520,
                    'limit'      => 1
                ),
                'shortcut-icon' => array (
                    'width'      => 18,
                    'height'     => 15,
                    'limit'      => 1
                ),
                
                
                'home-banner'   => array (
                    "width"     => 220,
                    "height"    => 145,
                    "limit"     => 4,
                    'details'   => array (
                        array (
                            'label' => 'banner-title1',
                            'name'  => 'banner_title1',
                            'class' => 'required'
                        ),

                        // add banner title
                        array (
                            'label' => 'banner-title1-cn',
                            'name'  => 'banner_title1_cn'
                        ),
                        array (
                            'label' => 'banner-title1-tw',
                            'name'  => 'banner_title1_tw'
                        ),
                        // end here //

                        array (
                            'label' => 'Banner Link 1',
                            'name'  => 'banner_permalink'
                        ),

                        array (
                            'label' => 'banner-title2',
                            'name'  => 'banner_title2',
                            'id'    => 'banner_title2-id'
                        ),

                        // add banner title //
                        array (
                            'label' => 'banner-title2-cn',
                            'name'  => 'banner_title2_cn',
                            'id'    => 'banner_title2-id-cn'
                        ),
                        array (
                            'label' => 'banner-title2-tw',
                            'name'  => 'banner_title2_tw',
                            'id'    => 'banner_title2-id-tw'
                        ),
                        // end here //

                        array (
                            'label' => 'Banner Link 2',
                            'name'  => 'banner_permalink_2'
                        )
                    ) 
                ),
                'page-left-bnnr' => array (
                    'width'      => 290,
                    'height'     => 145,
                    'details'   => array (
                        array (
                            'label' => 'banner-link',
                            'name'  => 'banner_permalink',
                            'class' => 'url'
                        ),
                        array (
                            'label' => 'banner-title',
                            'name'  => 'banner_title'
                        ),

                        // add banner title
                        array (
                            'label' => 'banner-title-cn',
                            'name'  => 'banner_title_cn'
                        ),
                        array (
                            'label' => 'banner-title-tw',
                            'name'  => 'banner_title_tw'
                        )
                    ) 
                ) ,
                'banner-page' => array (
                    'label'     => 'banner-page',
                    'width'      => 590,
                    'height'     => 290,
                    'limit'      => 1
                ),
                'ebook-large' => array (
                    'label'     => 'ebook-large',
                    'width'      => 178,
                    'height'     => 202,
                    'limit'      => 1
                ),
                'ebook-small' => array (
                    'label'     => 'ebook-small',
                    'width'      => 110,
                    'height'     => 122,
                    'limit'      => 1
                ),
                
                // Translate Two Banner
                'slide-id'         => array (
                    'label'     => 'image-slider-id',
                    'width'     => 1275,
                    'height'    => 444,
                    'details'   => array (
                        array (
                            'label' => 'banner-link',
                            'name'  => 'banner_permalink',
                            'class' => 'url'
                        ),
                        array (
                            'label' => 'title',
                            'name'  => 'title',
                        )
                        )
                ),
                
                'slide-en'         => array (
                    'label'     => 'image-slider-en',
                    'width'     => 1275,
                    'height'    => 444,
                    'details'   => array (
                        array (
                            'label' => 'banner-link',
                            'name'  => 'banner_permalink',
                            'class' => 'url'
                        ),
                        array (
                            'label' => 'title',
                            'name'  => 'title',
                        ))
                ),

                'slide-cn'         => array (
                    'label'     => 'image-slider-cn',
                    'width'     => 1275,
                    'height'    => 444,
                    'details'   => array (
                        array (
                            'label' => 'banner-link',
                            'name'  => 'banner_permalink',
                            'class' => 'url'
                        ),
                        array (
                            'label' => 'title',
                            'name'  => 'title',
                        ))
                ),

                'slide-tw'         => array (
                    'label'     => 'image-slider-tw',
                    'width'     => 1275,
                    'height'    => 444,
                    'details'   => array (
                        array (
                            'label' => 'banner-link',
                            'name'  => 'banner_permalink',
                            'class' => 'url'
                        ),
                        array (
                            'label' => 'title',
                            'name'  => 'title',
                        ))
                ),
                // end here
            )
        ) 
    )
);            