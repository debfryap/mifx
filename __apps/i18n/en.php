<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'Buat'                          =>'Create',
    'Live Akun'                =>'Live Account',
	'Demo Akun'                =>'Demo Account',
    'telp'                          => 'Phone',
    'fax'                           => 'Fax',
    'widget-icon'                   => 'Widget Icon',
    'shortcut-icon'                 => 'Shortcut Icon',
    'home-banner'                   => 'Banner',
    'banner-title1'                 => 'Title 1',
    'banner-title2'                 => 'Title 2',
    'banner-title'                  => 'Title',
    'page-left-bnnr'                => 'Left Banner',
    'meta-title'                    => 'Meta Title',
    'meta-description'              => 'Meta Description',
    'pilih-cabang'                  => 'Pilih Cabang',
    
    # online trading
    'banner-trading'                => 'Banner Trading',
    'widget-trading'                => 'Widget Trading',
    'banner-310x172'                => 'Banner 310 x 172',
    'banner-410x375'                => 'Banner 410 x 375', 
    'banner-page'                   => 'Banner Page', 
    'tool-video'                    => 'Tools Image',
    'banner-account'                => 'Banner Account',
    'widget-account'                => 'Widget Account',
    'create-free-demo-account'      => 'Create Demo Account',
    'create-monex-live-account'     => 'Create Live Account',
    'select-city'                   => 'Select City',
    'full-name'                     => 'Full Name',
    'phone'                         => 'Phone',
    'email'                         => 'Email',
    'deposit-awal'                  => 'Deposit Awal',
    'kode-verifikasi'               => 'Verification',
    # bad words edited by ahyo@mifx.com
    'privacy-agreement'             => 'You have read and agree Monex\'s <span>:privacy</span>',
    'opening-deposit'               => 'Initial Deposit',
    'referral'                      => 'Broker Code',
    'submit'                        => 'Submit',
    'risk-free'                     => 'Risk Free !!!',

    'contact-us'                    => 'Contact Us',
    'logo-partner'                  => 'Logo Partner',
    'banner-link'                   => 'Banner Link',
    'updates-banner'                => 'Updates Banner',
    'share'                         => 'Share',
    'no-data-found-title'           => 'Could not find requested page',
    'no-data-found-description'     => 'Your requested page could not be found on this site. Maybe it was move to new address or never exist. Please contact your web administrator',
    'no-data-found'                 => 'There is no data found for :group',
    'list-item'                     => 'List',
    'jadwal-monex-edukasi'          => '<:tag>Monex Education</:tag> Schedule',
    'ticket-information'            => 'Ticket Information',
    'back-to-list'                  => 'Back to :group',
    'judul-icon'                    => 'Judul Widget',
    'highlight'                     => 'Highlight',
    
    'others-article'                => 'Other Article',    
    'no-berita-found'               => 'Could not find requested article',
    'back-to-article-list'          => 'Back to Article List',
    
    'no-glossary-found'             => 'Could not find requested glossary',
    'back-to-glossary-list'         => 'Back to Glossary List',
    
    'previous-tutorial'             => 'Previous Tutorial',
    'next-tutorial'                 => 'Next Tutorial',
    'do-not-close-browser'          => 'Please wait for a while, and please do not close your browser',
    'error-captcha'                 => 'Captcha is wrong. Please enter again',
    'captcha-not-valid'             => 'Captcha did not match. Please correct',
    'verified-normal-error-title'   => 'Error',
    'verified-normal-error-desc'    => 'Oops ... your account could not be verified for this time. Reason :reason',
    'verified-fatal-error-title'    => 'Fatal Error',
    'verified-fatal-error-desc'     => 'Your account could not be verified for this time. Reason :reason. Please contact your administrator',
    'verified-success-title'        => 'Congratulations',
    'verified-success-desc'         => 'Congratulations. Your :type account has been verified. Your account detail has sent to your email at :email',

    // ebook form
    'manage-ebook'                  => 'Manage E-Book',
    'ebook'                         => 'E-Book',
    'ebook-title'                   => 'Title',
    'permalink'                     => 'Permalink',
    'ebook-description'             => 'Description',
    'url-demo'                      => 'URL Demo',
    'url-full'                      => 'URL Full',
    'create-ebook'                  => 'Create Ebook',
    // ebook widget
    'ebook-large'                   => 'Ebook Large',
    'ebook-small'                   => 'Ebook Small',
    'order_summary'                 => 'Order Summary',
    'register'                      => 'Register ',
    'registration-information'      => 'Registration Information',
    'registration-wording'          => 'Thank you for registering for this event. Your feedback is important to us.',
    
    
    'your-information'              => 'Your Information',
    'gender'                        => 'Gender',
    'male'                          => 'Male',  
    'female'                        => 'Female',
    'first-name'                    => 'First Name',
    'last-name'                     => 'Last Name',
    'email'                         => 'Email',
    'phone'                         => 'Phone',
    'city'                          => 'City',
    'address'                       => 'Address',
    
    
    'Back to list'                  => 'Back to list',
    'search-result'                 => 'Search Result for :keyword',
    'seminar'                       => 'Seminar',
    'date'                          => 'Date',
    'quantity-ticket'               => 'Total Ticket',
    'seminar-subject'               => ':name has been registered for :seminar',
    'thank-you-for-registering-seminar' => '<p>Thank you for confirming your details.</p><p>You are still in the registration queue and have not registered.</p><p>Please  wait for further confirmation.</p><p>Below your data details : </p>',
    'acc-source-list'               => 'Source List',
    'thank-ebook-registering'       => 'Thank you. We have send detail URL of this e-book to your registered email',
    'thank-you-title'               => 'Thank You',
    'computer-generated-message'    => 'This is a computer-generated letter. Please do not reply to the above email address.',
    'confirmation'                  => 'Reminder',    
    'alert'                         => 'Warning',
    'is-monex-client'               => 'Are you a client of  Monex?',
    'is-contact-by-monex'           => 'Have you been contacted by the Monex Marketing Team ?',
    'is-client-or-contacted'        => '<p> Please sign up at each branch office.. <br/>Registration via the website only applies to those who have become, or have not been contacted by the Customer Marketing Team Monex.</p>',
    'yes-button'                    => 'Yes',
    'no-button'                     => 'No',
    'date-must-selected'            => 'Date must be selected',
    'quantity-must-not-empty'       => 'Quantity could not be empty',
    'mcs-subject-mail'              => ':no-urut # Registrasi Seminar',
    'mcs-body-mail'                 => ':nama have registered with below details : ',
    'seminar-subject-mail'          => 'Registration of Seminar',
    'footer-wording'                => 'PT. Monex Investindo Futures operates under the license and supervision of BAPPEBTI and acts as a member of JFX and ICDX as an exchange and member of clearing house; KBI and ISI Clearing.',
    'forbidden-domain-email'        => 'Forbidden email domain name. Reason <b>:domain</b>',
    'server-not-responding'         => 'Server not responding, please contact your administrator or try again',  
    
    'image-slider-id'               => 'Image Slider Id',
    'image-slider-en'               => 'Image Slider En',
    

    'banner-title-cn'               => 'Chinese',
    'banner-title-tw'               => 'Traditional Chinese',    
    
    'error-nine-stars'              => 'Terjadi kesalahan pada saat mendaftar demo :group',
    'thank-you-nine-stars'          => '<p>Thank you for registering. Here is your account detail : </p> <p><b>Username</b> : :username</br> <b>Password</b> : :password</p>',

        
    'description-branch-events'     => 'Monex event schedule branches. Please direct registration to each branch office.',
    'corporate-events'              => '<span>Monex</span> Corporate Events',         

    'main-banner-id'                => 'Main Banner Id',
    'main-banner-en'               => 'Main Banner Eng',

    // account 
    'title-success-page-account'    =>'Terima kasih telah membuka :type <span>Account</span> dengan kami.',
	'title-form-account-demo'		=> 'Create Demo Account',
	'title-form-account-live'		=> 'Create Live Account',

    // add 
    'read-more'                     => 'Read More ',
    'sign-up-for-account-today'     => 'Sign Up for Account today !',
    'partner-registration'          => 'Partner Registration',
    'company-personal-name'         => 'Company / Personal Name',
    'handphone'                     => 'Handphone',
    'forex'                         => 'Forex',
    'commodities'                   => 'Commodities',
    'cfds'                          => 'CFDs',
    'index'                         => 'Index',
    'symbol'                        => 'Symbol',
    'bid'                           => 'Bid',
    'ask'                           => 'Ask',
    'high'                          => 'High',
    'low'                           => 'Low',
    'eurusd'                        => 'EURUSD',
    'gbpusd'                        => 'GBPUSD',
    'usdchf'                        => 'USDCHF',
    'usdjpy'                        => 'USDJPY',
    'audusd'                        => 'AUDUSD',
    'hanya-indikator'               => '*Hanya Indikator',
    'google'                        => 'GOOGLE',
    'goldman'                       => 'GOLDMAN',
    'exxon'                         => 'EXXON',
    'intel'                         => 'INTEL',
    'sign-up-for-account-today-account' => ' Sign Up <br /> for <span>Account</span> today ! ',
    'corporate-events-title-list'   => 'Corporate Events',
    'select-a-date-to-attend'       => 'Select a Date to Attend ',
    'please-select'                 => 'Please Select',
    'select-one'                    => 'Please Select',
    'event'                         => 'Event',
    'ends'                          => 'Ends',
    'quantity'                      => 'Quantity',
    'branch-event-title'            => 'Branch Events',
    'location'                      => 'Location',
    'empty'                         => 'empty',
    'search'                        => 'search',
    'other-e-book'                  => 'Other e-book',
    'download'                      => 'DOWNLOAD',
    'demo'                          => 'Demo',
    'full-version'                  => 'Full Version',
    'create-demo-account'           => 'Create Demo Account',
    'banner-title1-cn'              => 'Title Chinese',
    'banner-title1-tw'              => 'Title Traditional',
    'banner-title2-cn'              => 'Title 2 Chinese',
    'banner-title2-tw'              => 'Title 2 Traditional',
    'contact-form'                  => 'Contact Form',
    'Subject of Message'            => 'Subject of Message',
    'Pesan'                         => 'Pesan',
    'kode-wakil-pialang'            => 'Kode Wakil Pialang',
    'request-demo-multilateral'     => 'Request Demo Multilateral',
    'kantor-cabang'                 => 'Kantor Cabang',
    'monex-branch'                  => '<span style="color: #078d03;">MONEX</span> Branch',
    'tutorial-monex-mt4'            => 'Tutorial Monex Trader MT4',
    
     // add translate 21 mei 2014
    'thank-you-for-opening-live-account-with-us'    => 'Thank you for opening live account with us.',
    'thank-you-for-opening-demo-account-with-us'    => 'Thank you for opening demo account with us.',

    'its-so-easy-to-open-an-account-with-monex'     => 'Its so easy to open an account with monex, just follow the following steps :',
    'verify-registration'                           => 'Verify Registration',
    'verify-registration-by-clicking-verify-link-in'=> 'Verify registration by clicking verify link in your email. Please also check your junk mail, and be sure to add our email to your safe list',
    'a-open-this-email'                             => 'a. Open this email ( Link activation demo account )',
    'b-please-click-this-link-for-activate'         => 'b. Please click this link for activate your account',
    'please-make-sure-that-you-have-entered-the'    =>'*Please make sure that you have entered the right email address ( :email ) and phone number ( :phone ) because we need to send you verification by email and possibly by sms. if you have type in the wrong information, it is very important that you correct the error.',
    'submit-registration'                           => 'Submit Registration',
    'username-and-password'                         => 'Username <br/>and Password',
    'login-account'                                 => 'Login <br/>Account',
    // verifiy
    'check-email-form-usename-and-password'         => 'Check email for username and password',
    'congratulations-your-live-account-has-been'    => 'Congratulations, your live account has been verified. Your account detail has sent to <a class="green_font" href="mailto::mail">:mail</a>, please check your email for username and password. please also check your junk mail, and be sure to add our email your safe list',
    'download-monex-trader'                         => 'Download Monex Trader',
    'this-email-send-automatically-by-system'       => 'This email send automatically by system, and you do not need to reply.',
    'thank-you'                                     => 'Terima Kasih',

    'download-about-us'                             => 'Download',
    'view'                                          => 'View',
    'monex-company-profile'                         => 'Monex Company Profile',

    'btn-read-more'                                 => '<img src="'. URL::templateImage('material/btn_read.png') .'" />',

    'each-month-we-will-give'       => '<h3 class="norborder">Each month we will give away <br/>3 <span>Apple ipad Mini</span> to successful applicants in a draw for opening a verified and funded 
                                        <span>live account</span>. Register now to live account!</h3>
                                        <br/><br/>Add Licence by Kementerian Sosial RI  : 1951/LJS.PPSDBS.PI.01.02/9/2014',
    'to-qualify'                    => '<p>** Lucky Draw Qualifications  :</p><ul>
                                        <li>Participants  considered valid to enter the Lucky Draw are the participants who have successfully opened a LIVE ACCOUNT and have deposited funds with the verified data. For each lucky draw period has a specific registration time. Participants who have registered and been declared successful above mentioned criteria, will be included in the draw.</li>
                                        <li>Participants must provide complete and valid data. Limited only to one entry. No duplication / multiple entries  .</li>
                                        <li>Entries more than once will be directly eliminated from the lucky draw system.</li>
                                        <li>The draw is not applicable to the management and employees of PT. Monex Investindo Futures, consultants and relatives.</li>
                                        <li>The winner\'s name will be posted on the company website www.mifx.com, info via SMS, email and official notification letter to the address of each winner.</li>
                                        <li>The winner and the prize is valid only for a certain period of time.</li>
                                        <li>Tax on prize will be charged to the winners.</li>
                                        <li>Other information will be attached in details on the letter of notification of award.</li>
                                        </ul>
                                        <p class="note">*The term ipad mini and logo is the property of Apple Computers.</p>',
);
