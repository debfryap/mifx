<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'telp'                          => 'Telp',
    'fax'                           => 'Fax',
    'widget-icon'                   => 'Widget Icon',
    'shortcut-icon'                 => 'Shortcut Icon',
    'home-banner'                   => 'Banner',
    'banner-title1'                 => 'Title 1',
    'banner-title2'                 => 'Title 2',
    'banner-title'                  => 'Title',
    'page-left-bnnr'                => 'Left Banner',
    'meta-title'                    => 'Meta Title',
    'meta-description'              => 'Meta Description',
    'pilih-cabang'                  => '分行选择',
    
    # online trading
    'banner-trading'                => 'Banner Trading',
    'widget-trading'                => 'Widget Trading',
    'banner-310x172'                => 'Banner 310 x 172',
    'banner-410x375'                => 'Banner 410 x 375', 
    'banner-page'                   => 'Banner Page', 
    'tool-video'                    => 'Tools Image',
    'banner-account'                => 'Banner Account',
    'widget-account'                => 'Widget Account',
    'create-free-demo-account'      => '建立模拟账户',
    'create-monex-live-account'     => '创建真实账户',
    'select-city'                   => '选取城市',
    'full-name'                     => '全名',
    'phone'                         => '电话',
    'email'                         => '电邮',
    'Email'                         => '电邮',
    'deposit-awal'                  => 'Deposit Awal',
    'kode-verifikasi'               => 'Kode Verifikasi',
    'privacy-agreement'             => '您同意并已阅读 <span>:privacy</span>',
    'opening-deposit'               => '初始保证金',
    'referral'                      => '经纪人代码',
    'submit'                        => '提交',
    'risk-free'                     => '无风险！！！',

    'contact-us'                    => 'Contact Us',
    'logo-partner'                  => 'Logo Partner',
    'banner-link'                   => 'Banner Link',
    'updates-banner'                => 'Updates Banner',
    'share'                         => '分享',
    'no-data-found-title'           => 'Tidak dapat menemukan halaman yang diinginkan',
    'no-data-found-description'     => 'Halaman URL yang anda request tidak dapat ditampilkan. Mungkin halaman ini sudah dipindahkan ke halaman URL yang berbeda, atau tidak pernah ditampilkan. Silahkan menghubungi administrator website MIFX',
    'no-data-found'                 => 'Tidak ada data yang dapat ditampilan untuk :group',
    'list-item'                     => '列表',
    'jadwal-monex-edukasi'          => 'Monex教育 <:tag>时间表</:tag>',
    'ticket-information'            => 'Ticket Information',
    'back-to-list'                  => 'Kembali ke daftar :group',
    'judul-icon'                    => 'Judul Widget',
    'highlight'                     => 'Highlight',
    
    'others-article'                => 'Artikel Lainnya',    
    'no-berita-found'               => 'Belum ada berita yang dapat ditampilkan',
    'back-to-article-list'          => 'Kembali ke daftar berita',
    
    'no-glossary-found'             => 'Belum ada kosakata yang dapat ditampilkan',
    'back-to-glossary-list'         => 'Kembali ke daftar kosakata',
     
    'previous-tutorial'             => '以前的教程',
    'next-tutorial'                 => '接下来的教程',
    'do-not-close-browser'          => 'Silahkan tunggu beberapa saat dan jangan tutup browser anda.',
    'error-captcha'                 => '验证码仍然是错误的。请重新输入',
    'verified-normal-error-title'   => 'Error',
    'verified-normal-error-desc'    => 'Ups ... your account could not be verified for this time. Reason :reason',
    'verified-fatal-error-title'    => 'Fatal Error',
    'verified-fatal-error-desc'     => 'Your account could not be verified for this time. Reason :reason. Please contact your administrator',
    'verified-success-title'        => 'Congratulations',
    'verified-success-desc'         => 'Congratulations. Your :type account has been verified. Your account detail has sent to your email at :email',

    // ebook form
    'manage-ebook'                  => 'Manage E-Book',
    'ebook'                         => 'E-Book',
    'ebook-title'                   => 'Title',
    'permalink'                     => 'Permalink',
    'ebook-description'             => 'Description',
    'url-demo'                      => 'URL Demo', 
    'url-full'                      => 'URL Full',
    'create-ebook'                  => 'Create Ebook',
    // ebook widget
    'ebook-large'                   => 'Ebook Large',
    'ebook-small'                   => 'Ebook Small',
    'order_summary'                 => 'Order Summary',
    'register'                      => 'Register ',
    'registration-information'      => 'Informasi Pendaftaran',
    'registration-wording'          => 'Terima kasih telah mendaftar untuk acara ini. Tanggapan Anda penting bagi kami.',
    
    
    'your-information'              => 'Informasi Anda',     
    'gender'                        => 'Jenis Kelamin',
    'male'                          => 'Laki-Laki',
    'female'                        => 'Perempuan',
    'first-name'                    => 'Nama Depan',
    'last-name'                     => 'Nama Belakang',
    //'email'                         => 'Email',
    //'phone'                         => 'Telepon',
    'city'                          => '城市',
    'address'                       => '地址',
    
    
    'Back to list'                  => 'Kembali',
    'search-result'                 => 'Hasil Pencarian untuk :keyword',
    'seminar'                       => 'Seminar',
    'date'                          => '日期',
    'quantity-ticket'               => 'Jumlah Tiket',
    'seminar-subject'               => ':name telah mendaftar untuk mengikuti :seminar',
    'thank-you-for-registering-seminar' => '<p>Terima kasih atas konfirmasi data diri Anda. </p><p>Anda masih dalam daftar antrian registrasi dan belum terdaftar sebagai peserta. </p><p>Mohon kiranya dapat menunggu konfirmasi selanjutnya.</p><p>Berikut data anda : </p>',
    'acc-source-list'               => 'Source List',
    'thank-ebook-registering'       => 'Terimakasih. Kami telah mengirimkan detail alamat URL untuk e-book ini ke alamat email anda.',
    'thank-you-title'               => 'Terimakasih',
    'computer-generated-message'    => 'Surat ini dihasilkan oleh komputer dan tidak memerlukan konfirmasi kembali.', 
    'confirmation'                  => 'Konfirmasi',
    'alert'                         => 'Peringatan',
    'is-monex-client'               => 'Apakah Anda sudah menjadi Nasabah Monex?',
    'is-contact-by-monex'           => 'Apakah Anda sudah pernah dihubungi oleh Tim Marketing Monex?',
    'is-client-or-contacted'        => '<p>Silahkan mendaftar pada Kantor Cabang masing-masing. <br/>Pendaftaran via website hanya berlaku bagi yang belum menjadi Nasabah ataupun belum dihubungi oleh Tim Marketing Monex.</p>',
    'yes-button'                    => 'Ya',
    'no-button'                     => 'Belum',
    'date-must-selected'            => 'Tanggal tidak boleh kosong',
    'quantity-must-not-empty'       => 'Quantity tidak boleh kosong',
    'mcs-subject-mail'              => ':no-urut # Registrasi Seminar',
    'mcs-body-mail'                 => ':nama telah melakukan pendaftaran seminar, dengan data sebagai berikut',
    'seminar-subject-mail'          => ':no-urut # Registrasi Seminar',
    'footer-wording'                => 'PT. Monex Investindo Futures是在BAPPEBTI的监管之下按许可证经营，为雅加达期货交易所（BBJ） 及印尼商品及衍生品交易所（BKDI）会员与印尼期货结算所（KBI）及 ISI结算所会员。',
    'forbidden-domain-email'        => 'Email dengan domain <b>:domain</b> tidak diperbolehkan',
    'server-not-responding'         => 'Tidak ada respon dari server. Silahkan hubungi administrator anda atau coba sekali lagi',
    
    'image-slider-id'               => 'Image Slider Id',
    'image-slider-en'               => 'Image Slider En',
    
    
    'error-nine-stars'              => 'Terjadi kesalahan pada saat mendaftar demo :group',
    'thank-you-nine-stars'          => '<p>Thank you for registering. Here is your account detail : </p> <p><b>Username</b> : :username</br> <b>Password</b> : :password</p>',

    
    'description-branch-events'     => 'Monex分行活动日程表。请直接到各分行登记注册。',          
    'corporate-events'              => '<span>Monex</span> 企业活动',

    'main-banner-id'                => 'Main Banner Id',
    'main-banner-en'               => 'Main Banner Eng',

    // account 
    'title-success-page-account'    =>'Terima kasih telah membuka :type <span>Account</span> dengan kami.',
	'title-form-account-demo'		=> '建立模拟账户',
	'title-form-account-live'		=> '创建真实账户',

    // add 
    'read-more'                     => '阅读更多 ',
    'sign-up-for-account-today'     => '今天就登录注册帐户吧！',
    'partner-registration'          => '合作伙伴注册',
    'company-personal-name'         => '公司/个人名称',
    'handphone'                     => '手机',
    'forex'                         => '外汇',
    'commodities'                   => '商品',
    'cfds'                          => '差价合约',
    'index'                         => '指数',
    'symbol'                        => '符号',
    'bid'                           => '出價',
    'ask'                           => '要价',
    'high'                          => '高',
    'low'                           => '低',
    'eurusd'                        => '欧元兑美元',
    'gbpusd'                        => '英镑兑美元',
    'usdchf'                        => '美元兑瑞郎',
    'usdjpy'                        => '美元兑日元',
    'audusd'                        => '澳元兑美元',
    'hanya-indikator'               => '*仅限指示器',
    'google'                        => '谷歌',
    'goldman'                       => '高盛',
    'exxon'                         => '埃克森',
    'intel'                         => '英特尔',
    'sign-up-for-account-today-account' => '今天就登录注册<br /><span>帐户</span> 吧',
    'corporate-events-title-list'   => '企业活动',
    'select-a-date-to-attend'       => '选择一个日期来参加',
    'please-select'                 => '请选择',
    'select-one'                    => '请选择',
    'event'                         => '活动',
    'ends'                          => '结束',
    'quantity'                      => '数量',
    'branch-event-title'            => '分行活动',
    'location'                      => '地点',
    'empty'                         => '空白',
    'search'                        => '搜索',
    'other-e-book'                  => '其他电子书',
    'download'                      => '下载',
    'demo'                          => '演示',
    'full-version'                  => '查看完整版本',
    'create-demo-account'           => '创建一个模拟账户',
    'contact-form'                  => '联系表格',
    'Subject of Message'            => '邮件主题',
    'Pesan'                         => '讯息',
    'kode-wakil-pialang'            => '代码经纪',
    'request-demo-multilateral'     => '多边示范要求',
    'kantor-cabang'                 => '分公司',
    'monex-branch'                  => '<span style="color: #078d03;">MONEX</span> 分行',
    'tutorial-monex-mt4'            => 'Monex教程MT4交易',
    
     // add translate 21 mei 2014
    'thank-you-for-opening-live-account-with-us'    => '感谢您开立真实账户，',
    'thank-you-for-opening-demo-account-with-us'    => '感谢您与我们开设模拟账户。',
    
    'its-so-easy-to-open-an-account-with-monex'     => '通过monex 开立账户非常简易，只需按照以下步骤 :',
    'verify-registration'                           => '验证登记',
    'verify-registration-by-clicking-verify-link-in'=> '点击您电子邮件的验证链接以验证登记。同时也请检查您的垃圾邮件，并确定将我们 的电子邮件地址添加到您的安全名单',
    'a-open-this-email'                             => 'a. 打开此电邮（链接启用模拟帐户）',
    'b-please-click-this-link-for-activate'         => 'b. 请点击此链接以启用您的账户',
    'please-make-sure-that-you-have-entered-the'    => '*请确定您输入了正确的电子邮件地址（:email）和电话号码 （ :phone ） ，因为我们需要通过电子邮件也可能通过短信向您发送验证。如 果您输入了错误的信息， 请您纠正该错误，这很重要。',
    'submit-registration'                           => '验证登记',
    'username-and-password'                         => '用户名及密码',
    'login-account'                                 => '登录账户',
    // verifiy
    'check-email-form-usename-and-password'         => '检查电子邮件的用户名和密码',
    'congratulations-your-live-account-has-been'    => '恭喜您，您的真实账户已通过验证。您的账户详情已发送至 <a class="green_font" href="mailto::mail">:mail</a> 查您的电子邮件用户名和密码。同时也请检查您的垃圾邮件，并确定将我们的电子邮 件地址添加到您的安全名单',
    'download-monex-trader'                         => '下载Monex证券交易',
    'this-email-send-automatically-by-system'       => '由系统自动发送该邮件，并且你不需要回答。',
    'thank-you'                                     => '谢谢。',

    'download-about-us'                             => '下载',
    'view'                                          => '视图',
    'monex-company-profile'                         => '摩乃科斯公司简介',

    'btn-read-more'                                 => '<img src="'. URL::templateImage('material/btn-read-sf.png') .'" />',

    'each-month-we-will-give'       => '<h3 class="norborder">每个月我们都会赠送3苹果<br/><span>ipad</span>迷你成功申请人在抽签打开验证，并资助<span>Live</span>帐户。<br/>现在注<span>Live</span>账户！</h3>
                                        <br/><br/>添加了牌照 Kementerian Sosial RI  : 1951/LJS.PPSDBS.PI.01.02/9/2014',
    'to-qualify'                    => '<p>** 抽奖资格:</p><ul>
                                          <li> 与会者认为有效的进入抽奖是谁已经成功打开了一个真实账户并存入资金与核实数据的参与者。对于每一个抽奖周期有一个特定的登记时间。谁登记，并已申报成功的上述标准的参与者，将列入抽奖。</li>
                                          <li> 参赛者必须提供完整，有效的数据。仅限于一个条目。没有重复/多个条目。</li>
                                          <li> 作品多次将从抽奖系统被直接淘汰。</li>
                                          <li> 平局并不适用于PT.Monex Investindo期货，顾问和亲属的管理层和员工。</li>
                                          <li> 获奖者的名字将被公布在通过短信，电子邮件和正式通知信公司网站www.mifx.com，信息为每位获奖者的地址。</li>
                                          <li> 获胜者和价格仅对一定的时间周期。</li>
                                          <li> 税奖金将被记入获奖者。</li>
                                          <li> 其他信息将被贴在了中标通知书信的细节。</li>
                                        </ul>
                                        <p class="note">*这个词的iPad mini和标志是苹果电脑公司的财产。</p>',
);