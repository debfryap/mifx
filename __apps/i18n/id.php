<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'Create'                          =>'Buat',
    'Live Account'                =>'Live Akun',
	'Demo Account'                =>'Demo Akun',
    'telp'                          => 'Telp',
    'fax'                           => 'Fax',
    'widget-icon'                   => 'Widget Icon',
    'shortcut-icon'                 => 'Shortcut Icon',
    'home-banner'                   => 'Banner',
    'banner-title1'                 => 'Title 1',
    'banner-title2'                 => 'Title 2',
    'banner-title'                  => 'Title',
    'page-left-bnnr'                => 'Left Banner',
    'meta-title'                    => 'Meta Title',
    'meta-description'              => 'Meta Description',
    'pilih-cabang'                  => 'Pilih Cabang',
    
    # online trading
    'banner-trading'                => 'Banner Trading',
    'widget-trading'                => 'Widget Trading',
    'banner-310x172'                => 'Banner 310 x 172',
    'banner-410x375'                => 'Banner 410 x 375', 
    'banner-page'                   => 'Banner Page', 
    'tool-video'                    => 'Tools Image',
    'banner-account'                => 'Banner Account',
    'widget-account'                => 'Widget Account',
    'create-free-demo-account'      => 'Buat Demo Akun',
    'create-monex-live-account'     => 'Buat Live Akun',
    'select-city'                   => 'Pilih Kota',
    'full-name'                     => 'Nama Lengkap',
    'phone'                         => 'Telepon',
    'email'                         => 'Email',
    'deposit-awal'                  => 'Deposit Awal',
    'kode-verifikasi'               => 'Kode Verifikasi',
    # bad words edited by ahyo@mifx.com
    'privacy-agreement'             => 'Anda telah membaca dan setuju <span>:privacy</span>',
    'opening-deposit'               => 'Deposit Awal',
    'referral'                      => 'Kode Wakil Pialang',
    'submit'                        => 'Kirim',
    'risk-free'                     => 'Risk Free !!!',

    'contact-us'                    => 'Contact Us',
    'logo-partner'                  => 'Logo Partner',
    'banner-link'                   => 'Banner Link',
    'updates-banner'                => 'Updates Banner',
    'share'                         => 'Bagikan',
    'no-data-found-title'           => 'Tidak dapat menemukan halaman yang diinginkan',
    'no-data-found-description'     => 'Halaman URL yang anda request tidak dapat ditampilkan. Mungkin halaman ini sudah dipindahkan ke halaman URL yang berbeda, atau tidak pernah ditampilkan. Silahkan menghubungi administrator website MIFX',
    'no-data-found'                 => 'Tidak ada data yang dapat ditampilan untuk :group',
    'list-item'                     => 'Daftar',
    'jadwal-monex-edukasi'          => 'Jadwal <:tag>Monex Edukasi</:tag>',
    'ticket-information'            => 'Ticket Information',
    'back-to-list'                  => 'Kembali ke daftar :group',
    'judul-icon'                    => 'Judul Widget',
    'highlight'                     => 'Highlight',
    
    'others-article'                => 'Artikel Lainnya',    
    'no-berita-found'               => 'Belum ada berita yang dapat ditampilkan',
    'back-to-article-list'          => 'Kembali ke daftar berita',
    
    'no-glossary-found'             => 'Belum ada kosakata yang dapat ditampilkan',
    'back-to-glossary-list'         => 'Kembali ke daftar kosakata',
    
    'previous-tutorial'             => 'Tutorial sebelumnya',
    'next-tutorial'                 => 'Tutorial selanjutnya',
    'do-not-close-browser'          => 'Silahkan tunggu beberapa saat dan jangan tutup browser anda.',
    'error-captcha'                 => 'Captcha masih salah. Silahkan masukkan sekali lagi',
    'captcha-not-valid'             => 'Captcha did not match. Please correct',
    'verified-normal-error-title'   => 'Error',
    'verified-normal-error-desc'    => 'Ups ... your account could not be verified for this time. Reason :reason',
    'verified-fatal-error-title'    => 'Fatal Error',
    'verified-fatal-error-desc'     => 'Your account could not be verified for this time. Reason :reason. Please contact your administrator',
    'verified-success-title'        => 'Congratulations',
    'verified-success-desc'         => 'Congratulations. Your :type account has been verified. Your account detail has sent to your email at :email',

    // ebook form
    'manage-ebook'                  => 'Manage E-Book',
    'ebook'                         => 'E-Book',
    'ebook-title'                   => 'Title',
    'permalink'                     => 'Permalink',
    'ebook-description'             => 'Description',
    'url-demo'                      => 'URL Demo', 
    'url-full'                      => 'URL Full',
    'create-ebook'                  => 'Create Ebook',
    // ebook widget
    'ebook-large'                   => 'Ebook Large',
    'ebook-small'                   => 'Ebook Small',
    'order_summary'                 => 'Order Summary',
    'register'                      => 'Register ',
    'registration-information'      => 'Informasi Pendaftaran',
    'registration-wording'          => 'Terima kasih telah mendaftar untuk acara ini. Tanggapan Anda penting bagi kami.',
    
    
    'your-information'              => 'Informasi Anda',     
    'gender'                        => 'Jenis Kelamin',
    'male'                          => 'Laki-Laki',
    'female'                        => 'Perempuan',
    'first-name'                    => 'Nama Depan',
    'last-name'                     => 'Nama Belakang',
    'email'                         => 'Email',
    'phone'                         => 'Telepon',
    'city'                          => 'Domisili',
    'address'                       => 'Alamat',
    
    
    'Back to list'                  => 'Kembali',
    'search-result'                 => 'Hasil Pencarian untuk :keyword',
    'seminar'                       => 'Seminar',
    'date'                          => 'Tanggal',
    'quantity-ticket'               => 'Jumlah Tiket',
    'seminar-subject'               => ':name telah mendaftar untuk mengikuti :seminar',
    'thank-you-for-registering-seminar' => '<p>Terima kasih atas konfirmasi data diri Anda. </p><p>Anda masih dalam daftar antrian registrasi dan belum terdaftar sebagai peserta. </p><p>Mohon kiranya dapat menunggu konfirmasi selanjutnya.</p><p>Berikut data anda : </p>',
    'acc-source-list'               => 'Source List',
    'thank-ebook-registering'       => 'Terimakasih. Kami telah mengirimkan detail alamat URL untuk e-book ini ke alamat email anda.',
    'thank-you-title'               => 'Terimakasih',
    'computer-generated-message'    => 'Surat ini dihasilkan oleh komputer dan tidak memerlukan konfirmasi kembali.', 
    'confirmation'                  => 'Konfirmasi',
    'alert'                         => 'Peringatan',
    'is-monex-client'               => 'Apakah Anda sudah menjadi Nasabah Monex?',
    'is-contact-by-monex'           => 'Apakah Anda sudah pernah dihubungi oleh Tim Marketing Monex?',
    'is-client-or-contacted'        => '<p>Silahkan mendaftar pada Kantor Cabang masing-masing. <br/>Pendaftaran via website hanya berlaku bagi yang belum menjadi Nasabah ataupun belum dihubungi oleh Tim Marketing Monex.</p>',
    'yes-button'                    => 'Ya',
    'no-button'                     => 'Belum',
    'date-must-selected'            => 'Tanggal tidak boleh kosong',
    'quantity-must-not-empty'       => 'Quantity tidak boleh kosong',
    'mcs-subject-mail'              => ':no-urut # Registrasi Seminar',
    'mcs-body-mail'                 => ':nama telah melakukan pendaftaran seminar, dengan data sebagai berikut',
    'seminar-subject-mail'          => ':no-urut # Registrasi Seminar',
    'footer-wording'                => 'PT. Monex Investindo Futures beroperasi berdasarkan izin dan berada di bawah pengawasan BAPPEBTI, merupakan anggota bursa BBJ dan BKDI dan anggota kliring berjangka KBI & ISI Clearing.',
    'forbidden-domain-email'        => 'Email dengan domain <b>:domain</b> tidak diperbolehkan',
    'server-not-responding'         => 'Tidak ada respon dari server. Silahkan hubungi administrator anda atau coba sekali lagi',
    
    'image-slider-id'               => 'Slider Id',
    'image-slider-en'               => 'Slider En',
    'image-slider-cn'               => 'Slider Cn',
    'image-slider-tw'               => 'Slider Tw',
    
    
    'error-nine-stars'              => 'Terjadi kesalahan pada saat mendaftar demo :group',
    'thank-you-nine-stars'          => '<p>Thank you for registering. Here is your account detail : </p> <p><b>Username</b> : :username</br> <b>Password</b> : :password</p>',

    
    'description-branch-events'     => 'Jadwal event kantor cabang Monex. Silahkan langsung registrasi ke kantor cabang masing-masing.',          
    'corporate-events'              => '<span>Monex</span> Corporate Events',

    'main-banner-id'                => 'Banner Id',
    'main-banner-en'               => 'Banner Eng',
    'main-banner-cn'                => 'Banner Chinese',
    'main-banner-tw'                => 'Banner Traditional',

    'banner-title-cn'               => 'Chinese',
    'banner-title-tw'               => 'Traditional Chinese',

    // account 
    'title-success-page-account'    =>'Terima kasih telah membuka :type <span>Account</span> dengan kami.',
	'title-form-account-demo'      => 'Buat Demo Akun',
    'title-form-account-live'       => 'Buat Live Akun',

    'edit-language'                 => 'Edit Language', 

    // add 
    'read-more'                     => 'Baca Selengkapnya ',
    'sign-up-for-account-today'     => 'Sign Up for Account today !',
    'partner-registration'          => 'Pendaftaran Mitra',
    'company-personal-name'         => 'Perusahaan / Nama Pribadi',
    'handphone'                     => 'Handphone',
    'forex'                         => 'Forex',
    'commodities'                   => 'Commodities',
    'cfds'                          => 'CFDs',
    'index'                         => 'Index',
    'symbol'                        => 'Symbol',
    'bid'                           => 'Bid',
    'ask'                           => 'Ask',
    'high'                          => 'High',
    'low'                           => 'Low',
    'eurusd'                        => 'EURUSD',
    'gbpusd'                        => 'GBPUSD',
    'usdchf'                        => 'USDCHF',
    'usdjpy'                        => 'USDJPY',
    'audusd'                        => 'AUDUSD',
    'hanya-indikator'               => '*Hanya Indikator',
    'google'                        => 'GOOGLE',
    'goldman'                       => 'GOLDMAN',
    'exxon'                         => 'EXXON',
    'intel'                         => 'INTEL',
    'sign-up-for-account-today-account' => ' Sign Up <br /> for <span>Account</span> today ! ',
    'corporate-events-title-list'   => 'Corporate Events',
    'select-a-date-to-attend'       => 'Select a Date to Attend ',
    'please-select'                 => 'Please Select',
    'select-one'                    => 'Please Select',
    'event'                         => 'Event',
    'ends'                          => 'Ends',
    'quantity'                      => 'Quantity',
    'branch-event-title'            => 'Branch Events',
    'location'                      => 'Location',
    'empty'                         => 'empty',
    'search'                        => 'search',
    'other-e-book'                  => 'Other e-book',
    'download'                      => 'DOWNLOAD',
    'demo'                          => 'Demo',
    'full-version'                  => 'Full Version',
    'create-demo-account'           => 'Buat Demo Akun',
    'banner-title1-cn'              => 'Title Chinese',
    'banner-title1-tw'              => 'Title Traditional',
    'banner-title2-cn'              => 'Title 2 Chinese',
    'banner-title2-tw'              => 'Title 2 Traditional',
    'contact-form'                  => 'Contact Form',
    'Subject of Message'            => 'Subject of Message',
    'Pesan'                         => 'Pesan',
    'kode-wakil-pialang'            => 'Kode Wakil Pialang',
    'request-demo-multilateral'     => 'Request Demo Multilateral',
    'kantor-cabang'                 => 'Kantor Cabang',
    'monex-branch'                  => '<span style="color: #078d03;">MONEX</span> Branch',
    'tutorial-monex-mt4'            => 'Tutorial Monex Trader MT4',
    
     // add translate 21 mei 2014
    'thank-you-for-opening-live-account-with-us'    => 'Terimakasih telah membuka live account ini.',
    'thank-you-for-opening-demo-account-with-us'    => 'Terimakasih telah membuka demo account ini.',

    'its-so-easy-to-open-an-account-with-monex'     => 'Akun sangat mudah dibuka menggunakan Monex, silahkan mengikuti langkah-langkah di bawah ini:',
    'verify-registration'                           => 'Verifikasi Pendaftaran',
    'verify-registration-by-clicking-verify-link-in'=> 'Verifikasi Pendaftaran dengan mengklik tautan yang ada di email anda. Cek juga di "junk mail" anda, dan pastikan untuk menambah email kami dalam daftar aman.',
    'a-open-this-email'                             => 'a. Buka email ini (tautan pengaktifan akun demo)',
    'b-please-click-this-link-for-activate'         => 'b. Silahkan klik tautan ini untuk mengaktifkan akun anda',
    'please-make-sure-that-you-have-entered-the'    => '*Pastikan bahwa kamu telah memasuki alamat email (:email) dan nomor telepon (:phone) yang benar karena kami perlu megirimkan verifikasi melalui email atau melalui SMS. Jika anda telah mengisi informasi yang salah, sangat penting untuk memperbaiki kesalahan tersebut.',
    'submit-registration'                           => 'Mengajukan Pendaftaran',
    'username-and-password'                         => 'Nama Pengguna dan Kata Sandi',
    'login-account'                                 => 'Akun <br/>login',
    // verifiy
    'check-email-form-usename-and-password'         => 'Terimakasih telah membuka account live ini.',
    'congratulations-your-live-account-has-been'    => 'Selamat, live account anda telah divrifikasi. Detail akun anda telah dikirim ke <a class="green_font" href="mailto::mail">:mail</a>, silahkan cek email anda untuk nama pengguna dan kata sandi. Silahkan cek juga "junk mail" anda dan pastikan untuk menambah email kami dalam daftar aman.',
    'download-monex-trader'                         => 'Unduh Monex Trader',
    'this-email-send-automatically-by-system'       => 'This email send automatically by system, and you do not need to reply.',
    'thank-you'                                     => 'Terima Kasih',
    'Please Complete Your Data'                     => 'Silahkan untuk melengkapi data diri anda',

    'download-about-us'                             => 'Download',
    'view'                                          => 'View',
    'monex-company-profile'                         => 'Monex Company Profile',

    'btn-read-more'                                 => '<img src="'. URL::templateImage('material/btn-read-id.png') .'" />',

    'each-month-we-will-give'       => '<h3 class="norborder">Setiap bulan kami akan memberikan<br /> <span> 3 Apple ipad Mini</span> untuk peserta <br /> Undian Bulanan Bagi Pengunjung Website Monex yang telah berhasil<br /> membuka <span>live account</span> dan telah menyetorkan dananya  disertai data yang sudah diverifikasi. Daftar sekarang pada <span>Live Account!</span></h3>
                                        <br/><br/>Izin iklan dari Kementerian Sosial RI : 1951/LJS.PPSDBS.PI.01.02/9/2014',
    'to-qualify'                    => '<p>**Kualifikasi undian :</p><ul> 
                                        <li>Peserta yang dianggap sah untuk mengikuti undian adalah peserta yang telah berhasil membuka LIVE ACCOUNT dan telah menyetorkan dananya disertai data yang sudah diverifikasi. Untuk setiap periode undian memiliki jangka waktu registrasi  tertentu. Peserta yang telah melakukan registrasi dan telah dinyatakan sukses melakukan kriteria tsb  diatas, akan diikutsertakan dalam pengundian.</li>
                                        <li>Wajib memberikan informasi yang lengkap dan benar.Terbatas hanya satu kali entri. Tidak ada duplikasi / beberapa kali entri.</li>
                                        <li>Entri lebih dari satu kali akan langsung dieliminasi dari sistim undian.</li>
                                        <li>Undian ini tidak berlaku untuk manajemen & karyawan PT. Monex Investindo Futures, konsultan maupun keluarganya.</li>
                                        <li>Nama pemenang akan dimuat pada website perusahaan www.mifx.com, info melalui SMS, email dan surat pemberitahuan resmi ke alamat masing-masing pemenang. </li>
                                        <li>Pemenang dan hadiah berlaku hanya untuk periode tertentu dan waktu tertentu</li>
                                        <li>Pajak Hadiah ditanggung oleh Pemenang.</li>
                                        <li>Keterangan lainnya akan dicantumkan secara lengkap pada surat pemberitahuan pemenang.</li>
                                        </ul>
                                        
                                        <p class="note">*Syarat Ipad mini dan logo adalah milik Apple Computers.</p>',
);  