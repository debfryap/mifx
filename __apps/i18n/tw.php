<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'telp'                          => 'Telp',
    'fax'                           => 'Fax',
    'widget-icon'                   => 'Widget Icon',
    'shortcut-icon'                 => 'Shortcut Icon',
    'home-banner'                   => 'Banner',
    'banner-title1'                 => 'Title 1',
    'banner-title2'                 => 'Title 2',
    'banner-title'                  => 'Title',
    'page-left-bnnr'                => 'Left Banner',
    'meta-title'                    => 'Meta Title',
    'meta-description'              => 'Meta Description',
    'pilih-cabang'                  => '分行選擇',
    
    # online trading
    'banner-trading'                => 'Banner Trading',
    'widget-trading'                => 'Widget Trading',
    'banner-310x172'                => 'Banner 310 x 172',
    'banner-410x375'                => 'Banner 410 x 375', 
    'banner-page'                   => 'Banner Page', 
    'tool-video'                    => 'Tools Image',
    'banner-account'                => 'Banner Account',
    'widget-account'                => 'Widget Account',
    'create-free-demo-account'      => '建立模擬賬戶',
    'create-monex-live-account'     => '創建真實賬戶',
    'select-city'                   => '選取城市',
    'full-name'                     => '全名',
    'phone'                         => '電話',
    'email'                         => '電郵',
    'Email'                         => '電郵',
    'deposit-awal'                  => '初始保證金',
    'kode-verifikasi'               => 'Kode Verifikasi',
    'privacy-agreement'             => '您同意並已閱讀 <span>:privacy</span>',
    'opening-deposit'               => 'Deposit Awal',
    'referral'                      => '經紀人代碼',
    'submit'                        => '提交',
    'risk-free'                     => '無風險！！！',

    'contact-us'                    => 'Contact Us',
    'logo-partner'                  => 'Logo Partner',
    'banner-link'                   => 'Banner Link',
    'updates-banner'                => 'Updates Banner',
    'share'                         => '分享',
    'no-data-found-title'           => 'Tidak dapat menemukan halaman yang diinginkan',
    'no-data-found-description'     => 'Halaman URL yang anda request tidak dapat ditampilkan. Mungkin halaman ini sudah dipindahkan ke halaman URL yang berbeda, atau tidak pernah ditampilkan. Silahkan menghubungi administrator website MIFX',
    'no-data-found'                 => 'Tidak ada data yang dapat ditampilan untuk :group',
    'list-item'                     => '列表',
    'jadwal-monex-edukasi'          => 'Monex教育 <:tag>時間表</:tag>',
    'ticket-information'            => 'Ticket Information',
    'back-to-list'                  => 'Kembali ke daftar :group',
    'judul-icon'                    => 'Judul Widget',
    'highlight'                     => 'Highlight',
    
    'others-article'                => 'Artikel Lainnya',    
    'no-berita-found'               => 'Belum ada berita yang dapat ditampilkan',
    'back-to-article-list'          => 'Kembali ke daftar berita',
    
    'no-glossary-found'             => 'Belum ada kosakata yang dapat ditampilkan',
    'back-to-glossary-list'         => 'Kembali ke daftar kosakata',
    
    'previous-tutorial'             => '以前的教程',
    'next-tutorial'                 => '接下來的教程',
    'do-not-close-browser'          => 'Silahkan tunggu beberapa saat dan jangan tutup browser anda.',
    'error-captcha'                 => '驗證碼仍然是錯誤的。請重新輸入',
    'verified-normal-error-title'   => 'Error',
    'verified-normal-error-desc'    => 'Ups ... your account could not be verified for this time. Reason :reason',
    'verified-fatal-error-title'    => 'Fatal Error',
    'verified-fatal-error-desc'     => 'Your account could not be verified for this time. Reason :reason. Please contact your administrator',
    'verified-success-title'        => 'Congratulations',
    'verified-success-desc'         => 'Congratulations. Your :type account has been verified. Your account detail has sent to your email at :email',

    // ebook form
    'manage-ebook'                  => 'Manage E-Book',
    'ebook'                         => 'E-Book',
    'ebook-title'                   => 'Title',
    'permalink'                     => 'Permalink',
    'ebook-description'             => 'Description',
    'url-demo'                      => 'URL Demo', 
    'url-full'                      => 'URL Full',
    'create-ebook'                  => 'Create Ebook',
    // ebook widget
    'ebook-large'                   => 'Ebook Large',
    'ebook-small'                   => 'Ebook Small',
    'order_summary'                 => 'Order Summary',
    'register'                      => 'Register ',
    'registration-information'      => 'Informasi Pendaftaran',
    'registration-wording'          => 'Terima kasih telah mendaftar untuk acara ini. Tanggapan Anda penting bagi kami.',
    
    
    'your-information'              => 'Informasi Anda',     
    'gender'                        => 'Jenis Kelamin',
    'male'                          => 'Laki-Laki',
    'female'                        => 'Perempuan',
    'first-name'                    => 'Nama Depan',
    'last-name'                     => 'Nama Belakang',
    //'email'                         => 'Email',
    //'phone'                         => 'Telepon',
    'city'                          => '城市',
    'address'                       => '地址',
    
    
    'Back to list'                  => 'Kembali',
    'search-result'                 => 'Hasil Pencarian untuk :keyword',
    'seminar'                       => 'Seminar',
    'date'                          => '日期',
    'quantity-ticket'               => 'Jumlah Tiket',
    'seminar-subject'               => ':name telah mendaftar untuk mengikuti :seminar',
    'thank-you-for-registering-seminar' => '<p>Terima kasih atas konfirmasi data diri Anda. </p><p>Anda masih dalam daftar antrian registrasi dan belum terdaftar sebagai peserta. </p><p>Mohon kiranya dapat menunggu konfirmasi selanjutnya.</p><p>Berikut data anda : </p>',
    'acc-source-list'               => 'Source List',
    'thank-ebook-registering'       => 'Terimakasih. Kami telah mengirimkan detail alamat URL untuk e-book ini ke alamat email anda.',
    'thank-you-title'               => 'Terimakasih',
    'computer-generated-message'    => 'Surat ini dihasilkan oleh komputer dan tidak memerlukan konfirmasi kembali.', 
    'confirmation'                  => 'Konfirmasi',
    'alert'                         => 'Peringatan',
    'is-monex-client'               => 'Apakah Anda sudah menjadi Nasabah Monex?',
    'is-contact-by-monex'           => 'Apakah Anda sudah pernah dihubungi oleh Tim Marketing Monex?',
    'is-client-or-contacted'        => '<p>Silahkan mendaftar pada Kantor Cabang masing-masing. <br/>Pendaftaran via website hanya berlaku bagi yang belum menjadi Nasabah ataupun belum dihubungi oleh Tim Marketing Monex.</p>',
    'yes-button'                    => 'Ya',
    'no-button'                     => 'Belum',
    'date-must-selected'            => 'Tanggal tidak boleh kosong',
    'quantity-must-not-empty'       => 'Quantity tidak boleh kosong',
    'mcs-subject-mail'              => ':no-urut # Registrasi Seminar',
    'mcs-body-mail'                 => ':nama telah melakukan pendaftaran seminar, dengan data sebagai berikut',
    'seminar-subject-mail'          => ':no-urut # Registrasi Seminar',
    'footer-wording'                => 'PT. Monex Investindo Futures是在BAPPEBTI的監管之下按許可證經營，爲雅加達期貨交易所（BBJ） 及印尼商品及衍生品交易所（BKDI）會員與印尼期貨結算所（KBI）及 ISI結算所會員。',
    'forbidden-domain-email'        => 'Email dengan domain <b>:domain</b> tidak diperbolehkan',
    'server-not-responding'         => 'Tidak ada respon dari server. Silahkan hubungi administrator anda atau coba sekali lagi',
    
    'image-slider-id'               => 'Slider Id',
    'image-slider-en'               => 'Slider En',
    
    
    'error-nine-stars'              => 'Terjadi kesalahan pada saat mendaftar demo :group',
    'thank-you-nine-stars'          => '<p>Thank you for registering. Here is your account detail : </p> <p><b>Username</b> : :username</br> <b>Password</b> : :password</p>',

    
    'description-branch-events'     => 'Monex分行活動日程表。請直接到各分行登記注冊。',          
    'corporate-events'              => '<span>Monex</span> 企業活動',

    'main-banner-id'                => 'Main Banner Id',
    'main-banner-en'               => 'Main Banner Eng',

    // account 
    'title-success-page-account'    =>'Terima kasih telah membuka :type <span>Account</span> dengan kami.',
	'title-form-account-demo'		=> '建立模擬賬戶',
	'title-form-account-live'		=> '創建真實賬戶',

    // add 
    'read-more'                     => '閱讀更多 ',
    'sign-up-for-account-today'     => '今天就登錄注冊帳戶吧！',
    'partner-registration'          => '合作夥伴注冊',
    'company-personal-name'         => '公司/個人名稱',
    'handphone'                     => '手機',
    'forex'                         => '外彙',
    'commodities'                   => '商品',
    'cfds'                          => '差價合約',
    'index'                         => '指數',
    'symbol'                        => '符號',
    'bid'                           => '出價',
    'ask'                           => '要價',
    'high'                          => '高',
    'low'                           => '低',
    'eurusd'                        => '歐元兌美元',
    'gbpusd'                        => '英鎊兌美元',
    'usdchf'                        => '美元兌瑞郎',
    'usdjpy'                        => '美元兌日元',
    'audusd'                        => '澳元兌美元',
    'hanya-indikator'               => '*僅限指示器',
    'google'                        => '谷歌',
    'goldman'                       => '高盛',
    'exxon'                         => '埃克森',
    'intel'                         => '英特爾',
    'sign-up-for-account-today-account' => ' 今天就登錄注冊<br /> <span>帳戶</span> 吧！',
    'corporate-events-title-list'   => '企業活動',
    'select-a-date-to-attend'       => '選擇一個日期來參加',
    'please-select'                 => '請選擇',
    'select-one'                    => '請選擇',
    'event'                         => '活動',
    'ends'                          => '結束',
    'quantity'                      => '數量',
    'branch-event-title'            => '分行活動',
    'location'                      => '地点',
    'empty'                         => '空白',
    'search'                        => '搜索',
    'other-e-book'                  => '其他電子書',
    'download'                      => '下載',
    'demo'                          => '演示',
    'full-version'                  => '查看完整版本',
    'create-demo-account'           => '創建一個模擬賬戶',
    'contact-form'                  => '聯系表格',
    'Subject of Message'            => '郵件主題',
    'Pesan'                         => '讯息',
    'kode-wakil-pialang'            => '代碼經紀',
    'request-demo-multilateral'     => '多邊示範要求',
    'kantor-cabang'                 => '分公司',
    'monex-branch'                  => '<span style="color: #078d03;">MONEX</span> 分行',
    'tutorial-monex-mt4'            => 'Monex教程MT4交易',
    
    // add translate 21 mei 2014
    'thank-you-for-opening-live-account-with-us'    => '感谢您开立真实账户，',
    'thank-you-for-opening-demo-account-with-us'    => '感謝您與我們開設模擬賬戶。',
    'its-so-easy-to-open-an-account-with-monex'     => '通过monex开立账户非常简易，只需按照以下步骤：',
    'verify-registration'                           => '验证登记',
    'verify-registration-by-clicking-verify-link-in'=> '点击您电子邮件的验证链接以验证登记。同时也请检查您的垃圾邮件，并确定将我们 的电子邮件地址添加到您的安全名单',
    'a-open-this-email'                             => 'a. 打开此电邮（链接启用模拟帐户）',
    'b-please-click-this-link-for-activate'         => 'b. 请点击此链接以启用您的账户',
    'please-make-sure-that-you-have-entered-the'    =>'*请确定您输入了正确的电子邮件地址（:email）和电话号码 ( :phone ) ，因为我们需要通过电子邮件也可能通过短信向您发送验证。如 果您输入了错误的信息， 请您纠正该错误，这很重要。',
    'submit-registration'                           => '提交登记',
    'username-and-password'                         => '用户名及密码',
    'login-account'                                 => '登录账户',
    // verifiy
    'check-email-form-usename-and-password'         => '检查电子邮件的用户名和密码',
    'congratulations-your-live-account-has-been'    => '恭喜您，您的真实账户已通过验证。您的账户详情已发送至 <a class="green_font" href="mailto::mail">:mail</a>, #65292;请检 查您的电子邮件用户名和密码。同时也请检查您的垃圾邮件，并确定将我们的电子邮 件地址添加到您的安全名单',
    'download-monex-trader'                         => '下載Monex證券交易',
    'this-email-send-automatically-by-system'       => '由系統自動發送該郵件，並且你不需要回答。',
    'thank-you'                                     => '謝謝。',

    'download-about-us'                             => '下載',
    'view'                                          => '視圖',
    'monex-company-profile'                         => '摩乃科斯公司簡介',

    'btn-read-more'                                 => '<img src="'. URL::templateImage('material/btn-read-trad.png') .'" />',

    'each-month-we-will-give'       => '<h3 class="norborder">每個月我們都會贈送3蘋果<br/><span>ipad</span>迷你成功申請人在抽籤打開驗證，並資助<span>Live</span>帳戶。<br/>現在註<span>Live</span>賬戶！</h3>
                                       <br/><br/>添加了牌照 Kementerian Sosial RI  : 1951/LJS.PPSDBS.PI.01.02/9/2014',
    'to-qualify'                    => '<p>**抽獎資格:</p><ul>
                                          <li> 與會者認為有效的進入抽獎是誰已經成功打開了一個真實賬戶並存入資金與核實數據的參與者。對於每一個抽獎週期有一個特定的登記時間。誰登記，並已申報成功的上述標準的參與者，將列入抽獎。</li>
                                          <li> 參賽者必須提供完整，有效的數據。僅限於一個條目。沒有重複/多個條目。</li>
                                          <li> 作品多次將從抽獎系統被直接淘汰。</li>
                                          <li> 平局並不適用於PT.Monex Investindo期貨，顧問和親屬的管理層和員工。</li>
                                          <li> 獲獎者的名字將被公佈在通過短信，電子郵件和正式通知信公司網站www.mifx.com，信息為每位獲獎者的地址。</li>
                                          <li> 獲勝者和價格僅對一定的時間週期。</li>
                                          <li> 稅獎金將被記入獲獎者。</li>
                                          <li> 其他信息將被貼在了中標通知書信的細節。 </li>
                                        </ul>
                                        <p class="note">*這個詞的iPad mini和標誌是蘋果電腦公司的財產。</p>',
);