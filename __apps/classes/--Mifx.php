<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Helper Class
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Mifx {
    
    protected static $instance;    
    
    protected $page;
    
    protected $contentCrumb = array();        
    
    protected $_soap;
    
    public static function instance() {
        if (!isset(self::$instance)) self::$instance = new Mifx;
        
        return self::$instance;     
    }
    
    protected function get_page($limit,$total) {
        $this->page = Pagination::factory(array(
                        'total_items'       => $total,
                		'items_per_page'    => $limit,
                		'view'              => 'pagination/mifx',
                        'count_out'         => 3,
                        'count_in'          => 1
                      ));
    }
    
    protected function get_total($table,$is_active = true) {
        $db =  DB::select(DB::expr("COUNT(`$table`.`id`) AS `total`"))->from($table);
        
        if ($is_active === true) $db->where("$table.is_active","=",1);
        
        return $db->execute()->get('total');   
    }
    
    protected function get_list(array $configs = array()) {
        $table = $is_active = $fields = $order = null;
        $limit = 2;
        
        extract($configs);
        
        if (!empty($limit)) {
            //Item total
            $total = $this->get_total($table,$is_active);
            
            //Set paging
            $this->get_page($limit,$total);    
        } 
        
        if (!empty($fields))
            $dbSelect = DB::select_array($fields);
        else
            $dbSelect = DB::select();
        
        $dbSelect->from($table);
        
        if (!empty($order)) {
            if (is_array($order)) {
                foreach ($order as $k => $d) {
                    is_numeric($k) ? $dbSelect->order_by($d) : $dbSelect->order_by($k,$d);
                }
            } else {
                $dbSelect->order_by($order);
            }
        }
        
        if (isset($is_active)) $dbSelect->where("is_active","=",$is_active === true ? 1 : $is_active);
        
        if (!empty($limit)) {
            $dbSelect->limit($this->page->items_per_page)->offset($this->page->offset);
        }
        
        return $dbSelect->as_object()->execute();
    }
    
    public function list_schedules($perpage = 10) {
        $list = $this->get_list(array(
                    'table'     => 'monex_schedules',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date','closing_info','region','description'),
                    'order'     => array('date'=>'asc'),
                    'limit'     => $perpage
                 ));
                 
         $list_branch = $this->get_list(array(
                    'table'     => 'branch_events',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date','location','region','description'),
                    'order'     => array('date'=>'asc'),
                    'limit'     => $perpage
                 ));

        return View::factory('education/seminar-list')
                        ->set('table','monex_schedules') //->set('table',$table)
                        ->set('list',$list)
                        ->set('list_branch', $list_branch) 
                        ->set('page',$this->page->render());
    }
    
    public function list_article($perpage = 10) {
        $list = $this->get_list(array(
                    'table'     => 'news',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date'),
                    'order'     => array('date'=>'desc'),
                    'limit'     => $perpage
                 ));
                 
        return View::factory('education/artikel-list')
                    ->set('title',Page::get('simpleBreadCrumb:artikel'))
                    ->set('list',$list)
                    ->set('page',$this->page);   
    }
    
    public function others_article($limit = 3) {
        $tail_url = App::tail_active_uri();
        
        if (empty($tail_url->small_uri)) return '';
        
        $others   = DB::select('label','permalink')->from('news')
                            ->where('is_active','=',1)
                            ->and_where('permalink','!=',$tail_url->small_uri)
                            ->limit(3)
                            ->as_object()
                            ->execute();
        
        if (!$others->valid()) return '';
                            
        
        $html  = '<div class="other_art"><p>'.__('others-article').'</p><ul>'; 
        foreach ($others as $item) {
            $html .= '<li>'.HTML::anchor(Page::get('completedNavigationUri:artikel') . '/' . $item->permalink, $item->label).'</li>';    
        }
        $html .= '</ul></div>';
        
        return $html;
    }
    
    public function list_glossary($perpage = 60) {
        
        $item   = Request::$initial->param('item');
        
        $filter = Request::$initial->param('filter');
        
        
        $dbList = DB::select()->from(array('glossaries','g'))
                                                 ->where('is_active','=',1);
        
        if ( !empty($item) && !empty($filter) ) {
            $lowerItem = strtolower($item);
            if ($lowerItem  == 'filter')
                $dbList->and_where(DB::expr('SUBSTR(`g`.`label`,1,1)'),'=',$filter);
            elseif ($lowerItem == 'search') 
                $dbList->and_where('g.label','like',"%$filter%");                                   
        }
        
        $dbTotal = clone $dbList;
        
        $dbTotal = $dbTotal->select(DB::expr('COUNT(`g`.`id`) AS `total`'))->execute()->get('total');
        
        //Set paging
        $this->get_page($perpage,$dbTotal);
        
        //List data
        $dbList->select('label','permalink', 'id')->order_by('label', 'ASC')->limit($perpage)->offset($this->page->offset);
        
        //$alpha = DB::select(DB::expr('SUBSTR(`g`.`label`,1,1) AS `alpha`'))->from(array('glossaries','g'))->where('g.is_active','=',1)->group_by('alpha')->as_object()->execute();
        $alpha = DB::select(DB::expr('`label` as alpha, id, label'))->from(array('glossaries','g'))->where('g.is_active','=',1)->as_object()->execute();
        
        $list  = $dbList->as_object()->execute();   
             
        return View::factory('education/glossary-list')
                    ->set('list',$list)
                    ->set('alpha',$alpha)
                    ->set('page',$this->page->render('pagination/green'))
                    ->set($item,$filter);   
    }
    
    public function list_updates($section,$table,$perpage = 10) {
        $list = $this->get_list(array(
                    'table'     => $table,
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date'),
                    'order'     => array('date'=>'desc'),
                    'limit'     => $perpage
                 ));

        $list_branch = $this->get_list(array(
                    'table'     => 'branch_events',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date','location'),
                    'order'     => array('date'=>'desc'),
                    'limit'     => $perpage
                 ));
                 
        return View::factory('about/kegiatan-list')
                    ->set('table',$table)        
                    ->set('section',$section)
                    ->set('list',$list)
                    ->set('list_branch', $list_branch)
                    ->set('page',$this->page);   
    }
    
    public function soap() {
        
        ini_set("soap.wsdl_cache_enabled", "0");
        
        if (empty(self::$instance->_soap)) {
            self::$instance->_soap = new SoapClient( 'http://admin.mifx.com/soap/mifx' );
        }
    }
    
    public static function accountForm($type, $referral = null, $regularOrPartial = null) {
        
        $frm   = $type == 'create-monex-live-account' ? 'live' : 'demo' ; 
        
        $actionForm = Translate::uri(Language::$current , 'ajax/submit-' . $frm . '-account');
                                        
        $html  = Form::open( $actionForm , array ( 'id' => 'frm-' . $frm . '-account', 'class' => 'is-validate create_account' ) );
        
        // Monex Affiliate Script
        $adv     = Request::$initial->query('adv'); 
        if(!empty($adv)) $html .= Form::hidden('adv', $adv );
        
        $html .= '<p class="ca_title ' . ( $type == 'create-monex-live-account' ? 'live' : 'demo' ) . '">' . __($type) .'</p>';
        $html .= '<fieldset>';
        
        $html .= '<p>' ;
        $html .= Mifx::elementForm('kota',$frm);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('name',$frm);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('phone',$frm);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('email',$frm);
        $html .= '</p>';
        
        if ($frm == 'demo') {
            $html .= '<p>';
            $html .= Mifx::elementForm('deposit',$frm);
            $html .= '</p>';
        }
        
        if (isset($regularOrPartial)) {
            $checkSessionRegularOrPartial = App::$session->get('regularOrPartial'); 
            
            if (empty($checkSessionRegularOrPartial)) {
                App::$session->set('regularOrPartial',$regularOrPartial);
            }
        }
        
        $regularOrPartial = App::$session->get('regularOrPartial');
        
        $html .= Form::hidden('partialOrRegular',empty($regularOrPartial) ? "0" : $regularOrPartial);
        
        $html .= '<p>';
        $html .= Mifx::elementForm('referral',$frm,$referral);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('source',$frm,$referral);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('info',$frm);
        $html .= '</p>';
        
        $html .= '<p class="verifikasi">';
        $html .= Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4));
        //$html .= Form::hidden('captcha',null,array('class'=>'verif required'));
        
        $html .= Mifx::captcha();
        /**$html .= Captcha::instance()
                    ->reconfig('width',113)
                    ->reconfig('height',31)
                    ->render();**/
        $html .= '</p>';
        
        $html .= '<p class="agree">';
        $html .= Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) );
        $html .= __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') )
                 ));
        $html .= '</p>';
        
        $html .= '<p class="submit">';
        
        if ($type == 'create-free-demo-account' ) $html .= '<span>' . __('risk-free') . '</span>';
        
        $html .= Form::submit('submit',__('submit'),array('class'=>'btn_submit'));
        $html .= '</p>';
        
        $html .= '</fieldset>';
        $html .= Form::close();
        
        return $html;
    }
    
    public static function elementForm($type, $group = 'demo', $referral = null) {
        
        //echo "<!-- this page : ".$this->page." -->";
        
        $type    = ucfirst(strtolower($type));
        
        $id      = "$group-$type";
        
        $post    = Request::$initial->post();
        
        $kota = self::$instance->_soap->getKota();  
        
        $elementKota  = '<select id="'.$id.'" name="accountKota" class="required">';
        $elementKota .= '<option value="">' . __("select-city") . '</option>';
        foreach ($kota as $idKota => $namaKota) {
            $elementKota .= '<option value="' . $idKota . '">' . $namaKota . '</option>';
        }
        $elementKota .= '</select>';
               
        $elementName = Form::input( 'accountName'
                    ,empty($post) || !isset($post['accountName']) ? __('full-name') : !isset($post['accountName']) 
                    ,array(
                            'defaultvalue' => __('full-name'),
                            'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('full-name').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('full-name').'\'' )
                 );
        
        $elementPhone = Form::input( 'accountPhone'
                    ,empty($post) || !isset($post['accountPhone']) ? __('phone') : $post['accountPhone']
                    ,array(
                            'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('phone').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('phone').'\'' )
                 );
        
        $elementEmail = Form::input( 'accountEmail'
                    ,empty($post) || !isset($post['accountEmail']) ? __('email') : $post['accountEmail']
                    ,array(
                            'class'   => 'required email',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('email').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('email').'\'' )
                 );
        
        $referralAttrs = array(
                            'title'   => 'Ketik X jika tidak mengetahui',
                            //'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('referral').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('referral').'\'' );
                            
        if (isset($referral)) {
            $referral = self::$instance->_soap->getMarketing($referral);
            App::$session->set('referral_account',$referral);
        } else {
            $referral = App::$session->get('referral_account',__('referral')); 
        }
        
        if ($referral == 'Invalid Code') $referral =  __('referral'); 
        if ($referral != __('referral')) $referralAttrs['readonly'] = 'readonly';
        
        
        $elementReferral = Form::input( 'accountReferral'
                    ,empty($post) || !isset($post['accountReferral']) ? $referral : $post['accountReferral']
                    ,$referralAttrs
                 );
        
                 
        $deposit = self::$instance->_soap->getDeposit();
        
        $elementDeposit  = '<select id="'.$id.'" name="accountDeposit" class="required">';
        $elementDeposit .= '<option value="">' . __("opening-deposit") . '</option>';
        foreach ($deposit as $idDeposit => $namaDeposit) {
            $elementDeposit .= '<option value="' . $idDeposit . '">' . $namaDeposit . '</option>';
        }
        $elementDeposit .= '</select>';

        $deposit2 = self::$instance->_soap->getDeposit();
        
        $elementDeposit2  = '<select id="'.$id.'" name="accountDeposit" class="required">';
        $elementDeposit2 .= '<option value="">' . __("opening-deposit") . '</option>';
        $elementDeposit2 .= '<option value="1000">1.000</option>';
        foreach ($deposit2 as $idDeposit2 => $namaDeposit2) {
            $elementDeposit2 .= '<option value="' . $idDeposit2 . '">' . $namaDeposit2 . '</option>';
        }
        $elementDeposit2 .= '</select>';
        
        //$elementDeposit = Form::select( 'accountDeposit'
//                    ,array_merge( array( "" => __('opening-deposit') ), self::$instance->_soap->getDeposit() )
//                    ,empty($post) || !isset($post['accountDeposit']) ? null : $post['accountDeposit']
//                    ,array(
//                            'class'   => 'required',
//                            'id'      => $id )
//                 );
                                          
        $elementInfo  = Form::select( 'accountInformation[]'
                    ,self::$instance->_soap->getInfo() 
                    ,empty($post) || !isset($post['accountInformation']) ? null : $post['accountInformation'] 
                    ,array( 
                            'multiple' => 'multiple',
                            'class'    => 'required',
                            'id'       => $id ) 
                ); 
        
        //$src = self::$instance->_soap->getSourceList();   
//        $src[""] = __("acc-source-list"); ksort($src);
//        
//        $elementSource = Form::select( 'accountSource'
//                    ,$src
//                    ,empty($post) || !isset($post['accountSource']) ? null : $post['accountSource']
//                    ,array(
//                            'class'   => 'required',
//                            'id'      => $id )
//                 );
        
        $src = self::$instance->_soap->getSourceList();
        
        #echo Debug::vars($src);   
        
        //Check for query Source
        $qSource = Request::$initial->query('Source');
        
        //Check for session Source
        if (empty($qSource)) {
            $qSource = Session::instance()->get('Source');                
        } 
        
        if (!empty($qSource)) {
            //Set to session
            Session::instance()->set('Source',$qSource);
        }
        
        foreach ($src as $ksrc => $vsrc) {
            if ($referral != __('referral')) {
                if (strtolower($vsrc) === 'ref.link') {
                    $elementSourceValue = $ksrc; 
                    break;
                }
            } else {
                if (!empty($qSource) && $vsrc == $qSource) {
                    $elementSourceValue = $ksrc;
                    break;
                }
                if (empty($qSource) && $vsrc == 'Direct')
                {
                    $elementSourceValue = $ksrc;
                    break;
                }
            }
        }
        
        if (!isset($elementSourceValue)) {
            foreach ($src as $ksrc => $vsrc) {
                $elementSourceValue = $ksrc;
                break;
            }                
        }
        
        $elementSource = Form::hidden('accountSource',$elementSourceValue);
        
        $element = "element$type";
        
        return isset($$element) ? $$element : null;   
    }
    
    public static function sitemap($nav , $level = 0) {
        $html  = '<ul';
        if ($level == 0) $html .= ' class="site_map" style="margin-top:20px;"';
        $html .= '>';
        
        foreach ($nav as $n) {
            $html .= '<li';
            $html .= '>';
            $html .= HTML::anchor( Page::get("completedNavigationUri:$n->permalink") , Page::get("simpleBreadCrumb:$n->permalink") );
            
            if (!empty($n->child))
                $html .= self::sitemap($n->child, $level + 1);
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
    
    public static function set($key,$value) {
        if ( ($x = strpos($key,':')) === false)
        {
            self::$instance->$key = $value;
        }
        else 
        {
            $argument = substr($key,$x+1); $key = substr($key,0,$x);    
            self::$instance->$key  = array_merge(self::$instance->$key,array($argument => $value));
        }
    }
    
    public static function get($key,$default = null) {
        if ( ($x = strpos($key,':')) === false && !isset(self::$instance->$key) ) return $default;
        
        if ( $x !== false ) { $argument = substr($key,$x+1); $key = substr($key,0,$x); } 
        
        $get = self::$instance->$key;
        
        if (is_array($get) && $x !== false) {
            return !empty($get[$argument]) ? $get[$argument] : $default;
        }
        
        return $get;
    }
    
    public static function select_ticket($length = 3, $selected = null,$attrs = array()) {
        $options = array("0"=>0);
        for ($i = 1; $i <= $length; $i++)
            $options[$i] = $i;
            
        return Form::select('quantity',$options,$selected,$attrs);
    }
    
    
    protected static $_captcha; 
    
    public static function captcha($width = 200, $height = 70) {
        
        if ( empty(self::$_captcha) ) {
            //include DOCROOT . 'coolcaptcha/captcha.php';
            
            self::$_captcha = HTML::image( URL::base(true) .'coolcaptcha/captcha.php?sesId='. App::$session->id()   , array ( 'width'=>$width,'height'=>$height ) );
            
        } 
            
        return self::$_captcha;
    }

    
    protected static $spAffiliate;
    
    public static function soapAffiliate() {
        if (!isset(self::$spAffiliate))
            self::$spAffiliate = $client = new SoapClient('http://affiliate.mifx.com/soap/mifx');
            
        return self::$spAffiliate;   
    } 
    
    public static function registerAffiliate($post) {
        //source,type,nama,alamat,kota,email,telpon,mobile,website
        $soap = self::soapAffiliate();
        
        return $soap->register(array(
                    'source' => $post->affiliate_source,
                    'type'   => $post->affiliate_type,
                    'nama'   => $post->company,
                    'alamat' => $post->address,
                    'kota'   => $post->city,
                    'email'  => $post->email,
                    'telpon' => $post->phone,
                    'mobile' => $post->handphone,
                    'website'=> ''
               ));
    }
}

?>