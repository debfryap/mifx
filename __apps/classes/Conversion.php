<?php defined('SYSPATH') or die('No direct script access.'); 

/**
 * @Author 		Doni
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Conversion Class
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Conversion {

    public static function record($title, $element){  

        // validate
        if(empty($title) or count($element) == 0)    
            return false;      
        // insert database
        DB::insert('conversions', array('title', 'value','ip','create_time'))
                    ->values(array($title, json_encode($element), self::_get_ip(), date('Y:m:d H:i:s') ))->execute();
    }

    public static function _get_ip(){

        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];

        return $ip;
    }

    public static function selectYear(){
 
        $data = DB::query(Database::SELECT, 'SELECT YEAR(create_time) AS year FROM conversions GROUP BY year ORDER BY year DESC')->as_object()->execute();
        
        $html = '<select class="form-field" name="year" id="select-year">';       

        $year = (object) Request::current()->post();

        foreach($data as $item){
            $html .= '<option value="'. $item->year .'" '. ((isset($year->year) and $year->year == $item->year) ? ' selected' : '') .' >'. $item->year .'</option>';
        } 

        $html .='</select>';

        
        return $html;
    }
}