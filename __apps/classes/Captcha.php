<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Helper Class
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Captcha {
    
    public static function valid($response = null) {
        $_save_captcha = App::$session->get('captcha');
        
        return !empty($_save_captcha) && $response == $_save_captcha;
    }
}

?>