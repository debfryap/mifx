<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Helper Class
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Mifx {
    
    protected static $instance;    
    
    protected $page;
    
    protected $contentCrumb = array();        
    
    protected $_soap;
    
    public static function instance() {
        if (!isset(self::$instance)) self::$instance = new Mifx;
        
        //Navigation Collector
        Page::$instance->navigationCollector();
        
        return self::$instance;     
    }
    
    protected function get_page($limit,$total) {
        $this->page = Pagination::factory(array(
                        'total_items'       => $total,
                		'items_per_page'    => $limit,
                		'view'              => 'pagination/mifx',
                        'count_out'         => 3,
                        'count_in'          => 1
                      ));
    }
    
    protected function get_total($table,$is_active = true) {
        $db =  DB::select(DB::expr("COUNT(`$table`.`id`) AS `total`"))->from($table);
        
        if ($is_active === true) $db->where("$table.is_active","=",1);
        
        return $db->execute()->get('total');   
    }
    
    protected function get_list(array $configs = array()) {
        $table = $is_active = $fields = $order = null;
        $limit = 2;
        
        extract($configs);
        
        if (!empty($limit)) {
            //Item total
            $total = $this->get_total($table,$is_active);
            
            //Set paging
            $this->get_page($limit,$total);    
        } 
        
        if (!empty($fields))
            $dbSelect = DB::select_array($fields);
        else
            $dbSelect = DB::select();
        
        $dbSelect->from($table);
        
        if (!empty($order)) {
            if (is_array($order)) {
                foreach ($order as $k => $d) {
                    is_numeric($k) ? $dbSelect->order_by($d) : $dbSelect->order_by($k,$d);
                }
            } else {
                $dbSelect->order_by($order);
            }
        }
        
        if (isset($is_active)) $dbSelect->where("is_active","=",$is_active === true ? 1 : $is_active);
        
        if (!empty($limit)) {
            $dbSelect->limit($this->page->items_per_page)->offset($this->page->offset);
        }
        
        return $dbSelect->as_object()->execute();
    }
    
    public function list_schedules($perpage = 10) {
        $list = $this->get_list(array(
                    'table'     => 'monex_schedules',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date','closing_info','region','description'),
                    'order'     => array('date'=>'asc'),
                    'limit'     => $perpage
                 ));
                 
         $list_branch = $this->get_list(array(
                    'table'     => 'branch_events',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date','location','region','description'),
                    'order'     => array('date'=>'asc'),
                    'limit'     => $perpage
                 ));

        return View::factory('education/seminar-list')
                        ->set('table','monex_schedules') //->set('table',$table)
                        ->set('list',$list)
                        ->set('list_branch', $list_branch) 
                        ->set('page',$this->page->render());
    }
    
    public function list_article($perpage = 10) {
        $list = $this->get_list(array(
                    'table'     => 'news',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date'),
                    'order'     => array('date'=>'desc'),
                    'limit'     => $perpage
                 ));
                 
        return View::factory('education/artikel-list')
                    ->set('title',Page::get('simpleBreadCrumb:artikel'))
                    ->set('list',$list)
                    ->set('page',$this->page);   
    }
    
    public function others_article($limit = 3) {
        $tail_url = App::tail_active_uri();
        
        if (empty($tail_url->small_uri)) return '';
        
        $others   = DB::select('label','permalink')->from('news')
                            ->where('is_active','=',1)
                            ->and_where('permalink','!=',$tail_url->small_uri)
                            ->limit(3)
                            ->as_object()
                            ->execute();
        
        if (!$others->valid()) return '';
                            
        
        $html  = '<div class="other_art"><p>'.__('others-article').'</p><ul>'; 
        foreach ($others as $item) {
            $html .= '<li>'.HTML::anchor(Page::get('completedNavigationUri:artikel') . '/' . $item->permalink, $item->label).'</li>';    
        }
        $html .= '</ul></div>';
        
        return $html;
    }
    
    public function list_glossary($perpage = 60) {
        
        $item   = Request::$initial->param('item');
        
        $filter = Request::$initial->param('filter');
        
        
        $dbList = DB::select()->from(array('glossaries','g'))
                                                 ->where('is_active','=',1);
        
        if ( !empty($item) && !empty($filter) ) {
            $lowerItem = strtolower($item);
            if ($lowerItem  == 'filter')
                $dbList->and_where(DB::expr('SUBSTR(`g`.`label`,1,1)'),'=',$filter);
            elseif ($lowerItem == 'search') 
                $dbList->and_where('g.label','like',"%$filter%");                                   
        }
        
        $dbTotal = clone $dbList;
        
        $dbTotal = $dbTotal->select(DB::expr('COUNT(`g`.`id`) AS `total`'))->execute()->get('total');
        
        //Set paging
        $this->get_page($perpage,$dbTotal);
        
        //List data
        $dbList->select('label','permalink', 'id')->order_by('label', 'ASC')->limit($perpage)->offset($this->page->offset);
        
        //$alpha = DB::select(DB::expr('SUBSTR(`g`.`label`,1,1) AS `alpha`'))->from(array('glossaries','g'))->where('g.is_active','=',1)->group_by('alpha')->as_object()->execute();
        $alpha = DB::select(DB::expr('`label` as alpha, id, label'))->from(array('glossaries','g'))->where('g.is_active','=',1)->as_object()->execute();
        
        $list  = $dbList->as_object()->execute();   
             
        return View::factory('education/glossary-list')
                    ->set('list',$list)
                    ->set('alpha',$alpha)
                    ->set('page',$this->page->render('pagination/green'))
                    ->set($item,$filter);   
    }
    
    public function list_updates($section,$table,$perpage = 10) {
        $list = $this->get_list(array(
                    'table'     => $table,
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date'),
                    'order'     => array('date'=>'desc'),
                    'limit'     => $perpage
                 ));

        $list_branch = $this->get_list(array(
                    'table'     => 'branch_events',
                    'is_active' => true,
                    'fields'    => array('id','label','permalink','date','location'),
                    'order'     => array('date'=>'desc'),
                    'limit'     => $perpage
                 ));
                 
        return View::factory('about/kegiatan-list')
                    ->set('table',$table)        
                    ->set('section',$section)
                    ->set('list',$list)
                    ->set('list_branch', $list_branch)
                    ->set('page',$this->page);   
    }
    
    public function soap() {
        
        ini_set("soap.wsdl_cache_enabled", "0");
        
        if (empty(self::$instance->_soap)) {
            
            try{
                //self::$instance->_soap = new SoapClient( 'https://crm.mifx.com/soap/mifx', array('exceptions' => TRUE) );
                self::$instance->_soap = new SoapClient( 'http://dummy.crm.mifx.com/soap/mifx', array('exceptions' => TRUE) );

            }catch(Exception $e){
                // Error code here
            }
        } 
        
        return self::$instance->_soap;
    }
    
    public static function accountForm($type, $referral = null, $regularOrPartial = null) {
        
        //In case SOAP Server is currently down
        if (empty(self::$instance->_soap)) return 'Service is down';
        
        $frm   = $type == 'create-monex-live-account' ? 'live' : 'demo' ; 
        
        $actionForm = Translate::uri(Language::$current , 'ajax/submit-' . $frm . '-account');
                                        
        $html  = Form::open( $actionForm , array ( 'id' => 'frm-' . $frm . '-account', 'class' => 'is-validate create_account' ) );
        
        $html .= '<div class="ipad-camp"><img src="http://mifx.com/ipad-camp.png" alt=""/></div>';
        
        // Monex Affiliate Script
        $adv     = Request::$initial->query('adv'); 
        if(!empty($adv)) $html .= Form::hidden('adv', $adv );
        
        $html .= '<p class="ca_title ' . ( $type == 'create-monex-live-account' ? 'live' : 'demo' ) . '">' . __($type) .'</p>';
        $html .= '<fieldset>';
        
        $html .= '<p>' ;
        $html .= Mifx::elementForm('kota',$frm);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('name',$frm);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('phone',$frm);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('email',$frm);
        $html .= '</p>';
        
        if ($frm == 'demo') {
            $html .= '<p>';
            $html .= Mifx::elementForm('deposit',$frm);
            $html .= '</p>';
        }
        
        if (isset($regularOrPartial)) {
            $checkSessionRegularOrPartial = App::$session->get('regularOrPartial'); 
            
            if (empty($checkSessionRegularOrPartial)) {
                App::$session->set('regularOrPartial',$regularOrPartial);
            }
        }
        
        $regularOrPartial = App::$session->get('regularOrPartial');
        
        $html .= Form::hidden('partialOrRegular',empty($regularOrPartial) ? "0" : $regularOrPartial);
        
        $html .= '<p>';
        $html .= Mifx::elementForm('referral',$frm,$referral);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('source',$frm,$referral);
        $html .= '</p>';
        
        $html .= '<p>';
        $html .= Mifx::elementForm('info',$frm);
        $html .= '</p>';
        
        $html .= '<p class="verifikasi">';
        $html .= Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4));
        //$html .= Form::hidden('captcha',null,array('class'=>'verif required'));
        
        $html .= Mifx::captcha();
        /**$html .= Captcha::instance()
                    ->reconfig('width',113)
                    ->reconfig('height',31)
                    ->render();**/
        $html .= '</p>';
        
        $html .= '<p class="agree">';
        $html .= Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) );
        $html .= __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') )
                 ));
        $html .= '</p>';
        
        $html .= '<p class="submit">';
        
        if ($type == 'create-free-demo-account' ) $html .= '<span>' . __('risk-free') . '</span>';
        
        $html .= Form::submit('submit',__('submit'),array('class'=>'btn_submit'));
        $html .= '</p>';
        
        $html .= '</fieldset>';
        $html .= Form::close();
        
        return $html;
    }
    
    public static function elementForm($type, $group = 'demo', $referral = null) {
        
        //echo "<!-- this page : ".$this->page." -->";
        if ($referral==null){
			/*
			if(!isset($_COOKIE['referal']))
			{
				setcookie('referal', 'X', time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
			}
        	*/
			//setcookie('referal', 'X', time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
        }else{
        	setcookie('referal', $referral, time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
        }
        
        $type    = ucfirst(strtolower($type));
        
        $id      = "$group-$type";
        
        $post    = Request::$initial->post();
        
		
        //$kota = self::$instance->_soap->getKota();  
        
        $elementKota  = '<select id="'.$id.'" name="accountKota" class="required">';
        $elementKota .= '<option value="">' . __("select-city") . '</option>';
		
       // foreach ($kota as $idKota => $namaKota) {
       //     $elementKota .= '<option value="' . $idKota . '">' . $namaKota . '</option>';
       // }
		$elementKota .= '<option value=1>Aek Kanopan</option>';
		$elementKota .= '<option value=422>Agats</option>';
		$elementKota .= '<option value=350>Airmadidi</option>';
		$elementKota .= '<option value=423>Ambon</option>';
		$elementKota .= '<option value=351>Ampana</option>';
		$elementKota .= '<option value=352>Amurang</option>';
		$elementKota .= '<option value=353>Andolo</option>';
		$elementKota .= '<option value=2>Arga Makmur</option>';
		$elementKota .= '<option value=3>Arosuka</option>';
		$elementKota .= '<option value=261>Atambua</option>';
		$elementKota .= '<option value=262>Baa</option>';
		$elementKota .= '<option value=263>Badung</option>';
		$elementKota .= '<option value=264>Bajawa</option>';
		$elementKota .= '<option value=4>Balige</option>';
		$elementKota .= '<option value=300>Balikpapan</option>';
		$elementKota .= '<option value=5>Banda Aceh</option>';
		$elementKota .= '<option value=6>Bandar Lampung</option>';
		$elementKota .= '<option value=7>Bandar Seri Bentan</option>';
		$elementKota .= '<option value=150>Bandung</option>';
		$elementKota .= '<option value=354>Banggai</option>';
		$elementKota .= '<option value=151>Bangil</option>';
		$elementKota .= '<option value=154>Bangkalan</option>';
		$elementKota .= '<option value=8>Bangkinang</option>';
		$elementKota .= '<option value=9>Bangko</option>';
		$elementKota .= '<option value=265>Bangli</option>';
		$elementKota .= '<option value=152>Banjar (Jawa Barat)</option>';
		$elementKota .= '<option value=310>Banjarbaru</option>';
		$elementKota .= '<option value=299>Banjarmasin</option>';
		$elementKota .= '<option value=153>Banjarnegara</option>';
		$elementKota .= '<option value=355>Bantaeng</option>';
		$elementKota .= '<option value=155>Bantul</option>';
		$elementKota .= '<option value=10>Banyuasin</option>';
		$elementKota .= '<option value=156>Banyumas</option>';
		$elementKota .= '<option value=157>Banyuwangi</option>';
		$elementKota .= '<option value=311>Barabai</option>';
		$elementKota .= '<option value=356>Barru</option>';
		$elementKota .= '<option value=11>Batam</option>';
		$elementKota .= '<option value=158>Batang</option>';
		$elementKota .= '<option value=312>Batang Tarang</option>';
		$elementKota .= '<option value=159>Batu</option>';
		$elementKota .= '<option value=313>Batulicin</option>';
		$elementKota .= '<option value=12>Baturaja</option>';
		$elementKota .= '<option value=13>Batusangkar</option>';
		$elementKota .= '<option value=357>Bau-Bau</option>';
		$elementKota .= '<option value=160>Bekasi</option>';
		$elementKota .= '<option value=14>Bengkalis</option>';
		$elementKota .= '<option value=314>Bengkayang</option>';
		$elementKota .= '<option value=15>Bengkulu</option>';
		$elementKota .= '<option value=358>Benteng</option>';
		$elementKota .= '<option value=424>Biak</option>';
		$elementKota .= '<option value=266>Bima</option>';
		$elementKota .= '<option value=16>Binjai</option>';
		$elementKota .= '<option value=17>Bintuhan</option>';
		$elementKota .= '<option value=425>Bintuni</option>';
		$elementKota .= '<option value=18>Bireuen</option>';
		$elementKota .= '<option value=359>Bitung</option>';
		$elementKota .= '<option value=19>Blambangan Umpu</option>';
		$elementKota .= '<option value=21>Blang Kejeren</option>';
		$elementKota .= '<option value=20>Blangpidie</option>';
		$elementKota .= '<option value=161>Blitar</option>';
		$elementKota .= '<option value=162>Blora</option>';
		$elementKota .= '<option value=163>Bogor</option>';
		$elementKota .= '<option value=164>Bojonegoro</option>';
		$elementKota .= '<option value=360>Bolaang Uki</option>';
		$elementKota .= '<option value=165>Bondowoso</option>';
		$elementKota .= '<option value=315>Bontang</option>';
		$elementKota .= '<option value=361>Boroko</option>';
		$elementKota .= '<option value=426>Botawa</option>';
		$elementKota .= '<option value=166>Boyolali</option>';
		$elementKota .= '<option value=168>Brebes</option>';
		$elementKota .= '<option value=22>Bukittinggi</option>';
		$elementKota .= '<option value=362>Bulukumba</option>';
		$elementKota .= '<option value=167>Bumiayu</option>';
		$elementKota .= '<option value=363>Bungku</option>';
		$elementKota .= '<option value=316>Buntok</option>';
		$elementKota .= '<option value=364>Buol</option>';
		$elementKota .= '<option value=365>Buranga</option>';
		$elementKota .= '<option value=427>Burmeso</option>';
		$elementKota .= '<option value=23>Calang</option>';
		$elementKota .= '<option value=170>Ciamis</option>';
		$elementKota .= '<option value=169>Cianjur</option>';
		$elementKota .= '<option value=171>Cibinong</option>';
		$elementKota .= '<option value=172>Cikampek</option>';
		$elementKota .= '<option value=173>Cikarang</option>';
		$elementKota .= '<option value=174>Cilacap</option>';
		$elementKota .= '<option value=175>Cilegon</option>';
		$elementKota .= '<option value=176>Cirebon</option>';
		$elementKota .= '<option value=420>Cliquers</option>';
		$elementKota .= '<option value=24>Curup</option>';
		$elementKota .= '<option value=25>Daik</option>';
		$elementKota .= '<option value=428>Dataran Hunimoa</option>';
		$elementKota .= '<option value=429>Dataran Hunipopu</option>';
		$elementKota .= '<option value=177>Demak</option>';
		$elementKota .= '<option value=267>Denpasar</option>';
		$elementKota .= '<option value=178>Depok</option>';
		$elementKota .= '<option value=26>Dolok Sanggul</option>';
		$elementKota .= '<option value=268>Dompu</option>';
		$elementKota .= '<option value=366>Donggala</option>';
		$elementKota .= '<option value=27>Dumai</option>';
		$elementKota .= '<option value=430>Elelim</option>';
		$elementKota .= '<option value=431>Enarotali</option>';
		$elementKota .= '<option value=269>Ende</option>';
		$elementKota .= '<option value=367>Enrekang</option>';
		$elementKota .= '<option value=432>Fak Fak</option>';
		$elementKota .= '<option value=433>Fef</option>';
		$elementKota .= '<option value=179>Garut</option>';
		$elementKota .= '<option value=28>Gedong Tataan</option>';
		$elementKota .= '<option value=270>Gianyar</option>';
		$elementKota .= '<option value=368>Gorontalo</option>';
		$elementKota .= '<option value=180>Gresik</option>';
		$elementKota .= '<option value=482>Grobogan</option>';
		$elementKota .= '<option value=29>Gunung Sitoli</option>';
		$elementKota .= '<option value=30>Gunung Sugih</option>';
		$elementKota .= '<option value=31>Gunung Tua</option>';
		$elementKota .= '<option value=32>Idi Rayeuk</option>';
		$elementKota .= '<option value=434>Ilaga</option>';
		$elementKota .= '<option value=33>Indralaya</option>';
		$elementKota .= '<option value=181>Indramayu</option>';
		$elementKota .= '<option value=435>Jailolo</option>';
		$elementKota .= '<option value=182>Jakarta</option>';
		$elementKota .= '<option value=34>Jambi</option>';
		$elementKota .= '<option value=35>Jantho</option>';
		$elementKota .= '<option value=436>Jayapura</option>';
		$elementKota .= '<option value=183>Jember</option>';
		$elementKota .= '<option value=369>Jeneponto</option>';
		$elementKota .= '<option value=184>Jepara</option>';
		$elementKota .= '<option value=185>Jombang</option>';
		$elementKota .= '<option value=36>Kabanjahe</option>';
		$elementKota .= '<option value=437>Kaimana</option>';
		$elementKota .= '<option value=186>Kajen</option>';
		$elementKota .= '<option value=271>Kalabahi</option>';
		$elementKota .= '<option value=37>Kalianda</option>';
		$elementKota .= '<option value=317>Kandangan</option>';
		$elementKota .= '<option value=38>Karang Baru</option>';
		$elementKota .= '<option value=39>Karang Tinggi</option>';
		$elementKota .= '<option value=187>Karanganyar</option>';
		$elementKota .= '<option value=272>Karangasem</option>';
		$elementKota .= '<option value=438>Karubaga</option>';
		$elementKota .= '<option value=370>Kawangkoan</option>';
		$elementKota .= '<option value=40>Kayu Agung</option>';
		$elementKota .= '<option value=188>Kebumen</option>';
		$elementKota .= '<option value=189>Kediri</option>';
		$elementKota .= '<option value=273>Kefamenanu</option>';
		$elementKota .= '<option value=190>Kendal</option>';
		$elementKota .= '<option value=371>Kendari</option>';
		$elementKota .= '<option value=439>Kenyam</option>';
		$elementKota .= '<option value=41>Kepahiang</option>';
		$elementKota .= '<option value=191>Kepanjen</option>';
		$elementKota .= '<option value=440>Kepi</option>';
		$elementKota .= '<option value=304>Ketapang</option>';
		$elementKota .= '<option value=441>Kigamani</option>';
		$elementKota .= '<option value=42>Kisaran</option>';
		$elementKota .= '<option value=192>Klaten</option>';
		$elementKota .= '<option value=274>Klungkung</option>';
		$elementKota .= '<option value=43>Koba</option>';
		$elementKota .= '<option value=442>Kobakma</option>';
		$elementKota .= '<option value=372>Kolaka</option>';
		$elementKota .= '<option value=44>Kota Agung</option>';
		$elementKota .= '<option value=45>Kota Bumi</option>';
		$elementKota .= '<option value=443>Kota Mulia</option>';
		$elementKota .= '<option value=46>Kota Pinang</option>';
		$elementKota .= '<option value=318>Kotabaru</option>';
		$elementKota .= '<option value=373>Kotamobagu</option>';
		$elementKota .= '<option value=319>Kuala Kapuas</option>';
		$elementKota .= '<option value=320>Kuala Kurun</option>';
		$elementKota .= '<option value=321>Kuala Pembuang</option>';
		$elementKota .= '<option value=47>Kuala Tungkal</option>';
		$elementKota .= '<option value=193>Kudus</option>';
		$elementKota .= '<option value=444>Kumurkek</option>';
		$elementKota .= '<option value=194>Kuningan</option>';
		$elementKota .= '<option value=275>Kupang</option>';
		$elementKota .= '<option value=48>Kutacane</option>';
		$elementKota .= '<option value=374>Kwandang</option>';
		$elementKota .= '<option value=445>Labuha</option>';
		$elementKota .= '<option value=276>Labuhan Bajo</option>';
		$elementKota .= '<option value=49>Lahat</option>';
		$elementKota .= '<option value=50>Lahomi</option>';
		$elementKota .= '<option value=195>Lamongan</option>';
		$elementKota .= '<option value=51>Langsa</option>';
		$elementKota .= '<option value=277>Larantuka</option>';
		$elementKota .= '<option value=375>Lasusua</option>';
		$elementKota .= '<option value=278>Lewoleba</option>';
		$elementKota .= '<option value=52>Lhokseumawe</option>';
		$elementKota .= '<option value=53>Lhoksukon</option>';
		$elementKota .= '<option value=421>Libuo Palma</option>';
		$elementKota .= '<option value=54>Limapuluh</option>';
		$elementKota .= '<option value=55>Liwa</option>';
		$elementKota .= '<option value=480>Lombok</option>';
		$elementKota .= '<option value=56>Lotu</option>';
		$elementKota .= '<option value=57>Lubuk Basung</option>';
		$elementKota .= '<option value=58>Lubuk Bendaharo</option>';
		$elementKota .= '<option value=59>Lubuk Linggau</option>';
		$elementKota .= '<option value=60>Lubuk Pakam</option>';
		$elementKota .= '<option value=61>Lubuk Sikaping</option>';
		$elementKota .= '<option value=196>Lumajang</option>';
		$elementKota .= '<option value=376>Luwuk</option>';
		$elementKota .= '<option value=446>Maba</option>';
		$elementKota .= '<option value=197>Madiun</option>';
		$elementKota .= '<option value=198>Magelang</option>';
		$elementKota .= '<option value=199>Magetan</option>';
		$elementKota .= '<option value=200>Majalengka</option>';
		$elementKota .= '<option value=377>Majene</option>';
		$elementKota .= '<option value=378>Makale</option>';
		$elementKota .= '<option value=379>Makassar (Ujung Pandang)</option>';
		$elementKota .= '<option value=201>Malang</option>';
		$elementKota .= '<option value=380>Malili</option>';
		$elementKota .= '<option value=322>Malinau</option>';
		$elementKota .= '<option value=381>Mamasa</option>';
		$elementKota .= '<option value=382>Mamuju</option>';
		$elementKota .= '<option value=383>Manado (Menado)</option>';
		$elementKota .= '<option value=62>Manggar</option>';
		$elementKota .= '<option value=63>Manna</option>';
		$elementKota .= '<option value=447>Manokwari</option>';
		$elementKota .= '<option value=323>Marabahan</option>';
		$elementKota .= '<option value=384>Marisa</option>';
		$elementKota .= '<option value=385>Maros</option>';
		$elementKota .= '<option value=324>Martapura</option>';
		$elementKota .= '<option value=64>Martapura (Sumatera Selatan)</option>';
		$elementKota .= '<option value=386>Masamba</option>';
		$elementKota .= '<option value=448>Masohi</option>';
		$elementKota .= '<option value=280>Mataram</option>';
		$elementKota .= '<option value=279>Maumere</option>';
		$elementKota .= '<option value=281>Mbay</option>';
		$elementKota .= '<option value=65>Medan</option>';
		$elementKota .= '<option value=387>Melonguane</option>';
		$elementKota .= '<option value=303>Mempawah</option>';
		$elementKota .= '<option value=66>Menggala</option>';
		$elementKota .= '<option value=67>Mentok</option>';
		$elementKota .= '<option value=449>Merauke</option>';
		$elementKota .= '<option value=68>Metro</option>';
		$elementKota .= '<option value=69>Meulaboh</option>';
		$elementKota .= '<option value=70>Meureude</option>';
		$elementKota .= '<option value=202>Mojokerto</option>';
		$elementKota .= '<option value=450>Morotai Selatan</option>';
		$elementKota .= '<option value=71>Muara Aman</option>';
		$elementKota .= '<option value=72>Muara Bulian</option>';
		$elementKota .= '<option value=73>Muara Bungo</option>';
		$elementKota .= '<option value=74>Muara Dua</option>';
		$elementKota .= '<option value=75>Muara Enim</option>';
		$elementKota .= '<option value=76>Muara Sabak</option>';
		$elementKota .= '<option value=77>Muara Tebo</option>';
		$elementKota .= '<option value=325>Muara Teweh</option>';
		$elementKota .= '<option value=78>Muaro Sijunjung</option>';
		$elementKota .= '<option value=79>Muko Muko</option>';
		$elementKota .= '<option value=203>Mungkid</option>';
		$elementKota .= '<option value=451>Nabire</option>';
		$elementKota .= '<option value=452>Namlea</option>';
		$elementKota .= '<option value=453>Namrole</option>';
		$elementKota .= '<option value=326>Nanga Bulik</option>';
		$elementKota .= '<option value=327>Nanga Pinoh</option>';
		$elementKota .= '<option value=282>Negara</option>';
		$elementKota .= '<option value=328>Ngabang</option>';
		$elementKota .= '<option value=204>Ngamprah</option>';
		$elementKota .= '<option value=205>Nganjuk</option>';
		$elementKota .= '<option value=206>Ngawi</option>';
		$elementKota .= '<option value=329>Nunukan</option>';
		$elementKota .= '<option value=454>Oksibil</option>';
		$elementKota .= '<option value=388>Ondong Siau</option>';
		$elementKota .= '<option value=455>Oobo</option>';
		$elementKota .= '<option value=207>Pacitan</option>';
		$elementKota .= '<option value=80>Padang</option>';
		$elementKota .= '<option value=81>Padang Aro</option>';
		$elementKota .= '<option value=82>Padang Panjang</option>';
		$elementKota .= '<option value=83>Padang Sidempuan</option>';
		$elementKota .= '<option value=84>Pagaralam</option>';
		$elementKota .= '<option value=85>Painan</option>';
		$elementKota .= '<option value=302>Palangkaraya</option>';
		$elementKota .= '<option value=86>Palembang</option>';
		$elementKota .= '<option value=389>Palopo</option>';
		$elementKota .= '<option value=390>Palu</option>';
		$elementKota .= '<option value=208>Pamekasan</option>';
		$elementKota .= '<option value=87>Pandan</option>';
		$elementKota .= '<option value=209>Pandeglang</option>';
		$elementKota .= '<option value=391>Pangkajene</option>';
		$elementKota .= '<option value=89>Pangkal Pinang</option>';
		$elementKota .= '<option value=330>Pangkalan Bun</option>';
		$elementKota .= '<option value=88>Pangkalan Kerinci</option>';
		$elementKota .= '<option value=90>Panguruan</option>';
		$elementKota .= '<option value=91>Panyabungan</option>';
		$elementKota .= '<option value=210>Pare</option>';
		$elementKota .= '<option value=392>Pare-Pare</option>';
		$elementKota .= '<option value=92>Pariaman</option>';
		$elementKota .= '<option value=393>Parigi</option>';
		$elementKota .= '<option value=331>Paringin</option>';
		$elementKota .= '<option value=93>Parit Malintang</option>';
		$elementKota .= '<option value=481>Pasaman Barat</option>';
		$elementKota .= '<option value=394>Pasangkayu</option>';
		$elementKota .= '<option value=94>Pasir Pengarayan</option>';
		$elementKota .= '<option value=212>Pasuruan</option>';
		$elementKota .= '<option value=211>Pati</option>';
		$elementKota .= '<option value=95>Payakumbuh</option>';
		$elementKota .= '<option value=213>Pekalongan</option>';
		$elementKota .= '<option value=96>Pekanbaru</option>';
		$elementKota .= '<option value=214>Pelabuhan Ratu</option>';
		$elementKota .= '<option value=332>Pelaihari</option>';
		$elementKota .= '<option value=215>Pemalang</option>';
		$elementKota .= '<option value=97>Pematang Siantar</option>';
		$elementKota .= '<option value=333>Penajam</option>';
		$elementKota .= '<option value=395>Pinrang</option>';
		$elementKota .= '<option value=396>Polewali</option>';
		$elementKota .= '<option value=216>Ponorogo</option>';
		$elementKota .= '<option value=297>Pontianak</option>';
		$elementKota .= '<option value=397>Poso</option>';
		$elementKota .= '<option value=98>Prabumulih</option>';
		$elementKota .= '<option value=283>Praya</option>';
		$elementKota .= '<option value=99>Pringsewu</option>';
		$elementKota .= '<option value=217>Probolinggo</option>';
		$elementKota .= '<option value=334>Pulang Pisau</option>';
		$elementKota .= '<option value=100>Pulau Punjung</option>';
		$elementKota .= '<option value=218>Purbalingga</option>';
		$elementKota .= '<option value=335>Purukcahu</option>';
		$elementKota .= '<option value=219>Purwakarta</option>';
		$elementKota .= '<option value=220>Purwodadi</option>';
		$elementKota .= '<option value=221>Purwokerto</option>';
		$elementKota .= '<option value=222>Purworejo</option>';
		$elementKota .= '<option value=307>Putussibau</option>';
		$elementKota .= '<option value=284>Raba</option>';
		$elementKota .= '<option value=398>Raha</option>';
		$elementKota .= '<option value=101>Ranai</option>';
		$elementKota .= '<option value=223>Rangkasbitung</option>';
		$elementKota .= '<option value=336>Rantau</option>';
		$elementKota .= '<option value=102>Rantau Prapat</option>';
		$elementKota .= '<option value=399>Rantepao</option>';
		$elementKota .= '<option value=456>Rasiei</option>';
		$elementKota .= '<option value=400>Ratahan</option>';
		$elementKota .= '<option value=103>Raya</option>';
		$elementKota .= '<option value=224>Rembang</option>';
		$elementKota .= '<option value=104>Rengat</option>';
		$elementKota .= '<option value=401>Rumbia</option>';
		$elementKota .= '<option value=285>Ruteng</option>';
		$elementKota .= '<option value=105>Sabang</option>';
		$elementKota .= '<option value=106>Salak</option>';
		$elementKota .= '<option value=225>Salatiga</option>';
		$elementKota .= '<option value=298>Samarinda</option>';
		$elementKota .= '<option value=308>Sambas</option>';
		$elementKota .= '<option value=226>Sampang</option>';
		$elementKota .= '<option value=309>Sampit</option>';
		$elementKota .= '<option value=457>Sanana</option>';
		$elementKota .= '<option value=337>Sangatta</option>';
		$elementKota .= '<option value=107>Sarilamak</option>';
		$elementKota .= '<option value=458>Sarmi</option>';
		$elementKota .= '<option value=108>Sarolangun</option>';
		$elementKota .= '<option value=459>Saumlaki</option>';
		$elementKota .= '<option value=109>Sawahlunto</option>';
		$elementKota .= '<option value=110>Sei Rampah</option>';
		$elementKota .= '<option value=338>Sekadau</option>';
		$elementKota .= '<option value=111>Sekayu</option>';
		$elementKota .= '<option value=112>Selat Panjang</option>';
		$elementKota .= '<option value=286>Selong</option>';
		$elementKota .= '<option value=227>Semarang</option>';
		$elementKota .= '<option value=339>Sendawar</option>';
		$elementKota .= '<option value=113>Sengeti</option>';
		$elementKota .= '<option value=402>Sengkang</option>';
		$elementKota .= '<option value=460>Sentani</option>';
		$elementKota .= '<option value=228>Serang</option>';
		$elementKota .= '<option value=461>Serui</option>';
		$elementKota .= '<option value=114>Siak Sri Indrapura</option>';
		$elementKota .= '<option value=115>Sibolga</option>';
		$elementKota .= '<option value=116>Sibuhuan</option>';
		$elementKota .= '<option value=229>Sidayu</option>';
		$elementKota .= '<option value=403>Sidenreng</option>';
		$elementKota .= '<option value=117>Sidikalang</option>';
		$elementKota .= '<option value=230>Sidoarjo</option>';
		$elementKota .= '<option value=404>Sigi Biromaru</option>';
		$elementKota .= '<option value=118>Sigli</option>';
		$elementKota .= '<option value=119>Simpang Empat</option>';
		$elementKota .= '<option value=120>Simpang Tiga Redelong</option>';
		$elementKota .= '<option value=121>Sinabang</option>';
		$elementKota .= '<option value=231>Singaparna</option>';
		$elementKota .= '<option value=287>Singaraja</option>';
		$elementKota .= '<option value=301>Singkawang</option>';
		$elementKota .= '<option value=122>Singkil</option>';
		$elementKota .= '<option value=405>Sinjai</option>';
		$elementKota .= '<option value=305>Sintang</option>';
		$elementKota .= '<option value=123>Sipirok</option>';
		$elementKota .= '<option value=232>Situbondo</option>';
		$elementKota .= '<option value=233>Slawi</option>';
		$elementKota .= '<option value=234>Sleman</option>';
		$elementKota .= '<option value=288>Soe</option>';
		$elementKota .= '<option value=244>Solo</option>';
		$elementKota .= '<option value=124>Solok</option>';
		$elementKota .= '<option value=235>Soreang</option>';
		$elementKota .= '<option value=462>Sorendiweri</option>';
		$elementKota .= '<option value=463>Sorong</option>';
		$elementKota .= '<option value=236>Sragen</option>';
		$elementKota .= '<option value=125>Stabat</option>';
		$elementKota .= '<option value=237>Subang</option>';
		$elementKota .= '<option value=126>Subulussalam</option>';
		$elementKota .= '<option value=464>Sugapa</option>';
		$elementKota .= '<option value=128>Suka Makmue</option>';
		$elementKota .= '<option value=238>Sukabumi</option>';
		$elementKota .= '<option value=127>Sukadana</option>';
		$elementKota .= '<option value=340>Sukadana</option>';
		$elementKota .= '<option value=341>Sukamara</option>';
		$elementKota .= '<option value=239>Sukoharjo</option>';
		$elementKota .= '<option value=289>Sumbawa Besar</option>';
		$elementKota .= '<option value=240>Sumber</option>';
		$elementKota .= '<option value=241>Sumedang</option>';
		$elementKota .= '<option value=242>Sumenep</option>';
		$elementKota .= '<option value=465>Sumohai</option>';
		$elementKota .= '<option value=130>Sungai Penuh</option>';
		$elementKota .= '<option value=342>Sungai Raya</option>';
		$elementKota .= '<option value=129>Sungailiat</option>';
		$elementKota .= '<option value=406>Sunggu Minasa</option>';
		$elementKota .= '<option value=483>Surakarta</option>';
		$elementKota .= '<option value=243>Surabaya</option>';
		$elementKota .= '<option value=407>Suwawa</option>';
		$elementKota .= '<option value=290>Tabanan</option>';
		$elementKota .= '<option value=408>Tahuna</option>';
		$elementKota .= '<option value=132>Tais</option>';
		$elementKota .= '<option value=409>Takalar</option>';
		$elementKota .= '<option value=131>Takengon</option>';
		$elementKota .= '<option value=291>Taliwang</option>';
		$elementKota .= '<option value=292>Tambolaka</option>';
		$elementKota .= '<option value=343>Tamiang</option>';
		$elementKota .= '<option value=344>Tanah Grogot</option>';
		$elementKota .= '<option value=466>Tanah Merah</option>';
		$elementKota .= '<option value=246>Tangerang</option>';
		$elementKota .= '<option value=247>Tangerang Selatan</option>';
		$elementKota .= '<option value=345>Tanjung</option>';
		$elementKota .= '<option value=293>Tanjung (Nusa Tenggara Barat)</option>';
		$elementKota .= '<option value=133>Tanjung Balai (Sumatera Utara)</option>';
		$elementKota .= '<option value=134>Tanjung Balai Karimun (Kepulauan Riau)</option>';
		$elementKota .= '<option value=135>Tanjung Enim</option>';
		$elementKota .= '<option value=136>Tanjung Pandan</option>';
		$elementKota .= '<option value=137>Tanjung Pinang</option>';
		$elementKota .= '<option value=347>Tanjung Redeb</option>';
		$elementKota .= '<option value=346>Tanjung Selor</option>';
		$elementKota .= '<option value=138>Tapaktuan</option>';
		$elementKota .= '<option value=306>Tarakan</option>';
		$elementKota .= '<option value=139>Tarempa</option>';
		$elementKota .= '<option value=140>Tarutung</option>';
		$elementKota .= '<option value=245>Tasikmalaya</option>';
		$elementKota .= '<option value=142>Tebing Tinggi (Sumatera Selatan)</option>';
		$elementKota .= '<option value=141>Tebing Tinggi (Sumatera Utara)</option>';
		$elementKota .= '<option value=248>Tegal</option>';
		$elementKota .= '<option value=143>Teluk Dalam</option>';
		$elementKota .= '<option value=144>Teluk Kuantan</option>';
		$elementKota .= '<option value=249>Temanggung</option>';
		$elementKota .= '<option value=145>Tembilahan</option>';
		$elementKota .= '<option value=467>Teminabuan</option>';
		$elementKota .= '<option value=348>Tenggarong</option>';
		$elementKota .= '<option value=468>Ternate</option>';
		$elementKota .= '<option value=469>Tiakur</option>';
		$elementKota .= '<option value=349>Tideng Pale</option>';
		$elementKota .= '<option value=470>Tidore</option>';
		$elementKota .= '<option value=250>Tigaraksa</option>';
		$elementKota .= '<option value=471>Tigi</option>';
		$elementKota .= '<option value=410>Tilamuta</option>';
		$elementKota .= '<option value=472>Timika</option>';
		$elementKota .= '<option value=473>Tiom</option>';
		$elementKota .= '<option value=474>Tobelo</option>';
		$elementKota .= '<option value=146>Toboali</option>';
		$elementKota .= '<option value=411>Toli Toli</option>';
		$elementKota .= '<option value=412>Tomohon</option>';
		$elementKota .= '<option value=413>Tondano</option>';
		$elementKota .= '<option value=251>Trenggalek</option>';
		$elementKota .= '<option value=475>Tual</option>';
		$elementKota .= '<option value=147>Tuapejat</option>';
		$elementKota .= '<option value=252>Tuban</option>';
		$elementKota .= '<option value=253>Tulung Agung</option>';
		$elementKota .= '<option value=414>Tutuyan</option>';
		$elementKota .= '<option value=148>Ujung Tanjung</option>';
		$elementKota .= '<option value=415>Unaaha</option>';
		$elementKota .= '<option value=254>Ungaran</option>';
		$elementKota .= '<option value=294>Waibakul</option>';
		$elementKota .= '<option value=295>Waikabubak</option>';
		$elementKota .= '<option value=296>Waingapu</option>';
		$elementKota .= '<option value=476>Waisai</option>';
		$elementKota .= '<option value=477>Wamena</option>';
		$elementKota .= '<option value=417>Wanggudu</option>';
		$elementKota .= '<option value=416>Wangi Wangi</option>';
		$elementKota .= '<option value=478>Waris</option>';
		$elementKota .= '<option value=418>Watampone</option>';
		$elementKota .= '<option value=419>Watan Soppeng</option>';
		$elementKota .= '<option value=255>Wates</option>';
		$elementKota .= '<option value=479>Weda</option>';
		$elementKota .= '<option value=256>Wlingi</option>';
		$elementKota .= '<option value=257>Wonogiri</option>';
		$elementKota .= '<option value=258>Wonosari</option>';
		$elementKota .= '<option value=259>Wonosobo</option>';
		$elementKota .= '<option value=260>Yogyakarta</option>';
		
        $elementKota .= '</select>';
               
        $elementName = Form::input( 'accountName'
                    ,empty($post) || !isset($post['accountName']) ? __('full-name') : !isset($post['accountName']) 
                    ,array(
                            'defaultvalue' => __('full-name'),
                            'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('full-name').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('full-name').'\'' )
                 );
        
        $elementPhone = Form::input( 'accountPhone'
                    ,empty($post) || !isset($post['accountPhone']) ? __('phone') : $post['accountPhone']
                    ,array(
                            'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('phone').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('phone').'\'' )
                 );
        
        $elementEmail = Form::input( 'accountEmail'
                    ,empty($post) || !isset($post['accountEmail']) ? __('email') : $post['accountEmail']
                    ,array(
                            'class'   => 'required email',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('email').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('email').'\'' )
                 );
        
		
        $referralAttrs = array(
                            'title'   => 'Ketik X jika tidak mengetahui',
                            //'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('referral').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('referral').'\'' );
        
        $referralCheck = $referral;
        //$referral = App::$session->get('referral_account');             
        if (isset($referral)  and $referral != null) {
            $referral = self::$instance->_soap->getMarketing($referral);
            App::$session->set('referral_account',$referral);
            Cookie::set('key_referral', $referral);
        } else {
            $referral = App::$session->get('referral_account',__('referral')); 
        }
        
		
        if($referralCheck  != null and $referral == 'Invalid Code') echo '<script type="text/javascript"> jAlert(\'<b>Invalid key. Please contact your sales</b>\'); </script> '; 

        if ($referral == 'Invalid Code') $referral =  __('referral');
            
         if ($referral != __('referral')) {
            
            $referralAttrs['readonly'] = 'readonly';
            
            //Cookie::set('key_referral', $referral);
            Cookie::set('key_referral', $referral);
            // Set cookies to expire after 1 week
            Cookie::$expiration = 2592000;
        }
        
        //Get advertising code
        $getAdv = Request::$initial->query('adv');        
        if (!empty($getAdv) && strtolower($type) == 'referral')
        {
            $adv2brch = Mifx::soapAffiliate()->adv2branch($getAdv);
            $adv2wp   = Mifx::soapAffiliate()->adv2wp($getAdv);
            
            if ($adv2wp != 'WP058bm')
            {
                $referral = $adv2wp;
                $referralAttrs['readonly'] = 'readonly';
            }
        }
        //End of advertising
		
		/*updated by Hanna
		  Cookies Kode referal dihapus sementara apabila user mengakses dengan menggunakan link GAds
		*/
		$url =  isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
		$righturl = substr($url, -11);
		if ($righturl=="Source=GAds"){
			Cookie::set("key_referral", "", time() - 3600);
			Cookie::set("referal", "", time() - 3600);
			$elementReferral = Form::input( 'accountReferral'
                    ,empty($post) || !isset($post['accountReferral']) ? __('referral') : $post['accountReferral']
                    ,array(
                            'title'   => 'Ketik X jika tidak mengetahui',
                            //'class'   => 'required',
                            'id'      => $id,
                            'onfocus' => 'if(value==\''.__('referral').'\')value=\'\';',
                            'onblur'  => 'if(value==\'\')value=\''.__('referral').'\'' )
                 );
		}else{
        $elementReferral = Form::input( 'accountReferral'
                    ,empty($post) || !isset($post['accountReferral']) ? $referral : $post['accountReferral']
                    ,$referralAttrs
                 );
		 }
     
        $elementDeposit  = '<select id="'.$id.'" name="accountDeposit" class="required">';
        $elementDeposit .= '<option value="">' . __("opening-deposit") . '</option>';
		$elementDeposit .= '<option value="10000">10.000</option>';
		$elementDeposit .= '<option value="50000">50.000</option>';
		$elementDeposit .= '<option value="100000">100.000</option>';
		$elementDeposit .= '<option value="200000">200.000</option>';
		$elementDeposit .= '<option value="500000">500.000</option>';
		$elementDeposit .= '</select>';
		/*
        foreach ($deposit as $idDeposit => $namaDeposit) {
            $elementDeposit .= '<option value="' . $idDeposit . '">' . $namaDeposit . '</option>';
        }
		*/
        $elementDeposit2  = '<select id="'.$id.'" name="accountDeposit" class="required">';
        $elementDeposit2 .= '<option value="">' . __("opening-deposit") . '</option>';
		$elementDeposit2 .= '<option value="5000">5000</option>';
		$elementDeposit2 .= '</select>';        
		
		
		$elementDeposit3  = '<select id="'.$id.'" name="accountDeposit" class="required">';
        $elementDeposit3 .= '<option value="">' . __("opening-deposit") . '</option>';
		$elementDeposit3 .= '<option value="5000">5000</option>';
		$elementDeposit3 .= '</select>';        
		/* ================ TEST DI CLOSE BY HENDRA 1 JUL 2014 *
        $deposit2 = self::$instance->_soap->getDeposit();
        
        $elementDeposit2  = '<select id="'.$id.'" name="accountDeposit" class="required">';
        $elementDeposit2 .= '<option value="">' . __("opening-deposit") . '</option>';
        $elementDeposit2 .= '<option value="1000">1.000</option>';
		
		
        foreach ($deposit2 as $idDeposit2 => $namaDeposit2) {
            $elementDeposit2 .= '<option value="' . $idDeposit2 . '">' . $namaDeposit2 . '</option>';
        }
        $elementDeposit2 .= '</select>';
        */
		/* ================ TEST DI CLOSE BY HENDRA 1 JUL 2014 */
		
        //$elementDeposit = Form::select( 'accountDeposit'
//                    ,array_merge( array( "" => __('opening-deposit') ), self::$instance->_soap->getDeposit() )
//                    ,empty($post) || !isset($post['accountDeposit']) ? null : $post['accountDeposit']
//                    ,array(
//                            'class'   => 'required',
//                            'id'      => $id )
//                 );
                                          
        $elementInfo  = Form::select( 'accountInformation[]'
                    ,self::$instance->_soap->getInfo() 
                    ,empty($post) || !isset($post['accountInformation']) ? null : $post['accountInformation'] 
                    ,array( 
                            'multiple' => 'multiple',
                            'class'    => 'required',
                            'id'       => $id ) 
                ); 
        
        //$src = self::$instance->_soap->getSourceList();   
//        $src[""] = __("acc-source-list"); ksort($src);
//        
//        $elementSource = Form::select( 'accountSource'
//                    ,$src
//                    ,empty($post) || !isset($post['accountSource']) ? null : $post['accountSource']
//                    ,array(
//                            'class'   => 'required',
//                            'id'      => $id )
//                 );
        
        $src = self::$instance->_soap->getSourceList();
        
        #echo Debug::vars($src);   
        
        //Check for query Source
        $qSource = Request::$initial->query('Source');
        
        //Check for session Source
        if (empty($qSource)) {
            $qSource = Session::instance()->get('Source');                
        } 
        
        if (!empty($qSource)) {
            //Set to session
            Session::instance()->set('Source',$qSource);
        }
        
        foreach ($src as $ksrc => $vsrc) {
            if ($referral != __('referral')) {
                if (strtolower($vsrc) === 'ref.link') {
                    $elementSourceValue = $ksrc; 
                    break;
                }
            } else {
                if (!empty($qSource) && $vsrc == $qSource) {
                    $elementSourceValue = $ksrc;
                    break;
                }
                if (empty($qSource) && $vsrc == 'Direct')
                {
                    $elementSourceValue = $ksrc;
                    break;
                }
            }
        }
        
        if (!isset($elementSourceValue)) {
            foreach ($src as $ksrc => $vsrc) {
                $elementSourceValue = $ksrc;
                break;
            }                
        }
        
        $elementSource = Form::hidden('accountSource',$elementSourceValue);
        
        $element = "element$type";
        
        return isset($$element) ? $$element : null;   
    }
    
    public static function sitemap($nav , $level = 0) {
        $html  = '<ul';
        if ($level == 0) $html .= ' class="site_map" style="margin-top:20px;"';
        $html .= '>';
        
        foreach ($nav as $n) {
            
            if(empty($n->position)) continue;

            $html .= '<li';
            $html .= '>';
            $html .= ($n->permalink == 'live-chat' ? '<a href="#" onclick="Comm100API.open_chat_window(event, 2552);">'. $n->label. '</a>' :  HTML::anchor( Page::get("completedNavigationUri:$n->permalink") , Page::get("simpleBreadCrumb:$n->permalink") ) );
            
            if (!empty($n->child))
                $html .= self::sitemap($n->child, $level + 1);
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
    
    public static function set($key,$value) {
        if ( ($x = strpos($key,':')) === false)
        {
            self::$instance->$key = $value;
        }
        else 
        {
            $argument = substr($key,$x+1); $key = substr($key,0,$x);    
            self::$instance->$key  = array_merge(self::$instance->$key,array($argument => $value));
        }
    }
    
    public static function get($key,$default = null) {
        if ( ($x = strpos($key,':')) === false && !isset(self::$instance->$key) ) return $default;
        
        if ( $x !== false ) { $argument = substr($key,$x+1); $key = substr($key,0,$x); } 
        
        $get = self::$instance->$key;
        
        if (is_array($get) && $x !== false) {
            return !empty($get[$argument]) ? $get[$argument] : $default;
        }
        
        return $get;
    }
    
    public static function select_ticket($length = 3, $selected = null,$attrs = array()) {
        $options = array("0"=>0);
        for ($i = 1; $i <= $length; $i++)
            $options[$i] = $i;
            
        return Form::select('quantity',$options,$selected,$attrs);
    }
    
    
    protected static $_captcha; 
    
    public static function captcha($width = 200, $height = 70, $hidden = false) {
        
        if ( empty(self::$_captcha) ) {
            //include DOCROOT . 'coolcaptcha/captcha.php';
            
            self::$_captcha = HTML::image( URL::base(true) .'coolcaptcha/captcha.php?sesId='. App::$session->id() . ($hidden===true?'&hidden=1':'')   , array ( 'width'=>$width,'height'=>$height ) );
        } 
            
        return self::$_captcha;
    }

    
    protected static $spAffiliate;
    
    public static function soapAffiliate() {
        if (!isset(self::$spAffiliate))
            self::$spAffiliate = $client = new SoapClient('http://affiliate.mifx.com/soap/mifx');
            
        return self::$spAffiliate;   
    } 
    
    public static function registerAffiliate($post) {
        //source,type,nama,alamat,kota,email,telpon,mobile,website
        $soap = self::soapAffiliate();
        
        return $soap->register(array(
                    'source' => $post->affiliate_source,
                    'type'   => $post->affiliate_type,
                    'nama'   => $post->company,
                    'alamat' => $post->address,
                    'kota'   => $post->city,
                    'email'  => $post->email,
                    'telpon' => $post->phone,
                    'mobile' => $post->handphone,
                    'website'=> ''
               ));
    }
}

?>
