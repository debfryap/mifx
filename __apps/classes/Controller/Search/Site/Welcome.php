<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Search_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section;

    private $content;
    
    private $item;

    public function before() {
        
        $this->layout = 'one-column';        
        
        parent::before();
        
        App::$config->menu->active = 'about-us';
        
        $this->section  = $this->request->param('section1');      
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
             
        // Get input //
        $field = $this->section;

        if(empty($field) || $field == "") return false;
        
        // Query all //
        // Ebook //
        $ebook =  DB::select('*')
                        ->from('ebook')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // Faqs //
        $faqs = DB::select('*')
                        ->from('faqs')
                        ->where('question','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // Glossaries //
        $glossaries = DB::select('*')
                        ->from('glossaries')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // Monex Schedules //
        /*$monex_schedules = DB::select('*')
                        ->from('monex_schedules')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();*/
        // Monex Videos ///
        $monex_videos = DB::select('*')
                        ->from('monex_videos')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // News // 
        $news = DB::select('*')
                        ->from('news')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // News Event //
        $news_event = DB::select('*')
                        ->from('news_events')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // News Update //
        $news_update = DB::select('*')
                        ->from('news_updates')
                        ->where('label','like', '%'. $field .'%')
                        ->as_object()
                        ->execute();
        // Page //
        $page = DB::select('snp.id','snp.title','snp.intro','snp.description', 'sn.judul_icon' , 'sn.*')
                    ->from(array('site_navigation_pages','snp'))  
                    ->join(array('site_navigations','sn'))->on('sn.id', '=', 'snp.site_navigation_id')
                    ->where('snp.title','like','%'. $field .'%')
                    ->as_object()
                    ->execute();

        $this->layout->set('content', 
                        View::factory('layout/search-page')
                        ->set('ebook', $ebook)
                        ->set('faqs', $faqs)
                        ->set('glossaries', $glossaries)
                        //->set('monex_schedules', $monex_schedules)
                        ->set('monex_videos', $monex_videos)
                        ->set('news', $news)
                        ->set('news_event', $news_event)
                        ->set('news_update', $news_update)
                        ->set('page', $page)->render());
    }
}