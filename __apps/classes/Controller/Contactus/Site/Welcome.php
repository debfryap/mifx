<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contactus_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section = null;
    
    private $item = null;
    
    private $page;
    
    public function before() { 
        $this->layout = 'two-column';
        
        parent::before();
        
        App::$config->menu->active = 'contact-us';
        
        $this->section = $this->request->param('section1');
        
        $this->item    = $this->request->param('section2');
        
        $this->layout->set('live',true);
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
        
        if (!empty($this->post)) {


            /**
             * Conversion 
             * extract post
             */
            foreach($this->post as $key => $val){
                $element[$key]  = $val;
            }
            Conversion::record('Contact Us', $element); 
            /** end */  



            //Configuration email
            #echo Debug::vars($this->post);
            #echo Debug::vars(Contact::$config);
            $subject = "subject-of-message";
            $subject = !empty($this->post->$subject) ? $this->post->$subject : 'Somebody has send you an email';            
            $to      = !empty($this->post->kota) ? $this->post->kota : ( empty(Contact::$config->office->email) ? 'sales@mifx.com' : Contact::$config->office->email );
            #$to      = 'daniel@webarq.com'; 
            $noreply = !empty($this->post->email) ? $this->post->email : 'dari.web@mifx.com';            
            $mesage  = View::factory('contact/email')->set('post',$this->post)->render();
            
            Model::factory('Contact_Form')->save();
            
            if (!empty(Contact::$config) && !empty(Contact::$config->send_to_email)) 
            {
                Email::factory($subject,$mesage,'text/html')               
            				->from($noreply)
            				->to($to)
            				->send();  
                // kirim juga ke sales            
                Email::factory($subject,$mesage,'text/html')               
                            ->from($noreply)
                            ->to('sales@mifx.com')
                            ->send();              
            }
                        
            App::$session->set('submit-form',true);
             
            $this->redirect(Page::get('completedNavigationUri:contact-us'));
            
            return;
        }
        
        /** Get Left Banners **/
        $leftBanners = Model::factory('Medialibrary')->simple_application('page-left-bnnr',72,'site_navigations');
        $htmlBanner  = '';
        if (!empty($leftBanners) && $leftBanners->valid()) {
            foreach ($leftBanners as $b) {
                $detail = $link = $title = '';
                $attrs  = array( 'class' => 'btn' );
                if (!empty($b->detail)) {
                    $detail = json_decode($b->detail);
                    $link   = "#";
                    if (!empty($detail->banner_permalink)) {
                        if (strpos($detail->banner_permalink,'http') === 0) {
                            $link = $detail->banner_permalink;
                            $attrs['target'] = '_blank';
                        } else {
                            $link = Page::get("completedNavigationUri:$detail->banner_permalink");
                            if(empty($link)) {
                                $link = URL::front($detail->banner_permalink);
                            }    
                        }
                    }
                    
                    if(Language::$current == 'tw' || Language::$current == 'cn'){
                        $bannerTitleMedia = 'banner_title_'.Language::$current;
                        $title  = !empty($detail->$bannerTitleMedia) ? $detail->$bannerTitleMedia : $detail->banner_title;
                    }else
                        $title  = $detail->banner_title;    

                }
                
                $htmlBanner .= '<div class="platform">'.
                        HTML::image( URL::mediaImage( $b->folder . '/' . $b->file , null ) , array( 'alt' => '' ) ).
                        '<p>'.(preg_replace('/(monex)/i','<span>$1</span>',$title)).'</p>'.
                        '<p>'.HTML::anchor( $link , HTML::image( URL::templateImage('material/btn_read.png') ) , $attrs).'</p>'. 
                     '</div>';
            }    
        }
        
        $this->layout
             ->set('htmlBanner',$htmlBanner)
             ->set( 'content', 
                    View::factory('contact/home')
                        ->set( 'submitForm', App::$session->get('submit-form',false) )
                        ->set( 'provinces' , Model::factory('Contact_Province')->getProvinceOffice() ) );
    }
}