<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Ajax_Site extends Controller_Ajax {
    /**
     * object stdClass(7) {
        public accountkota => string(1) "0"
        public accountname => string(6) "Daniel"
        public accountphone => string(4) "0546"
        public accountemail => string(17) "daniel@webarq.com"
        public accountdeposit => string(1) "0"
        public accountreferral => string(4) "gbew"
        public accountinformation => string(13) "Search Engine"
        public captcha => string(4) "xm5r"
        
        kota name phone email referal deposit information
    }
    **/
    
    private $noreply;
    
    private $ccEducation = 'education@mifx.com';
    
    private $ccCorporate = 'corporatecommunication@mifx.com';
    
    public function before() {
        parent::before();
        
        $this->ccEducation = Contact::$config->office->seminar_education_email;
        
        $this->ccCorporate = Contact::$config->office->seminar_corporate_email;
        
        $this->noreply = 'noreply@mifx.com';
    }
    
    public function action_submit_demo_account () {
        if (empty($this->post)) 
        {
            $this->response->body('No Direct Access');
            return;    
        } 
        
        //Post
        $post   = $this->post;
        
        //Validate post
        if (empty($post->accountkota)
                || empty($post->accountname) || $post->accountname == __('full-name')
                || empty($post->accountphone) || $post->accountphone == __('phone')
                || empty($post->accountemail) || $post->accountemail == __('email')
                || empty($post->accountdeposit)
                || empty($post->accountinformation))
        {
            $this->response->body('<ul class="error"><li>'.__('Please Complete Your Data').'</li></ul>');
            return;
        }
 
        
        //Check email
        $domain = strstr($post->accountemail, '@');
        
        //Valid captcha
        $isCaptcha = Captcha::valid($post->captcha);
        //$isCaptcha = true;
        
        if(!$isCaptcha ){
            //Valid captcha
            $isCaptcha = $post->captcha === App::$session->get('hiddenCaptcha'); 
        } 
        
        if ( empty($domain) || strtolower(substr($domain,1)) == 'mifx.com' ) {
            $message = '<ul class="error"><li>'.__('forbidden-domain-email',array(':domain'=>'mifx.com')).'</li></ul>';     
        }
		elseif ( empty($domain) || strtolower(substr($domain,1)) == 'yopmail.com' ) {
            $message = '<ul class="error"><li>'.__('forbidden-domain-email',array(':domain'=>'yopmail.com')).'</li></ul>';   
		}
		elseif (!$isCaptcha) {
            $message = '<ul class="error"><li>'.__('error-captcha').'</li></ul>';
        } else {
            //Soap initialize
            Mifx::instance()->soap();
                    
            //Get soap
            $client = Mifx::get('_soap');    
            
            if (trim($post->accountreferral) === 'x' || $post->accountreferral == __('referral'))
                $post->accountreferral = 'X';
            
            /*
            if($client->regDemo($post->accountemail) OR $client->regLive($post->accountemail)){
                // set session, 23 Mei 2014
                App::$session->set('accountemail', $post->accountemail);
                App::$session->set('accountphone', $post->accountphone);
                App::$session->set('submit-form-account', true); 
                
                echo '<p>Your email already registered, plese click the button below to verify your account <br /><input type="submit" class="button-submit" value="Verify" onclick="window.location =\''. Translate::uri(Language::$current,'accounts/create-free-demo-account-success') .'\'" /></p>';
                
                die;
            }
            */

            //Regular or Partial
            $regularOrPartial = !empty($post->partialorregular) && ($post->partialorregular === 1 || $post->partialorregular === '1') ? 'demomonex1' : 'demomonex';
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            $dataSoap = array(
                        'kota'          => $post->accountkota,
                        'name'          => $post->accountname,
                        'phone'         => $post->accountphone,
                        'email'         => $post->accountemail,
                        'referal'       => $post->accountreferral,
                        'deposit'       => $post->accountdeposit,
                        'source'        => $post->accountsource,
                        'information'   => $post->accountinformation,
                        'group'         => $regularOrPartial,
                        'ip'            => $_SERVER['REMOTE_ADDR'], // tambahan ahyo@mifx.com                            
                        'date'          => date('Y-m-d H:i:s') ,
                        'cookie'		=> $_COOKIE['visit_id']
                    );
            
             // set session, 23 Mei 2014
            App::$session->set('accountemail', $post->accountemail);
            App::$session->set('accountphone', $post->accountphone);
            
            // Monex Affiliate Script
            if(!empty($post->adv)) $dataSoap['adv'] = $post->adv;

            $confirm = $client->registerDemo($dataSoap);
            
            //if ( !isset($confirm['status']) || ( !isset($confirm['data']['key']) && !isset($confirm['data']['link']) ) ) {
            if ( !isset($confirm['status']) ) {                
                $message = '<ul class="error"><li>Unknown Error</li></ul>';
            } elseif ($confirm['status'] == 'success') {
                
                if (!isset($confirm['data']) || !isset($confirm['data']['key'])) {
                    $this->response->body('<ul class="error"><li>Unknown Error</li></ul>');                        
                    return;
                }


                 /**
                 * Conversion 
                 * extract post
                 */
                foreach($post as $key => $val){
                    $element[$key]  = $val;
                }
                Conversion::record('Create Demo Account', $element); 
                /** end */ 

                
                $data    = $confirm['data'];
                
                /** **/ 
                $subject = "Link Activation MIFX"; // Demo Account";            
                $to      = $post->accountemail; 
                $noreply = $this->noreply;    
                $url     = Translate::uri(Language::$current,'accounts/verify-demo-account/'.$data['key']);
                
                if ($post->accountreferral == 'X')
                    $url .= '?welcome=true&key='.$post->accountname;
                        
                $mailFormat = Model::factory('Emailformat')->item('Activation MIFX Demo Account');
                $mailFormat = Translate::item('email_formats',$mailFormat);
            
                if(empty($mailFormat)){
                   $mesage  = View::factory('account/email-verify-url')
                                ->set( 'subject',$subject )
                                ->set( 'post',$post )
                                ->set( 'url', $url )
                                ->set( 'type','demo' )
                                ->render();
                } else {              
                    $subject = $mailFormat->subject;
                    $body    = $mailFormat->description;

                    $body    = str_replace('[user]', $post->accountname, $body);
                    $body    = str_replace('[type]', 'demo', $body);
                    $body    = str_replace('[link]','<a href="'.$url.'" target="_blank">',$body);
                    $body    = str_replace('[/link]','</a>',$body);
                    $body    = str_replace('[link-string]',$url,$body);
                    
                    $mesage  = View::factory('general/html-email')
                                        ->set( 'subject',$subject )
                                        ->set( 'body',$body )
                                        ->render(); 
                }
                    
                Email::factory($subject,$mesage,'text/html')               
                            ->from($noreply,'MIFX')
                            ->to($to)
                            ->send();    

                App::$session->set('submit-form-account', true); 

                $message = 'success';
                /** **/
            } elseif ($confirm['status'] == 'error') {
                
                $data    = $confirm['data'];
                
                $message = '<ul class="error">';
                
                foreach ($data as $error) {
                    $message .= '<li>'.$error[0].'</li>';
                }
                   
                $message .= '</ul>';
            }
            
        }
        
        $this->response->body($message);                       
    }
    
    public function action_submit_live_account() {
        if (empty($this->post)) 
        {
            $this->response->body('No Direct Access');
            return;    
        } 
        
        //Post
        $post   = $this->post;
        
        //Validate post
        if (empty($post->accountkota)
                || empty($post->accountname) || $post->accountname == __('full-name')
                || empty($post->accountphone) || $post->accountphone == __('phone')
                || empty($post->accountemail) || $post->accountemail == __('email')
                || empty($post->accountinformation))
        {
            $this->response->body('<ul class="error"><li>'.__('Please Complete Your Data').'</li></ul>');
            return;
        }

        //Check email 
        $domain = strstr($post->accountemail, '@'); 
        
        //Valid captcha
        $isCaptcha = Captcha::valid($post->captcha);
        //$isCaptcha = true;
        
        if ( empty($domain) || strtolower(substr($domain,1)) == 'mifx.com' ) {
            $message = '<ul class="error"><li>'.__('forbidden-domain-email',array(':domain'=>'mifx.com')).'</li></ul>';    
        } 
		elseif ( empty($domain) || strtolower(substr($domain,1)) == 'yopmail.com' ) {
            $message = '<ul class="error"><li>'.__('forbidden-domain-email',array(':domain'=>'yopmail.com')).'</li></ul>';   
		}		
		elseif (!$isCaptcha) {
            $message = '<ul class="error"><li>'.__('error-captcha').'</li></ul>';
        } else {
            //Soap initialize
            Mifx::instance()->soap();
                    
            //Get soap
            $client = Mifx::get('_soap');   
            
            /*
            if($client->regDemo($post->accountemail) OR $client->regLive($post->accountemail)){
                // set session, 23 Mei 2014
                App::$session->set('accountemail', $post->accountemail);
                App::$session->set('accountphone', $post->accountphone);
                App::$session->set('submit-form-account', true);
                
                echo '<p>Your email already registered, plese click the button below to verify your account <br /><input type="submit" class="button-submit" value="Verify" onclick="window.location =\''. Translate::uri(Language::$current,'accounts/create-free-demo-account-success') .'\'" /></p>';
                
                die;
            }
            */
            

            if (trim($post->accountreferral) === 'x' || $post->accountreferral == __('referral'))
                $post->accountreferral = 'X';
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            $dataSoap = array(
                        'kota'          => $post->accountkota,
                        'name'          => $post->accountname,
                        'phone'         => $post->accountphone,
                        'email'         => $post->accountemail,
                        'referal'       => $post->accountreferral,
                        //'deposit'       => $post->accountdeposit,
                        'source'        => $post->accountsource,
                        'information'   => $post->accountinformation ,
                        'ip'            => $_SERVER['REMOTE_ADDR'], // tambahan ahyo@mifx.com 
                        'date'          => date('Y-m-d H:i:s'),
                        'cookie'		=> $_COOKIE['visit_id'] 
                    );
            
             // set session, 23 Mei 2014
            App::$session->set('accountemail', $post->accountemail);
            App::$session->set('accountphone', $post->accountphone);
            
            // Monex Affiliate Script
            if(!empty($post->adv)) $dataSoap['adv'] = $post->adv;
            
            $confirm = $client->registerLive($dataSoap);
                
            //if ( empty($confirm['status']) || ( empty($confirm['data']['key']) && empty($confirm['data']['link']) ) ) {
            if ( empty($confirm['status']) ) {                
                $message = '<ul class="error"><li>Unknown Error</li></ul>';
            } elseif ($confirm['status'] == 'success') {
                

                 /**
                 * Conversion 
                 * extract post
                 */
                foreach($post as $key => $val){
                    $element[$key]  = $val;
                }
                Conversion::record('Create Live Account', $element); 
                /** end */  

                
                $data    = $confirm['data'];
                
                if ( isset($data['key']) ) {
                    /** **/ 
                    $subject = "Link Activation MIFX"; // Live Account";            
                    $to      = $post->accountemail; 
                    $noreply = $this->noreply;  
                    $url     = Translate::uri(Language::$current,'accounts/verify-live-account/'.$data['key']);
                    
                    if ($post->accountreferral == 'X')
                    $url .= '?welcome=true&key='.$post->accountname;
                        
                    $mesage  = View::factory('account/email-verify-url')
                                        ->set( 'subject',$subject )
                                        ->set( 'post',$post )
                                        ->set( 'url',$url )
                                        ->set( 'type','live' )
                                        ->render();
                    
                    Email::factory($subject,$mesage,'text/html')               
                				->from($noreply,'MIFX')
                				->to($to)
                				->send();  

                    //$_SESSION['submit-form-account'] = true;

                    App::$session->set('submit-form-account', true);
                    
                    $message = 'success';
                } elseif( isset($data['link']) ) {
                    $mailFormat = Model::factory('Emailformat')->item('Auto Login Client Area');
                    $mailFormat = Translate::item('email_formats',$mailFormat);
            
                    
                    $to      = $post->accountemail; 
                    $noreply = $this->noreply;
                        
                    if (empty($mailFormat)) {
                        $subject = "Link MIFX Auto Login";
                        $mesage  = '
                                    <p>Kepada yang terhomat,</p>
                                    <p>Silahkan klik&nbsp;<strong>' 
                                        . HTML::anchor( $data['link'] , 'disini' , array ( 'target'=>'_blank'))  
                                        . '</strong>&nbsp;atau salin&nbsp;<strong>'.$data['link'].'</strong>&nbsp;ke alamat browser yang anda gunakan untuk login ke client area.</p>

                                    <p>Terimakasih</p>';
                    } else {
                        $subject = $mailFormat->subject;
                        $body    = $mailFormat->description;
                        $body    = str_replace('[link]','<a href="'.$data['link'].'" target="_blank">',$body);
                        $body    = str_replace('[/link]','</a>',$body);
                        $body    = str_replace('[string-link]',$data['link'],$body);
                        
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                    }
                    
                    Email::factory($subject,$mesage,'text/html')               
                				->from($noreply,'MIFX')
                				->to($to)
                				->send();       
                    
                    //$_SESSION['submit-form-account'] = true;

                    App::$session->set('submit-form-account', true);
                    
                    //$message = "goTo:".$data['link'];
                    $message = 'success';   
                } else {
                    $message = '<ul class="error"><li>Unknown Error</li></ul>';
                }
                
                /** **/
            } elseif ($confirm['status'] == 'error') {
                $data = $confirm['data'];
                
                if ($confirm['status'] == 'error') {
                    $message = '<ul class="error">';
                    foreach ($data as $error) {
                        $message .= '<li>'.$error[0].'</li>';
                    }   
                    $message .= '</ul>'; 
                }    
            }
            
        }
        
        $this->response->body($message);   
    }
    
    
    
    public function action_seminar_registration() {
        if (empty($this->post)) 
        {
            $this->response->body('No Direct Access');
            return;    
        } 
        
        //Post
        $post   = $this->post; 
        
        /**
         * Conversion 
         * extract post
         */
        foreach($post as $key => $val){
            $element[$key]  = $val;
        }
        Conversion::record('Seminar Registration', $element); 
        /** end */ 
                   
        //Soap initialize
        Mifx::instance()->soap();
                
        //Get soap
        $client = Mifx::get('_soap');    
                         
        $register = $client->registerSeminar(array(
            'quantity' => $post->quantity,
            'gender'   => $post->seminargender,
            'nama'     => $post->seminarfirstname.' '.$post->seminarlastname,
            'telpon'   => $post->seminarphone,
            'email'    => $post->seminaremail,
            'kota'     => $post->seminarkota,
            'address'  => $post->seminaraddress,
            'seminar'  => array ($post->seminarid)
        ));
        
        if (empty($register['status'])) {
            $message = 'Unknown Error';
        } elseif ($register['status'] == 'success') {
            /** Check for citites **/
            $kota = $client->getKota();
            $post->seminarkota = isset($kota[$post->seminarkota]) ? $kota[$post->seminarkota] : $post->seminarkota;
            

            $noUrut = DB::select('*')->from('monex_schedules')->where('id', '=', $post->seminarid)->as_object()->execute()->current();
            $noUrut = (empty($noUrut->registration_user) ? 1 : $noUrut->registration_user + 1);
        
            DB::update('monex_schedules')->set(array('registration_user' => $noUrut))->where('id', '=', $post->seminarid)->execute();
           
            /** **/ 
            $subject = "Jadwal Seminar ".$post->seminarlabel;            
            $to      = $post->seminaremail; 
            $noreply = $this->noreply;     
            $mesage  = View::factory('education/email-seminar-registration')
                                ->set( 'subject',$subject )
                                ->set( 'post',$post )
                                ->render();
            
            //Mail to registrant   
            $mailer  = Email::factory($subject,$mesage,'text/html')               
        				->from($noreply,'MIFX')
        				->to($to)
                        ->send();
            
            //CC Email to MIFX Education 
            $subject = __('seminar-subject-mail', array(':no-urut' =>  $noUrut));
            // if ( stripos($post->seminarlabel,'mcs') !== false || stripos($post->seminarlabel,'Monex Corporate Seminar') ) {
            //     $subject = __('mcs-subject-mail');  
            // }
            $mesage  = View::factory('education/email-seminar-registration')
                                ->set( 'cc',true )
                                ->set( 'subject',$subject )
                                ->set( 'post',$post )
                                ->render();
            $ccmail  = Email::factory($subject,$mesage,'text/html')               
        				->from($noreply,'MIFX')
        				->to($this->ccCorporate);
                        
            //Check for MCS word or Monex Corporate Seminar
            // if ( stripos($post->seminarlabel,'mcs') !== false || stripos($post->seminarlabel,'Monex Corporate Seminar') ) {
            //     //CC Email to MIFX Corporate
            //     $ccmail->cc($this->ccCorporate);
            // }   
                                                 
            //Send cc email
            $ccmail->send();  
            
            $message = 'success';
            
        } elseif ($register['status'] == 'error') {
            
            $data    = $register['data'];
            
            $message = '<ul class="error">';
            
            foreach ($data as $error) {
                $message .= '<li>'.$error[0].'</li>';
            }
               
            $message .= '</ul>';
        }
        $this->response->body($message);                 
    }
    
    public function action_request_ebook () {
        if (empty($this->post)) 
        {
            $this->response->body('No Direct Access');
            return;    
        } 
        
        //Post
        $post   = $this->post;
        
        /**
         * Conversion 
         * extract post
         */
        foreach($post as $key => $val){
            $element[$key]  = $val;
        }
        Conversion::record('Request Ebook', $element); 
        /** end */ 

        //Valid captcha
        $isCaptcha = $post->captcha === App::$session->get('hiddenCaptcha'); 
        //$isCaptcha = true;
        
        if (!$isCaptcha) {
            $message = __('error-captcha');                
        } else {
            //Soap initialize
            Mifx::instance()->soap();
                    
            //Get soap
            $client = Mifx::get('_soap');    
            
            if (empty($post->accountreferral) || trim($post->accountreferral) === 'x' || $post->accountreferral == __('referral') )
                $post->accountreferral = 'X';
                
            $confirm = $client->registerEtc(array(
                        'kota'          => $post->accountkota,
                        'name'          => $post->accountname,
                        'phone'         => $post->accountphone,
                        'email'         => $post->accountemail,
                        'referal'       => $post->accountreferral,
                        //'deposit'       => $post->accountdeposit,
                        
                        'source'        => 22,
                        'type'          => 'e-book', 
                        'information'   => $post->accountinformation,
                        'cookie'		=> $_COOKIE['visit_id'],   
                        'reg_type_id'	=> 3,                         
                    ));
            
            if (empty($confirm['status']) || empty($post->ebook)) {
                $message = 'Unknown Error';
            } elseif ($confirm['status'] == 'success') {
                
                $data    = $confirm['data'];
                
                $ebook   = DB::select('label','url-full')->from('ebook')->where('permalink','=',$post->ebook)->as_object()->execute()->current();
                
                if (empty($ebook)) {
                    $message = 'Invalid E-BOOK';    
                } else {                    
                    /** **/ 
                    $url_full   = 'url-full'; 
                    $url_full   = strpos($ebook->$url_full,'http') === 0 ? $ebook->$url_full : URL::front('ebook/'.$ebook->$url_full);
                    
                    $mailFormat = Model::factory('Emailformat')->item('Download E-Book');
                    $mailFormat = Translate::item('email_formats',$mailFormat);
					$mailFormatAct = Model::factory('Emailformat')->item('Activation MIFX E-book');//activation mifx ebook
                    $mailFormatAct = Translate::item('email_formats',$mailFormatAct);
                    
                    $to      = $post->accountemail; 
                    $noreply = $this->noreply;
                        
                   /* edited by Hanna
					   email download ebook tidak perlu ada lagi
                    if (empty($mailFormat)) {
                        $subject = "Link Full Version E-BOOK ";
                        $mesage  = View::factory('account/email-ebook')
                                            ->set( 'subject',$subject )
                                            ->set( 'ebook',$ebook->label )
                                            ->set( 'url', $url_full )
                                            ->render();
                    } else {
                        $subject = $mailFormat->subject;
                        $body    = $mailFormat->description;
                        $body    = str_replace('[item]',$ebook->label,$body);
                        $body    = str_replace('[link]','<a href="'.$url_full.'" target="_blank">',$body);
                        $body    = str_replace('[/link]','</a>',$body);
                        $body    = str_replace('[string-link]',$url_full,$body);
                        
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                    }                                            
                    
                    Email::factory($subject,$mesage,'text/html')               
                				->from($noreply,'MIFX')
                				->to($to)
                				->send();  
                    */ 
					
					/* updated by Hanna
						pertama sekali mengecek terlebih dahulu apakah email format ada di admin-cp.
						apabila ada, maka format email akan diambil dari back end, namun apabila tidak ada
						maka akan diambil dari file view/account/email-verify-url
						
					*/
                                    
                    if (empty($mailFormatAct) ) {
                        $subject = "Link Activation MIFX E-book"; 
						$to      = $post->accountemail; 
                        $noreply = $this->noreply;
                        $mesage  = View::factory('account/email-verify-url')
                                            ->set( 'subject',$subject )
                                            ->set( 'post',$post )
                                            ->set( 'url', Translate::uri(Language::$current,'accounts/verify-ebook/'.$data['key']) )
                                            ->render();
					}else{
						$subject = "Link Activation MIFX E-Book";
						$to      = $post->accountemail; 
                        $noreply = $this->noreply;
                        $body    = $mailFormatAct->description;
                        $body    = str_replace('[user]',$post->accountname,$body);
                        $body    = str_replace('[link]',HTML::anchor(Translate::uri(Language::$current,'accounts/verify-ebook/'.$data['key']),'link ini',array('style'=>'text-decoration:none;color:blue;')),$body);
                        $body    = str_replace('[/link]','',$body);
                        $body    = str_replace('[link-string]',Translate::uri(Language::$current,'accounts/verify-ebook/'.$data['key']) ,$body);
                        
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body)
                                            ->render();
					}
					Email::factory($subject,$mesage,'text/html')               
                    				->from($noreply,'MIFX')
                    				->to($to)
                    				->send();
                    
                    
                    $message = 'success';
					
					/*
					if ( empty($data['link']) ) {
                        $subject = "Link Activation MIFX Live Account";            
                        $to      = $post->accountemail; 
                        $noreply = $this->noreply;
                        $mesage  = View::factory('account/email-verify-url')
                                            ->set( 'subject',$subject )
                                            ->set( 'post',$post )
                                            ->set( 'url', Translate::uri(Language::$current,'accounts/verify-live-account/'.$data['key']) )
                                            ->render();
                        
                        Email::factory($subject,$mesage,'text/html')               
                    				->from($noreply,'MIFX')
                    				->to($to)
                    				->send();
                    }
                    $message = 'success';
					*/
                }
            } elseif ($confirm['status'] == 'error') {
                
                $data    = $confirm['data'];
                
                $message = '<ul class="error">';
                
                foreach ($data as $error) {
                    $message .= '<li>'.$error[0].'</li>';
                }
                   
                $message .= '</ul>';
            }
            
        }
        
        $this->response->body($message);                       
    }
    
    public function action_seminar_registration_data(){
        
        //Post
        $post = $this->post;
        
        $message = View::factory('education/seminar-registration-data')->set('post', $post);
                
        $this->response->body($message);    
    }
    
	public function action_request_mte () {
        if (empty($this->post)) 
        {
            $this->response->body('No Direct Access');
            return;    
        } 
        
        //Post
        $post   = $this->post;
        
        /**
         * Conversion 
         * extract post
         */
        foreach($post as $key => $val){
            $element[$key]  = $val;
        }
        Conversion::record('Request MTE', $element); 
        /** end */ 

        //Valid captcha
        $isCaptcha = $post->captcha === App::$session->get('hiddenCaptcha'); 
        //$isCaptcha = true;
        
        if (!$isCaptcha) {
            $message = __('error-captcha');                
        } else {
            //Soap initialize
            Mifx::instance()->soap();
                   
            //Get soap
            $client = Mifx::get('_soap');    
            if (empty($post->accountreferral) || trim($post->accountreferral) === 'x' || $post->accountreferral == __('referral') )
                $post->accountreferral = 'X';
				
            $confirm = $client->registerEtc(array(
                        'kota'          => $post->accountkota,
                        'name'          => $post->accountname,
                        'phone'         => $post->accountphone,
                        'email'         => $post->accountemail,
                        'referal'       => $post->accountreferral,
                        //'deposit'       => $post->accountdeposit,
                        'source'        => 21,
                        'type'          => 'mte', 
                        'information'   => $post->accountinformation,
                        'cookie'		=> $_COOKIE['visit_id'],   
                        'reg_type_id'	=> 4,            					
                    ));
            
            if (empty($confirm['status']) /*|| empty($post->mte)*/) {
                $message = 'Unknown Error';
				
				//$message = var_export($confirm['data'],true);
				
            } elseif ($confirm['status'] == 'success') {
                $data    = $confirm['data'];  
				
				$to      = $post->accountemail; 
				$noreply = $this->noreply;
                    
				/* updated by Hanna*/
				
				$mailFormat = Model::factory('Emailformat')->item('Activation MIFX Demo Account');//activation MTE
				$mailFormat = Translate::item('email_formats',$mailFormat); 
					
				if (empty($mailFormat) ) {
					$subject = "Link Activation MIFX MTE"; 
					$to      = $post->accountemail; 
					$noreply = $this->noreply;
					$mesage  = View::factory('account/email-verify-url')
										->set( 'subject',$subject )
										->set( 'post',$post )
										->set( 'url', Translate::uri(Language::$current,'accounts/verify-mte/'.$data['key']) ) //ke class verify
										->render();
				}else{
					$subject = "Link Activation MIFX MTE";
					$to      = $post->accountemail; 
					$noreply = $this->noreply;
					$body    = $mailFormat->description;
					$body    = str_replace('[user]',$post->accountname,$body);
					$body    = str_replace('[type]','MTE',$body);
					$body    = str_replace('[link]',HTML::anchor(Translate::uri(Language::$current,'accounts/verify-mte/'.$data['key']),'link ini',array('style'=>'text-decoration:none;color:blue;')),$body);
					$body    = str_replace('[/link]','',$body);
					$body    = str_replace('[link-string]',Translate::uri(Language::$current,'accounts/verify-mte/'.$data['key']) ,$body);
					
					$mesage  = View::factory('general/html-email')
										->set( 'subject',$subject )
										->set( 'body',$body )
										->render();
				}
				Email::factory($subject,$mesage,'text/html')               
								->from($noreply,'MIFX')
								->to($to)
								->send();
				
				
				$message = 'Selamat <b>'.$post->accountname.', </b>registrasi MTE Anda berhasil. <br>Silakan cek email Anda dan ikuti petunjuknya untuk dapat melakukan aktivasi.';
				?><meta http-equiv="refresh" content="5;url=http://mifx.com/education/mte"><?php
            } elseif ($confirm['status'] == 'error') {
                
                $data    = $confirm['data'];
                
                $message = '<ul class="error">';
                
                foreach ($data as $error) {
                    $message .= '<li>'.$error[0].'</li>';
                }
                $message .= '</ul>';
            }
        }
        $this->response->body($message);                       
    }
	
    public function action_load_form()
    {
        //Soap initialize
        Mifx::instance()->soap();
        
        //Get soap
        $client = Mifx::get('_soap');
        
        if(empty($client)){
            echo("<script type=\"text/javascript\"> $(function(){ $('.homepage').remove(); });</script>"); die;
        }
        
        $type = $this->request->param('param1');
        $priv_text = $this->request->post('privaci');
        $priv_url  = $this->request->post('privaci_url');
        
        if (empty($priv_url))
        {
            Page::$instance->navigationCollector();
            $priv_text = Page::get('simpleBreadCrumb:kebijakan-privasi');
            $priv_url  = Page::get('completedNavigationUri:kebijakan-privasi');
        }
        
        $vars = array('privacy_text'=>$priv_text,'privacy_url'=>$priv_url);
        
        if (empty($type) || $type == 'account')
        {
            $view = 'general/frm-account-loader';    
        }
        else
        {
            $view = 'general/frm-account-mobile-loader';
        }
        
        $this->response->body(View::factory($view,$vars));
    }

    public function action_verified_email()
    {   
        /**
            Fungsi cek client sudah ada demo : hasDemo($email)
        
            Fungsi cek client sudah ada live: hasLive($email)
        
            Fungsi cek register via demo : regDemo($email)
        
            Fungsi cek register via live : regLive($email)
        
            Semua parameter menggunakan email dengan nilai return boolean. True jika ada dan False jika tidak ada.
        */
        //Soap initialize
        Mifx::instance()->soap();      
        //Get soap
        $client = Mifx::get('_soap');
        
        
        $email = $this->request->post('accountEmail');
        
        $post = new stdClass;
        
        $name = explode('@', $email);
        
        $post->accountname = $name[0];
        
        $type = "";
        $message = "";
        
        
        /**
        
        if($client->hasLive($email)){
            $type = 'Live';
            $message    = 'Your email has been verified !';
        }else {
            $type = 'Live';
            $message    = 'Your email has been verified !';
        }
        
        */
        
                
        if($client->hasDemo($email) || $client->hasLive($email)){
            $message    = 'Your email has been verified !';
            $type       = 'Demo';
        }else{
            $message    = 'Please check your email to verify your account !';
            $type       = 'Demo'; 
        } 
                
        $key = $client->getKey($email);
        
        echo $message;
        
        if(!empty($key)){
        
            $subject = "Link Activation MIFX Live Account";            
            $to      = $email; 
            $noreply = $this->noreply;
            $mesage  = View::factory('account/email-verify-url')
                                ->set( 'subject',$subject )
                                ->set( 'post',$post )
                                ->set( 'type', $type )
                                ->set( 'url', Translate::uri(Language::$current,'accounts/verify-demo-account/'. $client->getKey($email) ))
                                ->render();
            
            Email::factory($subject,$mesage,'text/html')               
                        ->from($noreply,'MIFX')
                        ->to($to)
                        ->send();
        }
    }

    public function action_load_form_account()
    {
        //Soap initialize
        Mifx::instance()->soap();
        
        //Get soap
        $client = Mifx::get('_soap');
        
        $section = $this->request->param('param1');
        $codeReferal = $this->request->post('code_referal');
        $regularOrPartial  = $this->request->post('regular_or_partial');
        
        $this->response->body( Mifx::accountForm($section, $codeReferal, $regularOrPartial) );
    }
}
