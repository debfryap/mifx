<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Home_Site_Welcome extends Controller_Default_Template_Site {
    
    public function before() {
        $this->layout = 'home';
        
        parent::before();
        
        //$this->scripts('http://quotes.monexnews.com:8000/socket.io/socket.io.js');    //Moved on window load
        $this->styles(URL::templateCss('home-form'));
        
        App::$config->menu->active = 'home';       
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap(); 
    }
    
    public function action_index () {
        /**
         * Get Seminar Schedule From SOAP
         */
        //$mifx = Mifx::get('_soap');
        //$schs = $mifx->getSeminar();
        
        $this->layout
             ->set('articles',Model::factory('News_List')->feature(3))
             //->set('soap_schedules',$schs)
             ->set('schedules',Model::factory('News_Monexschedule')->feature(1))
             ->set('welcome',Model::factory('Page')->navigation_page('selamat-datang-di-monex'))
             ->set('banners',Model::factory('Medialibrary')->simple_application('home-banner',1,'site_navigations'));
    }
}