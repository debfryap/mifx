<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Education_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section;
    
    private $item;
    
    private $content;
    
    private $footer;
    
    private $page;

    private $perpage;
    
    public function before() {
        
        $this->layout = 'education';        
        
        parent::before();
        
        $this->section = $this->request->param('section1');
        
        $this->item    = $this->request->param('section2');  
        
        //Append media validate
        $this->media_validate(true);
        
        $this->perpage = 3;
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
        
        //Default content 
        $this->page    = Model::factory('Page')->navigation_page();
        $this->content = View::factory('layout/common-page')->set('page',$this->page);
        
        $this->content_handling();
        
        $this->layout
             ->set('live',true)
             ->set('showNavigation',!empty($this->section))
             ->set('widget',empty($this->section))
             ->set('content',$this->content)
             ->set('footer',$this->footer);
    }
    
    private function content_handling() {
        switch ($this->section) {
            case "": case null:
                $this->home(); break;
            case "seminar-training":
                $this->seminar_training(); break;
            case "video" :
                $this->video(); break;
            case "e-book" :
                $this->ebook(); break;
            case "belajar-trading-101-bersama-monex" :
                $this->belajar_trading(); break;   
			case "mte" :
                $this->mte(); break;
            default:
                break;
        }
    }
    
    private function home() {
        //Monex Schedule
        $title          = __('jadwal-monex-edukasi',array(':tag'=>'span'));
        
        $this->content .= '<div class="schedul_edu"><h3>' . $title . '</h3>';
        
        $schedules      = Model::factory('News_Monexschedule')->feature(2);
        
        if (!empty($schedules)) {
            $schedules      = count($schedules) == 1 ? array($schedules) : $schedules;
            
            $this->content .= '<ul>';
            
            foreach ($schedules as  $i => $sch) { 
                $sch = Translate::item('monex_schedules',$sch);
                $this->content .= '<li>';
                //$this->content .= HTML::anchor( Page::get('completedNavigationUri:jadwal-monex-edukasi' . '/' .$sch->permalink), $sch->label);
                $this->content .= $sch->label;
                $this->content .= '<span>' . Page::datetime($sch->date,$sch->start,$sch->region) . '</span>';
                $this->content .= '</li>';
            }
            
            $this->content .= '</ul>';           
            $this->content .= '<p>'.( HTML::anchor( Page::get('completedNavigationUri:seminar-training'),  __('btn-read-more') ) ) .'</p>';
            $this->content .= '</div>';     
        }
        
        $this->footer = View::factory('education/footer')
                                    ->set('ebook',Model::factory('Page')->navigation_page('e-book'))
                                    ->set('videoItem',Model::factory('News_Monexvideo')->feature(1))
                                    ->set('videoDesc',Model::factory('Page')->navigation_page('video'))
                                    ->set('articles',empty($this->section) ? Model::factory('News_List')->feature(4,array('news-thumbnail')) : null);
          
        $this->layout
             ->set('leftBanners',Model::factory('Medialibrary')->simple_application('page-left-bnnr',!empty($this->page->id) ? $this->page->id : 0,'site_navigation_pages'));
    }
    
    private function seminar_training() {
        if (empty($this->item)) {
            //$this->content .= View::factory('education/seminar-list')->set('soap',true); 
            
            $this->content .= Mifx::instance()->list_schedules();
        } else {
            $this->layout->set('breadcrumb',Page::breadCrumb('full','as-parent-seminar-training') . 'Register');
            
            $this->content = View::factory('education/seminar-register')
                    ->set('item',Model::factory('News_Monexschedule')->item($this->item) )
                    ->set('post',$this->request->post());
        }   
    }
    
    private function video() {
        if (empty($this->item))
            $bigVideo      = Model::factory('News_Monexvideo')->latest(1);
        else
            $bigVideo      = Model::factory('News_Monexvideo')->item($this->item);
            
        
        $otherVideos   = !empty($bigVideo) ? Model::factory('News_Monexvideo')->others($this->item) : null;
        
        $this->content = View::factory('education/video')
                                ->set('title',Page::get('simpleBreadCrumb:video'))
                                ->set('current',$bigVideo)
                                ->set('others',$otherVideos)
                                ->set('tools',Page::searchNavigationChild(Page::get('navigationObjectComplete'),'video'))
                                ->render();
    }
    
   private function ebook() {

        $page = Model::factory('Page')->navigation_page('e-book');
		$this->item    = $this->request->param('section2');  
		/*updated by Hanna
		  untuk menambahkan parameter kode sales pada url mifx/ebook*/
		$item = Model::factory('Ebook')->current_ebook($this->item);
		
		if(!empty($this->item)){
			if($item == null){ 
				 //Get soap
				$client = Mifx::get('_soap');

				// Check Referral
				ini_set('default_socket_timeout', 600);

				$referral = $client->getMarketing($this->item);
				if ($referral != 'Invalid Code') {
					App::$session->set('referral_account',$referral);
					$this->item = null;
				 }
				 else{
					$referral = '';
				 }
			 }
			else{
				//jika yg di post tidak sama dengan session maka digantikan sessionnya
				if($this->item != App::$session->get('referral_account'))
				{
					$client = Mifx::get('_soap');
					// Check Referral
					ini_set('default_socket_timeout', 600);

					$referral = $client->getMarketing($this->item);
					if ($referral != 'Invalid Code') {
						App::$session->set('referral_account',$referral);
						$this->item = null;
					 }
					 else{
						$referral = '';
						App::$session->set('referral_account',$referral);
					 }
				}
			}
		}
		Cookie::set("cekreferal", Model::factory('Ebook')->current_ebook($this->item));
		$this->content = View::factory('layout/ebook-page')
                                    ->set('page', $page)
                                    ->set('current_ebook', Model::factory('Ebook')->current_ebook($this->item))
                                    ->set('slider_ebook', Model::factory('Ebook')->all_ebook())
                                    ->render();
    }

    private function belajar_trading() {
        
       
        if (empty($this->item))
            $bigVideo      = Model::factory('News_Belajartrading')->latest(1);
        else
            $bigVideo      = Model::factory('News_Belajartrading')->item($this->item);
    
        /*
          paramater dari function listing di ganti jadi $this->perpage karna di function listing parameter itu
          digunakan untuk paging yang digunakan untuk setiap halaman
        */
        $otherVideos   = !empty($bigVideo) ? Model::factory('News_Belajartrading')->listing() : null;
        
      
        $this->content = View::factory('education/belajar-trading')
                                ->set('title',Page::get('simpleBreadCrumb:video'))
                                ->set('current',$bigVideo)
                                /* 
                                  di $otherVideos ada dua array list dan paging, 
                                  untuk data video pake array "list" dan untuk paging pake array "paging"
                                */ 
                                ->set('others',$otherVideos) 
                                ->set('othersMt4', Model::factory('News_Monexvideo')->xdata()->execute())
                                ->set('page',Model::factory('Page')->navigation_page('belajar-trading-101-bersama-monex'))
                                ->set('tools',Page::searchNavigationChild(Page::get('navigationObjectComplete'),'video'))
                                ->render();
    }

    public function action_101()
    {
        $utm_default = '?utm_source=101Newspaper&utm_medium=Newspaper&utm_campaign=Offline';
        
        if(!empty($this->section)){
          if($this->section == 'tempo')$utm_default = '?utm_source=101Tempo&utm_medium=MajalahTempo&utm_campaign=Offline';  
            
          if($this->section == 'fm') $utm_default = '?utm_source=101FM&utm_medium=MajalahFM&utm_campaign=Offline';
          
          if($this->section == 'passfm') $utm_default = '?utm_source=101Passfm&utm_medium=TalkshowRadio&utm_campaign=Offline';
        } 
        
        //Redirect
        $this->redirect(Page::get('completedNavigationUri:belajar-trading-101-bersama-monex').$utm_default);
    }
    
	/*
    public function action_mte()
    {
        $utm_default = '?utm_source=MTEofflineAd&utm_medium=OtherOffline&utm_campaign=Edu-MTE';
        
        if(!empty($this->section)){
            if($this->section == 'id') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=InsvestorDaily&utm_campaign=Edu-MTE';
            if($this->section == 'fm') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=FuturesMonthly&utm_campaign=Edu-MTE';
            if($this->section == 'hic') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=HarianIndonesiaCH&utm_campaign=Edu-MTE';
            if($this->section == 'mi') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=MediaIndonesia&utm_campaign=Edu-MTE';
            if($this->section == 'ic') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=InClover&utm_campaign=Edu-MTE';
        }
        
        //Redirect
        $this->redirect(Page::get('completedNavigationUri:mte').$utm_default);
    }
    */
	private function mte()
    {
		$page = Model::factory('Page')->navigation_page('mte');
		
		/*updated by Hanna
		  untuk menambahkan parameter kode sales pada url mifx/mte*/
		if(!empty($this->item)){
				 //Get soap
				 $client = Mifx::get('_soap');
				 // Check Referral
				 ini_set('default_socket_timeout', 600);
				 $referral = $client->getMarketing($this->item);
				 if ($referral != 'Invalid Code') {
					 App::$session->set('referral_account',$referral);
					 $this->item = null;
				 }
			 } 
		 
        
        $this->content = View::factory('layout/common-page')
                                    ->set('page', $page)
                                  ->render();
		
		/*
        $utm_default = '?utm_source=MTEofflineAd&utm_medium=OtherOffline&utm_campaign=Edu-MTE';
        
        if(!empty($this->section)){
            if($this->section == 'id') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=InsvestorDaily&utm_campaign=Edu-MTE';
            if($this->section == 'fm') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=FuturesMonthly&utm_campaign=Edu-MTE';
            if($this->section == 'hic') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=HarianIndonesiaCH&utm_campaign=Edu-MTE';
            if($this->section == 'mi') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=MediaIndonesia&utm_campaign=Edu-MTE';
            if($this->section == 'ic') $utm_default = '?utm_source=MTE-offlineAd&utm_medium=InClover&utm_campaign=Edu-MTE';
        }
        
        //Redirect
        $this->redirect(Page::get('completedNavigationUri:mte').$utm_default);
		*/
    }	
}