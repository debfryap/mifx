<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Education_Site_LearningCenter extends Controller_Default_Template_Site {
    
    private $section;
    
    private $item;
    
    private $filter;
    
    private $content;
    
    public function before() {
        
        $this->layout = 'education';        
        
        parent::before();
        
        $this->section = $this->request->param('section'); $this->section = strtolower($this->section);        
        
        $this->item    = $this->request->param('item');
        
        $this->filter  = $this->request->param('filter');
        
        $this->layout->set('live',true);
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
        
        $sysMethods = array('tutorial','investment-clinic','trading-glossary','artikel');
        
        if (empty($this->section)) {
            foreach (Page::get('navigationObject') as $n) {
                if ($n->permalink != 'education') continue;
                if (empty($n->child)) break;
                foreach ($n->child as $n) {
                    if ($n->permalink != 'learning-center') continue;
                    if (empty($n->child)) break;
                    foreach ($n->child as $n) {
                        $this->section = $n->permalink; 
                        break;
                    } 
                    break;
                }
            }
        }
                        
        $call = Helper_Kernel::name($this->section,'_');                   
        $call = in_array($this->section,$sysMethods) && method_exists($this,"section_$call") ? "section_$call" : "section_general";
                
        //Set breadcrumb
        $this->layout
             ->set('showNavigation',$this->section)
             ->set('breadcrumb',Page::breadCrumb('full',$this->section));
                             
        $this->$call();
    }
        
        private function section_general() {
            //Default content
            $this->content = View::factory('layout/common-page')->set('page',Model::factory('Page')->navigation_page());
            
            $this->layout
                 ->set('widget',true)
                 ->set('content',$this->content);    
        }
    
        private function section_trading_glossary() {
            $lowerItem = strtolower($this->item);
            if ( empty($this->item) || ( ($lowerItem == 'filter' || $lowerItem == 'search') && !empty($this->filter)) ) {
                $this->layout
                     ->set('content',Mifx::instance()->list_glossary());
            } else {
                $glossary = DB::select()->from(array('glossaries','g'))->where('g.is_active','=',1)->and_where('g.permalink','=',$this->item)->as_object()->execute()->current();
        
                $alpha = DB::select(DB::expr('SUBSTR(`g`.`label`,1,1) AS `alpha`, `id`, `label` '))->from(array('glossaries','g'))->where('g.is_active','=',1)->group_by('alpha')->as_object()->execute();
                
                // redirect 404
                if(empty($glossary)) $this->redirect(URL::front('errors/404'));
                
                //set meta
                App::$config->meta->keywords = !empty($glossary->meta_title) ? $glossary->meta_title : $glossary->label;
                App::$config->meta->title = !empty($glossary->meta_title) ? $glossary->meta_title : $glossary->label;
                App::$config->meta->description = !empty($glossary->meta_description) ? $glossary->meta_description : Text::limit_chars(strip_tags($glossary->description), 160,"", true );
                
                $this->layout
                     ->set( 'content', View::factory('education/glossary-detail')->set('glossary',$glossary)->set('alpha',$alpha) );
            }
        }
    
        private function section_artikel() {
            if (empty($this->item)) {
                $this->layout
                     ->set('breadcrumb',Page::breadCrumb('full',$this->section))
                     ->set('content',Mifx::instance()->list_article());    
            } else {
                $article = Model::factory('News_List')->item($this->item,array('updates-banner'));
                
                // set meta
                App::$config->meta->keywords    = !empty($article->meta_title) ? $article->meta_title : $article->label;
                App::$config->meta->description = !empty($article->meta_description) ?  $article->meta_description : Text::limit_chars(strip_tags($article->description), 160,"", true );
                App::$config->meta->title       = !empty($article->meta_title) ?  $article->meta_title : $article->label;

                // redirect 404
                if(empty($article)) $this->redirect(URL::front('errors/404'));

                if($this->item =="sitemap"){
                    $content = '<h2 class="title_page">'.$article->label.'</h2>';
                    $this->layout->set( 'content', $content . Mifx::sitemap(Page::get('navigationObjectComplete')) )
                                 ->set('breadcrumb',Page::breadCrumb('full',"as-parent-$this->section") . (empty($article) ? $this->item : $article->label)); 
                }else{
                    $this->register('translate_url',array('type'=>'article','data'=>$article));                    
                    $this->layout
                         ->set('breadcrumb',Page::breadCrumb('full',"as-parent-$this->section") . (empty($article) ? $this->item : $article->label))
                         ->set('content',View::factory('education/artikel-detail')->set('article',$article));
                }
            }
        }
    
        private function section_investment_clinic() {
            
            $chapters = Data::traversing(false)
                            ->__set('db',Model::factory('Investmentclinic')->select('id','label','permalink')->from(array('investment_clinics','ic'))->where('ic.is_active','=',1))
                            ->__set('order_by','ic.ordering')
                            ->__set('column_traverse','ic.parent_id')
                            ->__set('next_column_traverse_value','id')
                            ->__set('traverse_start',0)
                            ->response();
                            
            $invesmentClinic = View::factory('education/online-ebook')
                                ->set('title',Page::get('simpleBreadCrumb:investment-clinic'))
                                ->set('list',$this->dropdownMenu('investment_clinics',Page::get('completedNavigationUri:normal-investment-clinic'),$chapters))
                                ->set('item',$this->getDescription('investment_clinics'));
                                
            $this->layout
                 ->set('breadcrumb',Page::breadCrumb('investment-clinic')) 
                 ->set('content',$invesmentClinic);    
        }
    
        /** Tutorial Section **/
        private function section_tutorial() {
            
            $chapters = Data::traversing(false)
                            ->__set('db',Model::factory('Tutorial')->select('id','label','permalink')->from(array('tutorials','t'))->where('t.is_active','=',1))
                            ->__set('order_by','t.ordering')
                            ->__set('column_traverse','t.parent_id')
                            ->__set('next_column_traverse_value','id')
                            ->__set('traverse_start',0)
                            ->response();
                            
            $tutorial = View::factory('education/online-ebook')
                                ->set('title',Page::get('simpleBreadCrumb:tutorial'))
                                ->set('list',$this->dropdownMenu('tutorials',Page::get('completedNavigationUri:normal-tutorial'),$chapters))
                                ->set('item',$this->getDescription('tutorials'));
                                
            $this->layout
                 ->set('breadcrumb',Page::breadCrumb('tutorial')) 
                 ->set('content',$tutorial);    
        }
        
            private function getDescription($table) {
                $preGet = DB::select('id','parent_id','ordering','label','description')
                            ->from($table)
                            ->where('is_active','=',1)
                            ->as_object();
                
                if (empty($this->item)) {
                    $item = $preGet->and_where('description','is not',null)->order_by('parent_id')->order_by('ordering')->execute()->current();
                } else {
                    $a = $b = explode('/',trim($this->item));
                    $e = array_pop($b);
                    
                    //Clone preGet, still need it later
                    $item = clone $preGet;
                    
                    //Limit it up to 2 mean need to check for multiple item with same permalink 
                    $item = $item->and_where('permalink','=',$e)->limit(2)->execute();
                    
                    //Yes it is multiple already, and should get only one :) on parent based id
                    if ($item->valid() && count($item) > 1) {
                        foreach ($a as $i) {
                            $gp = DB::select('id')->from($table)->where('permalink','=',$i)->and_where('parent_id','=',!isset($p) ? 0 : $p);                            
                            $p  = $gp->limit(1)->execute()->get('id');
                        }
                        $item = $preGet->and_where('id','=',$p)->order_by('parent_id')->order_by('ordering')->execute();
                    }
                    
                    
                    $item = $item->current();
                    
                    //There is no description huh ....
                    if (empty($item->description)) {
                        $child = $this->activeChild($item->id,$table);
                        
                        //Still no description ... arggggghhhh
                        if (empty($child)) {
                            $child = $this->grandChild($item->id, $table);    
                        }
                        
                        if (!empty($child)) {
                            $item = $child; 
                        }
                    }                                         
                }
                
                if (empty($item)) return null;
                
                $item = Translate::item($table, $item);
                
                //Set previous link
                $prev = $this->prevNextMenu($item,$table,'prev');
                $item->prev = empty($prev) ? '' : HTML::anchor( $prev, __('previous-tutorial'), array('class'=>'prev_art') );
                
                //Set next link
                $next = $this->prevNextMenu($item,$table,'next');
                $item->next = empty($next) ? '' : HTML::anchor( $next, __('next-tutorial'), array('class'=>'next_art') );
                
                return $item;
            }
            
            private function activeChild($parent,$table) {
                $g = DB::select('id','parent_id','ordering','label','description')
                            ->from($table)
                            ->where('is_active','=',1)
                            ->and_where('parent_id','=',$parent)
                            ->order_by('ordering')
                            ->as_object()
                            ->execute();
                if (empty($g))
                {
                    return null;
                }
                else 
                {
                    foreach ($g as $i) {
                        if (!empty($i->description))
                        {
                            return $i;
                        }
                        else
                        {
                            $this->activeChild($i->id, $table);
                        }    
                    }    
                }                                                 
            }
            
            private function grandChild($parent,$table) {
                $g = DB::select('id','parent_id','ordering','label','description')
                            ->from($table)
                            ->where('is_active','=',1)
                            ->and_where('parent_id','=',$parent)
                            ->order_by('ordering')
                            ->as_object()
                            ->execute();
                if (empty($g))
                {
                    return null;
                }
                else 
                {
                    foreach ($g as $i) {
                        $c = $this->grandChild($i->id, $table);
                        if (empty($c)) {
                            return $i;
                        }
                    }    
                }
                
            }
            
            private function prevNextMenu($item, $table, $dir, $level = 0) {
                if (empty($item)) return null;
                                
                if ($dir == 'next' && $level == 0) 
                {
                    //Check for child
                    $g = DB::select('id')
                                ->from($table)
                                ->where('parent_id','=',$item->id)
                                ->order_by('ordering')
                                ->limit(1)
                                ->as_object()
                                ->execute()
                                ->current();
                                
                    if (!empty($g))
                    {
                        $link = Mifx::get("contentCrumb:$table");
                        return empty($link[$g->id]) ? null : $link[$g->id];
                    }
                }
                
                //Check for sibling
                $g = DB::select('id')
                            ->from($table)
                            ->where('parent_id','=',$item->parent_id)
                            ->and_where('ordering',$dir == 'prev' ? '<' : '>',$item->ordering)
                            ->order_by('ordering',$dir == 'prev' ? 'desc' : 'asc')
                            ->limit(1)
                            ->as_object()
                            ->execute()
                            ->current();
                
                if (!empty($g))
                {
                    $link = Mifx::get("contentCrumb:$table");
                    return empty($link[$g->id]) ? null : $link[$g->id];
                } 
                else 
                {
                    //Check for parent sibling
                    $p = DB::select('id','parent_id','ordering','description')
                                ->from($table)
                                ->where('id','=',$item->parent_id)
                                ->limit(1)
                                ->as_object()
                                ->execute()
                                ->current();
                                                                                    
                    if (!empty($p)) {
                        if ($dir == 'prev' && !empty($p->description)) {                            
                            $link = Mifx::get("contentCrumb:$table");
                            return empty($link[$p->id]) ? null : $link[$p->id]; 
                        } else {
                            return $this->prevNextMenu($p,$table,$dir,$level+1);
                        }
                    }    
                }
                
                return null;
            }
        
            private function dropdownMenu($table,$link,$list,$parent = 0) {
                if (empty($list)) return null;
                
                $html = '<ul>';
                
                foreach ($list as $i) {
                    $i     = Translate::item($table,$i);
                    
                    $html .= '<li'.(!empty($i->child) ? ' class="parent"' : '').'>';
                    
                    $plink = $link . '/' . $i->permalink;
                    
                    
                    //Content breadcrumb check
                    $crumb = Mifx::instance()->get("contentCrumb:$table");
                    if (empty($crumb)) {
                        $crumb = array($i->id => $plink);
                    } else {
                        $crumb[$i->id] = $plink;
                    }
                     
                    Mifx::instance()->set("contentCrumb:$table",$crumb);
                    
                    $html .= HTML::anchor($plink,$i->label);
                    
                    if (!empty($i->child)) $html .= $this->dropdownMenu($table,$plink,$i->child,$parent + 1);
                    
                    $html .= '</li>';                
                }    
                
                return $html.'</ul>';
            } 
}