<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Onlinetrading_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section;
    
    public function before() {    
        
        $this->section = $this->request->param('section1'); 
        
        if(empty($this->section))
            $this->layout = 'online-trading';
        else
            $this->layout = 'two-column'; 
            
        parent::before(); 
        
           
        
        App::$config->menu->active = 'online-trading';
        
        $this->styles(URL::templateCss('online-trading.css'));      
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }

    public function action_index () {
        if (!empty($this->section)) {
            $this->layout
             ->set('live',true)
             ->set('showNavigation',true);

            if($this->section == "pertanyaan-yang-paling-sering-diajukan") {
                $content = View::factory('trading-product/form-page')
                                ->set('title',Page::get('simpleBreadCrumb:pertanyaan-yang-paling-sering-diajukan'))
                                ->set('pages',Page::getPages($this->section));
                
                $this->layout->set('content', $content);
            }else{
                
                $page = Model::factory('Page')->navigation_page_array();
                 
                 // redirect 404
                 if(empty($page)) $this->redirect(URL::front('errors/404'));
                 $this->layout
                     ->set('content',View::factory('layout/common-page')->set('page',Model::factory('Page')->navigation_page_array()));
            }
        }
    }
}