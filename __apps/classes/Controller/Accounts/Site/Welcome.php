<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Accounts_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section = null;
    
    private $item = null;
    
    private $page;

    private $title;
    
    public function before() { 
        $this->layout = 'account';
        
        parent::before();
        
        // add script js
        $this->scripts(URL::templateJs('responsive-images/jquery_matchmedia', 'responsive-images/jquery_picturefill'));
        
        App::$config->menu->active = 'accounts';
        
        $this->section = $this->request->param('section1');
        
        //$this->item   = $this->request->param('section2') !="" ? $this->request->param('section2') : Session::instance()->get('referral_account');
        $this->item   = $this->request->param('section2') !="" ? $this->request->param('section2') : Cookie::get('key_referral', null);
        
        $this->title   = empty($this->section) ? Page::get('simpleBreadCrumb:accounts') : Page::get('simpleBreadCrumb:'.$this->section);
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
        
        if ($this->section == 'open-live-account.php') {
            $this->page = Page::getPages( $this->section  , 'banner-account' );
            $this->section = 'create-monex-live-account';
        }
        
        if ($this->section == 'open-demo-account.php') {
            $this->page = Page::getPages( $this->section  , 'banner-account' );
            $this->section = 'create-free-demo-account';
        }
       
        if ( $this->section == 'create-monex-live-account' 
                OR $this->section == 'create-free-demo-account' 
                OR $this->section == 'create-monex-live-account-success' 
                OR $this->section == 'create-free-demo-account-success' ) {
            
            // Regular or Partial
            $regularOrPartial = $this->request->param('section3');        
                    
            //Disable banner
            $this->register('banner',false);    
            
            //Get page's
            if ( $this->section == 'create-monex-live-account' OR $this->section == 'create-free-demo-account' ) 
            {
                //$this->page = Page::getPages( $this->section  , 'banner-account' );
                
                //Set referral
                $this->layout->set('codeReferal',$this->item);
                
                $this->layout->set('regularOrPartial',$regularOrPartial);
            }
            else
            {
                $this->page = new stdClass;
                                
                $this->page->id = 24;
                                
                if ( $this->section == 'create-monex-live-account-success')
                {
                    $block             = "Live";
                    
                    $this->title = $this->page->title = Page::get("simpleBreadCrumb:create-monex-live-account");
                    
                    $this->layout->set('breadcrumb',Page::breadCrumb('create-monex-live-account'));
                }
                else
                {
                    $block             = "Demo";
                    
                    $this->title = $this->page->title = Page::get("simpleBreadCrumb:create-free-demo-account");
                    
                    $this->layout->set('breadcrumb',Page::breadCrumb('create-free-demo-account'));
                }

                // get session
                $submit_form = Session::instance()->get('submit-form-account');
                
                if($submit_form == true){
                   App::$session->set('submit-form-account', false);
               } else {        
                   echo '<script>window.location="http://mifx.com";</script>';
               }
                
                $this->layout->set('breadcrumb',Page::breadCrumb('accounts'));
                $this->title = 'Account';
                
                $this->page->description = View::factory('account/registration/thank-you-registering-page')
                                                    ->set('type',$block)
                                                    ->set('page', Model::factory('Page')->navigation_page('accounts'));  
                   
               
            }
        } else {
            //Get single page
            $this->page = Model::factory('Page')->navigation_page(empty($this->section) ? 'accounts' : $this->section,'banner-account');
        }
    }
    
    public function action_index () {

        $this->layout
             ->set('title',$this->title)
             ->set('section',$this->section)
             ->set('page',$this->page); 
        
        if (empty($this->section))
            $this->layout
                 ->set('widgets',Page::searchNavigationChild(Page::get('navigationObjectComplete'),'accounts'));
        
        if ($this->section == 'verify-status') 
        {
            $this->verify_status();
        }
    }
    
    protected function verify_status() {
        
        $page = new stdClass();
        $page->id          = 24;
        $page->title       = App::$session->get('verify-title');
        $page->description = App::$session->get('verify-description');
        
        if (empty($page->title))
        { 
            $page->title = 'Error 404';
            $page->description = 'No direct script access';
        } else {
            if (__('verified-normal-error-title') != $page->title) {
                $type ='Demo';
				if(App::$session->get('verify-type') == 'e-book')
				{
					$type = 'e-book';
				}elseif(App::$session->get('verify-type') == 'mte')
				{
					$type = 'mte';
				}
                $page->title = "";
                $page->description .= View::factory('account/registration/verify-status')
                                                    ->set('type',$type)
                                                    ->set('page', Model::factory('Page')->navigation_page('accounts'));
            }
        }
        
        $this->layout
             ->set('breadcrumb', Page::breadCrumb('accounts'))
             ->set('title', 'Account')
             ->set('page', $page);
        
        //Unset
        App::$session->set('verify-title',null);
        App::$session->set('verify-description',null);
    }
    
    /****/
}