<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Accounts_Site_Ninestars extends Controller_Default_Template_Site {
    
    private $group;
    
    private $_groups;
    
    public function before() { 
        $this->layout = 'one-column';
        
        parent::before();
        
        App::$config->menu->active = 'accounts';
        
        $this->group = $this->request->param('group');
                
        $this->_groups = array (
            'demosinarmas'  => array (
                'name'      => 'Sinar Mas',
                'email'     => 'support@sinarmasfutures.co.id'
            ),
            'demoATPF'      => array (
                'name'      => 'ATPF',
                'email'     => 'admin@asiatpf.com'
            ),
            'demoimf'       => array (
                'name'      => 'Demo IMF',
                'email'     => 'support@internationalmitrafutures.com'
            ),
            'demomonex'     => array (
                'name'      => 'CIKINI',
                'email'     => 'cikini@mifx.com'
            ),
            'demoninestars' => array (
                'name'      => 'Nine Stars',
                'email'     => 'admin@ninestars.com'
            ),
            'demomonexB'     => array (
                'name'      => 'Monex B',
                'email'     => 'sales.bintaro@mifx.com'
            ),
            'kontes'        => array (
                'name'      => 'Kontes',
                'email'     => 'kompetisi.monexaa@gmail.com',//'sales.denpasar@mifx.com'
            ),
            'kontes2'        => array (
                'name'      => 'Kontes2',
                'email'     => '',
            )			
        );
    }
    
    public function action_home () {   
        
        //Append media validate
        $this->media_validate(true);        
        
        if (empty($this->group) || empty($this->_groups[$this->group])) {
            $this->layout->set('content','<div class="container">No direct access</div>');
            return;
        }
        
        $message = null;
        
        $group_name = $this->_groups[$this->group]['name'];
        
        if (!empty($this->post)) {
            if (!empty($this->post->captcha))
                $captcha = Captcha::valid($this->post->captcha);
            else
                $captcha = false;
            
            if ($captcha === true) {
                //Soap initialize
                Mifx::instance()->soap();
                        
                //Get soap
                $client = Mifx::get('_soap');    
                
                $post          = $this->_groups[$this->group];
                $post['group'] = $this->group;

                if ($group_name=='Kontes' || $group_name=='Kontes2'){
                    $post['name']  = $_POST['accountName'];
                    $post['phone']  = $_POST['accountPhone'];
                    $post['email']  = $_POST['accountEmail'];
                    $post['deposit']  = $_POST['accountDeposit'];
                    //$comm   = $client->freeKontes($post);
                }//else{    
                   $comm   = $client->freeDemo($post);
                //}
                $html   = '<div class="container">';
                $html  .= '<h3>DEMO REGISTRASI '. (strtoupper($group_name)) .'</h3>';
                
                if ( empty($comm) || empty($comm['status']) || $comm['status'] != 'success' ) {
                    $html .= __('error-nine-stars',array(':group'=>$group_name));
                } else {
                    $username = $password = '';
                    
                    if (!empty($comm) && is_array($comm) && !empty($comm['message']) && is_array($comm['message']) && !empty($comm['message']['no_account'])) {
                        $username = $comm['message']['no_account'];
                    }
                    
                    if (!empty($comm) && is_array($comm) && !empty($comm['message']) && is_array($comm['message']) && !empty($comm['message']['no_account'])) {
                        $password = $comm['message']['password'];
                    }
                    
                    $html .= __('thank-you-nine-stars',array(
                                ':username' => $username,
                                ':password' => $password
                            ));    

                    if ($group_name=='Kontes'){
                        // send email
                        $subject = 'Kontes Registration';
                        $body = "<table>
                            <tr>
                                <td>Name</td>
                                <td>: ".$_POST['accountName']."</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>: ".$_POST['accountPhone']."</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>: ".$_POST['accountEmail']."</td>
                            </tr>
                            <tr>
                                <td>Deposit</td>
                                <td>: ".$_POST['accountDeposit']."</td>
                            </tr>
                            <tr>
                                <td>No.Account</td>
                                <td>: {$username}</td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td>: {$password}</td>
                            </tr>
                        </table>";
                        $to = 'sales.denpasar@mifx.com';
                        //$to = 'kompetisi.monexaa@gmail.com';
						$to = 'democom.mifxsby@gmail.com';
                        $noreply = 'noreply@mifx.com';
                        //@mail($to,$subject,$body);
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                
                        Email::factory($subject,$mesage,'text/html')               
                                    ->from($noreply,'Monex Investindo Futures')
                                    ->to($to)
                                    ->send();
                        						
                        // masukan ke database
                        $query = DB::insert('kontes', array(
                            'name', 
                            'phone', 
                            'email', 
                            'deposit', 
                            'no_account', 
                            'password',
                            'date')); // create sql request
                        $query->values(array(
                            $_POST['accountName'], 
                            $_POST['accountPhone'], 
                            $_POST['accountEmail'], 
                            $_POST['accountDeposit'], 
                            $username, 
                            $password,
                            date('Y-m-d H:i:s')
                                        )); // do it in loop
                        $query->execute(); // execute

                

                        $html = '<div class="container"><h3>DEMO REGISTRASI KONTES</h3>'.$body.'<p>Thank you for registering.</div>';    
                        $data['to'] = 'sales.denpasar@mifx.com';
                        $data['from'] = 'kontes@mifx.com';//$_POST['accountEmail'];//$noreply;
                        $data['subject'] = $subject.'<br>Reply to :'.$_POST['accountEmail'];
                        $data['msg'] = $body;

                        $client->emailkontes($data);
                        $data['to'] = 'susanto@mifx.com';
                        $client->emailkontes($data);
                    }elseif ($group_name=='Kontes2'){
                        // send email
                        $subject = 'Kontes 2 Registration';
                        $body = "<table>
                            <tr>
                                <td>Name</td>
                                <td>: ".$_POST['accountName']."</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>: ".$_POST['accountPhone']."</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>: ".$_POST['accountEmail']."</td>
                            </tr>
                            <tr>
                                <td>Deposit</td>
                                <td>: ".$_POST['accountDeposit']."</td>
                            </tr>
                            <tr>
                                <td>No.Account</td>
                                <td>: {$username}</td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td>: {$password}</td>
                            </tr>
                        </table>";
                        
						$to = 'sales.jogja@mifx.com';
                        $noreply = 'noreply@mifx.com';
                        //@mail($to,$subject,$body);
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                
                        Email::factory($subject,$mesage,'text/html')               
                                    ->from($noreply,'Monex Investindo Futures')
                                    ->to($to)
                                    ->send();
                        						
                        // masukan ke database
                        $query = DB::insert('kontes', array(
                            'name', 
                            'phone', 
                            'email', 
                            'deposit', 
                            'no_account', 
                            'password',
                            'date')); // create sql request
                        $query->values(array(
                            $_POST['accountName'], 
                            $_POST['accountPhone'], 
                            $_POST['accountEmail'], 
                            $_POST['accountDeposit'], 
                            $username, 
                            $password,
                            date('Y-m-d H:i:s')
                                        )); // do it in loop
                        $query->execute(); // execute

                

                        $html = '<div class="container"><h3>DEMO REGISTRASI KONTES 2</h3>'.$body.'<p>Thank you for registering.</div>';    
                        $data['to'] = $to;
                        $data['from'] = 'kontes@mifx.com';//$_POST['accountEmail'];//$noreply;
                        $data['subject'] = $subject.'<br>Reply to :'.$_POST['accountEmail'];
                        $data['msg'] = $body;

                        $client->emailkontes($data);
                    }
                    //$html .= Debug::vars($comm);
                }
                
                $html  .= '</div>';
                
                $this->layout->set('content',$html);
                return;
            }
        }
        
        if ($group_name=='Kontes'){
            Mifx::instance()->soap();
            $this->layout->set('content',View::factory('account/frm-kontes',array('group'=>$group_name)));    		
        }
		elseif($group_name=='Kontes2'){
            Mifx::instance()->soap();
            $this->layout->set('content',View::factory('account/frm-kontes2',array('group'=>$group_name)));    
		}
		else{
            $this->layout->set('content',View::factory('account/frm-nine-stars',array('group'=>$group_name)));
        }  
    }
    
    /****/
}