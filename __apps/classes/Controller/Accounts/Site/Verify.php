<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Accounts_Site_Verify extends Controller {
    
    private $token;
    
    private $api;
        
    public function before() {
        //Before
        parent::before();
        
        //Token
        $this->token = $this->request->param('token');
        
        if (empty($this->token)) {
            die('No direct script access.'); 
        }
        
        //Get param language
        $paramLang = $this->request->param('lang'); 
        
        if (!empty(App::$module->language)) {
            I18n::lang( empty($paramLang) ? Language::$default : $paramLang );
            Language::$current = I18n::$lang;
        }
        
        //Connect to API
        Mifx::instance()->soap();
        
        //Get API
        $this->api = Mifx::get('_soap');
    }
    
	public function action_verify_ebook() {        
        $verify = $this->api->verifyLive($this->token, 'date_verified='. date('Y-m-d H:i:s'));
		
        /** **/ /*
        $verify = array(
                        "status" => "success",
                        "data" => array(
                            "demo" => array (
                                "no_account" => "100712425",
                                "password" => "A5qPmX",
                                "investor" => "hPEbft"
                            ),
                            "marketing" => array (
                                "name" => "Sundari Indrasariani",
                                "kode" => "b0002rg",
                                "wp_code" => "WP030",
                                "email" => "sundari@mifx.com"
                            ),
                            "client" => array (
                                "username" => "trav.one@nouspeed.com",
                                "password" => "fb20ba87010b4b1bf977303061edfbaf",                                
                                "link" => "http://client.mifx.com/site/loginemail?key=z5m474o463m4z5w5r4i5s4p2b4h4b486s55426p294w5u5y2j4h4b4h4e4c4n444z5k4y21384m5w2t2i58464c453s2j5b4z2g5t2x2m584y2l5942484z2x2j5b4t20364s2"
                            )
                        )
                    ); /** **/   
        
        if (empty($verify) || empty($verify['status'])) {
            App::$session->set( 'verify-title' , __('verified-fatal-error-title') );
            
            App::$session->set( 'verify-description' , __( 'verified-fatal-error-desc', array ( ':reason' => 'unknown reason' ) ) );         
        } else {
            $status = $verify['status'];
            $data   = $verify['data'];
            
            if (strtolower($status) == 'success') {
                //$demo       = $data['demo'];        extract($demo,EXTR_PREFIX_ALL,'demo');
                $marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
                $client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');
				//set cookies
				$id = (int)$marketing['lead_id'];
                
                if (!isset($_COOKIE['lead_id'])){
				    $lead = array($id);
				}else{
					// ekstrak content 
					$lead = $_COOKIE['lead_id'];
					// jika lead dalam format json
					if ($this->isJson($lead)){
						// kembalikan ke array
						$lead = stripslashes($lead);
						$lead = json_decode($lead, true);
					}else{
						if (!is_array($lead)) $lead = array($lead);	
					}
					       	
					// cek apakah lead_id sudah ada dalam array ?
					
					if (!in_array($id,$lead)) $lead[] = $id;
					
				}
				
				setcookie('lead_id', json_encode($lead), time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                setcookie('referal', $marketing['referal'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
				//setcookie("lead_id",$marketing['lead_id'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
				//setcookie("referal",$marketing['referal'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                //if (!isset($_COOKIE['visit_id'])){
				//	setcookie("visit_id",$marketing['visit_id'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                //}
                
                /** **/ 
                $subject = "Monex Investindo Futures Demo Account";            
                $to      = $client_username; 
                $noreply = 'noreply@mifx.com';  
                          
                
               // $mailFormat = Model::factory('Emailformat')->item('MIFX Demo Account');
//                $mailFormat = Translate::item('email_formats',$mailFormat);
//
//                if(empty($mailFormat)){
//                    $mesage  = View::factory('account/email-detail-demo-account')
//                                         ->set( 'subject',$subject )
//                                         ->set( 'data',$data )
//                                         ->render();
//                }else {     
//                    $subject = $mailFormat->subject;
//                    $body    = $mailFormat->description;
//                     //ACCOUNT DEMO
//                    $body    = str_replace('[demo_no_account]',$demo_no_account,$body);
//                    $body    = str_replace('[demo_password]',$demo_password,$body);
//                     //CLIENT AREA DETAIL
//                    $body    = str_replace('[client_username]',$client_username,$body);
//                    $body    = str_replace('[client_password]',$demo_password,$body);
//                    $body    = str_replace('[login_url]','<a href="http:client.mifx.com" target="_blank" />http:client.mifx.com</a>',$body);
//                     //MARKETING DETAIL
//                    $body    = str_replace('[marketing_name]',$marketing_name,$body);
//                    $body    = str_replace('[marketing_email]',$marketing_email,$body);
//                    $body    = str_replace('[marketing_phone]',$marketing_phone,$body);
//                    $body    = str_replace('[marketing_branch_phone]',$marketing_phone_office,$body);
//                     //LINK DOWNLOAD
//                    $body    = str_replace('[link]', HTML::anchor( URL::base(true) . 'platform/mifx4setup.exe' ),$body);
//                     //add script 21 may 2014
//                    $body    = str_replace('[auto_login]', $client_link, $body);  
//                                        
//                    $mesage  = View::factory('general/html-email')
//                                        ->set( 'subject',$subject )
//                                        ->set( 'body',$body )
//                                        ->render(); 
//                }
//                
//                Email::factory($subject,$mesage,'text/html')               
//            				->from($noreply,'Monex Investindo Futures')
//            				->to($to)
//            				->send();
                            
                //Check query welcome
				App::$session->set( 'verify-type' , 'e-book');
                $welcome = $this->request->query('welcome');
                //if (!empty($welcome)) {                     
                    $mailFormat = Model::factory('Emailformat')->item('E-book Welcome Message');
                    $mailFormat = Translate::item('email_formats',$mailFormat);
                    
                    if (!empty($mailFormat)) {
                        
                        $link_client     = 'http://client.mifx.com';   
                        $link_product    = Translate::uri(Language::$current,'trading-products');
                        $link_seminar    = Translate::uri(Language::$current,'education/seminar-training');
                        $link_mifx       = 'http://www.mifx.com';
                        $link_monex      = 'http://www.monexnews.com/';
                        $link_demo_register = Translate::uri(Language::$current,'open-demo-account.php');                  
                        $link_open_live     = Translate::uri(Language::$current,'open-live-account.php');     
                        $link_unsubscribe   = Translate::uri(Language::$current,'');         
                        
                        $subject = $mailFormat->subject;
                        $body    = $mailFormat->description;
                        
                        $body    = str_replace('[link_client_area]',$link_client,$body);
                        $body    = str_replace('[link_open_live]',$link_open_live,$body);
                        $body    = str_replace('[link_product]',$link_product,$body);
                        $body    = str_replace('[link_seminar]',$link_seminar,$body);
                        $body    = str_replace('[link_mifx]',$link_mifx,$body);
                        $body    = str_replace('[link_monex]',$link_monex,$body);
                        $body    = str_replace('[link_demo_register]',$link_demo_register,$body);
                        
                        // add script 21 may 2014
                        $body    = str_replace('[client_username]',$client_username,$body);
                        $body    = str_replace('[client_password]',$client['password'],$body);
                        $body    = str_replace('[auto_login]', $client_link, $body);  
                        $body    = str_replace('[marketing_name]',$marketing_name,$body);
                        $body    = str_replace('[marketing_email]',$marketing_email,$body);
                        $body    = str_replace('[marketing_phone]',$marketing_phone,$body);
                        $body    = str_replace('[marketing_branch_phone]',$marketing_phone_office,$body);
                        $body    = str_replace('[link]', URL::base(true) . 'platform/mifx4setup.exe' ,$body);
                        $body    = str_replace('[link_unsubscribe]',$link_unsubscribe,$body);
                        
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                
                        Email::factory($subject,$mesage,'text/html')               
                    				->from($noreply,'Monex Investindo Futures')
                    				->to($to)
                    				->send();
                    }
                //}
                
                App::$session->set( 'verify-title' , __('verified-success-title') );
                
                App::$session->set( 'verify-description' , __( 'verified-success-desc', array ( ':type' => 'e-book', ':email' => $client_username ) ) );      
                    
            } else {
                App::$session->set( 'verify-title' , __('verified-normal-error-title') );
                
                App::$session->set( 'verify-description' , __( 'verified-normal-error-desc', array ( ':reason' => '<span class="error">' . $data['message'] . '</span>' ) ) );       
            }
        }
        
        $uri = Translate::uri(Language::$current,'accounts/verify-status');
        
        $this->redirect($uri);
    }
	
	public function action_verify_mte() {        
        $verify = $this->api->verifyLive($this->token, 'date_verified='. date('Y-m-d H:i:s'));
        
        if (empty($verify) || empty($verify['status'])) {
            App::$session->set( 'verify-title' , __('verified-fatal-error-title') );
            App::$session->set( 'verify-description' , __( 'verified-fatal-error-desc', array ( ':reason' => 'unknown reason' ) ) );         
        } else {
            $status = $verify['status'];
            $data   = $verify['data'];
            
            if (strtolower($status) == 'success') {
               // $demo       = $data['demo'];        extract($demo,EXTR_PREFIX_ALL,'demo');
                $marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
                $client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');
                
                $id = (int)$marketing['lead_id'];
                
                if (!isset($_COOKIE['lead_id'])){
				    $lead = array($id);
				}else{
					// ekstrak content 
					$lead = $_COOKIE['lead_id'];
					// jika lead dalam format json
					if ($this->isJson($lead)){
						// kembalikan ke array
						$lead = stripslashes($lead);
						$lead = json_decode($lead, true);
					}else{
						if (!is_array($lead)) $lead = array($lead);	
					}
					       	
					// cek apakah lead_id sudah ada dalam array ?
					
					if (!in_array($id,$lead)) $lead[] = $id;
					
				}
				
				setcookie('lead_id', json_encode($lead), time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                setcookie('referal', $marketing['referal'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                //setcookie("lead_id",$marketing['lead_id'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
				//setcookie("referal",$marketing['referal'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                //if (!isset($_COOKIE['visit_id'])){
				//	setcookie("visit_id",$marketing['visit_id'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                //}
                
                /** **/ 
                $subject = "Monex Investindo Futures MTE";            
                $to      = $client_username; 
                $noreply = 'noreply@mifx.com';  
                
                            
                //Check query welcome
				App::$session->set( 'verify-type' , 'mte');
                $welcome = $this->request->query('welcome');
                //if (!empty($welcome)) {                     
				$mailFormat = Model::factory('Emailformat')->item('MTE Welcome Message');
				$mailFormat = Translate::item('email_formats',$mailFormat);
                    
				if (!empty($mailFormat)) {
					$link_client     = 'http://client.mifx.com';   
					$link_product    = Translate::uri(Language::$current,'trading-products');
					$link_seminar    = Translate::uri(Language::$current,'education/seminar-training');
					$link_mifx       = 'http://www.mifx.com';
					$link_monex      = 'http://www.monexnews.com/';
					$link_demo_register = Translate::uri(Language::$current,'open-demo-account.php');                  
					$link_open_live     = Translate::uri(Language::$current,'open-live-account.php');     
					$link_unsubscribe   = Translate::uri(Language::$current,'');         
					
					$subject = $mailFormat->subject;
					$body    = $mailFormat->description;
					
					$body    = str_replace('[link_client_area]',$link_client,$body);
					$body    = str_replace('[link_open_live]',$link_open_live,$body);
					$body    = str_replace('[link_product]',$link_product,$body);
					$body    = str_replace('[link_seminar]',$link_seminar,$body);
					$body    = str_replace('[link_mifx]',$link_mifx,$body);
					$body    = str_replace('[link_monex]',$link_monex,$body);
					$body    = str_replace('[link_demo_register]',$link_demo_register,$body);
					
					// add script 21 may 2014
					$body    = str_replace('[client_username]',$client_username,$body);
					$body    = str_replace('[client_password]',$client['password'],$body);
					$body    = str_replace('[auto_login]', $client_link, $body);  
					$body    = str_replace('[marketing_name]',$marketing_name,$body);
					$body    = str_replace('[marketing_email]',$marketing_email,$body);
					$body    = str_replace('[marketing_phone]',$marketing_phone,$body);
					$body    = str_replace('[marketing_branch_phone]',$marketing_phone_office,$body);
					$body    = str_replace('[link]', URL::base(true) . 'platform/mifx4setup.exe' ,$body);
					$body    = str_replace('[link_unsubscribe]',$link_unsubscribe,$body);
					
					$mesage  = View::factory('general/html-email')
										->set( 'subject',$subject )
										->set( 'body',$body )
										->render();
			
					Email::factory($subject,$mesage,'text/html')               
								->from($noreply,'Monex Investindo Futures')
								->to($to)
								->send();
				}
                
                App::$session->set( 'verify-title' , __('verified-success-title') );
                App::$session->set( 'verify-description' , __( 'verified-success-desc', array ( ':type' => 'demo', ':email' => $client_username ) ) );      
            } else {
                App::$session->set( 'verify-title' , __('verified-normal-error-title') );
                App::$session->set( 'verify-description' , __( 'verified-normal-error-desc', array ( ':reason' => '<span class="error">' . $data['message'] . '</span>' ) ) );       
            }
        }
        $uri = Translate::uri(Language::$current,'accounts/verify-status');
        $this->redirect($uri);
    }
	
    public function action_verify_live_account () {          
        $verify = $this->api->verifyLive($this->token, 'date_verified='. date('Y-m-d H:i:s'));
        /** **/ /*
        $verify = array(
                        "status" => "success",
                        "data" => array(
                            "client" => array (
                                "username" => "trav.one@nouspeed.com",
                                "password" => "abpj3h",
                                "name" => "survey17",
                                "link" => "http://client.mifx.com/site/loginemail?key=z5m474o463m4z5w5r4i5s4p2b4h4b486s55426p294w5u5y2j4h4b4h4e4c4n444z5k4y21384m5w2t2i58464c453s2j5b4z2g5t2x2m584y2l5942484z2x2j5b4t20364s2"
                            ),
                            "marketing" => array (
                                "email" => "titik.chomsah@mifx.com",
                                "name" =>  "Titik Chomsah",
                                "kode" => "H0002RG",
                                "kode_wp" => "WP020",
                            )
                        )
                    );
        /** **/
        
        if (empty($verify) || empty($verify['status'])) {
            App::$session->set( 'verify-title' , __('verified-fatal-error-title') );
            
            App::$session->set( 'verify-description' , __( 'verified-fatal-error-desc', array ( ':reason' => 'unknown reason' ) ) );         
        } else {
            $status = $verify['status'];
            $data   = $verify['data'];
            
            if (strtolower($status) == 'success') {
                $marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
                $client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');
                
                $id = (int)$marketing['lead_id'];
                
                if (!isset($_COOKIE['lead_id'])){
				    $lead = array($id);
				}else{
					// ekstrak content 
					$lead = $_COOKIE['lead_id'];
					// jika lead dalam format json
					if ($this->isJson($lead)){
						// kembalikan ke array
						$lead = stripslashes($lead);
						$lead = json_decode($lead, true);
					}else{
						if (!is_array($lead)) $lead = array($lead);	
					}
					       	
					// cek apakah lead_id sudah ada dalam array ?
					
					if (!in_array($id,$lead)) $lead[] = $id;
					
				}
				
				setcookie('lead_id', json_encode($lead), time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                setcookie('referal', $marketing['referal'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                /** **/ 
                $subject = "Monex Investindo Futures Live Account";            
                $to      = $client_username; 
                $noreply = 'noreply@mifx.com';            
//                $mesage  = View::factory('account/email-detail-live-account')
//                                    ->set( 'subject',$subject )
//                                    ->set( 'data',$data )
//                                    ->render();
//                
//                Email::factory($subject,$mesage,'text/html')               
//            				->from($noreply,'Monex Investindo Futures')
//            				->to($to)
//            				->send();
                            
                //Check query welcome
                $welcome = $this->request->query('welcome');
               //if (!empty($welcome)) {                    
                    $mailFormat = Model::factory('Emailformat')->item('Live Account Welcome Message');
                    $mailFormat = Translate::item('email_formats',$mailFormat);
                    
                    if (!empty($mailFormat)) {
                        $marketing_phone = "";
                        $link_client     = $client_link;   
                        $link_product    = Translate::uri(Language::$current,'trading-products');
                        $link_seminar    = Translate::uri(Language::$current,'education/seminar-training');
                        $link_mifx       = 'http://www.mifx.com';
                        $link_monex      = 'http://www.monexnews.com/';
                        $link_demo_register = Translate::uri(Language::$current,'open-demo-account.php');            
                        $link_open_live     = Translate::uri(Language::$current,'open-live-account.php');     
                        $link_unsubscribe   = Translate::uri(Language::$current,'');         
                        
                        $subject = $mailFormat->subject;
                        $body    = $mailFormat->description;
                        
                        $body    = str_replace('[link_client_area]',$link_client,$body);
                        $body    = str_replace('[link_product]',$link_product,$body);
                        $body    = str_replace('[link_seminar]',$link_seminar,$body);
                        $body    = str_replace('[link_mifx]',$link_mifx,$body);
                        $body    = str_replace('[link_monex]',$link_monex,$body);
                        $body    = str_replace('[link_demo_register]',$link_demo_register,$body);
                        
                        // add script 21 may 2014
                        $body    = str_replace('[client_username]',$client_username,$body);
                        $body    = str_replace('[client_password]',$client_password,$body);
                        $body    = str_replace('[auto_login]', $client_link, $body);  
                        $body    = str_replace('[marketing_name]',$marketing_name,$body);
                        $body    = str_replace('[marketing_email]',$marketing_email,$body);
                        $body    = str_replace('[marketing_phone]',$marketing_phone,$body);
                        $body    = str_replace('[marketing_branch_phone]',$marketing_phone_office,$body);
                        $body    = str_replace('[link]', URL::base(true) . 'platform/mifx4setup.exe', $body);
                        $body    = str_replace('[link_unsubscribe]',$link_unsubscribe,$body);
                        
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                
                        Email::factory($subject,$mesage,'text/html')               
                    				->from($noreply,'Monex Investindo Futures')
                    				->to($to)
                    				->send();
                    }
                //}
                
                App::$session->set( 'verify-title' , __('verified-success-title') );
                
                App::$session->set( 'verify-description' , __( 'verified-success-desc', array ( ':type' => 'live', ':email' => $client_username ) ) );      
                    
            } else {
                App::$session->set( 'verify-title' , __('verified-normal-error-title') );
                
                App::$session->set( 'verify-description' , __( 'verified-normal-error-desc', array ( ':reason' => '<span class="error">' . $data['message'] . '</span>' ) ) );       
            }
        }
        
        $uri = Translate::uri(Language::$current,'accounts/verify-status');
        
        $this->redirect($uri);                     
    }
    
    public function action_verify_demo_account() {        
        $verify = $this->api->verifyDemo($this->token, 'date_verified='. date('Y-m-d H:i:s'));
        /** **/ /*
        $verify = array(
                        "status" => "success",
                        "data" => array(
                            "demo" => array (
                                "no_account" => "100712425",
                                "password" => "A5qPmX",
                                "investor" => "hPEbft"
                            ),
                            "marketing" => array (
                                "name" => "Sundari Indrasariani",
                                "kode" => "b0002rg",
                                "wp_code" => "WP030",
                                "email" => "sundari@mifx.com"
                            ),
                            "client" => array (
                                "username" => "trav.one@nouspeed.com",
                                "password" => "fb20ba87010b4b1bf977303061edfbaf",                                
                                "link" => "http://client.mifx.com/site/loginemail?key=z5m474o463m4z5w5r4i5s4p2b4h4b486s55426p294w5u5y2j4h4b4h4e4c4n444z5k4y21384m5w2t2i58464c453s2j5b4z2g5t2x2m584y2l5942484z2x2j5b4t20364s2"
                            )
                        )
                    ); /** **/   
        
        if (empty($verify) || empty($verify['status'])) {
            App::$session->set( 'verify-title' , __('verified-fatal-error-title') );
            
            App::$session->set( 'verify-description' , __( 'verified-fatal-error-desc', array ( ':reason' => 'unknown reason' ) ) );         
        } else {
            $status = $verify['status'];
            $data   = $verify['data'];
            
            if (strtolower($status) == 'success') {
                $demo       = $data['demo'];        extract($demo,EXTR_PREFIX_ALL,'demo');
                $marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
                $client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');
                
                $id = (int)$marketing['lead_id'];
                
                if (!isset($_COOKIE['lead_id'])){
				    $lead = array($id);
				}else{
					// ekstrak content 
					$lead = $_COOKIE['lead_id'];
					// jika lead dalam format json
					if ($this->isJson($lead)){
						// kembalikan ke array
						$lead = stripslashes($lead);
						$lead = json_decode($lead, true);
					}else{
						if (!is_array($lead)) $lead = array($lead);	
					}
					       	
					// cek apakah lead_id sudah ada dalam array ?
					
					if (!in_array($id,$lead)) $lead[] = $id;
					
				}
				
				setcookie('lead_id', json_encode($lead), time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                setcookie('referal', $marketing['referal'], time() + (10 * 365 * 24 * 60 * 60), '/', 'mifx.com');
                
                /** **/ 
                $subject = "Monex Investindo Futures Demo Account";            
                $to      = $client_username; 
                $noreply = 'noreply@mifx.com';  
                          
                
               // $mailFormat = Model::factory('Emailformat')->item('MIFX Demo Account');
//                $mailFormat = Translate::item('email_formats',$mailFormat);
//
//                if(empty($mailFormat)){
//                    $mesage  = View::factory('account/email-detail-demo-account')
//                                         ->set( 'subject',$subject )
//                                         ->set( 'data',$data )
//                                         ->render();
//                }else {     
//                    $subject = $mailFormat->subject;
//                    $body    = $mailFormat->description;
//                     //ACCOUNT DEMO
//                    $body    = str_replace('[demo_no_account]',$demo_no_account,$body);
//                    $body    = str_replace('[demo_password]',$demo_password,$body);
//                     //CLIENT AREA DETAIL
//                    $body    = str_replace('[client_username]',$client_username,$body);
//                    $body    = str_replace('[client_password]',$demo_password,$body);
//                    $body    = str_replace('[login_url]','<a href="http:client.mifx.com" target="_blank" />http:client.mifx.com</a>',$body);
//                     //MARKETING DETAIL
//                    $body    = str_replace('[marketing_name]',$marketing_name,$body);
//                    $body    = str_replace('[marketing_email]',$marketing_email,$body);
//                    $body    = str_replace('[marketing_phone]',$marketing_phone,$body);
//                    $body    = str_replace('[marketing_branch_phone]',$marketing_phone_office,$body);
//                     //LINK DOWNLOAD
//                    $body    = str_replace('[link]', HTML::anchor( URL::base(true) . 'platform/mifx4setup.exe' ),$body);
//                     //add script 21 may 2014
//                    $body    = str_replace('[auto_login]', $client_link, $body);  
//                                        
//                    $mesage  = View::factory('general/html-email')
//                                        ->set( 'subject',$subject )
//                                        ->set( 'body',$body )
//                                        ->render(); 
//                }
//                
//                Email::factory($subject,$mesage,'text/html')               
//            				->from($noreply,'Monex Investindo Futures')
//            				->to($to)
//            				->send();
                            
                //Check query welcome
                $welcome = $this->request->query('welcome');
                //if (!empty($welcome)) {                     
                    $mailFormat = Model::factory('Emailformat')->item('Demo Account Welcome Message');
                    $mailFormat = Translate::item('email_formats',$mailFormat);
                    
                    if (!empty($mailFormat)) {
                        
                        $link_client     = 'http://client.mifx.com';   
                        $link_product    = Translate::uri(Language::$current,'trading-products');
                        $link_seminar    = Translate::uri(Language::$current,'education/seminar-training');
                        $link_mifx       = 'http://www.mifx.com';
                        $link_monex      = 'http://www.monexnews.com/';
                        $link_demo_register = Translate::uri(Language::$current,'open-demo-account.php');                  
                        $link_open_live     = Translate::uri(Language::$current,'open-live-account.php');     
                        $link_unsubscribe   = Translate::uri(Language::$current,'');         
                        
                        $subject = $mailFormat->subject;
                        $body    = $mailFormat->description;
                        
                        $body    = str_replace('[link_client_area]',$link_client,$body);
                        $body    = str_replace('[link_open_live]',$link_open_live,$body);
                        $body    = str_replace('[link_product]',$link_product,$body);
                        $body    = str_replace('[link_seminar]',$link_seminar,$body);
                        $body    = str_replace('[link_mifx]',$link_mifx,$body);
                        $body    = str_replace('[link_monex]',$link_monex,$body);
                        $body    = str_replace('[link_demo_register]',$link_demo_register,$body);
                        
                         //ACCOUNT DEMO
                        $body    = str_replace('[demo_no_account]',$demo_no_account,$body);
                        $body    = str_replace('[demo_password]',$demo_password,$body);
                        // add script 21 may 2014
                        $body    = str_replace('[client_username]',$client_username,$body);
                        $body    = str_replace('[client_password]',$demo_password,$body);
                        $body    = str_replace('[auto_login]', $client_link, $body);  
                        $body    = str_replace('[marketing_name]',$marketing_name,$body);
                        $body    = str_replace('[marketing_email]',$marketing_email,$body);
                        $body    = str_replace('[marketing_phone]',$marketing_phone,$body);
                        $body    = str_replace('[marketing_branch_phone]',$marketing_phone_office,$body);
                        $body    = str_replace('[link]', URL::base(true) . 'platform/mifx4setup.exe' ,$body);
                        $body    = str_replace('[link_unsubscribe]',$link_unsubscribe,$body);
                        
                        $mesage  = View::factory('general/html-email')
                                            ->set( 'subject',$subject )
                                            ->set( 'body',$body )
                                            ->render();
                
                        Email::factory($subject,$mesage,'text/html')               
                    				->from($noreply,'Monex Investindo Futures')
                    				->to($to)
                    				->send();
                    }
                //}
                
                App::$session->set( 'verify-title' , __('verified-success-title') );
                
                App::$session->set( 'verify-description' , __( 'verified-success-desc', array ( ':type' => 'demo', ':email' => $client_username ) ) );      
                    
            } else {
                App::$session->set( 'verify-title' , __('verified-normal-error-title') );
                
                App::$session->set( 'verify-description' , __( 'verified-normal-error-desc', array ( ':reason' => '<span class="error">' . $data['message'] . '</span>' ) ) );       
            }
        }
        
        $uri = Translate::uri(Language::$current,'accounts/verify-status');
        
        $this->redirect($uri);
    }
    
    function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
