<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Partnership_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section;
    
    public function before() {
        
        $this->layout = 'two-column';        
        
        parent::before();
        
        $this->section = $this->request->param('section1');
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
        $post = $this->post;
        
        if(!empty($post)){
            
            /**
             * Conversion 
             * extract post
             */
            foreach($post as $key => $val){
                $element[$key]  = $val;
            }
            Conversion::record('Partnership', $element); 
            /** end */ 

            $register = Mifx::registerAffiliate($post);
            
            if (empty($register) || $register['status'] == 'error') {
                $errorRegistration = empty($register) ? 'Error. Could not register' : $register['data'];
                App::$session->set('error-register',$errorRegistration);
            } else {
                $subject = "Partner Registration";
                //$to      = 'doni@webarq.com'; 
                $to         = 'RFA@mifx.com';    
                $noreply = 'dari.web@mifx.com';            
                $mesage  = View::factory('contact/email_partnership')->set('post',$this->post)->render();
                
                Model::factory('Contact_Form')->save();
                
                Email::factory($subject,$mesage,'text/html')               
            				->from($noreply)
            				->to($to)
            				->send();  
                            
                App::$session->set('error-register',null);
                            
                App::$session->set('submit-form',true);
            }    
            
            $url = Page::get('completedNavigationUri:partnership');
            
            $this->redirect($url . '#form_partner');
            
            return;
        }
        
        
        /** Get Footer **/
        $footer = View::factory('education/footer')
                                    ->set('ebook',Model::factory('Page')->navigation_page('e-book'))
                                    ->set('videoItem',Model::factory('News_Monexvideo')->feature(1))
                                    ->set('videoDesc',Model::factory('Page')->navigation_page('video'))
                                   ->set('articles',empty($this->section) ? Model::factory('News_List')->feature(4,array('news-thumbnail')) : null);
        
        /** Get Page **/
        $page = Model::factory('Page')->navigation_page();
        
        /** Get Left Banners **/
        $leftBanners = Model::factory('Medialibrary')->simple_application('page-left-bnnr',!empty($page->id) ? $page->id : 0,'site_navigation_pages');
        $htmlBanner  = '';
        if (!empty($leftBanners) && $leftBanners->valid()) {
            foreach ($leftBanners as $b) {
                $detail = $link = $title = '';
                $attrs  = array( 'class' => 'btn' );
                if (!empty($b->detail)) {
                    $detail = json_decode($b->detail);
                    $link   = "#";
                    if (!empty($detail->banner_permalink)) {
                        if (strpos($detail->banner_permalink,'http') === 0) {
                            $link = $detail->banner_permalink;
                            $attrs['target'] = '_blank';
                        } else {
                            $link = Page::get("completedNavigationUri:$detail->banner_permalink");
                            if(empty($link)) {
                                $link = URL::front($detail->banner_permalink);
                            }    
                        }
                    }
                    
                    $title  = $detail->banner_title;    
                }
                
                $htmlBanner .= '<div class="platform">'.
                        HTML::image( URL::mediaImage( $b->folder . '/' . $b->file , null ) , array( 'alt' => '' ) ).
                        '<p>'.(preg_replace('/(monex)/i','<span>$1</span>',$title)).'</p>'.
                        '<p>'.HTML::anchor( $link ,  __('btn-read-more') , $attrs ).'</p>'. 
                     '</div>';
            }    
        }
        
        
        
        if(!empty($page) && $page->permalink == 'partnership') {                    
            //Call soap affiliate
            $affiliateSoap    = Mifx::soapAffiliate();
            $affiliateSources = $affiliateSoap->getSource();
            $affiliateTypes   = $affiliateSoap->getType();
        }
        
        /** **/                            
        $this->layout
             ->set('live',true)
             ->set('showNavigation',true)
             ->set('content',View::factory('layout/common-page')
                                ->set('page',$page)
                                ->set('submitForm', App::$session->get('submit-form',false))
                                ->set('affliateSources',empty($affiliateSources) ? '' : $affiliateSources)
                                ->set('affiliateTypes',empty($affiliateTypes) ? '' : $affiliateTypes)
                  )
             #->set('articles',Model::factory('News_List')->latest(4,array('news-thumbnail')))
             ->set('htmlBanner',$htmlBanner);
             #->set('footer',$footer);
    }
}