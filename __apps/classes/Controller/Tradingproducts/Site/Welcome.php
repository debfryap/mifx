<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Tradingproducts_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section;
    private $section2;
    
    public function before() {
        
        $this->section  = $this->request->param('section1');
        
        $this->section2 = $this->request->param('section2');
       

        if (empty($this->section))
            $this->layout = 'trading-home';   
        else
            $this->layout = 'two-column';         
        
        parent::before(); 
        
        App::$config->menu->active = 'trading-products';
        
        $this->styles(URL::templateCss('trading.css'));      
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
        
        $form = false;
        $page = null;
        
        if($this->section == 'bbj'){
            $this->action_bbj();
            return false;
        } 

        if($this->section == 'bkdi'){
            $this->action_bkdi();
            return false;
        } 

        if($this->section2 == "bbj-fx" || $this->section2 == "bkdi-icdx"){
            
            if($this->section2 == "bbj-fx") $action = URL::front(App::$config->menu->active. '/bbj');
            if($this->section2 == "bkdi-icdx") $action = URL::front(App::$config->menu->active. '/bkdi');

            $content = View::factory('trading-product/form-page')
                            ->set('title',Page::get('simpleBreadCrumb:'.$this->section2))
                            ->set('pages',Page::getPages($this->section2))
                            ->set('action', $action)
                            ->set('form_page',true);
        }else{

            $page = Model::factory('Page')->navigation_page_array();
            
            $content = View::factory('layout/common-page')->set('page', $page);
        }

        if (!empty($this->section)) {

            // redirect 404
            //if(empty($page)) $this->redirect(URL::front('errors/404'));
            
            $this->layout
                  ->set('live',true)
                  ->set('showNavigation',true)
                  ->set('content',$content);

        }
    }

    public function action_bbj(){

        /**
         * Conversion 
         * extract post
         */
        foreach($this->post as $key => $val){
            $element[$key]  = $val;
        }
        Conversion::record('Request Demo Multilateral(BBJ/FX)', $element); 
        /** end */ 

        $nama   = $this->post->nama;
        $email  = $this->post->email;
        $kode   = $this->post->kode;

        $mailFormat = Model::factory('Emailformat')->item('Request Demo Multilateral(BBJ/FX)');
        $mailFormat = Translate::item('email_formats',$mailFormat);
        $subject    = $mailFormat->subject;
        $body       = $mailFormat->description;
        
        //Email Recipient
        $to  = Contact::$config->office->email_bbj;
        
        // message
        $message  = '<html><head><title></title></head><body>';
        $message .= $body;
        $message .= '</body></html>';
        
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'To: Mary <'. $to .'>' . "\r\n";
        $headers .= 'From: '. $nama .' <'. $email .'>' . "\r\n";
        
        // Mail it
        mail($to, $subject, $message, $headers);
        
        $insert = DB::insert('trading_bbj', array('nama', 'email', 'kode', 'create_time'))
                            ->values(array($nama, $email, $kode, date('Y-m-d H:i:s')))->execute();
        
        if($insert)
            die('success !'.$to);
        else
            die('error !');
    }

    public function action_bkdi(){

        /**
         * Conversion 
         * extract post
         */
        foreach($this->post as $key => $val){
            $element[$key]  = $val;
        }
        Conversion::record('Request Demo Multilateral(BKDI/ICDX)', $element); 
        /** end */ 

        $nama   = $this->post->nama;
        $email  = $this->post->email;
        $kode   = $this->post->kode;

        $mailFormat = Model::factory('Emailformat')->item('Request Demo Multilateral(BKDI/ICDX)');
        $mailFormat = Translate::item('email_formats',$mailFormat);
        $subject    = $mailFormat->subject;
        $body       = $mailFormat->description;

        //Email Recipient
        $to  = Contact::$config->office->email_bkdi;

        // message
        $message  = '<html><head><title>Test Submit Email</title></head><body>';
        $message .= $body;
        $message .= '</body></html>';
        
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
         // Additional headers
        $headers .= 'To:  <'. $to .'>' . "\r\n";
        $headers .= 'From: '. $nama .' <'. $email .'>' . "\r\n";
        
        // Mail it
        mail($to, $subject, $message, $headers);
        
        $insert = DB::insert('trading_bkdi', array('nama', 'email', 'kode', 'create_time'))
                            ->values(array($nama, $email, $kode, date('Y-m-d H:i:s')))->execute();
        if($insert)
            die('success !'.$to);
        else
            die('error !');
    }
}