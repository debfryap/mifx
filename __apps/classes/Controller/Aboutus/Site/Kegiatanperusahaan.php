<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Aboutus_Site_Kegiatanperusahaan extends Controller_Default_Template_Site {
    
    private $section;
    
    private $item;
    
    public function before() {
        
        $this->layout = 'two-column';        
        
        parent::before();
        
        App::$config->menu->active = 'about-us';
        
        $this->section = $this->request->param('section');
        
        $this->item = $this->request->param('item');      
        
        //Append media validate
        $this->media_validate(true);
        
        //Call soap from MIFX
        Mifx::instance()->soap();
    }
    
    public function action_index () {
        
        if (empty($this->section)) {
            
            $navAbout = Page::get('navigationObject');
            
            foreach ($navAbout as $item) {
                if ($item->permalink == 'about-us') {
                    if (!empty($item->child)) {
                        foreach ($item->child as $child) {
                            if ($child->permalink == 'kegiatan-perusahaan') {
                                if (!empty($child->child)) {
                                    foreach ($child->child as $child) {
                                        $this->section = $child->permalink;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }    
            }
        }
        
        $this->layout
             ->set('live',true)
             ->set('showNavigation',$this->section);
        
        if (empty($this->section))  
            $this->section == 'monex-corporate-event';
        
        if ($this->section == 'monex-corporate-event' || $this->section == 'monex-update') {
            $function = str_replace("-","_",$this->section);
            $this->$function();
            return; 
        }
    }
    
    private function monex_corporate_event() {
        if (empty($this->item))
        {
            $this->layout->set('content',Mifx::instance()->list_updates('monex-corporate-event','news_events'));
        }
        else
        {
            $item = Model::factory('News_Listevent')->item($this->item,array('updates-banner'));
            $item = Translate::item('news_events',$item);
            // set meta
            App::$config->meta->title       = (!empty($item->meta_title) ?  $item->meta_title :  $item->label);
            App::$config->meta->description = (!empty($item->meta_description) ?  $item->meta_description :  Text::limit_chars(strip_tags($item->description), 160,"", true ));

            // redirect 404
            if(empty($item)) $this->redirect(URL::front('errors/404'));

            $this->layout
                 ->set('breadcrumb',Page::breadCrumb('full',"as-parent-$this->section") . (empty($item) ? $this->item : $item->label))
                 ->set('content',View::factory('about/kegiatan-detail')->set('item',$item)->set('section',$this->section));
        }
    }
    
    private function monex_update() {
        if (empty($this->item))
        {
            $this->layout->set('content',Mifx::instance()->list_updates('monex-update','news_updates'));
        }
        else
        {
            $item = Model::factory('News_Listupdate')->item($this->item,array('updates-banner'));
            $item = Translate::item('news_updates',$item);
            // set meta
            App::$config->meta->title       = (!empty($item->meta_title) ?  $item->meta_title :  $item->label);
            App::$config->meta->description = (!empty($item->meta_description) ?  $item->meta_description :  Text::limit_chars(strip_tags($item->description), 160,"", true ));
            
            // redirect 404
            if(empty($item)) $this->redirect(URL::front('errors/404'));
            
            $this->layout
                 ->set('breadcrumb',Page::breadCrumb('full',"as-parent-$this->section") . (empty($item) ? $this->item : $item->label))
                 ->set('content',View::factory('about/kegiatan-detail')->set('item',$item)->set('section',$this->section));
        }
    }
}