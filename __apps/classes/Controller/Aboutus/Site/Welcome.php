<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Frontend) 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Aboutus_Site_Welcome extends Controller_Default_Template_Site {
    
    private $section;
    
    private $page;
    
    private $item;
    
    public function before() {
        
        $this->layout = 'two-column';        
        
        parent::before();
        
        App::$config->menu->active = 'about-us';
        
        $this->section = $this->request->param('section1');      
        
        $this->item = $this->request->param('section2');   
        
        //Append media validate
        $this->media_validate(true);
        
        //Compare page
        $this->compare = $this->request->query('soap');
        
        //Call soap from MIFX
        if (empty($this->compare)) {
            Mifx::instance()->soap();
        }
        
        if (empty($this->section)) {
            
            $navAbout = Page::get('navigationObject');
            
            foreach ($navAbout as $item) {
                if ($item->permalink == 'about-us') {
                    if (!empty($item->child)) {
                        foreach ($item->child as $child) {
                            $this->section = $child->permalink;
                            break;
                        }
                    }
                    break;
                }    
            }
        }
    }
    
    public function action_index () {
        
        if($this->section== 'download'){   
            $url = 'media/kcfinder/docs/'. $this->item.'.pdf';
            
            if (file_exists($url)) {  
                //Brute force downloaded all files ....
                header("Content-type: application/force-download");
                header("Content-Transfer-Encoding: Binary");
                header("Content-length: ".filesize($url));
                header("Content-disposition: attachment; filename=\"".basename($url)."\"");
                readfile($url);
                return;
            }
        }
        
        $this->layout
             ->set('live',empty($this->compare))
             ->set('showNavigation',$this->section);
             
        if ($this->section == 'yang-perlu-diketahui' || $this->section == 'keunggulan-bertransaksi-di-monex')
        {
            $pages = Page::getPages($this->section);
            
            if ( !empty($pages) ) 
            {
                if ($this->section == 'yang-perlu-diketahui') {
                    $html  = '<div class="accordion_wrap">';
                    
                    foreach ($pages as $page) {
                        $html .= '<h2 class="acc_trigger"><a href="#">'.$page->title.'</a></h2>'.'<div class="acc_container">'.$page->description.'</div>';
                    }
                        
                    $html .= '</div>';
                }
                
                if ($this->section == 'keunggulan-bertransaksi-di-monex') {
                    $html = '';
                    foreach ($pages as $page) {
                        $page  = Translate::item('site_navigations_pages',$page);
                        
                        $html .= '<p>'
                                    . HTML::image( URL::templateImage('material/bullet.png') , array('style'=>'margin:0 10px;'))
                                    . $page->title . '</p>'
                                    . $page->description ;
                    }
                }
                
                $page = new stdClass;
                $page->title = Page::get("simpleBreadCrumb:$this->section");
                $page->description = $html;
            }
            
            $this->layout->set( 'content',
                                View::factory('layout/common-page')
                                ->set('page',isset($page) ? $page : null));
        }
        elseif ($this->section == 'sitemap') 
        {
            App::$config->menu->active = '';
            $content = '<h2 class="title_page">'.Page::get('simpleBreadCrumb:sitemap').'</h2>';
            $this->layout
                 ->set( 'content', $content . Mifx::sitemap(Page::get('navigationObjectComplete')) )
                 ->set( 'showNavigation' , null);    
        }
        elseif($this->section =='kantor-kami'){
            $this->contact_us();
        }
        
        else
        {
            $pages = Page::getPages($this->section);
            
            if(empty($pages)) {
                if(empty($pages)) $this->redirect(URL::front('errors/404'));
                $this->layout->set('content',__('no-data-found-description'));
                return;
            }
            $page = new stdClass;
            foreach($pages as $i) $page->id = $i->id;

            $this->layout->set('content',View::factory('layout/common-page')->set('page', Model::factory('Page')->navigation_page($this->section))->set('banners',Model::factory('Medialibrary')->simple_application('home-banner', $page->id ,'site_navigations')));
        }
    }
    
     private function contact_us(){

        if (!empty($this->post)) {
            //Configuration email
            #echo Debug::vars($this->post);
            #echo Debug::vars(Contact::$config);
            $subject = "subject-of-message";
            $subject = !empty($this->post->$subject) ? $this->post->$subject : 'Somebody has send you an email';            
            $to      = !empty($this->post->kota) ? $this->post->kota : ( empty(Contact::$config->office->email) ? 'daniel@webarq.com' : Contact::$config->office->email );
            #$to      = 'daniel@webarq.com'; 
            $noreply = !empty($this->post->email) ? $this->post->email : 'dari.web@mifx.com';            
            $mesage  = View::factory('contact/email')->set('post',$this->post)->render();
            
            Model::factory('Contact_Form')->save();
            
            if (!empty(Contact::$config) && !empty(Contact::$config->send_to_email)) 
            {
                Email::factory($subject,$mesage,'text/html')               
                            ->from($noreply)
                            ->to($to) 
                            ->send();  
            }
                        
            App::$session->set('submit-form',true);
             
            $this->redirect(Page::get('completedNavigationUri:contact-us'));
            
            return;
        }
        
        /** Get Left Banners **/
        $leftBanners = Model::factory('Medialibrary')->simple_application('page-left-bnnr',72,'site_navigations');
        $htmlBanner  = '';
        if (!empty($leftBanners) && $leftBanners->valid()) {
            foreach ($leftBanners as $b) {
                $detail = $link = $title = '';
                $attrs  = array( 'class' => 'btn' );
                if (!empty($b->detail)) {
                    $detail = json_decode($b->detail);
                    $link   = "#";
                    if (!empty($detail->banner_permalink)) {
                        if (strpos($detail->banner_permalink,'http') === 0) {
                            $link = $detail->banner_permalink;
                            $attrs['target'] = '_blank';
                        } else {
                            $link = Page::get("completedNavigationUri:$detail->banner_permalink");
                            if(empty($link)) {
                                $link = URL::front($detail->banner_permalink);
                            }    
                        }
                    }
                    
                    $title  = $detail->banner_title;    
                }
                
                $htmlBanner .= '<div class="platform">'.
                        HTML::image( URL::mediaImage( $b->folder . '/' . $b->file , null ) , array( 'alt' => '' ) ).
                        '<p>'.(preg_replace('/(monex)/i','<span>$1</span>',$title)).'</p>'.
                        '<p>'.HTML::anchor( $link , HTML::image( URL::templateImage('material/btn_read.png') ) , $attrs).'</p>'. 
                     '</div>';
            }    
        }
        
        $this->layout
             ->set('htmlBanner',$htmlBanner)
             ->set( 'content', 
                    View::factory('contact/home')
                        ->set( 'submitForm', App::$session->get('submit-form',false) )
                        ->set( 'provinces' , Model::factory('Contact_Province')->getProvinceOffice() ) );
    }
    
    public function after() {
        parent::after();
        $profiling = $this->request->query('profiling');
        if (!empty($profiling) && $profiling == 'true')
            echo View::factory('profiler/stats'); 
    }
}