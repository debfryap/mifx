<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class HTML Space
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_Default_Template_Site extends Controller_System {
    
    //Active URL param package
    protected $package;
    
    protected $layout;
    
    public function before() {
        parent::before(__CLASS__);    
    
        //Cookie application check
        $app_coookie = App::cookie('webarqsite',str_replace('/','',Kohana::$base_url));
        
        //Styles
       // $this->styles(URL::templateCss('flexslider','accordion','colorbox','bootstrap','bootstrap-responsive', 'jquery.fancybox-2.1.4'));
		//debfry edit
		$this->styles(URL::templateCss('flexslider','accordion','colorbox','bootstrap','style_tablet','style_mobile','jquery.fancybox-2.1.4'));
        
        //Scripts 
        $this->jquery = '1.7.2';
        $this->scripts(URL::templateJs('jquery.colorbox','jquery.flexslider','bootstrap','_bootstrap-collapse','jquery.jcarousellite', 'jquery.fancybox-2.1.4.pack', 'function'));
          
        //No CSS Reset
        $this->css_reset = false;
        
        //Navigation Collector
        Page::$instance->navigationCollector();
        
        //Predefined data
        $this->register('disclaimer',Model::factory('Page')->navigation_page('general-disclaimer'))
             ->register('logo',Model::factory('Medialibrary')->simple_application('logo-partner',1,'site_navigations'))
             ->register('page',Model::factory('Page')->navigation_page('home'))
             ->register('footer_forex_trading', Model::factory('Page')->navigation_page('footer-forex-trading'))
             ;
        
        //Layout
        if (!empty($this->layout))
            $this->layout = View::factory("layout/$this->layout");
    }
    
    public function after() {
        if (App::$config->system->offline) {
            //  
            $this->template = 'offline';
            
            parent::before();
        }
        
        $this->register('layout',$this->layout);

        //$this->scripts('http://chatserver.comm100.com/js/LiveChat.js?siteId=77063&planId=271&partnerId=-1');     
        
        parent::after();
    }

}