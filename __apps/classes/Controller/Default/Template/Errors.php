<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author    Daniel Simangunsong
 * @Company   Webarq
 * @copyright   2012
 * @Package     Controller (For Application Frontend) 
 * @Module      ~
 * @License   Kohana ~ Webarq ~ Daniel Simangunsong 
 * 
 * Calm seas, never make skillfull sailors  
**/ 
class Controller_Default_Template_Errors extends Controller_Default_Template_Site {
    
    private $section = null;
    
    private $item = null;
    
    private $page;
    
    public function before() { 
        $this->layout = 'one-column';
        
        parent::before();
        
        App::$config->menu->active = 'errors';
        
        $this->section = $this->request->param('section1');
        
        $this->item    = $this->request->param('section2');
        
    }
  
    public function action_index () {
        
        $this->layout
             ->set('htmlBanner' ,false)
             ->set('banner', null)
             ->set( 'content', View::factory('layout/404'));
    }
}