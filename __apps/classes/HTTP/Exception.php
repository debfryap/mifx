<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Exception Class
 * @Module      Application
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class HTTP_Exception extends Kohana_HTTP_Exception {
    /**
     * Generate a Response for all Exceptions without a more specific override
     * 
     * The user should see a nice error page, however, if we are in development
     * mode we should show the normal Kohana error page.
     * 
     * @return Response
     */
    public function get_response()
    {
        // Lets log the Exception, Just in case it's important!
        // Kohana_Exception::log($this);
 
        if (Kohana::$environment >= Kohana::DEVELOPMENT)
        {
            // Show the normal Kohana error page.
            return parent::get_response();
        }
        else
        {            
            $attributes['package']    = 'error'; 
            /**
             *  add code here
             *  Compro -  Custom template error
             *  403, 404, 400, 500 
             */
            $a = (array) parent::get_response();
            $code = null;
            
            $key = 0;            
            foreach($a as $val){
                
                if($key == 0)
                    $code = $val;
                    
                $key++;
            }
            if($code == 404)
                header('Location:'. URL::front('errors/'.$code));
            else{
            
                               
            // Generate a nicer looking "Oops" page.
            $view = View::factory('__system/general/error');
            $response = Response::factory()
                ->status($this->getCode())
                ->body($view->render());
 
            return $response;
            
            }
        }
    }
}