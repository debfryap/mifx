<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author    Daniel Simangunsong
 * @Company   Webarq
 * @copyright   2012
 * @Package     Account Email Template 
 * @Module      ~
 * @License   Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors  
**/ 

//get session
// $submit_form = Session::instance()->get('submit-form-account');

// if(!isset($submit_form)){

//   if($submit_form){
//     App::$session->set('submit-form-account', false);
//   } else {
//     echo '<script>window.location="http://mifx.com";</script>';
//   }
// }else{
//   echo '<script>window.location="http://mifx.com";</script>';
// }


if(isset($type) and $type =='Demo'){ ?>

<script type="text/javascript">
    var fb_param = {};
    fb_param.pixel_id = '6008224195327';
    fb_param.value = '0.00';
    fb_param.currency = 'USD';
    (function(){
      var fpw = document.createElement('script');
      fpw.async = true;
      fpw.src = '//connect.facebook.net/en_US/fp.js';
      var ref = document.getElementsByTagName('script')[0];
      ref.parentNode.insertBefore(fpw, ref);
    })();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008224195327&amp;value=0&amp;currency=USD" /></noscript>
<?php  } ?>
<?php if(isset($type) and $type =='Live'){ ?>
<script type="text/javascript">
    var fb_param = {};
    fb_param.pixel_id = '6008224195927';
    fb_param.value = '0.00';
    fb_param.currency = 'USD';
    (function(){
      var fpw = document.createElement('script');
      fpw.async = true;
      fpw.src = '//connect.facebook.net/en_US/fp.js';
      var ref = document.getElementsByTagName('script')[0];
      ref.parentNode.insertBefore(fpw, ref);
    })();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008224195927&amp;value=0&amp;currency=USD" /></noscript>
<?php }?>

<h3 class="norborder" style="margin-top:0; margin-bottom:10px;"><?php echo __('title-success-page-account', array(':type' => isset($type) ? $type : 'demo' )) ?></h3>

<?php echo (isset($page) and is_object($page)) ? $page->intro : '' ; ?>
<br />
<?php echo (isset($page) and is_object($page)) ? $page->description : '' ; ?>