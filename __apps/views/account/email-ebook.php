<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/ 

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <p>
            Silahkan klik <?php echo HTML::anchor($url,'link');?> ini, atau copy alamat <?php echo $url;?> ke browser anda, untuk membuka
            e-book <?php echo $ebook; ?>
        </p>
        
        <p>
            <?php echo __('thank-you-title'),'<br/>','<i>',__('computer-generated-message'),'</i>','<br/>'; ?>
        </p>
    </body>
</html>