<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * $confirm = $client->registerDemo(array(
                        'kota'          => $post->accountkota,
                        'name'          => $post->accountname,
                        'phone'         => $post->accountphone,
                        'email'         => $post->accountemail,
                        'referal'       => $post->accountreferral,
                        'deposit'       => $post->accountdeposit,
                        'information'   => $post->accountinformation                            
                    ));
                    
 * Calm seas, never make skillfull sailors	
**/ if (empty($post) || empty($post->accountname)) { echo 'Invalid request. Please contact your administrator.'; return; }  ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <p>
        Kepada yang terhormat <?php echo $post->accountname; ?>,
        </p>
        
        <p>
            Terima kasih telah membuka <?php //echo $type; ?> account di MONEX INVESTINDO FUTURES, namun <?php echo $post->accountname; ?> masih perlu untuk melakukan aktivasi.
        </p>
        
        <p>
        Silahkan klik <?php echo HTML::anchor($url,'link ini',array('style'=>'text-decoration:none;color:blue;')); ?>,
        atau copy paste alamat <span style="color: blue;"><?php echo $url; ?></span> ke browser yang <?php echo $post->accountname; ?> pakai.
        </p>
        
        <p>
            <?php echo __('thank-you-title'),'<br/>','<i>',__('computer-generated-message'),'</i>','<br/>'; ?>
        </p>
    </body>
</html>