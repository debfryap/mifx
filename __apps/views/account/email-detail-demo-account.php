<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/ if (empty($data) || empty($data['demo'])) { echo 'Invalid request. Please contact your administrator.'; return; }  

/**
         array(2) (
                "status" => string(7) "success"
                "data" => array(3) (
                    "demo" => array(3) (
                        "no_account" => string(9) "100712425"
                        "password" => string(6) "A5qPmX"
                        "investor" => string(6) "hPEbft"
                    )
                    "marketing" => array(4) (
                        "name" => string(20) "Sundari Indrasariani"
                        "kode" => string(7) "b0002rg"
                        "wp_code" => string(5) "WP030"
                        "email" => string(16) "sundari@mifx.com"
                    )
                    "client" => array(2) (
                        "username" => string(18) "survey02@izkey.com"
                        "password" => string(32) "fb20ba87010b4b1bf977303061edfbaf"
                    )
                )
            )  
 ) 
**/ 
$demo       = $data['demo'];        extract($demo,EXTR_PREFIX_ALL,'demo');
$marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
$client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <p>Kepada Yth,</p>
        
        <p>Selangkah lagi anda bergabung dengan Monex Investindo Futures.<br />Berikut data details untuk demo account anda</p>
        
        <table style="padding:10px;border:0;border-collapse:collapse;">
            <tbody>
                <tr>
                    <td style="background-color: #E2F4F6;padding:3px 10px;" colspan="2">ACCOUNT DEMO</td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">No. Account</td>
                    <td>: <?php echo $demo_no_account; ?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Password</td>
                    <td>: <?php echo $demo_password; ?></td>
                </tr>
                
                <tr><td>&nbsp;</td><td></td></tr>
                
                <tr>
                    <td style="background-color: #E2F4F6;padding:3px 10px;" colspan="2">CLIENT AREA DETAIL</td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">Login Username</td>
                    <td>: <?php echo $client_username; ?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Login Password</td>
                    <td>: <?php echo $demo_password; ?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Login URL</td>
                    <td>: <a href="http://client.mifx.com" style="text-decoration: none;">http://client.mifx.com</a></td>
                </tr>
                
                <tr><td>&nbsp;</td><td></td></tr>
                
                <tr>
                    <td style="background-color: #E2F4F6;padding:3px 10px;" colspan="2">MARKETING DETAIL</td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Marketing Name</td>
                    <td>: <?php echo $marketing_name;?></td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">Marketing Email</td>
                    <td>: <?php echo $marketing_email;?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Marketing Phone</td>
                    <td>: <?php echo $marketing_phone;?></td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">Branch Phone</td>
                    <td>: <?php echo $marketing_phone_office;?></td>
                </tr>
            </tbody>
        </table>
        
        <p><strong>Link Download Monex Trader : <?php echo HTML::anchor( URL::base(true) . 'platform/mifx4setup.exe' ); ?></strong></p>
        
        <p>Data penting demo account berada dalam client area.</p>
        <p>
            <?php echo __('thank-you-title'),'<br/>','<i>',__('computer-generated-message'),'</i>','<br/>'; ?>
        </p>
    </body>
</html>