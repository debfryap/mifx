<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author    Daniel Simangunsong
 * @Company   Webarq
 * @copyright   2012
 * @Package     Account Email Template 
 * @Module      ~
 * @License   Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors  
**/ 
?>

<?php if(isset($type) and $type =='Demo'){ ?>

<script type="text/javascript">
    var fb_param = {};
    fb_param.pixel_id = '6008224195327';
    fb_param.value = '0.00';
    fb_param.currency = 'USD';
    (function(){
      var fpw = document.createElement('script');
      fpw.async = true;
      fpw.src = '//connect.facebook.net/en_US/fp.js';
      var ref = document.getElementsByTagName('script')[0];
      ref.parentNode.insertBefore(fpw, ref);
    })();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008224195327&amp;value=0&amp;currency=USD" /></noscript>
<?php  } ?>
<?php if(isset($type) and $type =='Live'){ ?>
<script type="text/javascript">
    var fb_param = {};
    fb_param.pixel_id = '6008224195927';
    fb_param.value = '0.00';
    fb_param.currency = 'USD';
    (function(){
      var fpw = document.createElement('script');
      fpw.async = true;
      fpw.src = '//connect.facebook.net/en_US/fp.js';
      var ref = document.getElementsByTagName('script')[0];
      ref.parentNode.insertBefore(fpw, ref);
    })();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008224195927&amp;value=0&amp;currency=USD" /></noscript>
<?php }?>
<div class="container">
<?php if (isset($type) && $type=='e-book'){?>
<p class="title_1">Terima Kasih telah mendaftarkan e-book.</p>
<?php }
elseif (isset($type) && $type=='mte'){
	echo "<p class='title_1'>Terima Kasih telah mendaftarkan MTE.</p>";
}
else{
	?>
<div class="img_step">
	
         <p class="title_1"><?php echo  __('thank-you-for-opening-'. strtolower($type) .'-account-with-us'); ?></p>
         <p style="font-size: 16px;"><?php echo __('its-so-easy-to-open-an-account-with-monex'); ?></p>
         <div class="step_pic">
            <span data-picture data-alt="sdfsd">
               <span data-src="<?php echo URL::templateImage('content/accounts/step-2-small.png'); ?>" alt=""></span><!-- Ukuran yg disarankan xpx -->
               <span data-media="(min-width: 321px)" data-src="<?php echo URL::templateImage('content/accounts/step-2-small.png'); ?>"></span><!--  Ukuran yg disarankan 480xpx -->
               <span data-media="(min-width: 481px)" data-src="<?php echo URL::templateImage('content/accounts/step-2-middle.png'); ?>"></span><!--  Ukuran yg disarankan 768xpx -->
               <span data-media="(min-width: 940px)" data-src="<?php echo URL::templateImage('content/accounts/step-2-large.png'); ?>"></span><!--  Ukuran yg disarankan 940xpx -->
            </span>
         </div>
          <ul class="step_name">
            <li class="step_sub active"><?php echo __('submit-registration'); ?></li>
            <li class="step_reg"><?php echo __('verify-registration'); ?></li>
            <li class="step_user"><?php echo __('username-and-password'); ?></li>
            <li class="step_acc"><?php echo __('login-account'); ?></li>
         </ul>
      </div> 
    </div>
     <div class="container">
        <div class="step_info">
           <p class="title_1"><?php echo __('check-email-form-usename-and-password'); ?></p>
           <p style="font-size: 16px;"><?php echo __('congratulations-your-live-account-has-been', array(':mail' => Session::instance()->get('accountemail'))); ?></p>
           <p></p>
           <img src="<?php echo URL::templateImage('content/accounts/img-tutor-3.png'); ?>" alt=""/>
           <p>&nbsp;</p>
           <a target="_blank" href="<?php echo URL::base().'platform/mifx4setup.exe'; ?>" class="btn1">
              <img src="<?php echo URL::templateImage('content/accounts/icon-storage.png'); ?>" style="margin-right:10px" class="left"><?php echo __('download-monex-trader'); ?></a>
        </div>
 <?php } ?>       
     </div>
