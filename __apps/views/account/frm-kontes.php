<?php defined('SYSPATH') or die('No direct script access.'); 
 

/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>
<style>
.nine-stars .btn_submit {
    background: url("<?php echo URL::templateImage('material/bg_black.jpg');?>") repeat-x scroll 0 0 transparent;
    color: #FFFFFF;     font-family: myriad_semi;
    font-size: 16px; font-weight: normal;
    height: 37px; width: 123px; cursor: pointer;
    margin-right: 11px;  margin-left: 60px;
}
.nine-stars .input-text {
    border-radius: 0; height: 28px; border-left: 0;
}
</style>
<div class="container">
    <h3>DEMO REGISTRASI <?php echo empty($group) ? 'NINESTARS' : strtoupper($group); ?></h3>
    
    <?php echo Form::open(null,array( 'id'=>'frm-nine-stars' )); ?>
    <table width="50%">
    <tr>
        <td>Name</td>
        <td>: <?php echo Mifx::elementForm('name');?></td>
    </tr>
    <tr>
        <td>Phone</td>
        <td>: <?php echo Mifx::elementForm('phone');?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td>: <?php echo Mifx::elementForm('email');?></td>
    </tr>
    <tr>
        <td>Deposit</td>
        <td>: <?php echo Mifx::elementForm('deposit2');?></td>
    </tr>
    </table>
    
    
    

    <div style="width: 520px;padding:10px 0;" class="nine-stars">        
        <div style="padding-bottom: 10px;">
            *Please enter the letters as they are shown in the image below. Letters are not case-sensitive.
        </div>
        
        <p style="float: left; margin-top:10px;">Captcha <sup style="color: red;">*</sup></p>
        <p style="float: left; margin-left:10px;"><?php echo Mifx::captcha(); ?></p>
        <p style="float: left;"><?php echo Form::input('captcha',null, array( 'class'=>'required input-text') );?></p>
        <p style="clear: both;"><?php echo Form::submit('submit','Submit', array( 'class'=>'btn_submit') );?></p>
    </div>
    
    <?php echo Form::close(); ?>
</div> 


<script type="text/javascript">
    $(document).ready(function(){
        $('form#frm-nine-stars').validate();        
    });
</script>

<?php
return;
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ 

//Check for query Source
$qSource = Request::$initial->query('Source');

//Check for session Source
if (empty($qSource)) {
    $qSource = Session::instance()->get('Source');                
}

$qSource = empty($qSource) ? '' : "?Source=$qSource";

$post    = Request::$initial->post('');

//$accountUri    = URL::front('accounts');
$accountUri    = Translate::uri(Language::$current,'accounts');


$elementSubmit = Form::submit('submit','SUBMIT',array('class' => 'btn_submit'));

$demoActionForm = Translate::uri(Language::$current,'ajax/submit-demo-account');
$liveActionForm = Translate::uri(Language::$current,'ajax/submit-live-account');
                                                                                    
echo '<div class="account_wrap', (!empty($class) ? " $class" : ""), '">'
        
        //Demo Account
        ,'<div class="create-account-box demo-acc">'
        
        ,Form::open( $demoActionForm , array ( 'id' => 'frm-demo-account', 'class' => 'is-validate' ) )
        
        ,'<h3>'
            ,'<span>Create Demo Account</span>'
            ,HTML::image( URL::templateImage( 'material/arr_down.png' ),array( 'alt' => '' ) )
        ,'</h3>'
        , Mifx::elementForm('kota')
        , Mifx::elementForm('name')
        , Mifx::elementForm('phone')
        , Mifx::elementForm('email')
        , Mifx::elementForm('deposit')
        , Mifx::elementForm('referral')
        , Mifx::elementForm('source')
        , Mifx::elementForm('info')
        
        ,'<p class="agree">'
        , Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) )
        , __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') )
                 ))
        ,'</p>'
        
        //, Form::hidden('captcha',null,array('class'=>'verif required'))
        , Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4 ))
        , '<div class="captcha captcha-container">',Mifx::captcha(),'</div>'
        , '<p>', $elementSubmit ,'</p>'        
        , Form::close()
        
        ,'</div>'
        
        
        
        ,'<div class="btn_tab"><span class="demo_acc"></span></div>'                                   
     ,'</div>';
     
    if (!isset($idpopmedia) || $idpopmedia !== false)  
        echo '<div id="bpopup-media">',__('do-not-close-browser'),'</div>';
    
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.btn_tab').children('span').click(function(){
            
            //$(this).siblings('span').removeClass('active');  
            $(this).addClass('active');
              
            boxid = $(this).index();
            
            $('.create-account-box.active').removeClass('active');
            
            $('.create-account-box').eq(boxid).addClass('active');
        });
        
        //Validate default value for each element
        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            return !(element.value == param);
        });
            
        $('#live-Phone,#demo-Phone').numeric({
            allow : '-'
        }); 
        
        var captchaCount = 1;
        $('form.is-validate').each(function(){
            $(this).validate({
                rules            : {
                    accountName  : {
                        required : true,
                        notEqual : '<?php echo __('full-name'); ?>'
                    },
                    accountPhone : {
                        required : true,
                        notEqual : '<?php echo __('phone'); ?>'
                    } /**,
                    accountReferral : {
                        required : true,
                        notEqual : '<?php echo __('referral'); ?>'
                    } **/
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { },
                submitHandler  : function(form) {
                    
                    frmID = $(form).attr('id');
                    
                    //nous_process_message( $('#bpopup-media').html() , 1 );
                    nous_process_message_new();
                    
                    setTimeout(function(){                            
                        serialized = $(form).serialize();
                        $.ajax({
                            url     : form.action,
                            type    : 'POST',
                            data    : serialized,
                            async   : false,
                            success : function(res) {
                                str = $.trim(res);
                                if (str === 'success') {                                    
                                    if (frmID == 'frm-demo-account')
                                        window.location.href = '<?php echo $accountUri; ?>/create-free-demo-account-success<?php echo $qSource;?>';
                                    else
                                        window.location.href = '<?php echo $accountUri; ?>/create-monex-live-account-success<?php echo $qSource;?>';
                                    
                                    return;
                                } else {
                                    if (str.substr(0,5) == 'goTo:') {
                                        newUrl = str.substr(5);
                                        window.location.href = newUrl;
                                    } else {                                    
                                        nous_disable_message();
                                        // check captcha
                                        if(res=="<ul class=\"error\"><li>Captcha masih salah. Silahkan masukkan sekali lagi</li></ul>" || res=="<ul class=\"error\"><li>Captcha did not match. Please correct</li></ul>"){
                                           
                                            if(captchaCount==3){
                                                var action_captcha = $('.captcha-container img').attr('src');
                                                var img =$("<img />").attr('src', action_captcha)
                                                                        .load(function() {
                                                                            if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                                                                                alert('broken image!');
                                                                            } else {
                                                                                $(".captcha-container").html(img);
                                                                            }
                                                                        });
                                                
                                                captchaCount = 0;
                                            }
                                            
                                            captchaCount++;
                                        }
                                        jAlert(res);
                                    }
                                }
                            },
                            error   : function() {                                    
                                nous_disable_message();
                                
                                jAlert('<?php echo __('server-not-responding');?>');
                            }                         
                        });
                    },180);
                    
                    return false;
                    /** **/
                }
            });
        })
                
    });
</script>
 
<?php
return; 