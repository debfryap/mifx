<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/ if (empty($data) || empty($data['client'])) { echo 'Invalid request. Please contact your administrator.'; return; }  

/**
$verify = array(
                        "status" => string(7) "success"
                        "data" => array(
                            "client" => array(4) (
                                "username" => string(18) "survey17@izkey.com"
                                "password" => string(6) "abpj3h"
                                "name" => string(8) "survey17"
                                "link" => string(177) "http://client.mifx.com/site/loginemail?key=z5m474o463m4z5w5r4i5s4p2b4h4b486s55426p294w5u5y2j4h4b4h4e4c4n444z5k4y21384m5w2t2i58464c453s2j5b4z2g5t2x2m584y2l5942484z2x2j5b4t20364s2"
                            )
                            "marketing" => array(4) (
                                "email" => string(22) "titik.chomsah@mifx.com"
                                "name" => string(13) "Titik Chomsah"
                                "kode" => string(7) "H0002RG"
                                "kode_wp" => string(5) "WP020"
                            )
                        )
                    ); 
**/
$marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
$client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <p>
        Kepada Yth <?php echo $client_name ;?>, berikut data account live Anda
        </p>
        
        <table style="padding:10px;border:0;border-collapse:collapse;">
            <tbody>
                <tr>
                    <td style="background-color: #E2F4F6;padding:3px 10px;" colspan="2">CLIENT AREA DETAIL</td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">Login Username</td>
                    <td>: <?php echo $client_username; ?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Login Password</td>
                    <td>: <?php echo $client_password; ?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Auto Login URL</td>
                    <td>: <?php echo $client_link; ?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Login URL</td>
                    <td>: <a href="http://client.mifx.com" style="text-decoration: none;">http://client.mifx.com</a></td>
                </tr>
                
                <tr><td>&nbsp;</td><td></td></tr>
                
                <tr>
                    <td style="background-color: #E2F4F6;padding:3px 10px;" colspan="2">MARKETING DETAIL</td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Marketing Name</td>
                    <td>: <?php echo $marketing_name;?></td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">Marketing Email</td>
                    <td>: <?php echo $marketing_email;?></td>
                </tr>
                <tr>
                    <td style="padding:3px 10px;">Marketing Phone</td>
                    <td>: <?php echo $marketing_phone;?></td>
                </tr>
                <tr>
                    <td style="width:120px;padding:3px 10px;">Branch Phone</td>
                    <td>: <?php echo $marketing_phone_office;?></td>
                </tr>
            </tbody>
        </table>
        
        <p><strong>Link Download Monex Trader : <?php echo HTML::anchor( URL::base(true) . 'platform/mifx4setup.exe' ); ?></strong></p>
        
        <p>
            <?php echo __('thank-you-title'),'<br/>','<i>',__('computer-generated-message'),'</i>','<br/>'; ?>
        </p>
    </body>
</html>