<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/ if (empty($data) || empty($data['demo'])) { echo 'Invalid request. Please contact your administrator.'; return; }  

/**
 Array ( 
    [status] => success 
    [data] => Array ( 
        [demo] => Array ( 
            [no_account] => 100708899 
            [password]   => 1tSq7C 
            [investor] => k1qZPa 
        ) 
        [marketing] => Array ( 
                [name] => Sundari Indrasariani 
                [kode] => b0002rg 
                [wp_code] => WP030 
                [email] => sundari@mifx.com 
        ) 
        [client] => Array ( 
            [username] => pegel.linuxs@gmail.com 
            [password] => 93c6466f4992dba0fd5687ced32f4ddd 
        ) 
    ) 
 ) 
**/ 
$demo       = $data['demo'];        extract($demo,EXTR_PREFIX_ALL,'demo');
$marketing  = $data['marketing'];   extract($marketing,EXTR_PREFIX_ALL,'marketing');
$client     = $data['client'];      extract($client,EXTR_PREFIX_ALL,'client');

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <p>
        Berikut data yang dapat anda gunakan untuk login di Client Area MIFX
        </p>
        
        <table style="padding:10px;border:0;border-collapse:collapse;">
            <tbody>
                <tr>
                    <td style="font-weight: bold;width:120px;">Login Demo</td>
                    <td>: <?php echo $demo_no_account; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Login Username</td>
                    <td>: <?php echo $client_username; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Login Password</td>
                    <td>: <?php echo $demo_password; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Login URL</td>
                    <td>: <a href="http://client.mifx.com" style="text-decoration: none;">http://client.mifx.com</a></td>
                </tr>
            </tbody>
        </table>
        <p>
            <?php echo __('thank-you-title'),'<br/>','<i>',__('computer-generated-message'),'</i>','<br/>'; ?>
        </p>
    </body>
</html>