<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>
<style>
.nine-stars .btn_submit {
    background: url("<?php echo URL::templateImage('material/bg_black.jpg');?>") repeat-x scroll 0 0 transparent;
    color: #FFFFFF;     font-family: myriad_semi;
    font-size: 16px; font-weight: normal;
    height: 37px; width: 123px; cursor: pointer;
    margin-right: 11px;  margin-left: 60px;
}
.nine-stars .input-text {
    border-radius: 0; height: 28px; border-left: 0;
}
</style>
<div class="container">
    <h3>DEMO REGISTRASI <?php echo empty($group) ? 'NINESTARS' : strtoupper($group); ?></h3>
    
    <?php echo Form::open(null,array( 'id'=>'frm-nine-stars' )); ?>
    
    <div style="width: 520px;padding:10px 0;" class="nine-stars">        
        <div style="padding-bottom: 10px;">
            *Please enter the letters as they are shown in the image below. Letters are not case-sensitive.
        </div>
        
        <p style="float: left; margin-top:10px;">Captcha <sup style="color: red;">*</sup></p>
        <p style="float: left; margin-left:10px;"><?php echo Mifx::captcha(); ?></p>
        <p style="float: left;"><?php echo Form::input('captcha',null, array( 'class'=>'required input-text') );?></p>
        <p style="clear: both;"><?php echo Form::submit('submit','Submit', array( 'class'=>'btn_submit') );?></p>
    </div>
    
    <?php echo Form::close(); ?>
</div> 


<script type="text/javascript">
    $(document).ready(function(){
        $('form#frm-nine-stars').validate();        
    });
</script>