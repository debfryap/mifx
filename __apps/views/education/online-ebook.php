<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


if (empty($list)) { echo '<h2 class="title_page">',__('no-data-found-title'),'</h2><p>',__('no-data-found-description'),'</p>'; return; }
    
echo '<h2 class="title_page">', (!empty($title) ? $title : '') ,'</h2>';

echo '<div id="other_menu">',HTML::image(URL::templateImage('material/list_ico.jpg',null)),__('list-item'),'</div><div class="list_menu">',$list,'</div>';

if (empty($item)) return;

echo '<div class="detail_event">',
        '<div class="tit_page" style="margin-top:15px;"><strong>',$item->label,'</strong></div>',
        Page::sosialMedia(Page::get('completedNavigationUri:tutorial')),
     '</div>',
     $item->description;
     
     
echo '<div class="jump_page">';

if (!empty($item->prev)) echo $item->prev;
if (!empty($item->next)) echo $item->next;

echo '</div>';            
					
?>