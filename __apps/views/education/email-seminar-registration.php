<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/ ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    <body>
        <?php
            if (empty($cc)) {
                echo __('thank-you-for-registering-seminar');
            } else {
                echo '<p>',__('mcs-body-mail',array(':nama'=>$post->seminarfirstname.' '.$post->seminarlastname)),'<p>';
            }
        ?>
        
        <table style="padding:10px;border:0;border-collapse:collapse;">
            <tbody>
                <tr>
                    <td style="font-weight: bold;width:120px;"><?php echo __('full-name'); ?></td>
                    <td>: <?php echo $post->seminarfirstname.' '.$post->seminarlastname; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;width:120px;"><?php echo __('seminar'); ?></td>
                    <td>: <?php echo $post->seminarlabel; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;width:120px;"><?php echo __('date'); ?></td>
                    <td>: <?php echo $post->seminardate; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;width:120px;"><?php echo __('gender'); ?></td>
                    <td>: <?php echo $post->seminargender; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('first-name'); ?></td>
                    <td>: <?php echo $post->seminarfirstname; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('last-name'); ?></td>
                    <td>: <?php echo $post->seminarlastname; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('email'); ?></td>
                    <td>: <?php echo $post->seminaremail; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('phone'); ?></td>
                    <td>: <?php echo $post->seminarphone; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('city'); ?></td>
                    <td>: <?php echo $post->seminarkota; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('address'); ?></td>
                    <td>: <?php echo $post->seminaraddress; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;"><?php echo __('quantity-ticket'); ?></td>
                    <td>: <?php echo $post->quantity; ?></td>
                </tr>
            </tbody>
        </table>
        
        <p>
            <?php echo __('thank-you-title'),'<br/>','<br/>'; ?>
        </p>
    </body>
</html>