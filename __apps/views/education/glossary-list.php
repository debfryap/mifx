<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>
<h2 class="title_page"><?php echo Page::get('simpleBreadCrumb:trading-glossary'); ?></h2>
    
<?php 
    
    echo '<div class="trade_menu">';
    
    if ($alpha->valid()) {
        echo '<ul class="alfabeth">';
        
        $alpha_array = array();
        
        foreach ($alpha as $a) {
            
            $a = Translate::item('glossaries', $a);
            
            if(count($alpha_array) == 0) {
                
                $alpha_array[0] = substr($a->label, 0, 1);
                $result = 1;
                
                echo '<li>',
                    HTML::anchor(Page::sourceCorrection('trading-glossary','filter/' . strtolower(substr($a->alpha, 0, 1))) , substr($a->label,0, 1) ),
                 '</li>';
            
            }else {
                $result = array_search(substr($a->label, 0,1), $alpha_array);
            
            }
            
            if($result==false and $result !==0) {
        
                array_push($alpha_array, substr($a->label, 0, 1));
                
                echo '<li>',
                    HTML::anchor(Page::sourceCorrection('trading-glossary','filter/' . strtolower(substr($a->alpha, 0, 1))) , substr($a->label,0, 1) ),
                 '</li>';
            }
        }
        
        echo '</ul>';
    }
    
    echo '<div class="s_trade">
			<input id="glossary-keyword" type="text" placeholder="'. __('search') .'" value="',(isset($search) ? $search : __('search')),'" />
			<input type="submit" value="" class="btn_s glossary-search"/>
		  </div>';
    
    echo '</div>';
    
    if (!$list->valid())
    {
        echo __('no-glossary-found');
    } else {
    
        echo $page;    
        
        echo '<ul class="sorter_list">';    
        foreach ($list as $item) {
            // echo Debug::vars($item); 
            $item =  Translate::item('glossaries',$item);
            
            echo '<li>',
                    HTML::anchor(Page::sourceCorrection('trading-glossary',$item->permalink),$item->label),
                 '</li>';
        }
        echo '</ul>';
        
        echo $page;
    }    
?>

<script type="text/javascript">
    $(document).ready(function(){
        console.log('Zoom');
        $('.glossary-search').click(function(){
            var keyword = $.trim($('#glossary-keyword').val());
            if (keyword !== '') {
                window.location.href = '<?php echo Page::get('completedNavigationUri:trading-glossary');?>/search/'+keyword;
            }             
        });           
        
        $('#glossary-keyword').keydown(function(e){
            code = (e.keyCode ? e.keyCode : e.which);
           
            if (code == 13) {
                var keyword = $.trim($(this).val());
                if (keyword !== '') {
                    window.location.href = '<?php echo Page::get('completedNavigationUri:trading-glossary');?>/search/'+keyword;
                }             
            }
        });    
    });
</script>