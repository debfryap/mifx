<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>

<h2 class="title_page"><?php echo $title; ?></h2>


<?php
    echo '<div class="vid_wrp">';
    if (!empty($current)) {
        echo Page::video($current->source,590,360),
             '<p>',$current->label,'</p>',
             $current->description ;
    } 
    echo '</div>'; 
    
    echo '<span class="font_16">'. __('tutorial-monex-mt4') .'</span>'; 
    
    if (!empty($others)) {
        echo '<div class="sl_wrp">';
            echo '<a href="#" class="bt_next2 cont">Next</a>',
            
                 '<div class="slide_little">',
                 
                 '<ul>';
                 
            foreach ($others as $v) {
                echo '<li>',
                        HTML::anchor( Page::sourceCorrection('video',$v->permalink) , Page::youtubeImage($v->source,1,array('style'=>'height:120px;')) ),
                        '<br /> ',
                        $v->label,
                     '</li>';        
            }
            
            echo '</ul>',
            
                 '</div>',
                 
                 '<a href="#" class="bt_prev2 cont"></a>';
        echo '</div>';    
    }
    
    echo '<div class="btn_option">';
    
    if (!empty($tools)) {
        foreach ($tools as $t) {            
            $t = Translate::item('site_navigations',$t);
            $m = Model::factory('Medialibrary')->simple_application('tool-video',$t->id,'site_navigations')->current();            
            $a = array();
            if (strpos($t->permalink,'http') === 0) {
                $h = $t->permalink;
                $a['target'] = '_blank';
            } else {
                if ($t->permalink == 'trading-tools')
                    $h = Page::get('completedNavigationUri:trading-products');
                elseif ($t->permalink == 'platform-tutorial')
                    $h = Page::get('completedNavigationUri:online-trading');
                else
                    $h = Page::sourceCorrection('video',$t->permalink);
            }
            //$h = strpos($t->permalink,'http') === 0 ? $t->permalink : ( Page::get('completedNavigationUri:video') . '/' . $t->permalink );
            
            $l = empty($m->folder) ? $t->label : ( HTML::image( URL::mediaImage( $m->folder . '/' . $m->file, null)) . " $t->label" );  
            echo '<p>',
                    HTML::anchor($h,$l,$a),
                 '</p>';
        }    
    }
    
    echo '</div>'; ?>