<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


if (isset($soap) && $soap === true) {
    
} else if (!empty($list)) {
    echo '<div class="schedul_edu seminar">';
        echo '<h3>'. __('corporate-events-title-list') .'</h3>';
        echo '<p>'. __('select-a-date-to-attend') .'	<span class="sptr">:</span>';
        #echo '<select><option>Select Date</option></select>';
        
        echo '<select name ="selectDate" id="selectDate">';
        echo '<option value="">',__('select-one'),'</option>';
        
        $inDate = array();
        
        foreach ($list as $item) {
            
            if (in_array($item->date,$inDate)) continue; 
            
            $inDate[] = $item->date;
            
            //echo '<option value="',$item->permalink,'">',Page::datetime($item->date,null,$item->region),'</option>';    
            echo '<option value="',$item->date,'">',Page::datetime($item->date,null,$item->region),'</option>';
        }
        echo '</select>';
        
        echo '</p>';
            
        echo '<table width="100%">' ,
            
                '<tr>' ,
        		'<th>'. __('event') .'</th>' ,
        		'<th width="30%">'. __('ends') .'</th>' ,
        		'<th>'. __('quantity') .'</th>' ,
        	    '</tr>';
        
        $ilist = 1;
        $inline_html = '';
        foreach ($list as $item) {
            echo '<tr class="toggle-seminar" id="toggle-',$item->date,'">'; 
            
            if (!empty($item->description)) {
                $inline_link  = "#inline_content$ilist"; 
                $inline_html .= '<div id="inline_content'.$ilist.'">
                                    <div class="title_box">
                                        <span>Training & Seminar</span>
                                    </div>';
                                    
                                    
                $media = Model::factory('Medialibrary')->simple_application('image-monex-schedule', $item->id, 'monex_schedules')->current();
                
                if(isset($media->folder))
                    $inline_html .= '<img src="'.URL::mediaImage($media->folder .'/'. $media->file ,null).'" alt="" style="float:left; margin-right:20px;"/>';
                else
                    $inline_html .= '<img src="'.URL::templateImage('content/plan_trade.jpg',null).'" alt="" style="float:left; margin-right:20px;"/>';
                    
                $inline_html .= $item->description .'</div>';

                $inline_attrs = array( 'class'=>'inline' );
                $ilist++; 
            } else {
                $inline_attrs = null;
                $inline_link = '#';
            }
            
            echo '<td>', 
                    HTML::anchor( "$inline_link" , $item->label , $inline_attrs ),
                    '<br/> <span class="date">',Page::datetime($item->date,null,$item->region),'</span>',
                    '</td>';
            echo '<td>',$item->closing_info,'</td>';
            echo '<td>';
            //echo Form::select('quantity',array('1'=>'1','2'=>2),null,array('id'=>"quantity-$item->permalink"));
            echo Mifx::select_ticket(3,null,array('id'=>"quantity-$item->permalink","class"=>"ticket-selector","data-permalink"=>$item->permalink));
            echo '</td>';
            echo '</tr>';  
        }
        
        echo         
            '</table>' ,
            
            '<div style="display:none">' , $inline_html , '</div>' ,
            
            '<div class="paging">' ,
            $page ,
            ( empty($page)
                ? HTML::anchor( '#', HTML::image( URL::templateImage('material/btn_regis.png',null) ) , array ( 'style'=>'float:right;display:none;','id'=>'goToReservation' ) ) 
                : HTML::anchor( '#', HTML::image( URL::templateImage('material/btn_regis.png',null) ) , array ( 'style'=>'float:right;margin-top:-35px;display:none;','id'=>'goToReservation'  ) ) ),
            '</div>' ,
        '</div><br style="clear: both;" /><br />'; 

    if(!empty($list_branch)){
        // Branch Event
        echo '<div>';
        echo '<div class="schedul_edu seminar">';
        echo '<h3>'. __('branch-event-title') .'</h3>';
        #echo '<p>Select a Date to Attend    <span class="sptr">:</span>';
        #echo '<select><option>Select Date</option></select>';
        
        #echo '<select name ="selectDate" id="selectDate">';
        #echo '<option value="">',__('select-one'),'</option>';
        
        $inDate = array();
        
        foreach ($list_branch as $item) {
            
            if (in_array($item->date,$inDate)) continue; 
            
            $inDate[] = $item->date;
            
            //echo '<option value="',$item->permalink,'">',Page::datetime($item->date,null,$item->region),'</option>';    
            echo '<option value="',$item->date,'">',Page::datetime($item->date,null,$item->region),'</option>';
        }
        echo '</select>';
        
        echo '</p>';
            
        echo '<table width="100%">' ,
            
                '<tr>' ,
                '<th>'. __('event') .'</th>' ,
                '<th>'. __('location') .'</th>' ,
                '</tr>';

        if($list_branch->count()==0){
            echo '<tr><td class="date"> '. __('empty') .' </td><td class="date"> '. __('empty') .' </td></tr>';
        }
        
        $ilist = 1;
        $inline_html = ''; 
        foreach ($list_branch as $item) {
            echo '<tr class="toggle-seminar" id="toggle-',$item->date,'">'; 
            
            if (!empty($item->description)) {
                $inline_link  = "#inline_branch$ilist"; 
                $inline_html .= '<div id="inline_branch'.$ilist.'">
                                    <div class="title_box">
                                        <span>Training & Seminar</span>
                                    </div>';
                
                $media = Model::factory('Medialibrary')->simple_application('image-branch-event', $item->id, 'branch_events')->current();
                
                if(isset($media->folder))
                    $inline_html .= '<img src="'.URL::mediaImage($media->folder .'/'. $media->file ,null).'" alt="" style="float:left; margin-right:20px;"/>';
                else
                    $inline_html .= '<img src="'.URL::templateImage('content/plan_trade.jpg',null).'" alt="" style="float:left; margin-right:20px;"/>';
                    
                $inline_html .= $item->description .'</div>';

                $inline_attrs = array( 'class'=>'inline' );
                $ilist++; 
            } else {
                $inline_attrs = null;
                $inline_link = '#';
            }
             
            echo '<td>', 
                    HTML::anchor( "$inline_link" , $item->label , $inline_attrs ),
                    '<br/> <span class="date">',Page::datetime($item->date,null,$item->region),'</span>',
                    '</td>';
            echo '<td>',$item->location,'</td>';
           
            echo '</tr>';  
        } 
        
        echo         
            '</table>' ,
            '<div style="display:none">' , $inline_html , '</div>' ,
        '</div></div>'; 
    }
}
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.ticket-selector').change(function(){
            var ticketSelector = $(this);
            
            if (ticketSelector.val() == "0") {   
                $('.ticket-selector').removeClass('attended');                      //Remove all attended 
                
                $('a#goToReservation').hide();                                      //Hide registration button
            } else {
                $('.ticket-selector.attended').val("0").removeClass('attended');    //Remove previous attended
                
                ticketSelector.addClass('attended');                                //Set as attended
                
                $('a#goToReservation').show();                                      //Show registration button
            }                
        });
        
        $('#selectDate').change(function(){
            var toggleSeminarDate = $(this).val();
            
            $('.ticket-selector.attended').val("0").removeClass('attended');        //Remove previous attended
            
            if (toggleSeminarDate == "") {
                $('.toggle-seminar').show();                                        //Show all seminar
            } else {                
                $('.toggle-seminar').hide();                                        //Hide irrelevant seminar
                
                $('.toggle-seminar#toggle-'+toggleSeminarDate).show();              //Show seminar based on selected date
            }
        });
        
        $('a#goToReservation').click(function(){
            
            if ($('.ticket-selector.attended').length <= 0) {
                jAlert('<?php echo __('quantity-must-not-empty');?>','<?php echo __('alert'); ?>');
                return false;
            }
            
            $.alerts.okButton = '<?php echo __('yes-button');?>';
            $.alerts.cancelButton = '<?php echo __('no-button');?>';
            jConfirm('<ul><li><?php echo __('is-monex-client');?></li><li><?php echo __('is-contact-by-monex');?></li></ul>','<?php echo __('confirmation');?>',function(res){
                if (res === true) {                    
                    $.alerts.okButton = 'Ok';
                    jAlert('<?php echo __('is-client-or-contacted');?>','<?php echo __('alert');?>');
                    return false;
                } else {
                    var selectedSeminarItem = $('.ticket-selector.attended').attr('data-permalink');
                    window.location.href = '<?php echo Page::get('completedNavigationUri:normal-seminar-training');?>/'+selectedSeminarItem+'?ticket='+$('.ticket-selector.attended').val();      
                }
            });         
        });    
    });
</script>