
<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>
<h2 class="title_page"><?php echo $title; ?></h2>
				
<div class="schedul_edu seminar">
	<h3><?php echo $title; ?></h3>
    
    <?php 
        if (!$list->valid())
        {
            echo __('no-berita-found'), '</div>';
            return;
        }
        echo '<table width="100%">';    
        foreach ($list as $item) {
            $item = Translate::item('news',$item);
            echo '<tr>',
                    '<td>',
                        HTML::anchor(Page::sourceCorrection('artikel',$item->permalink),$item->label),
                        '<br/>',
                        '<span class="date">',Page::datetime($item->date),'</span>',
                    '</td>',
                 '</tr>';
        }
        echo '</table>';
        
        echo $page;    
    ?>
                      
</div>