<?php 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/

 //Soap initialize
Mifx::instance()->soap();
        
//Get soap
$client = Mifx::get('_soap'); 

$kota = $client->getKota();

$post->seminarkota = isset($kota[$post->seminarkota]) ? $kota[$post->seminarkota] : $post->seminarkota;

?>

<table style="padding:10px;border:0;border-collapse:collapse;">
<tbody>
    <tr>
        <td style="font-weight: bold;width:120px;"><?php echo __('full-name'); ?></td>
        <td>: <?php echo $post->seminarfirstname.' '.$post->seminarlastname; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;width:120px;"><?php echo __('seminar'); ?></td>
        <td>: <?php echo $post->seminarlabel; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;width:120px;"><?php echo __('date'); ?></td>
        <td>: <?php echo $post->seminardate; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;width:120px;"><?php echo __('gender'); ?></td>
        <td>: <?php echo $post->seminargender; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('first-name'); ?></td>
        <td>: <?php echo $post->seminarfirstname; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('last-name'); ?></td>
        <td>: <?php echo $post->seminarlastname; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('email'); ?></td>
        <td>: <?php echo $post->seminaremail; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('phone'); ?></td>
        <td>: <?php echo $post->seminarphone; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('city'); ?></td>
        <td>: <?php echo $post->seminarkota; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('address'); ?></td>
        <td>: <?php echo $post->seminaraddress; ?></td>
    </tr>
    <tr>
        <td style="font-weight: bold;"><?php echo __('quantity-ticket'); ?></td>
        <td>: <?php echo $post->quantity; ?></td>
    </tr>
</tbody>
</table>