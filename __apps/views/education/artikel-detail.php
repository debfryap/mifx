<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (empty($article)) { echo __('no-berita-found'); return; }

$banner = !empty($article->media_library['updates-banner']) 
                ? "<p>".HTML::image( URL::mediaImage($article->media_library['updates-banner']->folder . '/' . $article->media_library['updates-banner']->file,null) )."</p>"
                : '<p></p>';
                
echo 
    '<h2 class="title_page">',$article->label,'</h2>',
    '<div class="detail_event">',
        $banner,
        '<div class="tit_page">',
            '<strong>',$article->label,'</strong><br/>',
            Page::datetime($article->date),
        '</div>',
        Page::sosialMedia(Page::get('completedNavigationUri:artikel') . '/' . $article->permalink, $article->label),
    '</div>
    <div class="description_artikel">',
    $article->description,
    '</div>',
    HTML::anchor(Page::get('completedNavigationUri:artikel'),__('back-to-article-list'),array('style'=>'float:right')),
    Mifx::instance()->others_article() ;
?>
<script type="text/javascript">
    function fbs_click(){
      u = location.href;
      t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
      return false;
    }
    
    function linked_click(){
      u = location.href;
      t=document.title;window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&title=' +encodeURIComponent(t)+'&ro=false&summary=&source=','sharer','toolbar=0,status=0,width=626,height=436');
      
      return false;
    }
    
</script>