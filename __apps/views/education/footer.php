<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>

<?php if (!empty($videoDesc)) {
        echo '<div class="video_edu">',
                '<div class="container">',
                    '<div class="text_edu">',
                        '<h3 class="greeny">',(preg_replace('/(monex)/i','<span>$1</span>',$videoDesc->title)),'</h3>',
                        '<div style="max-height: 141px; height: 141px; overflow-y: hidden;">',
                        $videoDesc->description,
                        '</div>',
                        HTML::anchor(Page::get('completedNavigationUri:video'),HTML::image(URL::templateImage('material/btn_see.jpg'))),
                    '</div>',
                    '<div class="video">',
                    !empty($videoItem) 
                        ? HTML::anchor( Page::get('completedNavigationUri:video') . '/' . $videoItem->permalink, Page::youtubeImage($videoItem->source,array('style'=>'width:405px;height:255px;')) ) 
                        : HTML::image(URL::templateImage('content/thumb_vid.jpg',null)),
                    '</div>',
                '</div>',
             '</div>'; 
      } ?>

<div class="artikel_wrap">
	<div class="container">
        <?php
            echo '<h2>' , Page::get('simpleBreadCrumb:artikel') , '</h2>';
            if (!empty($articles)) {
                echo '<ul class="artikel_list">';
                foreach ($articles as $article) {
                    echo '<li>';
                    $newsThumb    = 'news-thumbnail';
                    $medLibConfig = Medialibrary::$config->media_library_application_types->image->$newsThumb;
                    echo '<div style="height:',$medLibConfig->height,'px;';                        
                    if (!empty($article->media_library[$newsThumb])) {
                        echo '">' , HTML::image(URL::mediaImage($article->media_library[$newsThumb]->folder . '/' . $article->media_library[$newsThumb]->file,null));
                    } else {
                        echo 'border:1px solid #ccc;">&nbsp;';
                    }
                    echo '</div>';
                    echo HTML::anchor( Page::sourceCorrection('artikel',$article->permalink) ,$article->label);
                    echo '</li>';
                }
                echo '</ul>';
                echo '<p style="clear:left;"><center>',HTML::anchor(Page::get('completedNavigationUri:artikel'),HTML::image(URL::templateImage('material/btn_read.png'))),'</center></p>';
            } ?>
	</div>
</div>

<?php if (!empty($ebook)) { 
    echo '<div class="book_wrap">',
            '<div class="container">',
                '<div class="img_book">',HTML::image(URL::templateImage('content/book.jpg')),'</div>',
                '<div class="text_book">',
                    '<h3 class="greeny">',(preg_replace('/(monex)/i','<span>$1</span>',$ebook->title)),'</h3>',
                    $ebook->intro,
                    '<p>',HTML::anchor(
                                Page::get('completedNavigationUri:e-book'),
                                HTML::image(URL::templateImage('material/btn_detail.jpg'))),'</p>',
                '</div>',
            '</div>',
         '</div>'; ?> 
<?php } ?>