<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author    Daniel Simangunsong
 * @Company   Webarq
 * @copyright   2012
 * @Package     Education View 
 * @Module      ~
 * @License   Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/


?>
<style type="text/css">
.fancybox-inner{ height: 644px !important;}
.fancybox-opened{ height:600px !important; }
.fancybox-opened iframe{ height:100%; }
</style>
<div style="display: inline-block;border-bottom: 5px solid #dedede; width: 100%;">
<h2 class="title_page" style="width: 80%; float: left; border: 0px;"><?php echo isset($page->title) ? $page->title: ''; ?></h2>
</div>
<br class="clear" />
<div class="sl_wrp">
<a href="#" class="bt_next3 cont">Next</a>
<div class="slide_little3">
<ul class="list_video">
<?php
    if (!empty($others)) {
        foreach ($others as $v) {
            $url_youtube = strpos($v->source,'http') !== 0 ? 'http://www.youtube.com/embed/'. $v->source .'?rel=0&amp;wmode=transparent&callback=?' : $v->source;
        
            echo '<li>'. HTML::anchor( $url_youtube , Page::youtubeImage($v->source, 'default', array('style'=>'height:120px;')). '<br><span>'. $v->label.'</span>', array('class' => "fancybox-media")) .'</li>';        
        }
    }
?></ul>
</div>
<a href="#" class="bt_prev3 cont"></a>
</div>
<br class="clear" />
<div style="height: 2px; width: 100%; border-top: 1px solid #dedede; margin-top: 3px;margin-bottom: 25px;"></div>
<a name="mte"></a>
<?php
    echo '<span class="font_16">'. __('tutorial-monex-mt4') .'</span>'; 
    
    if (!empty($othersMt4)) {
        echo '<div class="sl_wrp">';
            echo '<a href="#" class="bt_next2 cont">Next</a>',
             
                 '<div class="slide_little">',
                 
                 '<ul>';
                 
            foreach ($othersMt4 as $v) {
                $url_youtube = strpos($v->source,'http') !== 0 ? 'http://www.youtube.com/embed/'. $v->source .'?rel=0&amp;wmode=transparent' : $v->source;
               
                echo '<li>',
                        HTML::anchor( $url_youtube , Page::youtubeImage($v->source, 'default',array('style'=>'height:120px;')), array('class' => "youtube cboxElement") ),
                        '<br /> ',
                        $v->label,
                     '</li>';
            }
            
            echo '</ul>',
            
                 '</div>',
                 
                 '<a href="#" class="bt_prev2 cont"></a>';
        echo '</div>';    
    }
    
    echo '<div class="btn_option">';
    
    if (!empty($tools)) {
        foreach ($tools as $t) {            
            $t = Translate::item('site_navigations',$t);
            $m = Model::factory('Medialibrary')->simple_application('tool-video',$t->id,'site_navigations')->current();            
            $a = array();
            if (strpos($t->permalink,'http') === 0) {
                $h = $t->permalink;
                $a['target'] = '_blank';
            } else {
                if ($t->permalink == 'trading-tools')
                    $h = Page::get('completedNavigationUri:trading-products');
                elseif ($t->permalink == 'platform-tutorial')
                    $h = Page::get('completedNavigationUri:online-trading');
                else
                    $h = Page::sourceCorrection('video',$t->permalink);
            }
            //$h = strpos($t->permalink,'http') === 0 ? $t->permalink : ( Page::get('completedNavigationUri:video') . '/' . $t->permalink );
            
            $l = empty($m->folder) ? $t->label : ( HTML::image( URL::mediaImage( $m->folder . '/' . $m->file, null)) . " $t->label" );  
            echo '<p>',
                    HTML::anchor($h,$l,$a),
                 '</p>'; 
        }    
    }
    
    echo '</div>'; ?>
  

    <br class="clear" />
    <h3 class="text_education">
    <p style="margin-bottom: 8px; font-size: 23px;" align="justify"><span class="green_font">Apakah Anda seorang trader yang ingin belajar lebih?</span></p>
        Tidak peduli tingkat kemahiran Anda, ada banyak video untuk meningkatkan kemampuan trading Anda.</h3>
    
<?php //echo isset($page->description) ? $page->description: ''; ?>    
 <div id="form_trading">  
    <p style="margin-center: 6px; font-size: 18px;" align="center"><b>Registrasi Paket Video <br>Monex Trading Education (MTE)</p></b>
        <form method="post" action="<?php echo Translate::uri(Language::$current,'ajax/request-mte'); ?>" class="form-belajar-trading">
              <table width="280" border="0" align="center" cellpadding="0">
            <tbody>
                <br>
                <tr>
                      <td><label for="select">
                        <?php echo Mifx::elementForm('kota'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="textfield">
                          <?php echo Mifx::elementForm('name'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="textfield">
                          <?php echo Mifx::elementForm('phone'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="textfield">
                          <?php echo Mifx::elementForm('email'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="select2">
                      <?php echo Mifx::elementForm('referral'); ?>
                      </label></td>
                    </tr>
        
                <tr>
        
                    <td><?php echo Mifx::elementForm('source').Mifx::elementForm('info'); ?></td>
                </tr>
                <tr>
                      <td><!-- <label for="ckc">
                      <input type="checkbox" name="privacy_agreement" class="required" style="margin-top: -2px;margin-right: 4px;">
                      Anda telah membaca dan setuju -->
                      <?php /*
                        echo HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') );
                        //echo  Form::checkbox('privacy_agreement', null , false ,  
                         //           array( 'class'=>'required' ) ). 
                         //           __('privacy-agreement', array( ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') ))); 
                      */?>
            <?php
          echo '<p class="agree">';
                    echo Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) );
                    echo __('privacy-agreement',array(
                                ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') )
                             ));
                    echo '</p>';
            ?>
                      </label></td>
        </tr>
        <tr>
          <?php echo Form::hidden('mte',$page->permalink) ?>
        </tr>
                <tr>
                      <td><table width="100%" border="0" cellpadding="0">
                        <tbody><tr>
                          <td width="50%"><input name="captcha" type="text" class="captcha-text" maxlength="4" /></td>
                          <td width="50%"><?php 
                          
                          $captcha = Mifx::captcha(200,70,true);
                          echo $captcha;
                          echo Form::hidden('ucr',Session::instance()->get('captcha'))
                          ?></td>
                        </tr>
                      </tbody></table></td>
                    </tr>
                <tr>
                      <td align="left">
                        <input name="button" type="submit" class="btn_submit" id="button" value="<?php echo __('submit'); ?>">
                      </td>
                    </tr>
            </tbody>
          </table>
        </form>
<script type="text/javascript">
 $('form.form-belajar-trading').validate({
                rules            : {
                    accountEmail  : {
                        required : true,
                        email: true
                    }
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { },
                submitHandler  : function(form) {
                     
                     nous_process_message_new();
                setTimeout(function(){
                     serialized = $(form).serialize();
                            $.ajax({
                                url     : form.action,
                                type    : 'POST',
                                data    : serialized,
                                async   : false,
                                success : function(res) {
                                    console.log('function: after ajax send and success');
                                    console.log('data:');
                                    str = $.trim(res);
                                    console.log(res);
                                    
                                    
                                    nous_disable_message();
                                    jAlert(str);
                                    return false;
                                    
                                    
                                    if (str === 'success') {                                    
                                        if (frmID == 'frm-demo-account')
                                            window.location.href = '<?php echo $accountUri; ?>/create-free-demo-account-success<?php echo $qSource;?>';
                                        else
                                            window.location.href = '<?php echo $accountUri; ?>/create-monex-live-account-success<?php echo $qSource;?>';
                                        
                                        return;
                                    } else {
                                        if (str.substr(0,5) == 'goTo:') {
                                            newUrl = str.substr(5);
                                            window.location.href = newUrl;
                                        } else {                                     
                                            nous_disable_message();
                                            
                                              // check captcha
                                            if(res=="<ul class=\"error\"><li>Captcha masih salah. Silahkan masukkan sekali lagi</li></ul>" || res=="<ul class=\"error\"><li>Captcha did not match. Please correct</li></ul>"){
                                               
                                                if(captchaCount==3){
                                                    var action_captcha = $('.verifikasi img').attr('src');
                                                    var img = $("<img />").attr('src', action_captcha)
                                                                            .load(function() {
                                                                                if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                                                                                    alert('broken image, reload page!');
                                                                                } else {
                                                                                    $(".verifikasi img").remove();
                                                                                    $(".verifikasi").append(img);
                                                                                }
                                                                            });
                                                    captchaCount = 0;
                                                }
                                                
                                                captchaCount++;
                                            }

                                            jAlert(res);
                                        }
                                    }
                                } ,
                                error   : function() {                                    
                                    nous_disable_message();
                                    
                                    jAlert('<?php echo __('server-not-responding');?>');
                                }                          
                            });
                            
                        }, 180);
                     
                    }
        
    });
</script>
<div style="display: none;">
    <div id="flash">
    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" height="600" id="mte_swf" width="600">
      <param name="wmode" value="opaque" />
      <param name="allowfullscreen" value="true" />
      <param name="allowscriptaccess" value="always" />
      <param name="movie" value="//www.mte-media.com/players/mte_player10.swf?userRef=c8f3888&amp;language=indonesian&amp;product=stg&amp;open_lessons='1,2,3,4,5,6,7,8'&amp;contact_message=Please register.&amp;demo_mode=false&amp;lgl=undefined&amp;jumpToLesson=1&amp;backUrl=close_popup" />
      <embed allowfullscreen="true" allowscriptaccess="always" height="600" id="mte_swf" src="//www.mte-media.com/players/mte_player10.swf?userRef=c8f3888&amp;language=indonesian&amp;product=stg&amp;open_lessons='1,2,3,4,5,6,7,8'&amp;contact_message=Please register.&amp;demo_mode=false&amp;lgl=undefined&amp;jumpToLesson=1&amp;backUrl=close_popup" type="application/x-shockwave-flash" width="600" wmode="opaque">
      </embed>
    </object>
  </div>
</div>
<?php 

 if (!empty($banners)) {
      echo '<div class="thumb_hi">',
            '<ul>';
      $no=1; 
      foreach ($banners as $banner) {      
          
          $detail = json_decode($banner->detail);
          
          if (!empty($banner->folder))
          {          
              echo '<li>',
                      '<div class="img">',
                          HTML::image( URL::mediaImage( $banner->folder . '/' . $banner->file , null ) ),
                      '</div>';
                   
                        if(Language::$current == 'cn' || Language::$current == 'tw'){
                            $nameBannerTitle2 = 'banner_title2_'.Language::$current;

                            $bannerTitle2 =  !empty($detail->$nameBannerTitle2) ? $detail->$nameBannerTitle2 : $detail->banner_title2;
                
                        }else $bannerTitle2 = $detail->banner_title2; 
                          

                        echo $bannerTitle2;


                        if(Language::$current == 'cn' || Language::$current == 'tw'){ 
                            $nameBannerTitle1 = 'banner_title1_'.Language::$current;

                            $bannerTitle1 = !empty($detail->$nameBannerTitle1) ? $detail->$nameBannerTitle1 : $detail->banner_title1;
                        }else{ $bannerTitle1 = $detail->banner_title1; }

                       
                      echo '<p>',HTML::anchor( 
                              empty($detail->banner_permalink) ? '#' : $detail->banner_permalink , 
                              $bannerTitle1  ),'</p>',
                       
                       HTML::anchor( 
                          empty($detail->banner_permalink) ? '#' : $detail->banner_permalink , 
                          HTML::image( URL::templateImage('material/btn_detail.jpg') ) ,
                          array('class'=>'btn','style'=>empty($detail->banner_title2) ? 'bottom:10px;' : 'bottom:-10px;') ),
                   '</li>';   
                   
                   if($no==2) echo '<div style="clear: both; height: 15px;"></div>'; 
                   $no++;     
            
          }                             
      }
      echo '</ul>',
           '</div>';
  }
?>

<script type="text/javascript">
    $('#submit_verify').validate({
                rules            : {
                    accountEmail  : {
                        required : true,
                        email: true
                    }
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { },
                submitHandler  : function(form) {
                     
                     nous_process_message_new();z
                     
                     serialized = $(form).serialize();
                     
                     $.ajax({
                            url     : '<?php echo URL::front('ajax/verified_email') ?>',
                            type    : 'POST',
                            data    : serialized,
                            async   : false,
                            success : function(res) {
                                
                                jAlert(res,'success');
                                
                                form.accountEmail.value = "";
                                nous_disable_message();
                                
                                return false;
                            }
                     });
                     
                    }
        
    });
</script>