<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

if (empty($item)) 
{
    return;
}

$totalTicket = Request::$current->query('ticket');

Mifx::instance()->soap(); $soap = Mifx::get('_soap');

$kota = $soap->getKota();   
        
$elementKota  = '<select name="seminarKota" class="required" value="' . (empty($post) || !isset($post['seminarKota']) ? '' : $post['seminarKota']) .'">';
$elementKota .= '<option value="">' . __("select-city") . '</option>';
foreach ($kota as $idKota => $namaKota) {
    $elementKota .= '<option value="' . $idKota . '">' . $namaKota . '</option>';
}
$elementKota .= '</select>';
            

echo '<h2 class="title_page">',
        __('register'), Page::get('simpleBreadCrumb:seminar-training'), //' "',$item->label,'"', 
     '</h2>';

echo '<div class="schedul_edu seminar" style="margin-top:20px;">',
        '<h3>', __('order_summary'), '</h3>',
                
    Form::open( URL::front('ajax/seminar-registration') , array ( 'id' => 'frm-register-seminar' ) );
    
    echo Form::hidden('seminarDate',Page::datetime($item->date,$item->start,$item->region));
    echo Form::hidden('seminarLabel',$item->label);
    echo '<table width="100%">',
            '<tr>',
                '<th>Event</th>',			
    			'<th width="30%">&nbsp;</th>',
                '<th>Quantity</th>',
    	   	'</tr>',
            '<tr>',
                '<td>',$item->label,'<br /><span class="date">',Page::datetime($item->date,$item->start,$item->region) ,'</span></td>',			
    			'<td>&nbsp;</td>',
                '<td>',
                Mifx::select_ticket(3,$totalTicket);
                echo '</td>',
    	   	'</tr>',
         '</table>'; 
         
    echo '<div class="seminar" style="margin-top:20px;">',
            '<h3>',__('registration-information'),'</h3>',
            '<p>',__('registration-wording'),'</p>',
            
            '<div class="info_priv">',
                '<b>',__('your-information'),'</b>',
                
                '<p>',
                    '<span>',__('gender'),' *</span>',
                    Form::radio('seminarGender','male',false,array('class'=>'required')),' ', __('male'), '  &nbsp;',
                    Form::radio('seminarGender','female',false,array('class'=>'required')),' ',__('female'),
                '</p>',
                
                '<p>',
                    '<span>',__('first-name'),' *</span>',
                    Form::input('seminarFirstName',null,array('class'=>'required')),
                '</p>',
                
                '<p>',
                    '<span>',__('last-name'),' *</span>',
                    Form::input('seminarLastName',null,array('class'=>'required')),
                '</p>',
                
                '<p>',
                    '<span>',__('email'),' *</span>',
                    Form::input('seminarEmail',null,array('class'=>'required email')),
                '</p>',
                
                '<p>',
                    '<span>',__('phone'),' *</span>',
                    Form::input('seminarPhone',null,array('class'=>'required')),
                '</p>',
                
                '<p>',
                    '<span>',__('city'),' *</span>',
                    $elementKota,
                '</p>',
                
                '<p>',
                    '<span class="t_area">',__('address'),' *</span>',
                    Form::textarea('seminarAddress','',array('class'=>'required')),
                '</p>',
                
                '<p style="text-align:right;">',
                    HTML::anchor(Page::get('completedNavigationUri:seminar-training'),__('Back to list'),array('class'=>'btn_back')),
                    Form::hidden('seminarId',$item->id),
                    Form::submit('submit','Submit',array( 'class'=>'btn_sbt','style'=>'border:0;height:28px;')),
                '</p>',

            '</div>',
         '</div>';        
echo '</div>'; 

echo Form::close();    
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('#live-Phone,#demo-Phone').numeric({
            allow : '-'
        }); 
        
        $('#frm-register-seminar').validate({  
            invalidHandler : function(form, validator) { },     
            errorPlacement : function(error, element) { },
            submitHandler  : function(form) {
                
                frmID = $(form).attr('id');
                
                //nous_process_message( '<?php echo __('do-not-close-browser'); ?>' , 1 );
                nous_process_message_new();
                setTimeout(function(){                            
                    serialized = $(form).serialize();
                    $.ajax({
                        url     : form.action,
                        type    : 'POST',
                        data    : serialized,
                        async   : false,
                        success : function(res) {
                            if ($.trim(res) === 'success') {

                                // Track google analityc
                                ga('send', 'event', { eventCategory: 'Education', eventAction: 'Seminar'});
                                
                                nous_disable_message();
                                
                                $('.info_priv').fadeOut().html('<?php echo __('thank-you-for-registering-seminar');?>').fadeIn();                                          
                                
                                $.ajax({
                                    url     : '<?php echo URL::front('ajax/seminar-registration-data') ?>',
                                    type    : 'POST',
                                    data    : serialized,
                                    async   : false,
                                    success : function(data) {
                                        $('.info_priv').append(data);
                                    }
                                });
                                
                                
                                
                            } else {
                                nous_disable_message();
                                
                                jAlert(res);
                            }
                        }                         
                    });
                },180);
                
                return false;
                /** **/
            }
        });
    });
</script>
