
<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$post    = Request::$initial->post('');



$elementSubmit = Form::submit('submit','SUBMIT',array('class' => 'btn_submit'));
                                                           
echo '<div class="mobile-create-account-box demo-acc active">'
        
        ,Form::open( URL::front('ajax/submit-demo-account') , array ( 'id' => 'frm-demo-account', 'class' => 'is-validate' ) )
        
        ,'<h3>'
            ,'<span>Create Demo Account</span>'
            ,HTML::image( URL::base(true) . '/assets/default/site/images/material/arr_down.png',array( 'alt' => '' ) )
        ,'</h3>'
        , Mifx::elementForm('kota')
        , Mifx::elementForm('name')
        , Mifx::elementForm('phone')
        , Mifx::elementForm('email')
        , Mifx::elementForm('deposit')
        , Mifx::elementForm('referral')
        , Mifx::elementForm('source')
        , Mifx::elementForm('info')
        //, Form::hidden('captcha',null,array('class'=>'verif required'))
        
        //,'<input type="checkbox" name="privacy_agreement" class="required">'  //Disabled on June 13, 2014
        
        , '<p class="verifikasi">'
        , Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4))
        , '<div class="captcha captcha-container">',Mifx::captcha(),'</div>'
        , '</p>'
        
        ,'<p>'
        , Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) )
        , __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') ),
                    ':privacy' => HTML::anchor( empty($privacy_url) ? '#' : $privacy_url , empty($privacy_text) ? __('Kebijakan Privasi') : $privacy_text )
                 ))
        ,'</p>'
       
        
        , '<p class="clearfix" style="margin:5px 0 0 0">',$elementSubmit,'</p>'        
        , Form::close()
        
        ,'</div>'
        
        //Live Account
        ,'<div class="mobile-create-account-box live-acc" style="padding-bottom: 57px;">'
        
        ,Form::open( URL::front('ajax/submit-live-account') , array ( 'id' => 'frm-live-account', 'class' => 'is-validate' ) )
        
        ,'<h3>'
            ,'<span>Create Live Account</span>'
            ,HTML::image( URL::base(true) . '/assets/default/site/images/material/arr_down.png',array( 'alt' => '' ) )
        ,'</h3>'
        , Mifx::elementForm('kota','live')
        , Mifx::elementForm('name','live')
        , Mifx::elementForm('phone','live')
        , Mifx::elementForm('email','live')
        //, Mifx::elementForm('deposit','live')
        , Mifx::elementForm('referral','live')
        , Mifx::elementForm('source','live')
        , Mifx::elementForm('info','live')
        //, Form::hidden('captcha',null,array('class'=>'verif required'))
        , Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4))
        , '<div class="captcha captcha-container">',Mifx::captcha(),'</div>'
        
        ,'<p>'
        , Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) )
        , __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') ),
                    ':privacy' => HTML::anchor( empty($privacy_url) ? '#' : $privacy_url , empty($privacy_text) ? __('Kebijakan Privasi') : $privacy_text )
                 ))
        ,'</p>'
        
        , '<p class="clearfix" style="margin:5px 0 0 0">',$elementSubmit,'</p>'        
        , Form::close()
        
        ,'</div>'
        
        ,'<div class="btn_tab_mobile"><span class="demo_acc active"></span><span class="live_acc"></span></div>';
?>


<script type="text/javascript">
    $(document).ready(function(){
        $('.btn_tab_mobile').children('span').click(function(){
            
            $(this).siblings('span').removeClass('active');  
            $(this).addClass('active');
              
            boxid = $(this).index();
            console.log(boxid);
            
            $('.mobile-create-account-box.active').removeClass('active');
            
            $('.mobile-create-account-box').eq(boxid).addClass('active');
        });
    });
</script>
        