<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ ?>

<div class="account_wrap<?php if (!empty($class)) echo " $class";?>"<?php if (!empty($class) && $class == 'homepage') echo ' style="min-height:380px;"';?>>
    <div id="frm-wrapper" class="loader">
        <div style="float: left;margin:200px auto;height:30px;">
            <?php echo HTML::image(URL::base(true) . 'assets/default/site/images/loading.gif', array('width' => 32, 'height' => 32)); echo ('Please wait while loading registration form'); ?>
        </div>
    </div>
</div>


