<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$imageAttrs = array();
echo
    '<div class="sosMedia">',
        __('share'), ' : ',
        //HTML::anchor( Page::get("completedNavigationUri:$permalink"), HTML::image( URL::templateImage( 'material/ico_mail.jpg',array_merge($imageAttrs,array('alt'=>'Mail')) ) ) ), "\n\r" ,
        HTML::anchor( 'http://www.linkedin.com/shareArticle?mini=true&url=&lt;&title=&gt;', HTML::image( URL::templateImage( 'material/ico_linkin.jpg',array_merge($imageAttrs,array('alt'=>'Link In')) ) ) , array('target'=> '_blank',  'onclick' => 'return linked_click()')), "\n\r" ,
       // HTML::anchor( '#', HTML::image( URL::templateImage( 'material/ico_feeder.jpg',array_merge($imageAttrs,array('alt'=>'Feed')) ) ) , array('target'=> '_blank') ), "\n\r" ,
        HTML::anchor( "javascript:javascript:(function(){window.twttr=window.twttr||{};var D=550,A=450,C=screen.height,B=screen.width,H=Math.round((B/2)-(D/2)),G=0,F=document,E;if(C>A){G=Math.round((C/2)-(A/2))}window.twttr.shareWin=window.open('http://twitter.com/share','','left='+H+',top='+G+',width='+D+',height='+A+',personalbar=0,toolbar=0,scrollbars=1,resizable=1');E=F.createElement('script');E.src='http://platform.twitter.com/bookmarklets/share.js?v=1';F.getElementsByTagName('head')[0].appendChild(E)}());", HTML::image( URL::templateImage( 'material/ico_twitter.jpg',array_merge($imageAttrs,array('alt'=>'Twitter')) ) ) , array('target'=> '_blank') ), "\n\r" ,
        HTML::anchor( 'http://www.facebook.com/share.php?u=&lt;url&gt;', HTML::image( URL::templateImage( 'material/ico_facebook.jpg',array_merge($imageAttrs,array('alt'=>'Facebook')) ) ) , array('target'=> '_blank', 'onclick' => 'return fbs_click()') ), "\n\r" ,
    '</div>';
       
?>