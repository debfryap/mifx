

<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ 

//Check for query Source
$qSource = Request::$initial->query('Source');

//Check for session Source
if (empty($qSource)) {
    $qSource = Session::instance()->get('Source');                
}

$qSource = empty($qSource) ? '' : "?Source=$qSource";

$post    = Request::$initial->post('');

//$accountUri    = URL::front('accounts');
$accountUri    = Translate::uri(Language::$current,'accounts');


$elementSubmit = Form::submit('submit', __('submit') ,array('class' => 'btn_submit'));

$demoActionForm = Translate::uri(Language::$current,'ajax/submit-demo-account');
$liveActionForm = Translate::uri(Language::$current,'ajax/submit-live-account');
                                                                                    
echo '<div class="create-account-box demo-acc active">'
        
        ,Form::open( $demoActionForm , array ( 'id' => 'frm-demo-account', 'class' => 'is-validate' ) )
        
        ,'<h3>'
            ,'<span>'. __('create-demo-account') .'</span>'
            ,HTML::image( URL::base(true) . '/assets/default/site/images/material/arr_down.png',array( 'alt' => '' ) )
        ,'</h3>'
        , Mifx::elementForm('kota')
        , Mifx::elementForm('name')
        , Mifx::elementForm('phone')
        , Mifx::elementForm('email')
        , Mifx::elementForm('deposit')
        , Mifx::elementForm('referral')
        , Mifx::elementForm('source')
        , Mifx::elementForm('info')
        
        ,'<p class="agree">'
        , Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) )
        , __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') ),
                    ':privacy' => HTML::anchor( empty($privacy_url) ? '#' : $privacy_url , empty($privacy_text) ? __('Kebijakan Privasi') : $privacy_text )
                 ))
        ,'</p>'
        
        //, Form::hidden('captcha',null,array('class'=>'verif required'))
        , Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4 ))
        , '<div class="captcha captcha-container">',Mifx::captcha(),'</div>'
        , '<p>', $elementSubmit ,'</p>'        
        , Form::close()
        
        ,'</div>'
        
        //Live Account
        ,'<div class="create-account-box live-acc">'
        
        ,Form::open( $liveActionForm , array ( 'id' => 'frm-live-account', 'class' => 'is-validate') )
        
        ,'<h3>'
            ,'<span>', __('title-form-account-live') ,'</span>'
            ,HTML::image( URL::base(true) . '/assets/default/site/images/material/arr_down.png',array( 'alt' => '' ) )
        ,'</h3>'
        , Mifx::elementForm('kota','live')
        , Mifx::elementForm('name','live')
        , Mifx::elementForm('phone','live')
        , Mifx::elementForm('email','live')
        //, Mifx::elementForm('deposit','live')
        , Mifx::elementForm('referral')
        , Mifx::elementForm('source','live')
        , Mifx::elementForm('info','live')
        // Form::hidden('captcha',null,array('class'=>'verif required'))
        ,'<p class="agree">'
        , Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) )
        , __('privacy-agreement',array(
                    ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') ),
                    ':privacy' => HTML::anchor( empty($privacy_url) ? '#' : $privacy_url , empty($privacy_text) ? __('Kebijakan Privasi') : $privacy_text )
                 ))
        ,'</p>'
        ,'<br class="clearfix" />'
        
        , Form::input('captcha',null,array('class'=>'verif required', 'maxlength'=> 4 ))
        , '<div class="captcha captcha-container">',Mifx::captcha(),'</div>'
        , '<p>', $elementSubmit ,'</p>'
        , '<div style="height:25px;"></div>'        
        , Form::close()
        ,'</div>'
        
        ,'<div class="btn_tab"><span class="demo_acc active"></span><span class="live_acc "></span></div>';
     
    if (!isset($idpopmedia) || $idpopmedia !== false)  
        echo '<div id="bpopup-media">',__('do-not-close-browser'),'</div>';
    
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.btn_tab').children('span').click(function(){
            
            $(this).siblings('span').removeClass('active');  
            $(this).addClass('active');
              
            boxid = $(this).index();
            
            $('.create-account-box.active').removeClass('active');
            
            $('.create-account-box').eq(boxid).addClass('active');
        });
        
        //Validate default value for each element
        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            return !(element.value == param);
        });

        jQuery.validator.addMethod("validEmail", function(value, element) {
            
            var re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
            
            if (re.test(value)) {
                
                if (value.indexOf("@sharklasers.com", value.length - "@sharklasers.com".length) !== -1) { // if domain sharklasers.com
                    return false;
                }else if (value.indexOf("@yopmail.com", value.length - "@yopmail.com".length) !== -1) { // if domain yopmail.com
                    return false;
                } else if (value.indexOf("@guerrillamail.com", value.length - "@guerrillamail.com".length) !== -1) { // if domain guerrillamail.com
                    return false; 
                } else { return true; }

            } else {
                return false;
            }
        }, "Your email address is not valid.");

            
        $('#live-Phone,#demo-Phone').numeric({
            allow : '-'
        }); 
        
        var captchaCount = 1;
        $('form.is-validate').each(function(){
            $(this).validate({
                rules            : {
                    accountEmail : {
                        validEmail: true
                    },
                    accountName  : {
                        required : true,
                        notEqual : '<?php echo __('full-name'); ?>'
                    },
                    accountPhone : {
                        required : true,
                        notEqual : '<?php echo __('phone'); ?>'
                    } /**,
                    accountReferral : {
                        required : true,
                        notEqual : '<?php echo __('referral'); ?>'
                    } **/
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { },
                submitHandler  : function(form) {
                    
                    frmID = $(form).attr('id');
                    
                    //nous_process_message( $('#bpopup-media').html() , 1 );
                    nous_process_message_new();
                    
                    setTimeout(function(){                            
                        serialized = $(form).serialize();
                        $.ajax({
                            url     : form.action,
                            type    : 'POST',
                            data    : serialized,
                            async   : false,
                            success : function(res) {
                                str = $.trim(res);
                                if (str === 'success') {                                    
                                    if (frmID == 'frm-demo-account')
                                        window.location.href = '<?php echo $accountUri; ?>/create-free-demo-account-success<?php echo $qSource;?>';
                                    else
                                        window.location.href = '<?php echo $accountUri; ?>/create-monex-live-account-success<?php echo $qSource;?>';
                                    
                                    return;
                                } else {
                                    if (str.substr(0,5) == 'goTo:') {
                                        newUrl = str.substr(5);
                                        window.location.href = newUrl;
                                    } else {                                    
                                        nous_disable_message();
                                        // check captcha
                                        if(res=="<ul class=\"error\"><li><?php echo __('error-captcha'); ?></li></ul>" || res=="<ul class=\"error\"><li><?php echo __('captcha-not-valid'); ?></li></ul>"){
                                           
                                            if(captchaCount==3){
                                                var action_captcha = $('.captcha-container img').attr('src');
                                                var img = $("<img />").attr('src', action_captcha+"&refTrigger="+Math.random())
                                                                        .load(function() {
                                                                            if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                                                                                alert('broken image!');
                                                                            } else {
                                                                                $(".captcha-container").html(img);
                                                                            }
                                                                        });
                                                
                                                captchaCount = 0;
                                            }
                                            
                                            captchaCount++;
                                        }
                                        jAlert(res);
                                    }
                                }
                            },
                            error   : function() {                                    
                                nous_disable_message();
                                
                                jAlert('<?php echo __('server-not-responding');?>');
                            }                         
                        });
                    },180);
                    
                    return false;
                    /** **/
                }
            });
        })
                
    });
</script>