<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Account Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 *                  
 * Calm seas, never make skillfull sailors	
**/ 

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <?php echo isset($body) ? $body : 'No Email Body'; ?>
        <p>
            <?php echo __('thank-you'); ?> <br />
            <i style="font-size: 10px;"><?php echo __('this-email-send-automatically-by-system'); ?></i> <br />
        </p>
    </body>
</html>