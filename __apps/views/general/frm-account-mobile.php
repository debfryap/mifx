<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ ?>

<div class="account_wrap<?php if (!empty($class)) echo " $class";?>">
    <div id="frm-wrapper-mobile" class="loader">
    <?php echo HTML::image(URL::base(true) . 'assets/default/site/images/loading.gif'); echo ('Please wait while loading registration form'); ?>
    </div>
</div>