<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    HTML Error
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>
<div 
    style="border:3px solid #ccc;padding:20px;width:300px;margin:30px auto;text-align:justify;letter-spacing:1.2px;color:#000;font-family:arial;font-size: 14px;" 
    class="system-error">
    404 Page ... <?php echo empty($message) ? '' : '<br/>'.$message; ?>
    <div style="height:10px;line-height:10px;"></div>
    <div style="opacity: .8;color:#333;font-size:11px;">
        This page could not found on this server. Please contact your administrator
    </div>
    <div style="height:10px;line-height:10px;"></div>
</div>