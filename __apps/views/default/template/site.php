<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Space Html View
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
if ((int)(App::$config->version->html) <= 4) {
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
}elseif ((int)($site->html_version)  == 5) {
    echo '<!DOCTYPE html>'."\n";
}

$lang = '';

switch(Language::$current){
    case 'id': $lang = 'id'; break; // Indonesia
    case 'en': $lang = 'en'; break; // English
    case 'cn': $lang = 'zh-Hans'; break; // Simplified Chinese
    case 'tw': $lang = 'zh-Hant'; break; // Traditional Chinese
    
}

?>
<html xmlns="http://www.w3.org/1999/xhtml" <?php echo (!empty($lang) ? 'lang="'. $lang .'" xml:lang="'. $lang .'"' : ''); ?>>
<head>
    <link rel="shortcut icon" href="<?php echo URL::base(true) . 'favicon.ico' ;?>" type="image/x-icon" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1"  />    
	<?php
        foreach (App::$config->meta as $name=>$value) {
            if ($name != 'title') {
                if ($name == 'description')
                    echo '<meta name="'.$name.'" content="'.(Page::meta(null,'desc')).'"/>',"\r\n";                    
                else
                    echo '<meta name="'.$name.'" content="'.$value.'"/>',"\r\n";
            }
        } 
        
        if (!empty($site->styles)) {
            #echo Debug::vars($site->styles);
            foreach($site->styles as $cpath => $ctype) {
                $ctype  = empty($ctype) ? 'screen,projection' : $ctype;  
                $file = Helper_File::type($cpath) == 'css' ? $cpath : $cpath.'.css';
                echo HTML::style($file, array('media' => $ctype)), "\n"; 
            }
        }
        
        if (!empty($site->static_styles)) echo $site->static_styles;
        
        echo "<script type=\"text/javascript\">
                    var bs_path = '",Kohana::$base_url,"';
                    var bs_root = '",URL::root(),"';
                    var bs_site = '",URL::front(),"';
        </script>","\r\n";
        
        if (!empty($site->scripts)) {
            foreach($site->scripts as $file) { 
                $file = Helper_File::type($file) == 'js' ? $file : $file.'.js';
                echo HTML::script($file, NULL, TRUE), "\n"; 
            } 
        }
    ?>
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>	  	  		
      <script src="<?php echo URL::templateJs('html5shiv.js');?>"></script>
    <![endif]-->
	<!--[if IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo URL::templateCss('styleIE9.css');?>"/>	
	<![endif]-->
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="<?php echo URL::templateCss('styleIE8.css');?>"/>	
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php echo URL::templateCss('styleIE7.css');?>"/>	
	<![endif]-->    
    
    <!--Optimizely code-->
    <script src="//cdn.optimizely.com/js/2388230129.js"></script>

	<title><?php echo Page::meta(null,'title'); ?></title>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" data-class="body">
    <!-- header start -->
    <header>
        <div class="container">
            <div class="logo"><?php echo Themes::logo(); ?></div>
            
            <div class="right_header">
            
                <div class="sosMedia">
                    <a href="http://www.linkedin.com/company/pt.monex-investindo-futures" alt="Link In" target="_blank"><img src="<?php echo URL::templateImage('material/ico_linkin.jpg');?>" alt=""  width="17" height="17" /></a>
                    <a href="https://twitter.com/Monex_Futures" alt="Twitter" target="_blank"><img src="<?php echo URL::templateImage('material/ico_twitter.jpg');?>" alt=""  width="17" height="17" /></a>
                    <a href="https://www.facebook.com/MonexInvestindoFutures" alt="Facebook" target="_blank"><img src="<?php echo URL::templateImage('material/ico_facebook.jpg');?>" alt=""  width="17" height="17" /></a>
                    <a href="https://plus.google.com/103325306176280244740/posts" target="_blank">
                        <img src="<?php echo URL::templateImage('icon/google-plus.jpg');?>" alt=""  width="17" height="17" />
                    </a>
                    
                    <?php echo Widget::load('language/selector')->data('translate_url',isset($site->translate_url) ? $site->translate_url : null); ?>                
                    <a href="#" class="btn_search"></a>
                    <form method="post" action="" class="form_search" name="form_search"  style="display:none;" onSubmit="window.location='<?php echo URL::front('search')?>/'+form_search.search.value; return false;" >
                        <input type="text"
                        name="search" 
                            class="tsearch" 
                            value="Search" 
                            onfocus="if(value=='Search')value='';" 
                            onblur="if(value=='')value='Search'"/>
                            <span class="close"></span>
                    </form>
                </div>
                
                <!--<div class="button_share signin">
                    <a class="share_show" href="#" alt=""><img src="<?php echo URL::templateImage('material/btn_shared.jpg');?>" alt="" width="24" height="24"  /></a>
                </div>
                
                <div class="sosMedia_mobile" style="display: none;">
                    <a href="http://www.linkedin.com/company/pt.monex-investindo-futures" alt="Link In"><img src="<?php echo URL::templateImage('material/ico_linkin.jpg');?>" alt="" width="17" height="17"/></a>
                    <a href="https://twitter.com/Monex_Futures" alt="Twitter"><img src="<?php echo URL::templateImage('material/ico_twitter.jpg');?>" alt=""  width="17" height="17" /></a>
                    <a href="https://www.facebook.com/MonexInvestindoFutures" alt="Facebook"><img src="<?php echo URL::templateImage('material/ico_facebook.jpg');?>" alt=""  width="17" height="17" /></a>
                    <a href="https://plus.google.com/103325306176280244740/posts" target="_blank"><img src="<?php echo URL::templateImage('icon/google-plus.jpg');?>" alt=""  width="17" height="17" /></a>
                </div>-->
                
                <p class="phone"><a href="tel:<?php echo str_replace('-', '', Contact::getPhone());?>" style="text-decoration: none;color: #289824;"  onClick="ga('send', 'event', { eventCategory: 'Mobile', eventAction: 'Call'});"><?php echo Contact::getPhone();?></a></p>
                
               
                <!--
                <div class="menuOther">
                    <ul>
                        <li class="first"><a href="#">Client Area</a></li>
                        <li class="second"><?php echo HTML::anchor('contact-us',__('contact-us'));?></li>
                        <li class="last"><a href="#">Live Chat</a></li>
                    </ul>
                </div>
                -->
                <div class="clear"></div>
             <?php echo Widget::load('page/shortcut-navigation'); ?>
            
            </div>            
        </div>
        <?php echo Page::get('navigationHtml'); ?>
    </header> 
    
    
    <?php
        if (!isset($site->banner) || $site->banner !== false) echo Widget::load('page/auto-banner'); 
        echo !empty($site->layout) ? $site->layout : ''; ?>
    

     <?php if(App::$route_params['package'] != 'home'){ ?>
    <div class="button-accountright" id="button-accountright">
        <div><a href="<?php echo Translate::uri(Language::$current, 'open-demo-account.php'); ?>"><span class="demo_acc"></span></a></div>
        <br class="clear" />
        <div><a href="<?php echo Translate::uri(Language::$current, 'open-live-account.php'); ?>"><span class="live_acc"></span></a></div>
    </div>
    <script type="text/javascript">
        var wrap = $(".button-accountright");
    
        //wrap.on("scroll", function(e) {
         $(window).scroll(function () {
            
            console.log($(this).scrollTop());
         
          if ($(this).scrollTop() > 388) {
            wrap.addClass("button-accountright-fixed");
            console.log('fixed scroll');
          } else {
            wrap.removeClass("button-accountright-fixed");
            console.log('remove fixed scroll');
          }
          
        });
    </script>
    <?php } ?>


    <footer id="footer">
		<section class="disclaimer">
			<div class="container">	
                <?php
                    
                    if(App::$route_params['package'] == 'home'){
                        if(!empty($site->footer_forex_trading))
                            echo $site->footer_forex_trading->description;
                    }

                    if (!empty($site->disclaimer)) {
                        echo '<h4>',$site->disclaimer->title,'</h4>';
                        echo $site->disclaimer->description;
                    }
                    ?>
				
				<div class="partner">
                     <?php 
                        if (!empty($site->logo) && $site->logo->valid()) {
                            $leftDiv  = 1;
                            foreach ($site->logo as $logo) { 
                                if (!empty($logo->detail)) {
                                    $logoDetail = json_decode($logo->detail);
                                }
                                if($leftDiv <= 3) echo '<div style="float: left;">';

                                $url_image = URL::mediaImage($logo->folder . '/'. $logo->file,null);
                                list($width, $height) = getimagesize($url_image);
                                
                                $logoLink = empty($logoDetail->permalink) ? '#' : $logoDetail->permalink;                                
                                echo HTML::anchor($logoLink,HTML::image($url_image, array('width' =>$width, 'height' => $height)), array('target'=> '_blank','rel'=>'nofollow'));
                                
                                if($leftDiv <= 3) echo '</div>';
                                $leftDiv++;
                            }
                        } ?>
                    <!--
					<a href="#"><img src="images/content/logo_partner_icdx.jpg" alt="" class="icdx"/></a>
					<a href="#"><img src="images/content/logo_partner_bl.jpg" alt="" class="klin"/></a>
					<a href="#"><img src="images/content/logo_partner_jfx.jpg" alt="" class="jkt"/></a>
					<a href="#"><img src="images/content/logo_partner_bap.jpg" alt="" class="bap"/></a>
                    -->
				</div>
			</div>
		</section>
		<section class="footer_menu">
			<div class="container">	
                <?php echo Widget::load('page/footer-navigation'); ?>
				
				<div class="right_foot">
                    <?php echo !empty($site->page->description) ? $site->page->description : ''; ?>
				</div>
				<div class="foot_log">	
                    <?php 
                        echo Themes::logo('footerLogo');
                        //echo App::$config->copyright; 
                        echo __('footer-wording'); ?>
				</div>
			</div>
		</section>
	</footer>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-18130880-1', 'mifx.com');

  // Optimizely Universal Analytics Integration code
  window.optimizely = window.optimizely || [];
  window.optimizely.push(['activateUniversalAnalytics']);
      
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
    
    <script type="text/javascript">
        var sc_project=6768811; 
        var sc_invisible=1; 
        var sc_security="457e121b"; 
    </script>
    <script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
    <script type="text/javascript" src="http://chatserver.comm100.com/js/LiveChat.js?siteId=77063&planId=271&partnerId=-1"></script>
    <noscript>
        <div class="statcounter">
        <a title="web analytics" href="http://statcounter.com/" target="_blank">
        <img class="statcounter" src="http://c.statcounter.com/6768811/0/457e121b/1/" alt="web analytics"></a>
        </div>
    </noscript>
    
    <!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
------------------------------------------------- -->
<script type="text/javascript">
/* <![CDATA[ */ 
var google_conversion_id = 991080516;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/991080516/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php
echo '
 <script type="text/javascript">
 		var cookie = document.cookie;
        var csplit = cookie.split(";");

        var nameEQ = "referral=";
        var name = "X";
        for(var i=0;i < csplit.length;i++) {
                var c = csplit[i];
                while (c.charAt(0)==" ") c = c.substring(1,c.length);


                if (c.indexOf(nameEQ) == 0) name = c.substring(nameEQ.length,c.length);
            }
        if (name!="X"){
            var c2 = name.split("WP");

            if (typeof c2[1] != "undefined") {
                name="WP"+c2[1];
            }else{
                name="X";

            }
        } 
 		var ref = "'.@$_SERVER['HTTP_REFERER'].'";
        (function(fo,r,e,x,i,m,f){f=x.createElement(e),
        m=x.getElementsByTagName(e)[0];f.async=1;f.src=r;
        m.parentNode.insertBefore(f,m);f.onload=function()
        {i[fo].push(i.location.href,name,ref)};})
        ("_track", "//crm.mifx.com/js/track.js", "script", document, window);
    </script>
';?>
 
 <script type="text/javascript">
    <?php
    if (!empty(App::$module->language) && Language::$current != Language::$default){
        $href = Translate::uri(Language::$current,Language::$current.'/ajax/load-form');
    }else{
        $href = URL::front('ajax/load-form');
    }
    
    echo "var URL_ajax_load= '". $href ."';";
    ?>

    $(window).load(function(){
        $('.loader#frm-wrapper-mobile').load(URL_ajax_load + '/mobile',{'privaci' : '<?php echo Page::get('simpleBreadCrumb:kebijakan-privasi');?>','privaci_url':'<?php echo Page::get('completedNavigationUri:kebijakan-privasi'); ?>'},function(){
            $(this).find('.captcha-container').html('<div style="padding-top:11px;">Loading image</div>');   
            $(this).find('.btn_submit').hide(); 
        });
        //setTimeout(function(){            
            $('.loader#frm-wrapper').load(URL_ajax_load + '/account',{'privaci' : '<?php echo Page::get('simpleBreadCrumb:kebijakan-privasi');?>','privaci_url':'<?php echo Page::get('completedNavigationUri:kebijakan-privasi'); ?>'},function(){
                $('.mobile-create-account-box').each(function(){
                    $(this).find('.captcha-container').html($('form#frm-demo-account').find('.captcha-container').html());  
                    $(this).find('.btn_submit').show(); 
                })
            });
        //},5);             
    });
</script>
<style type="text/css">
    /* add style 22 january 2015 */
.button-accountright{
    position: absolute;
    top: 536px;
    right: 0px;
    background: #DEDEDE;
    width: 38px;
    height: 307px;
    
    -webkit-border-radius: 5px; 
    -moz-border-radius: 5px; 
    border-radius: 5px 0 0 5px; 
    padding: 5px 5px 5px 5px;
}

.button-accountright-fixed {
    position: fixed;
    top: 135px;
}


.button-accountright span.demo_acc {
    width: 41px;
    height: 154px;
    cursor: pointer;
    position: absolute;
    left: 6px;
    bottom: 5px;
    background: url(http://mifx.com/assets/default/site/images/material/btn_account_rotate.png) no-repeat bottom right;
}
.button-accountright span.live_acc {
    width: 41px;
    height: 154px;
    cursor: pointer;
    position: absolute;
    left: 6px;
    top: 3px;
    background: url(http://mifx.com/assets/default/site/images/material/btn_account_rotate.png) no-repeat top left;
} 


/* add 22 januari 2015*/
@media (max-width: 979px){
    .button-accountright {display: none;}
    .mobile-accountbutton {display: block !important;}
}
@media (min-width:320px) and (max-width: 480px) {
    
} 
@media (min-width:480px) and (max-width: 600px){    
        
    .right_header .phone {
        margin-top: 5px;
    }
    .mobile-accountbutton{
        margin-top: 10px;
        margin-left: 10px;
        
    }
}

</style>
</body>
</html>
