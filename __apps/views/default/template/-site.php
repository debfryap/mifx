<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Space Html View
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
if ((int)(App::$config->version->html) <= 4) {
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
}elseif ((int)($site->html_version)  == 5) {
    echo '<!DOCTYPE html>'."\n";
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="shortcut icon" href="<?php echo URL::base(true) . 'favicon.ico' ;?>" type="image/x-icon" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
	<?php
        foreach (App::$config->meta as $name=>$value) {
            if ($name != 'title') {
                if ($name == 'description')
                    echo '<meta name="'.$name.'" content="'.(Page::meta(null,'desc')).'"/>',"\r\n";                    
                else
                    echo '<meta name="'.$name.'" content="'.$value.'"/>',"\r\n";
            }
        } 
        
        if (!empty($site->styles)) {
            #echo Debug::vars($site->styles);
            foreach($site->styles as $cpath => $ctype) {
                $ctype  = empty($ctype) ? 'screen,projection' : $ctype;  
                $file = Helper_File::type($cpath) == 'css' ? $cpath : $cpath.'.css';
                echo HTML::style($file, array('media' => $ctype)), "\n"; 
            }
        }
        
        if (!empty($site->static_styles)) echo $site->static_styles;
        
        echo "<script>
                    var bs_path = '",Kohana::$base_url,"';
                    var bs_root = '",URL::root(),"';
                    var bs_site = '",URL::front(),"';
        </script>","\r\n";
        
        if (!empty($site->scripts)) {
            foreach($site->scripts as $file) { 
                $file = Helper_File::type($file) == 'js' ? $file : $file.'.js';
                echo HTML::script($file, NULL, TRUE), "\n"; 
            } 
        }
    ?>
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>	  	  		
      <script src="<?php echo URL::templateJs('html5shiv.js');?>"></script>
    <![endif]-->
	<!--[if IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo URL::templateCss('styleIE9.css');?>"/>	
	<![endif]-->
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="<?php echo URL::templateCss('styleIE8.css');?>"/>	
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php echo URL::templateCss('styleIE7.css');?>"/>	
	<![endif]-->
    
   

    
    

	<title><?php echo Page::meta(null,'title'); ?></title>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" data-class="body">
    <!-- header start -->
    <header>
        <div class="container">
            <div class="logo"><?php echo Themes::logo(); ?></div>
            
            <div class="right_header">
            
                <div class="sosMedia">
                    <!--<a href="#" alt="Mail" target="_blank"><img src="<?php echo URL::templateImage('material/ico_mail.jpg');?>" alt=""/></a>-->
                    <a href="http://www.linkedin.com/company/pt.monex-investindo-futures" alt="Link In" target="_blank"><img src="<?php echo URL::templateImage('material/ico_linkin.jpg');?>" alt=""/></a>
                    <!--<a href="<?php echo URL::front('feed'); ?>" alt="Feed" target="_blank"><img src="<?php echo URL::templateImage('material/ico_feeder.jpg');?>" alt=""/></a>-->
                    <a href="https://twitter.com/Monex_Futures" alt="Twitter" target="_blank"><img src="<?php echo URL::templateImage('material/ico_twitter.jpg');?>" alt=""/></a>
                    <a href="https://www.facebook.com/MonexInvestindoFutures" alt="Facebook" target="_blank"><img src="<?php echo URL::templateImage('material/ico_facebook.jpg');?>" alt=""/></a>
                    <a href="https://plus.google.com/103325306176280244740/posts" target="_blank">
                        <img src="<?php echo URL::templateImage('icon/google-plus.jpg');?>" alt=""/>
                    </a>
                    
                    <?php echo Widget::load('language/selector'); ?>        
                    <a href="#" class="btn_search"></a>
                    <form method="post" action="" class="form_search" name="form_search"  style="display:none;" onSubmit="window.location='<?php echo URL::front('search')?>/'+form_search.search.value; return false;" >
                        <input type="text"
                        name="search" 
                            class="tsearch" 
                            value="Search" 
                            onfocus="if(value=='Search')value='';" 
                            onblur="if(value=='')value='Search'"/>
                    </form>
                </div>
                
                <div class="button_share signin">
                    <a class="share_show" href="#" alt=""><img src="<?php echo URL::templateImage('material/btn_shared.jpg');?>" alt=""/></a>
                </div>
                
                <div class="sosMedia_mobile" style="display: none;">
                    <a href="#" alt="Mail"><img src="<?php echo URL::templateImage('material/ico_mail.jpg');?>" alt=""/></a>
                    <a href="#" alt="Link In"><img src="<?php echo URL::templateImage('material/ico_linkin.jpg');?>" alt=""/></a>
                    <a href="#" alt="Feed"><img src="<?php echo URL::templateImage('material/ico_feeder.jpg');?>" alt=""/></a>
                    <a href="#" alt="Twitter"><img src="<?php echo URL::templateImage('material/ico_twitter.jpg');?>" alt=""/></a>
                    <a href="#" alt="Facebook"><img src="<?php echo URL::templateImage('material/ico_facebook.jpg');?>" alt=""/></a>
                </div>
                
                <p class="phone"><a href="tel:<?php echo str_replace('-', '', Contact::getPhone());?>" style="text-decoration: none;color: #289824;"  onClick="_gaq.push(['_trackEvent', 'Mobile', 'Call' ]);"><?php echo Contact::getPhone();?></a></p>
                
               
                <!--
                <div class="menuOther">
                    <ul>
                        <li class="first"><a href="#">Client Area</a></li>
                        <li class="second"><?php echo HTML::anchor('contact-us',__('contact-us'));?></li>
                        <li class="last"><a href="#">Live Chat</a></li>
                    </ul>
                </div>
                -->
            </div>
             <?php echo Widget::load('page/shortcut-navigation'); ?>
            <div class="navbar">
                <img src="<?php echo URL::templateImage('material/button_menu.jpg');?>" alt="" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"/>			
            </div>  
            <?php echo Page::get('navigationHtml'); ?>
        </div>
    </header> 
    
    
    <?php
        if (!isset($site->banner) || $site->banner !== false) echo Widget::load('page/auto-banner'); 
        echo !empty($site->layout) ? $site->layout : ''; ?>
    
    <footer id="footer">
		<section class="disclaimer">
			<div class="container">	
                <?php
                    if (!empty($site->disclaimer)) {
                        echo '<h4>',$site->disclaimer->title,'</h4>';
                        echo $site->disclaimer->description;
                    }
                    ?>
				
				<div class="partner">
                     <?php 
                        if (!empty($site->logo) && $site->logo->valid()) {
                            $leftDiv  = 1;
                            foreach ($site->logo as $logo) { 
                                if (!empty($logo->detail)) {
                                    $logoDetail = json_decode($logo->detail);
                                }
                                if($leftDiv <= 3) echo '<div style="float: left;">';

                                $logoLink = empty($logoDetail->permalink) ? '#' : $logoDetail->permalink;                                
                                echo HTML::anchor($logoLink,HTML::image(URL::mediaImage($logo->folder . '/'. $logo->file,null)), array('target'=> '_blank','rel'=>'nofollow'));
                                
                                if($leftDiv <= 3) echo '</div>';
                                $leftDiv++;
                            }
                        } ?>
                    <!--
					<a href="#"><img src="images/content/logo_partner_icdx.jpg" alt="" class="icdx"/></a>
					<a href="#"><img src="images/content/logo_partner_bl.jpg" alt="" class="klin"/></a>
					<a href="#"><img src="images/content/logo_partner_jfx.jpg" alt="" class="jkt"/></a>
					<a href="#"><img src="images/content/logo_partner_bap.jpg" alt="" class="bap"/></a>
                    -->
				</div>
			</div>
		</section>
		<section class="footer_menu">
			<div class="container">	
                <?php echo Widget::load('page/footer-navigation'); ?>
				
				<div class="right_foot">
                    <?php echo !empty($site->page->description) ? $site->page->description : ''; ?>
				</div>
				<div class="foot_log">	
                    <?php 
                        echo Themes::logo('footerLogo');
                        //echo App::$config->copyright; 
                        echo __('footer-wording'); ?>
				</div>
			</div>
		</section>
	</footer>


    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-18130880-1']);
      _gaq.push(['_setDomainName', 'mifx.com']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
    
    <script type="text/javascript">
        var sc_project=6768811; 
        var sc_invisible=1; 
        var sc_security="457e121b"; 
    </script>
    <script type="text/javascript" src="http://www.statcounter.com/counter/counter.js"></script>
    <script type="text/javascript" src="http://chatserver.comm100.com/js/LiveChat.js?siteId=77063&planId=271&partnerId=-1"></script>
    <noscript>
        <div class="statcounter">
        <a title="web analytics" href="http://statcounter.com/" target="_blank">
        <img class="statcounter" src="http://c.statcounter.com/6768811/0/457e121b/1/" alt="web analytics"></a>
        </div>
    </noscript>
    
    <!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 991080516;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/991080516/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

    
</body>
</html>