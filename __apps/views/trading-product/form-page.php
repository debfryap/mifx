<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (empty($pages)) { echo '<h2 class="title_page">',__('no-data-found-title'),'</h2><p>',__('no-data-found-description'),'</p>'; return; }

if (isset($title))
echo '<h2 class="title_page">'.(preg_replace('/(monex)/i','<span>$1</span>',$title)).'</h2>'; 

if(!empty($form_page)){
  echo '
      <form method="post" name="form_trading" class="form_trading" onsubmit="return submit_komoditi(\''. $action .'\')">
        <div class="form_contact demo_multi" >             
          <div class="form">
            <span>'. __('request-demo-multilateral') .'</span>
            <table>
              <tbody><tr>
                <td>'. __('full-name') .' </td>
                <td>:</td> 
                <td><input type="text" value="" name="nama" id="nama" class="nama required" required></td>
              </tr>
              <tr>
                <td>'.__('email').' </td> 
                <td>:</td> 
                <td><input type="email" value="" name="email" id="email" class="email required" required></td>
              </tr>
              <tr>
                <td>'. __('kode-wakil-pialang') .'</td> 
                <td>:</td> 
                <td><input type="text" value="" name="kode" id="kode" class="kode required" required></td>
              </tr>
              <tr>
                <td colspan="3"><input type="submit" value="'. __('submit') .'" class="sbt_form"></td> 
              </tr>
            </tbody></table>
          </div>
        </div>
        </form>
        <div id="bpopup-media">Silahkan tunggu beberapa saat dan jangan tutup browser anda.</div>
        '; 

}
?>
  <div class="accordion_wrap">
    <?php
        foreach($pages as $page) {
            echo '<h2 class="acc_trigger auto"><a href="#">'. $page->title .'</a></h2>';
            echo '<div class="acc_container">'. $page->description .'</div>';
        }
    ?>
    </div>  

<script type="text/javascript">        
  var submit_komoditi = function(url){



        var nama = $("input[name='nama']");
        var email = $("input[name='email']");
        var kode = $("input[name='kode']");

        if(nama.val() ==""){
          nama.addClass('error');
          nama.focus();
          return false;
        }

        if(email.val() ==""){
          email.addClass('error');
          email.focus();
          return false;
        }

        if(kode.val() ==""){
          kode.addClass('error');
          kode.focus();
          return false;
        }



        //nous_process_message( $('#bpopup-media').html() , 1 );
        nous_process_message_new();
        
        $.ajax({
          url: url,
          type: 'POST',
          data: {'nama' :nama.val(), 'email' : email.val(), 'kode': kode.val() },
          success: function(data){
            $("form[name='form_trading']").slideUp('slow');
            nous_disable_message();
          },
          error: function(data){
            $("form[name='form_trading']").slideUp('slow');
            nous_disable_message();
            //alert(data);  
            console.log(data);
          }
        })
        return false;
    };
</script>