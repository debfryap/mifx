<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (empty($item)) { 
    echo '<h2 class="title_page">',__('no-data-found-title'),'</h2>',
         __('no-data-found-description'); 
    return; 
}

$banner = !empty($item->media_library['updates-banner']) 
                ? "<p>".HTML::image( URL::mediaImage($item->media_library['updates-banner']->folder . '/' . $item->media_library['updates-banner']->file,null) )."</p>"
                : '<p></p>';
                
echo 
    '<h2 class="title_page">',$item->label,'</h2>',
    '<div class="detail_event">',
        $banner,
        '<div class="tit_page">',
            '<strong>',$item->label,'</strong><br/>',
            Page::datetime($item->date),
        '</div>',
        Page::sosialMedia(Page::get("completedNavigationUri:$section") . '/' . $item->permalink, $item->label),
    '</div>',
    $item->description,
    HTML::anchor( Page::get("completedNavigationUri:$section"),__('back-to-list',array(':group'=>Page::get("simpleBreadCrumb:$section"))) );
?>