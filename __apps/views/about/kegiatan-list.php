<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Education View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
$title = preg_replace('/(monex)/i','<span>$1</span>',Page::get("simpleBreadCrumb:$section")); ?>

<h2 class="title_page"><?php echo $title; ?></h2>
				
<div class="corp_event">
	<span class="corp_title"><?php echo __('corporate-events') ?></span>
    
    <?php 
        if (!$list->valid())
        {
            echo '<p style="padding:0 10px;">',__("no-data-found" , array( ":group" => $title )), '</p></div>';
            return;
        }
        echo '<ul class="list_event">';    
        foreach ($list as $item) {
            $item = Translate::item($table,$item);
            echo '<li>',
                    HTML::anchor(Page::get("completedNavigationUri:$section") . '/' .$item->permalink,$item->label),
                    '<br/>',
                    '<span class="date">',Page::datetime($item->date),'</span>',
                 '</li>';
        }
        echo '</ul>';
        
        echo $page;    
    ?>                
</div>

<?php
if (!empty($list_branch))
{   
    echo '<br style="clear: both;" />';
    echo '<div class="schedul_edu seminar" style="margin-top: 20px;">';
    echo '<h3>'. __('monex-branch') .'</h3>';
    echo '<p>', __('description-branch-events') ,'</p>';
    echo '<table width="100%">' ,
            
                '<tr>' ,
                '<th>'. __('event') .'</th>' ,
                '<th>'. __('date') .'</th>',
                '<th>'. __('kantor-cabang') .'</th>' ,
                '</tr>';
                if($list_branch->count()==0){
                    echo '<tr><td class="date"> '. __('empty') .' </td><td class="date"> '. __('empty') .' </td><td class="date"> '. __('empty') .' </td></tr>';
                }
                foreach ($list_branch as $item) {
                    $item = Translate::item('branch_events',$item);
                    echo '<tr>',
                            '<td>', $item->label ,'</td>',
                            '<td>', Page::datetime($item->date) ,'</td>',
                            '<td>', $item->location ,'</td>',
                         '</tr>';
                }
    echo '</table>';
    echo '</div>';      
}
?>
