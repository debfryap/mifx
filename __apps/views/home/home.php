<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>

<section class="widget_wrap">
    <?php 
        echo '<div class="container">';
        foreach (Page::get('navigationObject') as $n) {
            
            if ($n->permalink != 'trading-products') continue;
            
            if (!empty($n->child)) {
                $tc = count($n->child);
                foreach ($n->child as $i => $n) {
                    
                    $n     = Translate::item('site_navigations',$n);
                    
                    $icon  = Model::factory('Medialibrary')->simple_application('widget-icon',$n->id,'site_navigations')->current();
                    
                    $short = Model::factory('Page')->navigation_page($n->permalink); //DB::select('intro')->from('site_navigation_pages')->where('site_navigation_id','=',$n->id)->execute()->get('intro');
                    $short = !empty($short->intro) ? substr(strip_tags($short->intro,'<strong><b><i><u>'),0,110) : '';
                    
                    echo '<div class="widget';
                    if ($i == 0) echo ' first';
                    if ($i == $tc-1) echo ' last';
                    echo '">';
                    echo '<span>',  
                            ( !empty($icon->folder) ? HTML::image( URL::mediaImage( $icon->folder . '/' . $icon->file ,null ) ) : '' ),                  
                            HTML::anchor(Page::get("completedNavigationUri:$n->permalink"), empty($n->judul_icon) ? $n->label : $n->judul_icon),   
                         '</span>',
                         "<p>$short ...</p>",
                         HTML::anchor(Page::get("completedNavigationUri:$n->permalink"), HTML::image(URL::templateImage('material/btn_more.jpg')));
                    
                    echo '</div>';
                }
            }
            break;
        } 
        echo '</div>'; 
        echo HTML::image( URL::templateImage('material/shadow_widget.jpg') , array('class'=>'shadow') );?>
</section>

<section class="gr">
	<div class="container">	
		<div class="plug">
            <?php echo View::factory('home/trading'); ?>
			<div class="greeter">
                <?php
                    if (!empty($welcome)) {
                        echo '<h2>', preg_replace('/(monex)/i','<b>$1</b>',$welcome->title) ,'</h2>',
                             $welcome->intro,
                             HTML::anchor( Page::get('completedNavigationUri:selamat-datang-di-monex') , HTML::image( URL::templateImage('material/btn_more.jpg') ) );
                    }?>
			</div>
			<div class="news_h">
				<?php
                    echo '<h2>', __('highlight'),' <b>', Page::get('simpleBreadCrumb:artikel'), '</b></h2>'; 
                    
                    if (!empty($articles)) {
                        echo '<ul>';
                        if (count($articles) == 1)
                        {
                            echo '<li>',
                                    HTML::anchor( Page::get('completedNavigationUri:artikel') . '/' .$articles->permalink , $articles->label ),
                                    '<i>',Page::datetime($articles->date),'</i>',
                                 '</li>';
                        }
                        else 
                        {
                            foreach ($articles as $a) 
                            { 
                                echo '<li>',
                                        HTML::anchor( Page::get('completedNavigationUri:artikel') . '/' .$a->permalink , $a->label ),
                                        '<i>',Page::datetime($a->date),'</i>',
                                     '</li>';
                            }
                        }
                        echo '</ul>';                        
                    } 
                    
                    echo '<div class="monex">',
                            __('jadwal-monex-edukasi',array(':tag'=>'b'));
                    
                    if (!empty($schedules)) {
                        foreach ($schedules as $i => $s) {
                            $title_schs = "title_". Language::$current;
                            $title_schs = empty($s[$title_schs]) ? $s["title_id"] : $s[$title_schs];
                            
                            echo '<p>',
                                        HTML::anchor( Page::get('completedNavigationUri:seminar-training') , $title_schs ), ' ',
                                        Page::datetime($s['date']),
                                     '</p>';
                            break;
                        }
                        //foreach ($schedules as $i => $s)
//                        {
//                            if (is_numeric($i))
//                            {                                
//                                echo '<p>',
//                                        HTML::anchor( Page::get('completedNavigationUri:seminar-training') , $s->label ), ' ',
//                                        Page::datetime($s->date),
//                                     '</p>'; 
//                            } else {                                
//                                echo '<p>',
//                                        HTML::anchor( Page::get('completedNavigationUri:seminar-training') , $schedules->label ), ' ',
//                                        Page::datetime($schedules->date),
//                                     '</p>'; 
//                                break;
//                            }
//                        }
                    }          
                                   
                    echo '</div>'; ?>
			</div>	
		</div>
		
	</div>
</section> 
<script type="text/javascript">
/*<![CDATA[*/

jQuery(function($) {
    //return false;
  
    var socket = io.connect('http://quotes.monexnews.com:8000');
    socket.emit('channel','quotes');
    
    socket.on('update',function(data){
        
        var quotes = $.parseJSON(data);
            
        $.each(quotes,function(key, val){ 
            if(val.symbol == 'EURUSD' ||
               val.symbol == 'GBPUSD' ||
               val.symbol == 'USDCHF' || 
               val.symbol == 'USDJPY' ||
               val.symbol == 'AUDUSD' ||
               val.symbol == 'XAUUSD' ||
               val.symbol == 'XAGUSD' ||
               val.symbol == '#CO-LS' ||
               val.symbol == 'NGAS'   ||
               val.symbol == 'GOOGLE' ||
               val.symbol == 'GOLDMAN'||
               val.symbol == 'EXXON'  ||
               val.symbol == 'INTEL'  ||
               val.symbol == 'BLK_ROCK' ||
               val.symbol == '#DJ5' ||
               val.symbol == '#SP50' ||
               val.symbol == '#NQ20' ||
               val.symbol == 'KSA' ||
               val.symbol == 'HKA' ) {
                    
                //console.log(val);
                s = val.symbol.replace('#','').replace('&','');
                tr = $('#' + s).parent();
                tr.removeClass('up').removeClass('down');
                
                $('#'+s).next().text(val.bid).removeClass('up').removeClass('down').addClass(val.status.bid);
                $('#'+s).next().next().text(val.ask).removeClass('up').removeClass('down').addClass(val.status.ask);
                $('#'+s).next().next().next().text(val.high).removeClass('up').removeClass('down').addClass(val.status.high);
                $('#'+s).next().next().next().next().text(val.low).removeClass('up').removeClass('down').addClass(val.status.low);
            		
                    
            }
        });
    });
});
/*]]>*/
</script>