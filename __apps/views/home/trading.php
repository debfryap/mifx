<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
?>
<div class="index_saham">
	<ul id="myTab" class="nav-tabs">
		<li class="active"><a href="#forex" data-toggle="tab"><span><?php echo __('forex'); ?></span></a></li>
		<li class=""><a href="#commodities" data-toggle="tab"><span><?php echo __('commodities'); ?></span></a></li>						
		<li class=""><a href="#cfds" data-toggle="tab"><span><?php echo __('cfds'); ?></span></a></li>						
		<li class="last"><a href="#index" data-toggle="tab"><span><?php echo __('index'); ?></span></a></li>						
	</ul>
	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade active in" id="forex">
			<table cellpadding="0" cellspacing="0" width="100%" class="tsaham">
				<tr>
					<th><?php echo __('symbol'); ?></th>
					<th><?php echo __('bid'); ?></th>
					<th><?php echo __('ask'); ?></th>
					<th><?php echo __('high'); ?></th>
					<th><?php echo __('low'); ?></th>
				</tr>
				<tr>
					<td id="EURUSD"><?php echo __('eurusd'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="GBPUSD"><?php echo __('gbpusd'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="USDCHF"><?php echo __('usdchf'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="USDJPY"><?php echo __('usdjpy'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="AUDUSD"><?php echo __('audusd'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5"><?php echo __('hanya-indikator'); ?></td>
				</tr>
			</table>
		</div>
		<div class="tab-pane fade" id="commodities">
			<table cellpadding="0" cellspacing="0" width="100%" class="tsaham">
			<tr>
					<th><?php echo __('symbol'); ?></th>
					<th><?php echo __('bid'); ?></th>
					<th><?php echo __('ask'); ?></th>
					<th><?php echo __('high'); ?></th>
					<th><?php echo __('low'); ?></th>
				</tr>
				<tr>
					<td id="XAUUSD">XAUUSD</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="XAGUSD">XAGUSD</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="CO-LS">#CO-LS</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<!--<td id="NGAS">NGAS</td>!-->
					<td id="NGAS"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="x">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5"><?php echo __('hanya-indikator'); ?></td>
				</tr>
			</table>
		</div>	
		<div class="tab-pane fade" id="cfds">
			<table cellpadding="0" cellspacing="0" width="100%" class="tsaham">
			<tr>
					<th><?php echo __('symbol'); ?></th>
					<th><?php echo __('bid'); ?></th>
					<th><?php echo __('ask'); ?></th>
					<th><?php echo __('high'); ?></th>
					<th><?php echo __('low'); ?></th>
				</tr>
				<tr>
					<td id="GOOGLE"><?php echo __('google'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="GOLDMAN"><?php echo __('goldman'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="EXXON"><?php echo __('exxon'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="INTEL"><?php echo __('intel'); ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="BLK_ROCK">BLK_ROCK</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5"><?php echo __('hanya-indikator'); ?></td>
				</tr>
			</table>
		</div>	
		<div class="tab-pane fade" id="index">
			<table cellpadding="0" cellspacing="0" width="100%" class="tsaham">
				<tr>
					<th><?php echo __('symbol'); ?></th>
					<th><?php echo __('bid'); ?></th>
					<th><?php echo __('ask'); ?></th>
					<th><?php echo __('high'); ?></th>
					<th><?php echo __('low'); ?></th>
				</tr>
				<tr>
					<td id="DJ5">#DJ5</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="SP50">#SP50</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td id="NQ20">#NQ20</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<!--<td id="HKA">HKA</td>!-->
					<td id="HKA"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<!--<td id="KSA">KSA</td>!-->
					<td id="KSA"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5"><?php echo __('hanya-indikator'); ?></td>
				</tr>
			</table>
		</div>	
	</div>
</div>