<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Contact Email Template 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>
    </head>
    
    <body>
        <p>
        Dear admin ...,
        </p>
        
        <table style="padding:10px;border:0;border-collapse:collapse;">
            <tbody>
                <?php
                    if (!empty(Contact::$config->field_contacts)) {
                        foreach (Contact::$config->field_contacts as $i => $el) {
                            $name  = $el->name;
                            
                            if (!empty(App::$module->language)) {
                                $langAct = Language::$current;
                                $langDef = Language::$default;
                                $label   = isset($el->label->$langAct) ? $el->label->$langAct : $el->label->$langDef;
                            } else {
                                $label = is_string($el->label) ? $el->label : 'Unknnown Field';    
                            }
                            
                            if($name =="kota"){
                            
                                $data_kota = Model::factory('Contact_Office')->getBranchOffice(array('email', $post->$name))->current();
                                
                                echo '<tr style="',($i == 0 ? 'background-color: #EAF2D3;' : ''),'border:0;border-collapse:collapse;">',
                                 '<td style="padding:10px;border:0;border-collapse:collapse;">Kota</td>',
                                 '<td style="padding:10px;"> : </td>',
                                 '<td style="padding:10px;">',(empty($data_kota->provinsi_name) ? 'Unknown Value' : $data_kota->provinsi_name),'</td>',
                                 '</tr>'; 
                                
                                
                            }else{
                                
                                echo '<tr style="',($i == 0 ? 'background-color: #EAF2D3;' : ''),'border:0;border-collapse:collapse;">',
                                     '<td style="padding:10px;border:0;border-collapse:collapse;">',$label,'</td>',
                                     '<td style="padding:10px;"> : </td>',
                                     '<td style="padding:10px;">',(empty($post->$name) ? 'Unknown Value' : $post->$name),'</td>',
                                     '</tr>';  
                            } 
                        }
                    } ?>
            </tbody>
        </table>
        
        Telah menghubungi anda melalui website MIFX pada tanggal <?php echo Page::tanggalIndonesia(date('Y-m-d')); ?>
        <p>
            Thank You <br />
            <i style="font-size: 10px;">This email send automatically by system, and you do not need to reply.</i> <br />
        </p>
    </body>
</html>