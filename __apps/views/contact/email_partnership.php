<?php defined('SYSPATH') or die('No direct script access.'); 

 

/**

 * @Author 		Daniel Simangunsong

 * @Company		Webarq

 * @copyright 	2012

 * @Package	    Contact Email Template 

 * @Module      ~

 * @License		Kohana ~ Webarq ~ Daniel Simangunsong

 * 

 * Calm seas, never make skillfull sailors	

**/ ?>

<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title><?php echo isset($subject) ? $subject : 'No Subject'; ?></title>

    </head>

    

    <body>

        <p>

        Form Registration Fields

        </p>

        

        <table style="padding:10px;border:0;border-collapse:collapse;">

            <tbody>

                <?php    

                    echo '<tr style="background-color: #EAF2D3; border:0;border-collapse:collapse;">',

                         '<td style="padding:10px;border:0;border-collapse:collapse;">Company / Personal Name</td>',

                         '<td style="padding:10px;"> : </td>',

                         '<td style="padding:10px;">',$post->company,'</td>',

                         '</tr>'; 

                    echo '<tr style="background-color: #EAF2D3; border:0;border-collapse:collapse;">',

                         '<td style="padding:10px;border:0;border-collapse:collapse;">Adress</td>',

                         '<td style="padding:10px;"> : </td>',

                         '<td style="padding:10px;">',$post->address,'</td>',

                         '</tr>';

                    echo '<tr style="background-color: #EAF2D3; border:0;border-collapse:collapse;">',

                         '<td style="padding:10px;border:0;border-collapse:collapse;">City</td>',

                         '<td style="padding:10px;"> : </td>',

                         '<td style="padding:10px;">',$post->city,'</td>',

                         '</tr>';

                    echo '<tr style="background-color: #EAF2D3; border:0;border-collapse:collapse;">',

                         '<td style="padding:10px;border:0;border-collapse:collapse;">Phone</td>',

                         '<td style="padding:10px;"> : </td>',

                         '<td style="padding:10px;">',$post->phone,'</td>',

                         '</tr>';

                    echo '<tr style="background-color: #EAF2D3; border:0;border-collapse:collapse;">',

                         '<td style="padding:10px;border:0;border-collapse:collapse;">Handphone</td>',

                         '<td style="padding:10px;"> : </td>',

                         '<td style="padding:10px;">',$post->handphone,'</td>',

                         '</tr>';

                    echo '<tr style="background-color: #EAF2D3; border:0;border-collapse:collapse;">',
                         '<td style="padding:10px;border:0;border-collapse:collapse;">Email</td>',
                         '<td style="padding:10px;"> : </td>',
                         '<td style="padding:10px;">',$post->email,'</td>',
                         '</tr>';

                    ?>

            </tbody>

        </table>

        

        Telah menghubungi anda melalui website MIFX pada tanggal <?php echo Page::tanggalIndonesia(date('Y-m-d')); ?>

        <p>

            Thank You <br />

            <i style="font-size: 10px;">This email send automatically by system, and you do not need to reply.</i> <br />

        </p>

    </body>

</html>