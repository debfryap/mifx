<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Contact View 
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (!empty($provinces)) {
    echo '<div class="accordion_wrap">';
    
    foreach ($provinces as $province) {
        echo '<h2 class="acc_trigger"><a href="#">',$province->name,'</a></h2>'
             ,'<div class="acc_container">';
        
        if (!empty(Contact::$config->field_contacts)) {
            
            if (!empty($frm_errors)) {
                echo '<div class="fix-form-message">',__('fix-form-message'),'</div>';
                echo '<ol class="errors">';
                foreach ($frm_errors as $err) {
                    echo '<li>',$err,'</li>';
                }
                echo '</ol>';
            }
            
            $address = '';
            
            $email   = null;
            
            if ($province->offices->valid()) 
            {    
                foreach ($province->offices as $office) {
                    $email[] = array($office->email, $office->name);
                    
                    $address .= '<p>'
                         .'<strong>'.$office->name.'</strong><br />'
                         .str_replace('</p>','<br/ >',str_replace('<p>','',$office->address))
                         .'<span>'.__('telp').'</span> : '.$office->phone. '<br />'
            			 .'<span>'.__('fax').'</span> : '.$office->fax
                         .'</p>';
                }
            }
            
            echo '<div class="form_contact"> <div class="form"> ';
            
            if (empty($submitForm)) {
                
                echo '<span>'. __('contact-form') .'</span>';
                
                echo Form::open('',array( 'id' => 'frm-' . $province->permalink, 'class' => 'contact-validate'));
                
                foreach (Contact::$config->field_contacts as $el) {
                    //echo Debug::vars($el);
                    $name  = $el->name;
                    
                    if (!empty(App::$module->language)) {
                        $langAct = Language::$current;
                        $langDef = Language::$default;
                        $label   = isset($el->label->$langAct) ? $el->label->$langAct : $el->label->$langDef;
                        if (isset($frm_post) && !empty($frm_post->$name)) {
                            $value = $frm_post->$name;     
                        } else {
                            $value = __($label);
                        }
                        
                    } else {
                        $label = is_string($el->label) ? $el->label : 'Unknnown Field';
                        
                        $value = isset($frm_post) && !empty($frm_post->$name) ? $frm_post->$name : __($label);    
                    }
                    
                    if (!empty($el->rule) && is_array($el->rule)) {
                        
                        foreach ($el->rule as $rule)
                            $attrs['class'] =  empty($attrs['class']) ? $rule : $rule." ".$attrs['class'];
                    }
                    
                    $attrs['onfocus'] = 'if(value==\''.__($label).'\')value=\'\';';
                    $attrs['onblur']  = 'if(value==\'\')value=\''.__($label).'\';';
                    $attrs['id']      = $name.'-'.$province->permalink;
                    
                    
                    
                    switch ($el->type) {
                        case "textarea" :
                            $elHtml = Form::textarea($name,$value,$attrs);
                            break;
                        case "text":
                            $elHtml = Form::input($name,$value,$attrs);
                            break;
                    }
                    
                    if ($name == 'kota') {
                        $elHtml = '<select name="'.$name.'" ' . HTML::attributes($attrs) . '>';
                            $elHtml .= '<option value="">'.__('pilih-cabang').'</option>';    
                            
                            if (!empty($email)) {
                                foreach ($email as $mail) {
                                    $elHtml .= '<option value="'.$mail[0].'">'.$mail[1].'</option>';    
                                }   
                            }
                        $elHtml .= '</select>';
                    }
                    
                    echo '<p>',$elHtml,'</p>';
                    
                    unset($attrs);
                }
                
                echo '<p><input type="submit" value="'. __('submit') .'" class="sbt_form" onClick="_gaq.push([\'_trackEvent\', \'Contact Us\', \'Contact Form\' ]);" /></p>';

                echo Form::close();
            } else {
                echo 'Terimakasih. Data anda sudah kami terima';
                App::$session->set('submit-form',false);
                $submitForm = false;
            }
        }
        
        echo '</div> </div>';
        
        echo '<div class="clear"></div>';
        
        echo $address;
        
        echo '</div>';                 
    }
    
    echo '</div>';
}

?>

<script type="text/javascript">
    $(document).ready(function(){
        <?php if (empty($submitForm)) { ?>
        jQuery.validator.addMethod("numeric", function(value, element) {
        	return this.optional(element) || /^(\d+-?)+\d+$/i.test(value);
        });
        
        $('form.contact-validate').each(function(){
            $(this).validate({
                rules            : {
                    <?php
                        if (!empty(Contact::$config->field_contacts)) {
                            foreach (Contact::$config->field_contacts as $i => $el) {
                                $name  = $el->name;
                                
                                if (!empty(App::$module->language)) {
                                    $langAct = Language::$current;
                                    $langDef = Language::$default;
                                    $label   = isset($el->label->$langAct) ? $el->label->$langAct : $el->label->$langDef;
                                } else {
                                    $label = is_string($el->label) ? $el->label : 'Unknnown Field';    
                                }    
                                
                                if (!empty($el->rule) && is_array($el->rule)) {
                                    if ($i > 0 ) echo ", ";
                                    echo "'$name' : { ";
                                    
                                    echo "notEqual : '$label'";
                                    foreach ($el->rule as $ii => $rule) {                                     
                                        echo ",$rule:true";        
                                    }
                                    
                                    echo "}";
                                }
                            }
                        } ?>
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { }
            });
        });
        <?php } ?>    
    });
</script>