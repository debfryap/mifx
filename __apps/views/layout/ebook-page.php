<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
if (empty($page)) { echo '<h2 class="title_page">',__('no-data-found-title'),'</h2><p>',__('no-data-found-description'),'</p>'; return; }

echo '<h2 class="title_page">'.(preg_replace('/(monex)/i','<span>$1</span>',$page->title)).'</h2>';
 
/** Get Media **/
if (!empty($page->id)) {
    $media = Model::factory('Medialibrary')->simple_application('banner-page', $page->id, 'site_navigation_pages');
    
    foreach($media as $media){
      echo "<p>";
      echo HTML::image(URL::root($media->folder . '/'. $media->file,null));
      echo "</p>";
    }
}    

  $permalink = Page::get("completedNavigationUri:e-book");
  $imageAttrs = array();

  echo 
      '<div class="sosMedia sosmed_ebook">',
        __('share'), ' : ',
        HTML::anchor( $permalink, HTML::image( URL::templateImage( 'material/ico_mail.jpg',array_merge($imageAttrs,array('alt'=>'Mail')) ) ) ), "\n\r" ,
        HTML::anchor( 'http://www.linkedin.com/shareArticle?mini=true&url='. $permalink .'&title='. $page->title .'&ro=false&summary=&source=', HTML::image( URL::templateImage( 'material/ico_linkin.jpg',array_merge($imageAttrs,array('alt'=>'Link In')) ) ) , array('target'=> '_blank')), "\n\r" ,
        // HTML::anchor( '#', HTML::image( URL::templateImage( 'material/ico_feeder.jpg',array_merge($imageAttrs,array('alt'=>'Feed')) ) ) , array('target'=> '_blank') ), "\n\r" ,
        HTML::anchor( 'https://twitter.com/intent/tweet?text='. $page->title .'&url='. $permalink .'&related=', HTML::image( URL::templateImage( 'material/ico_twitter.jpg',array_merge($imageAttrs,array('alt'=>'Twitter')) ) ) , array('target'=> '_blank') ), "\n\r" ,
        HTML::anchor( 'https://www.facebook.com/sharer/sharer.php?u='. $permalink, HTML::image( URL::templateImage( 'material/ico_facebook.jpg',array_merge($imageAttrs,array('alt'=>'Facebook')) ) ) , array('target'=> '_blank') ), "\n\r" ,
    '</div>';
    
    echo $page->description; 
      
    if(empty($current_ebook->id)){
        return;  
    }
    
    ?>

    <div class="ebook_wrap">
      <?php
      // get media
      $media = Model::factory('Medialibrary')->simple_application('ebook-large', $current_ebook->id, 'site_navigations');
      
      if (!empty($media) && $media->valid()) {
          foreach($media as $media){                              
              echo HTML::image(URL::root($media->folder . '/'. $media->file), array('class'=> 'img'));        
          }
      }else{
         echo HTML::image(URL::templateImage('content/e-book_large.jpg'), array('class'=> 'img')); 
      }
      ?>     
      <div class="descrip">
        <h2 class="title_page"><?php echo $current_ebook->label; ?></h2>
        <p><?php echo $current_ebook->description; ?></p>           
        <div class="btn_ebook">
          <a class="demo" href="<?php $url_demo = 'url-demo'; echo strpos($current_ebook->$url_demo,'http') === 0 ? $current_ebook->$url_demo : Url::front('ebook/'.$current_ebook->$url_demo);?>" target="_blank" onClick="ga('send', 'event', { eventCategory: 'Education', eventAction: 'Ebook', eventLabel: 'Demo'});"><?php echo __('demo'); ?></a>
          <div class="full" onClick="ga('send', 'event', { eventCategory: 'Education', eventAction: 'Ebook', eventLabel: 'Full'});"><?php echo __('full-version'); ?></div>
        </div>
      </div>
    </div>
    <div class="account_wrap" id="form_ebook" style="display: none;"> 
      <span class="arrow_ebook"></span>
        <?php

            $captcha = Mifx::captcha(200,70,true);
                    
            echo Form::open( URL::front('ajax/request-ebook') , array ( 'id' => 'frm-ebook-download' ) )
                    , '<fieldset>'
                    , Mifx::elementForm('kota')
                    , Mifx::elementForm('name')
                    , Mifx::elementForm('phone')
                    , Mifx::elementForm('email')
                    //, Mifx::elementForm('deposit')
                    , Mifx::elementForm('referral')
                    , Mifx::elementForm('source')
                    , Mifx::elementForm('info')
                    ,'<p class="agree">'
                    , Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) )
                    , __('privacy-agreement',array(
                                ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') )
                             ))
                    ,'</p>'
                    , Form::hidden('ebook',$current_ebook->permalink)
                    , Form::input('captcha',null,array('class'=>'verif required'))
                    , '<div class="captcha captcha-container">',$captcha,'</div>'
                    , Form::hidden('ucr',Session::instance()->get('captcha'))
                    , '<p><input type="submit" value="'. __('download') .'" class="btn_submit"/></p>'    
                    , '</fieldset>'    
                    , Form::close() ?>
    </div>
    <?php if (!empty($slider_ebook) && count($slider_ebook) > 1) echo '<span class="font_16">'. __('other-e-book') .'</span>'; ?>
        <div class="sl_wrp sl_wrp2">
         <?php
            $show_button = !empty($slider_ebook) && count($slider_ebook) > 4;
            if ($show_button)
                echo HTML::anchor( '#','',array( 'class'=>'bt_next2 cont enext') ); ?>
          <div class="slide_little2">
            <ul>

              <?php
               $active = Page::get('navigationRoot');
               foreach ($slider_ebook as $slider) {
                  
                  echo "<li>";
                  
                  // get media
                  $media = Model::factory('Medialibrary')->simple_application('ebook-small', $slider->id, 'site_navigations');
                  if (!empty($media) && $media->valid()) {
                    foreach($media as $media){                              
                        echo HTML::anchor( Page::sourceCorrection('e-book',$slider->permalink) , HTML::image(URL::root($media->folder . '/'. $media->file, array(), array('class'=> 'tool_action', 'data-placement'=> 'right', 'rel'=> 'popover', 'data-content' => $slider->label))));        
                    }
                  }else{
                    echo HTML::anchor(
                        Page::sourceCorrection('e-book',$slider->permalink)  , 
                        HTML::image(URL::templateImage('content/e-book.png')), 
                        array('class'=> 'tool_action', 
                          'data-placement'=> 'right', 
                          'rel'=> 'popover', 
                          'data-original-title' => $slider->label)
                        );
                  }

                  echo "</li>";
               }
            ?>        
            </ul>
          </div>
         <?php
            $show_button = !empty($slider_ebook) && count($slider_ebook) > 4;
            if ($show_button)
                echo HTML::anchor( '#','',array( 'class'=>'bt_prev2 cont eprev') ); ?>
        </div>
        
        
<script type="text/javascript">
    $(document).ready(function(){
	    $('#demo-Phone').numeric({
            allow : '-'
        }); 
        $('#frm-ebook-download').validate({
		/*
            rules            : {
                accountName  : {
                    required : true,
                    notEqual : '<?php echo __('full-name'); ?>'
                },
                accountPhone : {
                    required : true,
                    notEqual : '<?php echo __('phone'); ?>'
                },
                accountReferral : {
                    required : false,
                    notEqual : '<?php echo __('referral'); ?>'
                }
            },      
			*/          
            invalidHandler : function(form, validator) { },     
            errorPlacement : function(error, element) { },
            submitHandler  : function(form) {
                
                frmID = $(form).attr('id');
                
                //nous_process_message( $('#bpopup-media').html() , 1 );
                nous_process_message_new();
                setTimeout(function(){                            
                    serialized = $(form).serialize();
                    $.ajax({
                        url     : form.action,
                        type    : 'POST',
                        data    : serialized,
                        async   : false,
                        success : function(res) {
                            if ($.trim(res) === 'success') {

                                // Track google analityc
                                //_gaq.push(['_trackEvent', 'Education', 'Ebook', 'Full']); 
                                ga('send', 'event', 'Education', 'Ebook', 'Full');
                                
                                $('#frm-ebook-download').html('<div style="padding:10px;"><?php echo __('thank-ebook-registering'); ?></div>');    
                                nous_disable_message();
                            } else {
                                nous_disable_message();
                                
                                jAlert(res);
                            }
                        }                         
                    });
                },180);
                
                return false;
                /** **/
            }
        });
    })
</script>        