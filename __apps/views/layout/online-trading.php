<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Layout View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ ?> 

<section class="middle_page">
    <div class="container">
        <div class="breadcrumb"><?php echo Page::breadCrumb(); ?></div>
            <?php      
                    $nav = Page::get('navigationObject');
                    $active = Page::get('navigationRoot');

                    $no = 1;
                    foreach ($nav as $i) { 
                        // translate navigation page
                        $i  = Translate::item('site_navigations', $i);
                        if ($i->permalink == $active ){
                            if (!empty($i->child)) {
                                echo '<h2 class="title_page">'. $i->label .'</h2>';    
                                echo '<div class="wid_trader">';
                                    
                                foreach ($i->child as $a){
                                    $widget = 'widget-icon';
                                    $page   = Model::factory('Medialibrary')->simple_application($widget, $a->id, 'site_navigations');

                                    echo '<div class="wrap_trd">
                                          <div class="ctr">';
                                    if (!empty($page) && $page->valid()) {
                                        foreach ($page as $icon) {                              
                                            echo HTML::image(URL::root($icon->folder . '/'. $icon->file,null));        
                                        }
                                    }
                                    // set default image
                                    echo HTML::anchor(Page::sourceCorrection($active,$a->permalink), '<span>'. (empty($a->judul_icon) ? $a->label : $a->judul_icon) .'</span>');                                
                                    echo '</div></div>';
                                }
                            }
                            echo "</div>";
                        }
                    }
                ?>
        </div>

        <?php
        $no = 0;
        foreach ($nav as $i) {                
            if ($i->permalink == $active ){
                if (!empty($i->child)) {
                    // looping
                    foreach($i->child as $a){

                        // translate
                        $a  = Translate::item('site_navigation', $a);

                        // add after first word with tag span
                        $word = explode(' ', $a->label);
                        $title = '';
                        for($m = 0; $m <= (count($word)-1); $m++):
                            if($m!==0)
                                $title .= '<span>'. $word[$m] .'</span> ';
                            else
                                $title .= $word[$m] .' ';
                        endfor; 

                        // get child
                        $imageChild = '';             
                        if(!empty($a->child)){
                            foreach($a->child as $c){
                                $widget = 'widget-trading';
                                $media  = Model::factory('Medialibrary')->simple_application($widget, $c->id, 'site_navigations'); 
                                if (!empty($media) && $media->valid()) {
                                    foreach ($media as $mediaLib) {
                                        $imageChild .=  HTML::image(URL::front($mediaLib->folder . '/'. $mediaLib->file), array('class' => 'ico_mob'));
                                    }
                                }
                            }
                        }

                        // widget config
                        $widget = array('banner-trading', 'banner-310x172', 'banner-410x375');
                        $imagesBanner   = '';   
                        // get widget banner
                        foreach($widget as $w){
                            $page_banner    = Model::factory('Medialibrary')->simple_application($w, $a->id, 'site_navigations');

                            if (!empty($page_banner) && $page_banner->valid()) {
                                foreach ($page_banner as $banner) {                              
                                    $imagesBanner =  HTML::image(URL::front($banner->folder . '/'. $banner->file), array('class'=> 'img_tablet'));
                                }
                            }
                        }
                        if($a->id !=42){
                            echo '<div class="'. $a->permalink .'">
                                    '. $imageChild .'
                                    <div class="container">
                                    '. $imagesBanner .'
                                    <div class="'.($no==0 ? 'right' : '' ).' monex texting">
                                    <h3>'. $title .'</h3>';

                                   // get page 
                                    $page_content = "";
                                    if(!empty($a->child)){
                                        foreach($a->child as $child){
                                            $page_content   = Model::factory('Page')->navigation_page($child->permalink);
                                            $media = Model::factory('Medialibrary')->simple_application('widget-icon',$child->id,'site_navigations');
                                            $page_content  = Translate::item('site_navigation', $page_content);
                                       
                                            if(!empty($media)){
                                                foreach($media as $p){
                                                   echo "<p>";
                                                    echo '<img src="'. URL::root($p->folder . '/'. $p->file).'" alt="" />';
                                                    //echo '<a  href="'. URL::front($active.'/'. $a->permalink .'/'. $child->permalink) .'">'.$page_content->title.'</a>';
                                                    echo '<a  href="'. Page::sourceCorrection($active, $a->permalink .'/'. $child->permalink) .'">'.$page_content->title.'</a>';
                                                    echo $page_content->intro;    
                                                    echo "</p>";
                                                }
                                            }
                                        }
                                    }  

                                    $page_content  = Model::factory('Page')->navigation_page($a->permalink);
                                    $page_content  = Translate::item('site_navigation_pages', $page_content);
                                    
                                    if(!empty($page_content)){
                                        echo "<p>";
                                        echo $page_content->intro;  
                                        echo "</p>";

                                        if($a->id != 43){
                                            echo '<a href="'. Page::sourceCorrection($active, $a->permalink) .'">'. __('read-more') .'  
                                                <img src="'. URL::templateImage('material/arr_news.png') .'" alt=""></a>';
                                        }

                                    }
                                    
                                    echo '</div>
                                          </div>
                                          </div>';
                            $no++;
                        }
                    }
                }
            }
        }
        ?>  
       <div class="faq_wrap">
            <div class="container">
                <div class="text_faq">
                    <h3>Frequently Asked Questions</h3>
                    <div>
                    <?php
                    $page  = Model::factory('Page')->navigation_page('pertanyaan-yang-paling-sering-diajukan');
                    // translate 
                    $page  = Translate::item('site_navigation_pages', $page);
                    echo $page->intro;
                    ?>
                    </div>
                    <a href="<?php echo Page::sourceCorrection($active, 'pertanyaan-yang-paling-sering-diajukan'); ?>">Read More  <img src="<?php echo URL::templateImage('material/arr_news.png'); ?>" alt=""></a>
                </div>
            </div>
        </div>
        <div class="btn_signUp">
            <?php echo __('sign-up-for-account-today'); ?>
            <div>
            <a href="<?php echo Page::get('completedNavigationUri:open-demo-account.php');?>" class="demo_btn">Demo</a>
            <a href="<?php echo Page::get('completedNavigationUri:open-live-account.php');?>" class="live_btn">Live</a>
            </div>
        </div>
</section>