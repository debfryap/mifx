<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>

<section class="widget_wrap">
    <?php 
        echo '<div class="container">';
        
        echo View::factory('general/frm-account-mobile');
        
        foreach (Page::get('navigationObject') as $n) {
            
            if ($n->permalink != 'trading-products') continue;
            
            if (!empty($n->child)) {
                $tc = count($n->child);
                foreach ($n->child as $i => $n) {
                    
                    $n     = Translate::item('site_navigations',$n);
                    
                    $icon  = Model::factory('Medialibrary')->simple_application('widget-icon',$n->id,'site_navigations')->current();
                    
                    $short = Model::factory('Page')->navigation_page($n->permalink); //DB::select('intro')->from('site_navigation_pages')->where('site_navigation_id','=',$n->id)->execute()->get('intro');
                    $short = !empty($short->intro) ? substr(strip_tags($short->intro,'<strong><b><i><u>'),0,116) : '';
                    
                    $attr = $n->permalink == 'forex-komoditi' ? array('style'=> 'margin-left: -8px !important;margin-right: -2px !important;') : null;

                    echo '<div class="widget';
                    if ($i == 0) echo ' first';
                    if ($i == $tc-1) echo ' last';
                    echo '">';
                    echo '<span>',  
                            ( !empty($icon->folder) ? HTML::image( URL::mediaImage( $icon->folder . '/' . $icon->file ,null ), array('width' => 48, 'height' => 44) ) : '' ),                  
                            HTML::anchor(Page::get("completedNavigationUri:$n->permalink"), empty($n->judul_icon) ? $n->label : $n->judul_icon, $attr),   
                         '</span>',
                         "<p>$short ...</p>",
                         HTML::anchor(Page::get("completedNavigationUri:$n->permalink"), HTML::image(URL::templateImage('material/btn_more.jpg'), array('width'=>87, 'height'=> 16) ));
                    
                    echo '</div>';
                }
            }
            break;
        } 
        echo '</div>'; 
        echo HTML::image( URL::templateImage('material/shadow_widget.jpg') , array('class'=>'shadow', 'width'=>1074, 'height'=> 34) );?>
</section>

<section class="gr">
	<div class="container">
        <div class="widget_small">
        <?php
            foreach (Page::get('navigationObject') as $n) {
            
                if ($n->permalink != 'trading-products') continue;
                
                if (!empty($n->child)) {
                    $tc = count($n->child);
                    foreach ($n->child as $i => $n) {
                        
                        $n     = Translate::item('site_navigations',$n);
                        
                        $icon  = Model::factory('Medialibrary')->simple_application('widget-icon',$n->id,'site_navigations')->current();
                        //<a href=""> <img src="images/material/ico_forex.jpg" alt="" width="35px" height="35px"/> Forex & Komoditi</a>
//                    	<a href=""> <img src="images/material/ico_index.jpg" alt="" width="35px" height="35px"/> Index Futures</a>
//                    	<a href=""> <img src="images/material/ico_cfd.jpg" alt="" width="35px" height="35px"/> CFD</a>
//                    	<a href=""> <img src="images/material/ico_komod.jpg" alt="" width="35px" height="35px"/> Komoditi Multilateral</a>
                        
                        $judulIconTradingProduct = empty($n->judul_icon) ? $n->label : $n->judul_icon;


                        if (!empty($icon->folder)) {
                            $judulIconTradingProduct = HTML::image( URL::mediaImage( $icon->folder . '/' . $icon->file ,null ) , array ('width'=>'35px') ) . $judulIconTradingProduct ;  
                        }
                        echo HTML::anchor( Page::get("completedNavigationUri:$n->permalink") , $judulIconTradingProduct);
                    }
                }
                break;
            } 
        ?>
        </div>
        	
		<div class="plug" style="border-bottom: none !important;">
            <?php echo View::factory('home/trading'); ?>
			<div class="greeter">
                <?php
                    if (!empty($welcome)) {
                        echo '<h2>', preg_replace('/(monex)/i','<b>$1</b>',$welcome->title) ,'</h2>',
                             $welcome->intro,
                             HTML::anchor( Page::get('completedNavigationUri:selamat-datang-di-monex') , HTML::image( URL::templateImage('material/btn_more.jpg'), array('width'=>87, 'height'=> 16) ) );
                    }
                    
                    $lang   = Request::$initial->param('lang');
                    $relImg = file_exists('assets/default/site/images/material/ref-'.$lang.'.jpg') ? 'material/ref-'.$lang.'.jpg' : 'material/ref-id.jpg';
                    $absImg = HTML::image(URL::templateImage($relImg), array('width'=> 202, 'height'=>31));
                    $margin = $lang == 'id' ? '45px auto 0' : '9px auto 0';
                    
                    echo HTML::anchor(Translate::uri(Language::$current,'partnership'),$absImg,array('style'=>'display:block;width:202px;margin:'.$margin.';'));
                    ?>
			</div>
			<div class="news_h">
				<?php
                    echo '<h2>', __('highlight'),' <b>', Page::get('simpleBreadCrumb:artikel'), '</b></h2>'; 
                    
                    if (!empty($articles)) {
                        echo '<ul>';
                        if (count($articles) == 1)
                        {
                            echo '<li>',
                                    HTML::anchor( Page::get('completedNavigationUri:artikel') . '/' .$articles->permalink , $articles->label ),
                                    '<i>',Page::datetime($articles->date),'</i>',
                                 '</li>';
                        }
                        else 
                        {
                            foreach ($articles as $a) 
                            { 
                                echo '<li>',
                                        HTML::anchor( Page::get('completedNavigationUri:artikel') . '/' .$a->permalink , $a->label ),
                                        '<i>',Page::datetime($a->date),'</i>',
                                     '</li>';
                            }
                        }
                        echo '</ul>';                        
                    } 
                    /*
                    echo '<div class="monex">',
                            __('jadwal-monex-edukasi',array(':tag'=>'b'));
                    
                    if (!empty($soap_schedules)) {
                        foreach ($soap_schedules as $i => $s) {
                            $title_schs = "title_". Language::$current;
                            $title_schs = empty($s[$title_schs]) ? $s["title_id"] : $s[$title_schs];
                            
                            echo '<p>',
                                        HTML::anchor( Page::get('completedNavigationUri:seminar-training') , $title_schs ), ' ',
                                        Page::datetime($s['date']),
                                     '</p>';
                            break;
                        }
                    }
                    if (!empty($schedules)) {
                        foreach ($schedules as $i => $s)
                        {
                            if (is_numeric($i))
                            {                                
                                echo '<p>',
                                        HTML::anchor( Page::get('completedNavigationUri:seminar-training') , $s->label ), ' ',
                                        Page::datetime($s->date),
                                     '</p>'; 
                            } else {                                
                                echo '<p>',
                                        HTML::anchor( Page::get('completedNavigationUri:seminar-training') , $schedules->label ), ' ',
                                        Page::datetime($schedules->date),
                                     '</p>'; 
                                break;
                            }
                        }
                    }          
                                   
                    echo '</div>';*/ ?>
			</div>	
		</div>
		
        <?php 
        /*
            if (!empty($banners)) {
                echo '<div class="thumb_hi">',
    		              '<ul>'; 
                foreach ($banners as $banner) {      
                    
                    $detail = json_decode($banner->detail);
                    
                    if (!empty($banner->folder))
                    {$bannerAttrs = array();
                        if (isset($detail->banner_title1)) {
                            
                            if (!empty($detail->banner_permalink)) {
                                if (strpos($detail->banner_permalink,'http') !== false) {
                                    $bannerUri = $detail->banner_permalink;
                                    $bannerAttrs['target'] = '_blank';
                                } else {
                                    $bannerUri = Translate::uri(Language::$current,$detail->banner_permalink);
                                }
                            } else {
                                $bannerUri = '#';
                            }
                        }else{
                            $bannerUri = '#';
                        } 
                        
                        echo '<li>',
                                '<div class="img">',
                                    HTML::anchor($bannerUri , HTML::image( URL::mediaImage( $banner->folder . '/' . $banner->file , null ) ), $bannerAttrs ),
                                '</div>';
                                
                        
                        
                        if (isset($detail->banner_title2)) {

                            if(Language::$current == 'cn' || Language::$current == 'tw'){
                                $nameBannerTitle2 = 'banner_title2_'.Language::$current;

                                $bannerTitle2 = !empty($detail->$nameBannerTitle2) ? $detail->$nameBannerTitle2 : $detail->banner_title2;
                            }else
                                $bannerTitle2 = $detail->banner_title2;

                            if (!empty($detail->banner_permalink_2)) {
                                $bannerAttrs = array('style'=>'color: #249520;text-decoration:none;');
                                if (strpos($detail->banner_permalink_2,'http') !== false) {
                                    $bannerUri = $detail->banner_permalink_2;
                                    $bannerAttrs['target'] = '_blank';
                                } else {
                                    $bannerUri = Translate::uri(Language::$current,$detail->banner_permalink_2);
                                }

                                echo HTML::anchor( $bannerUri , $bannerTitle2 , $bannerAttrs );

                            } else {
                                echo $bannerTitle2;
                            }
                        }  
                        
                        if (isset($detail->banner_title1)) {
                            $bannerAttrs = array();
                            if (!empty($detail->banner_permalink)) {
                                if (strpos($detail->banner_permalink,'http') !== false) {
                                    $bannerUri = $detail->banner_permalink;
                                    $bannerAttrs['target'] = '_blank';
                                } else {
                                    $bannerUri = Translate::uri(Language::$current,$detail->banner_permalink);
                                }
                            } else {
                                $bannerUri = '#';
                            }

                            if(Language::$current == 'cn' || Language::$current == 'tw'){
                                $nameBannerTitle1 = 'banner_title1_'.Language::$current;

                                $bannerTitle1 = !empty($detail->$nameBannerTitle1) ? $detail->$nameBannerTitle1 : $detail->banner_title1;
                            }else
                                $bannerTitle1 = $detail->banner_title1;

                            echo '<p>' , HTML::anchor( $bannerUri , $bannerTitle1 , $bannerAttrs ) ,'</p>';
                        
                        }                            
                                
                        echo HTML::anchor( 
                                    empty($detail->banner_permalink) ? '#' : $detail->banner_permalink , 
                                    HTML::image( URL::templateImage('material/btn_detail.jpg') ) ,
                                    array('class'=>'btn') ),
                             '</li>';          
                    }                              
                }
                echo '</ul>',
                     '</div>';
            } */
            
            ?>
        
	</div>
</section> 
  
<div id="popup1" style="display:none">
  <div class="wrap_popup">
    <div class="btn_close">
      <a href="#" onclick="$.fancybox.close(); return false;">
        <img src="http://client2.webarq.com/microsite/bandung/assets/default/site/images/material/btn-close.png" width="55" height="55" alt="" />
      </a>
    </div>
    <div class="content">
      <img src="<?php echo URL::templateImage('material/bg-pemenang-jan-2015-revisi.jpg'); ?>" />
        <div class="data-table" style="display:none;">
            <table width="100%" border="0" align="center">
              <tbody>
                  <tr>
                    <td><span>Nama :</span>
                       Fahmi Wanto</td>
                    <td><span>Nama :</span>
                      Agus Sukmono Sigit </td>
                    <td><span>Nama :</span>
                      Yosef Ari Kuncoro Pramesti</td>
                  </tr>
                  <tr>
                    <td>Monex  Ravindo Lt. 8</td>
                    <td>Monex Cabang Menara Salemba</td>
                    <td>Monex Cabang  Tegal</td>
                  </tr>
                  <tr>
                    <td><span>No Account :</span>
                      XXX12333 </td>
                    <td><span>No Account :</span>
                      XXX02629 </td>
                    <td><span>No Account :</span>
                      XXX09198 </td>
                  </tr>
                </tbody>
              </table>
        </div>
    </div>
  </div>
</div>
  
<div id="popup2" style="display:none">
  <div class="wrap_popup">
    <div class="btn_close">
      <a href="#" onclick="$.fancybox.close(); return false;">
        <img src="http://client2.webarq.com/microsite/bandung/assets/default/site/images/material/btn-close.png" width="55" height="55" alt="" />
      </a>
    </div>
    <div class="content">
      <img src="<?php echo URL::templateImage('material/popup-MIC-2015.jpg'); ?>" />
        <div class="data-table" style="display:none;">
            <table width="100%" border="0" align="center">
              <tbody>
                  <tr>
                    <td><span>Nama :</span>
                       Fahmi Wanto</td>
                    <td><span>Nama :</span>
                      Agus Sukmono Sigit </td>
                    <td><span>Nama :</span>
                      Yosef Ari Kuncoro Pramesti</td>
                  </tr>
                  <tr>
                    <td>Monex  Ravindo Lt. 8</td>
                    <td>Monex Cabang Menara Salemba</td>
                    <td>Monex Cabang  Tegal</td>
                  </tr>
                  <tr>
                    <td><span>No Account :</span>
                      XXX12333 </td>
                    <td><span>No Account :</span>
                      XXX02629 </td>
                    <td><span>No Account :</span>
                      XXX09198 </td>
                  </tr>
                </tbody>
              </table>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function fix_form_register()
{
  var width_acc = $(window).width()-940;
  $(".banner_home .container.account_wrap3").css({ right:width_acc/2});
}

$(function(){
  fix_form_register();
});

$(window).load(function(){
  fix_form_register();
});
$(window).resize(function(){
  fix_form_register();
});

    //Index Forex
    $(window).load(function(){
        $.getScript('http://quotes.monexnews.com:8000/socket.io/socket.io.js',function(){
            /*<![CDATA[*/
             
            jQuery(function($) {
                //return false;
                var socket = io.connect('http://quotes.monexnews.com:8000');
                socket.emit('channel','quotes');
                
                socket.on('update',function(data){
                    
                    var quotes = $.parseJSON(data);
                        
                    $.each(quotes,function(key, val){ 
                        if(val.symbol == 'EURUSD' ||
                           val.symbol == 'GBPUSD' ||
                           val.symbol == 'USDCHF' || 
                           val.symbol == 'USDJPY' ||
                           val.symbol == 'AUDUSD' ||
                           val.symbol == 'XAUUSD' ||
                           val.symbol == 'XAGUSD' ||
                           val.symbol == '#CO-LS' ||
                           val.symbol == 'NGAS'   ||
                           val.symbol == 'GOOGLE' ||
                           val.symbol == 'GOLDMAN'||
                           val.symbol == 'EXXON'  ||
                           val.symbol == 'INTEL'  ||
                           val.symbol == 'BLK_ROCK' ||
                           val.symbol == '#DJ5' ||
                           val.symbol == '#SP50' ||
                           val.symbol == '#NQ20' ||
                           val.symbol == 'KSA' ||
                           val.symbol == 'HKA' ) {
                                
                            //console.log(val);
                            s = val.symbol.replace('#','').replace('&','');
                            tr = $('#' + s).parent();
                            tr.removeClass('up').removeClass('down');
                            
                            $('#'+s).next().text(val.bid).removeClass('up').removeClass('down').addClass(val.status.bid);
                            $('#'+s).next().next().text(val.ask).removeClass('up').removeClass('down').addClass(val.status.ask);
                            $('#'+s).next().next().next().text(val.high).removeClass('up').removeClass('down').addClass(val.status.high);
                            $('#'+s).next().next().next().next().text(val.low).removeClass('up').removeClass('down').addClass(val.status.low);
                        		
                                
                        }
                    });
                });
            
            });
            /*]]>*/
        });
    });
</script>