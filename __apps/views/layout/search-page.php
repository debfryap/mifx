<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
?>
    <div class="container">
      <div>
        <h2 style="border-bottom: 1px solid #D1D1D1; margin-left: -10px;"><?php echo __('search-result', array(':keyword'=>Request::$current->param('section1'))); ?></h2>
        <br />
        <?php

        // Ebook result //
        if(!empty($ebook) && $ebook->valid())
        {    
          echo "<h4>E-Book</h4>";
          echo "<ul>";
          foreach($ebook as $obj)
          {
            $obj  = Translate::item('ebook', $obj);
            echo HTML::anchor(URL::front('education/e-book/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // Faqs result //
        if(!empty($faqs) && $faqs->valid())
        {
          echo "<h4>Faqs</h4>";
          echo "<ul>";
          foreach($faqs as $obj)
          {
            $obj  = Translate::item('faqs', $obj);
            echo HTML::anchor(URL::front('online-trading/pertanyaan-yang-paling-sering-diajukan/' . $obj->permalink), '<li>'. $obj->question .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // Glossaries result //
        if(!empty($glossaries) && $glossaries->valid())
        {
          echo "<h4>Glossaries</h4>";
          echo "<ul>";
          foreach($glossaries as $obj)
          {
            $obj  = Translate::item('glossaries', $obj);
            echo HTML::anchor(URL::front('education/learning-center/trading-glossary/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // Monex Schedules result //
        /*if(!empty($monex_schedules) && $monex_schedules->valid())
        {
          echo "<h4>Monex Schedules</h4>";
          echo "<ul>";
          foreach($monex_schedules as $obj)
          {
            echo HTML::anchor(URL::front('glossaries/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }*/

        // Monex Videos result //
        if(!empty($monex_videos) &&  $monex_videos->valid())
        {
          echo "<h4>Monex Video</h4>";
          echo "<ul>";
          foreach($monex_videos as $obj)
          {
            $obj  = Translate::item('monex_videos', $obj);
            echo HTML::anchor(URL::front('education/video/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // News result //
        if(!empty($news) && $news->valid())
        {
          echo "<h4>News</h4>";
          echo "<ul>";
          foreach($news as $obj)
          {
            $obj  = Translate::item('news', $obj);
            echo HTML::anchor(URL::front('education/learning-center/artikel/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // News Event result //
        if(!empty($news_event) && $news_event->valid())
        {
          echo "<h4>News Event</h4>";
          echo "<ul>";
          foreach($news_event as $obj)
          {
            $obj  = Translate::item('news_events', $obj);
            echo HTML::anchor(URL::front('about-us/kegiatan-perusahaan/monex-corporate-event/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // News Update result //
        if(!empty($news_update) && $news_update->valid())
        {
          echo "<h4>News Updates</h4>";
          echo "<ul>";
          foreach($news_update as $obj)
          {
            $obj  = Translate::item('news_updates', $obj);
            echo HTML::anchor(URL::front('about-us/kegiatan-perusahaan/monex-update/' . $obj->permalink), '<li>'. $obj->label .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }

        // Page result //
        if(!empty($page) && $page->valid())
        {
          echo "<h4>Page</h4>";
          echo "<ul>";
          foreach($page as $obj)
          {
            $obj  = Translate::item('site_navigation_pages', $obj);
            echo HTML::anchor(Page::get('completedNavigationUri:'.$obj->permalink), '<li>'. $obj->title .'</li>', array('target'=> '_blank'));
          }
          echo "</ul>";
        }
        ?>
      </div>
    </div>
