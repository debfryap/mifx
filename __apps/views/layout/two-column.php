<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>

<section class="middle_page">
    <div class="container">
		<div class="left_wrap">
            <div class="scroll_mobile">
            <?php
                if (!empty($showNavigation)) {
                    echo Widget::load('page/side-navigation',array('showNavigation'=>$showNavigation));    
                }
                
               // if (!empty($live)) echo View::factory('general/frm-account'); 
                
					                    echo '
                        <div class="row-mte">
                            <ul class="parent-button-new">
                                <li>
                                    <a href="'. URL::front('open-live-account.php').'" class="button_acc-new">
                                        <span class="inside-ban">
                                        <img src="/media/images/widget-accounts/btn-live.jpg" width="59">
                                        <span class="ban-content">
                                        '.__('Buat').'<br>'.__('Live Akun').'<span> Monex </span></span></span></a>
								</li>
                                <li>
                                            <a href="'. URL::front('open-demo-account.php') .'" class="button_acc-new">
                                            <span class="inside-ban"><img src="/media/images/widget-accounts/btn-demo.jpg" width="59">
                                            <span class="ban-content">
                                                '.__('Buat').'<br><span></span>'.__('Demo Akun').'<span> Monex </span></span></span></a>
                                </li>
                            </ul>
                        </div>';
                if (!empty($htmlBanner)) echo $htmlBanner; ?>
                </div>
                <div class="side_menu_mobile"></div>
		</div>
		
		<div class="right_wrap">
			<div class="breadcrumb">
			    <?php  echo empty($breadcrumb) ? Page::breadCrumb() : $breadcrumb; ?>	
			</div>
			<?php echo isset($content) ? $content : ''; ?>

      <?php if (App::$config->menu->active =='about-us'){ 
                    if(Request::current()->param('section1') == 'selamat-datang-di-monex' ||
                     ( 
                        Request::current()->param('section1') == NULL AND 
                        Request::current()->controller() != "Kegiatanperusahaan"
                     )
                    ){?>
                <div class="ebook_wrap">
                  <img src="<?php echo URL::templateImage('content/Menuntun-Anda-di-Pasar-Keuangan-Global.png'); ?>" class="img">     
                  <div class="descrip">
                    <h2 class="title_page" style="margin-left: 48px;"><?php echo __('monex-company-profile'); ?></h2>
                    <p></p>           
                    <div class="btn_ebook">
                      <a class="demo" href="<?php echo URL::base().'ebook/Menuntun-Anda-di-Pasar-Keuangan-Global'; ?>" target="_blank"><?php echo __('view'); ?></a>
                      <div class="full">
                        <a style="color: #fff !important; text-decoration: none;" href="<?php echo URL::front('about-us/download/Menuntun-Anda-di-Pasar-Keuangan-Global'); ?>">
                            <?php echo __('download-about-us'); ?>
                        </a>
                      </div>
                    </div>
                  </div>
                </div> 
            <?php
                    }
                } 
            ?>
		</div>
	</div>
    <?php echo isset($footer) ? $footer : ''; ?>
</section>