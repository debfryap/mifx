<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Layout View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ ?> 
<div>
    <a href="<?php echo URL::front(); ?>">
        <img src="<?php echo URL::templateImage('content/banner-404.jpg'); ?>" alt="">
    </a>
</div>