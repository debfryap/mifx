<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Layout View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ 

//Check for query Source
$qSource = Request::$initial->query('Source');

//Check for session Source
if (empty($qSource)) {
    $qSource = Session::instance()->get('Source');                
}

$qSource = empty($qSource) ? '' : "?Source=$qSource";
 
$accountUri    = Translate::uri(Language::$current,'accounts');
?>

<section class="middle_page<?php if ($section == 'create-monex-live-account' || $section == 'create-free-demo-account') echo " no_background";?>">
    <div class="container">
        <div class="breadcrumb"><?php  echo empty($breadcrumb) ? Page::breadCrumb() : $breadcrumb; ?></div>        
        <?php 
            if ($section == 'verified-registrant-account') {
                echo 'Verified Registrant Account';
                echo '</div></section>';
                return;
            }
            
            if (empty($page)) 
            { 
                header('Location: '. URL::front('errors/404')); exit;
                echo '<h2 class="title_page">',__('no-data-found-title'),'</h2><p></p><p>',__('no-data-found-description'),'</p>';
                echo '</div></section>';
                return;
            }
            
            echo '<h2 class="title_page">',$title,'</h2>';
            
            echo '<div class="wid_trader">'; //Div 1
            
            $banner = 'banner-account';
            
            if (!empty($page->mediaObject->$banner) && $page->mediaObject->$banner->valid())
            {   
                $media  = $page->mediaObject->$banner->current();
                
                echo HTML::image( URL::mediaImage( $media->folder . '/' .$media->file , null ) , array('class'=>'img_account') );
            }
            else
            {
                if($section != null ){
                    $bannerId = $section == 'create-monex-live-account' 
                                || $section == 'create-free-demo-account'
                                || $section == 'create-monex-live-account-success' 
                                || $section == 'create-free-demo-account-success'
                                     ? 24 : $page->id ;
                        
                    $media = Model::factory('Medialibrary')->simple_application('banner-account',$bannerId,'site_navigation_pages')->current();
                    
                    if (!empty($media) and ($section != 'create-free-demo-account-success' and $section != 'verify-status' and $section != 'create-monex-live-account-success'))
                    {   
                        //echo HTML::image( URL::mediaImage( $media->folder . '/' .$media->file , null ) , array('class'=>'img_account') );
                    }
                }
            }
            
            if($section == 'create-free-demo-account-success' 
                or $section == 'verify-status'
                or $section == 'create-monex-live-account-success'){
                
                echo '<h3>',preg_replace('/(monex)/i','<span>$1</span>',$page->title),'</h3>';        
                echo $page->description;  

            } else {
                
                if($section == null )
                    echo '<div class="account_detail edited" style="width: 100% !important">'; // Div 2
                else
                    echo '<div class="account_detail edited">'; // Div 2

                if ($section == 'create-monex-live-account' || $section == 'create-free-demo-account')
                {
                    echo '<div class="caption" style="width: 350px;">'. __('each-month-we-will-give').'</div>';

                    $client = Mifx::get('_soap');
                    
                    if($client != null){
                        
                        $account_referral = $client->getMarketing($codeReferal);
                        
                        if($account_referral == 'Invalid Code' and $codeReferal != NULL){
                            echo '<script type="text/javascript"> jAlert(\'<b>Invalid key. Please contact your sales</b>\', \'error\'); </script> ';
                        }else{
                            //echo '<h3 class="norborder"> Sign Up <br /> for <span>Account</span> today ! </h3>';
                            //echo Mifx::accountForm($section,$codeReferal,$regularOrPartial);
                        ?>
                            <div id="div-form-account" class="loader">
                                <form method="post" id="frm-live-account" class="is-validate create_account">
                                    <div class="ipad-camp"><img src="http://client.webarq.com/mifx_compro/images/content/ipad-camp.png" alt=""/></div>
                                    <div style="float: left;margin:200px auto;height:30px; width: 100%;">
                                        <?php echo HTML::image(URL::base(true) . 'assets/default/site/images/loading.gif'); echo ('Please wait while loading registration form'); ?>
                                    </div>
                                </form>
                            </div>
                            <script type="text/javascript">
                                <?php 
                                    if (!empty(App::$module->language) && Language::$current != Language::$default){
                                        $href = Translate::uri(Language::$current,Language::$current.'/ajax/load-form-account');
                                    }else{
                                        $href = URL::front('ajax/load-form-account');
                                    }

                                    // Monex Affiliate Script
                                    $query = '?';
                                    $adv     = Request::$initial->query('adv');
                                    if(!empty($adv)) $query .= 'adv='. $adv;
                                    
                                    echo "var URL_ajax_load_account= '". $href ."';";
                                ?>
                                $(window).load(function(){

                                    $('#div-form-account').load(URL_ajax_load_account + '/<?php echo $section.$query;; ?>',{'code_referal' : '<?php echo $codeReferal;?>','regular_or_partial':'<?php echo $regularOrPartial;?>'},
                                    function(response, status, xhr){
                                        if(status == 'error'){
                                            console.log("Sorry but there was an error: "+ xhr.status +" "+ xhr.statusText );
                                            $(this).find('form div').html('<p align="center">Service is down, try again !</p>');
                                            return false;
                                        }else{
                                            load_form_account();
                                        }
                                        
                                    });
                                });
                            </script>
                        <?php 
                            echo '<div id="bpopup-media">',__('do-not-close-browser'),'</div>';
                        }
                    }   
                }
                else 
                {
                    //if ($section != 'create-monex-live-account-success' && $section != 'create-free-demo-account-success' )
                        //echo '<h3>',preg_replace('/(monex)/i','<span>$1</span>',$page->title),'</h3>';
                        
                    echo $page->description;  
                    
                    if (!empty($widgets)) {
                        echo '<div class="row">'; // Div Class Row
                        echo '<ul class="parent-button-new">';
                        foreach ($widgets as $w) {
                            
                            $w = Translate::item('site_navigations',$w);               
                                         
                            $m = Model::factory('Medialibrary')->simple_application('widget-account',$w->id,'site_navigations')->current();
                            
                            $h = '#';
                            
                            if ($w->permalink == 'create-monex-live-account' || $w->permalink == 'open-live-account.php' || $w->permalink == 'create-free-demo-account' || $w->permalink == 'open-demo-account.php')
                                $h = Page::get("completedNavigationUri:$w->permalink"); 
                            
                            echo '<li>';
                            
                            echo '<a href="'. $h .'" class="button_acc-new">',
                                 '<span class="inside-ban">';   ;
                            
                            if(!empty($m))
                                echo HTML::image( URL::mediaImage( $m->folder . '/' . $m->file , null), array('width' => 59 ) );
                            
                            echo '<span class="ban-content">';
                            
                            $title = !empty($w->judul_icon) ? $w->judul_icon : $w->label;
                             
                            if ($w->permalink == 'create-monex-live-account' || $w->permalink == 'open-live-account.php') {
                                $splitter = explode(' ',$title,2);
                                
                                echo count($splitter) < 2 ? $splitter[0]  : $splitter[0];    
                                
                                if (!empty($splitter[1])) echo "<br/>", preg_replace( '/(monex)/i','<span>$1</span>',$splitter[1]  );
                                
                            } elseif ($w->permalink == 'create-free-demo-account' || $w->permalink == 'open-demo-account.php') {
                                $splitter = explode(' ',$title,2);
                                
                                echo count($splitter) < 2 ?  $splitter[0]  : $splitter[0]; 
                                
                                if(Language::$current== 'en'){
                                    if (!empty($splitter[1])) echo "<br/>", preg_replace( '/(free)/i','<span>$1</span>',$splitter[1]  );   
                                }elseif(Language::$current == 'id'){
                                    if (!empty($splitter[1])) echo "<br/>", preg_replace( '/(gratis)/i','<span>$1</span>',$splitter[1]  ); 
                                }else{
                                    if (!empty($splitter[1])) echo "<br/>", preg_replace( '/(free)/i','<span>$1</span>',$splitter[1]  );
                                }   
                                
                            }  else {
                                
                                $splitter = explode(' ',$title,3);
                                
                                if (!empty($splitter[0]) and !empty($splitter[0])) echo "<span>", $splitter[0] .' '. $splitter[1] , '</span> '. $splitter[2]  ;  
                            }
                            
                            echo '</span></span></a></li>';
                        }
                        
                        echo '</ul>'; 
                        echo '</div>'; // End Div Class Row
                       
                    }
                }
                   
                echo '</div>';  //End Div 1 

                if ($section == 'create-monex-live-account' || $section == 'create-free-demo-account'){
                    // views terms and conditions
                    echo '<div class="drop-ipad">' . __('to-qualify') . /** '<a href="#" id="tutup">??</a>' . **/ '</div>';
                }
                
                echo '</div>';  //End Div 2 
        }
            ?>
    </div>      
    <?php
        if ($section == 'create-monex-live-account' || $section == 'create-free-demo-account')
        {
            if (!empty($page))
            {
                foreach ($page as $i => $p) {
                    echo '<div class="',($i % 2 == 0 ? 'step_register' : 'client_area'),'">',
                            '<div class="container">',
                                '<h2>',$p->title,'</h2>',
                                $p->description,
                            '</div>',
                         '</div>';                          
                }
            }
        } ?>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        
        //Validate default value for each element
        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            return !(element.value == param);
        });
                    
    });
    
    function load_form_account(){
            $(document).ready(function(){

            $('#live-Phone,#demo-Phone').numeric({
                allow : '-'
            }); 
            
            var captchaCount = 1;
            $('form.is-validate').each(function(){
                $(this).validate({
                    rules            : {
                        accountName  : {
                            required : true,
                            notEqual : '<?php echo __('full-name'); ?>'
                        },
                        accountPhone : {
                            required : true,
                            notEqual : '<?php echo __('phone'); ?>'
                        }/**,
                        accountReferral : {
                            required : true,
                            notEqual : '<?php echo __('referral'); ?>'
                        }**/
                    },                
                    invalidHandler : function(form, validator) { },     
                    errorPlacement : function(error, element) { },
                    submitHandler  : function(form) {
                        
                        frmID = $(form).attr('id');
                        
                        //nous_process_message( $('#bpopup-media').html() , 1 );
                        nous_process_message_new();
                        
                        setTimeout(function(){                            
                            serialized = $(form).serialize();
                            $.ajax({
                                url     : form.action,
                                type    : 'POST',
                                data    : serialized,
                                async   : false,
                                success : function(res) {
                                    str = $.trim(res);
                                    if (str === 'success') {                                    
                                        if (frmID == 'frm-demo-account')
                                            window.location.href = '<?php echo $accountUri; ?>/create-free-demo-account-success<?php echo $qSource;?><?php echo !empty($codeReferal) ? "/$codeReferal" : '' ?>';
                                        else
                                            window.location.href = '<?php echo $accountUri; ?>/create-monex-live-account-success<?php echo $qSource;?><?php echo !empty($codeReferal) ? "/$codeReferal" : '' ?>';
                                        
                                        return;
                                    } else {
                                        if (str.substr(0,5) == 'goTo:') {
                                            newUrl = str.substr(5);
                                            window.location.href = newUrl;
                                        } else {                                     
                                            nous_disable_message();
                                            
                                              // check captcha
                                            if(res=="<ul class=\"error\"><li>Captcha masih salah. Silahkan masukkan sekali lagi</li></ul>" || res=="<ul class=\"error\"><li>Captcha did not match. Please correct</li></ul>"){
                                               
                                                if(captchaCount==3){
                                                    var action_captcha = $('.verifikasi img').attr('src');
                                                    var img = $("<img />").attr('src', action_captcha+"&refTrigger="+Math.random())
                                                                            .load(function() {
                                                                                if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                                                                                    alert('broken image, reload page!');
                                                                                } else {
                                                                                    $(".verifikasi img").remove();
                                                                                    $(".verifikasi").append(img);
                                                                                }
                                                                            });
                                                    captchaCount = 0;
                                                }
                                                
                                                captchaCount++;
                                            }

                                            jAlert(res);
                                        }
                                    }
                                } ,
                                error   : function() {                                    
                                    nous_disable_message();
                                    
                                    jAlert('<?php echo __('server-not-responding');?>');
                                }                          
                            });
                        },180);
                        
                        return false;
                        /** **/
                    }
                });
            })

       });
    }
</script>
<style type="text/css">
.wid_trader .account_detail ul li{ width: 300px; }
.button_acc-new {
   border: 1px solid #cfcfcf;
   box-sizing: border-box;
   min-height: 130px;
   margin-left: 10px;
   margin-right: 10px;
   margin-bottom: 10px;
   background: #e4e4e4; /* Old browsers */
   background: -moz-linear-gradient(top,  #e4e4e4 0%, #f6f6f6 100%); /* FF3.6+ */
   background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e4e4e4), color-stop(100%,#f6f6f6)); /* Chrome,Safari4+ */
   background: -webkit-linear-gradient(top,  #e4e4e4 0%,#f6f6f6 100%); /* Chrome10+,Safari5.1+ */
   background: -o-linear-gradient(top,  #e4e4e4 0%,#f6f6f6 100%); /* Opera 11.10+ */
   background: -ms-linear-gradient(top,  #e4e4e4 0%,#f6f6f6 100%); /* IE10+ */
   background: linear-gradient(to bottom,  #e4e4e4 0%,#f6f6f6 100%); /* W3C */
   filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e4e4e4', endColorstr='#f6f6f6',GradientType=0 ); /* IE6-9 */
   box-shadow: 0 0 7px rgba(0,0,0,.1);
   text-decoration: none;
   padding: 30px 10px;
}
.button_acc-new:hover {
   box-shadow: 0 0 7px rgba(0,0,0,.3);
}
.inside-ban {
   display: block;
   margin: auto;
   width: 85%;
}
.button_acc-new img {
   box-sizing: border-box;
   display: inline-block;
   float: left;
   margin-right: 15px;
   width: 25%;
}
.button_acc-new .ban-content {
   box-sizing: border-box;
   color: #010101;
   display: inline-block;
   font-family: 'myriad_semi';
   font-size: 20px;
   padding-top: 6px;
   width: 68%;
   
   font-style: normal !important;
}
.button_acc-new .ban-content > span {color: #299925;}
.acc-list-box li.two{
    height: 225px !important;
}
</style>