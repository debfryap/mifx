<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Layout View
 * @Module      ~
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ ?> 

<section class="middle_page">
    <div class="container">
        <div class="breadcrumb"><?php echo Page::breadCrumb(); ?></div>
        <?php
            $nav        = Page::get('navigationAll');
            $active     = Page::get('navigationRoot');
            $no         = 1;
            
            foreach ($nav as $i) { 
                
                if ($i->permalink == $active ){

                    if (!empty($i->child)) {
                        echo '<h2 class="title_page">'. $i->label ."</h2>";
                        echo '<div class="wid_trader">';
                            
                        foreach ($i->child as $a) {
                            // get widget icon
                            $widget = 'widget-icon';
                            $page    = Model::factory('Medialibrary')->simple_application($widget, $a->id, 'site_navigations');

                            echo '<div class="widget '.($no==1 ? ' first' : '').'"><span>';
                            if (!empty($page) && $page->valid()) {
                                foreach ($page as $icon) {                              
                                    echo HTML::image(URL::root($icon->folder . '/'. $icon->file,null));
                                }
                            }

                            $attr = $a->permalink == 'forex-komoditi' ? array('style'=> 'margin-left: -8px !important;margin-right: -2px !important;') : null;

                            echo HTML::anchor(Page::get("completedNavigationUri:$a->permalink"), (empty($a->judul_icon) ? $a->label : $a->judul_icon), $attr);
                            echo '</span><div class="linked">';
                            
                            if($a->child){
                               foreach($a->child as $b => $c){
                                     
                                    if ($b ==3) break;                                    
                                    
                                    echo '<div class="list">' , $c->label,'</div>';
                                    
                                    $no++;
                                
                                }          
                            }

                            echo '</div></div>';
                         $no++;   
                        }
                        
                        echo '</div>';
                        break;
                }
            }
        }
        
        ?>
    </div>

    <div class="trader_width_wrap">            
        <?php   
                $y = 0;
                foreach ($nav as $i) { 
                    if ($i->permalink == $active ){
                        if(!empty($i->child)){
                            foreach ($i->child as $ia => $a) {
                                $widget = 'widget-icon';
                                $page = Model::factory('Page')->navigation_page($a->permalink, array($widget));
                                
                                if(!empty($page->mediaObject)):
                                    
                                    if ($ia == 0) {
                                        $height = 250;  
                                    } elseif ($ia == 1) {
                                        $height = 220; 
                                    } elseif ($ia == 2) {
                                        $height = 300;
                                    } else {
                                        $height = 250;
                                    } 
                                
                                    
                                    echo '<div class="'. $a->permalink .' col '. (($y % 2) == 0 ? ' right' : ' left') .' '.$y.'">
                                          <div class="container"><div class="text">
                                          <h3>'. $a->label .'</h3>
                                            <p>'. $page->intro .'</p>'.
                                            HTML::anchor(Page::get("completedNavigationUri:$a->permalink"), __('read-more'). HTML::image(URL::templateImage('material/arr_news.png')))
                                          .'</div></div></div>';
                                endif;
                                $y++;
                            }
                        }
                    }
                }
            ?>            
        </div>

        <div class="btn_signUp">
            <?php echo __('sign-up-for-account-today'); ?>
            <div>
            <a href="<?php echo Page::get('completedNavigationUri:open-demo-account.php');?>" class="demo_btn">Demo</a>
            <a href="<?php echo Page::get('completedNavigationUri:open-live-account.php');?>" class="live_btn">Live</a>
            </div>
        </div>
</section>