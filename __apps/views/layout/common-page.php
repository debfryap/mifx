<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (empty($page)) { header('Location:'. URL::front('errors/404')); exit; echo '<h2 class="title_page">',__('no-data-found-title'),'</h2><p>',__('no-data-found-description'),'</p>'; return; }

if (is_array($page)) {
    if (count($page) > 1) {
        foreach($page as $item) {
            echo '<h2 class="acc_trigger auto"><a href="#">'. $item->title .'</a></h2>';
            echo '<div class="acc_container">'. $item->description .'</div>';
        }
        return;
    } else {
        $page = array_shift($page);
    }          
}

echo '<h2 class="title_page">'.(preg_replace('/(monex)/i','<span>$1</span>',$page->title)).'</h2>';
 
/** Get Media **/
if (!empty($page->id)) {
    $media = Model::factory('Medialibrary')->simple_application('banner-page', $page->id, 'site_navigation_pages');
    
    foreach($media as $media){
      echo "<p>";
      echo HTML::image(URL::root($media->folder . '/'. $media->file,null));
      echo "</p>";
    }
}    

if($page->permalink !='partnership'){
  echo $page->description;
}


if(!empty($page->permalink) && $page->permalink =='partnership'){

    echo '<div style="clear: both;" class="partner_box01 clearfix">', $page->intro ,'</div>';
    echo $page->description;
    
    echo '<div class="form_contact form_partner" id="form_partner"> <div class="form">'; 
    
    $checkError = App::$session->get('error-register',null);
    
    if (isset($checkError)) {
        if (is_array($checkError)) {
            //echo '<h3>',__("error"),'</h3>';
            echo '<ul>';
            foreach ($checkError as $key => $messages) {
                if (is_array($messages)) foreach ($messages as $errorMessage) echo '<li>',$errorMessage,'</li>';
                else echo '<li>',$errorMessage,'</li>';
            }
            echo '</ul>';
        } else {
            echo 'Registration failed. Error while trying to contact server';
        }
        App::$session->set('error-register',null);
    }
    
    if(empty($submitForm)){
        echo '<span>'. __('partner-registration') .'</span>';
        echo Form::open('',array( 'id' => 'frm-partner', 'class' => 'contact-validate', 'required'));
        
        if (!empty($affliateSources)) {
            echo Form::hidden('affiliate_source',key($affliateSources));
        }
        
        if (!empty($affiliateTypes)) {
            echo Form::hidden('affiliate_type',key($affiliateTypes));
        }
        
        echo Form::input('company', NULL, array('placeholder'=> __('company-personal-name') , 'required','class'=>'required')),
            Form::textarea('address',NULL, array('placeholder'=> __('address'), 'style'=>'width: 265px !important; height: 80px !important;', 'required','class'=>'required')),
            Form::input('city', NULL, array('placeholder'=> __('city') , 'required','class'=>'required')),
            Form::input('phone', NULL, array('placeholder'=> __('phone') , 'required','class'=>'required')),
            Form::input('handphone', NULL, array('placeholder'=> __('handphone') , 'required','class'=>'required')),
            //Form::input('email', NULL, array('placeholder'=> 'Email' , 'required')),
            '<input type="email" name="email" placeholder="'. __('email') .'" required class="required email" />',
            '<p><input type="submit" value="'. __('submit') .'" class="sbt_form" style="float: left;" onClick="ga(\'send\', \'event\', { eventCategory: \'Partnership\', eventAction: \'Registration\'});" />',

            //Form::submit('Submit','SUBMIT',array ( 'class' => 'sbt_form', 'style' => 'float: left;' , 'onclick'=> '_gaq.push(["_trackEvent", "Partnership","Registration"])')),'</p>',
            
            Form::close();
        echo '<br style="clear: both;" />
            <p class="limited_order">limited order</p>';
    }else{
        echo 'Terimakasih. Data anda sudah kami terima. Silahkan periksa email anda untuk verifikasi';
        App::$session->set('submit-form',false);
        $submitForm = false;
    }
    echo '</div>';
}


if($page->permalink == "mte"){ ?>
    <div id="form_trading">
        <form method="post" action="<?php echo Translate::uri(Language::$current,'ajax/request-mte'); ?>" class="form-belajar-trading">
              <table width="280" border="0" align="center" cellpadding="0">
            <tbody>
                <br>
                <tr>
                      <td><label for="select">
                        <?php echo Mifx::elementForm('kota'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="textfield">
                          <?php echo Mifx::elementForm('name'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="textfield">
                          <?php echo Mifx::elementForm('phone'); ?>
                        </label></td>
                    </tr>
                <tr>
                      <td><label for="textfield">
                          <?php echo Mifx::elementForm('email'); ?>
                        </label></td>
                    </tr>
					<!--
                <tr>
                  <td><label for="textfield">
                      <?php echo Mifx::elementForm('deposit'); ?>
                    </label>
                  </td>
                </tr>				-->
                <tr>
                      <td><label for="select2">
                      <?php echo Mifx::elementForm('referral'); ?>
                      </label></td>
                    </tr>
                <tr>
				
                    <td><?php echo Mifx::elementForm('source').Mifx::elementForm('info'); ?></td>
                </tr>
                <tr>
                      <td><!-- <label for="ckc">
                      <input type="checkbox" name="privacy_agreement" class="required" style="margin-top: -2px;margin-right: 4px;">
                      Anda telah membaca dan setuju -->
                      <?php /*
                        echo HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') );
                        //echo  Form::checkbox('privacy_agreement', null , false ,  
                         //           array( 'class'=>'required' ) ). 
                         //           __('privacy-agreement', array( ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') ))); 
                      */?>
					  <?php
					echo '<p class="agree">';
                    echo Form::checkbox('privacy_agreement', null , false ,  array( 'class'=>'required' ) );
                    echo __('privacy-agreement',array(
                                ':privacy' => HTML::anchor( Page::get('completedNavigationUri:kebijakan-privasi') , Page::get('simpleBreadCrumb:kebijakan-privasi') )
                             ));
                    echo '</p>';
					  ?>
                      </label></td>
                    </tr>
                <tr>
                      <td><table width="100%" border="0" cellpadding="0">
                        <tbody><tr>
                          <td width="50%"><input name="captcha" type="text" class="captcha-text" maxlength="4" /></td>
                          <td width="50%"><?php 
                          
                          $captcha = Mifx::captcha(200,70,true);
                          echo $captcha;
                          echo Form::hidden('ucr',Session::instance()->get('captcha'))
                          ?></td>
                        </tr>
                      </tbody></table></td>
                    </tr>
                <tr>
                      <td align="left">
                        <input name="button" type="submit" class="btn_submit" id="button" value="<?php echo __('submit'); ?>" />
                        
                      </td>
                    </tr>
            </tbody>
          </table>
        </form>
<script type="text/javascript">
 $('form.form-belajar-trading').validate({
                rules            : {
                    accountEmail  : {
                        required : true,
                        email: true
                    }
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { },
                submitHandler  : function(form) {
                     
                     nous_process_message_new();
                setTimeout(function(){
                     serialized = $(form).serialize();
                            $.ajax({
                                url     : form.action,
                                type    : 'POST',
                                data    : serialized,
                                async   : false,
                                success : function(res) {
                                    console.log('function: after ajax send and success');
                                    console.log('data:');
                                    str = $.trim(res);
                                    console.log(res);
                                    
                                    ga('send', 'event', { eventCategory: 'mte', eventAction: 'button'});
                                    
                                    nous_disable_message();
                                    jAlert(str);
                                    return false;
                                    
                                    
                                    if (str === 'success') {                                    
                                        if (frmID == 'frm-demo-account')
                                            window.location.href = '<?php echo $accountUri; ?>/create-free-demo-account-success<?php echo $qSource;?>';
                                        else
                                            window.location.href = '<?php echo $accountUri; ?>/create-monex-live-account-success<?php echo $qSource;?>';
                                        
                                        return;
                                    } else {
                                        if (str.substr(0,5) == 'goTo:') {
                                            newUrl = str.substr(5);
                                            window.location.href = newUrl;
                                        } else {                                     
                                            nous_disable_message();
                                            
                                              // check captcha
                                            if(res=="<ul class=\"error\"><li>Captcha masih salah. Silahkan masukkan sekali lagi</li></ul>" || res=="<ul class=\"error\"><li>Captcha did not match. Please correct</li></ul>"){
                                               
                                                if(captchaCount==3){
                                                    var action_captcha = $('.verifikasi img').attr('src');
                                                    var img = $("<img />").attr('src', action_captcha)
                                                                            .load(function() {
                                                                                if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                                                                                    alert('broken image, reload page!');
                                                                                } else {
                                                                                    $(".verifikasi img").remove();
                                                                                    $(".verifikasi").append(img);
                                                                                }
                                                                            });
                                                    captchaCount = 0;
                                                }
                                                
                                                captchaCount++;
                                            }

                                            jAlert(res);
                                        }
                                    }
                                } ,
                                error   : function() {                                    
                                    nous_disable_message();
                                    
                                    jAlert('<?php echo __('server-not-responding');?>');
                                }                          
                            });
                            
                        }, 180);
                     
                    }
        
    });
</script>
<div style="display: none;">
    <div id="flash"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" height="600" id="mte_swf" width="600"><param name="wmode" value="opaque" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="//www.mte-media.com/players/mte_player10.swf?userRef=c8f3888&amp;language=indonesian&amp;product=stg&amp;open_lessons='1,2,3,4,5,6,7,8'&amp;contact_message=Please register.&amp;demo_mode=false&amp;lgl=undefined&amp;jumpToLesson=1&amp;backUrl=close_popup" /><embed allowfullscreen="true" allowscriptaccess="always" height="600" id="mte_swf" src="//www.mte-media.com/players/mte_player10.swf?userRef=c8f3888&amp;language=indonesian&amp;product=stg&amp;open_lessons='1,2,3,4,5,6,7,8'&amp;contact_message=Please register.&amp;demo_mode=false&amp;lgl=undefined&amp;jumpToLesson=1&amp;backUrl=close_popup" type="application/x-shockwave-flash" width="600" wmode="opaque"></embed></object></div>
</div>
<?php }

 if (!empty($banners)) {
      echo '<div class="thumb_hi">',
            '<ul>';
      $no=1; 
      foreach ($banners as $banner) {      
          
          $detail = json_decode($banner->detail);
          
          if (!empty($banner->folder))
          {          
              echo '<li>',
                      '<div class="img">',
                          HTML::image( URL::mediaImage( $banner->folder . '/' . $banner->file , null ) ),
                      '</div>';
                   
                        if(Language::$current == 'cn' || Language::$current == 'tw'){
                            $nameBannerTitle2 = 'banner_title2_'.Language::$current;

                            $bannerTitle2 =  !empty($detail->$nameBannerTitle2) ? $detail->$nameBannerTitle2 : $detail->banner_title2;
                
                        }else $bannerTitle2 = $detail->banner_title2; 
                          

                        echo $bannerTitle2;


                        if(Language::$current == 'cn' || Language::$current == 'tw'){ 
                            $nameBannerTitle1 = 'banner_title1_'.Language::$current;

                            $bannerTitle1 = !empty($detail->$nameBannerTitle1) ? $detail->$nameBannerTitle1 : $detail->banner_title1;
                        }else{ $bannerTitle1 = $detail->banner_title1; }

                       
                      echo '<p>',HTML::anchor( 
                              empty($detail->banner_permalink) ? '#' : $detail->banner_permalink , 
                              $bannerTitle1  ),'</p>',
                       
                       HTML::anchor( 
                          empty($detail->banner_permalink) ? '#' : $detail->banner_permalink , 
                          HTML::image( URL::templateImage('material/btn_detail.jpg') ) ,
                          array('class'=>'btn','style'=>empty($detail->banner_title2) ? 'bottom:10px;' : 'bottom:-10px;') ),
                   '</li>';   
                   
                   if($no==2) echo '<div style="clear: both; height: 15px;"></div>'; 
                   $no++;     
            
          }                             
      }
      echo '</ul>',
           '</div>';
  }
?>

<script type="text/javascript">
	$('#demo-Phone').numeric({
		allow : '-'
	}); 	
    $('#submit_verify').validate({
                rules            : {
                    accountEmail  : {
                        required : true,
                        email: true
                    }
                },                
                invalidHandler : function(form, validator) { },     
                errorPlacement : function(error, element) { },
                submitHandler  : function(form) {
                     
                     nous_process_message_new();
                     
                     serialized = $(form).serialize();
                     
                     $.ajax({
                            url     : '<?php echo URL::front('ajax/verified_email') ?>',
                            type    : 'POST',
                            data    : serialized,
                            async   : false,
                            success : function(res) {
                                
                                jAlert(res,'success');
                                
                                form.accountEmail.value = "";
                                nous_disable_message();
                                
                                return false;
                            }
                     });
                     
                    }
        
    });
</script>