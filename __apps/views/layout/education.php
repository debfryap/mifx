<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Layout View
 * @Module      ~
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ ?>

<section class="middle_page">
    <div class="container">
        <?php 
            if (!empty($widget)) {
                echo '<div class="wid_edu_wrap">';
                
                foreach (Page::get('navigationObject') as $n) {
                    if ($n->permalink != Page::get('navigationRoot')) continue;
                    
                    if (!empty($n->child)) {
                        foreach ($n->child as $key => $n) {

                          if($key > 4) continue;
                          
                            $n    = Translate::item('site_navigations',$n);
                            
                            $icon = Model::factory('Medialibrary')->simple_application('widget-icon',$n->id,'site_navigations')->current();
                            
                            $attrs['style'] = null;
                            
                            echo '<div class="edu_wid">';
                            
                            $img = '<img src="'. URL::mediaImage($icon->folder . '/' . $icon->file, null) .'">';
                            
                            $link = Page::get('completedNavigationUri:'.$n->permalink);
                            
                            if (!empty($n->child)) {
                                foreach ($n->child as $m) {
                                    $link = Page::get('completedNavigationUri:'.$m->permalink); break;
                                }                                    
                            }
                            echo HTML::anchor($link, ($n->judul_icon ? $img.'<span>'.$n->label.'</span>' : $img.'<span>'.$n->judul_icon.'</span>'), $attrs);
                            echo '</div>';
                        }
                    }
                    break;    
                }
                echo '</div>';
            } ?>
            
		<div class="left_wrap">
        <div class="scroll_mobile">
            <?php
                if (!empty($showNavigation)) {
                    $widget = Widget::load('page/side-navigation');
                    
                    if (is_string($showNavigation))
                        $widget->set('active',$showNavigation);
                        
                    echo $widget->render();
                }
                
               // if (!empty($live)) echo View::factory('general/frm-account')->set('section', 'edukasi'); 
                    echo '
                        <div class="row-mte">
                            <ul class="parent-button-new">
                                <li>
                                    <a href="'. URL::front('open-live-account.php').'" class="button_acc-new">
                                        <span class="inside-ban">
                                        <img src="'. URL::front('media/images/widget-accounts/btn-live.jpg') .'" width="59">
                                        <span class="ban-content">
                                        '.__('Buat').'<br>'.__('Live Akun').'<span> Monex </span></span></span></a>
								</li>
                                <li>
                                            <a href="'. URL::front('open-demo-account.php') .'" class="button_acc-new">
                                            <span class="inside-ban"><img src="'. URL::front('media/images/widget-accounts/btn-demo.jpg') .'" width="59">
                                            <span class="ban-content">
                                                '.__('Buat').'<br><span></span>'.__('Demo Akun').'<span> Monex </span></span></span></a>
                                </li>
                            </ul>
                        </div><div class="side_menu_mobile"></div>';
                if (!empty($leftBanners) && $leftBanners->valid()) {
                    foreach ($leftBanners as $b) {
                        /**
                        $detail = $link = $title = '';
                        if (!empty($b->detail)) {
                            $detail = json_decode($b->detail);
                            $link   = !empty($detail->banner_permalink) ? $detail->banner_permalink : '#';
                            $title  = $detail->banner_title;    
                        }
                        **/
                        
                        $detail = $link = $title = '';
                        $attrs  = array( 'class' => 'btn' );
                        if (!empty($b->detail)) {
                            $detail = json_decode($b->detail);
                            $link   = "#";
                            if (!empty($detail->banner_permalink)) {
                                if (strpos($detail->banner_permalink,'http') === 0) {
                                    $link = $detail->banner_permalink;
                                    $attrs['target'] = '_blank';
                                } else {
                                    $link = Page::get("completedNavigationUri:$detail->banner_permalink");
                                    if(empty($link)) {
                                        $link = URL::front($detail->banner_permalink);
                                    }    
                                }
                            }
                            
                            $title  = $detail->banner_title;    
                        }
                        
                        echo '<div class="platform">',
                                HTML::image( URL::mediaImage( $b->folder . '/' . $b->file , null ) , array( 'alt' => '' ) ),
                                '<p>',(preg_replace('/(monex)/i','<span>$1</span>',$title)),'</p>', 
                                '<p>',HTML::anchor( $link ,  __('btn-read-more') , $attrs),'</p>', 
                             '</div>';
                    }    
                } ?>
                </div>
		</div>
		
		<div class="right_wrap">
			<div class="breadcrumb">
			    <?php  echo empty($breadcrumb) ? Page::breadCrumb() : $breadcrumb; ?>	
			</div>
			<?php if (isset($content)) echo $content; ?>
		</div>        
	</div>
    <div class="clear"></div>
    
    <?php echo isset($footer) ? $footer : ''; ?>
    
</section>
