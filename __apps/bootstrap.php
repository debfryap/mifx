<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

                                    /** :D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D **/
/** :D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D **/  define('CMSPAGE','admin-cp');  /** :D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D **/
                                    /** :D:D:D:D:D:D:D:D:D:D:D:D:D:D:D:D **/

// Load the core Kohana class
require SYSPATH.'classes/Kohana/Core'.EXT;

if (is_file(APPPATH.'classes/Kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/Kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/Kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('Asia/Jakarta');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
	'base_url'   => '/mifx/',
    'index_file' => FALSE,
    'errors'      => FALSE
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
        /** Minimum Requirement Module **/
        'kernel'        => MODPATH.'webarq/kernel',
        'themes'        => MODPATH.'webarq/themes',
        'image'         => MODPATH.'image',        
        'database'      => MODPATH.'database',        
        'pagination'    => MODPATH.'pagination',
        'email'         => MODPATH .'email',
        
        'station'       => MODPATH.'webarq/station',
        //'captcha'       => MODPATH.'webarq/captcha',
        'user'          => MODPATH.'webarq/user',
        'page'          => MODPATH.'webarq/page',
        'language'      => MODPATH.'webarq/language',
        'contact'       => MODPATH.'webarq/contact',
        'news'          => MODPATH.'webarq/news',
        //'faq'           => MODPATH.'webarq/faq',
        
        
        /** Last module, no matter what as it config depend on it **/
        'medialibrary'  => MODPATH.'webarq/medialibrary',
	));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 * 
 * Recommended you copy all your init route in here
 */

$filter_lang = array('lang' => '([a-zA-Z]{2}|[a-zA-Z]{2}-[a-zA-Z]{2})'); 
 
//Error routing
Route::set('error', '(<lang>/)error(/<controller>)', array_merge($filter_lang,array('controller'=>'cms|site')) )
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'error',
		'controller' => 'cms',
		'action'     => 'index',
	));

//CMS routing
Route::set('cms', '(<lang>/)'.CMSPAGE.'(/<package>(/<controller>(/<action>(/<param1>(/<param2>(/<param3>(/<param4>)))))))',$filter_lang)
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'dashboard',
        'directory'  => 'cms',
		'controller' => 'manage', 
		'action'     => 'index',
	)); 
    
//Sitemap Member
Route::set('sitemap', '(<lang>/)sitemap', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'about-us',
        'directory'  => 'site',
		'controller' => 'welcome',
        'action'     => 'index',
        'section1'   => 'sitemap'
	));
    
//Ninestars Member
Route::set('nine-stars', '(<lang>/)nine-stars/<group>', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home'
	));
Route::set('nine-stars-sinarmas', '(<lang>/)sinarmas', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home',
        'group'      => 'demosinarmas'
	));
Route::set('nine-stars-ninestars', '(<lang>/)ninestars', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home',
        'group'      => 'demoninestars'
	));
Route::set('nine-stars-imf', '(<lang>/)imf', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home',
        'group'      => 'demoimf'
	));
Route::set('nine-stars-atpf', '(<lang>/)atpf', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home',
        'group'      => 'demoATPF'
	));
Route::set('nine-stars-kontes', '(<lang>/)kontes', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home',
        'group'      => 'kontes'
	));
Route::set('nine-stars-kontes2', '(<lang>/)kontes2', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'ninestars',
        'action'     => 'home',
        'group'      => 'kontes2'
	));

//Register Demo Account
Route::set('register-demo-account', '(<lang>/)open-demo-account.php(/<section2>(/<section3>))', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'welcome',
        'section1'   => 'open-demo-account.php',
        'action'     => 'index'
	));
Route::set('register-demo-account-no-php', '(<lang>/)open-demo-account(/<section2>(/<section3>))', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'welcome',
        'section1'   => 'open-demo-account.php',
        'action'     => 'index'
	));

//Register Live Account
Route::set('register-live-account', '(<lang>/)open-live-account.php(/<section2>)', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'welcome',
        'section1'   => 'open-live-account.php',
        'action'     => 'index'
	)); 
Route::set('register-live-account-no-php', '(<lang>/)open-live-account(/<section2>)', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'welcome',
        'section1'   => 'open-live-account.php',
        'action'     => 'index'
	)); 
    
 
//Verify Account
Route::set('verify-account', '(<lang>/)accounts/<action>/<token>', array_merge($filter_lang,array('action'=>'verify-live-account|verify-demo-account|verify-ebook|verify-mte'))) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'accounts',
        'directory'  => 'site',
		'controller' => 'verify'
	));
    
 
//Ajax
Route::set('ajax', '(<lang>/)ajax/<action>(/<param1>(/<param2>(/<param3>(/<param4>))))', $filter_lang) 
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'ajax',
		'controller' => 'site',
	));
 

//Tutorial
Route::set('tutorial', '(<lang>/)<before>/<section>(/<item>)',array_merge($filter_lang,array('before'=>'.+','item'=>'.+','section'=>'tutorial|investment-clinic')))
	       ->defaults(array(
                'package'     => 'education',
        		'controller'  => 'learning-center',
                'directory'   => 'site',
        		'action'      => 'index',
                'section'     => 'tutorial',
                'lang'        => Language::$default
        	 ));
             
//Predefined Navigation Collector
$predifined_package = 'home|about-us|trading-products|online-trading|accounts|education|partnership|contact-us|search';
$sub_task_filter    = array_merge($filter_lang,array('package'=>$predifined_package,'controller'=>'kegiatan-perusahaan|learning-center'));
Route::set('sub-task', '(<lang>)(/)(<package>/(<controller>(/<section>(/<item>(/<filter>)))))',$sub_task_filter)
	       ->defaults(array(
                'package'     => 'home',
        		'controller'  => 'welcome',
                'directory'   => 'site',
        		'action'      => 'index',
                'lang'        => Language::$default
        	 ));
                          
Route::set('basic-route', '(<lang>)(/)(<package>(/<section1>(/<section2>)))',array_merge($filter_lang,array('package'=>$predifined_package)) )
	       ->defaults(array(
                'package'     => 'home',
        		'controller'  => 'welcome',
                'directory'   => 'site',
        		'action'      => 'index',
                'lang'        => Language::$default
        	 ));
 
// 101
Route::set('101', '(<lang>/)101(/)(<section1>)', $filter_lang )
           ->defaults(array(
                'package'     => 'education', 
                'controller'  => 'welcome',
                'directory'   => 'site',
                'action'      => '101',
                'lang'        => Language::$default
             ));

// mte
Route::set('mte', '(<lang>/)mte(-)(<section1>)', $filter_lang )
           ->defaults(array(
                'package'     => 'education', 
                'controller'  => 'welcome',
                'directory'   => 'site',
                'action'      => 'mte',
                'lang'        => Language::$default
             ));      
           
 // Errors
Route::set('errors', '(<lang>/)errors(/)(/<section1>(/<section2>))', $filter_lang )
           ->defaults(array(
                'package'     => 'default',
                'controller'  => 'errors',
                'directory'   => 'template',
                'action'      => 'index',
                'lang'        => Language::$default
             ));

//Default routing   
Route::set('default', '(<lang>/)(<controller>(/<action>(/<id>)))',array( 'lang' => '([a-zA-Z]{2}|[a-zA-Z]{2}-[a-zA-Z]{2})') )
	       ->defaults(array(
        		'controller'  => 'welcome',
        		'action'      => 'index',
                'lang'        => Language::$default
        	 ));

/** Test Given URI **/ /** 
if (isset($_GET['debug-route']) && $_GET['debug-route'] == 'me') {
    $uri = 'sitemap'; 
    foreach (Route::all() as $r)
    {
      $request = Request::factory($uri);
      echo Debug::vars($r->matches($request));
    }
    exit;
}

/** **/
