<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'login'                 => 'Login',
    'logout'                => 'Logout',    
    'login_area'            => 'Login Area',
    'user'                  => 'User',
    'username'              => 'Username',
    'role'                  => 'Role',
    'email'                 => 'Email',
    'profile'               => 'Profile',
    'edit-profile'          => 'Edit Profile',
    'view-profile'          => 'View Profile',
    'manage-user'           => 'User Administration',
    'manage-role'           => 'Role Administration',
    'username'              => 'Username',
    'password'              => 'Password',
    're-password'           => 'Retype Password',
    'first-name'            => 'First Name',
    'last-name'             => 'Last Name',
    'middle-name'           => 'Middle Name',
    'name'                  => 'Full Name',
    'sex'                   => 'Sex',
    'male'                  => 'Male',
    'female'                => 'Female',
    'address'               => 'Address',
    'phone'                 => 'Phone',
    'mobile'                => 'Mobile Phone',
    'ip_addr'               => 'IP Address',
    'activation_key'        => 'Activation Key',
    'last_login'            => 'Your last login at :time',
    'last_session'          => 'Your last session :session',
    'forgot_password'       => 'Forgot your password?',
    'is-admin'              => 'Is Admin',
    'is_admin'              => 'Is Admin',
    'role'                  => 'Role',
    'naming-role'           => '":name" Roles',
    'view-user'             => 'View User',
    'create-user'           => 'Add New User',
    'edit-user'             => 'Edit User',
    'publish-user'          => 'Publish/Unpublish User',
    'delete-user'           => 'Delete User',
    'view-role'             => 'View Role',
    'create-role'           => 'Add New Role',
    'edit-role'             => 'Edit Role',
    'publish-role'          => 'Publish/Unpublish Role',
    'delete-role'           => 'Delete Role',
    'view-history'          => 'View History',
    'forgot-password'       => 'Forgot password ?',
    'general-info'          => 'General Information',
    'account-info'          => 'Account Information',
    'activity'              => 'Activity',
    'recent-activity'       => 'Recent Activity',
    'avatar_file_name'      => 'Photo',
    'old-password'          => 'Old Password',
    'actor'                 => 'Actor',
    'permission'            => 'Permissions',
    'edit-permission'       => 'Set Permission',
    'total-user'            => 'Total User',
    'view-detail-user'      => 'View User Detail',
    'detail-user'           => '":user" detail',
    
    'msg_welcome_user'      => 'Welcome <span class="username">:username</span>',
    'msg_error_login'       => 'Error. Please check your username or password',
    'msg_error_captcha'     => 'Error. You entered wrong security code. Please check and try again',
    'msg_error_same_pwd'    => 'Error. Please input same value to the password again',
    'msg_fill_old_password' => 'Error. You must fill in old password to change your current password',
    'forbidden-self-permission-assignment' => 'Self role permission assignment forbidden.',
    
    'history_users'         => 'account',
    'history_user_details'  => 'details',
);