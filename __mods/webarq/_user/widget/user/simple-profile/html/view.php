<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

if (empty($current)) { echo __('error.no-data'); return; }

$photo = !empty($current->avatar_file_name) && file_exists(URL::mediaImage(User::$config->images->medium->path.'/'.$current->avatar_file_name,true))
            ? URL::mediaImage(User::$config->images->medium->path.'/'.$current->avatar_file_name)
            : false; 
            
?>

<div id="widget_user_simple_profile">
    <?php if (!empty($photo)) { ?>
    <!-- Photo Box -->
    <div class="fl" style="width:120px;height:145px;border:1px  dotted #ccc;padding:5px;margin-right:20px;">
        <img src="<?php echo $photo;?>" style="width: 120px;"/>
    </div>    
    <!-- End Photo Box -->  
    <?php } ?>
    <div class="fl">
        <?php 
            if (User::authorise('user','edit-profile')) {
                echo '<a href="',URL::cms(),'user/profile/edit/id/',$current->id,'">Edit Profile</a>';
                echo '<div class="break5"></div>';
            }
        ?>        
        
        <h3 class="sub-title"><?php echo __('account-info');?></h3>
        
        <div class="fl" style="width: 100px;"><?php echo __('email'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->email;?></span></div>
        <div class="clear_left break3">&nbsp;</div>
        
        <div class="fl" style="width: 100px;"><?php echo __('username'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->username;?></span></div>
        <div class="clear_left break3">&nbsp;</div>
        
        <div class="fl" style="width: 100px;"><?php echo __('role'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->role_label;?></span></div>
        <div class="clear_left break3">&nbsp;</div>
        
        <div class="fl" style="width: 100px;"><?php echo __('ip_addr'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->ip_addr;?></div>
        <div class="clear_left break3">&nbsp;</div>
        
        <div class="break15"></div>
        
        <h3 class="sub-title"><?php echo __('general-info');?></h3>
        
        <div class="fl" style="width: 100px;"><?php echo __('name'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->full_name;?></div>
        <div class="clear_left break3">&nbsp;</div>
        
        <div class="fl" style="width: 100px;"><?php echo __('sex'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo __($current->sex);?></div>
        <div class="clear_left break3"></div>
        
        <div class="fl" style="width: 100px;"><?php echo __('address'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->address;?></div>
        <div class="clear_left break3"></div>
        
        <div class="fl" style="width: 100px;"><?php echo __('phone'); ?></div>
        <div class="fl" style="width: 20px;">:</div>
        <div class="fl"><span><?php echo $current->phone;?></div>
        <div class="clear_left break3"></div>
        
        <div class="break15"></div>
        
        <h3 class="sub-title"><?php echo __('recent-activity');?></h3>
        <?php 
            if (!empty($activities)) {
                echo '<ul id="recent-activity">';
                foreach ($activities as $activity) {
                    echo '<li>';
                    echo History::detail_activity($activity,false);
                    echo '</li>';    
                }
                echo '</ul>';
            } 
        ?>
    </div>          
    <div class="clear_left"></div>
</div>