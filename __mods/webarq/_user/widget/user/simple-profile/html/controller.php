<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_User_SimpleProfile extends Populate {
    
    protected $id;    
    
    private $current;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        if (isset($this->id)) {
            $this->current = Model::factory('User')->id_scan($this->id);
        } else {
            $this->current = User::admin();
        }
    }
    
    public function data() {    
        
        return array (
                'current' => $this->current,
                'activities' => User::activities(!empty($this->current->username) ? $this->current->username : NULL,10)
            );
    } 
}

?>