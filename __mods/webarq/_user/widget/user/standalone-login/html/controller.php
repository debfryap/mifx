<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_User_StandaloneLogin extends Populate {
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
    }
    
    public function data() {
        //Do nothing
    } 
}

?>