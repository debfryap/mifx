<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

?>

<div id="wg-user-admin-webarq-change-password" class="<?php echo !isset($class) ? 'normal' : $class; echo User::$config->captcha ? ' with-captcha' : ''?>">
    <div class="wg-header header-left"><div class="wg-header header-right"><div class="wg-header header-center">
        <div id="inner-header-right">
            <div class="logo-client"></div>
            <div class="clear"></div>
        </div>
    </div></div></div>  
    
    <div class="wg-content">
        <div class="wording">
        
            <?php 
                if (!isset($user_id) || $user_id === false) {
                    echo __('error-token');    
                } else { ?>
                
            <form id="frm-admin-webarq-change-password" 
                action="<?php echo URL::front('user/token/request-new-password');?>" 
                method="POST">
                <div>New Password</div>
                <div class="input">
                    <input type="password" name="password" id="password" class="password"/>
                </div>
                <div class="break15"></div>
                
                <div>Retype New Password</div>
                <div class="input">
                    <input type="password" name="re_password" id="re_password" class="re_password"/>
                </div>           
                <div class="break15"></div>
                
                <div style="border-top: 1px solid #eaeaea;padding-top:15px;height:32px;">
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>"/>
                    <input type="submit" class="request-password" value="Submit"/>
                    <div class="clear break1"></div>
                </div>
            </form>
            
            <?php } ?>
        </div>
    </div> 
    
    <div class="wg-footer"><div class="wording">WEBARQ Content Management System</div></div>
    <div class="break10"></div>
    <div class="logo-webarq">
        Copyright &copy; 2012
    </div>
    </div>
</div>

<script>
    center_middle('#wg-user-admin-webarq-change-password');
    
    $(document).ready(function(){
        
        $('#frm-admin-webarq-change-password').validate({
            errorPlacement   : function(error,element) {
                $('<div/>',{"class":"error-message"}).html(error).insertAfter(element);
            },
            rules            : {
                password     : {
                    required : true,
                    minlength:6
                },
                re_password  : {
                    required : true,
                    minlength:6,
                    equalTo  : '#password'
                }
            },
            submitHandler    : function(form) {
                
                nous_process_message('Please wait for a while ... ',true);
                
                setTimeout(function(){
                    
                    $.ajax({
                        url     : bs_site+'user/token/update-password',
                        async   : false,
                        type    : 'POST',
                        data    : {"token":"<?php echo $token;?>","password":$('#password').val(),"user_id":$('#user_id').val()},
                        success : function(response) {
                            nous_disable_message();
                            
                            $('#frm-admin-webarq-change-password').hide();      
                            
                            $('.wg-content').children('.wording').html(response);  
                        }
                    });
                       
                },10);     
            }
        });
    });
</script>