<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Edit Profile ~ CMS
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    $id    = Request::$initial->param('param2');
    $job   = strtolower(Request::$initial->param('param1'));
    
    echo Data::form(array(
        'title'     => $title,
        'id'        => 'form_update_profile',
        'multilang' => false,  
        'pack'      => array ('user:'.Request::$initial->param('param1').'-user'),
        'row_id'    => array('id',$id),
        'values'    => $job === 'create' ? false : 'auto',
        'override'  => array(
            'password' => ''
        ),
        'elements'  => array (
            //Site configuration  
            array (
                'label'     => __('re-password'),
                'name'      => 're_password',
                'id'        => 're_password',
                'class'     => 're_password',
                'type'      => 'password',
                'order'     => 'after password'
            ),
        )
    ));
?>

<script type="text/javascript">
    $(document).ready(function(){
        uploadify_maker($("#uploadify"),{
            width : 128,
            buttonText : "Upload Photo",
            uploader : bs_cms+"helper/notemplate/upload-photo-profile",
            fileObjName : 'photo_profile',
            fileTypeExts : '*gif;*.jpg;*.jpeg;*.png',
            'onUploadSuccess' : function(file, data, response) {
                if (response === true) {
                    chk_input = $('#avatar_file_name');
                    chk_val   = $.trim(data);
                    if (chk_input.length <= 0 ) {
                        $('<input/>',{
                            type  : 'hidden',
                            value : chk_val,
                            name  : 'avatar_file_name',
                            id    : 'avatar_file_name'
                        }).appendTo($('.block_avatar_file_name'));
                    } else {
                        chk_input.val(chk_val);
                    }    
                }
            }
        });
        
        jQuery.extend(jQuery.validator.messages, {
            equalTo : '<?php echo __('msg_error_same_pwd');?>'
        });
        
        $('#password,#username').rules("add", {
            minlength : 6
        }); 
        
        $('#re_password').rules("add", {
            minlength : 6,
            equalTo: "#password"
        }); 
    });
</script>
