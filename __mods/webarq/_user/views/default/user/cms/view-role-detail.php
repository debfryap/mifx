<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View Role ~ CMS
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

if (empty($role)) {
    echo __('error.no-data');
    return;
} 

?>

<h3 class="title-page"><?php echo __('naming-role',array(':name'=>$role->label));?></h3>
<div class="fl" style="width: 100px;"><?php echo __('total-user');?></div>
<div class="fl" style="width: 15px;">:</div>
<div class="fl"><?php echo $role->user;?></div>
<div class="line-break clear">&nbsp;</div>

<div class="fl" style="width: 100px;"><?php echo __('create_time');?></div>
<div class="fl" style="width: 15px;">:</div>
<div class="fl"><?php echo $role->create_time;?></div>
<div class="line-break clear">&nbsp;</div>

<?php 

    if (!empty($permissions)) {
        $active_module = '';
        
        echo '<div class="break20"></div>',
             '<h3 class="title-page">',__('permission'),'</h3>',
             '<span>Check/uncheck the checkbox under status column, to change role permission</span>',
             '<div class="break15"></div>',        
             '<div class="tabular">',
                //Header             
                '<div class="header">',        
                    '<div class="cell">',
                        __('permission'),
                    '</div>',
                    '<div class="cell" style="width:250px">',
                        __('status'),
                    '</div>',
                '</div>',
                
                //Row                             
                '<div class="row-group">';                
                foreach ($permissions as $permission) {
                    if ($active_module != $permission->module) {
                        $active_module = $permission->module;
                        echo '<div class="row" style="background-color:#ddd;">',
                                '<div class="cell" style="font-weight:bold;color:#000;">',__($active_module),'</div>',
                                '<div class="cell no-border-left"></div>',
                             '</div>';                        
                    }            
                    
                    $class_row = $permission->checked == 1 ? ' green' : ' pink';
                    $translate_permission = __($permission->permission);
                    echo '<div class="row',$class_row,'">',
                            '<div class="cell">',$translate_permission,'</div>',
                            '<div class="cell center">',
                                '<input type="checkbox" 
                                        class="ajax-permission-resubmission" 
                                        data-permission-id="',$permission->id,'"  
                                        data-permission-role-name="',$role->label,'"  
                                        data-permission-label="',$translate_permission,'"';
                                if (!empty($permission->checked)) echo ' checked="checked"'; 
                                echo '/>',
                            '</div>',
                         '</div>';                       
                }        
            
            echo '</div>';
        echo '</div>';                      //Tabular close
    }
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.ajax-permission-resubmission').click(function(){
            apr = $(this);
            is_checked = apr.is(':checked');
            next_apr_class = is_checked ? 'green' : 'pink';
            
            $.ajax({
                url     : bs_cms+'user/ajax-role/role-setting',
                type    : 'POST',
                async   : false,
                data    : {
                    value      : is_checked ? 1 : 0,
                    role_id    : <?php echo $role->id;?>,
                    role_name  : apr.attr('data-permission-role-name'),
                    pid        : apr.attr('data-permission-id'),
                    permission : apr.attr('data-permission-label'),
                },
                success : function(response) {
                    if ($.trim(response) == 'true') {
                        apr.parents('.row').removeClass('green pink').addClass(next_apr_class);
                    } else {
                        jAlert('<?php echo __('error.transaction');?>','warning');
                    }     
                } 
            });            
        });    
    });
</script>