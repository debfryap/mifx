<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    User activity ~ CMS
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


?>
<div class="tabular">
    <div class="header">
        <div class="cell" style="width: 25%;"><?php echo __('username');?></div>
        <div class="cell" style="width: 60%;"><?php echo __('activity');?></div>
        <div class="cell" style="width: 25%;"><?php echo __('date');?></div>
    </div>
    <div class="row-group">
        <div class="row">
            <div class="cell">Mr. Pinokio</div>
            <div class="cell">City Park Dozen Cript</div>
            <div class="cell">City Park Dozen Cript</div>
        </div>
        <div class="row red">
            <div class="cell publish">Mr. Pinokio</div>
            <div class="cell">City Park Dozen Cript</div>
            <div class="cell">City Park Dozen Cript</div>
        </div>
    </div>
</div>