<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_User_Cms_ManageUser extends Controller_Default_Template_Cms {
        
    public function before() {
        parent::before();
        
        $this->meta('title',__('manage-user'));
        
        App::$config->menu->active = 'manage-user';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('user',array('create-user','edit-user','publish-user','view-user','delete-user'));
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing
        $listing = Data::listing('user');
        
        //Listing header
        $listing->set_header('username',array('label'=>__('Username'),'width'=>300))     
                ->set_header('email')
                ->set_header('role_label',array('label'=>__('role')))
                ->set_header('full_name',array('label'=>__('name')))
                ;
                        
        //Listing search default
        $listing->tool('search',array('username','email','r.label'=>'role_label'));
        
        //Listing button
        if (User::authorise('user','create-user')) {
            $listing->create_action(__('create-user'));
        }
        
        if (User::authorise('user','edit-user')) {
            $listing->edit_action(array(
                'exception' => array('role_id'=>1,'is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        } 
        //Another ways for listing button       
        $listing->action(array(
                "delete" => array(
                    'exception' => array('is_system'=>1,'id'=>1,'i_absolute'=>true),
                    "authorise" => array("user"=>"delete-user"),
                    'ajax'      => 'ajax',                
                    'href'      => 'delete/{id}/{username}'
                ),
                "status" => array(
                    "exception" => array('is_system'=>1,'id'=>1,'i_absolute'=>true),
                    "authorise" => array("user"=>"publish-user"),
                    'ajax'      => 'ajax',
                    'href'      => '{function}/{id}/{username}'
                ),  
                "view"   => array(
                    "authorise" => array("user"=>"view-user"),
                    "href"      => 'view/{id}/{username}'
                ),                     
            ));
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-user')) 
            ->register('content')->use_string($listing->render())
                ;   
    }
    
    public function action_view() {
            
        $this->register('title',__('view-detail-user'));
        
        if (User::authorise('user','view-user')) {
            
            $this->media_tabular();
                                    
            $this->register('content')
                    ->use_widget('user/simple-profile')
                        ->set('id',$this->param1);
        } else {            
            $this
                ->register('content')
                    ->use_string(__('error.unauthorized'));
        }                     
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Uploadify
        $this->media_uploadify();
        
        //Check for authorisation
        $permission = $this->param1."-user";
        $this->authorise('user',$permission);
        
        //Support variable        
        $title = __($permission);        
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('user/cms/frm-user')
                    ->set('title',$title)
                ;   
    }
    
    public function action_save() {
        //Open builder
        $builder = Data::create();
        
        
        //Avatar profile
        if (!empty($this->post->avatar_file_name)) {
            $count    = count(json_decode(json_encode(User::$config->images),true));   
            foreach (User::$config->images as $k => $conf) {
                $w = $conf->size[0];
                $h = $conf->size[1];
                $i = empty($i) ? 1 : $i+1;
                
                $image = Picture::resize(array(
                    'src'    => 'media/temp/'.$this->post->avatar_file_name,
                    'dest'   => URL::mediaImage($conf->path,true),
                    'name'   => $this->post->username,
                    'width'  => $w,
                    'height' => $h,
                    'delete_src' => $i >= $count ? true : false
                )); 
            }
            
            $va = isset($image) ? Helper_File::type($image,'/') : $this->post->avatar_file_name;     
            $builder->post('avatar_file_name',$va);
        }
        
        //Password overwrite
        $builder->post('password',User::make_password($this->post->password));
        
        //Execution
        $do_save = $builder->save();
        
        if (!empty($do_save['users'])) {
            //Set transaction message
            Data::transaction_message(isset($this->post->app_transaction_pack) ? $this->post->app_transaction_pack : null);
            
            //Redirect
            $this->deflect('user/manage-user/form/edit/'.$do_save['users']);
        }
    }
    
    public function action_update() {
        //Open builder
        $builder = Data::update();
        
        //Password overwrite
        if (!empty($this->post->password)) {
            $builder->post('password',User::make_password($this->post->password));
        } else {
            $builder->post('password',false);
        }
        
        
        //Avatar profile
        if (!empty($this->post->avatar_file_name)) {
            $count    = count(json_decode(json_encode(User::$config->images),true));   
            foreach (User::$config->images as $k => $conf) {
                $w = $conf->size[0];
                $h = $conf->size[1];
                $i = empty($i) ? 1 : $i+1;
                
                $image = Picture::resize(array(
                    'src'    => 'media/temp/'.$this->post->avatar_file_name,
                    'dest'   => URL::mediaImage($conf->path,true),
                    'name'   => $this->post->username,
                    'width'  => $w,
                    'height' => $h,
                    'delete_src' => $i >= $count ? true : false
                )); 
            }
            
            $va = isset($image) ? Helper_File::type($image,'/') : $this->post->avatar_file_name;     
            $builder->post('avatar_file_name',$va);
        }
        
        //Execution
        $do_update = $builder->save();
        
        $id = $this->param1;
        
        if (!empty($do_update)) {
            //Set transaction message
            Data::transaction_message(isset($this->post->app_transaction_pack) ? $this->post->app_transaction_pack : null);
        }
         
        //Redirect
        $this->deflect('user/manage-user/form/edit/'.$id); 
    }
}