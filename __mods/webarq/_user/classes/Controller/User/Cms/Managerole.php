<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_User_Cms_ManageRole extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('manage-role'));
        
        App::$config->menu->active = 'manage-role';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('user',array('create-role','edit-role','delete-role','publish-role','view-role'));
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing using model
        $listing = Data::listing('role');
        
        //Listing header
        $listing->set_header('label',array('label'=>__('name'))) 
                ->set_header('user',array('style'=>'width:100px;text-align:center;'));        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('user','create-role')) {
            $listing->create_action(__('create-role'));
        }
        
        if (User::authorise('user','edit-role')) {
            $listing->edit_action(array(
                'exception' => array('id'=>'<= 1','i_absolute'=>true),
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('user','delete-role')) {
            $listing->delete_action(array(
                'exception' => array('is_system'=>1,'user'=>'>= 1','i_absolute'=>true),
                'ajax'      => 'ajax-role',                
                'href'      => 'delete/{id}/{label}'
            ));
        }
         
        //Another ways for listing button       
        $listing->action(array(
                "status" => array(
                    "exception" => array('id'=>'<= 1','i_absolute'=>true),
                    "authorise" => array("user"=>"publish-role"),
                    'ajax'      => 'ajax-role',
                    'href'      => '{function}/{id}/{label}'
                ),  
                "view"   => array(
                    "exception" => array('id' => '<= 1','i_absolute'=>true),
                    "authorise" => array("user"=>"view-role"),
                    "href"      => 'view/{id}'
                ),                     
            ));
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-role')) 
            ->register('content')->use_string($listing->render())
                ;   
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Check for authorisation
        $permission = $this->param1."-role";
        $this->authorise('user',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('user/cms/frm-role')
                    ->set('title',$title)
                ;   
    }
    
    public function action_view() {
            
        $this
            ->register('title')
                ->use_string(__('view-role'));
        
        if (User::authorise('user','view-role')) {
            
            $this->media_tabular();
            
            $this->register('content')
                    ->use_view('user/cms/view-role-detail')
                        ->set('role',Model::factory('Role')->get_by_id($this->param1))
                        ->set('permissions',Model::factory('Role')->available_permission_role_maps($this->param1))
                    ;
        } else {            
            $this
                ->register('content')
                    ->use_string(__('error.unauthorized'));
        }                     
    }
}