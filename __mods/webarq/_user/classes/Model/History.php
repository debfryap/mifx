<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_History extends Model_Dba {
    /** Data listing purpose **/
    public function total() {
        $q = Data::total(array('histories','h'),'actor',false)
                    ->join(array('users','u'))->on("u.username = h.actor")
                    ->join(array('roles','r'))->on("r.id = u.role_id")
                    ->where("r.id >= ".User::admin()->role_id);
        
        
        if (!empty($this->data_tool['search'])) {
            $search_key = $this->data_tool['search'];
            $q->where_open()->and_where("h.actor like %$search_key%")->or_where("h.description like %$search_key%")->where_close();
        }          
        return $q->execute()->current()->total; //App::$session->set('lst-tool',$data_tool);
    }
    
    public function tabular($limit=null,$offset=null) {
        
        $q = $this->select('h.actor','h.description','h.create_time','h.table')
                    ->from(array('histories','h'))
                    ->join(array('users','u'))->on("u.username = h.actor")
                    ->join(array('roles','r'))->on("r.id = u.role_id")
                    ->where("r.id >= ".User::admin()->role_id)
                    ->order_by('h.create_time','DESC')
                    ->limit(isset($limit) ? $limit : App::$config->perpage)
                    ->offset($offset);
        
        if (!empty($this->data_tool['search'])) {
            $search_key = $this->data_tool['search'];
            $q->where_open()->and_where("actor like %$search_key%")->or_where("description like %$search_key%")->where_close();
        }
        $q = $q->execute();
                
        
        $xitem = array();
         
        if (!empty($q)) {
            foreach ($q as $key => $item) {
                $xitem[$key] = $item;
                $xitem[$key]->activity = History::detail_activity($item,false);
            }
        }            
        return $xitem;
    }
    /** End Data listing purpose **/         
    
    protected function pre_select($field = NULL) {
        return $this->select('h.actor','h.description','h.create_time','h.table')
                    ->from(array('histories','h'));
    }
    
    //Get latest histories
    public function latest_histories($limit = NULL,$execute = TRUE) {
        if (is_bool($limit)) {
            $execute = $limit;
            $limit   = NULL;
        }
        
        $items = $this->pre_select()
                    ->join(array('users','u'))->on("u.username = h.actor")
                    ->join(array('roles','r'))->on("r.id = u.role_id")
                    ->order_by('h.create_time','DESC');
                    
        if (isset($limit)) $items->limit($limit);
        
        return $execute === TRUE ? $items->execute() : $items;
    }
    
    /**
     * Get latest histories depending on current role id
     * @param   int limit
     * @param   int role id
     * @return  Kohana Query Builder Execute
     */
    public function role_group_histories($limit = NULL, $role_id = NULL) {
        $role_id = is_numeric($role_id) ? $role_id : ($role_id === TRUE ? User::admin()->role_id : NULL);        
        $items   = $this->latest_histories($limit,FALSE);
        
        if (isset($role_id))
            $items->where("r.id >= ".User::admin()->role_id);
                        
        return $items->execute();
    }
    
    /**
     * Get latest histories depending on current role id, and range date
     * @param   int   span days
     * @return  array result
     */
    public function dashboard($span = 7) {
        $role_id = User::admin()->role_id;        
        $end     = $this->latest_histories(1,FALSE)->where("r.id >= ".User::admin()->role_id)->execute()->get('create_time');
        $start   = Date::before_date($end,"$span days");
        $items   = $this->select(DB::expr('COUNT(`h`.`actor`) AS `activity`'),DB::expr('DATE(`h`.`create_time`) AS `actual_date`'))
                        ->from(array('histories','h'))
                        ->join(array('users','u'))->on("u.username = h.actor")
                        ->join(array('roles','r'))->on("r.id = u.role_id")
                        ->where("r.id >= $role_id")
                        ->and_where("h.create_time <= $end")
                        ->and_where("h.create_time >= $start")
                        ->group_by('actual_date')
                        ->order_by('h.create_time','DESC')
                        ->execute();
                        
        if (!$items->valid()) return null;
        
        $max = 0;
        foreach ($items as $i => $item) {
            //Make sure activity is integer
            $int_activity = (int)$item->activity;
            
            //Search for maximum activity
            if ($int_activity > $max) $max = $int_activity;
            
            //Set activity items
            $result['item'][$i]['date']  = $item->actual_date;
            $result['item'][$i]['activity'] = $item->activity;
        }
        $result['max'] = $max;
        
        return $result;
    }
}