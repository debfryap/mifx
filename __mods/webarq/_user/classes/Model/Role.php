<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Role extends Model_Dba {        
    public function pre_select() {
        return $this->select('r.*',DB::expr("COUNT(u.id) AS `user`"),
                                DB::expr("CASE r.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('roles','r'))
                    ->join(array('users','u'),'LEFT')
                    ->on("u.role_id = r.id");    
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('roles','r'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select()->where("r.id",">=",User::admin()->role_id)->group_by("r.id");
                    
        return $model;
    }
    /** End Data listing purpose **/
    
    public function admin_role() {
        return $this->select('id','label')->from('roles')->where('is_admin = 1')->execute();
    }
    
    public function lower_role() {
        return $this->select('id','label')
                    ->from('roles')
                    ->where('is_admin = 1')
                    ->and_where('id','>=',User::admin()->role_id)
                    ->execute();
    }
    
    public function get_by_id($id) {
        return $this->pre_select()
                    ->where("r.id",">=",User::admin()->role_id)
                    ->and_where("r.id = $id")
                    ->group_by("r.id")
                    ->execute()
                    ->current();
    } 
    
    public function available_permission_role_maps($role_id) {
        $current_id = isset(User::admin()->role_id) ? (int)User::admin()->role_id : null; 
        $role_id    = (int) $role_id;
        
        if (empty($current_id) || $current_id >= $role_id) {
            return null;  
        } 
        
        $query  = "SELECT `nd`.*,
                        IF(`st`.`module_permission_id` IS NULL,0,1) AS `checked` 
                   FROM
                        (SELECT
                                `mprmi`.`module_permission_id` 
                            FROM
                                `module_permission_role_maps` AS `mprmi` 
                            WHERE `mprmi`.`role_id` = '$role_id'
                        ) AS `st` 
                    RIGHT JOIN 
                        (SELECT 
                                `m`.`label` `module`,`m`.`ordering`, `mp`.* 
                            FROM
                                `module_permission_role_maps` AS `mprm`
                            RIGHT JOIN `module_permissions` AS `mp` 
                                ON `mprm`.`module_permission_id` = `mp`.`id`
                            LEFT JOIN `modules` AS `m` 
                                ON `mp`.`module_id` = m.`id`
                  ";
        
        $query .= "WHERE `m`.`is_publish` >= 0";
        
        if ($current_id > 1) {
            $query .= " AND `mprm`.`role_id` = '$current_id'";    
        }
        
        $query .= "     ) AS `nd` 
                    ON `st`.module_permission_id = `nd`.`id` GROUP BY `nd`.`id` ORDER BY ordering ASC;";
        
        return $this->select()->query($query)->execute();
    }    
}