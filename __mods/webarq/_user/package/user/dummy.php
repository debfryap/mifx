<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Dummy data
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    "roles" => array (
        array (
            "id"        => 1,
            "label"     => "Superadmin",
            "is_admin"  => 1,
            "is_system" => 1,
            "is_active" => 1,
            "create_time"   => "now"
        ),
        array (
            "id"        => 2,
            "label"     => "Administrator",
            "is_admin"  => 1,
            "is_system" => 1,
            "is_active" => 1,
            "create_time"   => "now"
        ),
        array (
            "id"        => 3,
            "label"     => "Data Entry",
            "is_admin"  => 1,
            "is_system" => 0,
            "is_active" => 1,
            "create_time"   => "now"
        )
    ),
    
    
    "users" => array (
        array (
            "id"        => 1,
            "role_id"   => 1,
            "username"  => "superadmin",
            "password"  => "webarqc0m",
            "email"     => "superadmin@web.info",
            "is_system" => 1,
            "is_active" => 1,
            "create_time"   => "now"
        ),
        array (
            "id"        => 2,
            "role_id"   => 2,
            "username"  => "administrator",
            "password"  => "webarqc0m",
            "email"     => "admin@web.info",
            "is_system" => 1,
            "is_active" => 1,
            "create_time"   => "now"
        ),
        array (
            "id"        => 3,
            "role_id"   => 3,
            "username"  => "copywriter",
            "password"  => "webarqc0m",
            "email"     => "copywriter@web.info",
            "is_system" => 0,
            "is_active" => 1,
            "create_time"   => "now"
        )
    ),
    
    "user_details" => array (
        array (
            "id"        => 1,
            "user_id"   => 1,
            "first_name"=> "Superadmin",
            "sex"       => "male",
            "address"   => "Computer, HDD Track. 0.0, Disk 0",
            "phone"     => "",
            "avatar_file_name" => "" 
        ),
        array (
            "id"        => 2,
            "user_id"   => 2,
            "first_name"=> "Administrator",
            "sex"       => "male",
            "address"   => "Computer, HDD Track. 0.1, Disk 0",
            "phone"     => "",
            "avatar_file_name" => "" 
        ),
        array (
            "id"        => 3,
            "user_id"   => 3,
            "first_name"=> "Copy",
            "last_name" => "Writer",
            "sex"       => "male",
            "address"   => "Computer, HDD Track. 0.3, Disk 0",
            "phone"     => "",
            "avatar_file_name" => "" 
        )
    ),
    
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "user",
            "ordering"      => "total-row",
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "User module",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations" => array (
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user'",
            "label"         => "profile",
            "permalink"     => "profile",
            "ordering"      => 1,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user'",
            "label"         => "manage-user",
            "permalink"     => "manage-user",
            "ordering"      => 2,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user'",
            "label"         => "manage-role",
            "permalink"     => "manage-role",
            "ordering"      => 3,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user'",
            "label"         => "history",
            "permalink"     => "history",
            "ordering"      => 4,
            "create_time"   => "now"                              
        ),
    ),
    
    "module_navigation_group_maps" => array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='profile' AND `permalink` = 'profile' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-user' AND `permalink` = 'manage-user' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-role' AND `permalink` = 'manage-role' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"    => "now"
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='history' AND `permalink` = 'history' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='permission' AND `permalink` = 'permission' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
    ),
    
    /** **/
    "module_permissions" => array (
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "view-profile",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "edit-profile",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "view-user",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "create-user",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "edit-user",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "publish-user",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "delete-user",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "view-role",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "create-role",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "edit-role",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "publish-role",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "delete-role",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1",
            "permission"    => "view-history",
            "create_time"   => "now"                    
        )
    ),
    "module_navigation_permission_maps"  => array (
        //Profile 
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='profile' AND `permalink` = 'profile' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'view-profile' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='profile' AND `permalink` = 'profile' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'edit-profile' LIMIT 1",
            "create_time"   => "now" 
        ),
        
        //Manage User
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-user' AND `permalink` = 'manage-user' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'create-user' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-user' AND `permalink` = 'manage-user' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'edit-user' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-user' AND `permalink` = 'manage-user' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'publish-user' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-user' AND `permalink` = 'manage-user' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'delete-user' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-user' AND `permalink` = 'manage-user' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'view-user' LIMIT 1",
            "create_time"   => "now" 
        ),
        
        //Manage Role
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-role' AND `permalink` = 'manage-role' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'create-role' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-role' AND `permalink` = 'manage-role' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'edit-role' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-role' AND `permalink` = 'manage-role' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'publish-role' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-role' AND `permalink` = 'manage-role' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'delete-role' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-role' AND `permalink` = 'manage-role' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'view-role' LIMIT 1",
            "create_time"   => "now" 
        ),
        
        //History
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='history' AND `permalink` = 'history' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'view-history' LIMIT 1",
            "create_time"   => "now" 
        )
    ),
    /** **/
    "module_permission_role_maps" => array (
        array (
            "id"            => "auto",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'user' LIMIT 1) AND `permission` = 'view-profile' LIMIT 1",
            "role_id"       => 2,
            "create_time"   => "now"                    
        ),
    )
    /** **/
);

?>