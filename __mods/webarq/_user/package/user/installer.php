<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array (
    "roles" => array (
        array (
            "master" => "id",
            "type"   => "tinyint"
        ),
        array (
            "master" => "label",
            "form"   => array (
                "group"     => array("create-role","edit-role"),
                "minlength" => "4",
                "class"     => "required is_available",
                "data-ajax" => array (
                        "t" => (base64_encode('encode_roles')),
                        "f" => (base64_encode('encode_label')),
                        "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                    )                                
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_admin",
            "default" => 0,
            "form"   => array (
                "group" => array("create-role","edit-role"),                
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_system",
            "default" => 0
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-role","edit-role"),
            )
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    ),
    
    "users" => array (
        array (
            "master" => "id"
        ),
        array (
            "field"  => "role_id",
            "type"   => "tinyint",
            "form"   => array (
                "group"     => array("create-user"),
                "label"     => "role",
                "class"     => "required",
                "object"    => "userform:select_user_role_id",
            )
        ),
        array (
            "field"  => "username",
            "type"   => "char",
            "length" => 15,
            "form"   => array (
                "group"     => array("profile","create-user","edit-user"),
                "type"      => array("hidden","text"),
                "readonly"  => array("readonly",false),
                "class"     => "required is_available",
                "data-ajax" => array(
                    "t" => (base64_encode('encode_users')),
                    "f" => (base64_encode('encode_username')),
                    "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                ) 
            )
        ),
        array (
            "field"  => "password",
            "type"   => "varchar",
            "length" => 70,
            "form"   => array (
                "group" => array("profile","create-user","edit-user"),
                "type"  => "password",
                "class" => array(false,"required"),
                "data-ajax" => array (
                    "t" => (base64_encode('encode_users')),
                    "f" => (base64_encode('encode_email')),
                    "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                )  
            )
        ),
        array (
            "field"  => "email",
            "type"   => "varchar",
            "length" => 50,
            "form"   => array (
                "subtitle"  => __('account-info'),
                "group"     => array("profile","create-user","edit-user"),
                "readonly"  => array("readonly",false,false),
                "class"     => array("required email","required email is_available","required email is_available"),
                "order"     => "first",
                "data-ajax" => array(
                    "t" => (base64_encode('encode_users')),
                    "f" => (base64_encode('encode_email')),
                    "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                ) 
            )
        ),
        array (
            "type"   => "char",
            "field"  => "ip_addr",
            "length" => 15,
            "form"   => array (
                "group" => !empty(User::$config->check_ip) ? array("profile","create-user","edit-user") : "NULL",
                "class" => "required",
                "name"  => "ip_address",
                "hint"  => "Limit your account, so only met IP could login",
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_system"
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "form"   => array (
                "group" => array("create-user"),
            )
        ),
        array (
            "field"  => "activation_key",
            "type"   => "varchar",
            "length" => 70
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    ),
    
    
    "user_details" => array (
        array (
            "master" => "id"
        ),
        array (
            "type"   => "int",
            "field"  => "user_id"
        ),
        array (
            "master" => "label",
            "field"  => "first_name",
            "default" => "",
            "form"   => array (
                "subtitle"  => __('general-info'),
                "group" => array("profile","create-user","edit-user"),
                "label" => __('first-name'),
                "class" => "required",
            )
        ),
        array (
            "master" => "label",
            "field"  => "last_name",
            "default" => "",
            "form"   => array (
                "group" => array("profile","create-user","edit-user"),
                "name" => "nama_akhir",
                "label" => __('last-name'),
            )
        ),
        array (
            "master" => "label",
            "field"  => "middle_name" 
        ),
        array (
            "master" => "select",
            "field"  => "sex",
            "option" => "male,female",
            "form"   => array (
                "group" => array("profile","create-user","edit-user"),
                "option" => array('male'=>__('male'),'female'=>__('female')),
            )
        ),
        array (
            "master" => "text",
            "field"  => "address",
            "type"   => "mediumtext",
            "form"   => array (
                "group" => array("profile","create-user","edit-user"),
                "name" => "address",
                "label" => __('address'),
            )
        ),
        array (
            "master" => "label",
            "field"  => "phone",
            "length" => 50,
            "form"   => array (
                "group" => array("profile","create-user","edit-user"),
            )
        ),
        array (
            "master" => "label",
            "field"  => "avatar_file_name",
            "form"   => array (
                "group" => array("profile","create-user","edit-user"),
                "type" => "file",
                "id" => "uploadify",
                "hint"  => "ONLY JPEG, GIF, AND PNG ALLOWED, AND PLEASE NOTE ALL IMAGE WILL BE RESIZING INTO PROPER 120x145 SIZE",
                /**
                "preview" => array (
                    "type" => "image", 
                    "size" => array (120,145),
                    "path" => URL::mediaImage('users/avatars/120x145/',true)
                )
                **/
            )
        )
    ),
    
    
    "user_sessions" => array (
        array (
            "master" => "id",
            "field"  => "user_id",
            "increment" => false 
        ),
        array (
            "field"  => "session",
            "type"   => "varchar",
            "length" => 150,
            "primary"   => true
        ),
        array (
            "master" => "create_time",
            "field"  => "login",
            "primary"   => true
        ),
        array (
            "master" => "create_time",
            "field"  => "logout"
        )
    ),
    
    "user_tokens"   => array (
        array (
            "type"   => "int",
            "field"  => "user_id" 
        ),
        array (
            "field"  => "email",
            "type"   => "varchar",
            "length" => 50
        ),
        array (
            "type"   => "varchar",
            "length" => 50,
            "field"  => "token",
            "null"   => false   
        ),
        array (
            "type"   => "datetime",
            "field"  => "expired",
            "null"   => false   
        ),
        array (
            "type"   => "boolean",
            "field"  => "is_use",
            "default"=> 0
        )
    ) 
);