<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    File detail
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

    if (empty($file->name)) {
        echo __('error.no-data');
        return;    
    }
     
    
    $class_type = file_exists("assets/__system/lib/images/files/".$file->ext.".png")
                     ? $file->ext.'-file'
                     : 'unknown-file';
    
    if (isset($file->width)) {
        $height = 300; 
        $width  = 400;
        $scale  = $height/$file->height;
        $scl_wd = ceil($scale*$file->width); 
        if ($file->width < $width) {
            if ($file->width <= $scl_wd) {
                $width  = $file->width;
                $height = $file->height; 
            }
        }
        
        echo '<div style="width:'.$width.'px;overflow:hidden" class="view-detail image">';
            echo HTML::image($file->path,array(
                'style' => 'height:' . $height . 'px;'
             ));
    } else {
        
        $styles_bg  = file_exists("assets/__system/lib/images/files/".$file->ext.".png")
                        ? "assets/__system/lib/images/files/".$file->ext.".png"
                        : "assets/__system/lib/images/files/unknown.png";
        $styles_bg  = URL::root($styles_bg);     
        echo '<div class="view-detail image icon ' . $class_type .'" style="width:58px;background-image:url(' . $styles_bg . ');">';
    }             
    echo '</div>';    
          
    echo '<div style="width:390px;overflow:hidden;overflow-y:auto;" class="view-detail text">';        
        echo '<div class="inline-block label-section">Name</div>';
        echo '<div class="inline-block value-section"> : ',$file->name,'</div>';
        
        echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';
        echo '<div class="inline-block label-section">Size </div>';
        echo '<div class="inline-block value-section"> : ',$file->byte,' bytes</div>';
        
        echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';
        echo '<div class="inline-block label-section">File Type </div>';
        echo '<div class="inline-block value-section"> : ',$file->ext,'</div>';
        
        if (isset($file->width)) {        
            echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';
            echo '<div class="inline-block label-section">Image Width </div>';
            echo '<div class="inline-block value-section"> : ',$file->width,' px</div>';
        }
        
        if (isset($file->height)) {        
            echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';
            echo '<div class="inline-block label-section">Image Height </div>';
            echo '<div class="inline-block value-section"> : ',$file->height,' px</div>';
        }
        
        echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';
        echo '<div class="inline-block label-section">Upload </div>';
        echo '<div class="inline-block value-section"> : ',date('F Y,d @ H:i',strtotime($file->uploaded)),'</div>';
        
        if (!empty($uses[0])) {
            echo '<p>',__('media-file-usage'),'</p>';
            foreach ($uses as $media) {
                $plurar = $media->applicant_table;
                $singlr = Inflector::singular($plurar);
                if (!empty(Package::$_history->object->$singlr))
                    $label = Package::$_history->object->$singlr;
                elseif (!empty(Package::$_history->object->$plurar))
                    $label = Package::$_history->object->$plurar;
                else
                    $label = 'id';
                    
                if (isset($label))
                    $label = Model::factory('Medialibrary')->media_applicant($plurar,$label,$media->applicant_id);
                
                if (!isset($label))
                    $label = $plurar;
                
                $applicant_object = ucfirst(str_replace("_"," ",$singlr));
                $applicant_type   = $media->type;
                
                echo '<div>'
                        ,(__('media-used-in',array(
                                ':number'   => $media->used,
                                ':times'    => ((int)$media->used <= 1 ? 'time' : 'times'),
                                ':location' => "<span style=\"text-decoration:underline;\">$applicant_object <b>$label</b> as " . __("image-$applicant_type") . "</span>")))
                        ,'</div>';
            }
        } else {
            echo '<p style="font-weight:bolder;padding-top:15px;">',__('media-unused',array(':type'=>$file->media_type )),'</p>';
        }
    echo '</div>';
?>