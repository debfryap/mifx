<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    File list
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (empty($files[0])) { 
    $type  = $type == 'doc' ? 'document' : $type;
    echo __('no-available-media',array(':type'=>$type)); 
} else {
    foreach ($files as $file) {
        $readfile = Medialibrary::file_info("$file->media_folder_name/$file->media_file_name",18);
        
        if (!is_array($readfile)) continue;
        
        //$class_type = file_exists("__mods/webarq/medialibrary/widget/medialibrary/cms/images/files/".$readfile['ext'].".png")
        $class_type = file_exists("assets/__system/lib/images/files/".$readfile['ext'].".png")
                         ? $readfile['ext'].'-file'
                         : 'unknown-file';
        
        $styles_bg  = file_exists("assets/__system/lib/images/files/".$readfile['ext'].".png")
                        ? "assets/__system/lib/images/files/".$readfile['ext'].".png"
                        : "assets/__system/lib/images/files/unknown.png";
        $styles_bg  = URL::root($styles_bg);           
                                                    
        $json_read_file = json_encode(array_merge($readfile,array(
                            'media_id'   => $file->id,
                            'media_type' => $file->type,
                            'uploaded'   => $file->create_time,
                            'styles_bg'  => $styles_bg
                          )));
        
        echo '<div data-file=\''.$json_read_file.'\' class="gallery'; 
            $file_used = (int)$file->used;
            if ($file_used <= 0 && User::authorise('medialibrary',"delete-media-$type")) {
                echo " is-deleted";               
            }                        
            if ($type === 'image') {
                echo ' larger">';
                if (!empty($readfile['thumb'])) {                    
                    $thumb_width  = $readfile['thumb_width'];
                    $thumb_height = $readfile['thumb_height'];
                    $img_file     = $readfile['thumb'];
                } else {                    
                    $thumb_width  = Medialibrary::$config->thumb_width;
                    $thumb_height = Medialibrary::$config->thumb_height;    
                    $img_file     = $readfile['path'];                
                } 
                
                echo '<div class="thumb " style="padding-top:5px;height:',($thumb_height+5),'px;">';
                
                    echo HTML::image($img_file,array(
                        'width' => $thumb_width
                    ));   
                echo '</div>'; 
            } else {
                echo ' smaller">';
                echo '<div class="thumb '.$class_type.'" style="background-image:url('.$styles_bg.')">&nbsp;</div>';
            } 
                            
            echo '<div class="file_name">',$readfile['short_name'],'</div>';
            
            if ($file_used >= 1) {
                echo '<div class="file_used">';            
                    echo __('used-in',array(
                        '%time' => $file_used,
                        '%s'    => $file_used== 1 ? '' : 's'
                    ));
                echo '</div>';
            } else {
                #echo '<span class="unlink-media">',__('unlink'),'</span>';
            }
        echo '</div>';
    }
}
?>

