<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Medialibrary_BasicBanner extends Populate {
    /**
     * @var string Active package
     */
    protected $package;
    
    /**
     * @var string Media library table applicant name
     */
    protected $table;
     
    /**
     * @var int    Media library table applicant row id
     */
    protected $id;
    
    /**
     * @var sring Table applicant searching link
     * No longer use if isset($id) and so, will use to determine $id while unset
     * See class constructor
     */
    protected $permalink;
    
    /**
     * @var bool tailing banner condition  
     */
    protected $hierarchy_tailing = true;
    
    /**
     * @var object system tailing main active uri 
     */
    private $_tail_uri;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        $o = App::tail_active_uri($this->package,$this->permalink);
        $this->_tail_uri = Helper_Kernel::arr_to_object(array(
                                'uri'  => $o->get('active_uri'),
                                'tail' => $o->get('tail')));
        
        if (!isset($this->permalink))
            $this->permalink = $o->get('active_uri');
        
        if (!isset($this->table))
            $this->table = !isset($this->package) || $this->package == $this->permalink ? 'site_navigations' : Inflector::plural($this->package);
            
        if (!isset($this->id))
            $this->id = Model::factory('Dba')
                                ->select('id')
                                ->from($this->table)
                                ->limit(1)
                                ->where("permalink = $this->permalink")
                                ->execute()
                                ->get('id',null);   
        else
           $this->hierarchy_tailing = false;  
    }
    
    public function render() {
        
        if (!isset($this->id))
            return null;
            
        $media = Model::factory('Medialibrary')->get_application('banner',$this->id,$this->table)->current();
        
        if (!empty($media->applicant_folder)) {
            return $this->render_media($media);
        } else {
            
            if (!empty($this->hierarchy_tailing)) {
                if (!empty($this->_tail_uri->tail)) {
                    $tails = array_reverse(Helper_Kernel::object_to_array($this->_tail_uri->tail));
                    
                    //No need to check current active
                    array_shift($tails);
                    
                    if ($this->hierarchy_tailing === 'top') {
                        return $this->inside_event(array_pop($tails));  
                    }elseif ($this->hierarchy_tailing === 'package') {
                        $this->table = 'site_navigations';                
                        return $this->inside_event($this->package);  
                    }
                    
                    foreach ($tails as $permalink) {
                        $widget = $this->inside_event($permalink);
                        
                        if (!empty($widget)) {
                            return $widget;
                        }                    
                    }
                }                   
               
                if (isset($this->package)) { 
                    //Try using site navigations, if package is isset
                    $this->table = 'site_navigations';               
                    
                    //No need to tailing anymore, we are in top hierarchy 
                    $this->hierarchy_tailing = false;
                    
                    //Return anything we have                                        
                    return $this->inside_event($this->package);
                }              
            }
        }
    } 
    
    private function inside_event($permalink) {
        $widget = Widget::load('medialibrary/basic-banner',array(
                        'package'           => $this->package,
                        'table'             => $this->table,
                        'permalink'         => $permalink,
                        'hierarchy_tailing' => $this->hierarchy_tailing
                  ));
                  
        return $widget->render();
    }
    
    private function render_media($media) {      
        return '<div id="widget-medialibrary-basic-banner">'
               .HTML::image(URL::root($media->applicant_folder.'/'.$media->media_file_name),array(
                    'style' => 'width:'.Medialibrary::$config->media_library_application_types->image->banner->width.'px',
                    'alt'   => isset($media->detail->title) ? $media->detail->title : App::$config->system->image_alt
                ))
                .'</div>';
    }
}