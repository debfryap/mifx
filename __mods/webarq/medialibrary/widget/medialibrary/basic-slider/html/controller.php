<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Medialibrary_BasicSlider extends Populate {
    
    protected $table;
    
    protected $permalink;
    
    public function __construct(array $configs = array()) {
        //Inherit parent construct
        parent::__construct($configs);
    }
    
    public function data() {
        //Active uri
        $uri  = isset($this->permalink) ? $this->permalink : Request::$current->uri();
        
        //Get row applicant id
        $id = Model::factory('Dba')->select('id')->from($this->table)->where("permalink = $uri")->execute()->get('id',null);
        
        if ($id === null)
            return null;
        
        $getSlide = Model::factory('Medialibrary')->simple_application('slide',$id,$this->table);
        
        if (empty($getSlide[0]))
            return null;

                    
        foreach ($getSlide as $key => $item) {
            if (isset($item->detail))
                $item->detail = json_decode($item->detail);
                
            $slides['slides'][$key] = $item;
        }
        
        return $slides;
    }
}