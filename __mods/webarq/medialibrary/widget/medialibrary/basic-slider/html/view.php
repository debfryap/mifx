<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

if (empty($slides[0])) return;

?>

<div id="basic-slider-wrapper">
    <ul class="bjqs">
        <?php 
            foreach ($slides as $key => $item) 
            {
                unset($img_attr);
                
                echo '<li';
                echo '>';
                if (!empty($item->detail->permalink))
                    echo '<a href="',$item->detail->permalink,'">';
                                
                if (!empty($item->detail->title))
                    $img_attr['title'] = $item->detail->title;
                     
                echo HTML::image(URL::root($item->folder.'/'.$item->file),isset($img_attr) ? $img_attr : null);
                                
                if (!empty($item->detail->permalink))
                    echo '</a>';
                echo '</li>';
            } 
        ?> 
    </ul>
</div>
