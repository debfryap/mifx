<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

echo Widget::load('station/form')->data('print',false); 
  
?>
<h3 class="title-page"><?php echo $title; ?></h3>

<div id="media-library-wrapper">
    
    <?php
        echo Data::form(array(
            'title'     => null,
            'id'        => 'form-media-library-configuration',
            'multilang' => false,  
            'values'    => false,
            'cancel'    => false,
            'elements'  => array (
                array (
                    'label'     => __('thumb-width'),
                    'name'      => 'thumb_width',
                    'id'        => 'thumb-width',
                    'class'     => 'numeric required',
                    'min'       => '50',
                    'type'      => 'text',
                    'value'     => Medialibrary::$config->thumb_width
                ),
                array (
                    'label'     => __('thumb-height'),
                    'name'      => 'thumb_height',
                    'id'        => 'thumb-height',
                    'class'     => 'numeric required',
                    'min'       => '50',
                    'type'      => 'text',
                    'value'     => Medialibrary::$config->thumb_height
                ),
                array (
                    'label'     => __('allowed-media',array(':media'=>'image')),
                    'name'      => 'allowed_image',
                    'id'        => 'allowed-image',
                    'class'     => 'required',
                    'type'      => 'text',
                    'value'     => implode(';',Medialibrary::$config->extension->image),
                    "info"      => 'separate width (semicolon) (;)'
                ),
                array (
                    'label'     => __('allowed-media',array(':media'=>'video')),
                    'name'      => 'allowed_video',
                    'id'        => 'allowed-video',
                    'class'     => 'required',
                    'type'      => 'text',
                    'value'     => implode(';',Medialibrary::$config->extension->video),
                    "info"      => 'separate width (semicolon) (;)'
                ),
                array (
                    'label'     => __('allowed-media',array(':media'=>'document')),
                    'name'      => 'allowed_doc',
                    'id'        => 'allowed-doc',
                    'class'     => 'required',
                    'type'      => 'text',
                    'value'     => implode(';',Medialibrary::$config->extension->doc),
                    "info"      => 'separate width (semicolon) (;)'
                ),
                
                array (
                    'label'     => __('replace-same-file'),
                    'name'      => 'replace_same_file',
                    'id'        => 'replace_same_file',
                    'class'     => 'replace_same_file',
                    'type'      => 'checkbox',
                    'order'     => 'after password',
                    'checked'   => Medialibrary::$config->replace_same_file,
                    'info'      => 'Eg. You have upload file1.jpg, but currently file with same name and type already exist, so how do you want to handle ? '
                ),
            )
        ));
    ?>
</div>