var browser = {
    resultUpload : 'ul#result-upload'
};

browser.createFolder = function(element,position) {
    if ($('#media-file').children('.file-manager').hasClass('browse-media')) {
        $('.masking-html-container').append('<div class="new-folder-name-wrapper">\
                                    Folder Name : <input class="others" type="text" id="new-folder-name"/> \
                                    <input type="button" class="submit" value="Create"/>&nbsp;&nbsp;<input type="button" class="cancel" value="Cancel"/>\
                                 </div>');
        $('.new-folder-name-wrapper input[type="button"].cancel')
                .click(function(){$('.new-folder-name-wrapper').remove();});
    } else {
        enable_masking_layer('');
        
        $('.masking-html-container').html('<div class="new-folder-name-wrapper">\
                                        Folder Name : <input type="text" id="new-folder-name"/> \
                                        <input type="button" class="submit" value="Create"/>&nbsp;&nbsp;<input type="button" class="cancel" value="Cancel"/>\
                                     </div>');
        
        $('.new-folder-name-wrapper input[type="button"].cancel').click(function(){disable_masking_layer();});        
    }
                                     
    $('.new-folder-name-wrapper input[type="text"]#new-folder-name').focus().alphanumeric({allow:'-'});
    
    
    $('.new-folder-name-wrapper input[type="button"].submit').click(function(){
        folder_name = $.trim($('.new-folder-name-wrapper input[type="text"]#new-folder-name').val());
        if (folder_name == '') {
            jAlert('Folder name should be not empty.','Warning!!!');
        } else {
            browser.appendFolder(element,folder_name);
        }     
    });
}

browser.appendFolder = function(element,name) {        
    //Waiting message
    if ($('#media-file').children('.file-manager').hasClass('browse-media')) {
        $('.new-folder-name-wrapper').html('Please wait. This action may take a while to acomplish')
    } else {
        $('.masking-html-container').html('Please wait. This action may take a while to acomplish');
    }
                    
    //Folder path
    path = element.attr('data-dir');
                    
    setTimeout(function(){
        $.ajax({
            url     : bs_cms+"medialibrary/ajax/new-media-folder",
            type    : "POST",
            data    : {"parent":path,"name":name,'type':browser.media_type},
            async   : false,
            success : function(response) {    
                response = $.trim(response);
                
                if ($('#media-file').children('.file-manager').hasClass('browse-media')) {
                    $('.new-folder-name-wrapper').remove();    
                } else {
                    disable_masking_layer();
                }
                
                if (response === 'false') {
                    
                    jAlert('Can not perform your requested action at this moment, \
                            or you do not have authorisation for this section. Please contact \
                            your administrator if you think this is an error','Warning!!!');
                    return false;    
                }
                
                //Set tree icon
                element.siblings('span.tree').removeClass('open closed').addClass('open');
                
                //Append to parent    
                html = '<div class="folder child" style="display: block;">  \
                        <span class="tree" style="width:12px;">&nbsp;</span>   \
                        <span class="name" data-dir="'+path+'/'+name+'">'+name+'</span>   \
                        </div>';  
                element.parent().append(html);
                
                $('.folder').children('span.name')
                    .contextMenu({
                        menu: 'myDirMenu'
                    },
                    function(action, el, pos) {
                        if (action == 'newSubFolder') {
                            browser.createFolder(el,pos);
                        }
                    }); /** **/
                
                
                $('.folder').children('span.tree')
                        .unbind('click')
                        .bind('click',function(){
                            tree = $(this);
                            
                            if (tree.hasClass('open')) {
                                tree.siblings('.folder').slideUp('slow');           
                                tree.removeClass('open').addClass('closed');
                            } else if (tree.hasClass('closed')) {
                                tree.siblings('.folder').slideDown('slow');           
                                tree.removeClass('closed').addClass('open');
                            } 
                            
                            var tmp249090 = true;               
                        });
                $('.folder').children('span.name')
                        .unbind('click')          
                        .click(function(){
                            span = $(this);
                            $('.folder').children('span.name.current').removeClass('current');
                            span.addClass('current');                            
                            browser.loadFile(span.attr('data-dir'));    
                        });
            }
        });   
    },10);           
}


browser.loadFile = function(path) {
    //Hide previous result upload if any
    $(browser.resultUpload).fadeOut(200); serverReload = '';
    
    //Change active path
    $('span.media-active-folder span').html(path);
    
    //Uploadify init
    if (typeof browser.uploadify === 'undefined') {
        uploadify_maker($('#fileBrowser'),{
            //buttonImage     : bs_root+'assets/__system/lib/js/uploadify-3.2/button-upload.png',
            buttonText      : "Upload Files",
            formData        : {'cookie':$.cookie(cms_cookie),'type':browser.media_type,'path':path},
            uploader        : bs_cms+"medialibrary/uploadify/upload-file",
            fileObjName     : 'fileSelect',
            fileTypeExts    : browser.media_ext,
            onSelect        : function(file) {
                $(browser.resultUpload).html('');
            },
            onUploadSuccess : function(file, data, response) {
                datatrim = $.trim(data); 
                if (datatrim != 'true') {
                    $(browser.resultUpload).fadeIn(200);
                    if (data == 'false')
                        $(browser.resultUpload).append('<li class="error">'+file.name+' not uploaded to server</li>');
                    else
                        $(browser.resultUpload).append('<li class="file-exists">'+datatrim+'</li>');
                    if (serverReload == '')
                        serverReload = false;
                } else {
                    serverReload = true;
                }
            },
            onQueueComplete : function (queueData) {
                if(serverReload)
                    browser.loadFile($('.folder').children('span.name.current').attr('data-dir'));
            }
        });
        browser.uploadify = true;
    } else {
        $('#fileBrowser').uploadify('settings','formData',{'cookie':$.cookie(cms_cookie),'type':browser.media_type,'path':path});
    }
    
    $('#media-file').children('.file-manager').hide();
     
    $('#media-file').children('.loading-file').show();
    
    setTimeout(function(){
        $.ajax({
            url     : bs_cms+"medialibrary/ajax/listing-file",
            type    : "POST",
            data    : {"dir":path,'type':browser.media_type},
            async   : false,
            success : function(response) {
                $('#media-file').children('.loading-file').hide(function(){
                    
                    $('#media-file').children('.file-manager').html(response).show();
                    
                    
                    //Context menu folder
                    $('.file-manager.list-media .gallery.is-deleted')
                        .contextMenu({
                            menu: 'myFileMenu'
                        },
                            function(action, el, pos) {
                                if (action == 'deleteFile') {
                                    if (!el.hasClass('is-deleted'))
                                        return false;
                                    
                                    el_data = $.parseJSON(el.attr('data-file'));
                                    
                                    jConfirm('Please note, all deleted file could not be re-store again. Press ok to continue'
                                                ,'Warning!!!'
                                                ,function(act){
                                                    if (act) {
                                                        nous_process_message('Please wait while deleting all related data',true);
                                                        setTimeout(function(){
                                                            $.ajax({
                                                                url     : bs_cms+"medialibrary/ajax/delete-file",
                                                                type    : "POST",
                                                                data    : {media_id:el_data.media_id,path:el_data.rel_path,type:browser.media_type},
                                                                async   : false,  
                                                                success : function(response){
                                                                    if($.trim(response) === 'true') {
                                                                        el.remove();   
                                                                        nous_disable_message(); 
                                                                    } else {
                                                                        jAlert('Error while trying to delete a file. Please contact yoru administrator');
                                                                        nous_disable_message();
                                                                    }
                                                                }
                                                            });
                                                        },10);
                                                    }    
                                                });
                                }
                            });
                        
                    $('.file-manager.list-media .gallery')
                        .unbind('click')
                        .bind('click',function(){
                            browser.showDetail($(this));
                        });
                    
                    $('.file-manager.browse-media .gallery')
                        .unbind('click')
                        .bind('click',function(){
                            browser.selectMedia($(this));
                        });
                        
                    if ($('#media-file').children('.file-manager').hasClass('browse-media')) {
                        $('#fileBrowser').uploadify('settings','queueSizeLimit',1);
                        $('#fileBrowser').uploadify('settings','multi',false);
                    }
                    
                    center_middle('.masking-html-container');
                    
                    //$('body').addClass('noScroll');
                    
                    $('#masking-html-close,#transparent-masking').click(function(){
                        $('body').removeClass('noScroll');
                    });
                });    
            },
            error   : function() {
                $('#media-file').children('.loading-file').hide(function(){                        
                    $('#media-file').children('.file-manager').html('Error while trying to load files. Please contact your administrator').show();
                });    
            }
        });
    },10);
}

browser.showDetail = function(object) {
    
    enable_masking_layer('');
    $('#masking-html').css('opacity',1).html('Please wait while trying to retrieve file information, this may take a few seconds');center_middle('#masking-html');
    
    setTimeout(function(){
        $.ajax({
            url     : bs_cms+"medialibrary/ajax/file-detail",
            type    : "POST",
            data    : {"file":object.attr('data-file')},
            async   : false,
            success : function(response) {
                response = $.trim(response);
                if (response !== 'false') {
                    enable_masking_layer(false);
                    $('#masking-html').css('opacity',0).addClass('file-detail').append(response);
                    center_middle('#masking-html');
                    
                } else {
                    jAlert('Could not complete this action. Please contact your administrator','Warning!!!',function(){
                        enable_popUp(false);    
                    });
                }                  
            }
        });
    },500);
}

browser.selectMedia = function(object) {
    df  = object.attr('data-file');
    mdx = $('.tab-selector.media').children('ul').children('li.active');
    mdt = mdx.attr('data-type');    //media type
    mdn = mdx.attr('data-name');    //media name
    mdl = mdx.attr('data-limit');   //media limit
    
    if (typeof df == 'undefined') {
        jAlert('Could not complete action select media. Please contact your administrator','Warning!!!');
        return true;   
    }
    
    if (typeof mdl !== 'undefined' && mdl > 0) {
        mdl = parseInt(mdl);
        
        if ($('.media-container.active').children('.isSortable').children('.media-item-container').length >= mdl) {
            jAlert('You can not add another media to '+mdx.children('span').text()+'. Only '+mdl+' item allowed');
            return true;
        }
    }
    
    df = $.parseJSON(df);
    
    object.addClass('used');
    
    if ($('.media-container.active').children('.isSortable').children('.media-item-container#item-container-'+df.media_id).length > 0) {
        object.addClass('used');
        jAlert(df.name+' already selected, and does not need to adding anymore','Warning!!!');
        return true;
    }
    
    html  = '<div class="sortable media-item-container '+df.media_type+'" id="item-container-'+df.media_id+'">';
    html += '<div class="view-detail image icon';
    if (typeof df.thumb != 'undefined') {
        html +=  '" style="margin-left:5px;width:'+df.thumb_width+'px;/**height:'+(df.thumb_height+20)+'px;**/">';
        html += '<img  src="'+df.thumb+'" style="border:1px solid #ccc;width:'+df.thumb_width+'px;"/>';
        html += '<span style="margin-top:5px;" class="unlink">Unlink</span></div>';
    } else {
        html += '" style="background-image:url('+df.styles_bg+')"><span class="unlink">Unlink</span></div>';
    }
    
    html += '<div class="view-detail text">';
    
        html += '<div class="inline-block label-section">Name</div>';
        html += '<div class="inline-block value-section"> : '+df.name+' \
                        <input type="hidden" name="media_application['+mdn+']['+df.media_id+'][type]" value="'+mdt+'"/></div>';
        if (typeof df.rel_path !== 'undefined') 
            html += '<input type="hidden" name="media_application['+mdn+']['+df.media_id+'][rel_path]" value="'+df.rel_path+'"/>';
            
        html += '<div style="height:1px;line-height:1px;">&nbsp;</div>';
        
        data_details = mdx.attr('data-detail');
        
        if (typeof data_details != 'undefined') {
            data_details = $.parseJSON(data_details);
            $.each(data_details,function(i,ipt){
                html += '<div class="inline-block label-section">';
                html += (typeof ipt.label == 'undefined' ? ipt.name : ipt.label);
                html += '</div>';
                html += '<div class="inline-block value-section"> : ';
                
                
                //Manipulate attributes
                ipt.name = 'media_application['+mdn+']['+df.media_id+'][detail]['+ipt.name+']';
                ipt.type = ipt.type === undefined ? 'text' : ipt.type;
                
                //Join attributes
                dataAttr = '';
                for (a in ipt) {
                    dataAttr += ' '+a+'="'+ipt[a]+'"';                    
                }
                
                switch(ipt.type) {
                    case "textarea":
                        html += '<textarea'+dataAttr+'></textarea>'
                        break;
                    default :
                        html += '<input'+dataAttr+'/>'
                }
                //html += '<input type="text" name="media_application['+mdn+']['+df.media_id+'][detail]['+ipt.name+']"/>';
                html += '</div>';
                html += '<div style="height:1px;line-height:1px;">&nbsp;</div>';
            });
        }
    
    html += '</div>';
    html += '<div class="clear"></div>';
    
    html += '</div>';
                       
    $('.media-container.active .isSortable').append(html);
      
    
    if (typeof browser.sortable == 'undefined') {
        $('.isSortable').sortable({
            connectWith: ".media-item-container",  
            cursor: "move"
        });
        browser.sortable = true;
    }        
    
    $('.unlink')
        .unbind('click')
        .bind('click',function(){
            $(this).parents('.media-item-container').remove();
        }); 
}

$(document).ready(function(){
    
    setTimeout(function(){
    
        browser.media_type = $('#media-folder').children('.folder.top').attr('data-media-type');
        
        browser.media_ext  = $('#media-folder').children('.folder.top').attr('data-media-ext');
        
        //Tree method
        $('.folder').children('span.tree').click(function(){
            tree = $(this);
            
            if (tree.hasClass('open')) {
                tree.siblings('.folder').slideUp('slow');           
                tree.removeClass('open').addClass('closed');
            } else if (tree.hasClass('closed')) {
                tree.siblings('.folder').slideDown('slow');           
                tree.removeClass('closed').addClass('open');
            }                
        }); 
        
        //Context menu folder
        $('.folder').children('span.name')
            .contextMenu({
                menu: 'myDirMenu'
            },
                function(action, el, pos) {
                    if (action == 'newSubFolder') {
                        browser.createFolder(el,pos);
                    }
                })
            .click(function(){
                span = $(this);
                $('.folder').children('span.name.current').removeClass('current');
                span.addClass('current');  
                browser.loadFile(span.attr('data-dir'));  
            });
        
        //Load file
        setTimeout(function(){
            browser.loadFile($('.folder').children('span.name.current').attr('data-dir'));    
        },20);
    },250);
});