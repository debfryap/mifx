<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  
?>
<h3 class="title-page"><?php echo $title; ?></h3>

<div id="media-library-wrapper">
    <div class="liquid_column_mask liquid_left_column">
        <div class="liquid_right_column">
            <div class="liquid_column_wrap">
                <div id="media-file" class="liquid_content_right">
                    <span class="media-active-folder">PATH : <span><?php echo $active_folder;?></span></span>
                    
                    <?php if (User::authorise('medialibrary',"upload-media-$type")) { ?> 
                    <div class="uploader">
                        <input type="file" id="fileBrowser" name="fileBrowser" style="display: block;"/>
                        <ul id="result-upload" style="display: none;"></ul>
                    </div>
                    <div style="border-bottom: 1px solid #ccc;padding:10px 0;font-weight:bold;margin-bottom:5px;">Allowed type : <?php echo !empty(Medialibrary::$config->extension->$type) ? implode(',',Medialibrary::$config->extension->$type) : '' ;?></div>
                    <?php } else { ?>
                    <div style="border-bottom: 1px solid #ccc;padding:10px 0;font-weight:bold;margin-bottom:5px;"></div>    
                    <?php } ?>
                    
                    <div class="loading-file">Read directory ...</div>
                    
                    <div class="file-manager <?php echo isset($browserMedia) ? 'browse-media' : 'list-media';?>"></div>
                </div>
            </div>
            
            <div id="media-folder" class="liquid_content_left">
                <?php echo $folder; ?>    
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <ul id="myDirMenu" class="dirContextMenu">
		<li class="new-sub-folder"><a href="#newSubFolder">New Subfolder</a></li>
	</ul>
    <ul id="myFileMenu" class="dirContextMenu">
		<li class="new-sub-folder"><a href="#deleteFile">Delete</a></li>
	</ul>
</div>