<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Medialibrary_Cms extends Populate {
    
    protected $type = 'image';
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
    }
    
    public function data() {
        //Media handler
        $data['type']   = $this->type;
        $data['folder'] = Medialibrary::list_folder($this->type);
        $data['active_folder'] = '';
        
        return $data;      
    } 
}

?>