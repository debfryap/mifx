<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array (
    "media_libraries" => array (
        array (
            "master"    => "id",
            "type"      => "bigint"
        ),
        array (
            "field"     => "type",
            "type"      => "char",
            "length"    => 6
        ),
        array (
            "master"    => "label",
            "field"     => "media_file_name"
        ),
        array (
            "type"      => "varchar",
            "length"    => 1000,
            "field"     => "media_folder_name"
        ),
        array (
            "master"     => "create_time"
        )
    ),
    
    "media_library_applications" => array (
        array (
            "master"    => "id",
            "type"      => "int"
        ),
        array (
            "field"     => "type",
            "type"      => "char",
            "length"    => 20
        ),
        array (
            "field"     => "media_library_id",
            "type"      => "int"
        ),
        array (
            "field"     => "applicant_table",
            "type"      => "varchar",
            "length"    => 100   
        ),
        array (
            "field"     => "applicant_id",
            "type"      => "int"
        ),
        array (
            "field"     => "applicant_folder",
            "type"      => "varchar",
            "length"    => 500  
        ),
        array (
            "field"     => "ordering",
            "type"      => "smallint"
        )
    ),
    
    "media_library_application_details" => array (
        array (
            "master"    => "id"
        ),
        array (
            "type"      => "int",
            "field"     => "media_library_application_id"
        ),
        array (
            "type"      => "text",
            "field"     => "value", 
        )    
    ),
);