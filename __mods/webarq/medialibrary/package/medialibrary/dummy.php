<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array (
    
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "medialibrary",
            "ordering"      => "total-row",
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "Media Library",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations" => array (
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary'",
            "label"         => "image-library",
            "permalink"     => "image",
            "ordering"      => 1,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary'",
            "label"         => "video-library",
            "permalink"     => "video",
            "ordering"      => 2,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary'",
            "label"         => "document-library",
            "permalink"     => "document",
            "ordering"      => 3,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary'",
            "label"         => "media-library-configuration",
            "permalink"     => "configuration",
            "ordering"      => 4,
            "create_time"   => "now"
        )
    ),
    
    "module_navigation_group_maps" => array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='image-library' AND `permalink` = 'image' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='video-library' AND `permalink` = 'video' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='document-library' AND `permalink` = 'document' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='media-library-configuration' AND `permalink` = 'configuration' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        )
    ),
    
    "module_permissions" => array (
        //Image Permission
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "upload-media-image",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "browse-media-image",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "delete-media-image",
            "create_time"   => "now"                    
        ),
        
        
        //Video Permission
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "upload-media-video",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "browse-media-video",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "delete-media-video",
            "create_time"   => "now"                    
        ),
        
        
        //Doc Permission
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "upload-media-doc",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "browse-media-doc",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "delete-media-doc",
            "create_time"   => "now"                    
        ),
        
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1",
            "permission"    => "media-library-configuration",
            "create_time"   => "now"                    
        ),
    ),
    
    "module_navigation_permission_maps"  => array (
        //Navigation Media Image  
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='image-library' AND `permalink` = 'image' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'upload-media-image' LIMIT 1",
            "create_time"   => "now" 
        ),    
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='image-library' AND `permalink` = 'image' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'browse-media-image' LIMIT 1",
            "create_time"   => "now" 
        ),    
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='image-library' AND `permalink` = 'image' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'delete-media-image' LIMIT 1",
            "create_time"   => "now" 
        ),    
        
        //Navigation Media Video
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='video-library' AND `permalink` = 'video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'upload-media-video' LIMIT 1",
            "create_time"   => "now" 
        ),      
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='video-library' AND `permalink` = 'video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'browse-media-video' LIMIT 1",
            "create_time"   => "now" 
        ),      
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='video-library' AND `permalink` = 'video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'delete-media-video' LIMIT 1",
            "create_time"   => "now" 
        ),      
        
        //Navigation Media Doc
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='document-library' AND `permalink` = 'document' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'upload-media-doc' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='document-library' AND `permalink` = 'document' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'browse-media-doc' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='document-library' AND `permalink` = 'document' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'delete-media-doc' LIMIT 1",
            "create_time"   => "now" 
        ),        
        
        //Navigation configuration        
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='media-library-configuration' AND `permalink` = 'configuration' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'medialibrary' LIMIT 1) AND `permission` = 'media-library-configuration' LIMIT 1",
            "create_time"   => "now" 
        ),
    )
);