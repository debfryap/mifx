<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) (Image) 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Medialibrary_Cms_Image extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        App::$config->menu->active = 'image-library';
        
        $this->meta('title',__('image-library'));
        
        $this->media_header();        
        
        $this->authorise('medialibrary',array('upload-media-image','browse-media-image','delete-media-image'));
    }
    
    public function action_index() {
        $title = __('image-library');
        $this
            //Title
            ->register('title')
                ->use_string($title)
            
            //Content
            ->register('content')
                ->use_widget('medialibrary/cms')
                    ->data('title',$title)
                    ->set('type','image');        
    }
}