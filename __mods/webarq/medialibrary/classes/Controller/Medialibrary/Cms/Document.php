<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) (Document) 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Medialibrary_Cms_Document extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        App::$config->menu->active = 'document-library';
        
        $this->meta('title',__('document-library'));
        
        $this->media_header();
        
        $this->authorise('medialibrary',array('upload-media-doc','browse-media-doc','delete-media-doc'));
    }
    
    public function action_index() {
        $title = __('document-library');
        $this
            //Title
            ->register('title')
                ->use_string($title)
            
            //Content
            ->register('content')
                ->use_widget('medialibrary/cms')
                    ->data('title',$title)
                    ->set('type','doc');        
    }
}