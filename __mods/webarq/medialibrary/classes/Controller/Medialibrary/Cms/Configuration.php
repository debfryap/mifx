<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) (Video) 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Medialibrary_Cms_Configuration extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        App::$config->menu->active = 'media-library-configuration';
        
        $this->meta('title',__('media-library-configuration'));
        
        $this->media_header();
        
        $this->authorise('medialibrary',array('media-library-configuration'));
    }
    
    public function action_index() {
        // Validation
        $this->media_validate();
        
        // Content set
        $title = __('media-library-configuration');
        $this
            //Title
            ->register('title')
                ->use_string($title)
            
            //Content
            ->register('content')
                ->use_widget('medialibrary/configuration')
                    ->data('title',$title);        
    }
    
    public function action_update() {
        if (empty($this->post)) {         
            $this->register('title')->use_string('Error')
                 ->register('content')->use_string(__('no-data-post'))
                 ;        
            return true;
        }        
        
        $array_configuration = (array (
                    'thumb_width'       => $this->post->thumb_width,
                    'thumb_height'      => $this->post->thumb_height,
                    'replace_same_file' => isset($this->post->replace_same_file) ? true : false,
                    'extension'         => array (
                        'image' => Helper_Kernel::string_to_array($this->post->allowed_image),
                        'doc'   => Helper_Kernel::string_to_array($this->post->allowed_doc),
                        'video' => Helper_Kernel::string_to_array($this->post->allowed_video),
                    )
                ));
                 
        $string_json = json_encode($array_configuration);
        
        $update = Model::factory('Dba')->update('configurations')
                             ->set("value",$string_json)
                             ->set("update_time",date('Y-m-d H:i:s'))
                             ->where("id","=",Medialibrary::$config->id)
                             ->and_where("key = medialibrary")
                             ->execute();
                             
        if (!empty($update)) {  
            //Session message
            App::$session->set("transaction_message",__('success.transaction',array(
                ':object'=>'Configurations Medialibrary')));
                    
            History::record(array(
                'row_id'      => Medialibrary::$config->id,
                'table'       => 'configurations',
                'actor'       => User::admin()->username,
                'action'      => 'update'
            ));
        }
        $this->deflect("medialibrary/configuration");
    }
}