<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) (Document) 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Medialibrary_Cms_Ajax extends Controller_Default_Template_Ajaxcms {
    
    public function before() {
        parent::before();
    }
    
    public function action_new_media_folder() {
        $result = 'false';
        
        if (!empty($this->post->parent) && !empty($this->post->name) && !empty($this->post->type)) {
            $next_dir = $this->post->parent.'/'.$this->post->name;
            if (is_dir($this->post->parent) && !is_dir($next_dir)) {
                if (mkdir($next_dir)) {
                    //check for thumbnails folder
                    $thumb_folder = trim(str_replace(Medialibrary::$_path,'',$this->post->parent));
                    $first_images = strpos($thumb_folder, 'images'); 
                    
                    if ($this->post->type == 'image' 
                            && !is_dir(Medialibrary::$_thumb_path.'/'.$thumb_folder.'/'.$this->post->name)) {
                        if (mkdir(Medialibrary::$_thumb_path.'/'.$thumb_folder.'/'.$this->post->name)) {
                            //Auto create index file :)
                            if ($handler = fopen(Medialibrary::$_thumb_path.'/'.$thumb_folder.'/'.$this->post->name."index.html","w+")) {
                                fwrite($handler, '<html><title>No direct access</title><body>No direct access allowed</body></html>');
                            };
                        };
                    }
                    $result = 'true';
                }
            }
        }
        
        $this->content = $result;    
    }
    
    public function action_listing_file() {
        if (User::authorise('medialibrary'
                ,array('upload-media-'.$this->post->type,'browse-media-'.$this->post->type,'delete-media-'.$this->post->type))) {
            $html = 'NULL';
            if (!empty($this->post->dir) && !empty($this->post->type)) {
                $dir  = $this->post->dir;
                $html = View::factory('default/medialibrary/cms/list-files',array(
                            'path'  => $dir,
                            'type'  => $this->post->type,
                            'files' => Medialibrary::list_file($this->post->type,$dir)
                        ));
            }
            $this->content = $html;   
            return;
        }
        $this->content = __('error.401'); 
    }
    
    public function action_file_detail() {
        if (empty($this->post->file))  {
            $this->content = 'false'; return;
        }   
        
        $file = json_decode($this->post->file);
        $html = View::factory('default/medialibrary/cms/file-detail',array(
                    'file' => $file,
                    'uses' => Model::factory('Medialibrary')->media_used($file->media_id)
                )); 
        
        $this->content = $html;
    }
    
    public function action_browser_media() {
        if (User::authorise('medialibrary','browse-media-'.$this->post->type)) {
                    
            if (empty($this->post) || empty($this->post->type) || empty($this->post->name)) {
                $this->content = 'false'; return;
            }            
            
            $type = $this->post->type;
            
            $this->content = Widget::load('medialibrary/cms')
                                    ->set('type',$type)
                                    ->set('name',$this->post->name)
                                    ->data('title',__("$type-library"))
                                    ->data('browserMedia',true);
        } else {
            $this->content = __('error.401'); 
        }
    }
    
    public function action_delete_file() {
        if (User::authorise('medialibrary','delete-media-'.$this->post->type)) {
            if (!empty($this->post)) {        
                $is_used = Model::factory('Medialibrary')
                                   ->media_used($this->post->media_id,false)
                                   ->limit(1)
                                   ->execute()
                                   ->current();
                if (empty($is_used)) {
                    $delete = Model::factory('Medialibrary')->media_delete($this->post->media_id,$this->post->path);  
                    $this->content = 'true';  
                    return;
                }
            }
        }
                
        $this->content = 'false';
    }
}