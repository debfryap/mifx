<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) (Video) 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Medialibrary_Cms_Video extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        App::$config->menu->active = 'video-library';
        
        $this->meta('title',__('video-library'));
        
        $this->media_header();
        
        //Check for authorisation
        $this->authorise('medialibrary',array('upload-media-video','browse-media-video','delete-media-video'));
    }
    
    public function action_index() {
        $title = __('video-library');
        $this
            //Title
            ->register('title')
                ->use_string($title)
            
            //Content
            ->register('content')
                ->use_widget('medialibrary/cms')
                    ->data('title',$title)
                    ->set('type','video');        
    }
}