<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Ajax Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Medialibrary_Cms_Uploadify extends Controller{
    
    private $path;
    
    private $type;
    
    private $ext;
    
    public function before() {
        parent::before();
        
        $cookie = App::cookie('webarqcms');
        
        $post   = $this->request->post(); 
        
        $cookie = !empty($post['cookie']) ? $post['cookie'] : $cookie;
        
        list($root,$app,$salt,$session) = explode('::',$cookie,4);
                        
        App::$config->salt->encrypted =  $salt;
         
        User::current($cookie);
        
        if ((empty(User::admin()->is_login) || empty(User::admin()->is_admin)) 
                || empty($post['path']) || empty($post['type'])
                && ($this->request->controller() != 'auth' && $this->request->controller() != 'Auth')) {
            die(__('error.403'));
        }
        
        //Mapping role permission
        User::role_permission();
        
        $this->path = $post['path'];
        
        $this->type = $type = $post['type'];
        
        $this->ext  = Medialibrary::$config->extension->$type;//Helper_Kernel::object_to_array(Medialibrary::$config->extension->$type);
         
        //Get param language
        I18n::lang($this->request->param('lang'));
        if (!empty(App::$module->language))
            Language::$current = I18n::$lang;
    }
    
    public function action_index() {
        $this->response->body('Index');    
    }
    
    
    public function action_upload_file() {
        $html = 'false';
        
        if (!User::authorise('medialibrary','upload-media-'.$this->type)) {
            $this->response->body($html); return;    
        }
        
        $file = $_FILES['fileSelect'];
        
        //Validation file
        if (
            ! Upload::not_empty($file) ||
            ! Upload::valid($file) ||
            ! Upload::type($file,$this->ext))
        {            
            $this->response->body($html); return;
        }
        
        $file_name   = $file['name'];
        
        $system_name = Helper_File::system_name($file_name);
        
        $only_name   = Helper_File::name($system_name);
        
        $file_ext    = Helper_File::type($file_name);
        
        
        
        //Check for exist file
        if (file_exists("$this->path/$system_name")) {
            if (!(Medialibrary::$config->replace_same_file)) {
                $temp_file_id = 0;
                            
                $temp_prefix_name = '';
                
                while ( file_exists($save_true = ("$this->path/$only_name$temp_prefix_name.$file_ext"))){
                    $temp_file_id++;
                    $temp_prefix_name = "-$temp_file_id";
                }                
                $only_name = empty($temp_file_id) 
                                ? $only_name 
                                : "$only_name-$temp_file_id";
            } else {
                $this->response->body(__('file-exist',array(':name'=>$file_name)));
                return;       
            }    
        }  
        
        $final_name = "$only_name.$file_ext";        
        
        //If file uploaded
        if ($file = Upload::save($file, $final_name, $this->path)) {            
            //If image, create thumbnail
            if ($this->type == 'image') {
                $thumb_path = str_replace(Medialibrary::$_path,Medialibrary::$_thumb_path,$this->path);
                $resize = Picture::resize(array(
                    'src'    => "$this->path/$final_name",
                    'dest'   => "$thumb_path/",
                    'name'   => "$only_name",
                    'width'  => Medialibrary::$config->thumb_width,
                    'height' => Medialibrary::$config->thumb_height
                ));
            } 
              
            //Record data file            
            $row = json_decode(json_encode(array (
                        "id" => "auto",
                        "type" => $this->type,
                        "media_file_name" => $final_name,
                        "media_folder_name" => $this->path,
                        "create_time" => "now"
                 )));
            Installer::ignore('media_libraries',$row,'medialibrary',User::admin()->username);   
                          
            $html = 'true'; 
        }
        
        $this->response->body($html);
    }
}