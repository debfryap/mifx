<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Handler Class 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Kohana_Picture extends Populate{    
    
    /**
     * @var array allowed extension
     */  
    protected $ext = array ('jpg','jpeg','gif','png');
    
    /**
     * @var string full path image source (include file name)
     */
    protected $src;
    
    /**
     * @var bool delete image source
     */
    protected $delete_src = false;   
    
    /**
     * @var string destination folder
     */    
    protected $dest;
    
    /**
     * @var int resize width
     */    
    protected $width;
    
    /**
     * @var int resize height
     */    
    protected $height; 
    
    /**
     * @var string new file name
     */    
    protected $name;
    
    /**
     * @var int offset image x
     */
    protected $offset_x = null;
    
    /**
     * @var int offset image y
     */
    protected $offset_y = null;
    
    /**
     * @var array RGB (Red, Green, Blue) Color value
     */
    protected $rgb;
    
    /**
     * @var boolean (do not)replace existing image in destination
     */    
    protected $replace = true;
    
    /**
     * @var string class job
     */
    protected $_job;     
    
    /**
     * @var boolean keep image in full size before resize/crop
     */
    protected $_full_image= false;
    
    /**
     * @var boolean force to use new file, if file already exists will replace with a very new file
     */
    protected $use_new_file = true;
    
    /**
     * @var object image source factory
     */    
    private $_original;
    
    /**
     * @var int original width
     */    
    private $_original_width;
    
    /**
     * @var int original width
     */    
    private $_original_height;
    
    private $_save_path;
    
    public static function slice(array $configs = array()) {
        $configs['_job'] = 'slice';
        
        $x = new Picture($configs);
        return $x->_save_path;
    }
    
    public static function resize(array $configs = array()) {
        $configs['_job'] = 'resize'; 
        
        $x = new Picture($configs);
        return $x->_save_path;
    }
    
    public static function stretch(array $configs = array()) {
        $configs['_job'] = 'stretch';
        
        $x = new Picture($configs);
        return $x->_save_path;
    }
            
    public function __construct(array $configs = array()) {
        parent::__construct($configs);    
        
        $this->_original = Image::factory($this->src);
        
        $this->_original_width = $this->_original->width;
        
        $this->_original_height = $this->_original->height; 
        
        $this->name_checking();                 //Checking file name
        
        if ($this->use_new_file === false && file_exists($this->_save_path)) { //
            return;
        }
        
        if ($this->_original_width != $this->width || $this->_original_height != $this->height) {
            $method = $this->_job."_method";            
            $this->$method();                       //Proceeding method
        } else {
            $this->_original
                 //Save with new name
                 ->save($this->_save_path);
        }
        
        if (!empty($this->delete_src) && file_exists($this->src)) {
            unlink($this->src);
        }
    }
    
    private function name_checking() {
        if (empty($this->name)) {
            $this->name = Helper_File::type($this->src,'/');
        }
        
        $ext = Helper_File::type($this->name,'.');  
              
        if (empty($ext)) {
            $ext = Helper_File::type(Helper_File::type($this->src,'/'));
            $temp_name  = $this->name;
            $this->name = "$this->name.$ext";
            
            if (file_exists($this->dest.$this->name) && $this->replace === false) {
                $this->name =  "$this->name(".time().").$ext";   
            }
            
            if ($this->replace === true) {
                foreach ($this->ext as $ext) {
                    $target_file = $this->dest.$temp_name.".".$ext;
                    if (file_exists($target_file)) {
                        unlink($target_file);
                    }    
                }
            }
        }      
        
        $this->_save_path = "$this->dest/$this->name";  
    }
    
    private function stretch_method() {
        $w = empty($this->width) ? $this->_original->width : $this->width;
        $h = empty($this->height) ? $this->_original->height : $this->height;
        $this->_original->resize($w,$h,Image::NONE)->save($this->_save_path);    
    }
    
    private function slice_method() {
        if (empty($this->width)) {
            $this->_original->resize(null,$this->height)->save($this->_save_path);
        } elseif (empty($this->height)) {
            $this->_original->resize($this->width,null)->save($this->_save_path);
        } else {
            $this->_original->crop($this->width,$this->height,$this->offset_x,$this->offset_y)->save($this->_save_path);
            if ($this->_original_width < $this->width || $this->_original_height < $this->height) {
                $this->resampling();            
            }
        }
    }
    
    
    
    private function resize_method() {
        $im = $this->_original;         //Original Image    
        $ow = $this->_original_width;   //Original width
        $oh = $this->_original_height;  //Original height
        $nw = $this->width;             //New width
        $nh = $this->height;            //New height
        $rw = $rh = null;               //Resize width and height
        
        if (empty($nw) || empty($nh)) {
            $im->resize($nw,$nh)->save($this->_save_path);  
        } else {  
            $scw = $ow/$nw;     //Width  scale
            $sch = $oh/$nh;     //Height sclae
            $sw  = ($scw);      //Width  after scaling round($scw);
            $sh  = ($sch);      //Height after scaling round($sch);
            
            
            if ($sw <> $sh) {
                if ($ow > $oh) {
                    $after_resize_width  = round($ow*($nh/$oh));
                    $after_resize_height = round($oh*($nh/$oh));
                } else {
                    $after_resize_width  = round($ow*($nw/$ow));
                    $after_resize_height = round($oh*($nw/$ow)); 
                }
                
                
                if ($after_resize_width < $nw || $after_resize_height < $nh) {
                    if ($after_resize_width < $nw) {
                        $im->resize($nw,null);                                
                    } else  {                                
                        $im->resize(null,$nh);
                    }  
                } else {
                    if ($after_resize_width == $nw)
                        $im->resize($nw,null);
                    elseif ($after_resize_height == $nh)
                        $im->resize(null,$nh);
                    else
                        $im->resize($nw,null);
                }
            }                                                         
            $im
                //Crop with actual dimension
                ->crop($nw,$nh,$this->offset_x,$this->offset_y)
                //Save with new name
                 ->save($this->_save_path);
        }
    } 
    
    /**
     * Resampling image, fix it with new dimension
     */    
    private function resampling() {
        if (!empty($this->width) && !empty($this->height)) {
            //Image extension
            $ext = strtolower(Helper_File::type($this->name));
            
            //Create our canvas
            $canvas = imagecreatetruecolor($this->width, $this->height);
            
            //RGB split, default is white rgb
            list($red,$green,$blue) = empty($this->rgb) ? array(255,255,255) : $this->rgb;
            
            //Alpha transparency
            if (empty($this->rgb)) {
                imagealphablending($canvas, false);
                imagesavealpha($canvas,true);
                $background = imagecolorallocatealpha($canvas,$red,$green,$blue,127);
            } else {
                $background = imagecolorallocate($canvas, $red, $green, $blue);    
            }  

            //Fill canvas background
            imagefilledrectangle($canvas,0,0,$this->width,$this->height,$background); 
            
            //Destination file
            $dest_file = $this->dest.$this->name;
                        
            //Open temporary image
            if ($ext === 'jpg' || $ext === 'jpeg') {
                $temp = imagecreatefromjpeg($dest_file);
            } elseif ($ext === 'gif') {
                $temp = imagecreatefromgif($dest_file);
            } elseif ($ext === 'png') {
                $temp = imagecreatefrompng($dest_file);
            }
            
            // Count destination x and y point
            $dest_x = ($this->width-$this->_original_width)/2;
            $dest_y = ($this->height-$this->_original_height)/2;
            
            // Merge image to canvas
            imagecopymerge($canvas, $temp, $dest_x, $dest_y, 0, 0, $this->_original_width, $this->_original_height,100);
            
            // Save the image
            if ($ext === 'jpg' || $ext === 'jpeg') {
                imagejpeg($canvas,$dest_file);
            } elseif ($ext === 'gif') {
                imagegif($canvas,$dest_file);
            } elseif ($ext === 'png') {
                imagepng($canvas,$dest_file);
            }            
            
            // Free up memories
            imagedestroy($canvas);
        }
    }
}