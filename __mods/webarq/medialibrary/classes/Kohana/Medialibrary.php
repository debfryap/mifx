<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Media Handler Class 
 * @Module      Media Library
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Kohana_Medialibrary extends Populate {
    /**
     * @var string relative media root folder 
     * Do not change once you set application development environment to production
     */
    public static $_path = 'media/kcfinder';
    
    public static $_thumb_path = 'media/kcfinder/.thumbs';
    
    public static $image_type = array('jpg','gif','png','gif','bmp');
    
    protected $type = 'image';
    
    protected $active_path = null;
    
    public static $config;
    
    public static function instance() {
        return new Medialibrary();
    } 
    
    public static function install($config) {
        $default = isset($config->default) ? $config->default : null;
        if (!isset($default)) {
            $default = Kohana::$config->load('medialibrary')->default;
        }
                
        if (!empty($default['media_library_application_types'])) {
            if (!empty($default['media_library_application_types']['image'])) {
                foreach ($default['media_library_application_types']['image'] as $name => $attr) {
                    Helper_File::create_folder(Inflector::plural($name),URL::mediaImage(true));
                }
            }
        }
    }
    
    public static function get_folder($type,$name) {
        if (isset(Medialibrary::$config->media_library_application_types->$type->$name)) {
            return $type == 'image'
                         ? URL::mediaImage(Inflector::plural($name),true)
                         : Medialibrary::$_path."/".Inflector::plural($type);
        }
    }
    
    public static function list_folder($type,$return = 'html') {
        $media = new Medialibrary(array('type'=>$type));
        
        if ($return == 'basic') {
            return $media->basic_scan($media->active_path);                    
        }                
        
        if (is_dir($media->active_path)) {
            $child = $media->html_scan($media->active_path);
            
            if (!empty(Medialibrary::$config->extension->$type)) {
                $data_extension = null;
                foreach (Medialibrary::$config->extension->$type as $ext) {
                    $data_extension .= !isset($data_extension) ? "*.$ext" : ";*.$ext";
                }
            }
                                
            $html  = '<div class="folder top" data-media-type="'.$type.'" data-media-ext="'.$data_extension.'">';
            //$html .= '<a href="mediadir:'.$tmp.'">';
            $html .= '<span class="tree ';
            
            if(!empty($child))
                $html .= ' open';
                    
            $html .= '">&nbsp;</span>';
            $html .= '<span class="name current" data-dir="'.$media->active_path.'">'.Inflector::plural($media->type).'</span>';
            //$html .= '</a>';
            $html .= !empty($child) ? $child : '';
            $html .= '</div>';    
        } else {
            $html = 'Invalid Directory';
        }
        
        return $html;
    }
    
    public static function list_file($type,$path=null) {
        $media = new Medialibrary(array('type'=>$type));
        
        $path  = empty($path) ? $media->active_path : $path;
         
        if (!is_dir($path)) {
            return null;                 
        }
        
        return Model::factory('Medialibrary')->files_used($type,$path);
    }
    
    public static function file_info($full_file_path,$short=15) {
        
        //Nothing to do if file even not exist in this world :)
        if (!file_exists($full_file_path)) return null;
        
        //Read file information
        $info['name'] = Helper_File::type($full_file_path,'/');
        
        $info['ext']  = strtolower(Helper_File::type($info['name']));
            
        $info['path']     = URL::root($full_file_path);
        $info['rel_path'] = $full_file_path;
        
        //Image information
        if (in_array($info['ext'],Medialibrary::$image_type)) {
            $image_size = getimagesize($full_file_path);
            $info['mime']     = $image_size['mime'];
            $info['width']    = $image_size[0];
            $info['height']   = $image_size[1];
            
            $full_thumb_file = str_replace(Medialibrary::$_path,Medialibrary::$_thumb_path,$full_file_path);
            if (file_exists($full_thumb_file)) {
                $info['thumb'] = URL::root($full_thumb_file);
                $info['thumb_width']  = Medialibrary::$config->thumb_width;
                $info['thumb_height'] = Medialibrary::$config->thumb_height;
            }
        }
        
        $info['short_name'] = substr($info['name'],0,$short);
                
        $info['byte'] = filesize($full_file_path);
        
        return $info;
    }
    
    /**
     * @var string transaction type : create or update
     * @var int    applicant table id (row_id)
     * @var array  post media
     * @var bool   debugging 
     */
    public static function media_application_transaction($action,$applicant_id,$post,$debug) {
        //Media applicant table
        $media_table = $post['table'];
        
        //Tmp transaction
        $tmp = array();
         
        unset($post['table']);
                
        $applicant_status = Model::factory('Dba')
                            ->select()
                            ->query("SHOW TABLE STATUS LIKE 'media_library_applications'")
                            ->execute()
                            ->current();
                            
        $index_create = isset($applicant_status->Auto_increment) ? $applicant_status->Auto_increment : 1;
        
        if ($action == 'update' && isset($post['current_media_applicant'])) {
            $current_media_applicants = $post['current_media_applicant'];
            
            foreach ($current_media_applicants as $applicants) {                
                $not_applicants = isset($not_applicants)
                                        ? $not_applicants+$applicants
                                        : $applicants;    
            }
            
            unset($post['current_media_applicant']);    
        }
        
        if (!empty($post)) {
            foreach ($post as $name => $rows) {
                //Un label
                if ($action == 'update') {
                    if (isset($current_media_applicants[$name])) {
                        $previous_applications = $current_media_applicants[$name];
                        
                        $flip_array            = array_flip($previous_applications);
                    }
                }
                
                //Media order
                $media_order = 1;
                
                foreach ($rows as $row_id => $row_value) {            
                    //Media type
                    $media_type = $row_value['type'];
                    
                    //Applicant folder
                    $applicant_folder = Medialibrary::get_folder($media_type,$name);
                        
                    if ($action == 'create' || !isset($previous_applications) || (isset($previous_applications) && !in_array($row_id,$previous_applications))) {
                        
                        //Insert application
                        $tmp[$index_create]['main'] = Model::factory('Dba')
                                        ->insert('media_library_applications')
                                        ->row("type",$name)
                                        ->row("media_library_id",$row_id)
                                        ->row("applicant_table",$media_table)
                                        ->row("applicant_id",$applicant_id)
                                        ->row("applicant_folder",$applicant_folder)
                                        ->row("ordering",$media_order);
                        
                        //Insert application detail
                        if (isset($row_value['detail'])) {
                            $tmp[$index_create]['detail'] = Model::factory('Dba')
                                        ->insert('media_library_application_details')
                                        ->row('media_library_application_id',$index_create)
                                        ->row('value',json_encode($row_value['detail']));
                        }
                        
                        if ($debug === false && !empty($row_value['rel_path']) && $media_type == 'image') {
                            $img_rsz  = Picture::resize(array(
                                        'src'    => $row_value['rel_path'],
                                        'dest'   => $applicant_folder,
                                        'width'  => isset(Medialibrary::$config->media_library_application_types->$media_type->$name->width)
                                                        ? Medialibrary::$config->media_library_application_types->$media_type->$name->width
                                                        : null,
                                        'height' => isset(Medialibrary::$config->media_library_application_types->$media_type->$name->height)
                                                        ? Medialibrary::$config->media_library_application_types->$media_type->$name->height
                                                        : null,
                                    ));      
                        }
                        
                        $index_create++;
                    } else {  
                        if (!isset($index_update))
                            $index_update = 1;
                            
                        //Application row id
                        $applicant_row_id = $flip_array[$row_id]; 
                        
                        //Update application
                        $tmp[$index_update]['main'] = Model::factory('Dba')
                                        ->update('media_library_applications')
                                        ->set("ordering",$media_order)
                                        ->where("id = $applicant_row_id");
                                        /**
                                        ->where("type = $name")
                                        ->and_where("media_library_id = $row_id")
                                        ->and_where("applicant_table = $media_table")
                                        ->and_where("applicant_id = $applicant_id")
                                        **/
                                        ;
                                        
                        //Update application detail
                        if (isset($row_value['detail'])) {
                            //Check if detail has been entered before
                            //die(Model::factory('Dba')->select('id')->from('media_library_application_details')->where('media_library_application_id','=',$applicant_row_id));
                            $checking = Model::factory('Dba')->select('id')->from('media_library_application_details')->where('media_library_application_id','=',$applicant_row_id)->execute()->get('id');
                            
                            if (!empty($checking))
                                $tmp[$index_update]['detail'] = Model::factory('Dba')
                                            ->update('media_library_application_details')
                                            ->set('value',json_encode($row_value['detail']))
                                            ->where('media_library_application_id','=',$applicant_row_id);
                            else
                                $tmp[$index_update]['detail'] = Model::factory('Dba')
                                            ->insert('media_library_application_details')
                                            ->row('media_library_application_id',$applicant_row_id)
                                            ->row('value',json_encode($row_value['detail']));
                                                                
                        }
                        
                        //Check for deleted application
                        unset($not_applicants[$applicant_row_id]);
                        
                        $index_update++;
                    }
                    
                    $media_order++; 
                    #echo $row_id,'.................................................<br/>';
                    #echo Debug::vars($row_value);
                }
                
                if ($action == 'update') {
                    unset($previous_applications);
                    
                    unset($flip_array);
                }
            } 
        }
        
        if (!empty($not_applicants)) {
            foreach ($not_applicants as $applicant_row_id => $media_id) {
                if (!isset($index_delete))
                    $index_delete = -1;
                     
                //Delete application
                $tmp[$index_delete]['delete_main'] = Model::factory('Dba')->delete('media_library_applications')
                                                                          ->where("id = $applicant_row_id");
                                                                          
                //Delete application detail
                $tmp[$index_delete]['delete_details'] = Model::factory('Dba')->delete('media_library_application_details')
                                                                          ->where("media_library_application_id = $applicant_row_id");
                $index_delete--;                                                                              
            }
        }
        
        return $tmp;   
    } 
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs); 
        
        $this->active_path = Medialibrary::$_path.'/'.Inflector::plural($this->type);
    }
    
    private function basic_scan($root=null,$level=0) {
        $list_dir = null;
        
        if (!is_dir($root))
            return null;
            
        $open_dir = opendir($root);
        
        if (!$open_dir)
            return null;
        
        $index = 0;
        while (($file = readdir($open_dir)) !== false) {
            if ($file{0} == '.')
                continue;
            
            $tmp = "$root/$file";
                                             
            if (is_dir($tmp)) {
                $list_dir[$index]['name']  = $file;
                $list_dir[$index]['path']  = $tmp; 
                $list_dir[$index]['child'] = $this->basic_scan($tmp,$level+1);
            } else {
                $name = $root;
                if (($last_pos = strrpos($root,'/')) !== false) {
                    $name = substr($name,$last_pos+1);    
                }
                
                $list_dir[$index]['name']  = $name;
                $list_dir[$index]['path']  = $root; 
                $list_dir[$index]['files'][] = $file;
            }
            
            $index++;
        }            
        
        return $list_dir;
    }

    private function html_scan($root=null,$level=0) {
        $html = '';
        
        if (!is_dir($root))
            return null;
            
        $open_dir = opendir($root);
        
        if (!$open_dir)
            return null;
        
        $index = 0;
        while (($file = readdir($open_dir)) !== false) {
            if ($file{0} == '.')
                continue;
            
            $tmp = "$root/$file";
                                 
            if (is_dir($tmp)) {
                
                $child = $this->html_scan($tmp,$level+1);
                
                $html .= '<div class="folder child">';
                //$html .= '<a href="mediadir:'.$tmp.'">';
                $html .= '<span class="tree ';
                
                if(!empty($child))
                    $html .= ' open';
                        
                $html .= '">&nbsp;</span>';
                $html .= '<span class="name" data-dir="'.$tmp.'">'.$file.'</span>';
                //$html .= '</a>';
                $html .= !empty($child) ? $child : '';
                $html .= '</div>';
            } 
            
            $index++;
        }            
        
        return $html;
    }
    
    
    
    /**
     * @param   $configs    array configuration with pair
     *          table       string  applicant table name
     *          item        integer id or object item
     *          type        string  applicant type or array applicant type eg. slide,banner ....
     */
    public  static function append_library(array $configs = array()) {
        extract($configs);
        
        if (empty($item)) return null;
        
        $type = isset($type) ? $type : null;
        
        if (is_numeric($item)) {
            return Model::factory('Medialibrary')->get_application($type,$item,$table);
        }
        
        foreach ($item as $i => $d) {
            $medias = Model::factory('Medialibrary')->get_application($type,$d->id,$table);
            if (!empty($medias)) {
                if (count($medias) == 1) {
                    $d->my_media = $medias->current();
                } else {
                    foreach ($medias as $j => $e) {
                        $d->my_media[$j] = $e;
                    }
                }
                
                $tmp[$i] = $d;    
            }
        }
        
        return $tmp;    
    } 
}