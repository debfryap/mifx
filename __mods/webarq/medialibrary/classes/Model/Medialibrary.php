<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      Medialibrary
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Medialibrary extends Model_Dba {
    
    public function pre_select() {
        return $this->select('ms.*')
                    ->from(array('media_libraries','ms'));
    }
    
    public function get_all() {
        return $this->pre_select()->execute();
    }
    
    public function get_all_by_type($type='image') {
        return $this->pre_select()->where("ms.type = $type")->execute();
    }
    
    public function files_used($type='image', $path=null) {
        $get = $this->pre_select()
                    ->select(DB::expr('COUNT(msa.`media_library_id`) AS `used`'))
                    ->join(array('media_library_applications','msa'),'left')
                        ->on('msa.media_library_id = ms.id')
                    ->where("ms.type = $type")
                    ->group_by('ms.id');
        
        if (isset($path))
            $get->and_where("ms.media_folder_name = $path");
        
        return $get->execute();
    }
    
    public function get_application($type=null,$applicant_id=null,$applicant_table=null) {
        $x = $this->select('msa.*','ms.media_folder_name','ms.media_file_name','ms.type',array('msd.value','detail'))
                    ->from(array('media_library_applications','msa'))
                    ->join(array('media_libraries','ms'),'LEFT')
                        ->on("ms.id = msa.media_library_id")
                    ->join(array('media_library_application_details','msd'),'LEFT')
                        ->on("msd.media_library_application_id = msa.id");
        
        $f = 'where';
        
        if (isset($type)) {
            if (is_array($type)) {
                $x->where_open();
                foreach ($type as $i => $t) {
                    $i == 0 ? $x->where("msa.type = $t") : $x->or_where("msa.type = $t");
                }                    
                $x->where_close();
            } else {
                $x->where("msa.type = $type");
            }
             
            $f = 'and_where';
        }
        
        if (isset($applicant_id)) {
            $x->$f("msa.applicant_id = $applicant_id"); $f = 'and_where';    
        }
        
        if ($applicant_table) {
            $x->$f("msa.applicant_table = $applicant_table"); 
        }
        $x->order_by('msa.ordering');
        
        return $x->execute();    
    }
    
    
    /**
     * @return folder,file,detail
     */
    public function simple_application($type=null,$applicant_id=null,$applicant_table=null) {
        $x = $this->select(array('msa.applicant_folder','folder'),array('ms.media_file_name','file'),array('msd.value','detail'))
                    ->from(array('media_library_applications','msa'))
                    ->join(array('media_libraries','ms'),'LEFT')
                        ->on("ms.id = msa.media_library_id")
                    ->join(array('media_library_application_details','msd'),'LEFT')
                        ->on("msd.media_library_application_id = msa.id");
        
        $f = 'where';
        
        if (isset($type)) {
            $x->$f("msa.type = $type"); $f = 'and_where';
        }
        
        if (isset($applicant_id)) {
            $x->$f("msa.applicant_id = $applicant_id"); $f = 'and_where';    
        }
        
        if ($applicant_table) {
            $x->$f("msa.applicant_table = $applicant_table"); 
        }
        $x->order_by('msa.ordering');
        
        return $x->execute();    
    }
    
    public function media_used($media_id,$execute=true) {
        $x = $this->select("ms.id","msa.type","msa.applicant_table","msa.applicant_id",
                                DB::expr("COUNT(`ms`.`id`) AS `used`"))
                    ->from(array("media_libraries","ms"))
                    ->join(array("media_library_applications","msa"),"RIGHT")->on("msa.media_library_id = ms.id")
                    ->where("ms.id = $media_id")
                    ->group_by("msa.type","msa.applicant_table");
                    
        return $execute === true ?$x->execute() : $x;
    }
    
    public function media_delete($media_id,$media_path=null) {
        $delete = $this->delete('media_libraries')
                       ->where("id = $media_id")
                       ->execute();
        if (isset($media_path) && file_exists($media_path)) {
            unlink($media_path);
        }
        return $delete;
    }
    
    public function media_applicant($table,$field,$id) {
        return $this->select($field)->from($table)->limit(1)->where('id','=',$id)->execute()->get($field);
    }
    
    public function applicant_delete($applicant_table,$applicant_id = null) {
        //Check for table first
        $isTable = DB::query(Database::SELECT,"SHOW TABLES LIKE 'media_library_applications'")->execute()->current();
        
        //Mean media library not loading yet
        if (empty(Medialibrary::$config) && empty($isTable)) return null;
        
        //For cleansheet delete purpose, we must delete details to @first time offcourse
        $deleteDetailIn = DB::select('id')->from('media_library_applications')->where("applicant_table","=",$applicant_table);
        
        if (isset($applicant_id)) $deleteDetailIn->and_where("applicant_id","=",$applicant_id);
        
        $deleteDetail   = DB::delete('media_library_application_details')
                                ->where('media_library_application_id','IN',$deleteDetailIn)
                                ->execute();
        
        //Preparing for delete                
        $query = DB::delete('media_library_applications')->where("applicant_table","=",$applicant_table);
        
        //Conditional delete
        if (isset($applicant_id)) $query->and_where("applicant_id","=",$applicant_id);
        
        $query->execute();
    }
}