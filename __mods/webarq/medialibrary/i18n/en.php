<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Medialibrary
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'medialibrary'                  => 'Media Library',
    'image-library'                 => 'Image Library',
    'video-library'                 => 'Video Library',
    'document-library'              => 'Document Library',
    'no-available-media'            => 'No :type files available',
    'file-exist'                    => 'File :name already exist at server',
    'media-library-configuration'   => 'Media Library Configuration',
    'upload-media'                  => 'Upload Media',
    'browse-media'                  => 'Browse Media',
    'delete-media'                  => 'Delete Media',
    'thumb-width'                   => 'Image Thumbnail Width', 
    'thumb-height'                  => 'Image Thumbnail Height',
    'replace-same-file'             => 'Override file with same name',
    'allowed-media'                 => 'Allowed :media extension',
    'image-slider'                  => 'Image Slider',
    'banner-header'                 => 'Banner Header',
    'video'                         => 'Video',
    'document'                      => 'Document',
    'media-used-in'                 => 'Used :number :times in :location',
    'media-unused'                  => 'This :type not currently in use',
    'media-file-usage'              => 'Media usage detail : ',
    'unlink'                        => 'Unlink',
);