<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Medialibrary
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'medialibrary'                  => 'Media Library',
    'image-library'                 => 'Image Library',
    'video-library'                 => 'Video Library',
    'document-library'              => 'Document Library',
    'no-available-media'            => 'No :type files available',
    'file-exist'                    => 'File :name already exist at server',
    'media-library-configuration'   => 'Media Library Configuration',
    'upload-media-image'            => 'Upload Media Image',
    'browse-media-image'            => 'Browse Media Image',
    'delete-media-image'            => 'Delete Media Image',
    'upload-media-video'            => 'Upload Media Video',
    'browse-media-video'            => 'Browse Media Video',
    'delete-media-video'            => 'Delete Media Video',
    'upload-media-doc'              => 'Upload Media Document',
    'browse-media-doc'              => 'Browse Media Document',
    'delete-media-doc'              => 'Delete Media Document',
    'thumb-width'                   => 'Image Thumbnail Width', 
    'thumb-height'                  => 'Image Thumbnail Height',
    'replace-same-file'             => 'Override file with same name',
    'allowed-media'                 => 'Allowed :media extension',
    'banner-header'                 => 'Banner Header',
    'video'                         => 'Video',
    'document'                      => 'Document',
    'media-used-in'                 => 'Used :number :times in :location',
    'media-unused'                  => 'This :type not currently in use',
    'media-file-usage'              => 'Media usage detail : ',
    'unlink'                        => 'Unlink',
);