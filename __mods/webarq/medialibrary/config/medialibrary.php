<?php defined('SYSPATH') or die('No direct script access.');

return array  (
    'default' => array (
        'thumb_width'       => 100,
        'thumb_height'      => 100,
        'replace_same_file' => true,
        'extension'         => array (
            'image'         => array ('jpg','jpeg','png','gif'),
            'doc'           => array ('doc','txt','pdf'),
            'video'         => array ('flv','mp4')
        ),
        'nstt' => array(
                    'nstt',
                    'replace_same_file',
                    'media_library_application_types'
            )
    )
);            