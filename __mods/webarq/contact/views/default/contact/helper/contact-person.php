<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Contact Field ~ CMS
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>
<div class="dynamic-element contact-person">
    <div class="block_left label" style="width: 120px;"><?php echo __('name'); ?></div>
    <div class="center block_separator">:</div>
    <div class="block_right">
        <div>
            <?php 
                $name = isset($name) ? $name : '';
                echo Form::input("office[contact_person][$i][name]",$name,array('class'=>'required'));  ?>
        </div>
        <div class="error_information"></div>
    </div>
    <div class="line-break clear_left">&nbsp;</div>
    <div class="block_left label" style="width: 120px;"><?php echo __('phone'); ?></div>
    <div class="center block_separator">:</div>
    <div class="block_right">
        <div>
            <?php 
                $phone = isset($phone) ? $phone : '';
                echo Form::input("office[contact_person][$i][phone]",$phone); ?>
        </div>
        <div class="error_information"></div>
    </div>    
    <div class="line-break clear_left" style="border-bottom: 1px solid #333;">&nbsp;</div>    
</div>
    
<!-- Last button -->
<?php
    if (!empty($l)): ?>
    <div>
        <div data-target="contact-person" class="dynamic-button add" style="float: left;">+</div> 
        <div data-target="contact-person" class="dynamic-button del" style="float: left;">x</div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.dynamic-button').click(function(){
                    h = $(this);
                    t = $('.dynamic-element.'+h.attr('data-target'))
                    l = t.length;
                    if (h.hasClass('add')) {
                        $.ajax({
                            url     : '<?php echo URL::cms('contact/ajax/contact-person');?>',
                            type    : "POST",
                            data    : {'i':l},
                            success : function(r) {
                                h.parent().before(r);        
                            }    
                        });
                    } else {
                        if (l-1 < 0) return;
                        t.eq(l-1).remove();
                    }    
                });    
            });
        </script>
    </div>        
<?php 
    endif; ?>
<!-- End Last Button -->