<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Office ~ CMS
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    echo Data::form(array(
        'action'    => false,
        'title'     => $title,
        'id'        => 'form_office',
        'multilang' => false,
        'pack'      => array ('contact:'.Request::$initial->param('param1').'-office'),
        'type'      => strtolower(Request::$initial->param('param1')),
        'row_id'    => array('id',Request::$initial->param('param2')),
    ));
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('#get-gmLatLng').click(function(){
            enable_masking_layer(false);
            
            $('#masking-html').html('loading map...');
                        
            center_middle('#masking-html');
            
            setTimeout(function(){
                $.ajax({
                    url     : bs_cms+'contact/ajax/gmap-get-lat-lng',
                    async   : false,
                    success : function(response) {
                        
                        enable_masking_layer(false);
                
                        $('#masking-html').css({
                            width  : 600,
                            height : 300
                        });
                            
                        center_middle('#masking-html');
                        
                        $('#masking-html').append(response);
                        
                        $('#mymap').css({
                            width  : 580,
                            height : 280
                        });
                    }                 
                });
            },10)
        });
    })
</script>