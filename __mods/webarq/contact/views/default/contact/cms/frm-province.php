<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Office ~ CMS
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    echo Data::form(array(
        'action'    => false,
        'title'     => $title,
        'id'        => 'frm-province',
        'multilang' => false,
        'pack'      => array ('contact:'.Request::$initial->param('param1').'-province'),
        'type'      => strtolower(Request::$initial->param('param1')),
        'row_id'    => array('id',Request::$initial->param('param2')),
    ));
?>