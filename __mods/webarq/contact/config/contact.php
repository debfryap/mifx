<?php defined('SYSPATH') or die('No direct script access.');

return array  (
    'default' => array (   
        'office'                            => array (
            'name'                          => 'PT. Web Architect Technology',
            'phone'                         => array(
                                                    array (
                                                        'number' => '+62 21 29308702'
                                                    )
                                               ),
            'fax'                           => array(
                                                    '+62 21 29308701'
                                               ),
            'address'                       => 'Menara Citicon Lt. 15 Suite A Jl. S. Parman Kav 72 Jakarta 11410',
            'email'                         => 'info@webarq.com',
            'slogan'                        => 'We build your online presence',
            'latitude'                      => 0,
            'longitude'                     => 0,
            'contact_person'                => array (
                array (
                    'name'                  => 'Webarq',
                    'phone'                 => '+62 21 29308702'                
                )
            )
        ),    
            
        'field_attributes'                  => array (
            array (
                'label'                     => 'contact-field-label',
                'name'                      => 'label',
                'class'                     => 'required',
                'type'                      => 'text',
                'length'                    => 20,
                'multilang'                 => true,
                'referrer'                  => 'name'
            ),
            array (
                'label'                     => 'contact-field-name',
                'name'                      => 'name',
                'class'                     => 'required name',
                'type'                      => 'text',
                'data-separator'            => '_'
            ),
            array (
                'label'                     => 'contact-field-type',
                'name'                      => 'type',
                'type'                      => 'select'
            ),
            array (
                'label'                     => 'contact-field-rule',
                'name'                      => 'rule',
                'type'                      => 'select',
                'multiple'                  => 'multiple',
                'style'                     => 'min-height:80px;'
            ),
            array (
                'label'                     => 'contact-field-message',
                'name'                      => 'message',
                'type'                      => 'text',
                'multilang'                 => true
            )
        ),
        'field_attribute_showed'            => array ('contact-field-label','contact-field-name','contact-field-rule','contact-field-message'),          
        'field_rules'                       => array ('required','email','numeric'),  
        'field_allowed_types'               => array ('text','textarea'),
        
        //Allow visitor send an email through contact form 
        //@default will use contact office email
        //@dynamic configuration
        'send_to_email'                     => true,
        
        //Save to database submitted contact form       @dynamic configuration        
        'save_to_database'                  => false,
        
        //Enable branch office                          @dynamic configuration
        'enable_branch_office'              => false,      
        
        //Branch office type @set to null if not used
        'type'                              => array (
            'marketing_office'              => 'Marketing Office',
            'warehouse'                     => 'Warehouse'      
        ),
        //Comment this line to enable branch office type
        'type'                              => null,          
        
        //Enable province
        'use_province'                      => true,
        
        //Enable city
        'use_city'                          => false,
        
        //Enable contact person for both contact configuration and branch office 
        'use_contact_person'                => false,
        
        //Enable google map locator
        'google_map'                        => false,
        
        //System mail from address @use for form
        'system_mail_from'                  => 'system@webarq.com',
        
        //Not save to table
        'nstt'                 => array(
                    'field_attributes',
                    'field_attribute_showed',
                    'field_attribute_showed',
                    'field_rules',
                    'field_allowed_types',
                    'use_contact_person',
                    'google_map',
                    'type',
                    'use_province',
                    'use_city',
                    'use_contact_person',
                    'system_mail_from',
                    'nstt',
            )
    )
);            