<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Contact_Configuration extends Populate {
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
    }
    
    public function data() {
        $tabs = null;
        
        if (User::authorise('contact','manage-contact-configuration')) {
            $tabs['general'] = array (
                'id'    => 'ctc-general',
                'label' => 'General',
                'class' => 'active'
            );
        }
        
        if (User::authorise('contact',array('create-element-contact-field','edit-element-contact-field','delete-element-contact-field'))) {
            $tabs['field'] = array (
                'id'    => 'ctc-field',
                'label' => __('contact-field')
            );
        }
        return array (
            'config'  => Contact::$config,
            'tabs'    => $tabs,
        );
        
    } 
}

?>