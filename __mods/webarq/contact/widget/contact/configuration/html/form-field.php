<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

$config = Contact::$config;
$values = null;
$attributes   = $config->field_attributes;

$field_types  = Helper_Kernel::object_to_array($config->field_allowed_types);
$attributes[2]->option = array_combine($field_types,$field_types);

$field_rules  = Helper_Kernel::object_to_array($config->field_rules);
$attributes[3]->option = array_combine($field_rules,$field_rules);



if (isset($id) && isset($config->field_contacts[$id])) {
    //disable referrer label
    $attributes[0]->referrer = false;
    
    $current = $config->field_contacts[$id];
    $values  = Helper_Kernel::object_to_array($current);
    
    foreach ($attributes as $attribute) {
            
        $attr_name = $attribute->name;
        
        if (empty($attribute->multilang)) {            
            if ($attr_name == 'rule') {      
                if (isset($values[$attr_name])) {
                    if (!empty(App::$module->language))
                        $values[$attr_name][Language::$default] = array_combine($values[$attr_name],$values[$attr_name]);
                    else
                        $values[$attr_name] = array_combine($values[$attr_name],$values[$attr_name]);
                } else {
                    if (!empty(App::$module->language))
                        $values[$attr_name][Language::$default] = null;
                    else
                        $values[$attr_name] = null;
                } 
                    
                if (isset($values['option']))
                    $selOption = $values['option'];
            } else {
                $tmp = $values[$attr_name];
                unset($values[$attr_name]);
                if (!empty(App::$module->language))
                    $values[$attr_name][Language::$default] = $tmp;
                else
                    $values[$attr_name] = $tmp;
            }
        } else {
            if (empty(App::$module->language)) 
                $values[$attr_name] = is_array($values[$attr_name]) ? array_shift($values[$attr_name]) : $values[$attr_name];   
            elseif (!is_array($values[$attr_name])) {
                $tmp = $values[$attr_name];
                unset($values[$attr_name]);
                $values[$attr_name][Language::$default] = $tmp;
            }     
        }
    }
    
    $action .= "/$id";
} 

echo Data::form(array(
            'action'    => $action,
            'title'     => $title,
            'id'        => 'form-manage-field',
            'cancel'    => false,
            'elements'  => $attributes,
            'values'    => $values
         ));
?>

<script type="text/javascript">

    function addSelOption(idx,value,label) {
        x  = '<div class="itemSelOption" id="iSn'+idx+'">';
        x += '<?php if (!empty(App::$module->language)) echo '<div class="tab-content lang lang-',Language::$default,'" id="lang-',Language::$default,'">'; ?>';
        x += 'Value : <input type="text" name="option['+idx+'][value]" class="required"';
        if (typeof value !== 'undefined') x+= ' value="'+value+'"';
        x += '/>';
        x += ' Label : <input type="text" name="option['+idx+'][label]" class="required"';
        if (typeof label !== 'undefined') x+= ' value="'+label+'"';
        x += '/> ';
        x += '<?php if (!empty(App::$module->language)) echo '</div>'; ?>';
        x += '</div>';
        
        return x;
    }
        
    function htmlSelOption(i) {
        thisk = $('select.form-field[name="type"]');
        html  = '<div class="line-break clear_left">&nbsp;</div> \
                 <div class="label block_left">Field Option</div> \
                 <div class="center block_separator">:</div> \
                 <div class=" block_name block_right">';
                 
        if (typeof i == 'undefined')
            html += addSelOption(0)+addSelOption(1);
        else
            html += i;
            
        html += '<div style="float:right;margin-top:5px;background-color:#fff;padding:5px 5px 0;border-radius:3px;-moz-border-radius:3px;line-height:18px;"> \
                        <span class="removeLast"></span> <span class="addLast"></span></div>';
        html += '</div><div style="clear:left;height:1px;">&nbsp;</div>';
        
        selOption = $('<div/>',{'class':'selOption','id':'selOption'}); 
        selOption.html(html);                               
        thisk_parent = thisk.parents('.block_right').after(selOption);            
        
        $('.removeLast').unbind('click').bind('click',function(){
            if ($('div.itemSelOption').length > 2)
                $(this).parent('div').prev('div.itemSelOption').remove();
        });               
        
        $('.addLast').unbind('click').bind('click',function(){
            $(this).parent('div').before(addSelOption($('div.itemSelOption').length+1));
        });    
    }
    
    $(document).ready(function(){
        //Replacing with ajax
        $('#form-manage-field').data("validator").settings.submitHandler = function (form) {
            
            thform = $('#form-manage-field');
                                                
            $('#masking-html').html('Please wait for a while ...');
            
            center_middle('#masking-html');
            
            setTimeout(function(){
                $.ajax({
                    url     : thform.attr('action'),
                    type    : "POST",
                    data    : thform.serialize(),
                    async   : false,
                    success : function(response) {   
                        //$('#masking-html').html(response); return false;
                        if ($.trim(response) == 'true') {
                            setTimeout(function(){
                                $('.tabular#tab-content-field').load(bs_cms+'contact/ajax/reload-contact-field',function(){
                                    disable_masking_layer();                                    
                                });
                            },10);
                        } else {
                            disable_masking_layer();      
                        }
                        
                    }
                }); 
            },10);
            
            return false;
        }
        
        $('select.form-field[name="type"]').change(function(){
            thisk = $(this);
            
            if (thisk.val() == 'select') {
                htmlSelOption();
            }  else {
                $('#selOption.selOption').remove();   
            }   
            center_middle('#masking-html');
        });
        
        <?php
            if (isset($selOption)) {
                echo "selOption = ",json_encode($selOption),";";
                echo "if (typeof selOption !== 'undefined'){opt = '';$.each(selOption,function(i,val){opt += addSelOption(i,val.value,val.label);});htmlSelOption(opt);}";
            }
        ?>
    });
</script>