<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

if (!empty($config->field_contacts)) {
    $config = Helper_Kernel::object_to_array($config);
    
    $fields = $config['field_contacts'];
} 
?>
<?php 
    if (User::authorise('contact','create-element-contact-field')) 
        echo '<div class="createField" id="createField">Add Field</div>'; ?>
    
<div class="tabular" style="margin-top:10px;" id="tab-content-field">
    
    <div class="header">
        <?php
            if (!isset($fields)) 
            {
                echo '<div class="cell no-data">#######</div>';
            } 
            else 
            {
                foreach ($config['field_attribute_showed'] as $name)
                {
                    echo '<div class="cell ',$name,'">',__("$name"),'</div>';
                } 
                echo '<div class="cell action job2">',__("action"),'</div>';
            }
        ?>
    </div>
    
    <div class="row-group">
        <?php 
            if (!isset($fields))
            {
                echo '<div class="row red">'
                            ,'<div class="cell center no-data">'
                                ,__('error.no-data')
                                    ,'</div></div>'; 
            }
            else {
                foreach ($fields as $index => $field) {
                    echo '<div class="row sortable-area" data-list-ordering="index-',$index,'">';
                        foreach ($config['field_attributes'] as $attributes) {
                            if (in_array($attributes['label'],$config['field_attribute_showed'])) {
                                
                                $attr_name = str_replace('contact-field-','',$attributes['label']);
                                
                                echo '<div class="cell ',$attributes['name'],'">';
                                if (isset($field[$attr_name])) {
                                    if (is_array($field[$attr_name])) {
                                        if ($attr_name == 'label' || $attr_name == 'message') {
                                            echo is_array($field[$attr_name])
                                                    ? (!empty(App::$module->language)
                                                        ? $field[$attr_name][Language::$default]
                                                        : array_shift($field[$attr_name]))
                                                    : $field[$attr_name];
                                        } elseif ($attr_name == 'rule') {
                                            echo implode(", ",$field[$attr_name]);    
                                        }
                                    } else {
                                        echo $field[$attr_name];
                                    }
                                }
                                echo '</div>'; 
                                
                            } 
                        } 
                        
                        echo '<div class="cell action">';
                            
                            if (User::authorise('contact','edit-element-contact-field'))
                                echo '<a ',
                                        'class="icon edit" title="Click to edit this item" ',
                                        'href="',URL::cms(),'contact/ajax/create-edit-field/',$index,'"></a>';
                            
                            if (User::authorise('contact','delete-element-contact-field'))
                                echo '<a ',
                                        'class="icon delete" title="Click to delete this item" ',
                                        'href="',URL::cms(),'contact/ajax/delete-contact-field/',$index,'"></a>';
                        echo '</div>';
                    
                    echo '</div>';
                }
            }
        ?>
    </div>
</div>