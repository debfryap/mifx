<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  


echo Widget::load('station/form')->data(array('print' => false,'validation'=>true)); 

?>

<div id="widget-contact-configuration">
    <div id="station-form-wrapper">
        
        <div id="content-form">
        
            <div class="title-form">Contact Configuration</div>
                          
            <div id="content">
                
                <?php 
                    if (isset($tabs) && count($tabs) > 1) { 
                        echo '<div class="tab-selector" id="common">';
                            echo '<ul id="lang-tab">';
                                foreach ($tabs as $tab) {
                                    echo '<li id="',$tab['id'],'" class="',(isset($tab['class']) ? $tab['class'] : ''),'"><span>',$tab['label'],'</span></li>';
                                }
                                echo '<div class="clear"></div>';
                            echo '</ul>';
                        echo '</div>';        
                    } ?>
                
                
                <?php if (isset($tabs['general'])): ?>
                    <div class="tab-content common" id="common-ctc-general">
                        <?php
                            $tab = Kohana::find_file('widget/'.$_path.'/html/','tab-content-general');
                            if (!empty($tab)) {
                                echo Widget::capture($tab,array(
                                        'config'  => $config 
                                    ));
                            }
                        ?>
                    </div>
                <?php endif; ?>
                
                <?php if (isset($tabs['field'])): ?>
                    <div class="tab-content common" id="common-ctc-field">
                        <?php
                            $tab = Kohana::find_file('widget/'.$_path.'/html/','tab-content-field');
                            if (!empty($tab)) {
                                echo Widget::capture($tab,array(
                                        'config'  => $config
                                    ));
                            }
                        ?>
                    </div>
                <?php endif; ?>
            </div> 
        </div>
    </div>
</div>