<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

?>
<?php 
    echo !empty(App::$config->message->transaction) 
        ? '<div id="transaction_message">'.App::$config->message->transaction.'</div>' 
        : ''; 
    
    $config = Contact::$config;?>
  
<form 
    method="post" 
    class="main-editor"
    id="form-contact-configuration"
    action="<?php echo URL::cms();?>contact/configuration/update/">
    
    <div>
        <div class="block_left label"><?php echo __('config-office-name'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[name]',$config->office->name,array(
                                'id'=>'office-name','class'=>'office-name required'));
                ?>
            </div>
            <div class="error_information"></div>
            <!-- <div class="element-information">Permalink should be use character, numbers and dash only'</div> -->
        </div>
        <div class="line-break clear_left">&nbsp;</div>            
        
        <div class="block_left label"><?php echo __('config-office-email'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[email]',$config->office->email,array(
                                'id'=>'office-email','class'=>'office-email required email'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                
        
        <div class="block_left label">BBJ/FX Email</div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[email_bbj]',isset($config->office->email_bbj) ? $config->office->email_bbj : '',array(
                                'id'=>'office-email-bbj','class'=>'office-email-bbj required email'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>               
        
        <div class="block_left label">BKDI/ICDX Email</div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[email_bkdi]',isset($config->office->email_bkdi) ? $config->office->email_bkdi : '',array(
                                'id'=>'office-email-bkdi','class'=>'office-email-bkdi required email'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                
        
        <div class="block_left label">Seminar Education Email</div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[seminar_education_email]',isset($config->office->seminar_education_email) ? $config->office->seminar_education_email : '',array(
                                'id'=>'office-email-seminar-education-email','class'=>'office-email-bbj required email'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                    
        
        <div class="block_left label">Seminar Corporate Email</div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[seminar_corporate_email]',isset($config->office->seminar_corporate_email) ? $config->office->seminar_corporate_email : '',array(
                                'id'=>'office-email-seminar-corporate-email','class'=>'office-email-seminar-corporate-email required email'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>             
        
        <div class="block_left label"><?php echo __('config-office-phone'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    if (!empty($config->office->phone)) {
                        $phone = '';
                        
                        foreach ($config->office->phone as $i => $item) {
                            $phone .= $i > 0 ? ', ' : '';
                            $phone .= $item->number;
                            if (isset($item->ext)) $phone .= ':'.$item->ext;
                        }
                    }
                    //$phone = is_array($config->office->phone) 
//                                ? implode(', ',$config->office->phone) : $config->office->phone;
                    echo Form::input('office[phone]',$phone,array(
                                'id'=>'office-phone','class'=>'office-phone required'));
                ?>
            </div>
            <div class="error_information"></div>
            <div class="element-information">
                Use comma to separate each phone, and colon to enable extension if any
                <br />
                &nbsp;
                Eg. xxx-xxxxxxx:abc, aaa-aaaaaaa this mean xxx-xxxxxxx have abc as extension, while aaa-aaaaaaa does not
                have one.
            </div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                 
        
        <div class="block_left label"><?php echo __('config-office-fax'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    $fax = is_array($config->office->fax) 
                                ? implode(', ',$config->office->fax) : $config->office->fax;
                    echo Form::input('office[fax]',$fax,array(
                                'id'=>'office-fax','class'=>'office-fax'));
                ?>
            </div>
            <div class="error_information"></div>
            <div class="element-information">Use comma to separate each fax, eg. xxx-xxxxxxx, aaa-aaaaaaa</div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                       
        
        <div class="block_left label">Slogan</div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    $slogan = isset($config->office->slogan) 
                                ? $config->office->slogan : '';
                    echo Form::input('office[slogan]',$slogan);
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                       
        
        <div class="block_left label"><?php echo __('config-office-address'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    $class = User::admin()->role_id == 1 ? 'simplex-editor' : 'simple-editor';
                    echo Form::textarea('office[address]',$config->office->address,array(
                                'id'=>'office-address','class'=>$class.' office-address required'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                  
        
        <?php if (Contact::$config->use_contact_person === true): ?>
        <div class="block_left label"><?php echo __('contact-person'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    if (!empty($config->office->contact_person)) {
                        foreach ($config->office->contact_person as $i => $person) {
                            $info      = Helper_Kernel::object_to_array($person);
                            $info['i'] = $i;
                            $info['l'] = $i+1 == count($config->office->contact_person);
                            echo View::factory('default/contact/helper/contact-person',$info);
                        } 
                    } else {
                        echo View::factory('default/contact/helper/contact-person',array('i'=>0,'l'=>true));
                    } ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>           
        <?php endif; ?>      
        
        
        <?php if (Contact::$config->google_map): ?>
        <div class="block_left label"><?php echo __('map-latitude'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[latitude]',$config->office->latitude,array(
                                'id'=>'map_latitude','class'=>'office-latitude required'));
                ?>
            </div>
            <div class="error_information"></div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>                  
        
        <div class="block_left label"><?php echo __('map-longitude'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div id="lang-id" class="tab-content lang lang-id">
                <?php
                    echo Form::input('office[longitude]',$config->office->longitude,array(
                                'id'=>'map_longitude','class'=>'office-longitude required'));
                ?>
            </div>
            <div class="error_information"></div>
            <div class="element-information">Please click in <span id="get-gmLatLng" style="border-bottom:1px solid #333;font-size:120%;cursor:pointer;">here</span> to get your google map latitude and longitude</div>
        </div>
        <div class="line-break clear_left">&nbsp;</div>    
        <?php endif; ?>
        <div class="block_left label"><?php echo __('contact-form-handling'); ?></div>
        <div class="center block_separator">:</div>
        <div class="block_right">
            <div>
                <div style="margin-top:5px;">
                    <input type="checkbox" name="send_to_email" <?php echo $config->send_to_email === true ? 'checked="checked"' : ''; ?>/>
                </div> 
                <div style="float:left;margin: -14px 0 0 19px;">
                    <?php echo __('send-to-email'); ?>
                </div>
                <div class="clear_left">&nbsp;</div>
                
                <div style="margin-top:-3px;">
                    <input type="checkbox" name="save_to_database" <?php echo $config->save_to_database === true ? 'checked="checked"' : ''; ?>/>
                </div> 
                <div style="float:left;margin: -14px 0 0 19px;">
                    <?php echo __('save-to-database'); ?>
                </div>
                <div class="clear_left">&nbsp;</div>
                
                <!--<?php if (User::admin()->id == 1) { ?>
                <div style="margin-top:-3px;">
                    <input type="checkbox" name="enable_branch_office" <?php echo $config->enable_branch_office === true ? 'checked="checked"' : ''; ?>/>
                </div> 
                <div style="float:left;margin: -14px 0 0 19px;">
                    <?php 
                            echo __('enable-branch-office');  ?>
                </div>
                <div class="clear_left">&nbsp;</div>
                <?php } ?>-->
            </div>
        </div>
        
        <div class="line-break clear_left">&nbsp;</div>
        <div class="center no_label">&nbsp;</div>
        <div class=" block_right submit-form"><span>Submit</span></div>
        <div class="break8 clear_left">&nbsp;</div>            
    </div>                                           
</form>