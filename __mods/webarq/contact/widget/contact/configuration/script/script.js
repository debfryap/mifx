$(document).ready(function(){
    /** 
        * CONTACT GENERAL ****
    **/
    $('input[type="checkbox"][name="save_to_database"]').change(function () {    
        thisk = $(this);
        if (thisk.attr("checked")) {            
            return true;         
        }        
        jConfirm('Are you sure to do this? Please be note, all related data/table will be deleted and could not be restored'
                ,'Warning !!!'
                ,function(act){
                    if (act === true) {
                        return true;
                    }
                    thisk.attr("checked",true);
                });
    });
    
    $('input[type="checkbox"][name="enable_branch_office"]').change(function () {    
        thisk = $(this);
        if (thisk.attr("checked")) {            
            return true;         
        }
        jConfirm('Are you sure to do this? Please be note, all related data/table will be deleted and could not be restored'
                ,'Warning !!!'
                ,function(act){
                    if (act === true) {
                        return true;    
                    }
                    thisk.attr("checked",true);
                });
    });
    
    
    /** 
        * CONTACT FIELD ****
    **/
    
    //Create contact field
    $('#createField').click(function(){
        enable_masking_layer('');
        
        $('#masking-html').css('opacity',1).html('Please wait for a while ...'); center_middle('#masking-html');
        
        setTimeout(function(){
            $.ajax({
                url     : bs_cms+'contact/ajax/create-edit-field',
                type    : "POST",
                data    : {'action' : 'create'},
                async   : false,
                success : function(response) {                    
                    enable_masking_layer(false);
                    $('#masking-html').css('opacity',0).addClass('fieldConfig').append(response);
                    center_middle('#masking-html');
                    
                }
            }); 
        },10);
    });
    
    //Edit contact field
    $('body').on('click','a.icon.edit',function(e){
        e.preventDefault();
        
        enable_masking_layer('');
        
        $('#masking-html').css('opacity',1).html('Please wait for a while ...'); center_middle('#masking-html');
        
        thisk = $(this);
        
        setTimeout(function(){
            $.ajax({
                url     : thisk.attr('href'),
                type    : "POST",
                data    : {'action' : 'edit'},
                async   : false,
                success : function(response) {
                    if ($.trim(response) == 'false')
                    {
                        disable_masking_layer();
                        jAlert('You do not have any permission. Please contact your administrator');
                        return false;
                    }
                    enable_masking_layer(false);
                    $('#masking-html').css('opacity',0).addClass('fieldConfig').append(response);
                    center_middle('#masking-html');
                }
            }); 
        },10);
        
        return false;        
    });
    
    
    //Delete contact field
    $('body').on('click','a.icon.delete',function(e){
        e.preventDefault();
        
        thisk = $(this);
        
        jConfirm('Are you sure that you want to permanently deleting the selected database objects?','Warning',function(r){
            if (r) {
                
                enable_masking_layer('');
                
                $('#masking-html').html('Please wait for a while ...');
                
                center_middle('#masking-html');
                
                setTimeout(function(){
                    $.ajax({
                        url     : thisk.attr('href'),
                        type    : "POST",
                        data    : {'action' : 'edit'},
                        async   : false,
                        success : function(response) {
                            if ($.trim(response) == 'true') {
                                setTimeout(function(){
                                    $('#tab-content-field').load(bs_cms+'contact/ajax/reload-contact-field',function(){
                                        disable_masking_layer();                                    
                                    });
                                },10);
                            } else {
                                disable_masking_layer();      
                            }
                            
                        }
                    }); 
                },10);
            }    
        });
        
        
        return false;        
    });
    
    $("#tab-content-field .row-group").sortable({
        connectWith: ".sortable-area",                                            
        opacity: 0.6, 
        cursor: "move", 
        update: function(event, ui) {
            
            sorting  = $(this).sortable("serialize",{attribute:"data-list-ordering", key: "sorting"});
            
            nous_process_message("update");
            
            $.ajax({
                url     : bs_cms+'contact/ajax/reordering-contact-field',
                type    : "POST",
                data    : {"sorting":sorting},
                success : function(response) {
                    $('#tab-content-field').load(bs_cms+'contact/ajax/reload-contact-field',function(){
                        $("#nous-waiting-layering").remove();                                   
                    });
                }
            });
        }
    }); 
    
    $('#get-gmLatLng').click(function(){
            enable_masking_layer(false);
            
            $('#masking-html').html('loading map...');
                        
            center_middle('#masking-html');
            
            setTimeout(function(){
                $.ajax({
                    url     : bs_cms+'contact/ajax/gmap-get-lat-lng',
                    async   : false,
                    success : function(response) {
                        
                        enable_masking_layer(false);
                
                        $('#masking-html').css({
                            width  : 600,
                            height : 300
                        });
                            
                        center_middle('#masking-html');
                        
                        $('#masking-html').append(response);
                        
                        $('#mymap').css({
                            width  : 580,
                            height : 280
                        });
                    }                 
                });
            },10)
        });
});