<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Contact_FormContact {
    
    private $_messages;
    
    public function render() {
        if (!isset(Contact::$config->field_contacts))
            return __('contact-configuration-error');
            
        $configs = Contact::$config->field_contacts;
        
        $html  = Form::open(URL::front('contact-us/submit'),array(
                    'id' => 'form_contact'));
        
        if (!empty(App::$config->message->transaction)) 
            $html .= '<div id="transaction_message">'.App::$config->message->transaction.'</div>';
        
        $html .= '<div>';
        foreach ($configs as $field) {
            $field = $this->translate($field);
            $html .= '<div class="two-column left">';
            $html .= $field->label;
            $html .= '</div>';
            $html .= '<div class="two-column right">';
            $html .= Data_Form_Html::element(array(
                            'element'   => $field 
                        ));
            $html .= '</div>';
            $html .= '<div class="clear" style="height:1px;line-height:1px;">&nbsp;</div>';
        }
        $html .= '</div>';
        
        $html .= Form::submit('submit','Submit');                    
        $html .= Form::close();
        
        $html .= $this->validate_script();
        
        return $html;                          
        
    }
    
    private function validate_script() {
        
        $html  = '<script type="text/javascript">';
        $html .= '$(document).ready(function(){';
        $html .= '$("form#form_contact").validate();';
        $html .= '});';
        $html .= '</script>';
        
        return $html;     
    }
    
    private function translate($field) {
        $tmp = new stdClass();
        foreach ($field as $key => $value) {
            if (!empty(App::$module->language) && is_object($value)) {
                $langd = Language::$default;
                $langc = Language::$current;
                
                if (isset($value->$langd)) {
                    $value = isset($value->$langc) && trim($value->$langc) != ""
                                ? $value->$langc
                                : $value->$langd;
                }     
            }
            
            if ($key == 'option') {
                foreach ($value as $option)
                    $tmp->option[$option->value] = $option->label; 
                continue;
            }
            
            if ($key == 'rule') {
                $tmp->class = implode(" ",$value);
                continue;
            }
            
            if ($key == 'name') {
                $tmp->name = "ctc[$value]";
                continue;
            }
            
            $tmp->$key = $value;
        }
        return $tmp;
    }
}