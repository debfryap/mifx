<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'contact'                       => 'Contact',
    'contact-field'                 => 'Contact Fields',
    'contact-configuration'         => 'Contact Configuration',
    'contact-office'                => 'Branch Office',
    'manage-office'                 => 'Manage Branch Office',
    'create-office'                 => 'Create Branch Office',
    'edit-office'                   => 'Edit Branch Office',
    'publish-office'                => 'Publish/Un-publish Branch Office',
    'delete-office'                 => 'Delete Office',
    'config-office-name'            => 'Office Name',
    'config-office-type'            => 'Office Type',
    'config-office-email'           => 'Office Email Address',
    'config-office-address'         => 'Office Address',
    'config-office-phone'           => 'Office Phone',
    'config-office-fax'             => 'Office Fax',
    'office-name'                   => 'Office Name',
    'office-type'                   => 'Office Type',
    'office-email'                  => 'Office Email Address',
    'office-address'                => 'Office Address',
    'office-phone'                  => 'Office Phone',
    'office-fax'                    => 'Office Fax',
    'map-latitude'                  => 'Google Map Latitude',
    'map-longitude'                 => 'Google Map Longitude',
    'save-to-database'              => 'Save to database submitted contact form',
    'contact-form-field'            => 'Contact Form Field',
    'contact-form-handling'         => 'Contact Form Handling',
    'send-to-email'                 => 'Send to email',
    'manage-contact-field'          => 'Manage Contact Field',
    'contact-field-label'           => 'Field Label',
    'contact-field-name'            => 'Field Name',
    'contact-field-type'            => 'Field Type',
    'contact-field-rule'            => 'Field Rule',
    'contact-field-message'         => 'Field Error Message',    
    'create-contact-field'          => 'Create New Field',
    'contact-configuration-error'   => 'Error. Please check your contact configuration',
    'manage-contact-configuration'  => 'Manage Configuration',
    'enable-branch-office'          => 'Enable Branch Office',
    'submit-form'                   => 'Submit Form',
    'create-element-contact-field'  => 'Create Field Contact Form',
    'edit-element-contact-field'    => 'Edit Field Contact Form',
    'delete-element-contact-field'  => 'Delete Field Contact Form',
    'view-submit-form'              => 'View Submit Form',
    'contact-person'                => 'Contact Person',
    'province'                      => 'Province',
    'manage-province'               => 'Manage Province',
    'province-name'                 => 'Province Name',
    'create-province'               => 'Create Province',
    'edit-province'                 => 'Edit Province',
    'delete-province'               => 'Delete Province',
    'city'                          => 'City',
    'manage-city'                   => 'Manage City',
    'city-name'                     => 'City Name',
    'create-city'                   => 'Create City',
    'edit-city'                     => 'Edit City',
    'delete-city'                   => 'Delete City',
    'contact_person'                => 'Contact Person',
    'contact_mobile'                => 'Contact Mobile',
    'location'                      => 'Cabang',
);