<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'contact'               => 'Contact',
    'contact-field'         => 'Contact Fields',
    'contact-configuration' => 'Contact Configuration',
    'contact-office'        => 'Office',
    'create-office'         => 'Create Office',
    'edit-office'           => 'Edit Office',
    'publish-office'        => 'Publish/Un-publish Office',
    'delete-office'         => 'Delete Office',
    'office-name'           => 'Office Name',
    'office-type'           => 'Office Type',
    'office-email'          => 'Office Email Address',
    'office-address'        => 'Office Address',
    'office-phone'          => 'Office Phone',
    'office-fax'            => 'Office Fax',
    'map-latitude'          => 'Google Map Latitude',
    'map-longitude'         => 'Google Map Longitude',
    'save-to-database'      => 'Save to database submitted contact form',
    'contact-form-field'    => 'Contact Form Field',
    'contact-form-handling' => 'Contact Form Handling',
    'send-to-email'         => 'Send to email',
    'manage-contact-field'  => 'Manage Contact Field',
    'contact-field-label'   => 'Field Label',
    'contact-field-name'    => 'Field Name',
    'contact-field-type'    => 'Field Type',
    'contact-field-rule'    => 'Field Rule',
    'contact-field-message' => 'Field Error Message',    
    'create-contact-field'  => 'Create New Field',
    
    
    'manage-contact-configuration'  => 'Manage Configuration',
);