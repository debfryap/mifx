<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Dummy data
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$is_google_map = Contact::$config->google_map;

$firstNav      = 1;

$dummy_navigations = array (
    array (
        "id"            => "auto",
        "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact'",
        "label"         => "contact-configuration",
        "permalink"     => "configuration",
        "ordering"      => $firstNav,
        "create_time"   => "now"                     
    )    
);

$dummy_navigation_groups = array (
    array (
        "id"                            => "auto",
        "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-configuration' AND `permalink` = 'configuration' LIMIT 1",
        "module_navigation_group_id"    => 1,
        "create_time"                   => "now"
    )
);

$dummy_permissions = array (
    array (
        "id"            => "auto",
        "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
        "permission"    => "manage-contact-configuration",
        "create_time"   => "now"                    
    ),
       
    //Contact form configuration        
    array (
        "id"            => "auto",
        "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
        "permission"    => "create-element-contact-field",
        "create_time"   => "now"                    
    ),
    array (
        "id"            => "auto",
        "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
        "permission"    => "edit-element-contact-field",
        "create_time"   => "now"                    
    ),
    array (
        "id"            => "auto",
        "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
        "permission"    => "delete-element-contact-field",
        "create_time"   => "now"                    
    )
);

$dummy_navigation_permissions = array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-configuration' AND `permalink` = 'configuration' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'manage-contact-configuration' LIMIT 1",
            "create_time"   => "now" 
        ), 
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-configuration' AND `permalink` = 'configuration' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'create-element-contact-field' LIMIT 1",
            "create_time"   => "now" 
        ), 
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-configuration' AND `permalink` = 'configuration' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'edit-element-contact-field' LIMIT 1",
            "create_time"   => "now" 
        ),       
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-configuration' AND `permalink` = 'configuration' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'delete-element-contact-field' LIMIT 1",
            "create_time"   => "now" 
        )
);

    
if (Contact::$config->save_to_database) {
    
    array_push(
        $dummy_navigations,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact'",
            "label"         => "submit-form",
            "permalink"     => "submit-form",
            "is_publish"    => 1,
            "ordering"      => 2,
            "create_time"   => "now"                     
        )
    );  
    array_push(
        $dummy_navigation_groups,
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='submit-form' AND `permalink` = 'submit-form' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        )
    ); 
    array_push(
        $dummy_permissions,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "view-submit-form",
            "create_time"   => "now"                    
        )
    );
    array_push(
        $dummy_navigation_permissions,
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='submit-form' AND `permalink` = 'submit-form' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'view-submit-form' LIMIT 1",
            "create_time"   => "now" 
        )
    );
}

if (Contact::$config->use_province) {
    
    array_push(
        $dummy_navigations,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact'",
            "label"         => "province",
            "permalink"     => "province",
            "is_publish"    => 1,
            "ordering"      => 3,
            "create_time"   => "now"                     
        )
    );     
    array_push(
        $dummy_navigation_groups,
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='province' AND `permalink` = 'province' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        )
    );  
    array_push(
        $dummy_permissions,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "create-province",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "edit-province",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "delete-province",
            "create_time"   => "now"                    
        )
    );
    array_push(
        $dummy_navigation_permissions,
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='province' AND `permalink` = 'province' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'create-province' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='province' AND `permalink` = 'province' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'edit-province' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='province' AND `permalink` = 'province' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'delete-province' LIMIT 1",
            "create_time"   => "now" 
        )
    );
}

if (Contact::$config->use_city) {
    
    array_push(
        $dummy_navigations,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact'",
            "label"         => "city",
            "permalink"     => "city",
            "is_publish"    => 1,
            "ordering"      => 4,
            "create_time"   => "now"                     
        )
    );     
    array_push(
        $dummy_navigation_groups,
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='city' AND `permalink` = 'city' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        )
    );  
    array_push(
        $dummy_permissions,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "create-city",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "edit-city",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "delete-city",
            "create_time"   => "now"                    
        )
    );
    array_push(
        $dummy_navigation_permissions,
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='city' AND `permalink` = 'city' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'create-city' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='city' AND `permalink` = 'city' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'edit-city' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='city' AND `permalink` = 'city' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'delete-city' LIMIT 1",
            "create_time"   => "now" 
        )
    );
}

if (Contact::$config->enable_branch_office) {
                
    array_push(
        $dummy_navigations,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact'",
            "label"         => "contact-office",
            "permalink"     => "office",
            "is_publish"    => 1,
            "ordering"      => 5,
            "create_time"   => "now"                     
        )
    );   
    array_push(
        $dummy_navigation_groups,
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-office' AND `permalink` = 'office' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        )
    ); 
    array_push(
        $dummy_permissions,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "create-office",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "edit-office",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "publish-office",
            "create_time"   => "now"                    
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1",
            "permission"    => "delete-office",
            "create_time"   => "now"                    
        )
    );
    array_push(
        $dummy_navigation_permissions,
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-office' AND `permalink` = 'office' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'create-office' LIMIT 1",
            "create_time"   => "now" 
        ), 
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-office' AND `permalink` = 'office' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'edit-office' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-office' AND `permalink` = 'office' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'publish-office' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='contact-office' AND `permalink` = 'office' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'contact' LIMIT 1) AND `permission` = 'delete-office' LIMIT 1",
            "create_time"   => "now" 
        )
    );
}

return array (   
    //Do not delete this section 
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "contact",
            "ordering"      => "total-row",
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "Contact Us",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations"                => $dummy_navigations,
    "module_navigation_group_maps"      => $dummy_navigation_groups,    
    "module_permissions"                => $dummy_permissions,
    "module_navigation_permission_maps" => $dummy_navigation_permissions
);

?>