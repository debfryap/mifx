<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$is_superadmin  = empty(User::admin()->id) ? null : (User::admin()->role_id != 1 ? false : true);

$contact_installer = array();

if (Contact::$config->save_to_database) {
    $contact_installer['contact_submit_forms'] = array (
        array (
            'master' => 'id',
            'type'   => 'smallint'
        ),
        array (
            'field'  => 'value',
            'type'   => 'text'
        ),
        array (
            'master' => 'is_something',
            'field'  => 'is_read',
        ),
        array (
            'master' => 'create_time'
        ),
        array (
            "master"    => "update_time"
        ) 
    );
}

if (Contact::$config->use_province) {
    $contact_installer["provinces"] = array (
        array (
            "master" => "id",
            "type"   => "smallint"
        ),
        array (
            "master" => "label",
            "field"  => "name",
            "form"   => array (
                "label"     => __('province'),
                "group"     => array("create-province","edit-province"),
                "minlength" => "3",
                "class"     => "required is_used",
                "referrer"  => "permalink",
            )
        ),
        array (
            "master" => "label",
            "field"  => "permalink",
            "form"   => array (
                "group"       => array('create-province','edit-province'),
                "class"       => "required permalink",
                "minlength"   => "3",
            )
        ),
        array (
            "master"    => "ordering",
            "form"      => array (
                "group"      => array('create-province','edit-province'),
                "type"       => "ordering",
                "class"      => "required",
            )
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    );
}

if (Contact::$config->use_city) {    
    $contact_installer["cities"] = array (
        array (
            "master" => "id",
            "type"   => "mediumint"
        ),
        Contact::$config->use_province
        ? array (
            "field"  => "province_id",
            "type"   => "smallint",
            "form"   => array (
                "label"     => __('province'),
                "group"     => array("create-city","edit-city"),
                "class"     => "required challenge-order",
                "query"     => array (
                    "table"     => "provinces",
                    "value"     => "id",
                    "label"     => "name"
                )
            )
        ) : 
        null,
        array (
            "master" => "label",
            "field"  => "name",
            "form"   => array (
                "label"     => __('city'),
                "group"     => array("create-city","edit-city"),
                "minlength" => "3",
                "class"     => "required is_used",
                "referrer"  => "permalink"
            )
        ),
        array (
            "master" => "label",
            "field"  => "permalink",
            "form"   => array (
                "group"       => array('create-city','edit-city'),
                "class"       => "required permalink",
                "minlength"   => "3",
            )
        ),
        array (
            "master"    => "ordering",
            "form"   => array (
                "group"      => array('create-city','edit-city'),
                "type"       => "ordering",
                "data-field" => Contact::$config->use_province ? "province_id" : null,
                "class"      => "required",
                "order"      => "after is_feature",
                "parenting"  => Contact::$config->use_province ? "province_id" : null
            )
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    );
}



if (Contact::$config->enable_branch_office) {

    $use_google_map = Contact::$config->google_map;
    
    $contact_installer["contact_offices"] = array (
        array (
            "master" => "id",
            "type"   => "tinyint"
        ),
        array (
            "master" => "label",
            "field"  => "name",
            "form"   => array (
                "label"     => __('office-name'),
                "group"     => array("create-office","edit-office"),
                "class"     => "required"
            )
        ),
        isset(Contact::$config->office->type)
        ? array (
            "field"  => "type",
            "type"   => "char",
            "length" => 40,
            "form"   => array (
                "label"     => __('office-type'),
                "group"     => array("create-office","edit-office"),
                "type"      => "select",
                "option"    => Contact::$config->office->type
            )
        )
        : null,
        Contact::$config->use_province
        ? array (
            "field"  => "province_id",
            "type"   => "smallint",
            "form"   => array (
                "label"     => __('province'),
                "group"     => array("create-office","edit-office"),
                "class"     => "required",
                "query"     => array (
                    "table"     => "provinces",
                    "value"     => "id",
                    "label"     => "name",
                    "order"     => true
                )
            )
        )
        : null,
        Contact::$config->use_city
        ? array (
            "field"  => "city_id",
            "type"   => "mediumint",
            "form"   => array (
                "label"     => __('city'),
                "group"     => array("create-office","edit-office"),
                "class"     => "required",
                "query"     => array (
                    "table"     => "cities",
                    "value"     => "id",
                    "label"     => "name"
                )
            )
        )
        : null,
        array (
            "master" => "label",
            "field"  => "url_location",
            "form"   => array (
                "type"      => "hidden",
                "group"     => array("create-office","edit-office"),
                "class"     => "required urlLocation"
            )
        ),
        array (
            "master" => "label",
            "field"  => "email",
            "form"   => array (
                "label"     => __('office-email'),
                "group"     => array("create-office","edit-office"),
                "class"     => "email"
            )
        ),
        array (
            "field"  => "address",
            "type"   => "mediumtext",
            "form"   => array (
                "label"     => __('office-address'),
                "group"     => array("create-office","edit-office"),
                "class"     => "required simple-editor"
            )
        ),
        array (
            "field"  => "phone",
            "type"   => "varchar",
            "length" => 100,
            "form"   => array (
                "label"     => __('office-phone'),
                "group"     => array("create-office","edit-office"),
                "class"     => "multi-phone",
                "info"      => "Separate with comma"
            )
        ),
        array (
            "field"  => "fax",
            "type"   => "char",
            "length" => 13,
            "form"   => array (
                "label"     => __('office-fax'),
                "group"     => array("create-office","edit-office"),
            )
        ),
        /**
        array (
            "master" => "label",
            "field"  => "contact_person",
            "form"   => array (
                "group"     => array("create-office","edit-office")
            )
        ),
        array (
            "master" => "label",
            "type"   => "char",
            "field"  => "contact_mobile",
            "length" => 13,
            "form"   => array (
                "group"     => array("create-office","edit-office"),
                "class"     => "numeric"
            )
        ),
        **/
        
        $use_google_map === true
        ? array (
            "field"  => "map_latitude",
            "type"   => "varchar",
            "length" => 40,
            "form"   => array (
                "label"     => __('map-latitude'),
                "group"     => array("create-office","edit-office"),
                "class"     => "numeric"
            )
        )
        : null,
        
        $use_google_map === true
        ? array (
            "field"  => "map_longitude",
            "type"   => "varchar",
            "length" => 40,
            "form"   => array (
                "label"     => __('map-longitude'),
                "group"     => array("create-office","edit-office"),
                "class"     => "numeric",
                "info"      => "Please click in <span id='get-gmLatLng' style='border-bottom:1px solid #333;font-size:120%;cursor:pointer;'>here</span> to get your google map latitude and longitude"
            )
        )
        : null,
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-office","edit-office"),
                "order" =>"after ordering",
                "default" => 1
            )
        ),
        array (
            "master"    => "ordering",
            "form"      => array (
                "group"      => array('create-office','edit-office'),
                "type"       => "ordering",
                "class"      => "required"
            )
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    );
    
    if (Contact::$config->use_contact_person) {
        $contact_installer["contact_office_persons"] = null;    
    }
}
    
return $contact_installer;

