<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_City extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('city'));
        
        App::$config->menu->active = 'city';
    }
    
    public function action_index() {
        
        if (!Contact::$config->use_city) { $this->authorise(false); return; }
        
        //Check for authorisation
        $this->authorise('contact',array('create-city','edit-city','delete-city'));
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing using model
        $listing = Data::listing('Contact_City');
        
        //Listing header
        $listing->set_header('name',array('label'=>'city-name'));
        
        if (Contact::$config->use_province)
            $listing->set_header('province',array('label'=>'province-name'));        
        
        //Listing tool search by
        $listing->tool('search','name');
        
        //Listing default order by field ordering
        $listing->order_by();
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('contact','create-city')) {
            $listing->create_action(__('create-city'));
        }
        
        if (User::authorise('contact','edit-city')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('contact','delete-city')) {
            
            $href = URL::cms()."helper/ajax/delete/cities/{id}/{name}?ordering={ordering}";
            
            if (Contact::$config->use_province) {
                $href .= '&ordering_parent_field=province_id&ordering_parent_value={province_id}';    
            }
            
            $listing->delete_action(array(
                'href'      => $href,
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-city')) 
            ->register('content')->use_string($listing->render())
                ;   
        
    }
    
    public function action_form() {
        
        if (!Contact::$config->use_city) { $this->authorise(false); return; }
        
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        //$this->media_header();
        
        //Activate ckeditor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-city";
        $this->authorise('contact',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Append js
        $this->scripts(URL::libJs('function/jquery.location'));
        $this->scripts('http://maps.google.com/maps/api/js?sensor=false&shield=a.js');
       
        //Append css
        //$this->styles(URL::libCss('uploadify-3.2/uploadify.css'));
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('contact/cms/frm-city')
                    ->set('title',$title)
                ;   
    }
}