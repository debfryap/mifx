<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_Field extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('contact-field'));
        
        App::$config->menu->active = 'contact-field';
        
         //Check for authorisation
        $this->authorise('contact',array(
                            'create-element-contact-field','edit-element-contact-field','delete-element-contact-field'));
    }
    
    public function action_index() {        
        //Append css styles
        $this->styles(URL::libCss('tabular-css','jquery/themes/jquery.ui.all'));
        $this->scripts(URL::libJs(
                            'jquery/ui/jquery.ui.core.min'
                            ,'jquery/ui/jquery.ui.widget.min'
                            ,'jquery/ui/jquery.ui.mouse.min'
                            ,'jquery/ui/jquery.ui.sortable.min'));
        
        //Open listing
        $listing = Data::listing('Contact_Field');
        
        //Enable listing sortable
        $listing->sortable('contact_field_elements');
        
        //Default ordering
        $listing->order_by('ordering');
        
        //Listing header
        $listing->set_header('label')     
                ->set_header('name')
                ->set_header('type')
                ->set_header('rule')
                ->set_header('rule')
                ;        
        
        //Listing button
        if (User::authorise('contact','create-element-contact-field')) {
            $listing->create_action(__('create-element-contact-field'));
        }
                
        if (User::authorise('contact','edit-element-contact-field')) {
            $listing->edit_action(__('edit-element-contact-field'));
        }
                
        if (User::authorise('contact','delete-element-contact-field')) {
            $listing->delete_action(array(
                    'label' => __('delete-element-contact-field'),
                    'href'  => URL::cms().'helper/ajax/delete/contact_field_elements/{id}/{label}'
                ));            
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-contact-field')) 
            ->register('content')->use_string($listing->render())
                ;   
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Check for authorisation
        $permission = $this->param1."-element-contact-field";
        $this->authorise('contact',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('contact/cms/frm-contact-field')
                    ->set('title',$title)
                ;   
    }
}