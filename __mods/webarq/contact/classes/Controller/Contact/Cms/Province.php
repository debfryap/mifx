<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_Province extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('province'));
        
        App::$config->menu->active = 'province';
    }
    
    public function action_index() {
        
        if (!Contact::$config->use_province) { $this->authorise(false); return; }
        
        //Check for authorisation
        $this->authorise('contact',array('create-province','edit-province','delete-province'));
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing using model
        $listing = Data::listing('Contact_Province');
        
        //Listing header
        $listing->set_header('name',array('label'=>'province-name'));        
        
        //Listing tool search by
        $listing->tool('search','name');
        
        //Listing default order by field ordering
        $listing->order_by();
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('contact','create-province')) {
            $listing->create_action(__('create-province'));
        }
        
        if (User::authorise('contact','edit-province')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('contact','delete-province')) {
            $listing->delete_action(array(
                'href'      => URL::cms()."helper/ajax/delete/provinces/{id}/{name}?ordering={ordering}",
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-province')) 
            ->register('content')->use_string($listing->render())
                ;   
        
    }
    
    public function action_form() {
        
        if (!Contact::$config->use_province) { $this->authorise(false); return; }
        
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        //$this->media_header();
        
        //Activate ckeditor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-province";
        $this->authorise('contact',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Append js
        $this->scripts(URL::libJs('function/jquery.location'));
        $this->scripts('http://maps.google.com/maps/api/js?sensor=false&shield=a.js');
       
        //Append css
        //$this->styles(URL::libCss('uploadify-3.2/uploadify.css'));
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('contact/cms/frm-province')
                    ->set('title',$title)
                ;   
    }
}