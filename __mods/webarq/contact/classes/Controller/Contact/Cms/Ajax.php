<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_Ajax extends Controller_Default_Template_Ajaxcms {
    
    public function before() {
        parent::before();
    }
    
    public function action_gmap_get_lat_lng() {
        $this->content = '<div id="mymap"></div>
                            <script type="text/javascript">
                                    var lat_frm_input = "#map_latitude";
                                    var lng_frm_input = "#map_longitude";
                                    set_map();
                            </script>'; 
    }
    
    
    public function action_create_edit_field() {

        if (empty($this->post->action) || (!User::authorise('contact','create-element-contact-field') && !User::authorise('contact','edit-element-contact-field'))) {            
            $this->content = 'false'; return;
        }
        
        $action = $this->post->action;
        
        $form_action = $action == 'create'
                        ? URL::cms().'contact/ajax/save-contact-field'
                        : URL::cms().'contact/ajax/update-contact-field';
                                                
        $view     = Kohana::find_file('widget/contact/configuration/html/','form-field');
        //$this->content = $view; return;
        $this->content = !empty($view) 
                                ? Widget::capture($view,array(
                                                'action' => $form_action,
                                                'title'  => __($action.'-contact-field'),
                                                'id'     => $this->param1)) 
                                : 'false';
    }
    
    public function action_reload_contact_field() {
        $view     = Kohana::find_file('widget/contact/configuration/html/','tab-content-field-ajax');
        
        $this->content = !empty($view) 
                                ? Widget::capture($view) 
                                : 'false';
    }
    
    public function action_save_contact_field() {
        $content = 'false';
        
        if (!empty($this->post->name)) {
            
            $p = $this->post;
            
            $arrConfig = Helper_Kernel::object_to_array(Contact::$config);
            
            $idxArrConfig = isset($arrConfig['field_contacts']) ? count($arrConfig['field_contacts']) : 0; 
            
            unset($p->app_transaction_multilanguage);
            
            $arrConfig['field_contacts'][$idxArrConfig] = $p; 
            
            //Update configurations
            Model::factory('Dba')
                    ->update('configurations')
                    ->set('value',json_encode($arrConfig))
                    ->set('update_time',date('Y-m-d H:i:s'))
                    ->where("key = contact")
                    ->execute();
            
            $label = is_array($p->label) ? $p->label[Language::$default] : $p->label;
                                
            //Log history
            History::record(array(
                'row_id'      => Contact::$config->id,
                'table'       => 'configuration',
                'actor'       => User::admin()->username,
                'description' => '{"object":"'.$label.'","item":"contact field","action":"add"}'
            ));
            
            $content = 'true';                        
        }
        $this->content = $content;
    }
    
    public function action_update_contact_field() {
        
        $content = 'false';
        
        if (isset($this->param1) && !empty($this->post->name) && User::authorise('contact','edit-element-contact-field')) {
            
            $p = $this->post;
            
            $arrConfig = Helper_Kernel::object_to_array(Contact::$config);
            
            unset($p->app_transaction_multilanguage);
            
            $arrConfig['field_contacts'][$this->param1] = $p; 
            
            //Update configurations
            Model::factory('Dba')
                    ->update('configurations')
                    ->set('value',json_encode($arrConfig))
                    ->set('update_time',date('Y-m-d H:i:s'))
                    ->where("key = contact")
                    ->execute();
            
            $label = is_array($p->label) ? $p->label[Language::$default] : $p->label;
                                
            //Log history
            History::record(array(
                'row_id'      => Contact::$config->id,
                'table'       => 'configuration',
                'actor'       => User::admin()->username,
                'description' => '{"item":"'.$label.'","object":"contact field","action":"update"}'
            ));
            
            $content = 'true';                        
        }
        $this->content = $content;
    }
    
    public function action_delete_contact_field() {
        $this->content = 'false';
        if (isset($this->param1) && User::authorise('contact','delete-element-contact-field')) {
            //Array contact configuration
            $arrConfig = Helper_Kernel::object_to_array(Contact::$config);
            
            //Label field
            $label = !isset($arrConfig['field_contacts'][$this->param1])
                        ? 'unknown'
                        : (is_array($arrConfig['field_contacts'][$this->param1]['label']) 
                                ? $arrConfig['field_contacts'][$this->param1]['label'][Language::$default] 
                                : $arrConfig['field_contacts'][$this->param1]['label']);
            
            //Total contact field
            $total_field = count($arrConfig['field_contacts'])-1;
            
            if ($this->param1 < $total_field) {
                for ($i = $this->param1;$i<$total_field;$i++) {
                    $arrConfig['field_contacts'][$i] = $arrConfig['field_contacts'][$i+1];    
                }
            }
            
            //We just need unset the last field :)
            unset($arrConfig['field_contacts'][$total_field]);
            
            //Update configurations
            Model::factory('Dba')
                    ->update('configurations')
                    ->set('value',json_encode($arrConfig))
                    ->set('update_time',date('Y-m-d H:i:s'))
                    ->where("key = contact")
                    ->execute();
                                
            //Log history
            History::record(array(
                'row_id'      => Contact::$config->id,
                'table'       => 'configuration',
                'actor'       => User::admin()->username,
                'description' => '{"object":"'.$label.'","item":"contact field","action":"delete","todo":"from"}'
            ));
            
            $this->content = 'true';    
        }
    }
    
    public function action_reordering_contact_field() {
        $this->content = 'false';    
        if (!empty($this->post->sorting)) {
            $blank_sorting = str_replace('sorting=','',$this->post->sorting);
            $sorting       = explode("&",$blank_sorting);
            
            //Array contact configuration
            $arrConfig = Helper_Kernel::object_to_array(Contact::$config);
            
            $tmpFields = array();
            
            foreach ($sorting as $new_order => $prev_order) {
                $tmpFields[$new_order] = $arrConfig['field_contacts'][$prev_order];       
            }
            
            //Unset contact field
            unset($arrConfig['field_contacts']);
            
            //Re-set contact field
            $arrConfig['field_contacts'] = $tmpFields;
            
            //Update configurations
            Model::factory('Dba')
                    ->update('configurations')
                    ->set('value',json_encode($arrConfig))
                    ->set('update_time',date('Y-m-d H:i:s'))
                    ->where("key = contact")
                    ->execute();
                                
            //Log history
            History::record(array(
                'row_id'      => Contact::$config->id,
                'table'       => 'configuration',
                'actor'       => User::admin()->username,
                'description' => '{"object":"contact field","action":"update"}'
            ));
            
            $this->content = 'true';
        }
    }
    
    public function action_contact_person() {
        $this->content = isset($this->post->i)
                            ? View::factory('default/contact/helper/contact-person',array('i'=>$this->post->i))
                            : '';
    }
    
    public function action_test_email() {
        Email::factory('Test Email', 'Body Email', 'text/html')
            				->from(Contact::$config->system_mail_from)
            				->to('daniel@webarq.com')
            				->send();    
    }
}