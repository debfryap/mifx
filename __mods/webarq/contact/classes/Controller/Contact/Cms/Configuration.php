<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_Configuration extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
    }
    
    public function action_index() {
        //Activate media validate
        $this->media_validate();
        $this->scripts(URL::libJs('function/jquery.location'));
        $this->scripts('http://maps.google.com/maps/api/js?sensor=false&shield=a.js');
        
        //Active media sortable
        $this->media_sortable();
        
        //Media ckeditor
        $this->media_ckeditor();
        
        $this->meta('title',__('contact-configuration'));
        
        App::$config->menu->active = 'contact-configuration';
        
        //Check for authorisation
        $this->authorise('contact',array(
                            'manage-contact-configuration',
                            'create-element-contact-field','edit-element-contact-field','delete-element-contact-field'));
        
        //Append tabular css styles
        $this->media_tabular();
                
        //Assign to template
        $this
            ->register('title')
                ->use_string(__('contact-configuration')) 
            ->register('content')
                 ->use_widget('contact/configuration')
                    ->set('model','dba')
                    ->data('title',__('contact-configuration'));
    }
    
    public function action_update() {
        $this->authorise('contact',array(
                            'manage-contact-configuration'));
        $html = '';
        
        if (!empty($this->post)) {
            //Get current contact configuration
            $ac = Helper_Kernel::object_to_array(Contact::$config);
            
            //Get rid unwanted configuration from saving to the database
            $uc = Kohana::$config->load('contact')->default;
            $ac = array_diff_key($ac,array_flip($uc['nstt']));
                        
            $ac['send_to_email']        = isset($this->post->send_to_email);
            $ac['save_to_database']     = isset($this->post->save_to_database);
            #$ac['enable_branch_office'] = isset($this->post->enable_branch_office);
            $ac['office']               = $this->post->office;
            
            //Phone processing
            $phones                     = array_filter(explode(',',str_replace(', ',',',$ac['office']['phone'])));            
            foreach ($phones as $i => $phone) {
                $explode = explode(':',$phone,2);
                $tmp[$i]['number'] = $explode[0];
                if (isset($explode[1]))
                    $tmp[$i]['ext']    = $explode[1];
            }
            $ac['office']['phone']  = $tmp;
            
            $ac['office']['fax']    = array_filter(explode(',',str_replace(', ',',',$ac['office']['fax'])));
                                    
            //Update "contact" @configuration table            
            $do_config =  Model::factory('Dba')
                            ->update('configurations')
                            ->set("value",json_encode($ac))
                            ->where("key = contact")
                            ->execute();
                        
            if (!empty($do_config)) {
                
                //Configuration update time 
                Model::factory('Dba')
                            ->update('configurations')
                            ->set("update_time",App::date())
                            ->where("key = contact")
                            ->execute();
                            
                //Session message
                App::$session->set("transaction_message",__('success.transaction',array(
                    ':object'=>'Contact Configuration')));
                    
                History::record(array(
                    'row_id'      => Contact::$config->id,
                    'table'       => 'configurations',
                    'actor'       => User::admin()->username
                ));
            }
        } 
        $this->deflect("contact/configuration?tab=general");
    }
    
    private function create_table_contact_form () {
        $forms = array(
            array (
                'master' => 'id',
                'type'   => 'smallint'
            ),
            array (
                'field'  => 'value',
                'type'   => 'text'
            ),
            array (
                'master' => 'is_something',
                'field'  => 'is_read',
            ),
            array (
                'master' => 'create_time'
            )                     
        );
        
        $mimic_master = Package::shadowing_master_table(json_decode(json_encode($forms)));
        
        return Installer::create('contact_submit_forms',$mimic_master);
    }
    
    private function delete_table_contact_form () {
        return DB::query(Database::INSERT,'DROP TABLE `contact_submit_forms`')->execute();
    }
}