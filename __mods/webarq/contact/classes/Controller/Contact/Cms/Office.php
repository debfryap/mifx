<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_Office extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('contact-office'));
        
        App::$config->menu->active = 'contact-office';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('contact',array('create-office','edit-office','delete-office','publish-office','view-office'));
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing using model
        $listing = Data::listing('Contact_Office');
        
        $listing->tool('search','name');
        
        //Listing header
        $listing->set_header('name',array('label'=>'contact-office')) 
                ->set_header('phone')        
                ->set_header('fax',array('label'=>'Fax'));        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('contact','create-office')) {
            $listing->create_action(__('create-office'));
        }
        
        if (User::authorise('contact','edit-office')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('contact','publish-office')) {
            $listing->status_action(array(
                'href'      => URL::cms()."helper/ajax/{function}/contact_offices/{id}/{name}",
            ));
        }
        
        if (User::authorise('contact','delete-office')) {
            $listing->delete_action(array(
                'href'      => URL::cms()."helper/ajax/delete/contact_offices/{id}/{name}",
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-office')) 
            ->register('content')->use_string($listing->render())
                ;   
        
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        //$this->media_header();
        
        //Activate ckeditor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-office";
        $this->authorise('contact',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Append js
        $this->scripts(URL::libJs('function/jquery.location'));
        $this->scripts('http://maps.google.com/maps/api/js?sensor=false&shield=a.js');
       
        //Append css
        //$this->styles(URL::libCss('uploadify-3.2/uploadify.css'));
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('contact/cms/frm-office')
                    ->set('title',$title)
                ;   
    }
}