<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Cms_SubmitForm extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('submit-form'));
        
        App::$config->menu->active = 'submit-form';
        
         //Check for authorisation
        $this->authorise('contact','view-submit-form');
    }
    
    public function action_index() {  
        //Check for authorisation
        $this->authorise('contact','view-submit-form');
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing using model
        $listing = Data::listing('Contact_Form')->__set('update_time',false);
        
        //Listing header
        $fields   = Contact::$config->field_contacts;        
        foreach ($fields as $key => $field) {
            if ($key >= 2)
                break;
                
            $listing->set_header($field->name);
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-office')) 
            ->register('content')->use_string($listing->render())
                ;   
    }
}