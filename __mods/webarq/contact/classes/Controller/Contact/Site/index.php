<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Front End) 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Contact_Site_Index extends Controller_Default_Template_Site {
    
    public function before() {
        parent::before();
    }
    
    public function action_index() {
        $this->media_validate();
        
        $this->meta('title','Webarq CMS Front View')
             ->register('banner',Widget::load('medialibrary/basic-banner'))
             ->register('content')
                ->use_view('layout/basic-two-column')
                ->set('left',$this->simple_detail())
                ->set('right',Widget::load('contact/form-contact'));
	}
    
    public function action_submit() {
        $html = Debug::vars($this->post);
        
        if (!empty($this->post->ctc)) {
            if (Contact::$config->send_to_email === true) {
                $this->simple_email();
            }
            if (Contact::$config->save_to_database === true) {
                Model::factory('Dba')->insert('contact_submit_forms')
                                     ->row('value',json_encode($this->post->ctc))
                                     ->row('create_time',date('Y-m-d H:i:s'))
                                     ->execute();
            }
            
            //Session message
            App::$session->set("transaction_message","Success. We will contact you as soon as possible");
                
            $this->redirect('contact-us');
        }
    }
    
    private function simple_email() {
        //Email Recipient
        $to  = Contact::$config->office->email;
        
        // subject
        $subject = 'Test Submit Email';
        
        // message
        $message  = '<html><head><title>Test Submit Email</title></head><body>';
        $message .= '<p>Hi <b>'.Contact::$config->office->name.'</b></p>';
        $message .= '<table style="border:none;">';
        
        foreach ($this->post->ctc as $k => $v) 
            $message .= '<tr><td>'.$k.'</td><td>:</td><td>'.$v.'</td></tr>';
            
        
        $message .= '</table></body></html>';
        
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
        $headers .= 'From: Birthday Reminder <birthday@example.com>' . "\r\n";
        $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
        $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
        
        // Mail it
        mail($to, $subject, $message, $headers);
    }
    
    private function simple_detail() {
        $htm  = '<h3 class="title">'.Contact::$config->office->name.'</h3>';
        $htm .= '<p>'.Contact::$config->office->address.'</p>';
        $htm .= '<p>Email '.Contact::$config->office->email.'</p>';
        $htm .= '<p>Phone '.Contact::$config->office->phone.'</p>';
        $htm .= '<p>Fax '.Contact::$config->office->fax.'</p>';
        return $htm;
    }
}