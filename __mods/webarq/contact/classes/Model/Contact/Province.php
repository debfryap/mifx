<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Contact_Province extends Model_Dba {  
    
    protected function pre_select(){
        return $this->select('pr.*')
                    ->from(array('provinces','pr'));
    }  
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('provinces','pr'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
        
        return $model;
    }
    /** End Data listing purpose **/
    
    
    public function getProvince($field = null) {
        $query = $this->select('id','name','permalink')
                    ->from('provinces')
                    ->order_by('ordering');
                    
        
        return $query->execute();
    }
    
    public function getProvinceOffice() {
        $provinces = $this->getProvince();
        
        if ($provinces->valid()) {
            foreach ($provinces as $p) {                
                //Get offices
                $p->offices = Model::factory('Contact_Office')->getBranchOffice(array('province_id',$p->id));
                
                
                $offices[$p->id] = $p;
            }
            
            return $offices;
        }
        
        return null;
    }
}