<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Contact_Office extends Model_Dba {  
    
    protected function pre_select(){
        return $this->select('cof.*',
                                DB::expr("CASE cof.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('contact_offices','cof'));
    }  
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('contact_offices','cof'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
        
        return $model;
    }
    /** End Data listing purpose **/
    
    public function getBranchOffice($field = null,$activeness = true) {
         $get = $this->pre_select()->select(DB::expr("sn.name as provinsi_name"))
                    ->join(array('provinces','sn'))->on('sn.id = cof.province_id');
        
        if (isset($activeness)) {
            $get->where('is_active = ' . ( $activeness === true ? 1 : 0 ) );
        }
        
        if (isset($field)) {
            if (is_array($field)) 
            {
                $whereField = $field[0];
                $whereValue = $field[1];     
            }
            else 
            {
                $whereField = 'id';
                $whereValue = $field;
            }
            if (isset($activeness))
                $get->and_where("$whereField = $whereValue");
            else
                $get->where("$whereField = $whereValue");
        }
        
        return $get->order_by('ordering')->execute();
                
    }
        
}