<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Contact_Field extends Model_Dba {
    // Model table
    protected $table = 'contact_field_elements';
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array($this->table,'cf'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->select()
                      ->from(array($this->table,'cf'));
        
        return $model;
    }
    /** End Data listing purpose **/
}