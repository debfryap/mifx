<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Contact_Form extends Model_Dba {
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('contact_submit_forms','csr'),false);
        
        return $model;
    }
    
    public function tabular($limit=null,$offset=null) {
        $items = $this->select()
                    ->from(array('contact_submit_forms','csr'))
                    ->order_by('csr.id','DESC')
                    ->limit(isset($limit) ? $limit : App::$config->perpage)
                    ->offset($offset)
                    ->execute();
                    
        $xitem = array();
        
        if (!empty($items)) {
            foreach ($items as $key => $item) {
                
                $tmp = new stdClass();
                
                foreach ($item as $keyItem => $valueItem) {
                    if ($keyItem == 'value') {
                        //Active form field
                        $fields   = Contact::$config->field_contacts;
                        
                        //Make to object submit form
                        $objValue = json_decode($valueItem);
                        
                        for ($i = 0; $i < 2; $i++) {
                            $tmpKey = $fields[$i]->name;
                            $tmp->$tmpKey = $objValue->$tmpKey;               
                        }
                        continue;    
                    }
                    $tmp->$keyItem = $valueItem;
                }
                $xitem[$key] = $tmp;
            }
        }    
        
        return $xitem;
    }
    /** End Data listing purpose **/
    
    public function save() {
        $post = Request::$current->post();
         
        //Minimum validation 
        if (empty($post)
            || empty(Contact::$config) 
            || empty(Contact::$config->save_to_database)
            || empty(Contact::$config->field_contacts)) return;
        
        //Table validation
        $isTable = $this
                        ->select()
                        ->query("SHOW TABLE STATUS LIKE 'contact_submit_forms'")
                        ->execute()
                        ->current();
        
        if (empty($isTable)) return;
        
        foreach (Contact::$config->field_contacts as $field)
            $submit[$field->name] = empty($post[$field->name]) ? '' : $post[$field->name];
        
        $submit = $this->insert('contact_submit_forms')
                       ->row('value',json_encode($submit))
                       ->row('create_time',date('Y-m-d H:i:s'))
                       ->execute();        
    }
    
    public function fieldLabel($name = null) {
        if ( empty(Contact::$config) 
                || empty(Contact::$config->save_to_database)
                || empty(Contact::$config->field_contacts) ) return null;
            
        $isMultiLang = !empty(App::$module->language);
                    
        foreach (Contact::$config->field_contacts as $field)
        {
            $lang = Language::$current;
            $label[$field->name] = $isMultiLang ? $field->label->$lang : $field->label; 
        }            
        return empty($label) ? null : ( isset($name) ? ( !empty($label[$name]) ? $label[$name] : null ) : $label );                  
    }
}