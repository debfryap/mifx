<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Contact_City extends Model_Dba {  
    
    protected function pre_select(){
        if (Contact::$config->use_province)
            return $this->select('c.*',array('pr.name','province'))
                    ->from(array('cities','c'))
                    ->join(array('provinces','pr'),'left')->on('pr.id = c.province_id');
        else
            return $this->select('c.*')
                    ->from(array('cities','c'));
    }  
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('cities','c'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
        
        return $model;
    }
    /** End Data listing purpose **/
}