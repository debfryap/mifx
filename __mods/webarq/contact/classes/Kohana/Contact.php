<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Media Handler Class 
 * @Module      Contact
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Kohana_Contact {
    
    public static $config;
    
    /**
     * Module up (working both on development and production mode)
     */
    public static function up($config) {
                
    }
    
    /**
     * Module install (working on development mode only)
     */
    public static function install() {
        
        if (!self::$config->use_province) {
            Data::drop_table('provinces');
            Data::undo_module_navigation('contact',array('province'));
        }
        
        if (!self::$config->use_city) {
            Data::drop_table('cities');
            Data::undo_module_navigation('contact',array('city'));
        }
        
        if (!self::$config->enable_branch_office) {
            Data::drop_table('contact_offices');
            Data::undo_module_navigation('contact',array('office'));
        }
        
        if (!self::$config->save_to_database) {
            Data::drop_table('contact_submit_forms');
            Data::undo_module_navigation('contact',array('submit-form'));
        }
    }
    
    public static function getPhone($i = 0, $ext = false) {
        if (empty(self::$config->office->phone)) return;
        
        if (is_bool($i)) { $ext = $i; $i = 0; } 
        
        $c = self::$config->office->phone[0];
        
        $t = '';
        
        if (isset($c->number)) { $t .= $c->number;  }
        
        if ($ext && isset($c->ext)) { $t .= $c->ext; }
        
        return $t;
    }
}