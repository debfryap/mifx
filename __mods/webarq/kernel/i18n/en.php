<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(   
    //Classification Error I18n
    'error.301'                 => 'Moved Permanently',    
    'error.400'                 => 'Bad Request',
    'error.401'                 => 'Unauthorized',
    'error.402'                 => 'Payment Required',
    'error.403'                 => 'Forbidden',
    'error.404'                 => 'Page Not Found',
    'error.405'                 => 'Method Not Allowed',
    'error.406'                 => 'Not Acceptable',
    'error.407'                 => 'Proxy Authentication Required',
    'error.408'                 => 'Request Time Out',
    'error.409'                 => 'Conflict',
    'error.410'                 => 'Gone',
    'error.411'                 => 'Length Required',
    'error.412'                 => 'Precondition Failer',
    'error.413'                 => 'Request Entity to Large',
    'error.414'                 => 'Request URI to Long',
    'error.415'                 => 'Unsupported Media Type',
    'error.416'                 => 'Requested Range Not Satisfiable',
    'error.417'                 => 'Expectation Failer',
    'error.500'                 => 'Internal Server Error',
    'error.501'                 => 'Not Implemented',
    'error.502'                 => 'Bad Gateway',
    'error.503'                 => 'Service Unavailable',
    'error.504'                 => 'Gateway Timeout',
    'error.505'                 => 'HTTP Version Not Supported',

    'error.no_config_file'      => 'No configuration file found',
    'error.invalid-config'      => 'Invalid configuration',
    'error.already_used'        => ':item already in used',  
    'error.model_not_found'     => 'Model :model_name not found',
    'error.widget_not_exist'    => 'System could not locate widget <span class="widget_name">&quot;:widget_name&quot;</span> you have been requested',
    'error.write_cookie'        => 'Error while trying to write cookie to your browser. Please enable in your browser setting, if you do not know, again read your browser manual or just ask for the help :)',    
    'error.no-data'             => 'There is no data to display',
    'error.unauthorized'        => 'Sorry, seems like you do not have permission to access this page yet. If you think this is an error, please contact your administrator',
    'error.transaction'         => 'Error, database transaction could be contain 1 or more errors. Please contact your admin for further information',
    'error.no-pair'             => 'Could not use data :type function. Please contact administrator',
    'error.no-post-data'        => 'Could not use data :type function. You do not have post data. Please contact administrator',
        
    'success.transaction'       => 'Success. <span class="Data_object">:object</span> has updated.',
);