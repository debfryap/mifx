<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    SEO Handler
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * 
**/
 

class Request extends Kohana_Request {
    
    public function execute()
	{
		if ( ! $this->_external)
		{
			$processed = Request::process($this, $this->_routes);

			if ($processed)
			{
   	            // Store the matching route
				$this->_route = $processed['route'];
				$params = $processed['params'];
                
                // Store all params to all routes
                App::$route_params = $params;
                
				// Is this route external?
				$this->_external = $this->_route->is_external();

				if (isset($params['directory']))
				{
					// Controllers are in a sub-directory
					App::$route_params['directory'] = $this->_directory = preg_replace('/\/[a-z]/e', 'strtoupper("$0")', $params['directory']);
				}
                
                if (!empty($params['package']))
                {
                    // Have sub package directory
                    $this->_directory = ucfirst( strtolower( preg_replace('/[\s_-]+/', '', $params['package'] ) ) ).'/'.$this->_directory;
                }
                
				// Store the controller
        		$str = 'x'.strtolower(trim($params['controller']));
        		$str = (preg_replace('/[\s_-]+/', ' ', $str));
        		$controller = substr(str_replace(' ', '', $str), 1);
                $this->_controller = ucwords(str_replace(array('-','_'),'',$controller));
                
				// Store the action
				$this->_action = (isset($params['action']))
					? (str_replace('-','_',$params['action']))
					: Route::$default_action;

				// These are accessible as public vars and can be overloaded
				unset($params['controller'], $params['action'], $params['directory'],$params['package']);

				// Params cannot be changed once matched
				$this->_params = $params;
			}
		}
        
        #$this->debugging($processed);
        
		if ( ! $this->_route instanceof Route)
		{
			return HTTP_Exception::factory(404, 'Unable to find a route to match the URI: :uri', array(
				':uri' => $this->_uri,
			))->request($this)
				->get_response();
		}

		if ( ! $this->_client instanceof Request_Client)
		{
			throw new Request_Exception('Unable to execute :uri without a Kohana_Request_Client', array(
				':uri' => $this->_uri,
			));
		}

		return $this->_client->execute($this);
    }   
    
    private function debugging($processed) {
        echo Debug::vars($processed);
        echo '<pre>',
             'Directory  : ',$this->_directory,'<br/>',
             'Controller : ',$this->_controller,'<br/>',
             'Action : ',$this->_action,'<br/>',
             '</pre>';
    }     
}