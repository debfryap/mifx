<?php defined('SYSPATH') OR die('No direct script access.');

class HTML extends Kohana_HTML {
    
    public static function xattributes(array $attributes = null) {
        
        if (empty($attributes)) return null;
        
        $css_keys = array('width','height','color','background','padding','margin','line-height','text-align','font','font-size');
        
        $need_px  = array('width','height','padding','margin','line-height');
        
        $not_css  = Helper_Kernel::unset_exist_key($attributes,$css_keys);
        
        $html = '';        
        
        foreach ($css_keys as $i => $key) {
            
            if ($i == 0) {
                $html .= 'style="';
            } elseif ($i == (count($css_keys)-1)) {
                if (!empty($not_css['style'])) {
                    $html .= $not_css['style'];    
                }
                $html .= '"';   
            }
            
            if (isset($attributes[$key])){
                
                $html .= "$key:";
                
                if (in_array($key,$need_px)) {
                    $html .= str_replace('px','',$attributes[$key]).'px;';    
                } else {
                    $html .= $attributes[$key].';';
                } 
            }
        }
           
        unset($not_css['style']);
             
        return $html.HTML::attributes($not_css);
    }
    
    
}