<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Simplified Kohana Model
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * @Todo        clone not working yet :'(
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * Attention!!!
 * This model mostly simplifying the way to written the behaviour of conditional where, and on . 
 * Also DB query type
 * 
 *  Ex.
 *  =====================================================================================================================
 *  Sintax                                                   Original DB
 *  =====================================================================================================================
 *      
 *  $db = Model::factory('dba');
 *  $db->select('c1','c2')->from('table')->where('c1 = a')   DB::select('c1','c2')->from('table')->where('c1','=','a')                                              
 *  $db->select()->query('string query') ...                 DB::query(Database::SELECT,'string query')   
 * 
 *  =====================================================================================================================
**/ 

class Model_Dba {
    /**
     * @var string config database group to use
     */
    public $connection = NULL;
    
    /**
     * @var object database transaction data
     */
    protected $transaction;
    
    /**
     * @var object database transaction instance
     */
    public $_instance;      
    
    /**
     * @param boolean return object of database transaction
     */
    public $_as_object = true; 
    
    /**
     * @param array $data_tool (helper for listing) 
     */
    public $data_tool;
    
    public function __construct() {
        if (empty(Kernel::$master->connection)) {
            $master_load = Package::load('kernel','master','master');
            $con         = !empty($master_load->connection) ? $master_load->connection : 'default';
        } else {
            $con = Kernel::$master->connection;
        }
        $this->connection  = $con; 
        
        $this->transaction = new stdClass();
        $this->transaction->values  = array();
        $this->transaction->columns = array();
        $this->transaction->set     = array();
    }
    
    /**
     * @param mixed string colum name
     */    
    public function select($columns = null){
        $get_args = func_get_args();
        
        $columns = is_array($columns) && count($get_args)  <= 1 ? $columns : $get_args;
        
        $this->transaction->columns = empty($this->transaction->columns) 
                                            ? $columns 
                                            : array_merge($this->transaction->columns,$columns);
                                          
        $this->_instance = new Database_Query_Builder_Select($this->transaction->columns);
        
        if (!empty($this->transaction->table))
            $this->from($this->transaction->table);
                
        $this->transaction->type = 'select'; 
        return $this;
    }
    
    public function insert($table=null,array $columns=array()) {
        $this->_instance = new Database_Query_Builder_Insert($table,$columns);
        $this->transaction->type = 'insert'; 
        return $this;
    }
    
    public function update($table=null) {
        $this->_instance = new Database_Query_Builder_Update($table);
        $this->transaction->type = 'update'; 
        return $this;
    }
    
    public function delete($table=null) {
        $this->_instance = new Database_Query_Builder_Delete($table);
        $this->transaction->type = 'delete'; 
        return $this;
    } 
    
    public function query($type=null,$query=null) {
        $type = strtolower($type);
        if ($type != 'select' && $type != 'insert' && $type != 'update' && $type != 'delete') {
            $query = $type; 
            $type  = $this->transaction->type;
        } else {
            $this->transaction->type = $type;
        }
        
        $type  = $type == 'select' 
            ? Database::SELECT 
            : ( $type == 'insert' 
                    ? Database::INSERT 
                    : ( $type == 'update' 
                        ? Database::UPDATE 
                        : Database::DELETE ));
        $this->_instance = new Database_Query($type, $query);
            
        return $this;
    }
    
    public function from($table) {
        if (!empty($this->_instance)) $this->_instance->from($table);
        $this->transaction->table = $table;  
        return $this;   
    }
    
    public function columns($columns) {
        $this->_instance->columns($columns);
        return $this;
    }
    
    public function values($values) {
        $this->_instance->values($values);
        return $this;
    }
    
    
    /**
     * @var associative array of (column => value) pairs or string column
     * @var string value     
     */
    public function set($pairs,$value = null) {
        //$this->_instance->set($pairs);
        if (is_array($pairs))        
            $this->transaction->set = array_merge($this->transaction->set,$pairs);
        else 
            $this->transaction->set[$pairs] = $value; 
                                     
        return $this;
    }
        
    
    /**
     * @var associative array of (column => value) pairs or string column
     * @var string value    
     * 
     * Check if row give already exist in transaction, use the latest insert. 
     */
    protected function check_row($key,$value) {    
        $exist = false;
        foreach ($this->transaction->columns as $k => $v) {
            if ($v !== $key) {
                $this->transaction->columns[$k] = $v;
                $this->transaction->values[$k]  = $this->transaction->values[$k];
            } else {
                $exist = true;
                $this->transaction->columns[$k] = $key;
                $this->transaction->values[$k]  = $value;
            }
        }
        if ($exist === false) {
            $this->transaction->columns[] = $key;
            $this->transaction->values[]  = $value;
        }
    }
    
    public function row($pairs,$value = null) {
        if (is_array($pairs)) {
            foreach ($pairs as $key => $value) {  
                $value = (strtolower($key) == 'create_time' || strtolower($key) == 'update_time') && strtolower($value) == 'now' 
                    ? App::date()
                    : $value;
                $this->check_row($key,$value); 
            }
        } elseif (!empty($this->transaction->columns)) {  
            $this->check_row($pairs,$value);
        } else {
            $this->transaction->columns[] = $pairs;
            $this->transaction->values[]  = $value;
        }
        
        return $this;
    }
    
    /**
     * =============================================================================================================
     * Mimic DB Builder :)
     * =============================================================================================================
    **/
    protected function make_where($condition,$column,$operator=null,$value=null) {
        if (!isset($operator) && !isset($value)) {
            list($column,$operator,$value) = explode(' ',$column,3);
            $value = strtolower($value) == 'null' ? NULL : $value;
        }
        if ($condition == 'where')
            $this->_instance->where($column,$operator,$value);
        elseif ($condition == 'and_where')
            $this->_instance->and_where($column,$operator,$value);
        elseif ($condition == 'or_where')
            $this->_instance->or_where($column,$operator,$value);
    }
    
    /**
     * @param string field name
     * @param string operator
     * @param string value
     * @example 
     *      $this->select()->from('tables')->where('field_name','=','value')
     *      or 
     *      $this->select()->from('tables')->where('field_name = value')
     */
    public function where($column=null,$operator=null,$value=null) {
        $this->make_where('where',$column,$operator,$value);
        return $this;
    }
    
    /**
     * @param string field name
     * @param string operator
     * @param string value
     * @example 
     *      $this->select()->from('tables')->and_where('field_name','=','value')
     *      or 
     *      $this->select()->from('tables')->and_where('field_name = value')
     */
    public function and_where($column=null,$operator=null,$value=null) {
        $this->make_where('and_where',$column,$operator,$value);
        return $this;
    }
    
    /**
     * @param string field name
     * @param string operator
     * @param string value
     * @example 
     *      $this->select()->from('tables')->or_where$column('field_name','=','value')
     *      or 
     *      $this->select()->from('tables')->or_where$column('field_name = value')
     */
    public function or_where($column=null,$operator=null,$value=null) {
        $this->make_where('or_where',$column,$operator,$value);
        return $this;
    }   
    
    public function where_open()  { return $this->and_where_open(); }
    public function where_close() { return $this->and_where_close(); }
        
    public function and_where_open() {
        $this->_instance->and_where_open();
        return $this;
    }
        
    public function and_where_close() {
        $this->_instance->and_where_close();
        return $this;
    }  
    
    public function or_where_open() {
        $this->_instance->or_where_open();    
    }
    
    public function or_where_close() {
        $this->_instance->or_where_close();    
    }
        
    public function group_by($field) {
        $this->_instance->group_by($field);
        return $this;
    }
        
    public function order_by($field,$direction = 'asc') {
        $this->_instance->order_by($field,$direction);
        return $this;
    }
    
    public function offset($number) {
        $this->_instance->offset($number);
        return $this;
    }
    
    public function limit($limit=1) {
        $this->_instance->limit($limit);
        return $this;
    }
    
    public function join($table,$type=null) {
        $this->_instance->join($table,$type);
        return $this;
    }
    
    public function on($column,$operator=null,$value=null) {
        if ((!isset($operator) || $operator === true) && !isset($value)) {
            $expression = $operator;
            list($column,$operator,$value) = explode(' ',$column,3);
            
            if ($expression === true) {
                $value = DB::expr("'$value'");
            }
        }
        
        $this->_instance->on($column,$operator,$value);
        return $this;
    }
    
    protected function __query_completion() {
        if (!empty($this->transaction->columns) && !empty($this->transaction->values) && $this->transaction->type == 'insert')
            $this->_instance->columns($this->transaction->columns)->values($this->transaction->values);   
        
        if (!empty($this->transaction->set) && $this->transaction->type == 'update')
            $this->_instance->set($this->transaction->set);
    }        
    
    public function execute($db = NULL, $as_object = TRUE, $object_params = NULL) {
        $connection = empty($db) ? $this->connection : $db;
        
        if ($this->_as_object && $this->transaction->type == 'select')
            $this->as_object();
        
        //Query completion
        $this->__query_completion(); 
        
                         
        $result = $this->_instance->execute($connection,$as_object,$object_params);
        
        //Reset Model before return result
        $this->__destructor();
        
        //Returning query database transaction    
        return $result;
    }
    
    public function index($index=0) {
        $run = $this->execute();
        return !empty($run[$index]) ? $run[$index] : null;    
    }
    
    public function current($key=null) {
        $current = $this->index(0);
        
        return $key === null ? $current : (isset($current->$key) ? $current->$key : null);
    }
    
    public function as_object($class=TRUE, array $params = NULL) {
        $this->_instance->as_object($class,$params);
        return $this;
    }
    
    public function as_array() {
        $this->_instance->as_array();
        return $this;    
    }
    
    public function printing() {
        //Query completion
        $this->__query_completion(); 
        
        return $this->_instance;
    }
    
    //Manual Destruction Method
    public function __destructor() {
        $this->transaction = new stdClass();
        $this->transaction->values  = array();
        $this->transaction->columns = array();
        $this->transaction->set     = array();
        $this->_instance = null;
    }
    
    //Magic to String
    public function __toString() {
        $this->__query_completion(); 
        return (string)($this->_instance);
    }
}