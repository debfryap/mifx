<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Date helper.
 *
 * @package    Kohana
 * @category   Helpers
 * @author     Kohana Team
 * @copyright  (c) 2007-2012 Kohana Team
 * @license    http://kohanaframework.org/license
 * 
 * @author     Daniel Simangunsong
 */
class Date extends Kohana_Date {
    
    /**
     * Get "span date" after date
     * @param   string  base date, eg 2013-05-20
     * @param   string  span date, eg 7 days, 1 month, 2 months
     * @param   string  return format date
     */
    public static function after_date($date, $span, $format = 'Y-m-d H:i:s') {
        $i = new Datetime($date);
        return $i->modify("+ $span")->format($format);
    }    
    
    /**
     * Get "span date" before date
     * @param   string  base date, eg 2013-05-20
     * @param   string  span date, eg 7 days, 1 month, 2 months
     * @param   string  return format date
     */
    public static function before_date($date, $span, $format = 'Y-m-d H:i:s') {
        $i = new Datetime($date);
        return $i->modify("- $span")->format($format);
    }

} // End date
