<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Logic helper class
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/
 
 
class Kohana_Logic {
    public static function check_exception($row_item,array $exception,$attrs = NULL) {
        if (is_array($row_item)) {
    
            $row_item = Helper_Kernel::arr_to_object($row_item,FALSE);
        } 
        
        foreach ($exception as $key => $value) {
            //Exception key does not have related value 
            if (!isset($row_item->$key)) return true;
            
            //Array condition                            
            if (is_array($value)) {
                //$in_exception = in_array($row_item->$key , $value); // Could not use this way for multidimensional array
                foreach ($value  as $value_item) {
                    if ($row_item->$key == $value_item) {
                        $in_exception = true;
                        break;
                    } else {
                        if (is_array($value_item)) {
                            list($use_key,$use_value) = $value_item;
                            if ($row_item->$use_key == $use_value) {
                                $in_exception = true;
                                break;
                            }   
                        } else {
                            $in_exception = false;
                        }
                    }    
                }
                
                //If in array just return true
                if ($in_exception === true) { 
                    return true; 
                } else { 
                    //System will assume this is need logical compare
                    foreach ($value as $value2) {
                        $compare = Logic::compare($row_item->$key,$value2,$attrs);
                        
                        //Return false if one of them does not match (system use and)
                        if ($compare === false) return false;
                    }
                    
                    if (isset($compare) && $compare === true) 
                        return true;
                    else 
                        continue;
                }
            }
            
            //Logic compare
            $compare = Logic::compare($row_item->$key,$value,$attrs);
            
            //Return true if match (system use or)
            if ($compare === true) return true;
        }   
        return false; 
    } 
    
    public static function compare($var1,$var2,$attrs = NULL) {
        
        $method = "equal"; //Default logic operator
        
        foreach (self::$operator as $symbol => $name) {
            $position = strpos($var2,$symbol);
            if ($position === 0) {
                
                $var2   = trim(substr($var2,strlen($symbol)));
                
                $method = $name;
                
                break;
            }
        }
        
        $logic  = new Logic($var1,$var2,$attrs); //Inisiate logic
        
        return $logic->$method();                
    }
    
    private static $operator = array(
        '===' => 'identical',
        '=='  => 'equal',
        '!==' => 'not_identical',
        '!='  => 'not_equal',
        '>='  => 'greater_equal',
        '>'   => 'greater',
        '<='  => 'lower_equal',
        '<>'  => 'lower_greater',
        '<'   => 'lower',
    ); 
    
    private $var_1;
    
    private $var_2;
    
    private $attrs;
        
    
    public function __construct($var1,$var2,$attrs = NULL) {
        
        $this->var_1 = is_numeric($var1) ? (int) $var1 : $var1;
        
        $this->var_2 = is_numeric($var1) ? (int) $var2 : $var2;
        
        $this->attrs = $attrs;
    }
    
    private function equal() {
        return $this->var_1 == $this->var_2;    
    }
    
    private function not_equal() {
        return $this->var_1 != $this->var_2;
    }
    
    private function identical() {
        return $this->var_1 === $this->var_2;
    }
    
    private function not_identical() {
        return $this->var_1 !== $this->var_2;
    }
    
    private function greater() {
        return $this->var_1 > $this->var_2;
    }
    
    private function greater_equal() {
        return $this->var_1 >= $this->var_2;
    }
    
    private function lower() {
        return $this->var_1 < $this->var_2;
    }
    
    private function lower_equal() {
        return $this->var_1 <= $this->var_2;
    }
    
    private function lower_greater() {
        return $this->var_1 <> $this->var_2;
    }
}