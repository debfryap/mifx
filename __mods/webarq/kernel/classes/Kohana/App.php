<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    App data handler
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/ 

Class Kohana_App {
    /**
     * @var (object)    Pre-defined application configuration 
     */
    public static $config;    
    
    /**
     * @var (object)    Valid application session information
     */
    public static $session;  
    
    /**
     * @var (object)    Valid application cookie information
     */
    public static $_cookie;    
    
    /**
     * @var (object)    Active module    
     */ 
    public static $module;
    
    /**
     * @var (array)     Application message type      
     */
    public static $message_type = array ('error','warning','public','transaction');
    
    /**
     * @var array request params
     */
    public static $route_params = array();
    
    /**
     * @var bool  show default language on uri
     * eg. say you have page 'about-us' and 'en' as default language
     * if set to true, when user currently in en language, then en will be add before about-us,
     * and the result are something like 'en/about-us'
     */
    public static $show_default_language_in_uri = false; 
    
    public static function up() {
        /** Application configuration -- load from config->site **/
        App::$config  = json_decode(json_encode(Kohana::$config->load('site')->site));
        
        /** Pre-defined per-page limit **/
        App::$config->perpage = 10;
        
        /** Application session configuration  **/
        App::$session = Session::instance();
        
        /** Other way to calling application session id **/
        App::$session->id = App::$session->id();
        
        /** Collect module name, path **/
        App::$module = new stdClass();
        foreach (Kohana::modules() as $name => $path) {
            App::$module->$name = $path;
        }
        
        /** App salt configuration **/
        App::salt();
    }
    
    public static function isInstall() {
        return Kohana::$environment == Kohana::DEVELOPMENT 
                        && (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest');
    }
    
    public static function isAjax() {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }    
    
    /** 
     * Down Application 
     */
    public static function down () {
        
        if (App::isInstall() && !App::isAjax()) {    
            $mods = array_merge(array('templanation','config'),array_keys(Kohana::modules()));
            
            Model::factory('Dba')->update('modules')->set(array('is_publish'=>1))->where('label','in',$mods)->execute();
            
            Model::factory('Dba')->update('modules')->set(array('is_publish'=>-1))->where('label','not in',$mods)->execute();
        }
        
    }
          
    /** Application salt configuration **/
    public static function salt() {  
        $sess_salt = App::$session->get(App::$config->salt->name,NULL);
        
        if (empty($sess_salt)) {
            $new_salt = Helper_Kernel::random_salt();
            App::$session->set(App::$config->salt->name,$new_salt);
        } 
        
        Cookie::$salt = App::$session->get(App::$config->salt->name,App::$config->salt->key);    
        App::$config->salt->encrypted = Cookie::salt(App::$config->salt->name,App::$config->salt->value);
    }
    
    /** Application cookies **/
    public static function cookie($cookie_name,$app_name = NULL,$expire_time = NULL) {
        $cookie_name  = App::cookie_name($cookie_name,$app_name);
        $app_cookie   = true;
        $cookie_value = Cookie::get($cookie_name,NULL);
                
        if (empty($cookie_value)) {
            $cookie_value  = empty($app_name) ? 'local' : $app_name;
            $cookie_value .= '::'.$cookie_name;
            $cookie_value .= '::'.App::$config->salt->encrypted;
            $cookie_value .= '::'.App::$session->regenerate();
            
            $app_cookie    = Cookie::set($cookie_name,$cookie_value,$expire_time);
        }  
        
        if (!$app_cookie) { //mean our cookie set failed
            throw new Kohana_Exception(__('error.write_cookie'));
        }
        
        return $cookie_value;
    }
    
    /**
     * @param   mixed   $format
     * Options : 
     *          full.   Format "Y-m-d H:i:s" (default)   
     *          0.      Format "Y-m-d"
     *          1.      Format "H:i:s"
     *          
     *          others as php date format, see php manual
     * @return  php date formatted function
     */
    public static function date($format='full') {
        switch($format) {
            case "full" :
                $format = "Y-m-d H:i:s"; break;
            case 0 :
                $format = "Y-m-d"; break;
            case 1 :
                $format = "H:i:s"; break;
            default:break;
        }
        
        return date($format);
    }
    
    /** Return proper cookie name base on Application base url **/
    public static function cookie_name($cookie_name,$appname = NULL) {
        $app_name     = !isset($appname) ? str_replace('/','',Kohana::$base_url) : $appname;
        $cookie_name  = empty($app_name) ? $cookie_name : $cookie_name."_$app_name";
        return $cookie_name;
    }
    
    public static function check_file($path,$file,$extension) {
        return Kohana::find_file($path,$file,$extension);
    }
    
    public static function json_config_module($module,$group='default') {
        $x = Kohana::$config->load($module);
        return empty($x->$group) ? '{}' : json_encode($x->$group);
    }
    
    public static function load_config($key) {
        return Model::factory('Dba')
                    ->select('id','value')
                    ->from('configurations')
                    ->where("key = $key")
                    ->limit(1)
                    ->execute()
                    ->current();
    }
    
    public static function update_config($key,$value) {
        return Model::factory('Dba')->update('configurations')
                    ->set(array('value'=>$value,'update_time'=>App::date()))
                    ->where('key','=',$key)
                    ->execute();
    }
    
    public static function establishment($module,$file_config,$key_name) {
        //Load table configurations
        $config_table = App::load_config($key_name);
        
        if (empty($config_table->value)) {
            if (!empty($file_config->default)) {
                if (isset($file_config->default['nstt'])) {
                    $file_config->default['nstt'][] = 'nstt';
                    $saveConfig = array_diff_key($file_config->default,array_flip($file_config->default['nstt']));
                } else {
                    $saveConfig = $file_config->default;    
                }
                
                if (empty($saveConfig)) {
                    //Class module
                    $class_name = ucfirst(strtolower($module));
                        
                    //Class config variable
                    $tmp        = $file_config->default; unset($tmp['nstt']);
                    $class_name::$config = Helper_Kernel::array_to_object($tmp);
                    
                    return;
                }
                
                $saveConfig = json_encode($saveConfig);
                
                $row = json_decode(json_encode(array (
                            "id" => "auto",
                            "key" => "$key_name",
                            "value" => $saveConfig,
                            "create_time" => "now"
                     )));
                     
                $insert = Installer::ignore('configurations',$row,$module);
                
                $table_config_id    = $insert[0];
                $table_config_value = $saveConfig;
            }       
        } else {
            $table_config_id    = $config_table->id;
            $table_config_value = $config_table->value;
        }
        
        if (!empty($table_config_value)) {
            
            //Make configuration as an array
            $array_config_table = json_decode($table_config_value,true);
            
            //Merge configuration (both of from file and database) 
            $array_merge = $array_config_table + $file_config->default; 
            
            //Objectify configuration array
            $xconfig = json_decode(json_encode($array_merge));
            
            //Class module
            $class_name = ucfirst(strtolower($module));
            
            if (class_exists($class_name)) { 
                //Class config variable
                $class_name::$config = $xconfig;
                
                //Configuration database id
                $class_name::$config->id = $table_config_id;
                
                if (is_callable("$class_name::up")) 
                    $class_name::up($file_config);
            }            
        }
    }
    
    //Search for active category through traversing system
    public static function tail_active_uri($package = null, $permalink = null) {
        if (!isset($permalink)) {
            //Remove unnecessary slash from main uri
            $link = trim(Request::$initial->uri(),'/');
            
            if (!empty(App::$module->language)) {
                $link = stripos($link,Language::$current) === 0
                            ? substr($link,strlen(Language::$current)+1)
                            : $link;
            }
            
            $poin = isset($package) ? strlen($package)+1 : 0;        
            
            if (strtolower($package) == strtolower($link))
                return new App(array('active_uri'=>$link,'tail'=>null));
            
            $link = substr($link,$poin);
        } else {
            $link = trim($permalink,'/');
        }
        
        $result['active_uri'] = $link;
        
        $a = $t = explode('/',$link);
        
        $result['small_uri']  = array_pop($t); 
        $result['parent_uri'] = array_shift($t);
        
        foreach ($a as $index => $xlink) {
            $index++;
            $l = !isset($l) ? $xlink : "$l/$xlink"; 
            #$result['tail'][$index] = $l;
            $result['tail'][$index] = $xlink;
        }
        
        return new App($result);
    }
    
    /**
     * @var string main active uri
     */    
    public $active_uri;
    
    /**
     * @var array tailing main active uri in traverse mode
     */
    public $tail;
    
    public function __construct(array $configs = array()) {
        if (!empty($configs))
            foreach ($configs as $key => $value)
                $this->$key = $value;
    }
    
    public function get($key) {
        return isset($this->$key) ? $this->$key : null;
    }
}