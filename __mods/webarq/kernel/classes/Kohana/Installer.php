<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Table Maker (Create or Update)
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/ 


class Kohana_Installer extends Populate {
    //String, type of table transaction (create or alter)
    protected $type;
    
    //String, table name
    protected $table;
    
    //Array object of table columns
    protected $columns = array();
    
    //Array object of previous table status. Only need when transction type is update
    protected $status;
    
    //Query transaction
    protected $query;
    
    //Object temporary setting
    protected $temp;
    
    //String active module name
    protected $module;
    
    //Object array insert row data
    protected $row;
    
    //Temporary table primaries dump
    protected $sys_table_primaries = array();
    
    //Temporary table engine
    protected $sys_table_engine = null;
    
    //Temporary table charset
    protected $sys_table_charset = null;
    
    //Temporary table collation
    protected $sys_table_collation = null;
    
    //Object database model
    protected $db;
    
    //Boolean check before insert row
    protected $check_before_insert = false;
    
    //Int length of string to check 
    protected $check_string_length = 60;
    
    //String actor name
    protected $actor = 'system';
    
    //Array simplify our value, let system handle it
    public static $skip_value = array('auto','total-row');
    
    public static function create($table_name,$table_columns) {
        $configs = array(
            "type"    => "create",
            "table"   => $table_name,
            "columns" => $table_columns,
            "model"   => "Dba"
        ); 
        
        return new Installer($configs);
    }
    
    public static function alter($table_name,$table_columns,$status) {
        $configs = array(
            "type"    => "alter",
            "table"   => $table_name,
            "columns" => $table_columns,
            "status"  => $status,
            "model"   => "Dba"
        );
         
        return new Installer($configs);
    } 
    
    public static function ignore($table_name,$table_row,$module=null,$actor='system') {
        if (empty($table_row)) return $table_row;
        $configs = array(
            "model"   => "Dba",
            "table"   => $table_name,
            "row"     => $table_row,
            "module"  => $module,
            "actor"   => $actor
        );
        $installer = new Installer($configs);
        return $installer->do_ignore();
    }
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        $this->sys_table_primaries = array();
        
        $do_function = "do_".$this->type;
        
        if ( !empty($this->type) && method_exists($this,$do_function) ) {
            $this->$do_function();
        }
    } 
    
    protected function build_column_query($commas,$index=null,$current_column=null) {
        if (!empty($this->temp->field)) {
            $this->query_column_name($this->temp->field,$commas,$current_column);
            $this->query_column_type();    
            $this->query_column_length();
            $this->query_column_unsigned();
            $this->query_column_collation();
            $this->query_column_null();
            $this->query_column_default();
            $this->query_column_increment();
            $this->query_column_comment();
            
            if (isset($index)) $this->query .= $this->query_column_position($index);
            
            //Primary preparation
            !empty($this->temp->primary) && $this->temp->primary === true 
                ? $this->sys_table_primaries[$this->temp->field] = $this->temp->field : '';
        }
    }
    
    protected function query_column_name($name,$commas,$current_column=null) {
        $this->query .= $commas === true ? ", " : "";
        if (empty($current_column)) {
            if ($this->type == 'alter') $this->query .= "ADD COLUMN ";
            $this->query .= "`".$name."` ";
        } else {
            $current_column = $current_column === true ? $name : $current_column;
            $this->query .= "CHANGE `$current_column` `$name` "; 
        }
    }
    
    protected function query_column_type() {
        $this->query .= $type = strtoupper($this->temp->type); 
           
        if ($type == 'ENUM') {
            $this->query .= " ('";
            $this->query .= str_replace(',',"','",$this->temp->option);
            $this->query .= "') ";
        }
    }
    
    protected function query_column_length() { 
        $this->query .= !empty($this->temp->length) ? "(".$this->temp->length.") " : " ";
    }
    
    protected function query_column_unsigned() {
        $type = $this->temp->type;
        $this->query .= !empty(Kernel::$master->property->unsigned->$type) && !empty($this->temp->unsigned)
                ?"UNSIGNED "
                :"";
    }
    
    protected function query_column_collation() {
        $this->query .= !empty($this->temp->collation) ? "COLLATE ".$this->temp->collation." " : " ";
    }
    
    protected function query_column_null(){
        #$this->query .= !empty($this->temp->null) ? "NULL " : "NOT NULL ";
        if ($this->temp->type != 'enum') {
            $null = $this->temp->primary === true 
                ? false
                : (is_bool($this->temp->null) ? $this->temp->null : false);
            $this->query .= $null === true ? "NULL " : "NOT NULL ";
        }
    }
    
    protected function query_column_default() {
        $this->query .=  isset($this->temp->default)
            ? "DEFAULT '".$this->temp->default."' "
            : "";
    }
    
    protected function query_column_increment() {
        $this->query .= !empty($this->temp->increment) && ($this->temp->increment === true || strtolower($this->temp->increment) == 'true') 
            ? "AUTO_INCREMENT " : ""; 
    }
    
    protected function query_column_comment() {
        $this->query .= !empty($this->temp->comment)  ? "COMMENT '".$this->temp->comment."' " : ""; 
    }
    
    protected function query_column_position($index) {
        if ($index <= 0) {
            return "FIRST "; 
        }else{
            $index_min = $index-1;
            if (empty($this->columns[$index_min]->field)) {
                $this->query_column_position($index_min);       
            }else{
                return "AFTER `".$this->columns[$index_min]->field."` ";
            }
        }
    }
    
    protected function is_engine_charset_collation($column) {
        if (!empty($column->engine)) $this->sys_table_engine = $column->engine;
        if (!empty($column->charset)) $this->sys_table_charset = $column->charset ;
        if (!empty($column->collation)) $this->sys_table_collation = $column->collation;
    }
    
    
    
    protected function query_table_primary() {
        $do_primary   = 
        $drop_primary = false;
        if ($this->type == 'alter' && $this->status->alter_table === true) {
            $current_primaries = empty($this->status->sys_table_primaries) ? array() : $this->status->sys_table_primaries;
            if (empty($this->sys_table_primaries) && empty($current_primaries)) {
                $do_primary = $drop_primary = false;
            } else {                
                #echo (count($this->sys_table_primaries) .'vs'. count($current_primaries));
                if (count($this->sys_table_primaries) != count($current_primaries)) {
                    $do_primary = $drop_primary = true;
                } else {                    
                    foreach ($current_primaries as $field) {
                        if (!in_array($field,$this->sys_table_primaries)) {
                            $do_primary = $drop_primary = true;
                        }
                    }
                }                
                $drop_primary = empty($current_primaries) ? false : $drop_primary;
            }
        } elseif ($this->type == 'create') {
            $do_primary = true;
        }
        
        if ($do_primary === true) {
            if (!empty($this->columns) && !empty($this->sys_table_primaries) && is_array($this->sys_table_primaries)){
                $count_primary = 1;
                $this->query .= $this->type == 'create' 
                        ? ", "
                        : ($drop_primary === true ? ", DROP PRIMARY KEY, ADD " : ", ADD "); 
                $this->query .= "PRIMARY KEY (";
                foreach ($this->sys_table_primaries as $key_primary=>$name_primary){
                    $this->query .= "`$name_primary`";
                    $this->query .= $count_primary < count($this->sys_table_primaries) ? ',' : '';
                    $count_primary++;
                }
                $this->query .= ')';
            }else{
                if ($this->type == 'alter'){
                    $this->query .= ", DROP PRIMARY KEY ";
                }
            }
       }    
    }
    
    protected function query_table_engine() {
        $alter_engine = strtolower(empty($this->sys_table_engine) ? Kernel::$master->engine : $this->sys_table_engine);
        $do_engine    = true;
        if($this->type == 'alter') {
            $now_engine = strtolower($this->status->Engine);
            if ($now_engine == $alter_engine) {
                $do_engine = false;
            } else {
                $this->query .= ', ';
                $this->status->alter_table = true;
            }
        }
        
        if ($do_engine) $this->query .= "ENGINE=".$alter_engine." ";
        return $do_engine;
    }
    
    protected function query_table_charset($commas=null) {
        $alter_charset = strtolower(empty($this->sys_table_charset) ? Kernel::$master->charset : $this->sys_table_charset);
        $do_charset    = true;
        
        if($this->type == 'alter') {
            $now_charset = explode('_',$this->status->Collation,2);
            $now_charset = strtolower($now_charset[0]);
            if ($now_charset == $alter_charset) {
                $do_charset = false;
            } else{
                if($commas === false) $this->query .= ', '; 
                $this->status->alter_table = true;
            }
        }
        
        if ($do_charset) $this->query .= "CHARSET=".$alter_charset." ";
        return $do_charset;
    }
    
    protected function query_table_collation($commas=null) {
        $alter_collation = strtolower(empty($this->sys_table_collation) ? Kernel::$master->collation : $this->sys_table_collation);
        $do_collation    = true;
        
        if($this->type == 'alter') {
            $now_collation = strtolower($this->status->Collation);
            if ($now_collation == $alter_collation) {
                $do_collation = false;
            } else {
                if($commas === false) $this->query .= ', ';
                $this->status->alter_table = true;
            }    
        }
        
        if ($do_collation) $this->query .= "COLLATE=".$alter_collation." ";
    } 
    
    protected function match_column() {
        //Current table column length        
        $this->status->cur_length = !empty($this->status->columns)
                    ? count($this->status->columns) : 0;
        
        //New table column length        
        $this->status->new_length = 0;
        foreach ($this->columns as $st_key => $st_column) 
            if (!empty($st_column->field)) $this->status->new_length++;
            
        if ($this->status->cur_length != $this->status->new_length) $this->status->alter_table = true;        
        
        if ($this->status->cur_length < $this->status->new_length)
            $this->add_column();
        elseif ($this->status->cur_length > $this->status->new_length)
            $this->drop_column();
        else 
            $this->remake_column(); 
    
    }
    
    protected function add_column() {        
        if (!empty($this->status->columns) && !empty($this->columns)) {
            $commas = false;            
            foreach ($this->columns as $st_key => $st_column) {                
                //Continue if no column configuration
                if (empty($st_column)) continue;
                                                                
                $in_setting = false;
                if (!empty($st_column->field)) {
                    foreach ($this->status->columns as $nd_key => $nd_column) {
                        if ($st_column->field == $nd_column->Field) {
                            $in_setting = true; 
                        }
                        if (!empty($nd_column->Key) && strpos(strtolower($nd_column->Key),'pri') !== false) {
                            $this->status->sys_table_primaries[$nd_column->Field] = $nd_column->Field;
                        }            
                    }                
                    if ($in_setting === false) {
                        $this->temp = $st_column;
                        $this->build_column_query($commas,($st_key));
                        $commas = true;
                    }
                    //Unfortunately we must prepare our primary key again :(
                    !empty($st_column->primary) && $st_column->primary === TRUE 
                        ? $this->sys_table_primaries[$st_column->field] = $st_column->field : '';
                
                }else{
                    $this->is_engine_charset_collation($st_column);
                }
            }
        }
    }
    
    protected function drop_column() {
        if (!empty($this->status->columns) && !empty($this->columns)) {
            foreach ($this->status->columns as $st_key => $st_column) { 
                                                                
                $in_setting = false;
                foreach ($this->columns as $nd_key => $nd_column) {
                    if (!empty($nd_column->field) && $st_column->Field == $nd_column->field) {                        
                        //Unfortunately we must prepare our primary key again :(                
                        !empty($nd_column->primary) && $nd_column->primary === TRUE 
                            ? $this->sys_table_primaries[$nd_column->field] = $nd_column->field : '';
                        $in_setting = true; break; 
                    }
                }
                
                if ($in_setting === false) {
                    $this->query .= empty($commas) ? "" : ", ";
                    $this->query .= "DROP COLUMN `$st_column->Field` ";
                    $commas = true;
                }
                
                $current = Package::mimic_master_column($st_column);
                !empty($current->primary) && $current->primary === TRUE 
                    ? $this->status->sys_table_primaries[$current->field] = $current->field : '';
            }               
            
            foreach ($this->columns as $st_key => $st_column) {             
                //Continue if no column configuration
                if (empty($st_column)) continue;
                
                if (empty($st_column->field)) $this->is_engine_charset_collation($st_column);
            }
        }
    }
    
    protected function remake_column() {
        if ($this->status->cur_length != 0) {
            $commas    = false;
            $min_field = 0;
            
            //Count current primary    
            foreach ($this->status->columns as $nd_key => $nd_column) {
                if (!empty($nd_column->Key) && strpos(strtolower($nd_column->Key),'pri') !== false) {
                    $this->status->sys_table_primaries[$nd_column->Field] = $nd_column->Field;
                }            
            }
                                        
            foreach ($this->columns as $st_key => $st_column) {              
                //Continue if no column configuration
                if (empty($st_column)) continue;
                
                if (!empty($st_column->field)) {
                    $this->temp = $st_column;
                    $current    = Package::mimic_master_column($this->status->columns[$st_key]);
                                        
                    if (strtolower($this->temp->field) == strtolower($current->field)) {                        
                        foreach ($this->temp as $used_key => $used_value) {
                            if (empty(Kernel::$master->exception_key->$used_key) && $current->$used_key != $used_value) {
                                $this->build_column_query($commas,$st_key,$current->field);
                                $this->status->alter_table = $commas = true;
                                break;                                   
                            }
                        }
                    } else {
                        $found = false;
                        foreach ($this->status->columns as $nd_key => $nd_column) {
                            if (strtolower($this->temp->field) == strtolower($nd_column->Field)) {
                                $found = true; break;
                            }
                        }   
                                                                                                               
                        $this->build_column_query(
                                $commas,
                                ($found === true ? $st_key : null),
                                ($found === true ? true : $current->field)
                            );
                        $this->status->alter_table = $commas = true;
                    }
                    
                    //Unfortunately we must prepare our primary key again :(
                    !empty($st_column->primary) && $st_column->primary === TRUE 
                        ? $this->sys_table_primaries[$st_column->field] = $st_column->field : '';
                }else{
                    $this->is_engine_charset_collation($st_column);
                }
            }
        }
    }
    
    protected function do_create() {
        
        if (empty($this->columns)) return;
        
        $this->query  = "CREATE TABLE `$this->table` ( ";
        $commas       = false;
        
        foreach ($this->columns as $key => $column) {
            //Continue if no column configuration
            if (empty($column)) continue;
            
            //Build each column query
            if (!empty($column->field)) {
                $this->temp = $column;
                $this->build_column_query($commas);
                $commas = true;
            }else{
                $this->is_engine_charset_collation($column);    
            }
        }
        
        $this->query_table_primary();
        $this->query .= ") ";
        $this->query_table_engine();
        $this->query_table_charset();
        $this->query_table_collation();
        
        #echo $this->query.'<br/><br/>';
        DB::query(Database::INSERT,$this->query)->execute();
        $this->query = "";
    }
    
    protected function do_alter() {
        if (empty($this->columns)) return;
                
        $this->status->alter_table = false; //Default status :)
        
        //Existing columns table
        $this->status->columns = $this->db
                                     ->select()
                                     ->query("SHOW FULL COLUMNS FROM `$this->table`")                                     
                                     ->execute();
                                                                                
        $this->query = "ALTER TABLE `$this->table` ";
        $this->match_column();       
        $this->query_table_primary();
        $do_engine    = $this->query_table_engine();
        $do_charset   = $this->query_table_charset($do_engine);
        $do_collation = $this->query_table_collation($do_engine === true ? true : $do_charset);
        
        if ($this->status->alter_table === true) {
            #if ($this->table == 'user_details') {
                #echo $this->query.'<br/><br/>';
                #die();
            #}
            DB::query(Database::INSERT,$this->query)->execute();    
        }        
    }
    
    protected function value_modifier($key,$value) {
        $temp  = strtolower($value);
        $this->check_before_insert = true;
        if ($temp == 'now' && ($key == 'create_time' || $key == 'update_time')) {
            return App::date();
        } elseif ($key == 'password') {
            return User::make_password($value);    
        } elseif (is_object($value)) {
            return json_encode($value);
        } elseif (strpos(trim(strtolower($temp)),'select') !== false && strpos(trim(strtolower($temp)),'select') == 0) {
            $this->check_before_insert = false;
            $do_value = DB::query(Database::SELECT,$value)->as_object()->execute();
            return empty($do_value[0]) ? 'select_empty' : $do_value[0]->id;     
        } elseif (in_array($temp,Installer::$skip_value)) {
            return $this->skip_value_modifier($temp,$key,$value);
        }
        
        $this->check_before_insert = false;
        return $value;
    }
    
    protected function skip_value_modifier($temp,$key,$value) {
        if ($temp === 'auto') {
            return !empty($this->status->Auto_increment) ? $this->status->Auto_increment : (isset($this->status->Rows) ? $this->status->Rows+1 : 'select_empty');
        } elseif ($temp === 'total-row') {
            return isset($this->status->Rows) ? $this->status->Rows+1 : 'select_empty';
        }
        return $value;
    }
    
    protected function do_ignore() {
        //Check status table
        $this->status = $this->db
                             ->select()
                             ->query("SHOW TABLE STATUS LIKE '$this->table'")
                             ->execute()
                             ->current();
        
        //Check table columns                     
        $x = $this->db->select()->query("SHOW COLUMNS FROM $this->table")->execute();
        if (!empty($x)) {
            foreach ($x as $column) {
                $find_string = strpos($column->Type,"(");
                $columns[$column->Field] = $find_string !== false ? substr($column->Type,0,$find_string) : $column->Type;
            }    
        }
        
                                                                                             
        $insert_columns = $insert_values = $need_check = $check_where = NULL; 
        
        $do_insert = true; 
                  
        //Ordering setting
        if (isset($this->row->ordering)) {        
            foreach ($this->row as $key => $value) {
                $keyLower = strtolower($key);
            
                $value = $this->value_modifier(strtolower($key),$value); //Column value
            
                if ($value === false) continue;
                
                if ($keyLower == 'parent_id' || strpos($keyLower,'category_id')) {
                    $count = $this->db
                                      ->select()
                                      ->query("SELECT COUNT(`$keyLower`) AS `total` FROM `$this->table` t WHERE t.`$keyLower` = $value;")
                                      ->execute()
                                      ->current(); 
                    $this->row->ordering = isset($count->total) ? $count->total+1 : 1;
                    break;
                }    
            }
        }
        
        foreach ($this->row as $key => $value) {
            
            $value = $this->value_modifier(strtolower($key),$value); //Column value
            
            if ($value === false) continue;
            
            if ($this->check_before_insert === true) {
                $need_check = true;
            } else {                    
                $arrSkipIgnore = Helper_Kernel::object_to_array(Kernel::$master->skip_ignore); 
                
                if (empty($arrSkipIgnore[$columns[$key]]) && empty($arrSkipIgnore[$key])) {
                    $arr_value = count(str_split($value)); //Split value
                    if ($arr_value <= $this->check_string_length) {
                        $check_where .= empty($check_where) ? " WHERE `$key` = '$value'" : " AND `$key` = '$value'";    
                    }
                }
            }
            
            $insert_columns .= empty($insert_columns) ? "`$key`" : ",`$key`";
            
            if ($value === 'select_empty') return false; //Query select for value return empty;
            
            $insert_values  .= empty($insert_values) ? "'$value'" : ",'$value'";    
        }        
        
        if ($need_check === true) {
            
            $check_row = $this->db
                              ->select()
                              ->query("SELECT `$key` FROM `$this->table` $check_where")
                              ->execute()
                              ->current();
                
            $do_insert = !empty($check_row) ? false : $do_insert;
        }
        
        if ($do_insert === true) {
            //Save to table
            $query  = "INSERT IGNORE INTO `$this->table` ($insert_columns) VALUES ($insert_values)";
            
            $insert = $this->db->query('insert',$query)->execute();  
            
            Installer::insert_history($this->table,$insert,$this->row,$this->actor);
            
            return $insert;
        }
        
        return false;
    }
    
    /**
     * @var string table name
     * @var array  kohana insert return
     */
    public static function insert_history($table,$insert,$row, $actor = 'system') {            
        if (!empty($insert[1])) {
            //Set default row id
            $row_id = $insert[0];
            
            //Check for row id if no auto increment return;
            if (empty($row_id)) {                     
                $columns = Model::factory('Dba')
                                 ->select()
                                 ->query("SHOW FULL COLUMNS FROM `$table`")                                     
                                 ->execute();
                                                                                              
                foreach ($columns as $column) {
                    if (!empty($column->Key)) {
                        $field  = $column->Field;
                        $row_id = $row->$field; 
                    }                                               
                }
            }
            
            History::record(array(
                'actor'       => $actor,
                'row_id'      => $row_id,
                'table'       => $table,
                'insert_row'  => $row
            ));
        }
    } 
    
}