<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget handler
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors
 * 
 * This widget sems like view, but it have controller and view file like small module. :)
 * And we put our widget in one folder called "widget" right after application or classified module folder.
 * We should naming our this widget , only with character, number, dash and underscore.
 * However dash will be replace to an empty space, when naming widget controller class while underscore 
 * will be act as "/"
 * 
**/
 

class Kohana_Widget extends Populate {
    //(string) widget name
    protected $_path;
    
    //(array)  widget vars
    protected $_vars;
    
    //(object) widget controller class name
    protected $_class;
    
    //(array)  widget view data
    protected $_data = array();
    
    //(int)    widget level error
    private $level_error = 0;
    
    //Load widget
    public static function load($_widget,array $_vars=array()) {
        return new Widget(array(
                '_path' => $_widget,
                '_vars' => $_vars
            ));
    }       
    
    public function __construct($configs = array()) {
        parent::__construct($configs);
    }
    
    /** Given variable to class **/    
    public function set($key,$value) {
        if (is_array($key))
            foreach ($key as $new_key=>$value) $this->_vars[$new_key] = $value;
        else
            $this->_vars[$key] = $value;
            
        return $this;
    }
    
    /** Give variable directly to widget view **/
    public function data($key=null,$value = null) {
        if (!empty($key) && (is_array($key) || is_object($key))) {
            foreach ($key as $_key => $value)
                $this->_data[$_key] = $value;
        } elseif (!empty($key)) {
            $this->_data[$key] = $value;
        }
        return $this;
    }
    
    protected function locate_widget() {
        $this->_path = preg_replace('/^\/+/', '',$this->_path);
        $find_class  = Kohana::find_file('widget',$this->_path.'/html/controller');
        
        if (!empty($find_class)) {
            $class_name = $this->_path;
            $class_name = ucwords(preg_replace('/[\/\_]+/',' ', strtolower($class_name)));
            $class_name = str_replace(' ','_',$class_name);
            $class_name = ucwords(preg_replace('/-/',' ',$class_name));
            $class_name = "Widget_".str_replace(" ","",$class_name);
            
            require_once $find_class;
        
            //Set path to widget view
            $this->_vars['_path'] = $this->_path;
            
            if (class_exists($class_name)) 
                $this->_class = new $class_name($this->_vars);
            else
                $this->level_error = 1;
        }
    }
    
    public function render() { 
        
        $this->locate_widget();
        
        if ($this->level_error == 1) {
            return __('error.widget_not_exist',array(
                    ':widget_name'=>$this->_path
                ));
        }
        
        $html = $this->css_js(); //Find widget js and css style
        
        if (!empty($this->_class)) {
            
            if (method_exists($this->_class,'render')) { //Find render method
                $html .= $this->_class->render();
                return $html;
            } elseif (method_exists($this->_class,'data')) {  //Find render data
                $this->data($this->_class->data());
            } 
        } 
        
        $find_view = Kohana::find_file('widget/'.$this->_path.'/html/','view');
        
        //Set path to widget view
        $this->_data['_path'] = $this->_path;
        
        if (!empty($find_view)) {
            $html .= Widget::capture($find_view,$this->_data);
            return $html;
        } else {
            return __('error.widget_not_exist',array(
                    ':widget_name'=>$this->_path
                ));
        }
    }
    
    protected function css_js() {
        $css_file  = preg_replace("{\\\}", "/",Kohana::find_file('widget',$this->_path.'/css/style','css'));
        $js_file   = preg_replace("{\\\}", "/",Kohana::find_file('widget',$this->_path.'/script/script','js'));
        $root_path = URL::root();
        
        if (!empty($css_file) || !empty($js_file)) {
            //Lets check location folder based on kohana cascading file
            $app_path = Helper_File::type(substr(APPPATH,0,-1),false,DIRECTORY_SEPARATOR);
            $in_app   = preg_match("/$app_path/",!empty($css_file) ? $css_file : $js_file);
                        
            if ($in_app !== 0) {
                $parted = $app_path;
            } else {
                $mod_path = Helper_File::type(substr(MODPATH,0,-1),false,DIRECTORY_SEPARATOR);
                $in_mod   = preg_match("/$mod_path/",!empty($css_file) ? $css_file : $js_file);
                if ($in_mod !== 0) {
                    $parted   = $mod_path;
                } else {
                    $sys_path = Helper_File::type(substr(SYSPATH,0,-1),false,DIRECTORY_SEPARATOR);
                    $in_sys   = preg_match("/$sys_path/",!empty($css_file) ? $css_file : $js_file);
                    if ($in_sys !== 0) {
                        $parted   = $sys_path;
                    }
                }
            }
            
            $html = '';
            if (!empty($parted)) {
                if (!empty($css_file)) {
                    $file_data = $root_path.strstr($css_file,$parted);
                     
                    if ( !isset($this->_vars['css']) || $this->_vars['css'] === true ) {
                        $html = HTML::style($file_data);
                    }
                }
                
                if (!empty($js_file)) {
                    $file_data = $root_path.strstr($js_file,$parted); 
                    
                    if ( !isset($this->_vars['js']) || $this->_vars['js'] === true ) {
                        $html .= HTML::script($file_data);
                    }
                }
                
                //Push data widget url
                $length = strlen(strstr($file_data,'css'));
                $this->_data['wg_url'] = substr($file_data,0,-($length));
                
                return $html;
            }
        }
        return '';
    }
    
    public static function capture($view_path,$view_vars = array()) {
        //Import the view variables to local namespace
		extract($view_vars, EXTR_SKIP);

		//Capture the view output
		ob_start();

		try
		{
			//Load the view within the current scope
			include $view_path;
		}
		catch (Exception $e)
		{
			//Delete the output buffer
			ob_end_clean();

			//Re-throw the exception
			throw $e;
		}

		//Get the captured output and close the buffer
		return ob_get_clean();
    }
    
    
    public function __toString() {
		try
		{
			return $this->render();
		}
		catch (Exception $e)
		{
			//Display the exception message
			Kohana_Exception::handler($e);

			return '';
		}
    }
    
}