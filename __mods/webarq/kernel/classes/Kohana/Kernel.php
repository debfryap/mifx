<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Populate Class
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/
 

class Kohana_Kernel {
    //(object)  master package
    public static $master;
    
    
    //(string)  session message type :)
    public static $message_type = array ('error','warning','public','transaction');
        
    public static function open() {
        
         /** Load kernel master data**/
        $kernel_pack    = Package::load('kernel','master');
        Kernel::$master = $kernel_pack->master;        
        Kernel::$master->model = Model::factory("Dba");
        
        //Rewrite application configuration
        Kernel::reconfig_site();    
        
        /** Write application message **/
        Kernel::app_message();
        
        /** Set our application environment **/
        Kohana::$environment = strtolower(App::$config->system->environment) === 'production'
                ? Kohana::PRODUCTION
                : Kohana::DEVELOPMENT;
        
        /** One stop module package installer **/        
        Package::install();
    }  
    
    /** Merge your array **/
    public static function recursive_merge($arr_destination,$arr_to_merge) {
        foreach ($arr_to_merge as $key => $value) {
            if (is_array($value) || is_object($value)) {
                if (is_object($arr_destination)) {
                    if (!isset($arr_destination->$key)) {
                        $arr_destination->$key = $value;
                    } else {
                        $arr_destination->$key = Kernel::recursive_merge($arr_destination->$key,$value);
                    }
                } else {
                    if (!isset($arr_destination[$key])) {
                        $arr_destination[$key] = $value;
                    } else {
                        $arr_destination[$key] = Kernel::recursive_merge($arr_destination[$key],$value);
                    }
                }
            } else {
                if (is_object($arr_destination)) {
                    $arr_destination->$key = $value;
                } else {
                    $arr_destination[$key] = $value;
                }
            }   
        }
        
        return $arr_destination;
    } 
    
    public static function reconfig_site() {
        
        //No need to do this if ajax
        //if (App::isAjax()) return;
        
        if ( Kohana::$environment == Kohana::DEVELOPMENT ) {
            $is_table = Kernel::$master->model
                             ->select()
                             ->query("SHOW TABLE STATUS LIKE 'configurations'")
                             ->execute()
                             ->current();
                             
            if (!empty($is_table)) {                     
                //Insert ignore site config into table
                $site = '{"system":{"offline":false,"environment":"development"},"perpage":"10","meta":{"title":"WCMS Version 2.0.10","keywords":"WCMS Version 2.0.10","description":"WCMS Version 2.0.10 Human Develop"}}';
                       
                $query = "INSERT IGNORE INTO `configurations` (`id`,`key`,`value`,`create_time`) VALUES (1,'site','$site','".date('Y-m-d H:i:s')."')";
                
                
                $insert = DB::query(Database::INSERT,$query)->execute();
                if (!empty($insert[0])) {
                    History::record(array(
                        'row_id'      => $insert[0],
                        'table'       => 'configurations',
                        'action'      => 'create',
                        'object'      => 'site'                        
                    ));
                }  
            }      
        } else {
            $is_table = true;    
        }
        
        if (!empty($is_table)) {
            //Get from table
            $site_table_config = DB::select('value')
                                    ->from('configurations')
                                    ->where('key','=','site')
                                    ->as_object()
                                    ->execute()
                                    ->current();
            if (!empty($site_table_config->value)) {
                $config = json_decode($site_table_config->value);
                App::$config = Kernel::recursive_merge(App::$config,$config);
            }
        }  
            
        //Application cookie
        App::$_cookie = new Cookie;
    }
    
    
    public static function app_message() {
        App::$config->message = new stdClass();  
        foreach (Kernel::$message_type as $type) {
            App::$config->message->$type = App::$session->get("$type"."_message");
        }
    }
    
    public static function destroy_app_message() {      
        foreach (Kernel::$message_type as $type) {
            App::$session->set("$type"."_message",null);
        }    
    }
    
    public static function error($message,$error_code=404){
        throw HTTP_Exception::factory($error_code,$message);
    }
    
    public static function destroy() {
        Kernel::destroy_app_message();
    }
    
    
}