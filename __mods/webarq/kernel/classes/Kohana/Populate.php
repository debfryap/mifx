<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Populate Class
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/
 

abstract class Kohana_Populate {    
    //(Array) Class configuration array
    protected $_config = array();
    
    //(Object) Database model object
    protected $db;
    
    //(Object) Iniate database model
    protected $_db = null;
    
	public function __construct(array $config = array(),$file_config_name = null,$file_config_group='default') {
		//Configuration setup
        $new_config = $config;
        if (!empty($file_config_name)) {
            
            //Load user config file
            $config_file = Kohana::$config->load($file_config_name);
            
            //Get rid of possible stray config group names
            unset($new_config['group']);
            
            if (isset($config_file->$file_config_group) && is_array($config_file->$file_config_group))
                //Add config group values, not overwriting existing keys
                $new_config += $config_file->$file_config_group;
        }
        
        //Setup config
        $this->setup($new_config);
        
        //Reset config
        $this->_config = array();
	}
    
	public function setup(array $config = array()) {
        
		//Overwrite the current config settings
		$this->_config = $config + $this->_config;
        
        //Recursively set each config to be property.
        foreach ($this->_config as $key=>$value) {
            if ($key == 'model') {
                if (is_array($value) || is_object($value)) {
                    $this->db = new stdClass;
                    foreach ($value as $model_name) {
                        $model = Helper_Kernel::proper_model_name($model_name);                     
                    
                        $this->db->$model_name = Model::factory($model);
                    }
                } else {
                    $model = Helper_Kernel::proper_model_name($value);
                    
                    if (!empty($model))
                        $this->db = Model::factory($model);
                } 
            } else {
                $this->$key = $value;
            }
        }
        
        //Db
        $this->_db = Model::factory('Dba');
	}
    
    /** 
     * This is the machine who give back all module property, simple but powerfull.
     * @param 'Key' 
     * @return 'Property' or 'NULL' if not found
    **/
        
	public function __get($key) {
		return isset($this->$key) ? $this->$key : NULL;
	}
    
    
	/** 
     * Wow wow wow .. there is something more. We can set up our config as much as you want
     * Using this ofcourse
     * @param 'Key'
     * @param 'Value'
    **/ 
     
	public function __set($key, $value=null) {
		$this->setup(array($key => $value));
        return $this;
	}
    
    /**
     * =============================================================================================================
     * === Mimic database condition support ========================================================================
     * =============================================================================================================
    **/
    protected function make_where($condition,$column,$operator=null,$value=null) {
        if (!isset($operator) && !isset($value)) {
            list($column,$operator,$value) = explode(' ',$column,3);
            $value = strtolower($value) == 'null' ? NULL : $value;
        }
        if ($condition == 'where')
            $this->_db->where($column,$operator,$value);
        elseif ($condition == 'and_where')
            $this->_db->and_where($column,$operator,$value);
        elseif ($condition == 'or_where')
            $this->_db->or_where($column,$operator,$value);
    }
    
    /**
     * @param string field name
     * @param string operator
     * @param string value
     * @example 
     *      $this->select()->from('tables')->where('field_name','=','value')
     *      or 
     *      $this->select()->from('tables')->where('field_name = value')
     */
    public function where($column=null,$operator=null,$value=null) {
        $this->make_where('where',$column,$operator,$value);
        return $this;
    }
    
    /**
     * @param string field name
     * @param string operator
     * @param string value
     * @example 
     *      $this->select()->from('tables')->and_where('field_name','=','value')
     *      or 
     *      $this->select()->from('tables')->and_where('field_name = value')
     */
    public function and_where($column=null,$operator=null,$value=null) {
        $this->make_where('and_where',$column,$operator,$value);
        return $this;
    }
    
    /**
     * @param string field name
     * @param string operator
     * @param string value
     * @example 
     *      $this->select()->from('tables')->or_where$column('field_name','=','value')
     *      or 
     *      $this->select()->from('tables')->or_where$column('field_name = value')
     */
    public function or_where($column=null,$operator=null,$value=null) {
        $this->make_where('or_where',$column,$operator,$value);
        return $this;
    }   
    
    public function where_open()  { return $this->and_where_open(); }
    public function where_close() { return $this->and_where_close(); }
        
    public function and_where_open() {
        $this->_db->and_where_open();
        return $this;
    }
        
    public function and_where_close() {
        $this->_db->and_where_close();
        return $this;
    }
        
    public function group_by($field) {
        $this->_db->group_by($field);
        return $this;
    }
        
    public function order_by($field,$direction = 'asc') {
        $this->_db->order_by($field,$direction);
        return $this;
    }
    
    public function offset($number) {
        $this->_db->offset($number);
        return $this;
    }
    
    public function limit($limit=1) {
        $this->_db->limit($limit);
        return $this;
    }
    
    public function join($table,$type=null) {
        $this->_db->join($table,$type);
        return $this;
    }
    
    public function on($column,$operator=null,$value=null) {
        if (!isset($operator) && !isset($value)) {
            list($column,$operator,$value) = explode(' ',$column,3);
        }
        $this->_db->on($column,$operator,$value);
        return $this;
    }
}