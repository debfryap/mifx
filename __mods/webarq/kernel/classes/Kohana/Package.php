<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Packaging Installer Class
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/

 
class Kohana_Package {    
    //Current module package setting
    protected $pack;
    
    //Object Model
    protected $model;
    
    //Object silly history module item
    public static $_history;
    
    public static function install() {
        return new Package();
    }
    
    public function __construct() {
        $this->model = Kernel::$master->model;
        
        //Trying to load silly history item :)
        Package::silly_history_load(App::$module);
        
        foreach (App::$module as $name => $path) {                     
            //Module establishment
            if (empty(Kernel::$master->skip_module->$name)) {
                //Proper key name
                $key_name    = Inflector::singular($name);
                
                //Check for configuration file
                $file_config = Kohana::$config->load($key_name);
        
                App::establishment($name,$file_config,$key_name);
                
                //Installing while on Development and not ajax requested
                if (Kohana::$environment == Kohana::DEVELOPMENT 
                        && (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')) {
                    //Load package
                    $this->pack = Package::load($name);
                    
                    //Make table
                    $this->make_table($name);
            
                    //Class object
                    $class_name   = ucfirst(strtolower($name));
                    
                    if (class_exists($class_name)) {
                        //Installation
                        if (is_callable("$class_name::install")) {
                            $class_name::install($file_config);    
                        }
                    }
                }
            }
        }
    }
    
    //(array) modules
    public static function silly_history_load($modules) {
        Package::$_history = new stdClass();
        foreach ($modules as $name => $path) {
            $history_item = Package::load($name,'history-map');
            if (!empty($history_item)) {
                foreach ($history_item as $k => $v) {
                    foreach ($v as $t => $vv) {
                        if (!isset(Package::$_history->$k)) {
                            Package::$_history->$k = new stdClass();
                        }
                        Package::$_history->$k->$t = $vv;
                    }
                }
            }
        }
    } 
    
    protected function make_table($module) {
        if (!empty($this->pack)) {
            foreach ($this->pack as $table => $columns) {
                $columns = Package::shadowing_master_table($columns);
                
                //Table check
                $is_table = $this->model
                                 ->select()
                                 ->query("SHOW TABLE STATUS LIKE '$table'")
                                 ->execute()
                                 ->current();
                
                //Calling table maker
                if (empty($is_table)) {
                    Installer::create($table,$columns);
                } else {   
                    Installer::alter($table,$columns,$is_table);
                }
            }            
        }
           
        //Dummy table
        $dummies = Package::load($module,'dummy');
        //foreach ($this->pack as $table => $columns) {    
            //Dummy oh dummy
            if (!empty($dummies)) {
                foreach ($dummies as $table => $rows) {
                    
                    if (empty($rows)) continue;
                    
                    foreach ($rows as $row) {
                        Installer::ignore($table,$row,$module);
                    }
                }
            }
        //}
    }
        
    
    /**
     * @var     array or object column setting
     * @return  a new column setting (merge with default column)
     */
    public static function mimic_master_column($column) {
        $column             = Helper_Kernel::lowering_key($column);
        $master_column      = json_decode(json_encode(Kernel::$master->column),true);
        $column             = is_object($column) ? json_decode(json_encode($column),true) : $column;
        $master_merge       = json_decode(json_encode(array_merge($master_column,$column)));
        $master_merge->type = strtolower($master_merge->type);
        $master_merge       = Package::master_set($master_merge);
        $master_merge       = Package::master_key($master_merge);
        $master_merge       = Package::master_null($master_merge);
        $master_merge       = Package::master_increment($master_merge);
        $master_merge       = Package::master_collation_charset($master_merge);
                
        return $master_merge;
    }
    
    public static function master_collation_charset($column) {
        $type = $column->type;
        $column->collation = !isset(Kernel::$master->property->collation->$type) 
            ? null
            : (empty($column->collation) ? Kernel::$master->collation : $column->collation);
        return $column;
    }
    
    public static function master_key($column) {
        if ((!empty($column->key) && strpos(strtolower($column->key),'pri') !== false )) {
            $column->primary = true;
        }
        return $column;
    }
    
    public static function master_increment($column) {
        if (!empty($column->extra) && strpos(strtolower($column->extra),'auto_increment') !== false ) {
            $column->increment = true;
        }
        return $column;
    }
    
    public static function master_null($column) {
        $column->null = is_string($column->null) 
            ? (strtolower($column->null) == 'no' ? false : true) 
            : ($column->primary === true ? false : (is_bool($column->null) ? $column->null : false));
        return $column;
    }
    
    public static function master_set($column) {
        $type = $column->type;
        $open_bracked  = strpos($type,'(');
        $close_bracked = strpos($type,')');
        $rest_substr   = null;
        
        //Check for column length
        $column->length =  !empty(Kernel::$master->property->length->$type)
            ? (!empty($column->length) ? $column->length : Kernel::$master->property->length->$type)
            : null;
        
        //Check for column type based on mysql column type typografy .. eg. INT(11), CHAR(200)                
        if ($open_bracked !== false && $close_bracked !== false && $open_bracked < $close_bracked) {
            $column->type = $check_type = strtolower(substr($type,0,$open_bracked));
            $rest_substr  = substr($type,$open_bracked+1,-1);
            
            $column->length = !empty(Kernel::$master->property->length->$check_type) 
                ? (!empty($column->length) ? $column->length : (int) $rest_substr) 
                : null;
        }
        
        if ($column->type == 'enum') { 
            $column->option = isset($rest_substr) ? str_replace(array("\"","'"),"",$rest_substr) : $column->option;
            if (!empty($column->default) && !in_array($column->default,explode(',',$column->option))) {
                $column->default = null;
            }
        } else {                    
            $column->option = null;
        }
        
        return $column;
    }
    
    public static function shadowing_master_table($columns) {
        $arr_temp_column = array();
        if (!empty($columns)) {
            foreach ($columns as $column) {
                
                if (empty($column)) continue;
                
                $temp_column = null; 
                if (!empty($column->field) && empty($column->master)) {
                    $temp_column = $column;    
                } else if (!empty($column->master)) {
                    $temp_column = 
                    $std_column  = array();
                    foreach (Kernel::$master->field as $master_column ) {
                        if ($master_column->field == $column->master) {
                            foreach ($master_column as $key => $value){ 
                                $key = strtolower($key); 
                                $temp_column[$key] = $value; 
                            }
                            foreach ($column as $key => $value) {
                                $key = strtolower($key);
                                $std_column[$key] = $value; 
                            }
                                                                                                             
                            $temp_column = json_decode(json_encode(array_replace_recursive($temp_column,$std_column)));
                            
                            //I do not know, whether this has affect
                            unset($temp_column->master);
                        } 
                    }
                    
                    /** **
                    $temp_column = new stdclass();
                    $std_column  = new stdclass();
                    foreach (Kernel::$master->field as $master_column ) {
                        if ($master_column->field == $column->master) {
                            foreach ($master_column as $key => $value){ 
                                $key = strtolower($key); 
                                $temp_column->$key = $value; 
                            }
                            foreach ($column as $key => $value) {
                                $key = strtolower($key);
                                $std_column->$key = $value; 
                            }
                            
                            $temp_column = json_decode(json_encode($temp_column),true);
                            $std_column  = json_decode(json_encode($std_column),true);                            
                            $replace_master_using = Helper_Kernel::array_replace_merge($temp_column,$std_column);                                                           
                            $temp_column = json_decode(json_encode($replace_master_using));
                            
                            //I do not know, whether this has affect
                            unset($temp_column->master);
                        } 
                    }
                    /** **/
                } else {
                    $last_column = $column;
                }
                
                if (!empty($temp_column)) {
                    $arr_temp_column[] = Package::mimic_master_column($temp_column);
                }
            }
            if (!empty($last_column)) $arr_temp_column[] = $last_column;
                                    
            return $arr_temp_column;
        }
    }
    
    public static function load($package_module,$package_name='installer',$item=null,$object=true) {
        $modules = App::$module;
        $load_package = null;
        
        if (is_bool($item)) { $object = $item; $item = null; }
                 
        if (!empty($modules->$package_module)) {
            //Package information will be merge instead overwrite            
            $files = Kohana::find_file('package',$package_module.'/'.$package_name,'php',true);
            
            if (!empty($files)) {                 
                foreach ($files as $file) {
                    //Force loaded file to be an array
                    $load_file  = Kohana::load($file);
                    foreach ($load_file as $key => $value) {
                        $load_file[$key] = is_string($value) ? json_decode($value,true) : $value;
                    }
                    
                    $load_package = empty($load_package)
                                ? $load_file
                                : Helper_Kernel::array_recursive_merge($load_package,$load_file); //Function missing
                } 
                
                if ($object === true) {
                    if (!empty($item)) {
                        return empty ($load_package[$item]) ? null : ( is_string($load_package[$item]) ? json_decode($load_package[$item]) : json_decode(json_encode($load_package[$item])));
                    }
                    
                    $pack = new stdClass(); 
                    
                    if (empty($load_package)) return null;
                                                             
                    foreach ($load_package as $key => $value) {
                        $key = is_int($key) ? 'key'.$key : $key;                        
                        $pack->$key = is_string($value)
                                ? json_decode($value) 
                                : json_decode(json_encode($value));
                    }
                    
                    return $pack;
                }
                
                return empty($item) ? $load_package : (isset($load_package[$item]) ? $load_package[$item] : null);
            }
        }
    } 
    
    public static function find_master_table(array $tables,$index=0) {
        if (isset($tables[$index])) {
            if (count($tables) === 1) {
                return $tables[0];
            }            
                    
            $needle = strtolower(Inflector::singular($tables[$index]));
            
            foreach ($tables as $k => $v) {
                if ($k != $index) {
                    $haystack = strtolower(Inflector::singular($v));
                    if (strpos($haystack,$needle) === 0) {
                        return $tables[$index]; 
                    }
                }
            }
            return Package::find_master_table($tables,$index+1);
        } else {
            return null;
        }  
    }
    
    
}