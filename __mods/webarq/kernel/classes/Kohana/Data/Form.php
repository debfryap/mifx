<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data form builder
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/


class Kohana_Data_Form {
    //(Int) form column layout
    protected $_column = 2;
    
    //(Bool) Dual language mode
    protected $_multilang = true;
    
    //(Array) form attributes
    protected $_attributes = array();
    
    //(Array) form element
    protected $_elements = array();
    
    /**
     * @var   boolean, show or not transaction column which is 
     *        pairing in table => column_name => element_name
     */
    protected $_pair_transaction = true;
    
    //(Object) form element (merge between element from module pack, and element from the instance)
    protected $_form;
    
    //(Array) form values
    protected $_values = 'auto';
    
    protected $_override = array();
    
    protected $_manual = false;
    
    //(Array) Not attributes key
    protected $_not_attribute_key = 
        array(
                'column','multilang','elements','values',
                'action','pack','module','controller','map',
                'row_id','check_by_lang','pair_transaction',
                'title','cancel','override','jquery_validation',
                'manual','media'
        );
    
    //(Array) form package
    protected $_pack = array();
    
    //(String) application module
    protected $_module;
    
    //(String) application controller
    protected $_controller;
    
    //(String) application action
    protected $_action = null;
        
    //(Int)    transaction item id
    protected $_row_id;
    
    //(String) form element for requirement check
    protected $_check_by_lang = false;
    
    //(Boolean) Return mode, true for process only 
    protected $_map = false;
    
    //(String) form title
    protected $_title;
    
    //(String) form job type create || edit
    protected $_type;
    
    //(Bool) Form cancel button
    protected $_cancel = true;
    
    //(Bool) Using pre-defined jquery validation
    protected $_jquery_validation = true;
    
    //(Array) (dis)able form media 
    protected $_media = null;
    
    public static $active_form;
    
    public static function column (array $pack) {
        return new Data_Form(array(
                'pack'  => $pack,
                'map'   => true
             ));
    }
    
    public function __construct(array $serial = array()) {        
        foreach ($serial as $key => $value) {
            if (!in_array($key,$this->_not_attribute_key))
                $this->_attributes["$key"] = $value;
            else {
                if ($key == 'elements')
                    $value = is_array($value) ? json_decode(json_encode($value)) : $value;
                    
                $_key = "_".$key; $this->$_key = $value;
            }
        }    
        
        $this->_type = !isset($this->_type) ? strtolower(Request::$initial->param('param1')) : $this->_type;
        
        $this->_row_id = !isset($this->_row_id) 
                                ? array('id',Request::$initial->param('param2')) 
                                : (is_array($this->_row_id) ? $this->_row_id : array('id',$this->_row_id));
                                
        $this->_controller =  empty($this->_controller) 
                                ? strtolower(App::$route_params['controller']) 
                                : $this->_controller;
                                
        $this->_form = Data_Form_Pack::instance($this->_pack,$this->_elements,$this->_map);
        
        /**
         * Get media form transaction value
         * 
         * Move to render method @ Sept 11, 2013 ... Daniel Simangunsong
         * 
         */
        //$this->transaction_media();   
        
        if ($this->_values == 'auto' && !empty($this->_row_id) 
                && !empty($this->_form->tables) 
                && $this->_type != 'create') {
            //Get common form transation value                    
            $this->transaction_values();
        }
    }
    
    /** 
     * This is the machine who give back all module property, simple but powerfull.
     * @param 'Key' 
     * @return 'Property' or 'NULL' if not found
    **/
        
	public function get($key) {
		return isset($this->$key) ? $this->$key : NULL;
	}
    
    
	/** 
     * Wow wow wow .. there is something more. We can set up our config as much as you want
     * Using this ofcourse
     * @param 'Key'
     * @param 'Value'
    **/ 
     
	public function set($key, $value=null) {
        if (!in_array($key,$this->_not_attribute_key))
            $this->_attributes["$key"] = $value;
        else {
            if ($key == 'elements')
                $value = is_array($value) ? json_decode(json_encode($value)) : $value;
                
            $_key = "_".$key; $this->$_key = $value;
        }
        return $this;
	}
    
    private function transaction_values() {
        $values = array();
        
        $master_table   = Package::find_master_table($this->_form->tables);
        $row_column     = $this->_row_id[0];
        $row_value      = $this->_row_id[1];
        $foreign_column = Inflector::singular($master_table)."_id";
        
        foreach ($this->_form->transaction as $table => $columns) {
            $xcolumns  = array();
            $xrow_column = $table === $master_table ? $row_column : $foreign_column;
            foreach ($columns as $column => $form) {
                $xcolumns[] = $column;
            }
            
            /**
             * Edit this query to enabling multilanguage for multiple value.
             * Maybe someday you need to enable one category in some language but disable it in another language
             */
            
            $getRow = Model::factory('Dba')
                        ->select($xcolumns)
                        ->from($table)
                        ->where("$xrow_column = $row_value")
                        ->execute();
                        
            if (!empty($getRow[0])) {
                if (count($getRow) == 1) {
                    foreach ($getRow[0] as $fieldX => $valueX) {
                        if (!empty(App::$module->language) && $this->_multilang === true) {
                            $values[$fieldX][Language::$system] = $valueX; 
                        } else {
                            $values[$fieldX] = $valueX;
                        }
                    }   
                } else {
                    for ($i = 0; $i < count($getRow); $i++) {
                        foreach ($getRow[$i] as $fieldX => $valueX) {
                            if (!empty(App::$module->language) && $this->_multilang === true) {
                                $values[$fieldX][Language::$system][$i] = $valueX;   
                            } else {
                                $values[$fieldX][$i] = $valueX;
                            }
                        }
                    }            
                }                     
            }
            
            //Check for translation
            if (!empty(App::$module->language) && $this->_multilang === true && !empty($row_value)) {
                $t = Model::factory('Language')->translate($table,$row_value);
                            
                if (!empty($t[0])) {
                    foreach ($t as $i) {
                        $values[$i->row_column][$i->language_code] = $i->row_value;    
                    }
                }
            }
        }
        
        if (!empty($this->_override) && !empty($getRow)) {
            foreach ($this->_override as $xcolumn => $xvalue) {
                if (!empty(App::$module->language) && $this->_multilang === true) {
                    if (is_array($xvalue)) {
                        foreach ($xvalue as $xlanguage => $xvalue_i) {
                            $values[$xcolumn][$xlanguage] = $xvalue_i;    
                        }
                    } else {
                        foreach ($values[$xcolumn] as $xlanguage => $xvalue_i) {
                            $values[$xcolumn][$xlanguage] = $xvalue;  
                        }
                    }    
                } else {
                    $values[$xcolumn] = $xvalue;    
                }
                
            }    
        }
        $this->_values = $values;  
    }
    
    private function transaction_media() {
        if (!empty($this->_media) && is_array($this->_media)) {
            $media_table = $this->_media[0];
            
            $media_items = is_array($this->_media[1]) ? $this->_media[1] : array($this->_media[1]);
            
            unset($this->_media);
            
            $this->_media['table'] = $media_table;
                       
            foreach ($media_items as $key => $media) {
                $x = strpos($media,':');
                 
                if ($x !== false) {
                    list($type,$name) = explode(':',$media);
                    $this->_media['type'][$key] = array ($type,$name);
                } else {
                    $this->_media['type'][$key] = array ($media,$media);
                }
                
                $item = $this->_type === 'edit'
                    ? Model::factory('Medialibrary')->get_application($name,$this->_row_id[1],$media_table)
                    : null;
                                
                $this->_media['media'][$key] = $item;                                            
            }
        }
    }
    
    public function transaction_map() {
        return $this->_form->transaction;
    }
    
    public function push() {
        return $this;
    }
    
    public function render($value = null) {
        
        $this->transaction_media();
        
        return Widget::load('station/form')->data(array(
            'action'        => $this->_action,
            'type'          => $this->_type,
            'row_id'        => $this->_row_id,
            'xmodule'       => empty($this->_module) ? App::$route_params['package'] : $this->_module,
            'controller'    => $this->_controller,
            'elements'      => $this->_form->element,
            'pair_transaction' => $this->_pair_transaction,
            'transaction'   => $this->_form->transaction,
            'values'        => $this->_values,
            'attributes'    => $this->_attributes,
            'column'        => $this->_column,
            'multilang'     => $this->_multilang,
            'check_by_lang' => $this->_check_by_lang,
            'pack'          => $this->_pack,
            'title'         => $this->_title,
            'cancel'        => $this->_cancel,
            'validation'    => $this->_jquery_validation,
            'manual'        => $this->_manual,
            'media'         => $this->_media
        ))->render();
    }
    
    public function __toString() {
		try
		{
			return $this->render();
		}
		catch (Exception $e)
		{
			// Display the exception message
			Kohana_Exception::handler($e);

			return '';
		}
    }
}