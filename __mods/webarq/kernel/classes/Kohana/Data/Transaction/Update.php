<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data transaction create
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors
 * 
**/ 


class Kohana_Data_Transaction_Update {
    
    private $post;
    
    private $pack;
    
    private $pairs;  
    
    private $master;  
    
    private $tables;
    
    private $multilang;    
    
    private $debugging;
    
    private $load_pack;
    
    private $allow_empty;
    
    private $primary_where_value;
    
    private $primary_where_column;
    
    private $_result = false;
    
    private $_checking;
    
    private $_ordering;
    
    private $foreign;
    
    private $not_in_value;
    
    private $skip_special_columns = array('update_time');
    
    private $update_time;
        
    public $transaction;
    
    public static function open(array $configs = array()) {
        return new Data_Transaction_Update($configs);
    }
    
    public function __construct(array $configs = array()) {
        // Check for language
        $this->multilang = empty(App::$module->language) ? false : $this->multilang;        
                                
        //Set master as standard object        
        $this->master = new stdClass();    
        
        //Set transaction as standard object
        $this->transaction = new stdClass(); 
        
        //Set checking as standard object
        $this->_checking = json_decode('{"multiple":[],"translations":[]}');
        
        //Update variables
        foreach ($configs as $key => $value) {
            if (strtolower(trim($key)) === 'master')
                $this->master->table = $value;
            else 
                $this->$key = $value;
        }
        
        //Make sure post is one level object
        $this->post = is_array($this->post) ? Helper_Kernel::arr_to_object($this->post,false) : $this->post;
                    
        //Load pack
        if (!isset($this->load_pack) && !empty($this->pack)) {            
            $this->load_pack = Data_Form_Pack::instance($this->pack,null,true);
        }
        
        //Check for transaction pair
        if (empty($this->pairs)) {
            if (!empty($this->load_pack->transaction)) {
                $this->pairs = $this->load_pack->transaction;
            }
        }        
        if (empty($this->pairs)) {
            throw new Kohana_Exception(__('error.no-pair',array(':type'=>'update')));
        }
        
        //Item row index
        $param_1 = Request::$initial->param('param1');
        
        $param_2 = Request::$initial->param('param2');
        
        $this->master->column    = isset($param_2) ? $param_1 : 'id';        
                                           
        $this->master->increment = isset($param_2) ? $param_2 : $param_1;
        
        //Collect table transaction data, and assign exact value to the pairing variable
        foreach ($ipairs = $this->pairs as $table => $pairs) {
            $this->tables[] = $table; 
            foreach ($pairs as $column_name => $post_name) {
                
                $colomn_value = $this->mapping_value($post_name);
                
                $this->pairs[$table][$column_name] = $colomn_value;
                
                $column_name = strtolower($column_name);
                
                //Check order
                if (isset($pairs['ordering'])) {
                    if (!isset($this->post->previous_ordering)) {
                        throw new Kohana_Exception('Error, need previous ordering post to done this action');    
                    }
                                            
                    //Data order
                    $new_order      = $this->mapping_value($pairs['ordering']);
                    
                    $previous_order = $this->post->previous_ordering;
                    
                    $fix_order      = $new_order != $previous_order;
                    
                    if ($fix_order === true) {
                        $builder[0] = Model::factory('Dba')->update($table);
                        
                        if ($new_order > $previous_order ) {
                            $builder[0]->set("ordering",DB::expr('`ordering` - 1'))
                                     ->where("ordering > $previous_order")
                                     ->and_where("ordering <= $new_order");
                        } else {
                            $builder[0]->set("ordering",DB::expr('`ordering` + 1'))
                                     ->where("ordering < $previous_order")
                                     ->and_where("ordering >= $new_order");
                        } 
                    }
                    
                    //Fixing order where parenting
                    $helper_field_ordering_parenting = isset($this->post->helper_field_ordering_parenting) && $this->post->helper_field_ordering_parenting == $column_name;
                    
                    if ($helper_field_ordering_parenting) {
                        
                        $order_extend_condition = 'previous_ordering_parenting'; 
                        
                        $order_extend_condition_field = $column_name;
                        
                        $order_extend_condition_value = $colomn_value;
                        
                        $order_extend_condition_previous_value = $this->post->$order_extend_condition;
                    
                        if ($order_extend_condition_previous_value != $order_extend_condition_value) {
                            
                            if (isset($builder[0])) {
                                unset($builder[0]);
                            }    
                            
                            $builder[1] = Model::factory('Dba')
                                            ->update($table)
                                            ->set("ordering",DB::expr('`ordering` - 1'))
                                            ->where("ordering >= $previous_order")
                                            ->and_where("$order_extend_condition_field = $order_extend_condition_previous_value");
                                            
                            $builder[2] = Model::factory('Dba')
                                            ->update($table)
                                            ->set("ordering",DB::expr('`ordering` + 1'))
                                            ->where("ordering >= $new_order")
                                            ->and_where("$order_extend_condition_field = $order_extend_condition_value");
                        } else {
                            if (isset($builder[0]))
                                $builder[0]->and_where("$order_extend_condition_field = $order_extend_condition_value");
                            
                        } 
                    }
                        
                    if (!empty($builder) && !isset($this->transaction->fix_order)) {
                        foreach ($builder as $key => $builder_instance) {
                            $this->transaction->fix_order[$key] =  
                                $builder_instance->and_where($this->master->column,"<>",$this->master->increment);                                
                        }
                    }
                    
                }
            }
        }            
        
        //Mastering data
        if (empty($this->master->table)) {
            $this->master->table = Data::check_parent($this->tables);
        }
        $this->master->foreign_id = Inflector::singular($this->master->table)."_id";
                
        $this->master->status = Model::factory('Dba')
                            ->select()
                            ->query("SHOW TABLE STATUS LIKE '".$this->master->table."'")
                            ->execute()
                            ->current();
        
        $this->compile();
    }
    
    /**
     * Return value post by post name
     */
    private function mapping_value($post_name) {
        if (!isset($this->post->$post_name)) {
            return $this->mapping_pack($post_name);
        }
            
        $post = $this->post->$post_name;
        
        if ($post === false) {
            return false;
        }
        
        $is_array = is_array($post);
        
        if (!$is_array) {
            if (substr(trim($post),0,9) === 'increment') {
                return false;
            } 
        }
        
        if ($is_array) {
            return $post;
        } elseif (is_object($post)) {
            return json_encode($post);
        } else {
            return trim($post);
        }
    }
    
    /**
     */
    private function mapping_pack($post_name) {    
        $load_pack = $this->load_pack;
        if (isset($load_pack)) {          
            if (empty($load_pack->element)) return false; //no pack to check
            
            foreach ($load_pack->element as $k => $v) {
                if ($v->name == $post_name) {
                    return isset($v->seeking_value) ? $v->seeking_value : '';
                }
            };                
            return false; //not found even in package
        }
        return false; //no pack to check    
    }
    
    private function compile() {
        
        foreach ($this->pairs as $table => $pairs) {
            //Default table transactions
            $this->transaction->updates[$table] = Model::factory('Dba')->update($table);
            
            foreach ($pairs as $column_name => $column_value) { 
                     
                //Skip special column
                if ((in_array($column_name,$this->skip_special_columns) && $table === $this->master->table)
                        || $column_value === false) {
                    continue;
                }
                
                if (is_array($column_value)) {
                    /**
                     * There is 3(two) condition how our post could be an array in return.
                     * 1. Have 2(two)/more in active language
                     * 2. Master row have multiple row in another related table
                     * 3. Both of these 2(two) condition meet in post (in development) *grinnn*
                     * Let's handle it 
                     */
                     
                     //Check for language module load, and column value for system lang    
                     $is_master_language_post = !empty(App::$module->language) ? isset($column_value[Language::$system]) : false;                                                                
                                                               
                     if ($this->multilang === true && $is_master_language_post === true) {
                        //Condition 1 and 3
                        $this->query_translation($table,$column_name,$column_value,$unset);                          
                     } else {
                        //Condition 2
                                                                                 
                        if (isset($this->transaction->updates[$table]) && empty($unset_master)) {                   
                            //Get rid previous default table transaction        
                            unset($this->transaction->updates[$table]);
                            $unset_master = true;
                        }
                        
                        $iteration = 0;                                         
                        foreach ($column_value as $index => $column_value2) {
                            //Builder multi checking select
                            $is_foreign = isset($this->foreign[$table]) ? in_array($column_name,$this->foreign[$table]) : true;
                            if (!isset($this->transaction->multi_checks[$table][$index]) && $is_foreign) {
                                $this->transaction->multi_checks[$table][$index] = 
                                    Model::factory('Dba')
                                        ->select($column_name)
                                        ->from($table)
                                        ->where($this->master->foreign_id,'=',$this->master->increment);
                            }
                            if ($is_foreign) {
                                $this->transaction->multi_checks[$table][$index]->and_where("$column_name = $column_value2");
                            }
                            
                            //Builder multi updates
                            if (!isset($this->transaction->multi_updates[$table][$index])) {
                                //Replace with a new one update
                                $this->transaction->multi_updates[$table][$index] = 
                                        Model::factory('Dba')->update($table)
                                            ->set($this->master->foreign_id,$this->master->increment)                                            
                                            ->where($this->master->foreign_id,'=',$this->master->increment);                           
                            } 
                            if ($is_foreign) {
                                $this->transaction->multi_updates[$table][$index]->and_where("$column_name = $column_value2");
                                
                                //Not in value, to get rid of non selecting foreign key
                                $this->not_in_value[$column_name][] = "'$column_value2'";
                            }
                            
                            //Builder multi insert
                            if (!isset($this->transaction->multi_inserts[$table][$index])) {
                                //Replace with a new one insert
                                $this->transaction->multi_inserts[$table][$index] = Model::factory('Dba')->insert($table);
                                
                                //Auto set master table id as foreign key to each related table
                                $this->transaction->multi_inserts[$table][$index]->row(
                                    $this->master->foreign_id,$this->master->increment);                           
                            }
                            $this->transaction->multi_inserts[$table][$index]->row($column_name,$column_value2);   
                            $iteration++;                         
                        }                                                                  
                        
                        //Builder multi delete
                        if (isset($this->not_in_value)) {
                            if (!isset($this->transaction->multi_deletes[$table])) {
                                $this->transaction->multi_deletes[$table] = 
                                    Model::factory('Dba')->delete($table)
                                        ->where($this->master->foreign_id,'=',$this->master->increment); 
                            }   
                            
                            foreach ($this->not_in_value as $column => $values) {                                
                                $value = implode(',',$values);
                                $this->transaction->multi_deletes[$table]->and_where($column,"NOT IN",DB::expr("($value)"));
                            }
                        }
                     } 
                } else {
                    if (isset($this->transaction->updates[$table])) {
                        $this->query_set($this->transaction->updates[$table],$column_name,$column_value);
                    } 
                    
                    if (isset($this->transaction->multi_updates[$table])) {
                        $this->query_set($this->transaction->multi_updates[$table],$column_name,$column_value);
                    } 
                    
                    if (isset($this->transaction->multi_inserts[$table])) {
                        $this->query_row($this->transaction->multi_inserts[$table],$column_name,$column_value);
                    }   
                }  
            }
            
            unset($this->not_in_value);
        }   
        
        //Media library 
        if (!empty($this->post->media_application) && !empty(App::$module->medialibrary)) {
            
            $post = $this->post->media_application;
            
            if (isset($this->post->current_media_applicant))
                $post['current_media_applicant'] = $this->post->current_media_applicant;
            
            $this->transaction->media_application =  Medialibrary::media_application_transaction(
                                                            'update'
                                                            ,$this->master->increment
                                                            ,$post
                                                            ,$this->debugging);
        }
    }
    
    public function query_translation($table,$column,$post,&$unset) {
        //Looping through active language, and not system language
        foreach (Language::$active as $lang) {
            
            if (!isset($post[$lang->code])) return;
                        
            $value = $post[$lang->code];
            
            $is_array_value = is_array($value);
            
            if ($lang->code == Language::$system ) {                
                if ($is_array_value) {
                    if ($table == $this->master->table) {
                        $temp = null;
                        foreach ($value as $k => $v) {
                            $temp = isset($temp) ? $temp."::".$v : $v;
                        }                        
                        $value = $temp;
                        $this->query_set($this->transaction->updates[$table],$column,$value);
                    } else { 
                        $this->query_lang_filter($table,$column,$value,$lang->code,$unset);
                    }    
                } else {
                    if (isset($this->transaction->updates[$table])) {
                        $this->query_set($this->transaction->updates[$table],$column,$value);
                    } 
                }
            } else {
                if ($is_array_value) { 
                    $this->query_lang_filter($table,$column,$value,$lang->code,$unset);
                } else {
                    /**
                     * Assign checking translation value, need it later
                     * I'm to tired(lazy) at May 07, 2013 i just check it directly rather than 
                     * later
                     */
                    $checking = Model::factory('Dba')
                                        ->select('row_column')
                                        ->from('translations')
                                        ->where("language_code = $lang->code")
                                        ->and_where("row_table = $table")
                                        ->and_where("row_id = ".$this->master->increment)
                                        ->and_where("row_column = $column")
                                        ->execute()
                                        ->current()
                                        ;
                    if (empty($checking)) { //insert
                        if ($value != "") { 
                            $this->transaction->translation_inserts[$table][$column][$lang->code] = 
                                    Model::factory('Dba')->insert('translations')
                                        ->row('language_code',$lang->code)
                                        ->row('row_table',$table)
                                        ->row('row_id',$this->master->increment)
                                        ->row('row_column',$column)
                                        ->row('row_value',$value);
                        }               
                    }  else { //update
                        $this->transaction->translation_updates[$table][$column][$lang->code] = 
                                Model::factory('Dba')->update('translations')
                                    ->set('row_value',$value)
                                    ->where("language_code = $lang->code")
                                    ->and_where("row_table = $table")
                                    ->and_where("row_id = ".$this->master->increment)
                                    ->and_where("row_column = $column");
                    } 
                }
            }            
        }
    }
    
    private function query_set($instance,$column,$value) {
        if (is_array($instance)) {
            foreach ($instance as $next_instance) {
                $this->query_set($next_instance,$column,$value);    
            }
        } else {
            $instance->set($column,$value);
        }
    }
    
    private function query_row($instance,$column,$value) {
        if (is_array($instance)) {
            foreach ($instance as $next_instance) {
                $this->query_row($next_instance,$column,$value);    
            }
        } else {
            if ($value !== "")            
                $instance->row($column,$value);
        }
    }
        
    private function query_lang_filter($table,$column,$post,$lang,&$unset) {
        if (empty($unset)) {
            //Get rid previous default table transaction
            if (isset($this->transaction->updates[$table])) {        
                unset($this->transaction->updates[$table]);
            }
            //Get rid previous multi delete   
            if (isset($this->transaction->multi_deletes[$table])) {
                unset($this->transaction->multi_deletes[$table]);        
            }         
            //Get rid previous multi check
            if (isset($this->transaction->multi_checks[$table])) {
                unset($this->transaction->multi_checks[$table]);        
            }
            //Get rid previous multi update
            if (isset($this->transaction->multi_updates[$table])) {
                unset($this->transaction->multi_updates[$table]);        
            }
            //Get rid previous multi insert
            if (isset($this->transaction->multi_inserts[$table])) {
                unset($this->transaction->multi_inserts[$table]);       
            }
            $unset = true;
        }
                        
        $iteration = 0;                                         
        foreach ($post as $index => $column_value2) {
            //Builder multi checking select
            $is_foreign = isset($this->foreign[$table]) ? in_array($column,$this->foreign[$table]) : true;
            if (!isset($this->transaction->multi_checks[$table][$lang][$iteration]) && $is_foreign) {
                $this->transaction->multi_checks[$table][$lang][$index] = 
                    Model::factory('Dba')
                        ->select($column)
                        ->from($table)
                        ->where($this->master->foreign_id,'=',$this->master->increment)
                        ->and_where("lang_code = $lang");
            }
            if ($is_foreign) {
                $this->transaction->multi_checks[$table][$lang][$index]->and_where("$column = $column_value2");
            }
            
            //Builder multi updates
            if (!isset($this->transaction->multi_updates[$table][$lang][$index])) {
                //Replace with a new one update
                $this->transaction->multi_updates[$table][$lang][$index] = 
                        Model::factory('Dba')->update($table)         
                            ->set($this->master->foreign_id,$this->master->increment)                                   
                            ->where($this->master->foreign_id,'=',$this->master->increment)
                            ->and_where("lang_code = $lang");                           
            } 
            if ($is_foreign) {
                $this->transaction->multi_updates[$table][$lang][$index]->and_where("$column = $column_value2");
                
                //Not in value, to get rid of non selecting foreign key
                $this->not_in_value[$lang][$column][] = "'$column_value2'";
            }
            
            //Builder multi insert
            if (!isset($this->transaction->multi_inserts[$table][$lang][$index])) {
                //Replace with a new one insert
                $this->transaction->multi_inserts[$table][$lang][$index] = Model::factory('Dba')->insert($table);
                
                //Auto set master table id as foreign key to each related table
                $this->transaction->multi_inserts[$table][$lang][$index]
                     ->row($this->master->foreign_id,$this->master->increment)
                     ->row("lang_code",$lang);                           
            }
            $this->transaction->multi_inserts[$table][$lang][$index]->row($column,$column_value2);   
            $iteration++;                         
        }                                                                
                        
        //Builder multi delete
        if (isset($this->not_in_value)) {
            if (!isset($this->transaction->multi_deletes[$table][$lang])) {
                $this->transaction->multi_deletes[$table][$lang] = 
                    Model::factory('Dba')->delete($table)
                        ->where($this->master->foreign_id,'=',$this->master->increment)
                        ->and_where("lang_code = $lang"); 
            }   
            
            foreach ($this->not_in_value as $xlang => $i) {
                if ($lang == $xlang) {
                    foreach ($i as $column => $ii) {                        
                        $value = implode(',',$ii);
                        $this->transaction->multi_deletes[$table][$lang]->and_where($column,"NOT IN",DB::expr("($value)"));    
                    }
                }
            }
        }
    }     
        
    private function query_printing($data) {
        if (is_array($data)) {
            foreach ($data as $Q) {
                $this->query_printing($Q);
            }
        } else {
            echo $data,'<br/>';
        }
    }
    
    private function query_execution($data,$table = null) {
        if (is_array($data)) {
            foreach ($data as $Q) {
                $this->query_execution($Q);
            }
        } else {
            $execute = $data->execute();
            if (!empty($execute)) {
                $this->_result = true;
            }
        }
    }
    
    private function do_debug() {
        echo 'A. Query Transaction :';
        echo '<p></p>';
        if (!empty($this->transaction->updates)) {
            $temp_query = $this->transaction->updates;
            
            //Getride master            
            if (isset($temp_query[$this->master->table])) {
                if ($this->update_time !== false && !isset($this->pairs[$this->master->table]['update_time']) 
                    && !is_array($temp_query[$this->master->table])) {
                    $time = $this->update_time === true ? date('Y-m-d H:i:s') : $this->update_time;
                    echo $temp_query[$this->master->table]->set('update_time',$time),'<br/>';                     
                } else {
                    echo $temp_query[$this->master->table],'<br/>';
                }
                unset($temp_query[$this->master->table]);   
            }
            
            $this->query_printing($temp_query);
        }     
        if (!empty($this->transaction->multi_deletes)) {
            $this->query_printing($this->transaction->multi_deletes);
        }           
        if (!empty($this->transaction->multi_checks)) {
            $this->query_printing($this->transaction->multi_checks);
        }    
        if (!empty($this->transaction->multi_updates)) {
            $this->query_printing($this->transaction->multi_updates);
        }           
        if (!empty($this->transaction->multi_inserts)) {
            $this->query_printing($this->transaction->multi_inserts);
        }        
        if (!empty($this->transaction->translation_inserts)) {
            $this->query_printing($this->transaction->translation_inserts);
        }      
        if (!empty($this->transaction->translation_updates)) {
            $this->query_printing($this->transaction->translation_updates);
        }     
        if (!empty($this->transaction->fix_order)) {
            $this->query_printing($this->transaction->fix_order);
        }     
        if (!empty($this->transaction->media_application)) {
            $this->query_printing($this->transaction->media_application);
        }
        
        echo '<p></p>';    
        echo 'B. Pairing data, [table][column][value] : ';
        echo Debug::vars($this->pairs);   
        if (isset($this->post->media_application)) {
            echo 'Media Application : ';
            echo Debug::vars($this->post->media_application);    
             
            if (isset($this->post->current_media_applicant)) {
                echo 'Current Application : ';
                echo Debug::vars($this->post->current_media_applicant);   
            }
        }
        echo Debug::vars($this->post);    
        die();
    }
    
    public function finalize() {
        
        if (!empty($this->transaction->updates)) {
            foreach ($this->transaction->updates as $table => $instance) {
                $xwhere_column = strtolower($table) === strtolower($this->master->table) ? 'id' : $this->master->foreign_id;
                $instance->where($xwhere_column,'=',$this->master->increment);
            }
        }     
        
        //Get rid unecessary transaction
        if (isset($this->transaction->multi_checks)) {
            foreach ($this->transaction->multi_checks as $xtable => $xinstance) {
                foreach ($xinstance as $xindex_i => $xinstance_i) {
                    if (is_array($xinstance_i)) {
                        foreach ($xinstance_i as $xindex_ii => $xinstance_ii) {
                            $execute = $xinstance_ii->execute()->current();
                            if (empty($execute)) {
                                unset($this->transaction->multi_updates[$xtable][$xindex_i][$xindex_ii]);
                            } else {
                                unset($this->transaction->multi_inserts[$xtable][$xindex_i][$xindex_ii]);
                            }
                        }    
                    } else {
                        $execute = $xinstance_i->execute()->current();
                        if (empty($execute)) {
                            unset($this->transaction->multi_updates[$xtable][$xindex_i]);
                        } else {
                            unset($this->transaction->multi_inserts[$xtable][$xindex_i]);
                        }
                    }
                }
            }    
            unset($this->transaction->multi_checks);
        }
                       
        if ($this->debugging) {
            return $this->do_debug();
        }
        
        if (!empty($this->transaction->updates)) {
            foreach ($this->transaction->updates as $table => $instance) {                
                //Check for update time, for master table only
                if ($table == $this->master->table && $this->update_time !== false  
                        && !isset($this->pairs[$this->master->table]['update_time'])
                        && !is_array($instance)) {
                    $this->update_time = $this->update_time === true ? date("Y-m-d H:i:s") : $this->update_time;                             
                }
                
                
                $xclone = clone $instance;  
                $update = $instance->execute();
                
                if (!empty($update)) {
                    if ($table === $this->master->table) {
                        $this->_result[$table] = $this->master->increment;
                    } else {
                        if (empty($this->_result)) {
                            $this->_result = true;
                        }
                    }
                }
            }
        }
            
        if (!empty($this->transaction->multi_deletes)) {
            $this->query_execution($this->transaction->multi_deletes);
        }           
        if (!empty($this->transaction->multi_updates)) {
            $this->query_execution($this->transaction->multi_updates);
        }           
        if (!empty($this->transaction->multi_inserts)) {
            $this->query_execution($this->transaction->multi_inserts);
        }        
        if (!empty($this->transaction->translation_inserts)) {
            $this->query_execution($this->transaction->translation_inserts);
        }      
        if (!empty($this->transaction->translation_updates)) {
            $this->query_execution($this->transaction->translation_updates);
        }            
        if (!empty($this->transaction->fix_order)) {
            //$this->query_printing($this->transaction->fix_order); die();
            $this->query_execution($this->transaction->fix_order);
        }   
        if (!empty($this->transaction->media_application)) {
            $this->query_execution($this->transaction->media_application);
        }
        
        
        if (!empty($this->_result)) { 
            //Update time
            if (!empty($this->update_time)) {
                Model::factory('Dba')
                    ->update($this->master->table)
                    ->set('update_time',$this->update_time)
                    ->where('id','=',$this->master->increment)
                    ->execute();
            }
            //Append history
            History::record(array(
                'row_id'      => $this->master->increment,
                'table'       => $this->master->table,
                'actor'       => User::admin()->username,
                'action'      => 'update'
            ));    
        }
        
        return $this->_result;
    }
}