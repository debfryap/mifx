<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data transaction create
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors
 * 
**/ 


class Kohana_Data_Transaction_Create {
    
    private $post;
    
    private $pack;
    
    private $pairs;  
    
    private $master;  
    
    private $tables;
    
    private $multilang;    
    
    private $debugging;
    
    private $load_pack;
    
    private $allow_empty;
    
    private $primary_where_value;
    
    private $primary_where_column;
    
    private $create_time;
    
    private $_result;
    
    private $foreign;    
    
    public $transaction;
    
    public static function open(array $configs = array()) {
        return new Data_Transaction_Create($configs);
    }
    
    public function __construct(array $configs = array()) {
        // Check for language
        $this->multilang = empty(App::$module->language) ? false : $this->multilang;        
        
        //Set master as standard object        
        $this->master = new stdClass();    
        
        //Set transaction as standard object
        $this->transaction = new stdClass(); 
        
        //Update variables
        foreach ($configs as $key => $value) {
            if (strtolower(trim($key)) === 'master')
                $this->master->table = $value;
            else 
                $this->$key = $value;
        }
        
        //Make sure post is one level object
        $this->post = is_array($this->post) ? Helper_Kernel::arr_to_object($this->post,false) : $this->post;
                    
        //Load pack
        if (!isset($this->load_pack) && !empty($this->pack)) {            
            $this->load_pack = Data_Form_Pack::instance($this->pack,null,true);
        }
        
        //Check for transaction pair
        if (empty($this->pairs)) {
            if (!empty($this->load_pack->transaction)) {
                $this->pairs = $this->load_pack->transaction;
            }
        }        
        if (empty($this->pairs)) {
            throw new Kohana_Exception(__('error.no-pair',array(':type'=>'update')));
        }
        
        //Collect table transaction data, and assign exact value to the pairing variable
        foreach ($ipairs = $this->pairs as $table => $pairs) {
            $this->tables[] = $table; 
            foreach ($pairs as $column_name => $post_name) {
                
                $colomn_value = $this->mapping_value($post_name);
                
                $this->pairs[$table][$column_name] = $colomn_value;
                
                $column_name = strtolower($column_name);
                
                //Check order
                if ($column_name == 'ordering' && !isset($this->transaction->fix_order[$table])) {
                    $this->transaction->fix_order[$table] = 
                            Model::factory('Dba')
                                ->update($table)
                                ->set("ordering",DB::expr('`ordering` + 1'))
                                ->where("ordering >= $colomn_value");
                }
            }
        }            
        
        //Master table
        if (empty($this->master->table)) {
            $this->master->table = Data::check_parent($this->tables);
        }
        
        $this->master->status = Model::factory('Dba')
                            ->select()
                            ->query("SHOW TABLE STATUS LIKE '".$this->master->table."'")
                            ->execute()
                            ->current();
                            
                                          
        $this->master->increment = isset($this->master->status->Auto_increment) 
                    ? $this->master->status->Auto_increment
                    : 0;
        
        
        $this->compile();
    }
    
    /**
     */
    private function mapping_value($post_name) {
        if (!isset($this->post->$post_name)) {
            return $this->mapping_pack($post_name);
        }
            
        $post = $this->post->$post_name;
        
        $is_array = is_array($post);
        
        if (!$is_array) {
            if (substr(trim($post),0,9) === 'increment') {
                return false;
            } 
        }
        
        if ($is_array) {
            return $post;
        } elseif (is_object($post)) {
            return json_encode($post);
        } else {
            return trim($post);
        }
    }
    
    /**
     */
    private function mapping_pack($post_name) {        
        $load_pack = $this->load_pack;
        if (isset($load_pack)) {             
            if (empty($load_pack->element)) return false; //no pack to check
            
            foreach ($load_pack->element as $k => $v) {
                if ($v->name == $post_name) {
                    return isset($v->seeking_value) ? $v->seeking_value : '';
                }
            };                
            return false; //not found even in package
        }        
        return false; //no pack to check    
    }
    
    private function compile() {
        
        foreach ($this->pairs as $table => $pairs) {
            //Default table transactions
            $this->transaction->inserts[$table] = Model::factory('Dba')->insert($table);
            
            //Add parent id to child table
            if ($table !== $this->master->table) {
                $this->transaction->inserts[$table]->row(
                    Inflector::singular($this->master->table).'_id',$this->master->increment);    
            }
            
            foreach ($pairs as $column_name => $column_value) {
                    
                if ($column_value === false) {
                    continue;
                }
                
                $helper_field_ordering_parenting 
                    = isset($this->post->helper_field_ordering_parenting) 
                        && (is_array($this->post->helper_field_ordering_parenting) 
                            ? in_array($column_name,$this->post->helper_field_ordering_parenting) 
                            : $this->post->helper_field_ordering_parenting == $column_name);
                
                if ( $helper_field_ordering_parenting && isset($this->transaction->fix_order[$this->master->table]) ) { 
                    $this->transaction->fix_order[$this->master->table]->and_where("$column_name = $column_value");    
                }
                
                if (is_array($column_value)) {
                    /**
                     * There is 3(two) condition how our post could be an array in return.
                     * 1. Have 2(two)/more in active language
                     * 2. Master row have multiple row in another related table
                     * 3. Both of these 2(two) condition meet in post (in development) *grinnn*
                     * Let's handle it 
                     */
                     
                     //Check for language module load, and column value for system lang    
                     $is_master_language_post = !empty(App::$module->language) ? isset($column_value[Language::$system]) : false;                                                                
                                                               
                     if ($this->multilang === true && $is_master_language_post === true) {
                        //Condition 1 and 3
                        $this->query_translation($table,$column_name,$column_value,$unset);                          
                     } else {
                        //Condition 2
                                                                                 
                        if (isset($this->transaction->inserts[$table]) && empty($unset_master)) {                   
                            //Get rid previous default table transaction        
                            unset($this->transaction->inserts[$table]);
                            $unset_master = true;
                        }
                        
                        $iteration = 0;                                         
                        foreach ($column_value as $index => $column_value2) {
                            if (!isset($this->transaction->inserts[$table][$index])) {
                                //Replace with a new one
                                $this->transaction->inserts[$table][$index] = Model::factory('Dba')->insert($table);
                                
                                //Auto set master table id as foreign key to each related table
                                $this->transaction->inserts[$table][$index]->row(
                                    Inflector::singular($this->master->table).'_id',$this->master->increment);                           
                            } 
                            
                            $this->transaction->inserts[$table][$index]->row($column_name,$column_value2);  
                        }
                     }
                } elseif ($column_value !== false) {
                    if (isset($this->transaction->inserts[$table])) {
                        #$this->transaction->inserts[$table]->row($column_name,$column_value);
                        $this->query_row($this->transaction->inserts[$table],$column_name,$column_value);
                    }    
                }
            }
            
            //Master mean single query :)
            if ($this->create_time !== false && !isset($this->pairs[$this->master->table]['create_time'])) {
                $time = $this->create_time === true ? date("Y-m-d H:i:s") : $this->create_time;
                $this->transaction->inserts[$this->master->table]->row('create_time',$time);
            }   
        }  
        
        //Fix ordering
        if (!empty($this->transaction->fix_order)) {
            foreach ($this->transaction->fix_order as $table => $instance) {
                $and_where_field = $table === $this->master->table 
                        ? 'id' 
                        : Inflector::singular($this->master->table).'_id';                
                $this->transaction->fix_order[$this->master->table]->and_where(
                    $and_where_field,'<>',$this->master->increment);
            }  
        }   
        
        //Media library 
        if (!empty($this->post->media_application) && !empty(App::$module->medialibrary)) {            
            $this->transaction->media_application =  Medialibrary::media_application_transaction(
                                                            'create'
                                                            ,$this->master->increment
                                                            ,$this->post->media_application
                                                            ,$this->debugging);
        }
    }
    
    private function query_row($instance,$column,$value) {
        if (is_array($instance)) {
            foreach ($instance as $next_instance) {
                $this->query_row($next_instance,$column,$value);    
            }
        } else {
            if ($value !== "")
                $instance->row($column,$value);
        }
    }
    
    private function query_translation($table,$column,$post,&$unset) {   
        //Looping through active language, and not system language
        foreach (Language::$active as $lang) {
            
            if (!isset($post[$lang->code])) return;
                        
            $value = $post[$lang->code];
            
            $is_array_value = is_array($value);
                    
            if ($lang->code == Language::$system ) {                
                if ($is_array_value) {
                    if ($table == $this->master->table) {
                        $temp = null;
                        foreach ($value as $k => $v) {
                            $temp = isset($temp) ? $temp."::".$v : $v;
                        }                        
                        $value = $temp;
                        $this->transaction->inserts[$table]->row($column,$value);
                    } else { 
                        $this->query_lang_filter($table,$column,$value,$lang->code,$unset);    
                    }    
                } else {
                    if (isset($this->transaction->inserts[$table])) {
                        if ($value == "") {
                            $value = $this->pairs[$table][$column][Language::$default];
                        }
                        $this->query_row($this->transaction->inserts[$table],$column,$value);
                    } 
                }
            } else {
                if ($is_array_value) { 
                    $this->query_lang_filter($table,$column,$value,$lang->code,$unset);
                } else {
                    if ($value !== '') 
                        $this->transaction->translations[$table][$table][$column][$lang->code] = 
                            Model::factory('Dba')->insert('translations')
                                ->row('language_code',$lang->code)
                                ->row('row_table',$table)
                                ->row('row_id',$this->master->increment)
                                ->row('row_column',$column)
                                ->row('row_value',$value);
                        
                }
            }
        }    
    }    
    
    private function query_lang_filter($table,$column,$post,$lang,&$unset) {
        if (isset($this->transaction->inserts[$table]) && empty($unset)) {
            //Get rid previous default table transaction        
            unset($this->transaction->inserts[$table]);
                
            $unset = true;
        }
        
        $iteration = 0;                                         
        foreach ($post as $index => $value) {
            if (!isset($this->transaction->inserts[$table][$lang][$iteration])) {
                $this->transaction->inserts[$table][$lang][$iteration] = 
                    Model::factory('Dba')
                        ->insert($table)
                        ->row(Inflector::singular($this->master->table).'_id',$this->master->increment)
                        ->row('lang_code',$lang);
            }
            
            $this->transaction->inserts[$table][$lang][$iteration]->row($column,$value);
            
            $iteration++;
        }
    }
        
    private function query_printing($data) {
        if (is_array($data)) {
            foreach ($data as $Q) {
                $this->query_printing($Q);
            }
        } else {
            echo $data,'<br/>';
        }
    }
                
    private function query_execution($transactions,$next_table=null,$check_table=false) {
        foreach ($transactions as $table => $instance) {
            $allowed = $check_table === true 
                        ? !empty($this->master->table) && $table !== $this->master->table
                        : true;
                        
            if ($allowed) {                
                if (is_array($instance)) {          
                    $this->query_execution($instance,$table,$check_table);
                } else { 
                    $insertion = $instance->execute();
                    $insert_id = !isset($insertion[0]) ? 0 : $insertion[0];
                    
                    /** **
                    //Append history
                    History::record(array(
                        'row_id'      => $insert_id,
                        'table'       => !isset($next_table) ? $table : $next_table,
                        'actor'       => User::admin()->username
                    ));
                    /** **/
                }
            } 
        }
    }
    
    private function do_debug() {
        echo 'A. Query Transaction :';
        echo '<p></p>';
        if (!empty($this->transaction->inserts)) {
            $this->query_printing($this->transaction->inserts);
        }
        
        if (!empty($this->transaction->translations)) {
            $this->query_printing($this->transaction->translations);
        }
        
        if (!empty($this->transaction->fix_order)) {
            $this->query_printing($this->transaction->fix_order);
        }
        
        if (!empty($this->transaction->media_application)) {
            $this->query_printing($this->transaction->media_application);
        }
        
        echo '<p></p>';    
        echo 'B. Pairing data, [table][column][value] : ';
        echo Debug::vars($this->pairs);     
        if (isset($this->post->media_application)) {
            echo 'Media Application : ';
            echo Debug::vars($this->post->media_application);   
        }
        
        die();
    }
    
    public function finalize() {
        
        if ($this->debugging) {
            return $this->do_debug();
        }
        
        //Execution master
        if (!empty($this->master->table)) {
            $insert_master = $this->transaction->inserts[$this->master->table]->execute();
        }        
            
        if (!empty($insert_master)) {
            /** **/
            //Append history
            History::record(array(
                'row_id'      => $this->master->increment,
                'table'       => $this->master->table,
                'actor'       => User::admin()->username
            ));
            /** **/    
            
            //Rest execution
            if (!empty($this->transaction->inserts)) {
                $this->query_execution($this->transaction->inserts,null,true);
            }
            
            //Translation
            if (!empty($this->transaction->translations)) {
                $this->query_execution($this->transaction->translations);
            }
            
            //Fix ordering
            if (!empty($this->transaction->fix_order[$this->master->table])) {
                $this->query_execution($this->transaction->fix_order);
            }
            
            //Media
            if (!empty($this->transaction->media_application)) {
                $this->query_execution($this->transaction->media_application);
            }
            
            $this->_result[$this->master->table] = $insert_master[0];
        }
        
        return $this->_result;
    }
}