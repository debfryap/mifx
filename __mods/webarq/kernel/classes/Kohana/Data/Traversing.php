<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data db select traversing
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/


 class Kohana_Data_Traversing extends Populate {
    /**
     * @var String Column traversing
     */
    protected $column_traverse = 'parent_id';
    
    /**
     * @var String Column value for next traversing
     */
    protected $next_column_traverse_value = 'id';
    
    /**
     * @var Mixed Traversing starting value
     */
    protected $traverse_start = 0;
    
    /**
     * @var Int Traversing limit, 0 mean unlimited 
     */
    protected $deep = 0;
    
    /**
     * @var String Table name
     */
    protected $table;
    
    /**
     * @var Array  Table column's to select
     */
    protected $columns = '*';
    
    /**
     * @var Object Database instance
     */
    protected $db;
    
    /**
     * @var String returning data type (array or object)
     */
    protected $style = 'object';
    
    protected $order_by;
    
    /**
     *(Mixed)  array configuration 
     *(String) table name
     */
    public static function sampling($mixed) {
        return new Data_Traversing($mixed); 
    }
    
    /**
     * Ideally, configs is a string of table name, but sometimes we could passing it as an
     * array, if we need different traverse or next traverse value column.
     */
    public function __construct($mixed) {
        //Parsing configuration
        parent::__construct(is_array($mixed) ? $mixed : array('table'=>$mixed));
        
        //Set db instance
        if (empty($this->db)) {
            $this->db   = Model::factory('Dba')->select($this->columns)->from($this->table);
        }                
    }
    
    //(Mixed) (String) Column name
    public function __columns($field) {
        //Rewrite db instance
        $this->columns = func_get_args();
        
        return $this;
    }
    
    protected function loop_data($traverse_value,$level=0,$parent_node=0) {
        $q = clone $this->db;
        
        $q = $main_query = $q->_instance; 
        
        $find_from  = strripos($main_query,"FROM");
        
        $find_limit = strripos($main_query,"LIMIT");
        
        $find_limit = $find_limit > $find_from ? $find_limit : false;           //Tricky
                
        if ($find_limit !== false) {
            $limit_query = trim(substr($main_query,$find_limit));            
            $main_query  = trim(substr($main_query,0,$find_limit-1));
        }
        
        $find_group = strripos($main_query,"GROUP BY");
        
        $find_group = $find_group > $find_from ? $find_group : false;           //Tricky
        
        if ($find_group !== false) {
            $group_query  = trim(substr($main_query,$find_group));
            $main_query   = trim(substr($main_query,0,$find_group-1));
        }
        
        $find_where = strripos($main_query,"WHERE");
        
        $find_where = $find_where > $find_from ? $find_where : false;           //Tricky
                   
        $proper_ctr   = $this->make_proper_field($this->column_traverse);
            
        if ($find_where !== false) {
            $where_query  = trim(substr($main_query,$find_where));     
            $main_query   = trim(substr($main_query,0,$find_where-1));
            
            //$ploding      = explode("`$this->column_traverse`",$where_query,2); 
            $ploding      = explode("$proper_ctr",$where_query,2);
            
            $where_query  = trim($ploding[0]);
            
            if (isset($ploding[1])) {
                $where_query .= " $proper_ctr";
                $ploding_1    = trim(substr(trim($ploding[1]),1));
                $first_quote  = strpos($ploding_1,"'");
                
                if ($first_quote === false || $first_quote !== 0) { //Mean traversing value is integer
                
                    $first_space = strpos($ploding_1," ");
                    
                    $where_query .= " = '$traverse_value'";
                    if ($first_space !== false) {
                        $where_query .= " ".trim(substr($ploding_1,$first_space));
                    }
                } elseif ($first_quote === 0) {
                    $where_query .= " = '$traverse_value'";
                    $ploding_2    = trim(substr($ploding_1,1));
                    $second_quote = strpos($ploding_2,"'");
                    $where_query .= " ".trim(substr($ploding_2,$second_quote+1)); 
                }
            } else {
                if (isset($traverse_value))
                    $where_query  .= " AND $proper_ctr = $traverse_value";
            }
        } else {
            if (isset($traverse_value))
                $where_query  = " WHERE $proper_ctr = $traverse_value";
        }
        
        
        $full_query = $main_query;
        if (isset($where_query)) $full_query .= " $where_query";
        if (isset($group_query)) $full_query .= " $group_query";
        
        if (!empty($this->order_by)) {        
            if (!is_array($this->order_by)) {
                $this->order_by = array($this->order_by=>'ASC');
            }
            
            foreach ($this->order_by as $field => $direction) {
                
                $fix_order = $this->make_proper_field($field);
                 
                $full_query .= empty($order_by) 
                                    ? " ORDER BY $fix_order $direction"
                                    : ", $fix_order $direction";
                $order_by    = true;
            }
        }
        
        if (isset($limit_query) && $level === 0) $full_query .= " $limit_query";
        //echo $full_query,'<br/>'; die();
        $d = Model::factory('Dba')->select()->query($full_query)->execute();
        
        $n = $this->next_column_traverse_value;
        
        if (!empty($d[0])) {
            foreach ($d as $k => $v) {
                if ($this->style === 'object') {                
                    $v->level       = $level;
                    $v->parent_node = $parent_node == 0 ? $parent_node : $parent_node.'-'.$v->id;
                    
                    if (empty($this->deep) || ($level < $this->deep)) 
                        $v->child       = $this->loop_data($v->$n,$level+1,$parent_node.'-'.$v->id);
                        
                } else {
                    $v = Helper_Kernel::object_to_array($v);
                    $v['level']       = $level;
                    $v['parent_node'] = $parent_node == 0 ? $parent_node : $parent_node.'-'.$v['id'];
                    
                    if (empty($this->deep) || ($level < $this->deep))
                        $v['child']       = $this->loop_data($v[$n],$level+1,$parent_node.'-'.$v['id']);
                        
                }
                
                $result[$k] = $v;    
            }     
        } else {
            return null;
        }
        
        return $result;
    }
        private function make_proper_field($field) {
            $explode_by_dot = explode('.',$field);
                
            return count($explode_by_dot) == 2 ? "`$explode_by_dot[0]`.`$explode_by_dot[1]`" : "`$field`";        
        }
    
    public function response() {
        return empty($this->table) 
                ? null
                : $this->loop_data($this->traverse_start);
    }
     
 }