<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data db create
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/


 class Kohana_Data_Transaction {
    //(Bool) Allow or not executing transaction when all related element form does not have value (only empty string)
    private $_allow_empty = false;
          
    //(Bool) Multilanguage transaction
    private $_multilang = false;
    
    /**
     * @var string transaction type (create,read,update,delete)
     */
    private $_type;
    
    /**
     * @var object model class
     */
    private $_model_class;
    
    /**
     * @var string method model class
     */
    private $_model_action;
    
    /**
     * @var array transaction pack 
     */        
    private $_pack = array();
    
    /**
     * @var array [table][column][post form] pairing
     */
    private $_pair = array();
    
    /**
     * @var array [table][foreign-key column] pairing
     */
    private $_foreign;
    
    /**
     * @var array post data 
     */
    private $_post;
    
    /**
     * @var array object transaction
     */
    private $_transaction;
    
    /**
     * @var string master table name
     */
    private $_master;
    
    /**
     * @var mixed boolean or string time format eg. datetime,date,time, or integer
     * affected only to master table
     * 
     * false : mean no use create_time
     * true  : will be use default international datetime format : Y-m-d H:i:s 
     */
    private $_create_time = true;
    
    private $_update_time = true;
        
    private $_compile = false;
    
    public $data;
    
    public function __construct($type) {
        $this->_type = $type;
        
        $post = Request::$initial->post();
        if (!empty($post)) {
            //Transaction used pairs
            if (isset($post['app_transaction_pairs'])) {
                $this->_pair = $post['app_transaction_pairs'];
                unset($post['app_transaction_pairs']);
            }
            
            //Transaction used package
            if (isset($post['app_transaction_pack'])){
                $this->pack(explode(',',$post['app_transaction_pack']));
                unset($post['app_transaction_pack']);
            }
            
            //Transaction used foreign key
            if (isset($post['app_transaction_foreign_key'])) {
                $this->_foreign = $post['app_transaction_foreign_key'];
                unset($post['app_transaction_foreign_key']);
            }
            
            // Multilanguage checking
            if (isset($post['app_transaction_multilanguage']) && $post['app_transaction_multilanguage'] == 'true') {
                $this->_multilang = true;
                unset($post['app_transaction_multilanguage']);    
            }
            
            unset($post['app_transaction_module']);
            unset($post['app_transaction_controller']);
            
            $this->_post = $post;
        }
    }
    
    /**
     * @param string model name
     */
    public function model($model,$action = null) {
       $this->_model_class  = Model::factory(ucfirst(strtolower($model)));
       $this->_model_action = $action === null ? $this->_type : $action;
       return $this; 
    }
    
    /**
     * set transaction table master
     * @param string table master name
     */
    public function master($string) {
        $this->_master = $string;
        return $this;
    }
    
    /**
     * set post data
     * @param string post form name
     * @param mixed post form value
     */
    public function post($name,$value=null) {
        if (is_array($name)) {
            foreach ($name as $kkk => $vvv) { //Looping Merge vs array_merge hahahaahahah
                $this->_post[$kkk] = $vvv;
            }
        } else {
            $this->_post[$name] = $value;
        }
        return $this; 
    } 
    
    /**
     * set pairing dat, format : [table][column][element]
     * will be used later, in process mapping value 
     * @param string table name
     * @param string column name
     * @param string post element name
     */
    public function pairs($table,$column=null,$post_form=null) {
        if (is_array($table)) {
            foreach ($table as $name => $pairs) {
                $this->_pair[$name] = !isset($this->_pair[$name]) ? $pairs : array_merge($this->_pair[$name],$pairs);
            }
        } elseif (is_array($column)) {
            $this->_pair[$table] = !isset($this->_pair[$table]) ? $column : array_merge($this->_pair[$table],$column);
        } else {
            $this->_pair[$table][$column] = !isset($post_form) ? $column : $post_form;
        }
        return $this;
    }
    
    /**
     * set foreign key of table, instead use of singular-table_id 
     * eg. users, user_details will use user_id as automatic foreign key :)
     * @param string table name
     * @param string column name
     * @param string post element name
     */
    public function foreign($table,$columns=null) {
        if (is_array($table)) {
            foreach ($table as $t => $c) {
                $this->_foreign[$t] = $c;
            }
        } else {
            $this->_foreign[$table] = $columns;
        }
        return $this;    
    }
    
    /**
     * @param mixed boolean or string time format eg. datetime,date,time, or integer
     */
    public function create_time($time) {
        $this->_create_time = $time;
        return $this;
    }
    
    /**
     * @param see create time
     */
    public function update_time($time) {
        $this->_update_time = $time;
        return $this;
    }
    
    public function compile($debug = false) {
        if (empty($this->_model_class) && !empty($this->_post)) {
            $transaction_type  = strtolower($this->_type);
            $transaction_class = "Data_Transaction_".ucfirst($transaction_type);
            
            $transaction_attrs = array(
                    'master'        => $this->_master,
                    'load_pack'     => isset($load_pack) ? $load_pack : null,
                    'pack'          => $this->_pack,
                    'pairs'         => $this->_pair,
                    'foreign'       => $this->_foreign,
                    'post'          => $this->_post,
                    'multilang'     => true, //$this->_multilang
                    'allow_empty'   => $this->_allow_empty, 
                    'debugging'     => $debug
                );
            
            if ($transaction_type == 'create') {
                $transaction_attrs['create_time'] = $this->_create_time;    
            } elseif ($transaction_type == 'update') {
                $transaction_attrs['update_time'] = $this->_update_time;
            }
            
            $this->data = $transaction_class::open($transaction_attrs);
            
            $this->_compile = true;
        }
        
        return $this;
    }   
     
    public function save($debug = false) {
        if (!empty($this->_model_class)) {
            $method = $this->_model_action;
            if (in_array($method,array('insert','delete','select','update'))) {
                $method = $method."s";
            }
            return $this->_model_class->$method();
        } else {
            if (empty($this->_post)) {
                throw new Kohana_Exception(__('error.no-post-data'));
            } else { 
                
                //Compile when compiler still empty
                if (empty($this->_compile)) {
                    $this->compile($debug);                
                }
                
                return $this->data->finalize();
            }
        }    
    }
    
    /**
     * @param string pack name
     * ..->pack(module-name:form-group-name)
     * ..->pack(array('module-name1:form-group-name1','module-name2:form-group-name2'))
     */
    private function pack($pack_name) {
        if (is_array($pack_name)) {
            $this->_pack = array_merge($this->_pack,$pack_name);
        } else {               
            $this->_pack[] = $pack_name;
        }
    }
    
 }