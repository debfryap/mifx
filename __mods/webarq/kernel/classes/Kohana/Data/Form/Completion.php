<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data form builder completion 
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors
 * 
 * 
**/ 


class Kohana_Data_Form_Completion {
    //(Object) element 
    protected $_object;
    
    //(Object) form element
    protected $_element;
    
    protected $_no_length = array('checkbox','select','radio');
    
    private $_index_form_group;
    
    public static function instance($element,$index=null) {
        $obj = new Data_Form_Completion($element,$index);
        return $obj->builder();
    }   
    
    public function __construct($element,$index = null) {
        //Clone element object, to avoid rewriting
        $this->_object = clone $element;
        
        //Index active form group element
        $this->_index_form_group = $index;
        
        //Set object form, from clone element
        $this->_element = !isset($this->_object->form) ? $this->_object : $this->_object->form;    
    }
    
    public function builder() {
        
        $this->complete_label();
        $this->complete_name();
        $this->complete_tb_column();
        $this->complete_id();
        $this->complete_type();
        $this->complete_length();
        $this->complete_state();
        $this->finishing_attributes();
        
        return $this->_object;
    }
    
    protected function complete_label() {
        if (empty($this->_element->label))
            $this->_element->label = $this->_object->field;
    }
    
    protected function complete_name() {
        if (empty($this->_element->name) || $this->_element->name == 'master')
            $this->_element->name = $this->_object->field;
    }
    
    protected function complete_tb_column() {
        $this->_element->tb_column = $this->_object->field;
    }
    
    protected function complete_id() {
        $this->_element->id = empty($this->_element->id) 
                                ? $this->_element->name 
                                : $this->_element->id;
        $this->_element->id = str_replace(array('[',']'),'',$this->_element->id);
    }
    
    protected function complete_type() {
        if (empty($this->_element->type)) {
            foreach (Kernel::$master->property->form as $type => $holder) {
                if (in_array($this->_object->type,$holder)) {
                    $this->_element->type = $type;
                    if ($this->_object->type == 'enum' && empty($this->_element->option)) {
                        $arrs = explode(',',$this->_object->option);
                        $this->_element->option = array_combine(array_values($arrs),$arrs);
                    } 
                    return;
                }
            };
        } elseif (is_array($this->_element->type)) {
            $tmp = $this->_element->type;
            
            $this->_element->type = isset($this->_index_form_group) && isset($tmp[$this->_index_form_group])
                                ? $tmp[$this->_index_form_group]
                                : $tmp[0];
        }
    }
    
    protected function complete_length() {
        if (!empty($this->_object->length) 
            && empty($this->_element->length)
            && !in_array(strtolower($this->_element->type),$this->_no_length))
            $this->_element->length = $this->_object->length;
    }
    
    /**
     * Define how to serve form checkbox, since we just have "on" or "null" in return.
     */
    protected function complete_state() {
        if (($this->_element->type == 'checkbox' || $this->_element->type == 'radio') &&
            (!isset($this->_element->state) || !is_array($this->_element->state)))
            $this->_element->state = array ('on' => 1, "off" => 0);
    }
    
    /**
     * Finishing check array group
     */
    private function finishing_attributes() {
        
        $exceptions = array('state','option');
        
        foreach ($this->_element as $key => $value) {
            if ($value === false) {
                unset($this->_element->$key);
            } elseif (is_array($value) && !in_array(strtolower($key),$exceptions)) {
                $tmp = $this->_element->$key;
                $this->_element->$key = isset($tmp[$this->_index_form_group])
                                    ? $tmp[$this->_index_form_group]
                                    : $tmp[0];
                                    
                if ($this->_element->$key === false) {
                    unset($this->_element->$key);
                } 
            }
        }
    }
}