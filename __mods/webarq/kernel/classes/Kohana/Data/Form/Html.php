<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data form builder html
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors
 * 
 * 
**/ 


Class Kohana_Data_Form_Html {
    //Form element 
    protected $element = null;
    
    //Form element attributes
    protected $attributes = array();
    
    // :)
    protected $_not_attributes = array (
                'type','value','selected',
                'name','wording','info','hint',
                'subtitle','option','group',
                'object','multilang','role','state',
                'order','table','preview','uploadify',
                'tb_column','label','seeking_value',
                'traverse','ordering','referrer','multireferrer','parenting',
                'check_by_lang','row_id','query'      
            ); 
    
    /** Create and return instance form element html **/
    public static function element($configs) {
        $x = new Data_Form_Html($configs); return $x->render();
    }
    
    public function __construct($configs=null) {
        if (!empty($configs)) {
            foreach ($configs as $key => $value) {
                $this->$key = $value;
            }
        }   
        
        //Default value
        $this->default_value();
              
        //Set element attributes
        $this->html_attributes();
        
        //Keep some value in array
        $this->keep_in_array();
    }
    
    public function render() {
        if (!empty($this->element->html))
            return $this->html_html();
        elseif (!empty($this->element->object))
            return $this->html_object();
        elseif (!empty($this->element->traverse))
            return $this->html_traverse();
        elseif (!empty($this->element->query))
            return $this->html_query();
        
        $method = "html_".$this->element->type; 

        return $this->$method();
    }
    
    private function default_value() { 
        //Assign null if not set or 
        if (!isset($this->element->value))
            $this->element->value = isset($this->element->default)
                                    ? $this->element->default
                                    : null;
    }
    
    private function keep_in_array() {
        $to_keep = array('option','value','selected');
                        
        foreach ($to_keep as $keep) {
            if (!empty($this->element->$keep) && is_object($this->element->$keep))
                $this->element->$keep = json_decode(json_encode($this->element->$keep),true);
        }
    }
    
    private function html_attributes() {
        foreach ($this->element as $key => $value) {
            if (!in_array($key,$this->_not_attributes)) {
                if (is_object($value)) {
                    $this->attributes[$key] = json_encode($value); //json_decode(json_encode($value),true)
                } elseif (is_array($value)) {                    
                    foreach ($value as $value2) {
                        if (isset($this->attributes[$key]))
                            $this->attributes[$key] .= " $value2";
                        else
                            $this->attributes[$key] = $value2;    
                    }                    
                } else {
                    if (isset($this->attributes[$key]))
                        $this->attributes[$key] .= " $value";
                    else
                        $this->attributes[$key] = $value; 
                }
            } else {
                if ($key === 'referrer') {
                    if (!isset($this->element->lang) 
                            || (!empty(App::$module->language)&& $this->element->lang == Language::$default) || !empty($this->element->multireferrer)) {
                        $this->attributes['data-referral'] = $value;
                        $this->attributes['class'] = isset($this->attributes['class'])
                            ? $this->attributes['class'].' referrer'
                            : 'referrer';
                    }
                }
            }
        }
        
        $this->attributes['class'] = isset($this->attributes['class']) ? "form-field ".$this->attributes['class'] : "form-field";
        
        if (!empty($this->attributes['length'])) { 
            $this->attributes['maxlength'] = $this->attributes['length'];
            unset($this->attributes['length']); 
        }
        
        if (isset($this->element->tb_column)) {
            $keyLower = strtolower($this->element->tb_column);
            
            if (($keyLower == 'parent_id' || strpos($keyLower,'category_id'))) {
                $this->attributes['class'] = isset($this->attributes['class'])
                    ? $this->attributes['class'] //.' tailing-url'
                    : ''; //'tailing-url';
            }
        }
       
        if (!empty($this->attributes['class'])) {
            $this->attributes['class'] = str_replace(
                                            'full-editor',
                                            User::admin()->role_id == 1 ? 'mega-editor' : 'full-editor',
                                            $this->attributes['class']);
            $this->attributes['class'] = str_replace(
                                            'simple-editor',
                                            User::admin()->role_id == 1 ? 'simplex-editor' : 'simple-editor',
                                            $this->attributes['class']);
        } 
    }
    
    private function html_file() {
        return Form::file($this->element->name,$this->attributes);
    }
    
    private function html_password() {
        return Form::password($this->element->name,$this->element->value,$this->attributes);
    }
    
    private function html_text() {
        return Form::input($this->element->name,$this->element->value,$this->attributes);
    }
    
    private function html_hidden() {
        return Form::hidden($this->element->name,$this->element->value,$this->attributes);
    }
    
    private function html_textarea() {
        return Form::textarea($this->element->name,$this->element->value,$this->attributes);
    }
    
    private function html_checkbox() {
        return $this->checkbox_radio('checkbox');
    }
    
    private function html_radio() {
        return $this->checkbox_radio('radio');
    }
    
        private function checkbox_radio($method) {
            //Default is un-check
            $checked = false;
            
            //Check for default value
            $value   = isset($this->element->value)
                            ? $this->element->value
                            : (isset($this->element->default) ? $this->element->default : 0);
                                    
            if (isset($this->element->state)) {            
                $state = is_array($this->element->state)
                            ? Helper_Kernel::arr_to_object($this->element->state)
                            : $this->element->state;
            }
            
            if (isset($this->element->checked) && is_bool($this->element->checked)) {
                $checked = $this->element->checked;
            } elseif (isset($state)) {            
                $checked = isset($state->on) && $state->on == $this->element->value ? true : false;
            }      
            
            //Reverse value based on state condition
            if (isset($state)) {
                $value = isset($state->on) && $state->on != $value ? $state->on : $value;
            }
            
            if (!empty($this->element->multiple)) {
                $html = '<div class="break5"></div>';
                $name = $this->element->name.'[]';
                $i    = 1;
                foreach ($this->element->option as $value => $label) {
                    $checked = is_array($this->element->value) && in_array($value,$this->element->value)
                                    ? true
                                    : $value == $this->element->value;
                    $html  .= '<div class="fl checkbox_container">'
                                      .Form::$method($name,$value,$checked,$this->attributes)
                                      .'</div>'
                                      .'<div class="fl checkbox_wording">'
                                      .__($label).'</div>';
                    $html  .= $i < count($this->element->option) ? '<div class="clear break15"></div>' : '<div class="clear break1"></div>';
                    $i++;
                }
            } else {                               
                $html  = '<div class="fl checkbox_container">'
                                  .Form::$method($this->element->name,$value,$checked,$this->attributes)
                                  .'</div>'
                                  .'<div class="fl checkbox_wording">'
                                  .__($this->element->label).'</div>'
                                  .'<div class="clear"></div>';
            }
            
            return $html;
        }
    
    private function html_select() {
        $options = empty($this->element->option) 
                            ? array('1'=>'Publish','0'=>'Unpublish') 
                            : $this->element->option;
        $options = is_array($options) ? $options : explode(',',$options);
        
        $selected = !empty($this->element->selected) ? $this->element->selected : $this->element->value;
        
        $name = !empty($this->element->multiple)
                    ? $this->element->name."[]"
                    : $this->element->name;
        
        if (isset($this->element->value)) {
            $this->attributes['data-current-value'] = is_array($this->element->value) ? json_encode($this->element->value) : $this->element->value;
        }
        
        unset($this->attributes['minlength']);
        unset($this->attributes['maxlength']);
        
        $select = Form::select($name,$options,$selected,$this->attributes);
        
        if (!empty($this->element->disabled)) {
            $select .= Form::hidden($this->element->name,$this->element->value);
        }
        
        return $select;
    }
    
    private function html_submit() {
        
    }
    
    private function html_button() {
        
    }
    
    private function html_html() {        
        return $this->element->html;  
    }
    
    private function html_object() {
        $object = $this->element->object;
        if (strpos($object,":")) {
            list ($class,$method) = explode(":",$object);
        } else {
            $class  = App::$route_params['package'];
            $method = $object; 
        }
        $class = ucfirst(strtolower($class));
        
        $std   = clone $this->element;        
        $std->attributes = $this->attributes;
        
        return $class::$method($std);
    }
    
    private function html_ordering() {
        $html = '';                        
        if (empty($this->element->table))
            return __('error.invalid-config');
        
        $attrs = isset($this->attributes) ? $this->attributes : array();
        
        $attrs['min'] = 1;
        
        $builder_max  = Model::factory('Dba')
                            ->select(DB::expr('COUNT(`id`) AS `total`'))
                            ->from($this->element->table);
        $html = '';
        
        if (isset($this->element->parenting)) {
            $parent_field = $this->element->parenting['field'];
            $parent_value = $this->element->parenting['value'];
            $html .= Form::hidden('helper_field_ordering_parenting',$parent_field);
            
            $builder_max->where($parent_field,"=",$parent_value);            
        }
        
        if ($this->element->job_type == 'edit') {
            $html .= Form::hidden('previous_ordering',$this->element->value);
            if (isset($this->element->parenting)) {
                $parent_field = $this->element->parenting['field'];
                $parent_value = $this->element->parenting['value'];
                //$builder_max->where($parent_field,"=",$parent_value);  
                $html .= Form::hidden('previous_ordering_parenting',$parent_value);
            }
        }
          
        $attrs['data-table'] = $this->element->table;
        $attrs['data-form-type'] = Request::$initial->param('param1');
        
        $tmp_max = $builder_max->execute()
                            ->current()
                            ->total;
                            
        $attrs['max'] = strtolower($this->element->job_type) == 'edit' ? $tmp_max : $tmp_max+1;
                                    
        if ($attrs['max'] == 1) {
            $attrs['readonly'] = 'readonly';
        }            
        
        $value = isset($this->element->value) ? $this->element->value : $attrs['max'];
        
        return Form::input($this->element->name,$value,$attrs) . $html;
    }
    
    private function html_traverse() {
        if (empty($this->element->table) && empty($this->element->traverse->table)) {
            return __('error.invalid-config');
        }
        
        $name = !empty($this->element->multiple)
                    ? $this->element->name."[]"
                    : $this->element->name;
        
        
        if (isset($this->element->value))
            $this->attributes['data-current-value'] = $this->element->value;
        
        unset($this->attributes['minlength']);
        unset($this->attributes['maxlength']);
            
        $select  = '<select name="'.$name.'" '.HTML::attributes($this->attributes).'>';
        
        if ( !isset($this->element->traverse->top_level) || $this->element->traverse->top_level === true || ($this->element->job_type == 'edit' && $this->element->value == 0 ) ) {
            $select .= '<option value="0">'.(__('this-is-parent')).'</option>';
        } else {
            $select .= '<option value="">'.(__('please-select')).'</option>';
        }
        
        $select .= $this->option_select_traverse();
        $select .= '</select>';
        
        if (!empty($this->element->disabled)) {
            $select .= Form::hidden($this->element->name,$this->element->value);
        }
        return $select;        
    }        
                    
        private function option_select_traverse($parent = 0,$level=0) {
            
            $traverse = $this->element->traverse;
                
            //Traversing limitation
            $deep = isset($traverse->deep) ? $traverse->deep : (isset(App::$config->system->traversing_deep) ? App::$config->system->traversing_deep : 0);
            
            if (!is_numeric($deep) || $deep <= 0) 
                return '';
            
            $option_html = '';
             
            $traverse_table = isset($traverse->table) ? $traverse->table : $this->element->table;
            
            $traverse_field = empty($traverse->field) ? 'parent_id' : $traverse->field;
            
            $traverse_value = empty($traverse->value) ? 'id' : $traverse->value;
            
            $traverse_label = empty($traverse->label) ? 'label' : $traverse->label;
            
            $ordering       = isset($ordering) && empty($ordering) ? false : true; 
            
            $to_select      = array($traverse_value,$traverse_label,$traverse_field);
            
            if (!empty($traverse->permalink)) {
                $permalink = $traverse->permalink === true ? 'permalink' : $traverse->permalink; 
                $to_select[] = $permalink;
            }
            
            if (!empty($traverse->exception)) {
                foreach ($traverse->exception as $exception_field => $exception_value) {
                    $to_select[] = $exception_field;
                }
            }
            
            $builder   = Model::factory('Dba')->select($to_select);
            
            $builder->from($traverse_table)->where("$traverse_field = $parent");
            
            if ($ordering === true) {
                $builder->order_by("ordering","asc");
            }
            
            $items = $builder->execute();
            
            if (empty($items[0])) {
                return null;
            }
            
            foreach ($items as $idx => $row) {
                
                if ( isset($traverse->multi_relation) && ( $traverse->multi_relation === false || is_array($traverse->multi_relation) || is_object($traverse->multi_relation) ) ) {
                    $relation_table = isset($traverse->table_relation) 
                                ? $traverse->table_relation
                                : (empty($this->element->table) 
                                        ? null 
                                        : $this->element->table);
                }
                
                if ($this->element->job_type === 'edit') {
                    if (!isset($relation_table) || $relation_table === $traverse_table) {
                        if (empty($this->element->row_id) || !is_array($this->element->row_id)) {
                            throw new Kohana_Exception('Error. Please check your element row id');
                        } else {
                            list($row_id_element,$row_id_value) = $this->element->row_id;
                            if ($row->$row_id_element === $row_id_value && $this->element->tb_column  == 'parent_id') {
                                continue;
                            }
                        }
                    }
                }
                
                $option_html .= '<option data-level ="'.$level.'" value ="'.$row->$traverse_value.'" ';
                
                $is_selected  = is_array($this->element->value) ? in_array($row->$traverse_value,$this->element->value) : $this->element->value === $row->$traverse_value;
                
                $option_html .=  $is_selected ? 'selected="selected" ' : '';
                
                if ( isset($relation_table) && is_array($traverse->multi_relation) ) {
                    $in_array = Logic::check_exception($row,array($traverse_value=>$traverse->multi_relation));
                    if ($in_array) unset($relation_table);        
                }  
                                    
                if ( isset($relation_table) ) {
                                    
                    $field_relation = !isset($traverse->field_relation) 
                            ? (isset($traverse->table) ? Inflector::singular($traverse_table)."_id" : "id")
                            : $traverse->field_relation;
                
                    $get_item = Model::factory('Dba')
                                        ->select($field_relation)
                                        ->from($relation_table)
                                        ->where("$field_relation","=",$row->$traverse_value)
                                        ->limit(1)
                                        ->execute()
                                        ->current();
                                                                               
                    if (!empty($get_item)) {
                        $disabled = $this->element->job_type === 'create'
                                        ? true
                                        : ($this->element->value === $row->$traverse_value ? false : true);
                    }
                }
                
                
                if (!empty($traverse->exception)) {
                    foreach ($traverse->exception as $exception_field => $exception_value) {
                        $in_exception = Logic::check_exception($row,array($exception_field=>$exception_value));
                        if ($in_exception === true) {
                            $disabled = true;
                        }
                    }
                }
                
                if (isset($disabled) && $disabled === true) {
                    $option_html .= ' disabled="disabled"';
                    unset($disabled);
                }
                
                if (isset($permalink)) {
                    $option_html .= ' data-permalink="'.$row->$permalink.'"';
                }
                
                $option_html .= '>';
                
                for ($i = 0; $i < $level; $i++)
                    $option_html .= $i == 0 ? "--" : "&nbsp;--";
                
                $option_html .= $level > 0 ? '&nbsp;&nbsp;' : '';
                
                $option_html .= $row->$traverse_label;
                
                $option_html .= '</option>';
                
                //Check for traversing child
                if ($level < ($deep-1)) {
                    $option_html .= $this->option_select_traverse($row->$traverse_value,$level+1);
                }
            }
            
            return $option_html;
        }
        
    private function html_query() {
        $query = $this->element->query;
            
        $table = isset($query->table) ? $query->table : $this->element->table;
        
        $value = empty($query->value) ? 'id' : $query->value;
        
        $label = empty($query->label) ? 'label' : $query->label;
            
        $build = Model::factory('Dba')->select($value,$label)
                                          ->from($table);
        
        if (!empty($query->order)) {
            $o = $query->order;
            if ($o === true) {
                $build->order_by('ordering');
            } elseif (is_string($o)) {
                $build->order_by($o);
            } elseif (is_object($o) || is_array($o)) {
                foreach ($o as $f => $d) {
                    if (is_numeric($f))
                        $build->order_by($d);
                    else
                        $build->order_by($f,$d);    
                }
            } 
        }                                          
                                          
        $gets = $build->execute();
        
        $options = array(''=>'Please select');
        if (!empty($gets)) {
            foreach ($gets as $get)
                $options[$get->$value] = $get->$label;
        }
        
        $this->element->option = $options;
        
        return $this->html_select();
    }
}