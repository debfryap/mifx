<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Data form builder pack
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * Please remember, that we always 0 as starting point for element array indexing.
 * 
**/
 

class Kohana_Data_Form_Pack {
    //(Array) form package name
    protected $_modules = array();
    
    //(Array) form group name
    protected $_groups = array();
    
    //(Array) final element array
    protected $_elements = array();
    
    //(Array) temporary element array
    protected $_temp_elements = array();
    
    //(Int) total elements
    protected $_total_element;
    
    //(Array) configuration key using for element order
    //protected $_order_keys = array('first','before','after');
    
    //(String) swap or push :) 
    protected $_order_method = 'push';   
    
    //(Array) infected transaction tables 
    public $tables;
    
    //(Array) Returning elements
    public $element = array();
    
    //(Integer) index group element
    public static $_group_index;
    
    /**
     * @var Array element of transaction data
     * Pairing in table => column_name => element_name
     */
    public $transaction = array();
    
    /**
     * @var string package name
     * @var mixed array/json new form elements     
     */     
    public static function instance($pack,$e = null,$m = false) {
        return new Data_Form_Pack($pack,$e,$m);    
    }
    
    /**
     * @var array module package 
     * @var mixed array/json form elements
     * @var boolean mapping form element
     */
    public function __construct($pack,$e = null, $m = false) {
        if (!empty($pack)) {
            foreach ($pack as $value) {
                $module = $this->_modules[] = Helper_File::name($value,':');
                $group  = $this->_groups[]  = Helper_File::type($value,':');
                $this->collect_element($module,$group);
            }
        }
        
        //Merge extend element
        $this->extend_element($e);
        
        //Count total element
        $this->_total_element = count($this->_elements);
        
        //if ($m === false)        
        //Re-Ordering Array
        $this->element_order();
    }
    
    /**
     * Collect affected element, from module package
     * @param (string) module name
     * @param (string) form group name
     */
    protected function collect_element($module,$group) {
        $loaded_pack = Package::load($module,'installer');
        if (!empty($loaded_pack)) { 
            foreach ($loaded_pack as $table => $columns) {
                //Try to shadow master configuration                                
                $columns = Package::shadowing_master_table($columns);
                
                if (empty($columns)) continue;
                                
                foreach ($columns as $column) {                    
                    //Check for role permission
                    $passed = isset($column->form->role) && User::admin()->role_id <= $column->form->role
                                ? true : (isset($column->form->role) ? false : true);   
                                        
                     if (!empty($column->form->group)) {
                        //Make sure form group is array
                        $_available_group = is_array($column->form->group) ? $column->form->group : array($column->form->group);
                                                
                        //Only matched group will buffering                                                
                        if (in_array($group,$_available_group)&& $passed === true) {  
                            //Check keys for current selected group
                            $index_group = array_search($group,$_available_group);
                            if(!isset(Data_Form::$active_form[$module])) {
                                Data_Form::$active_form[$module] = $index_group;
                            }  
                        
                            //Check for form input type and length :)
                            $column->form->table = $table;
                            $column = Data_Form_Completion::instance($column,$index_group);
                            
                            if (!empty($column->form->order) && strtolower($column->form->order) == 'first') {
                                $this->push_up(count($this->_elements),0,$column);    
                            } else {
                                $this->push_current($column);
                            }
                            
                            /**
                             * Table column pairing technique
                            **/ 
                            $this->transaction[$table][$column->field] = $column->form->name;    
                            
                            
                            if (empty($this->tables) || !in_array($table,$this->tables)) {
                                $this->tables[] = $table;
                            }
                        }
                    }
                } 
                   
            }
        }
    }
    
    protected function extend_element($e) {
        if (!empty($e) && is_array($e)) {
            foreach ($e as $k => $v) {
                if (!empty($v->special)) {
                    $s = new stdClass();
                    $s->form = $v;
                    $this->_elements[] = $s;
                    continue;
                }
                
                //Check for role permission
                $passed = isset($v->role) && User::admin()->role_id <= $v->role
                            ? true : (isset($v->role) ? false : true);
                
                if ($passed === true) {
                    
                    $s = clone $v;
                    $s->form  = clone $v;
                    $s->field = empty($s->field) ? $s->name : $s->field;
                    $c = Data_Form_Completion::instance($s);
                    
                    if (!empty($s->table))
                        $this->transaction[$s->table][$s->field] = $s->form->name;
                        
                    //Push to elements
                    $this->_elements[] = $c;
                }
            }
        }
    }
    
    //Re-ordering handler method
    protected function element_order() {
        
        foreach ($this->_elements as $index => $element) {
            if (!empty($element->form->order) && strtolower($element->form->order) != 'first') {
                $temporary  = explode(" ",$element->form->order,2);
                $order_key  = strtolower(current($temporary));
                $target_key = $this->element_key($temporary[1]);
                $indexing   = strtolower($order_key) == 'after' ? 1 : -1;
                
                if ($target_key !== false) {
                    $target_element = $this->_elements[$target_key];
                    $element_key    = $this->element_key($element->field);
                                        
                    if ($this->_order_method == 'push') {
                        $this->push_order($element_key,$target_key+$indexing,$element);
                    } elseif ($this->_order_method == 'swap') {
                        $this->swap_order($element_key,$target_key+$indexing,$element);
                    }
                }
            }
        }
        
        //Filling up element, with form attribute only :) + seeking value
        foreach ($this->_elements as $index => $element) {
            $this->element[$index] = $element->form;
            if (!isset($this->element[$index]->default) && isset($element->default)) {
                $this->element[$index]->seeking_value = $element->default;
            } 
        }
    }
    
    //Get array index by column field name
    protected function element_key($field_name) {
        foreach ($this->_elements as $index => $column) {
            if ($column->field == $field_name) {
                return $index;
            }
        }
        return false;    
    }
    
    //Reorder element push method
    protected function push_order($current_index,$new_index,$element) {
        //Push (up) or (down) another element in range
        if ($current_index > $new_index)
            $this->push_up($current_index,$new_index,$element);
        elseif ($current_index < $new_index)
            $this->push_down($current_index,($new_index-1),$element);
    }
    
    protected function push_current($element) {
        $this->_elements[] = $element;
    }
    
    protected function push_up($current_index,$new_index,$current_element) {
        for ($i=$current_index;$i>$new_index;$i--) {
            $this->_elements[$i] = $this->_elements[$i-1];
        }
        
        //Set actual element
        $this->_elements[$new_index] = $current_element;
    }
    
    protected function push_down($current_index,$new_index,$current_element) {
        for ($i=$current_index;$i<$new_index;$i++){
            $this->_elements[$i] = $this->_elements[$i+1];
        }
        
        //Set actual element
        $this->_elements[$new_index] = $current_element;
    }
    
    //Reorder element swap method
    protected function swap_order($current_index,$new_index,$current_element) {
        $this->_elements[$current_index] = $this->_elements[$new_index];
        $this->_elements[$new_index] = $current_element;
    }
}