<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Post data handler
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
**/


class Kohana_Data_Listing extends Populate {
    /**
     * @var string  html string  
     */
    protected $html = '';
    
    /**
     * @var int total listing item
     */
    protected $total;
    
    /**
     * @var int limit per listing
     */
    protected $limit;
    
    /**
     * @var int current page number
     */
    protected $page;
    
    /**
     * @var string html pagination
     */
    protected $paging;
    
    /**
     * @var string date format
     */
    protected $date_format = 'M d, Y';
    
    /**
     * @var array (o) listing items
     */
    protected $items;        
    
    /**
     * @var string (if) used db model
     * at least db model class must have limit and tabular method
     */
    protected $db;
    
    /**
     * @var array tabular column header
     */
    protected $headers;    
    
    /**
     * @var bool use numbering column header
     */
    protected $numbering = true;
    
    /**
     * @var bool use create time column header
     */
    protected $create_time = true;
    
    /**
     * @var bool use update time column header
     */
    protected $update_time = true;
    
    /**
     * @var array action listing
     */
    protected $_action = array();
    
    /**
     * @var array tool listing
     */
    protected $_tool = array (
                        "search"  => "label",
                        "sort_by" => true
                        );                  
    
    /**
     * @var array post tool
     */
    protected $_data_tool;
    
    /**
     * @var 
     */
    protected $_sortable = false;
    
    /**
     * @var integer total header 
     */
    protected $total_header = 0;
    
    /**
     * @var string tabular style : using css or table
     * view method, using css table instead html table, or reverse
     */  
    protected $_style = 'css';
    
    /**
     * @var string traversal field
     * activate traversing tabular mode
     */
    protected $_traversing = null;
    
    /**
     * @var String Column traversing
     */
    protected $_traverse_column = 'parent_id';
    
    /**
     * @var String Column value for next traversing
     */
    protected $_traverse_next_column_value = 'id';
    
    /**
     * @var Mixed Traversing starting value
     */
    protected $_traverse_start = 0;
    
    
    /**
     * @var string order by field
     */
    protected $_order_by = null;
    
    
    public $model_data;
    
    public function __construct($configs = null) {
        $configs = empty($configs) ? array() : (is_string($configs) ? array('model'=>$configs) : $configs);
        
        parent::__construct($configs);
        
        //Tool preparation
        $this->prepare_tool();
        
        if (!empty($this->db)) {
            $this->model_data = new stdClass();
            $this->db->data_tool =  $this->_data_tool;
            $this->model_data->total   = $this->db->total();     
        }
    }  
    
    private function prepare_tool() {
        $package    = strtolower(App::$route_params['package']);
        $controller = str_replace("-","",strtolower(App::$route_params['controller']));
        
        //Data tool
        $key_active_tool = $package."_".$controller;
        
        $post = Request::$current->post();
        
        $data_tool = App::$session->get(App::cookie_name('lst-tool'),null);
        
        if (!empty($post)) {
            foreach ($this->_tool as $tool => $val) {
                if ($tool === 'sort_by') {
                    $data_tool[$key_active_tool][$tool] = !empty($val) && isset($post['tool'][$tool])
                                    ? $post['tool'][$tool]
                                    : (isset($post['sort_by']) 
                                            ? null 
                                            : ( !empty($data_tool[$key_active_tool][$tool]) 
                                                ? $data_tool[$key_active_tool][$tool] 
                                                : null));
                } else {
                    $data_tool[$key_active_tool][$tool] = !empty($val) && isset($post['tool'][$tool])
                                    ? ($post['tool'][$tool] != "" 
                                        ? $post['tool'][$tool] 
                                        : null)
                                    : (!empty($data_tool[$key_active_tool][$tool]) 
                                        ? $data_tool[$key_active_tool][$tool] 
                                        : null);
                }
            }      
            
            App::$session->set(App::cookie_name('lst-tool'),$data_tool);
        }
        $this->_data_tool = isset($data_tool[$key_active_tool]) ? $data_tool[$key_active_tool] : null;
    }
    
    private function validate() {
        if (!empty($this->db)) { 
            
            //Validate total item            
            if (empty($this->total)) {                   
                if (is_object($this->model_data->total)) {                     
                    // Keyword searching
                    if (isset($this->_data_tool['search']) && !empty($this->_tool['search'])) {
                        $this->model_data->total->where_open();
                        $search_field = is_array($this->_tool['search']) ? $this->_tool['search'] : array($this->_tool['search']);
                        
                        foreach ($search_field as $fieldWhere => $valueWhere) {
                            //Check for query field
                            $fieldWhere = is_numeric($fieldWhere) ? $valueWhere : $fieldWhere;
                            
                            //Append query total
                            $this->model_data->total->or_where("$fieldWhere LIKE %".$this->_data_tool['search']."%");
                        }
                            
                        $this->model_data->total->where_close();    
                    }
                    $this->total = $this->model_data->total->execute()->current()->total;
                } else {
                    $this->total = $this->model_data->total;          
                }             
            }

                
            //Validate limit item
            if (empty($this->limit))
                $this->limit = App::$config->perpage;
            
            //Calling pagination
            $this->paging = Pagination::factory(array(
                                'total_items'       => $this->total,
                        		'items_per_page'    => $this->limit,
                        		'view'              => 'pagination/floating',
                                'count_out'         => 3,
                                'count_in'          => 1
                            ));
            
            if (!empty($this->db)) {
                $this->model_data->tabular = $this->db->tabular($this->limit,$this->paging->offset);     
            }
            
            //Validate current page
            if (empty($this->page)){
                $this->page = Request::$initial->query('page');
                if (empty($this->page)){
                    $this->page = 1;
                }
            }                
                                        
            //Validate listing item
            if (empty($this->items)) {
                if (is_object($this->model_data->tabular)) {                        
                    //Session order table
                    $sort_by_field = isset($this->_data_tool['sort_by'])
                                        ? $this->_data_tool['sort_by']
                                        : null;
                        
                    if (empty($this->_traversing)) {
                        $builder = $this->model_data->tabular
                                        ->limit($this->limit)
                                        ->offset($this->paging->offset);
                        
                        if (empty($sort_by_field)) {
                            $sort_by_field = $this->_order_by;
                        }
                        
                        // Keyword searching
                        if (isset($this->_data_tool['search']) && !empty($this->_tool['search'])) {
                            $builder->where_open();
                            $search_field = is_array($this->_tool['search']) ? $this->_tool['search'] : array($this->_tool['search']);
                            
                            foreach ($search_field as $fieldWhere => $valueWhere) {
                                //Check for query field
                                $fieldWhere = is_numeric($fieldWhere) ? $valueWhere : $fieldWhere;
                                
                                //Append query total
                                $builder->or_where("$fieldWhere LIKE %".$this->_data_tool['search']."%");
                            }
                                
                            $builder->where_close();    
                        } 
                       
                        // Short by check
                        if (!empty($sort_by_field)) {
                            if (is_array($sort_by_field)) {
                                foreach ($sort_by_field as $field => $direction) {
                                    $builder->order_by($field,$direction);
                                }
                            } else {
                                $builder->order_by($sort_by_field,'ASC');
                            }
                        }
                        
                        $this->items = $builder->execute();
                    } else {
                        $traverse = Data::traversing(false);
                        
                        $builder = $this->model_data->tabular->limit($this->limit)->offset($this->paging->offset);
                        
                        // Keyword searching
                        if (isset($this->_data_tool['search']) && !empty($this->_tool['search'])) {
                            $builder->where_open();
                            $search_field = is_array($this->_tool['search']) ? $this->_tool['search'] : array($this->_tool['search']);
                            
                            foreach ($search_field as $field) 
                                $builder->or_where("$field LIKE %".$this->_data_tool['search']."%");
                                
                            $builder->where_close();   
                            
                            /**
                             *  Unfortunately we must search the first one which is match to search value
                             */
                            $c = clone $builder->_instance;      
                                      
                            if (!empty($this->_order_by)) {
                                if (is_array($this->_order_by)) {
                                    foreach ($this->_order_by as $of => $od) {
                                        $c->order_by($of,$od);
                                    }
                                } else {
                                    $c->order_by($sort_by_field,'ASC');
                                }
                            }
                            
                            $cget   = $c->limit(1)->execute();
                            $tstart = $cget->get($this->_traverse_column);
                            
                            $this->_traverse_start = !empty($tstart) ? $tstart : $this->_traverse_start;
                            /**
                             * Finishh ... */
                        } 
                        
                        if (empty($sort_by_field)) {
                            $sort_by_field = $this->_order_by;
                        }
                                                                                                                                                
                        $traverse
                            ->__set('db',$builder)
                            ->__set('order_by',$sort_by_field)                            
                            ->__set('column_traverse',$this->_traverse_column)
                            ->__set('next_column_traverse_value',$this->_traverse_next_column_value)
                            ->__set('traverse_start',$this->_traverse_start)
                            ->__set('traverse_search',isset($this->_data_tool['search']) ? $this->_data_tool['search'] : null);
                                
                        $this->items = $traverse->response();                            
                    }    
                } else {
                    $this->items = $this->model_data->tabular;
                }
            }
        }    
    }
    
    private function traversing_item($traversing_value=0, $level=0) {
        
        $cloning_instance = clone $this->model_data->tabular;
                            
        //Data result
        if ($level == 0) {
            $cloning_instance->limit($this->limit)->offset($this->paging->offset);
        }
        
        $cloning_instance->where("$this->_traversing",'=',$traversing_value);
        
        $d = $cloning_instance->execute();
        
        unset($cloning_instance);
        
        if (!empty($d[0])) {
            foreach ($d as $k => $v) {
                $v->level   = $level;
                $v->child   = $this->traversing_item($v->id,$level+1);
                $result[$k] = $v;
                        
            }     
        } else {
            return null;
        }
        
        return $result;
    }
    
    private function html_add_action() {
        if (!empty($this->_action['create'])) {
            $action = $this->_action['create'];
            if (is_array($action)) {                
                //Check for authorisation
                $authorise = empty($action['authorise']) ? true  : User::authorise($action['authorise']);
                
                $label = !empty($action['label']) ? $action['label'] : __('add-new');
                
                //Determine action href
                if (!isset($action['href'])) {
                    $action['href'] = 'form/create';
                }
                
                $href  = $this->href_value_string('create',$action);      
            } 
            
            if ($authorise) {
                $this->html .= '<div id="create-button">';
                $this->html .= '<a href="'.$href.'">';
                $this->html .= "<span>$label</span>";
                $this->html .= '</a>';
                $this->html .= '</div>';
            }
            
            
            //Get rid item create as we do not need it anymore
            unset($this->_action['create']);
        }   
    }
    
    private function html_tool() {
        
        //Html tool generator
        $this->html .= '<div class="tool-wrapper">';
        
        if (!empty($this->_tool)) {
            foreach ($this->_tool as $key => $config) {
                $func = "html_tool_$key";
                if (method_exists($this,$func))
                    $this->$func($config);
            }
        }
        $this->html .= '</div>';  
        $this->html .= '<div class="clear break20"></div>';  
    }
    
        private function html_tool_sort_by($config) {
            if ($config === false) return;
            
            $data_tool   = isset($this->_data_tool['sort_by']) ? $this->_data_tool['sort_by'] : null;
            
            $this->html .= '<div class="item-tool sort_by">';            
            $this->html .= Form::open(URL::site(Request::$initial->uri(), TRUE));
            $this->html .= '<ul class="background-css '.(!empty($data_tool) ? " active" : "").'">';
            $this->html .= '<li>';
            $this->html .= __('sort-by');
            
            $this->html .= '<ul class="item-tool-selection">';
            foreach ($this->headers as $key => $attr) {
                $class       = '';
                $this->html .= '<li class="'.$class.'">';
                $this->html .= '<div class="checkbox">';
                $checked     = isset($data_tool[$key]);
                $value       = isset($data_tool[$key]) ? $data_tool[$key] : null;
                $this->html .= Form::checkbox('tool[sort_by]['.$key.']',$value,$checked);
                $this->html .= '</div>';
                $this->html .= '<div class="label';
                $this->html .= !empty($value) ? " $value" : "";
                $this->html .= '">'; 
                $this->html .= is_array($attr) === true 
                                    ? (empty($attr['label']) ? __($key) : __($attr['label'])) 
                                    : (empty($attr) ? __($key) : __($attr));   
                $this->html .= '</div>';                                                                        
                $this->html .= '</li>';                                            
            }
            
            if (!empty($this->create_time)) {
                $class       = '';
                $this->html .= '<li class="'.$class.'">';
                $this->html .= '<div class="checkbox">';
                $checked     = isset($data_tool['create_time']);
                $value       = isset($data_tool['create_time']) ? $data_tool['create_time'] : null;
                $this->html .= Form::checkbox('tool[sort_by][create_time]',$value,$checked);
                $this->html .= '</div>';
                $this->html .= '<div class="label'.(!empty($value) ? " $value" : "").'">'.__('create_time').'</div>';                                   
                $this->html .= '</li>';
            }
            
            if (!empty($this->update_time)) {
                $class       = '';
                $this->html .= '<li class="'.$class.'">';
                $this->html .= '<div class="checkbox">';
                $checked     = isset($data_tool['update_time']);
                $value       = isset($data_tool['update_time']) ? $data_tool['update_time'] : null;
                $this->html .= Form::checkbox('tool[sort_by][update_time]',$value,$checked);
                $this->html .= '</div>';
                $this->html .= '<div class="label'.(!empty($value) ? " $value" : "").'">'.__('update_time').'</div>';                                   
                $this->html .= '</li>';
            }
            
            $this->html .= '<li>';
            $this->html .= Form::submit('sort_by','Go');
            $this->html .= '</li>';
            $this->html .= '</ul>';
            $this->html .= '</li>';
            $this->html .= '</ul>';
            $this->html .= Form::close();
            $this->html .= '</div>';
        }
        
        private function html_tool_search($config) {
            if ($config === false) return;
            
            $this->html .= '<div class="item-tool search">';  
            $this->html .= Form::open(URL::site(Request::$initial->uri(), TRUE));     
            $this->html .= 'Search : ';       
            $this->html .= Form::input('tool[search]',!empty($this->_data_tool['search']) ? $this->_data_tool['search'] : '');
            $this->html .= Form::submit('Go','Go');       
            $this->html .= Form::close();
            $this->html .= '</div>';
        }
    
    private function open_cell($attr) {
        $x = $attr;        
        $z = is_array($attr);
        $this->html .= $this->_style === 'css' ? '<div class="cell' : '<td class="';
        $this->html .= $z === true && !empty($attr['class']) ? ' '.$attr['class'].'" ' : '"';
        
        unset($x['label']); unset($x['class']);
        
        $this->html .= HTML::xattributes($x);
        $this->html .= '>';
    }
    
    private function html_header() {
                
        $this->html .= $this->_style === 'css' ? '<div class="header">' : '<thead><tr>';
        
        if (!empty($this->items[0])) {
            if (!empty($this->numbering)) {
                $this->total_header++;
                
                $extend_class = empty($this->_traversing) ? '' : ' traversing-index';
                
                $this->html  .= $this->_style === 'css' 
                                    ? "<div class='cell numbering $extend_class'>#</div>"
                                    : "<td class='numbering $extend_class'>#</td>";
            }
            
            foreach ($this->headers as $key => $attr) {
                $this->total_header++;
                $this->open_cell($attr);
                $this->html .= is_array($attr) === true 
                                    ? (empty($attr['label']) ? __($key) : __($attr['label'])) 
                                    : (empty($attr) ? __($key) : __($attr));
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';        
            }
            
            if (!empty($this->create_time)) {
                $this->total_header++;
                $this->html .= $this->_style === 'css' ? '<div class="cell create-time">' : '<td class="cell create-time">';
                $this->html .= (__('create_time'));
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }
            
            if (!empty($this->update_time)) {
                $this->total_header++;
                $this->html .= $this->_style === 'css' ? '<div class="cell update-time">' : '<td class="cell update-time">';
                $this->html .= (__('update_time'));
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }
            
            $this->html_header_action();
        } else {
            $this->html .= $this->_style === 'css' ? '<div class="cell">#######</div>' : '<td class="cell">#######</td>';    
        }
        
        $this->html .= $this->_style === 'css' ? '</div>' : '</tr></thead>';
    } 
        
        private function html_header_action() {
            //Set to temporary action
            $action = $this->_action;
            
            if (!empty($action)) {
                $this->total_header++;
                $this->html .= $this->_style === 'css' ? '<div': '<td'; 
                $this->html .= ' class="cell action job'.(count($action)).'">';
                $this->html .= __('action');
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }
            
        }
        
    private function set_format_date($value) {
        if (!is_bool($value)) {            
            if ($value == 'datetime')    
                $this->date_format = 'M d, Y H:i:s';            
            elseif ($value == 'date')
                $this->date_format = 'M d, Y';
            elseif ($value == 'time')    
                $this->date_format = 'H:i:s';
            else
                $this->date_format = $value;
        }
    }
    
    private function html_body($items=null,$level=0,$parent_number=null) {
        $index = !empty($this->paging) ? $this->paging->offset+1 : 1;
        
        $items = !isset($items) ? $this->items : $items;
        
        foreach ($items as $key => $item) {
            
            $class_row_cell = $index % 2 == 0 ? '' : 'blue';
            
            if (!empty($item->is_system)) {
                $class_row_cell = 'system';
            }
            
            if (!empty($item->is_feature) || !empty($item->is_highlight)) {
                $class_row_cell = 'lightBlue';
            }
            
            if (isset($item->is_active) && $item->is_active == 0) {
                $class_row_cell = 'red';
            }
            
            $need_sortable = !empty($this->_sortable) && isset($item->ordering);
            
            if ($need_sortable) {
                $class_row_cell .= ' sortable-area';
            }
            
            $this->html .= $this->_style === 'css' ? '<div' : '<tr';
            $this->html .= ' class="row '.$class_row_cell.'"';
            
            if ($need_sortable) {
                $this->html .= ' data-list-ordering="index-'.$item->ordering.'.'.$item->id.'"';
            } else {
                $this->_sortable = false;
            }
            
            if (!empty($this->_traversing)) {
                $this->html .=' data-tt-id="'.$item->id.'"';
                
                $traverse_field = $this->_traverse_column;
                
                if (!empty($item->$traverse_field)) {
                    $this->html .=' data-tt-parent-id="'.$item->$traverse_field.'"';
                }
            }
            
            $this->html .= '>';
            
            if (!empty($this->numbering)) {
                $this->html .= $this->_style === 'css' 
                                    ? '<div class="cell numbering">'.$parent_number.$index.'</div>'
                                    : '<td>'.$parent_number.$index.'</td>';
            }
             
            foreach ($this->headers as $key => $attr) {
                $this->open_cell($attr);
                $string = isset($item->$key) ? $item->$key : "No $key";
                
                if (isset($this->_data_tool['search']) && !empty($this->_tool['search'])) {
                    $search_field = is_array($this->_tool['search']) ? $this->_tool['search'] : array($this->_tool['search']);
                    if (in_array($key,$search_field)) {
                        $string = str_ireplace($this->_data_tool['search']
                                            ,'<span class="search-result-item">'.$this->_data_tool['search'].'</span>'
                                            ,$string);
                    }
                }
                
                if (isset($attr['limit']) && is_numeric($attr['limit']) && strlen($string) > $attr['limit']) {
                    $this->html .= substr($string,0,$attr['limit']) .' ...';
                } else {    
                    $this->html .= $string;
                }
                
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }   
    
            if (!empty($this->create_time)) {                    
                $this->set_format_date($this->create_time);                    
                $this->html .= $this->_style === 'css' ? '<div class="cell create-time">' : '<td class="cell create-time">';
                $this->html .= !empty($item->create_time) ? (date($this->date_format,strtotime($item->create_time))) : '-';
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }      
    
            if (!empty($this->update_time)) {                    
                $this->set_format_date($this->update_time);                    
                $this->html .= $this->_style === 'css' ? '<div class="cell update-time">' : '<td class="cell update-time">';
                $this->html .= !empty($item->update_time) ? (date($this->date_format,strtotime($item->update_time))) : '-';
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }  
    
            $this->html_body_action($item);
                                 
            $this->html .= $this->_style === 'css' ? '</div>' : '</tr>';
            
            if (!empty($this->_traversing) && !empty($item->child)) {
                $next_parent_number = isset($parent_number) ? "$parent_number$index." : "$index.";
                $this->html .= $this->html_body($item->child,$level++,$next_parent_number);
            }
                
            $index++;
        }                   
    }   
    
        private function html_body_action($item) {
            
            //Set to temporary action
            $action = $this->_action;
            
            //Action button
            if (!empty($action)) {
                foreach ($action as $action_key => $action_attr) {
                    //String to lower, just to make sure everything is okay as we wish
                    $strtolower_action_key = strtolower($action_key);
                    
                    //Declare action button as an empty string
                    $button_vars  = "button_".$strtolower_action_key;
                    $$button_vars = '';
                    
                    //Check for authorisation
                    $authorise = empty($action_attr['authorise']) || User::authorise($action_attr['authorise']);
                    
                    //Check for exception
                    if (!empty($action_attr['exception'])) {
                        $absolute = !empty($action_attr['exception']['i_absolute']) 
                                        ? $action_attr['exception']['i_absolute'] 
                                        : false;
                                                        
                        unset($action_attr['exception']['i_absolute']);
                        
                        $item->im_proud_to_be_born_as_batak = $strtolower_action_key;
                        
                        $in_exception = Logic::check_exception($item,$action_attr['exception'],$action_key);
                        
                        unset($item->im_proud_to_be_born_as_batak);
                        
                        if ( User::admin()->role_id === 1 || User::admin()->role_id === '1' ) {                            
                            $in_exception = ! in_array($absolute,array(true,1,'1')) ? false : $in_exception;                        
                        }
                    } else {
                        $in_exception = false;
                    }
                                                                                   
                    if ($authorise && !($in_exception)) {
                        $button_class = "icon $strtolower_action_key";
                        $tooltip_action = __($strtolower_action_key);
                        
                        if (isset($item->is_active) && $strtolower_action_key == 'status') {
                            $x_active = $item->is_active == 1;
                            $button_class  .= $x_active ? ' active' : ' in-active';
                            $tooltip_action = $x_active ? __('unpublish') : __('publish');
                        }
                        
                        if (isset($item->is_feature) && $strtolower_action_key == 'feature') {
                            $x_feature = $item->is_feature == 1;
                            $button_class  .= $x_feature ? ' highlight' : ' unhighlight';
                            $tooltip_action = $x_feature == 1 ? __('unfeatured') : __('featured');
                        }
                        
                        if ($strtolower_action_key == 'status' || $strtolower_action_key == 'delete' || $strtolower_action_key == 'feature') {
                            if (empty($action_attr['ajax']) || $action_attr['ajax'] !== false) {
                                $button_class .= ' ajax';    
                            }
                        }
                        
                          
                        $tooltip = isset($action_attr['tooltip']) 
                                ? $action_attr['tooltip'] 
                                : __('click_to_something',array(
                                        ':something'=>"$tooltip_action"));    
                        
                        $$button_vars .= '<a class="'.$button_class.'"';
                        if ($tooltip !== false) {
                            $$button_vars .= ' title="'.$tooltip.'"';
                        }
                        
                        $rest_attr = Helper_Kernel::unset_exist_key($action_attr,array('href','exception','authorise','ajax','tooltip','tooltip_action'));
                        $button_vars  .= HTML::attributes($rest_attr);
                        $button_link_href = $this->href_value_string($strtolower_action_key,$action_attr);
                        $$button_vars .= ' href="'.($this->href_value_parser($button_link_href,$item)).'"';                        
                        $$button_vars .= '>';
                        $$button_vars .= '</a>';
                    }
                }   
                
                $this->html .= $this->_style === 'css' ? '<div class="cell action">' : '<td class="cell action">';
                                
                $this->html .= isset($button_status) ? $button_status : '';
                $this->html .= isset($button_feature)   ? $button_feature   : '';  
                $this->html .= isset($button_edit)   ? $button_edit   : '';
                $this->html .= isset($button_view)   ? $button_view   : '';
                
                //Delete button only visible when there is no child attached to item
                if (empty($item->child))          
                    $this->html .= isset($button_delete) ? $button_delete : '';
                
                $this->html .= $this->_style === 'css' ? '</div>' : '</td>';
            }            
        }
            private function href_value_string($key,$attr) {
                //Check for http 
                if (!isset($attr['href'])) return '#';
                
                if (strpos($attr['href'],'http://') !== false) {
                    return $attr['href'];
                } else {
                    $href  = isset($attr['cms']) && $attr['cms'] === false ? URL::front() : URL::cms();
                    $href .= App::$route_params['package']; 
                    if (isset($attr['ajax'])) {
                        $href .= is_bool($attr['ajax']) ? '/ajax' : '/'.$attr['ajax'];                            
                    } else {
                        $href .= '/'.strtolower(App::$route_params['controller']);
                    }
                    $href .= '/'.$attr['href'];
                    return $href;
                }
            }
            
            private function href_value_parser($string,$value) {
                if (is_array($value) || is_object($value)) {
                    foreach ($value as $ky => $va) {
                        
                        if ($ky === 'child') continue;
                        
                        $$ky = preg_replace('/[^\da-z]/i', ' ', $va);
                        $$ky = preg_replace('!\s+!', '%20', $$ky);
                    }
                } else {
                    $$ky = preg_replace('/[^\da-z]/i', ' ', $value);
                    $$ky = preg_replace('!\s+!', '%20', $$ky);
                }
                
                
                
                return preg_replace('/\{([A-Z_]+)\}/ei', "$$1", $string);
            }
    
    public function set($key,$value=null) {
        $this->__set($key,$value);
        return $this;
    }
    
    public function set_header($name,$attributes=null) {
        if (is_array($name)) {
            foreach ($name as $nn => $aa) {
                if (is_numeric($nn))
                    $this->set_header($aa,null);
                else
                    $this->set_header($nn,$aa);
            }
        } else {
            $this->headers[$name] = $attributes;    
        }
        
        return $this;    
    } 
    
    /**
     * @var     str keyword eg. create, edit 
     * @var     arr attributes value
     * @return  htm button action listing
     */ 
    public function action($key,$attributes=null) {
        if (is_array($key)) {
            $this->_action = array_merge($this->_action,$key);
        } else {
            $this->_action[$key] = $attributes;
        }
        return $this;
    }
    
    public function create_action($attr) {
        $this->_action['create'] = is_array($attr) ? $attr : array('label'=>$attr);
        return $this;
    }
    
    public function edit_action($attr) {
        $this->_action['edit'] = $attr;
        return $this;
    }
    
    public function delete_action($attr) {
        $this->_action['delete'] = $attr;
        return $this;
    }
    
    public function status_action($attr) {
        $this->_action['status'] = $attr;
        return $this;
    }
    
    public function view_action($attr) {
        $this->_action['view'] = $attr;
        return $this;
    }
    
    public function feature_action($attr) {
        $this->_action['feature'] = $attr;
        return $this;
    }
    
    /**
     * @var     str keyword eg. search, filter
     * @var     mix value
     * @return  htm tool listing
     */    
    public function tool($key,$values=null) {
        if (is_array($key)) {
            $this->_tool = $key;
        } else {
            $this->_tool[$key] = $values;
        }
        return $this;
    }
        
    public function sortable($table_name = false) {
        $this->_sortable = $table_name;
        return $this;
    }
    
    /**
     *@var string listing style, css table or html table
     */
    public function style($style = 'css') {
        $this->_style = strtolower($style);
        return $this;
    }
    
    /**
     * @var string field to traversing 
     */
    public function traversing($traverse_column = null,$next_traversing_value = null, $starting_level = null) {
        if (isset($traverse_column)) {
            $this->_traverse_column = $traverse_column;
        }
        
        if (isset($next_traversing_value)) {
            $this->_traverse_next_column_value = $next_traversing_value;
        }
        
        if (!isset($starting_level)) {
            if (!empty($this->db) && method_exists($this->db,'starting_traversing_level')) {                
                $this->_traverse_start = $this->db->starting_traversing_level();
            } else {
                $this->_traverse_start = 0;
            }    
        } else {
            $this->_traverse_start = $starting_level;
        }
        
        $this->_traversing = true;
        
        return $this;    
    }
    
    /**
     * @var mixed  string(array) field(field=>direction pairs)
     * @var string order direction (ASC) (DESC) 
     */
    public function order_by($field='ordering',$direction='ASC') {
        if (is_array($field)) {
            foreach ($field as $new_field => $new_direction) {
                if (is_int($new_field)) {
                    $new_field     = $new_direction;
                    $new_direction = $direction;
                }
                $this->_order_by[$new_field] = $new_direction;
            }
        } else {
            $this->_order_by = array($field => $direction);
        }
         
        return $this;
    }
        
    public function render() {
        
        $this->validate();
        
        $this->html_add_action();        
        
        $this->html_tool();
        
        $this->html .= $this->_style === 'css' ? '<div class="tabular"' : '<table class="tabular"' ;
        
        if (!empty($this->_traversing)) {
            $this->html .= ' id="traversing-activate"';    
        }
        
        $this->html .= ">";
                
        $this->html_header();
            
        $this->html .= $this->_style === 'css' ? '<div' : '<tbody';
        
        $this->html .= ' class="row-group">';
        
        if (!empty($this->items[0])) {
            $this->html_body();
        } else {
            
            if ($this->_style === 'css') {
                $this->html .= '<div class="row-group">
                                    <div class="row red">
                                        <div class="cell center" style="width:2000px;">'.__('error.no-data');
                $this->html .= '</div></div></div>';
            } else {
                $this->html .= '<tbody><tr><td class="center">'.(__('error.no-data')).'</td></tr></tbody>';
            }
        }   
            
        $this->html .= $this->_style === 'css' ? '</div>' : '</tbody>';
        
        $this->html .= $this->_style === 'css' ? '</div>' : '</table>';;
        
        if (!empty($this->items)) {    
            $this->html .= '<div class="tabular-pagination">'.$this->paging.'</div>';
        }
        
        if (!empty($this->_sortable) || !empty($this->_traversing)) {
            $this->html .= '
                <script type="text/javascript">
                    $(document).ready(function(){ ';
                    if (!empty($this->_sortable)) {
                        $this->html .= '
                            $(".row-group").sortable({
                                connectWith: ".sortable-area",                                            
                                opacity: 0.6, 
                                cursor: "move", 
                                update: function(event, ui) {
                                    
                                    sorting  = $(this).sortable("serialize",{attribute:"data-list-ordering", key: "sorting"});
                                    
                                    nous_process_message("update");
                                    $.ajax({
                                        url     : bs_cms+"helper/ajax/reordering",
                                        type    : "POST",
                                        data    : {"sorting":sorting,"table":"'.$this->_sortable.'"},
                                        success : function(response) {
                                            index = parseInt(response);
                                            
                                            $(".row-group .numbering").each(function(){
                                                $(this).text(index);
                                                index++;
                                            });
                                            $("#nous-waiting-layering").remove();
                                        }
                                    });
                                }
                            }); 
                        ';
                    }
                    
                    if (!empty($this->_traversing)) {
                        $this->html .= '
                            $("#traversing-activate").treetable({ 
                                initialState :"expanded",
                                expandable   : true,
                                indent       : 5 
                            });
                        ';
                    }
            $this->html .= '
                    });
                </script>
            ';
        }
        
        return $this->html;
    }
    
    public function __toString() {
        return $this->render();
    }
}