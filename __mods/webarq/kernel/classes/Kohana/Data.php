<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    App data handler
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors
 * 
**/ 


class Kohana_Data {    
    /**
     * @return string some data html listing
     */
    public static function listing($configs = null) {
        return new Data_Listing($configs);
    }
    
    /**
     * @var    config mix (array) of module form attributes or (string) module form
     * @return string auto generated html form
     */
    public static function form(array $serials = array()) {    
        return new Data_Form($serials);           
    }
    
    /**
     * @var    array package
     * @return pairing array (table_name)(table_column_name)(form_element_name)
     */
    public static function column(array $packs = array()) {    
        return Data_Form::column($packs)->transaction_map();            
    }
    
    /**
     * @var    object return post data
     * @var    string transaction type, create or update only
     * @return 
     */
    public static function post($post) {
        return Data_Post::submit($post);
    }
    
    public static function create() {
        return new Data_Transaction('create');
    }
    
    public static function update() {
        return new Data_Transaction('update');
    }
    
    public static function read(array $config) {
        /**
         * table
         * field
         */
    }
    
    public static function delete() {
        
    }
    
    /**
     * @var string table name
     * @var boolean execution
     */
    public static function traversing($table=null,$execute=true) {
        
        if (is_bool($table)) {
            $configs = true;
            $execute = $table;
        } else {
            if (is_array($table)) {
                foreach ($table as $key => $a) {
                    $only_table = is_numeric($key); break;
                }
                $configs = $only_table === true ? array('table'=>$table) : $table;
            } else {
                $configs = array('table' => array($table,$table));
            }    
        }
        
        $t = Data_Traversing::sampling($configs);
        
        /**
        $execute = is_bool($table) ? $table : $execute;
        
        $table   = is_bool($table) ? true : $table;
        
        $t = Data_Traversing::sampling($table);
        **/
        
        return $execute === true ? $t->response() : $t;
    }
    
    /**
     * 
     */
    public static function reverse_traversing($table=null,$execute=true) {
        
        $execute = is_bool($table) ? $table : $execute;
        
        $table   = is_bool($table) ? true : $table;
        
        #$t = Data_Traversing::sampling($table);
        
        #return $execute === true ? $t->response() : $t;
    }
    
    public static function get_category_uri($table,$id_category,$uri = null) { 
        $get = Model::factory('Dba')->select('permalink','parent_id')
                                   ->from($table)
                                   ->where("id = $id_category")
                                   ->limit(1)
                                   ->execute()
                                   ->current();
        
        if (!empty($get->permalink)) {
            $uri = Data::get_category_uri($table
                                                        ,$get->parent_id
                                                        ,(!isset($uri) ? $get->permalink : $get->permalink.'/'.$uri));
        }                                  
        
        return $uri;
    }
    
    /**
     * Check parent value in array given. Work only for singular/plurar word.
     * Eg.
     * Say we have an array like array ('product_details','products','product_images','product_properties')
     * This function will return 'product' as parent.
     * Why? Product exists in others value.
     */    
    public static function check_parent(array $sources,$index=0) {
        
        if (count($sources) == 1) return $sources[0];
        
        foreach ($sources as $item) {
            $current_item = Inflector::singular($sources[$index]);
            
            if (strpos($item,$current_item) !== false)
                $temp_parent = $current_item;
            else
                $temp_parent = null;
        }
        
        return $temp_parent === null && $index < count($sources)
            ? Data::check_parent($sources,$index+1)
            : Inflector::plural($temp_parent);
    }
    
    /**
     * Return foreign key from table name given.
     */
    public static function foreign_key($table,$key="id") {
        $singular = Inflector::singular($table);
        return $singular."_".$key;
    }
    
    public static function total($table,$field='id',$execute=true) {
        $alias   = is_array($table) ? $table[1] : $table;
        $execute = is_bool($field) ? $field : $execute;
        $field   = is_bool($field) ? 'id' : $field;        
        $builder = Model::factory('Dba')->select(DB::expr("COUNT($alias.$field) AS `total`"))->from($table);
        return $execute === true ? $builder->execute()->current()->total : $builder;
    }
    
    public static function transaction_message($post,$action = 'created') {
        if (empty($post) || empty($post->app_transaction_pack)) {
            $transaction_object = "data";
        } else {
            $pack = explode(',',$post->app_transaction_pack);
            $count_pack = count($pack);
            $iteration = 1; 
            foreach ($pack as $index => $package) {
                list($module,$group) = explode(":",$package,2);
                
                //Check for object history 
                if (isset(Package::$_history->object->$module)) {
                    $objectHistory = Package::$_history->object->$module;
                    if (isset($post->$objectHistory)) {
                        $tgroup = $post->$objectHistory;
                    }
                
                    $module = ucfirst($module);  
                    
                } else {
                    if (!empty($post->app_transaction_pairs)) {
                        foreach ($post->app_transaction_pairs as $tPair => $cPair) {
                            if (isset(Package::$_history->object->$tPair)) {
                                $objectHistory = Package::$_history->object->$tPair;
                                if (isset($post->$objectHistory)) {
                                    $tgroup = $post->$objectHistory;
                                }   
                            } else {
                                $tPair = Inflector::singular($tPair);
                                
                                if (isset(Package::$_history->object->$tPair)) {
                                    $objectHistory = Package::$_history->object->$tPair;
                                    if (isset($post->$objectHistory)) {
                                        $tgroup = $post->$objectHistory;
                                    }   
                                } 
                            }
                            
                            if (isset($tgroup)) { break; }
                        }
                    }
                }
                     
                if (!empty($tgroup) && is_array($tgroup)) {
                    if ( !empty(App::$module->language) && $tgroup[Language::$system] !== '' ) {
                        $tgroup = $tgroup[Language::$system];
                    } else {
                        $tgroup = array_shift($tgroup);
                    }     
                } elseif ( empty($tgroup) ) {
                    $tgroup = isset($post->label) ? $post->label : 'data';
                }
                
                $tgroup = $tgroup === $group ? 'data' : $tgroup;
                if (!empty($tgroup) && is_array($tgroup)) {
                    if ( !empty(App::$module->language) && $tgroup[Language::$system] !== '' ) {
                        $tgroup = $tgroup[Language::$system];
                    } else {
                        $tgroup = array_shift($tgroup);
                    }     
                }
                if ($iteration ==  $count_pack && $count_pack > 1 ) {
                    $transaction_object .= ", and $module $tgroup";
                } else {                        
                    $transaction_object = isset($transaction_object) ? $transaction_object.", $module $tgroup" : "$module $tgroup";    
                }
                
                $iteration++;
            } 
        }
        App::$session->set( "transaction_message" , __( 'success.transaction',array( ':object'=>$transaction_object,':action'=>$action )) );
    }
    
    public static function transaction_message2($packs = array()) {
        if (!empty($packs)) {
            $pack = explode(',',$packs);
            $count_pack = count($pack);
            $iteration = 1; 
            foreach ($pack as $index => $package) {
                list($module,$group) = explode(":",$package,2);
                if ($iteration ==  $count_pack && $count_pack > 1 ) {
                    $transaction_object .= ", and $module $group";
                } else {                        
                    $transaction_object = isset($transaction_object) ? $transaction_object.", $module $group" : "$module $group";    
                }
                $iteration++;
            }    
        } else {
            $transaction_object = 'data';
        }
        App::$session->set("transaction_message",__('success.transaction',array(
            ':object'=>$transaction_object)));
    }
    
    public static function update_order(array $configs) {
        extract($configs);
        
        if (empty($table) || empty($order)) {
            throw new Kohana_Exception('Error, please check your order setting');      
        }
        
        list($current_order,$previous_order) = $order;
        
        if (!empty($parent))  {
            list($current_parent,$previous_parent) = $parent;
             
        }     
    }
    
    public static function mailer($to, $subject, $message) {
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        // Additional headers
        $headers .= 'From: '.Contact::$config->system_mail_from . "\r\n";
        
        // Mail it
        mail($to, $subject, $message, $headers);
    }
    
    /**
     * @param   mixed $table table name, $table1, $table2, $table3 ...
     */
    public static function drop_table($tables) {
        $args = func_get_args();
        foreach ($args as $t) {
            
            if (empty($t)) continue;
            
            $do = DB::query(Database::DELETE,"DROP TABLE IF EXISTS `$t`")->execute();
            
            Model::factory('Medialibrary')->applicant_delete($t);
        }    
    }
    
    /**
     * @param   mixed   $table module name to check or array navigations permalink
     * @param   mixed   $navigations array navigations permalink
     */
    public static function undo_module_navigation($module,$navigations = null) {
        if (is_array($module)) {
            $navigations = $module;
            $module = null;    
        }
        
        $selNav  = DB::select("mn.id")->from(array("module_navigations","mn"));
        
        $undoNav = DB::delete('module_navigations');
        
        $selNav->where_open();
        
        $undoNav->where_open();
        
        foreach ($navigations as $i => $n) {            
            if ($i == 0) {
                $selNav->where("permalink","=","$n");
                
                $undoNav->where("permalink","=","$n");
                
            } else {
                $selNav->or_where("permalink","=","$n");
                
                $undoNav->or_where("permalink","=","$n");
            }
        }
        
        $selNav->where_close();
        
        $undoNav->where_close();
        
        if (isset($module)) {
            $selNav->join(array("modules","m"))->on("m.id","=","mn.module_id")->and_where("m.label","=","$module");
            
            $undoNav->and_where("module_id","=",DB::select('id')->from('modules')->where("label","=","$module")->limit(1));
        }
        
        $c                  = clone $selNav;
        $undoPermission     = "DELETE FROM `module_permissions` WHERE `id` IN (SELECT `module_permission_id` FROM `module_navigation_permission_maps` WHERE `module_navigation_id` IN ($c))";
        $undoRolePermission = "DELETE FROM `module_permission_role_maps` WHERE `module_permission_id` IN (SELECT `module_permission_id` FROM `module_navigation_permission_maps` WHERE `module_navigation_id` IN ($c))";
        $undoNavPermission  = "DELETE FROM `module_navigation_permission_maps` WHERE `module_navigation_id` IN ($c)";
        
        $selNav            = $selNav->execute()->current();
        
        if (!empty($selNav)) {                                                                   
            //Undo permission                                      
            DB::query(Database::INSERT,$undoPermission)->execute();
                                                                   
            //Undo role permission                                      
            DB::query(Database::INSERT,$undoRolePermission)->execute();
            
            //Undo navigation permission                           
            DB::query(Database::INSERT,$undoNavPermission)->execute();            
        
            //Undo navigation execution
            $undoNav = $undoNav->execute();
        }
    }
}