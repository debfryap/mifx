<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    App history data handler
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 * 
 * 
    History::record(array(
        'row_id'      => null,
        'table'       => '',
        'actor'       => 'system',
        'action'      => 'create',
        'description' => null
    ));    
 *
**/ 


class Kohana_History extends Populate {    
    //(Int) or string of table row id
    protected $row_id = null;
    
    //(String) table name
    protected $table = null;
    
    //(String) actor name
    protected $actor = null;
    
    //(String) database job action
    protected $action = null;
    
    //(String) Specified value of row table, eg. if you create user, object could be then Username ....
    protected $object = null;
    
    //(String) Specified item that related to the object
    protected $item = null;
    
    //(Text)  histories additional description
    protected $description = null;
    
    //History job type
    protected $type = 'nothing';
    
    //(Object)Inserted row want to record
    protected $insert_row = null;
    
    
    public static function simple_record($id,$table,$label,$action) {
        History::record(array(
            'row_id'      => $id,
            'table'       => $table,
            'actor'       => User::admin()->username,
            'description' => '{"object":"'.$label.'","action":"'.$action.'"}'            
        ));
    }
    
    public static function record(array $configs = array()) {
        $configs['type'] = 'record';
        
        return new History($configs);             
    }
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        if (isset($this->table) && (isset($this->row_id))) {
            $this->check_id();
            $this->check_actor();           //Actor
            $this->check_action();          //Action
            $this->check_decription();      //Description
            $this->selector();
        }
    } 
    
    protected function check_id() {
        if (strpos(trim(strtolower($this->row_id)),'select') !== false) {
            $do_value = DB::query(Database::SELECT,$this->row_id)->as_object()->execute();
            $this->row_id = empty($do_value[0]) ? 0 : $do_value[0]->id;    
        }
    }
    
    /** Check all needed value **/
    protected function check_actor () {
        $this->actor = empty($this->actor) ? 'system' : $this->actor;
    }
    
    protected function check_action () {
        if (empty($this->action)) {
            $singular_table = Inflector::singular($this->table);
            $plurar_table   = $this->table;
                        
            $this->action = empty(Package::$_history->action->$plurar_table) 
                                ? (empty(Package::$_history->action->$singular_table)
                                        ? 'create' 
                                        : Package::$_history->action->$singular_table) 
                                : Package::$_history->action->$plurar_table;
        }
    }
    
    protected function check_decription() {
        if (empty($this->description)) {
            $this->description .= '{';
            if (isset($this->object)) {
                $this->description .= '"object":"'.$this->object.'",';
            }
            if (isset($this->item)) {
                $this->description .= '"item":"'.$this->item.'",';
            }
            if (isset($this->insert_row)) {
                $this->generate_description();
            }
            if (!isset($this->object) && !isset($this->insert_row)) {
                $this->insert_row = Model::factory('Dba')
                                        ->select()
                                        ->from($this->table)
                                        ->where("id = ".$this->row_id)
                                        ->execute()
                                        ->current();  
                $this->generate_description();
            }
            $this->description .= '"action":"'.$this->action.'"';
            $this->description .= '}';
        }
    }
    
    
    protected function selector() {
        $function = $this->type.'_history';
        if (method_exists($this,$function)) {
            $this->$function();
        }
    }
    
    protected function generate_description() {        
        //Record to history
        $singular_table = Inflector::singular($this->table);
                
        $plurar_table   = $this->table;
        
        if (empty(Package::$_history->object->$plurar_table)) {
            $history_object = empty(Package::$_history->object->$singular_table) 
                                    ? 'label'
                                    : Package::$_history->object->$singular_table;    
                                                
        } else {
            $history_object = Package::$_history->object->$plurar_table;
        }
        
        //Object history                            
        if (strpos($history_object,'->') !== false){
            list($seek_table,$seek_column) = explode('->',$history_object,2);
            
            $seek_table = Inflector::singular($seek_table); 
            
            $conditional_column = $seek_table.'_id';
            
            if (!empty($this->insert_row->$conditional_column)) {
                
                if (strpos(trim(strtolower($this->insert_row->$conditional_column)),'select') !== false) {
                    $conditional_value = DB::query(Database::SELECT,$this->insert_row->$conditional_column)->as_object()->execute()->current();
                    $conditional_value = isset($conditional_value->id) ? $conditional_value->id : null;
                } else {
                    $conditional_value = $this->insert_row->$conditional_column;
                }
                
                $seek_row = Model::factory('Dba')
                                ->select($seek_column)
                                ->from(Inflector::plural($seek_table))
                                ->where("id = ".$conditional_value)
                                ->execute()
                                ->current();
                                
                if (!empty($seek_row->$seek_column)) {
                    $this->description .= '"object":"'.$seek_row->$seek_column.'",';    
                }   
            }   
        }else{
            $this->description .= !empty($this->insert_row->$history_object) ? '"object":"'.$this->insert_row->$history_object.'",' : "";    
        }
        
        //Item history
        
        $history_item = !empty(Package::$_history->item->$plurar_table) 
                            ? Package::$_history->item->$plurar_table
                            : (!empty(Package::$_history->item->$singular_table)
                                ? Package::$_history->item->$singular_table
                                : null);
        
        if (isset($history_item)) {
            
            if (strpos($history_item,'->') !== false){
                list($seek_table,$seek_column) = explode('->',$history_item,2);
                $conditional_column = $seek_table.'_id';
                if (!empty($this->insert_row->$conditional_column)) {
                    
                    if (strpos(trim(strtolower($this->insert_row->$conditional_column)),'select') !== false) {
                        $conditional_value = DB::query(Database::SELECT,$this->insert_row->$conditional_column)->as_object()->execute()->current();
                        $conditional_value = isset($conditional_value->id) ? $conditional_value->id : null;
                    } else {
                        $conditional_value = $this->insert_row->$conditional_column;
                    }
                    
                    $seek_row = Model::factory('Dba')
                                    ->select($seek_column)
                                    ->from(Inflector::plural($seek_table))
                                    ->where("id = ".$conditional_value)
                                    ->execute()
                                    ->current();
                                    
                    if (!empty($seek_row->$seek_column)) {
                        $this->description .= '"item":"'.$seek_row->$seek_column.'",';    
                    }   
                }   
            }else{
                $this->description .= !empty($this->insert_row->$history_item) ? '"item":"'.$this->insert_row->$history_item.'",' : "";    
            }
        }
    }
    
    protected function record_history() {
        $query = "INSERT IGNORE 
                    INTO `histories` (`row_id`,`table`,`actor`,`description`,`create_time`)
                    VALUES ('$this->row_id','$this->table','$this->actor','$this->description','".date('Y-m-d H:i:s')."')
                 ";
        $execute = DB::query(Database::INSERT,$query)->execute();
    }
    
    
    public static function detail_activity($item=NULL,$actor=true) {
        if (!empty($item)) {            
            $x = json_decode($item->description);
            
            if (!empty($x->message)) return $x->message;
            
            if (empty($x->action)) return null;
            
            $xactor  = !empty($item->actor) ? '<span class="actor">'.$item->actor.'</span>' : '';            
            $action = !empty($x->action) ? '<span class="action">'.($x->action).'</span>' : '';
            $object = !empty($x->object) ? '<span class="object">'.$x->object.'</span>' : '';
            $xitem   = !empty($x->item) ? '<span class="item">'.$x->item.'</span>' : '';
            $table  = !empty($item->table) ? '<span class="table">'.$item->table.'</span>' : '';
            
            #system ADD module_navigation_group_maps main ke product_list
            #superadmin UPDATE site configurations
            
            $i  = '';
            $i .= !empty($actor) ? "$xactor $action " : "$action ";
            
            $exclude_array = array ('login','logout');
            
            if (!in_array(strtolower($x->action),$exclude_array)) {                
                if (!empty($xitem)) {
                    $a  = isset($x->todo) ? $x->todo : 'in to';
                    $a  = strtolower($x->action) == 'update' ? '' : $a; 
                    $t  = isset($x->table) ? $x->table : $table;
                    $i .= "$object $a $t ";
                    $i .= "$xitem ";
                } else {
                    $i .= "$table:$object ";
                }
            }
            
            #$i .= "on <span class=\"date\">$item->create_time</span>";
            
            return $i;
        }   
        return NULL; 
    }
}