<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Helper File
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Helper_File {
    
    public static function type($file = NULL,$dot_preceding = false,$separator = '.') {
        if (!is_bool($dot_preceding)) {
            $separator     = $dot_preceding;
            $dot_preceding = false;
        }        
        
        $dot_ext = strrchr($file,$separator);
        return $dot_preceding === true ? $dot_ext : substr($dot_ext,1);
    }
    
    public static function name($file = NULL,$separator = '.') {
        return str_replace(self::type($file,true,$separator),'',$file);
    }
    
    public static function system_name($haystack,$needle='-') {
        $file_name = Helper_Kernel::name(Helper_File::name($haystack),$needle);
        $file_ext  = Helper_File::type($haystack);
        
        return !empty($file_ext) ? $file_name.'.'.$file_ext : $file_name;
    }
    
    public static function create_folder($path,$root_dir=null,$permission = 0755) {     
        if (isset($root_dir) && !is_string($root_dir)) {
            $permission = $root_dir;
            $root_dir   = null;
        }
        
        if (!is_dir($root_dir.$path)) {
            $dir = $root_dir;
            foreach (explode('/',$path) as $_path) {
                $dir .= $_path.'/';
                
                if (!is_dir($dir)) {
                    //Create directory
                    if (mkdir($dir,$permission)) {
                        //Auto create index file :)
                        if ($handler = fopen($dir."index.html","w+")) {
                            fwrite($handler, '<html><title>No direct access</title><body>No direct access allowed</body></html>');
                        };
                    }
                }
            }  
        }
    } 
    
}

?>