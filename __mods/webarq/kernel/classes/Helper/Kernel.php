<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Helper File
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Helper_Kernel {
    
    //System file naming    
    public static function name($haystack='default',$needle = '-',$lowering = true) {
        $regex   = preg_replace("/[^A-Za-z0-9+]/", $needle, trim($haystack));
        $regex   = $lowering === true ? strtolower($regex) : $regex;
        $pattern = '/-{2,}/';
        $replace = '-';
        return preg_replace("/[$needle]+/","$needle",preg_replace($pattern, $replace, $regex));
    }
    
    
    public static function random_salt() {
        $pattern      = array ('9'=>'1','6'=>'2','3'=>'2','2'=>'2');
        $session_id   = Session::instance()->id();
        $random_salt  = '';
        foreach ($pattern as $start=>$length) {
            $random_salt .= substr($session_id,$start,$length);
        }
        
        return $random_salt;
    }
    
    /**
     * @param  array or (array)object
     * @return array or (array)object with lowering key.
     */
    public static function lowering_key($object = null) {        
        $result = null;
        $is_array  = is_array($object);
        $is_object = is_object($object);
        
        if ($is_array === true || $is_object === true) {
            $result = $is_array === true ? array() : new StdClass();
            foreach ($object as $key => $value) {
                $key  = strtolower($key);
                $is_array === true ? $result[$key] = $value : $result->$key = $value;
            }
        }
        
        return $result;
    }
    
    public static function array_replace_merge($current,$new) {
        foreach ($new as $key => $value) {
            if (is_array($value)) {
                $current[$key] = !isset($current[$key]) || strtolower($key) == 'option' 
                    ? $value
                    : self::array_replace_merge($current[$key],$value);
            } else {
                $current[$key] = $value;
            }        
        }
        
        return $current;
    }
        
    public static function excerpt($text,$length,$continu = ' ...') {
        $itext  = substr($text,0,$length);
        $iarray = str_split($text);
        
        return count($iarray) > $length ? $itext.$continu : $text;
    }
    
    public static function object_to_array ($object = null) {
        return json_decode(json_encode($object),true);
    }
    
    public static function array_to_object(array $array = array()) {
        return json_decode(json_encode($array));
    }
    
    public static function arr_to_object($arr = NULL, $all = TRUE) {        
        if(!is_array($arr)):
            return $arr;
        endif;
        
        $object = new stdClass();
        
        foreach ($arr as $k=>$v):
            $k = strtolower(trim($k));
            if (!empty($k)): 
                if ($all):
                    $object->$k = self::arr_to_object($v);
                else:
                    $object->$k = $v;
                endif;
            endif;
        endforeach;
        return $object;
    }
    
    public static function mixed_search($string=null){
        return  preg_replace("/".self::$instance->search_value."/i", "<span class=\"search_value\">\$0</span>", $string);
    }
    
    //haystack : timbunan rumput
    //needle   : jarum
    //unset an item from haystack while exist to in array needle
    public static function unset_exist_key(array $haystack=array(),array $needle) {
        return empty($haystack) && empty($needle) 
            ? null 
            : ( empty($haystack) 
                        ? null 
                        : array_diff_key($haystack,array_flip($needle)));     
    }
    
    public static function proper_model_name($name) {
        return ucfirst(preg_replace_callback(
                    '/_[a-z]?/',
                    create_function(
                        // single quotes are essential here,
                        // or alternative escape all $ as \$
                        '$matches',
                        'return strtoupper($matches[0]);'
                    ),
                    $name
                ));
    }
    
    public static function string_to_array ($string,$separator=';',$good_array=true){
        $tmp_array = $separator == '' 
            ? str_split($string)
            : explode($separator,$string);
        
        if ($good_array === true)
            return array_filter(array_map('trim',$tmp_array));            
        else
            return array_map('trim',$tmp_array);
    }
    
}

?>