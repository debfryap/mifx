<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * URL helper class.
 *
 * @package    Kohana
 * @category   Helpers
 * @author     Kohana Team
 * @copyright  (c) 2007-2012 Kohana Team
 * @license    http://kohanaframework.org/license
 * 
 * @author      Daniel Simangunsong
 */
 
 
class URL extends Kohana_URL {
    
    //Admin prefix
    public static $admin_prefix = 'admin-cp';
    
    public static function root($folder = '') {
        $r = URL::base(true);
        
        if (empty($folder))
            return $r;
            
        $r .=  trim($folder,'/');
        
        if (strpos($folder,'.') === false)
            $r .= '/';
        
        return $r;
    }
    
    public static function cms($protocol = true) {
        $link = '';
        if (is_string($protocol)) {
            $link = $protocol;
            $protocol = true;
        }
        
        return URL::site(self::$admin_prefix,$protocol,true)."/".$link;    
    }
    
    public static function front($link='') {
        return URL::site($link,true);
    }
    
    public static function assets($file = null,$dir = null,$type='js') {
        if (!isset($dir))
            return URL::site('assets',true,false) . '/' . (isset($file) ? trim($file,'/') . '/' : '') ;
        
        $dir = str_replace("//","/",$dir."/");
        if (is_array($file)) {
            foreach ($file as $i => $ii) {
                if ($type == 'css' && !is_numeric($i))
                    $arr[URL::site('assets/' . $dir.$i,true,false)] = $ii; 
                else
                    $arr[] = URL::site('assets/' . $dir.$ii,true,false);
            }
            return count($arr) == 1 ? $arr[0] : $arr;
        } else { 
            return URL::site('assets/' . $dir . $file,true,false);
        }    
    }
    
    public static function libJs($file = null) {
        if (!isset($file)) return self::assets('__system/lib/js');        
        $file = is_array($file) ? $file : func_get_args();
        return self::assets($file,'__system/lib/js/');
    }
    
    public static function libCss($file = null) {
        if (!isset($file)) return self::assets('__system/lib/css');
        $file = is_array($file) ? $file : func_get_args();
        return self::assets($file,'__system/lib/css/','css');
    }
    
    public static function libImage($file = null) {
        if (!isset($file)) return self::assets('__system/lib/images');
        return self::assets($file,'__system/lib/images/');
    }
    
    public static function sysJs($file = null) {
        if (!isset($file)) return self::assets('__system/index/js');
        $file = is_array($file) ? $file : func_get_args();
        return self::assets($file,'__system/index/js/');
    }
    
    public static function sysCss($file = null) {
        if (!isset($file)) return self::assets('__system/index/css');
        $file = is_array($file) ? $file : func_get_args();
        return self::assets($file,'__system/index/css/','css');
    }
    
    public static function sysImage($file = null) {
        if (!isset($file)) return self::assets('__system/index/images');
        return self::assets($file,'__system/index/images/');
    }
    
    public static function templateCss($file = null) {
        if (!isset($file)) return self::assets( self::themes() . '/'. self::template() . '/css');
        $file = is_array($file) ? $file : func_get_args();
        return self::assets($file,self::themes() . '/' .self::template() .'/css/','css');
    }
    
    public static function templateJs($file = null) {
        if (!isset($file)) return self::assets( self::themes() . '/'. self::template() . '/js');
        $file = is_array($file) ? $file : func_get_args();
        return self::assets($file,self::themes() . '/' .self::template() .'/js/');
    }
    
    public static function templateImage($file = null) {
        if (!isset($file)) return self::assets( self::themes() . '/'. self::template() . '/images');
        return self::assets($file,self::themes() . '/' .self::template() .'/images/');
    }
    
    public static function media($type, $file = null, $relative = false) {
        //Check for file
        if (is_bool($file)) {
            $relative = $file;
            $file     = "";
        }
        
        $prefix = $relative === null ? '' : "media/$type/";
                        
        return $relative === false || $relative === null ? self::root($prefix . $file) : $prefix . $file;
    }
    
    public static function mediaImage($file = null, $relative = false) {
        return self::media('images',$file,$relative);
    }
    
    public static function mediaDoc($file = null, $relative = false) {
        return self::media('docs',$file,$relative);
    }
    
    public static function mediaVideo($file = null, $relative = false) {
        return self::media('videos',$file,$relative);
    }
    
    public static function anchor($uri,$title = null,$attributes = null) {
        $make_uri = URL::site($uri,true,true);
        return HTML::anchor($make_uri,$title,$attributes);
    }
    
    public static function themes() {
        return @Themes::$instance->name;
    }
    
    public static function template() {
        return @Themes::$instance->template;
    }
    
    public static function test_uri() {
        //Access uri : http://127.0.0.1/prayer/admin-cp/helper/no-template/test-uri/bebi
        $uri = array (
            'page' => array
            (
                'a' => Request::$initial->uri(),    // "admin-cp/helper/no-template/test-uri/bebi"                            
                'b' => URL::base(TRUE, FALSE).Request::$initial->uri(), //"http://127.0.0.1/prayer/admin-cp/helper/no-template/test-uri"
                'c' => URL::site(Request::$initial->uri()), //"/prayer/admin-cp/helper/no-template/test-uri/bebi"
                'd' => URL::site(Request::$initial->uri(), TRUE), //"http://127.0.0.1/prayer/admin-cp/helper/no-template/test-uri/bebi"
            ),

            'application' => array
            (
                'a' => URL::base(), //"/prayer/"
                'b' => URL::base(TRUE, TRUE), //"http://127.0.0.1/prayer/"
                'c' => URL::site(), //"/prayer/"
                'd' => URL::site(NULL, TRUE), //"http://127.0.0.1/prayer/"
            ),
        );
        
        return Debug::vars($uri);
    }
}