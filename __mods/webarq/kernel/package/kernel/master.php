<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Installation Predefined Package
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
 *  
**/ 
    
return array(
    "master" => array(
        /** database property **/
        "connection"    => "default",
        "charset"       => "utf8",
        "collation"     => "utf8_unicode_ci",
        "engine"        => "MyISAM",
         
        "property" => array (
            "length" => array (
                "tinyint"   => 4,
                "smallint"  => 6,
                "mediumint" => 9,
                "int"       => 11,
                "bigint"    => 20,
                "char"      => 25,
                "varchar"   => 255
            ),
            "collation" => array (
                "tinytext"  => true,
                "text"      => true,
                "mediumtext"=> true,
                "longtext"  => true,
                "varchar"   => true,
                "char"      => true,
                "enum"      => true
            ),
            "unsigned" => array (
                "tinyint"   => true,
                "smallint"  => true,
                "mediumint" => true,
                "int"       => true,
                "bigint"    => true,
                "float"     => true,
                "decimal"   => true,
                "double"    => true
            ),
            "form" => array (
                "text"      => array("tinyint","smallint","mediumint","int","bigint","decimal","float","char","varchar"),
                "textarea"  => array("tinytext","text","mediumtext","longtext"),
                "select"    => array("enum"),
                "checkbox"  => array("boolean")
            )               
        ),
        "skip_module" => array (
            "kernel"        => true,
            "database"      => true,
            "themes"        => true,
            "pagination"    => true,
            "captcha"       => true,
            'image'         => true
        ),
        "skip_ignore" => array (
            "tinytext"          => true,
            "text"              => true,
            "mediumtext"        => true,
            "longtext"          => true,
            "date"              => true,
            "time"              => true,
            "datetime"          => true,
            
            "ordering"          => true,
            "is_system"         => true,
            "is_active"         => true,
        ),
        "reserve" => array(
            "engine"    => true,
            "collation" => true,
            "charset"   => true,
            "master"    => true
        ),
        
        
        /** form property **/
        "column" => array(
            "default"   => null,
            "type"      => "char",
            "length"    => null,
            "null"      => true,
            "unsigned"  => false,
            "increment" => false,
            "zerofill"  => false,
            "comment"   => "",
            "option"    => "active,inactive",
            "primary"   => false
        ),
        //Exception installer package key
        "exception_key" => array (
            "form"      => true,
            "master"    => true,
            "key"       => true,
            "extra"     => true,
            "foreign"   => true
        ),
        
        /** You can add your master table field **/        
        "field" => array (
            array (
                "field"     => "id",
                "type"      => "int",
                "primary"   => true,
                "increment" => true,
                "form"      => array(
                    "name"  => "master",
                    "type"  => "text"
                )
            ),
            array (
                "field"     => "label",
                "type"      => "varchar",
                "form"      => array (
                    "name"  => "master",
                    "type"  => "text"
                )
            ),
            array (
                "field"     => "select",
                "type"      => "enum",
                "option"    => "publish,un-publish",
                "form"      => array (
                    "name"   => "master",
                    "type"   => "select",
                    "option" => array("publish"=>"Publish","un-publish"=>"Un-Publish")
                )
            ),
            array (
                "field"     => "is_something",
                "type"      => "boolean",
                "default"   => 0,
                "form"      => array (
                    "name"  => "master",
                    "type"  => "checkbox",
                    "state" => array ('on' => 1, "off" => 0)
                )
            ),
            array (
                "field"     => "radio",
                "type"      => "boolean",
                "form"      => array (
                    "name"  => "master",
                    "type"  => "radio",
                    "state" => array ('on' => 1, "off" => 0)
                )
            ),
            array (
                "field"     => "text",
                "type"      => "mediumtext",
                "form"      => array (
                    "name"  => "master",
                    "type"  => "textarea"
                )
            ),
            array (
                "field"     => "create_time",
                "type"      => "datetime",
                "form"      => array (
                    "name"  => "master",
                    "type"  => "text"
                )
            ),
            array (
                "field"     => "update_time",
                "type"      => "datetime",
                "form"      => array (
                    "name"  => "master",
                    "type"  => "text"
                )
            ),
            array (
                "field"     => "int",
                "type"      => "int",
                "form"      => array (
                    "name"  => "master",
                    "type"  => "text"
                )
            ),
            array (
                "field"     => "ordering",
                "type"      => "int",
                "null"      => false,
                "form"      => array (
                    "name"  => "ordering",
                    "type"  => "text"
                )
            )
        )     
    )
);