<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(    
    'offline'                           => 'Offline Page',
    'config'                            => 'Configuration',
    'application-navigation'            => 'Application Navigation',
    'application-navigation-group'      => 'Application Navigation Group',
    'application_navigation_group_id'   => 'Application Navigation Group',
    'new-application-navigation'        => 'New Application Navigation',
    'module-setting'                    => 'Module Configuration',
    'module-navigation'                 => 'Module Navigation',
    'module_navigation_group'           => 'Module Navigation Group',
    'general-setting'                   => 'General Configuration',
    'history'                           => 'History',
    'is-page'                           => 'Is Page',
    'page-title'                        => 'Page Title',
    'page-body'                         => 'Page Body',
    'page-section'                      => 'Page Section',
    'history_configurations'            => 'configurations',
    'traversing-deep'                   => 'Traversing Deep',
    'label'                             => 'Label',
    'title'                             => 'Title',
    'keyword'                           => 'Keyword',
    'description'                       => 'Description',
    'status'                            => 'Status',
    'environment'                       => 'Environment',
    'is_active'                         => 'Is Active',
    'is-active'                         => 'Is Active',
    'is_system'                         => 'Is System',
    'is-system'                         => 'Is System',
    'is_publish'                        => 'Publish',
    'is-publish'                        => 'Is Publish',
    'publish'                           => 'Publish',
    'un-publish'                        => 'Un-Publish',
    'ordering'                          => 'Order',
    'belum-ada-judul'                   => 'Belum ada Judul',
    'belum-ada-content'                 => 'Ingat ya, contentnya belum ada neh',
    'index'                             => 'No.',
    'date'                              => 'Date',
    'date-time'                         => 'Date Time',
    'create_time'                       => 'Create Time',
    'update_time'                       => 'Updated',
    'action'                            => 'Action',
    'parent_id'                         => 'Parent ID',
    'permalink'                         => 'Permalink',
    'this_is_parent'                    => 'This is parent',
    'please-select'                     => 'Please Select',
    'create'                            => 'Create',
    'update'                            => 'Update',
    'perpage'                           => 'Perpage',
    'add-new'                           => 'Add New Item',
    'click_to_something'                => 'Click to :something this item',
    'module'                            => 'Module',
    'permission'                        => 'Permissions',
    'edit-configuration'                => 'Edit Configuration',
    'select-one'                        => 'Please Select',
    'this-is-parent'                    => 'This is a parent',
    'permalink'                         => 'URL',
    'used-in'                           => 'Used in %time time%s',
    'no-data-post'                      => 'No data post to perform this action',
    'logo'                              => 'Logo',
    'navigation-position'               => 'Position',
    'copy-right'                        => 'Copyright',
    'no-item-data'                      => 'No :item found',
    'bread-crumb-position'              => 'You are in here : ',
    'image-alt'                         => 'Image Alternative Text',
    'dashboard'                         => 'Dashboard',
    'no-activity'                       => 'There is no activity log could found',
    'online-users'                      => ':num :type currently online',
    'online'                            => 'Online :total'
);