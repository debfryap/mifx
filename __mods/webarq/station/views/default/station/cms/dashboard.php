<?php defined('SYSPATH') or die('No direct script access.'); 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Category ~ CMS
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/
?>
<div class="left_section">
    <div id="content_header"><h3 class="statistic">Activities Statistic Overview</h3></div>
    
    <div id="content_body" style="position:relative;">	
        <div id="chart"><?php if (empty($graphical)) echo __('no-activity'); ?></div>
    </div>
</div>
        
<div class="right_section">
	<div id="content_header">
		<h3 class="activities">Latest Activities</h3>
	</div>
    
    <!-- USER LATEST ACTIVITY -->
    <div id="content_body">			
        <div class="latest_activity">
        <?php 
            if (!empty($activities[0])) {
                echo '<ul>';
                foreach ($activities as $activity) {
                    echo '<li>',
                            '<a href="#" style="cursor:default;">',
                                '<p class="x-small">',date('d M Y - h.i',strtotime($activity->create_time)),'</p>',
                                '<p class="small">',History::detail_activity($activity,true),'</p>',
                                //'<p class="small"><span class="blue1">Danu Andoko</span> create new page "<span class="blue2">Lorem Ipsum page</span>"</p>',
                            '</a>
                        </li>';
                }
                echo '</ul>';    
            } 
        ?>
        </div>
    </div>		
    <!-- END USER LATEST ACTIVITY -->
    
    
    <?php 
        
        //Get admin login
        $totalLogin = $loginAdmin['total']; ?>
	<div class="spacev"></div>
	<div id="content_header">
		<h3 class="user">
            User Admin
            <?php if (!empty($totalLogin)) echo '<sub style="font-size:70%;">(',__('online-users', array( ':num'=>$totalLogin,':type'=> $totalLogin > 1 ? 'users' : 'user' ) ),')</sub>'; ?>
        </h3>
	</div>
    
	<div id="content_body">
    	<div class="user_admin">
            <?php                 
                if (!empty($users[0])) {
                    echo '<ul>';
                    foreach ($users as $user) {
                        echo '<li>';
                        $photo = !empty($user->avatar_file_name) && file_exists(URL::mediaImage(User::$config->images->small->path.'/'.$user->avatar_file_name,true))
                            ? URL::mediaImage(User::$config->images->small->path.'/'.$user->avatar_file_name)
                            : false; 
                        echo '<div class="picture">',(!empty($photo) ? HTML::image($photo,array('style'=>'width:37px;')) : ''),'</div>';
                        
                        echo '<div class="group_name">';
                        echo '<p class="name">',$user->full_name,'</p>';
                        echo '<p class="mail">',$user->email,'</p>';
                        echo '<p class="status">',$user->role_label,'</p>';
                        if (!empty($loginAdmin[$user->id])) {
                            echo '<p class="online" style="color:#3CCBD1;">'
                                    ,__('online',array(
                                        ':total' => $loginAdmin[$user->id] == 1 
                                            ? '('.$loginAdmin[$user->id].') user'
                                            : '('.$loginAdmin[$user->id].') users'
                                        ))
                                    ,'</p>';
                        }
                        echo '</div>';
                        
                        //echo '<div class="message"><a href="">send message</a></div>';
                        echo '</li>';
                    }
                    echo '</ul>';
                } 
            ?>
            	
    		<p class="viewAll">
    			<?php 
                        echo User::authorise('user',array('create-user','edit-user','publish-user','view-user','delete-user')) 
                                ? HTML::anchor(URL::cms().'user/manage-user','View All')
                                : ''; 
                        ?>
    		</p>
    	</div>
	</div>		
</div>

<div class="clear"></div> 

<?php if (empty($graphical)) return ; ?>    
<script type="text/javascript">
    $(document).ready(function(){
        <?php
            echo "var line1 = [";
            foreach ($graphical['item'] as $i => $x) {
                if ($i > 0) echo ", ";
                echo "['",$x['date'],"',",$x['activity'],"]";
            }
            $last_date = $graphical['item'][0]['date'];
            $first_date = $x['date'];
            echo "]";
        ?>

        var plot1 = $.jqplot('chart', [line1], {        	   
    		grid: {					
    			background: '#fff',
    			drawBorder: false,
    			shadow: false, 
    		},
    		seriesDefaults: {
    			showMarker: true,
    			pointLabels: {
    				show: true
    		  }
            },
    		title: {
    			fontFamily: 'arial',
    			//text: 'Statistical Report',
    			textAlign : 'center',
    			fontSize: '20px'
    		},				
    		seriesColors: ["#32B0EA"],
    		animate: true,					
    		animateReplot: true,
    		cursor: {
    			show: true,
    			zoom: false,
    			looseZoom: false,
    			showTooltip: true
    		},						
    		axes:{
    		    yaxis:{
  		            min : 0
    		    }  ,
    			xaxis:{
    				renderer:$.jqplot.DateAxisRenderer,
                    tickOptions : {
                        formatString: '%d %B %Y',
                        formatter : function(format,value) {
                            return ' abds ';
                        }
                    },
                    tickInterval:'1 day',
                    min : '<?php echo date('Y-m-d',strtotime($first_date. ' -1 day')); ?>',
                    max : '<?php echo date('Y-m-d',strtotime($last_date. ' +1 day')); ?>'
    			}
    		},
    		/*series:[
    			{lineWidth:4}
    		]*/
    	}); 
        
        $('.jqplot-xaxis div:first-child').hide();    
        $('.jqplot-xaxis div:last-child').hide();
    }); 
</script>