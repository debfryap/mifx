<?php defined('SYSPATH') or die('No direct script access.'); 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Form Category ~ CMS
 * @Module      News
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/
?>
<style>
#station-form-wrapper select {
    font                : 11px/14px 'arial';
    border              : 1px solid #bababa;
    border-radius       : 3px;
    -moz-border-radius      : 3px;
    -webkit-border-radius   : 3px;
    color               : #666;
    width               : 200px;
    height              : 30px;
    line-height         : 30px;
    padding             : 5px 2px;
}
</style>
<div class="left_section">
    <div id="content_header"><h3 class="statistic">Activities Conversions</h3></div>
    
    <div id="content_body" style="position:relative;">  
        <div id="chart"><?php if (empty($graphical)) echo __('no-activity'); ?></div>
    </div>
</div>
        
<div class="right_section">
    
    <!-- SELECT YEAR -->
    <div id="station-form-wrapper">
    <!-- start form -->
        <form method="post" action="" id="form-select-year">
            <p>
                Select Year
                <?php
                    echo Conversion::selectYear();
                ?>
            </p>
        </form>
    </div>
    <br />
    <div class="clear"></div> 
    <!--/END SELECT YEAR--->
    <div id="content_header">
        <h3 class="activities">Latest Conversions</h3>
    </div>
    <!-- USER LATEST ACTIVITY -->
    <div id="content_body">         
        <div class="latest_activity">
        <?php 
            if (!empty($activities[0])) {
                echo '<ul>';
                foreach ($activities as $activity) {
                    
                    $detail =  json_decode($activity->value);
                    
                    echo '<li>',
                            '<a href="#" style="cursor:default;">',
                                '<p class="x-small">',date('d M Y - h.i',strtotime($activity->create_time)),'</p>',
                                '<p class="small">',
                                    '<p class="small">
                                        <span class="actor">', $activity->title ,'</span>
                                        <span class="action"></span>
                                        <span class="table">IP</span>:<span class="object">', $activity->ip ,'</span>
                                    </p>',
                                '</p>',
                                //'<p class="small"><span class="blue1">Danu Andoko</span> create new page "<span class="blue2">Lorem Ipsum page</span>"</p>',
                            '</a>
                        </li>';
                }
                echo '</ul>';    
            } 
        ?>
        </div>
    </div>      
    <!-- END USER LATEST ACTIVITY -->
        <div class="clear"></div> 
        <br />
            <div id="content_header">
                <h3 class="activities">Color Chart</h3>
            </div>
            <!-- USER LATEST ACTIVITY -->
            <div id="content_body">         
                <div class="latest_activity">
                <ul>
                <?php
                 $color = array('32B0EA', '32EA5C', 'BB2ED7', 'ABFC36', '5B2ED7', 'F7FF37','FFDF37','FFB737','FF8F37','E5317C','2E60D7');
                 $num = 0;
                 foreach ($graphical['item'] as $key => $val ) {
                    echo "<li>". $key ."<div style=\"height: 10px; widht: 100px; background: #". $color[$num] ."\"></div></li>";
                    $num++;
                 }
                ?>
                </ul>
                </div>
            </div>      
        <div class="clear"></div> 
    </div>
    <div class="clear"></div> 
</div>
<?php //die(Debug::vars($graphical['item'])); 
if (empty($graphical)) return ; ?>    
<script type="text/javascript">
    $(document).ready(function(){
        
        // select year
        $('select#select-year').change(function(){
            $('form#form-select-year').submit(); 
        });
        
        <?php
        
         $month = array( 0 => 'Januari', 1 => 'Februari', 2 =>'Maret', 3 => 'April', 4 => 'Mei', 5 => 'Juni', 6 => 'Juli', 7 => 'Agustus', 8 => 'September', 9 => 'Oktober', 10 => 'November', 11 => 'Desember' );
         
         echo 'var plot1 = $.jqplot(\'chart\',[';
         
         foreach ($graphical['item'] as $key => $val ) {
            
            echo "[";
            
            foreach($val as $a => $b){
                
                echo "['". $a ."', $b]";   
                
                if (count($val) > 0 ) echo ", ";
            }
            
            echo "]";
            
            if(count($graphical['item']) > 0) echo ", ";
         }
        
        ?>
        ], {               
            grid: {                 
                background: '#fff',
                drawBorder: false,
                shadow: false, 
            },
            seriesDefaults: {
                showMarker: true,
                pointLabels: {
                    show: true
              }
            },
            title: {
                fontFamily: 'arial',
                //text: 'Statistical Report',
                textAlign : 'center',
                fontSize: '20px'
            },              
            seriesColors: [<?php
                $num = 0;
                foreach($graphical['item'] as $item){
                    echo "'#". $color[$num] ."',";
                    $num++;
                }
            ?>],
            animate: true,                  
            animateReplot: true,
            cursor: {
                show: true,
                zoom: false,
                looseZoom: false,
                showTooltip: true
            },                      
            axes:{
                yaxis:{
                    min : 0,
                   // tickOptions:{
                     //   formatString:'$%.2f'
                      //  }
                }  ,
                xaxis:{
                    renderer:$.jqplot.DateAxisRenderer,
                    tickOptions : {
                        formatString: '%B',
                        formatter : function(format,value) {
                            return ' abds ';
                        }
                    }
                }
            }
        }); 
        
        $('.jqplot-xaxis div:first-child').hide();    
        $('.jqplot-xaxis div:last-child').hide();
    }); 
</script>