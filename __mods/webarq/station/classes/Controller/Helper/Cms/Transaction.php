<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Helper transaction controller
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Helper_Cms_Transaction extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
    }
    
    public function action_error() {
        $title = $content = 'No transaction';
        if (!empty(App::$config->message->transaction)) {
            $title   = 'Error';
            $content = App::$config->message->transaction;
        }
         
        $this->register('title')->use_string($title)
             ->register('content')->use_string($content)
             ;
    }
    
    public function action_success() {
        
    }
    
    public function action_save() {
        if (empty($this->post)) {         
            $this->register('title')->use_string('Error')
                 ->register('content')->use_string(__('no-data-post'))
                 ;        
            return false;
                    
        }        
        
        $debug  = empty($this->post->debug) ? false : true;
         
        #$debug  = true;
        
        $data = Data::create();        
        
        $save = $data->save($debug);
        
        if (!empty($save)) {
                
            if (!empty($this->post->app_transaction_module)){
                if (!empty($this->post->app_transaction_pack)) {
                    $pack = explode(',',$this->post->app_transaction_pack);
                    $count_pack = count($pack);
                    $iteration = 1; 
                    foreach ($pack as $index => $package) {
                        list($module,$group) = explode(":",$package,2);
                        if ($iteration ==  $count_pack && $count_pack > 1 ) {
                            $transaction_object .= ", and $module $group";
                        } else {                        
                            $transaction_object = isset($transaction_object) ? $transaction_object.", $module $group" : "$module $group";    
                        }
                        $iteration++;
                    }    
                } else {
                    $transaction_object = 'data';
                }
                App::$session->set("transaction_message",__('success.transaction',array(
                    ':object'=>$transaction_object)));
                $this->deflect($this->post->app_transaction_module.'/'.$this->post->app_transaction_controller);    
            } else {
                $this->deflect('helper/transaction/success');
            }
        } else {
            //Set message
            App::$session->set("transaction_message",__('error.transaction'));
            
            //Redirect url
            $query_r  = !isset($this->post->app_transaction_module) ? '' : '?m='.$this->post->app_transaction_module;
            $query_r .= !isset($this->post->app_transaction_controller) ? '' : (empty($query_r) ? '?c='.$this->post->app_transaction_controller : '&c='.$this->post->app_transaction_controller);
            $this->deflect('helper/transaction/error'.$query_r);
        }
    }
    
    public function action_update() {
        if (empty($this->post)) {         
            $this->register('title')->use_string('Error')
                 ->register('content')->use_string(__('no-data-post'))
                 ;        
            return false;
                    
        }        
        
        $debug  = empty($this->post->debug) ? false : true;
        
        #$debug  = true;
        
        $update = Data::update()->save($debug);
        
        if (!empty($this->post->app_transaction_module)){
            if (!empty($update)) {
                if (!empty($this->post->app_transaction_pack)) {
                    $pack = explode(',',$this->post->app_transaction_pack);
                    $count_pack = count($pack);
                    $iteration = 1; 
                    foreach ($pack as $index => $package) {
                        list($module,$group) = explode(":",$package,2);
                        if ($iteration ==  $count_pack && $count_pack > 1 ) {
                            $transaction_object .= ", and $module $group";
                        } else {                        
                            $transaction_object = isset($transaction_object) ? $transaction_object.", $module $group" : "$module $group";    
                        }
                        $iteration++;
                    }    
                } else {
                    $transaction_object = 'data';
                }
                App::$session->set("transaction_message",__('success.transaction',array(
                    ':object'=>$transaction_object)));
            }
                    
            $url = $this->post->app_transaction_module.'/'.$this->post->app_transaction_controller.'/form/edit';
            
            $param1 = Request::$initial->param('param1');
            if (!empty($param1)) { $url .= '/'.$param1; }
            
            $param2 = Request::$initial->param('param2');
            if (!empty($param2)) { $url .= '/'.$param2; }
            
            $this->deflect($url);    
        } else {
            $this->deflect('helper/transaction/success');
        }
    }
}