<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Ajax Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Helper_Cms_Notemplate extends Controller{

    public function before() {
        parent::before();
        
        $cookie = App::cookie('webarqcms');
        $post   = $this->request->post('cookie');
        $cookie = !empty($post) ? $this->request->post('cookie') : $cookie;
        list($root,$app,$salt,$session) = explode('::',$cookie,4);                
        App::$config->salt->encrypted =  $salt; 
        User::current($cookie);
        
        if ((empty(User::admin()->is_login) || empty(User::admin()->is_admin)) && ($this->request->controller() != 'auth' && $this->request->controller() != 'Auth')) {
            die(__('error.403'));
        }
    }
    
    public function action_index() {
        $this->response->body('Index');    
    }
    
    
    public function action_upload_photo_profile() {
        $directory = URL::media('temp',true);
        $post_file = $this->request->post('Filename');
        $file_name = Helper_File::name($post_file);
        $file_ext  = Helper_File::type($post_file);
        $file_name = User::admin()->username.'-'.Helper_File::system_name($file_name).'.'.$file_ext;
        
        $upload = Upload::save($_FILES['photo_profile'],$file_name, $directory);
        
        $this->response->body($file_name);
    }
    
    public function action_test_uri() {
        $uri = array (
            'page' => array
            (
                'a' => Request::$initial->uri(),
                'b' => URL::base(TRUE, FALSE).Request::$initial->uri(),
                'c' => URL::site(Request::$initial->uri()),
                'd' => URL::site(Request::$initial->uri(), TRUE),
            ),

            'application' => array
            (
                'a' => URL::base(),
                'b' => URL::base(TRUE, TRUE),
                'c' => URL::site(),
                'd' => URL::site(NULL, TRUE),
            ),
        );
        
        $this->response->body(Debug::vars($uri));
    }
}