<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Ajax Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Helper_Cms_Ajax extends Controller_Default_Template_Ajaxcms {
    
    public function action_index() {
        
    }
    
    public function action_t123() {
        
        $x = Data::create()
            //->master('navigation')
            ->pairs(
                array(
                    'products' => array(
                        'label'       => 'product_lbl',
                        'description' => 'product_dsc',
                    ),
                    'product_category_maps' => array (
                        'product_category_id' => 'product_ctd'
                    )
                )
             )
             /** **/
            ->foreign('product_category_maps',array('product_category_id'))
            ->post(array(
                'product_lbl' => 'Single Product',
                'product_dsc' => 'Single Product Description',
                'product_ctd' => array (2,3,5),
            ))
            /** **/
            /** **/
            ->post(array(
                'product_lbl' => array('en'=>'New Product After','id'=>'Produk Baru Baru'),
                'product_dsc' => array('en'=>'New Product Description After','id'=>'Deskripsi Produk baru Baru'),
                'product_ctd' => array('en'=>array(2,5,6),'id'=>array(2,5)),
                'product_ctd' => array(2,5,6),
                'update_time' => true
            ))
            /** **/
            ->save(true);
            
    }
    
    public function action_st_upload() {
        
    }
    
    public function action_old_data() {             
        $t = !empty($this->post->t) ? trim($this->post->t) : null;      //table name
        $f = !empty($this->post->f) ? trim($this->post->f) : null;      //field query name
        $q = !empty($this->post->chk) ? trim($this->post->chk) : null;  //field query value
        $c = !empty($this->post->c) ? trim($this->post->c) : null;      //field condition name
        $v = !empty($this->post->v) ? trim($this->post->v) : null;      //field condition value
        
        if (!isset($t) || !isset($f) || !isset($q) || !isset($c) || !isset($v) ) {
            $this->content = 'false';
        } else {
            $select = Model::factory('Dba')->select($f)->from($t)->where("$c = $v")->and_where("$f = $q")->execute()->current();
            $this->content = !empty($select) ? 'true' : 'false';
        }
    }
    
    public function action_check_exist() {          
        $t = !empty($this->post->t) ? trim($this->post->t) : null;      //table name
        $f = !empty($this->post->f) ? trim($this->post->f) : null;      //field query name
        $q = !empty($this->post->chk) ? trim($this->post->chk) : null;  //field query value
        $c = !empty($this->post->c) ? trim($this->post->c) : null;      //field condition name
        $v = !empty($this->post->v) ? trim($this->post->v) : null;      //field condition value
        
        if (!isset($t) || !isset($f)) { 
            $this->content = 'false';
        } else {
            $t = substr(base64_decode($t),7);
            $f = substr(base64_decode($f),7);
            
            $builder = Model::factory('Dba')->select($f)->from($t)->where("$f = $q")->limit(1);            
            if (isset($c) && isset($v)) {
                $builder->and_where("$c <> $v");    
            }
            
            $select  = $builder->execute()->current();
            
            $this->content = empty($select) ? 'true' : 'false';
        }
        
    }
    
    public function action_publish() {
        $table  = $this->param1;
        $id     = $this->param2;
        
        if ( empty($table) || empty($id) ) { $this->content = 'false'; return; }
        
        $label  = $this->param3;
        $field_id = $this->request->query('primary');
        $field_id = isset($field_id) ? $field_id : 'id';
        
        //$author = Inflector::singular($table);
        
        //if (User::authorise($author,"publish-$author")) {
            $builder = Model::factory('Dba')->update($table)->set('is_active',1)->where("$field_id = $id");
            
            $is_system = $this->request->query('is_system'); 
            
            if (isset($is_system)) { $builder->and_where("is_system = $is_system"); }
            
            $do = $builder->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,$table,$label,'publish');
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        //} else {
        //    $this->content = 'true';
        //}
    }
    
    public function action_unpublish() {
        $table  = $this->param1;
        $id     = $this->param2;
        
        if ( empty($table) || empty($id) ) { $this->content = 'false'; return; }
        
        $label  = $this->param3;
        $field_id = $this->request->query('primary');
        $field_id = isset($field_id) ? $field_id : 'id';
        
        //$author = Inflector::singular($table);
        
        //if (User::authorise($author,"publish-$author")) {
            $builder = Model::factory('Dba')->update($table)->set('is_active',0)->where("$field_id = $id");
            
            $is_system = $this->request->query('is_system'); 
            
            if (isset($is_system)) { $builder->and_where("is_system = $is_system"); }
            
            $do = $builder->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,$table,$label,'unpublish');
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        //} else {
        //    $this->content = 'true';
        //}
    }
    
    
    
    public function action_feature() {
        $table  = $this->param1;
        $id     = $this->param2;
        
        if ( empty($table) || empty($id) ) { $this->content = 'false'; return; }
        
        $label  = $this->param3;
        $field_id = $this->request->query('primary');
        $field_id = isset($field_id) ? $field_id : 'id';
        
        //$author = Inflector::singular($table);
        
        //if (User::authorise($author,"publish-$author")) {
            $builder = Model::factory('Dba')->update($table)->set('is_feature',1)->where("$field_id = $id");
            
            $is_system = $this->request->query('is_system'); 
            
            if (isset($is_system)) { $builder->and_where("is_system = $is_system"); }
            
            $do = $builder->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,$table,$label,__('featured'));
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        //} else {
        //    $this->content = 'true';
        //}
    }
    
    public function action_unfeature() {
        $table  = $this->param1;
        $id     = $this->param2;
        
        if ( empty($table) || empty($id) ) { $this->content = 'false'; return; }
        
        $label  = $this->param3;
        $field_id = $this->request->query('primary');
        $field_id = isset($field_id) ? $field_id : 'id';
        
        //$author = Inflector::singular($table);
        
        //if (User::authorise($author,"publish-$author")) {
            $builder = Model::factory('Dba')->update($table)->set('is_feature',0)->where("$field_id = $id");
            
            $is_system = $this->request->query('is_system'); 
            
            if (isset($is_system)) { $builder->and_where("is_system = $is_system"); }
            
            $do = $builder->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,$table,$label,__('unfeature'));
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        //} else {
        //    $this->content = 'true';
        //}
    }
    
    public function action_delete() {
        $table    = $this->param1;
        $id       = $this->param2;
        $label    = $this->param3;
        $field_id = $this->request->query('primary');
        $field_id = isset($field_id) ? $field_id : 'id';
        $get      = $this->get;
        
        //Check for parent or category
        if (!empty($get)){
            foreach ($get as $key => $value) {
                if ($key == 'parent' || $key == 'parent_id' || strpos($key,'category_id') || strpos($key,'_subject_id') || $key == 'site_navigation_id') {
                    
                    $has_child = $key == 'parent' || $key == 'parent_id';
                    
                    if ($key == 'parent') {                
                        $parent_key   = 'parent_id';
                        $parent_value = $get->parent;
                    } else {                
                        $parent_key   = $key;
                        $parent_value = $get->$key;
                    }
                }
            }
        }
        
        //$author = Inflector::singular($table);
        
        if (empty($table) || !isset($id)) {
            $this->content = 'false'; return true;    
        }
        
        //if (User::authorise($author,"delete-$author")) {  
            $builder[0] = Model::factory('Dba')->delete($table)->where("$field_id = $id");
            
            if (!empty(App::$module->language)) {
                Translate::delete($table,$id);    
            }
            
            if (isset($get->is_system)) { 
                $is_system = $get->is_system; 
                $builder[0]->and_where("is_system = $is_system"); 
            }
            
            if (isset($get->ordering)) {
                $builder[1] = Model::factory('Dba')
                                    ->update($table)
                                    ->set("ordering",DB::expr('`ordering` - 1'))
                                    ->where("ordering > $get->ordering");
                                    
                if (isset($get->ordering_parent_field) && isset($get->ordering_parent_value)) {
                    if (is_array($get->ordering_parent_field)) {
                        $opvs = $get->ordering_parent_value;
                        foreach ($get->ordering_parent_field as $idx => $opr) {
                            $builder[1]->and_where($opr,"=",$opvs[$idx]);    
                        }
                    } else {
                        $builder[1]->and_where($get->ordering_parent_field,"=",$get->ordering_parent_value);
                    }    
                }
            }
            
            if (isset($parent_key) && isset($get->ordering)) {
                                
                $builder[1] = $builder[1]->and_where("$parent_key = ".$parent_value);
                
                if ($has_child === true) {
                    $total_destination = Model::factory('Dba')->select(DB::expr("COUNT(`$parent_key`) AS `total`"))
                                            ->from($table)
                                            ->where("$parent_key = ".$parent_value)
                                            ->execute()
                                            ->current()
                                            ->total-1;    
                                                        
                    $builder[3] = Model::factory('Dba')
                                        ->update($table)
                                        ->set('is_active',0)
                                        ->set("$parent_key",$parent_value)
                                        ->set("ordering",DB::expr("`ordering` + $total_destination"))
                                        ->where("$parent_key = $id");
                }
            }
            
            if (isset($get->relation_table)) {
                $relation_tables = is_array($get->relation_table) ? $get->relation_table : explode('-',$get->relation_table);
                $def_foreign_field = Inflector::singular($table).'_id';
                foreach ($relation_tables as $related_table) {
                    //Foreign Key
                    $foreign_field = $def_foreign_field;
                    
                    //Delete relation table medialibary
                    if ($related_table == 'media_library_applications') {
                        Model::factory('Medialibrary')->applicant_delete($table,$id);
                        continue;
                    }
                    
                    //Select relation table
                    if (!empty(App::$module->language)) {
                        $s = Model::factory('Dba')->select()->from($related_table)->where("$foreign_field = $id")->execute();
                    }
                    
                    //Delete relation table
                    $x = Model::factory('Dba')->delete($related_table)
                                    ->where("$foreign_field = $id")->execute(); 
                                    
                    //Delete relation table translation row
                    if (isset($s) && !empty($s[0])) {
                        foreach ($s as $i) {
                            if (isset($i->id)) {
                                Translate::delete($related_table,$i->id);       
                            }
                        }
                    }
                }
            }
            
            //Remove media library application
            Model::factory('Medialibrary')->applicant_delete($table,$id);
            
            foreach ($builder as $instance) {
                #echo $instance,'<br/>';
                $do_delete = $instance->execute();
                
                if (!empty($do_delete)) {
                    $do = true;
                }
            }
            
            #die();
            
            if (!empty($do)) {                
                //Record history
                History::simple_record($id,$table,$label,'delete');
                
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        //} else {
        //    $this->content = 'true';
        //}
    }
    
    public function action_reordering() {
        if (empty($this->post->sorting) || empty($this->post->table)) {
            $content = 'false';
        } else {
            
            $ref_uri = $this->request->referrer();
            
            $query_page = stripos($ref_uri,'page=');
            
            $page = 1;
            
            if ($query_page !== false) {
                $temp_ref   = substr($ref_uri,($query_page+5));
                
                $query_next = stripos($temp_ref,'&');
                
                $page       = $query_next === false ? (int)$temp_ref : (int)(substr($temp_ref,0,$query_next));
            } 
            
            $sorting = explode("&",$this->post->sorting);
                        
            if (!empty($sorting)) {
                
                $by_field = empty($this->post->by_field) ? 'id' : $this->post->by_field;
                
                foreach ($sorting as $key => $row) {
                    $value = substr($row,(stripos($row,'='))+1);
                    
                    $items = explode('.',$value);
                    
                    $new_order = (($page-1)*App::$config->perpage)+$key+1;
                         
                    $model = Model::factory('Dba')
                                        ->update($this->post->table)
                                        ->set("ordering",$new_order)
                                        ->where("$by_field = $items[1]")
                                        ->execute();
                    
                }
                
                $content = (($page-1)*App::$config->perpage)+1;
            }
        }    
        
        $this->content = $content;
    }
    
    public function action_order_challenge() {
        if (!empty($this->post->table) && isset($this->post->parent_value)) {
            $parent_field = empty($this->post->parent_field) ? 'parent_id' : $this->post->parent_field;
            
            $maximum = Model::factory('Dba')
                            ->select(DB::expr("COUNT(`$parent_field`) AS `total`"))
                            ->from($this->post->table)
                            ->where($parent_field,"=",$this->post->parent_value)
                            ->execute()
                            ->current()
                            ->total;
            $this->content = empty($maximum) ? 1 : ($this->post->form_type == 'create' ? $maximum+1 : $maximum);
        } else {
            $this->content = 'false';
        }
    }
}