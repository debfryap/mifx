<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


Class Controller_Error_Cms extends Controller_Default_Template_Cms {

    public function before() {
        parent::before();
    }
    
    public function action_index() {
        echo 'Error CMS';
    }    
}