<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Dashboard_Cms_Manage extends Controller_Default_Template_Cms {
    
    public function before() {
        
        $this->template = 'dashboard';
        
        parent::before();
        
        //Active menu
        App::$config->menu->group = 'dashboard';
        
        //Meta title
        $this->meta('title',__('dashboard')); 
        
        //Authorisation
        //$this->authorise('config','edit-configuration');      
        
        //Set static scripts
        $this->register('static_scripts','<!--[if IE 7]><script language="javascript" type="text/javascript" src="'. URL::sysJs('jquery/jqplot/excanvas.js') . '"></script><![endif]-->
                                                                <!--[if IE 8]><script language="javascript" type="text/javascript" src="' . URL::sysJs('jquery/jqplot/excanvas.js') . '"></script><![endif]-->
                                                        	    <!--[if IE 9]><script language="javascript" type="text/javascript" src="' . URL::sysJs('jquery/jqplot/excanvas.js') . '"></script><![endif]-->');
     
        $this->styles(URL::assets('main','default/cms/css','css'));
        $this->styles(URL::sysCss('jquery/jqplot.min'));
        $this->scripts(URL::sysJs(
                            'jquery/jqplot/jquery.jqplot.min'
                            ,'jquery/jqplot/jqplot.barRenderer.min'
                            ,'jquery/jqplot/jqplot.highlighter.min'
                            ,'jquery/jqplot/jqplot.pointLabels.min'
                            ,'jquery/jqplot/jqplot.dateAxisRenderer.min'));
    }
        
    public function action_index() {                
        //Initialize user helper
        $helpUser   = User::instance();
        
        //Get admin login
        $loginAdmin = $helpUser->whoIsLoginAdmin();
                
        $this->register('title',__('dashboard'))
             ->register('content')
                ->use_view('station/cms/dashboard')
                    ->set('graphical',Model::factory('History')->dashboard(7))
                    ->set('activities',Model::factory('History')->role_group_histories(5,true))
                    ->set('users',Model::factory('User')->tabular()->limit(3)->execute())
                    ->set('loginAdmin',$loginAdmin);
    }
}
    