<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Config_Cms_moduleSetting extends Controller_Default_Template_Cms {
        
    public function action_index() {
        $this->register('title')
                ->use_string(__('module-setting'))
            ;    
    }
}
    