<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Config_Cms_generalSetting extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active menu
        App::$config->menu->active = 'general-setting';
        
        //Meta title
        $this->meta('title','General Setting'); 
        
        //Authorisation
        $this->authorise('config','edit-configuration');      
    }
        
    public function action_index() {
        $this->action_form();
    }
    
    public function action_form() { 
        //Activate media validate
        $this->media_validate();
        
        //Uploadify
        $this->media_uploadify();
               
        $this->register('title')
                ->use_string(__('general-setting'))
             ->register('content')
                ->use_widget('station/general-setting')
                ->set('model','dba')       
            ;    
    }
    
    public function action_update() {
        //Site configuration
        $this->post->site['system']['offline'] = $this->post->site['system']['offline'] == 0 ? true : false;
        
        //Logo
        if (!empty($_FILES['logo']['name'])) {
            $folder = App::$config->logo->folder."/";
            if ($file = Upload::save($_FILES['logo'],NULL,$folder)) {
                foreach (array('gif','jpg','jpeg','png') as $type) {
                    if (file_exists($folder."logo.$type") 
                            && strtolower(App::$config->logo->file) != strtolower("logo.$type")) {
                        unlink($folder."logo.$type");                        
                    }        
                }
                                
                $sysExt  = Helper_File::type($file);
                $sysName = "logo.$sysExt";
                
                $this->post->site['logo'] =  array (
                            "width"         => App::$config->logo->width,
                            "height"        => App::$config->logo->height,
                            "folder"        => App::$config->logo->folder,
                            "file"          => $sysName   
                        );
                
                Picture::resize(array(
                        'src'    => $file,
                        'dest'   => $folder,
                        'name'   => $sysName,
                        'width'  => App::$config->logo->width,
                        'height' => App::$config->logo->height,
                        'delete_src' => true
                    )); 
            }
        } else {
            $this->post->site['logo'] =  array (
                        "width"         => App::$config->logo->width,
                        "height"        => App::$config->logo->height,
                        "folder"        => App::$config->logo->folder,
                        "file"          => App::$config->logo->file   
                    );
        }
        if (!empty($_FILES['footerLogo']['name'])) {
            $folder = App::$config->logo->folder."/";
            if ($file = Upload::save($_FILES['footerLogo'],NULL,$folder)) {
                foreach (array('gif','jpg','jpeg','png') as $type) {
                    if (file_exists($folder."logo.$type") 
                          && !empty(App::$config->footerLogo->file)  
                          && strtolower(App::$config->footerLogo->file) != strtolower("footerLogo.$type")) {
                        unlink($folder."logo.$type");                        
                    }        
                }
                                
                $sysExt  = Helper_File::type($file);
                $sysName = "footerLogo.$sysExt";
                
                $this->post->site['footerLogo'] =  array (
                            "width"         => 99,
                            "height"        => 50,
                            "folder"        => App::$config->logo->folder,
                            "file"          => $sysName   
                        );
                
                Picture::resize(array(
                        'src'    => $file,
                        'dest'   => $folder,
                        'name'   => $sysName,
                        'width'  => 75,
                        'height' => 50,
                        'delete_src' => true
                    )); 
            }
        } else {
            $this->post->site['footerLogo'] =  array (
                        "width"         => App::$config->footerLogo->width,
                        "height"        => App::$config->footerLogo->height,
                        "folder"        => App::$config->footerLogo->folder,
                        "file"          => App::$config->footerLogo->file   
                    );
        }
        
        //Json site configuration
        $data_sc = json_encode($this->post->site);
        
        $result_sc = DB::update('configurations')
                    ->set(array("value"=>$data_sc))
                    ->where("key","=","site")
                    ->execute();
        
        //User configuration
        $data_uc = array (
                'check_ip'      => $this->post->allow_ip_check == 0 ? false : true,
                'captcha'       => $this->post->allow_captcha == 0 ? false : true,
                'hash'          => 'ripemd160',
                'loginbyemail'  => true,
                'images'        => array (
                    "small"     => array (
                        'path'  => 'users/avatars/50x61/',  
                        'size'  => array(50,61)
                    ),
                    "medium"    => array (
                        'path'  => 'users/avatars/120x145/',
                        'size'  => array(120,145)
                    )
                )
            );     
        $data_uc = json_encode($data_uc);
        
        $result_uc = DB::update('configurations')
                    ->set(array("value"=>$data_uc))
                    ->where("key","=","user")
                    ->execute();
                    
        $return = '';
        
        if (!empty($_FILES['favIcon']['name'])) {
            if (move_uploaded_file($_FILES['favIcon']['tmp_name'], $_FILES['favIcon']['name'])) {
                $result_sc = true;
            }
        }
        
        if (!empty($result_sc) || !empty($result_uc)) {
            App::$session->set("transaction_message",__('success.transaction',array(
                ':object'=>'Site configuration')));
            $return = '/success';
            
            //Append history 
            if (!empty($result_sc)) {
                DB::update('configurations')
                    ->set(array("update_time" => App::date()))
                    ->where("key","=","site")
                    ->execute();
                History::record(array(
                    'row_id'      => DB::select('id')->from('configurations')->where("key","=","site"),
                    'table'       => 'configurations',
                    'actor'       => User::admin()->username,
                    'description' => '{"object":"site","action":"update"}'
                    
                ));
            }
                
            if (!empty($result_uc)) {
                DB::update('configurations')
                    ->set(array("update_time" => App::date()))
                    ->where("key","=","user")
                    ->execute();
                History::record(array(
                    'row_id'      => DB::select('id')->from('configurations')->where("key","=","user"),
                    'table'       => 'configurations',
                    'actor'       => User::admin()->username,
                    'description' => '{"object":"user","action":"update"}'
                ));
            }
            
        } 
        $this->deflect('config/general-setting/form'.$return);
    }
}
    