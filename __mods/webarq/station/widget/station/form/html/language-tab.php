<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Station Form Language Tab
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  
?>
<!-- language tab -->
<div class="tab-selector lang">
    <ul id="lang-tab">
        <?php
            foreach (Language::$active as $key => $language) {
                echo '<li id="'.$language->code.'"'; echo $key == 0 ? ' class="active"':''; echo '>';
                echo '<span'; echo $key == 0 ? ' class="default"':''; echo '>';
                echo ucfirst(strtolower($language->label)); 
                echo '</span>';
                echo '</li>';                    
            } 
            echo '<div class="clear"></div>';
        ?>
    </ul>
</div>
<!-- end language tab -->            