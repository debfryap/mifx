<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Station Form Media Tab
 * @Module      Kernel
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  
?>
<!-- media tab -->
<div class="tab-selector media">
    <ul id="media-tab">
        <?php
            //echo '<li id="media-content" class="active"><span>Content</span></li>';
            foreach ($media_items as $media) {
                
                list($type,$name) = $media;
                
                if (!isset(Medialibrary::$config->media_library_application_types->$type->$name))
                    throw new Kohana_Exception('Invalid media :type :name',array(   
                                ':type' => $type,
                                ':name' => $name
                            ));
                
                $config = Medialibrary::$config->media_library_application_types->$type->$name;
                
                if (isset($config->details)) {
                    if (!isset($config->details[0]->label))
                        $config->details[0]->label = __($config->details[0]->name);
                    else
                        $config->details[0]->label = __($config->details[0]->label);
                    $json  = json_encode($config->details);
                }
                                          
                echo '<li '
                        ,'id="media-'.$name.'" '
                        ,'data-type="',$type,'" '
                        ,'data-name="',$name,'" '
                        ,(isset($json) ? "data-detail='$json'" : '')
                        ,(isset($config->limit) ? "data-limit=$config->limit" : '')
                        ,'>';
                echo '<span>';
                echo isset($config->label) ? __($config->label) : (is_string($config) ? __($config) : __($name)); 
                echo '</span>';
                echo '</li>';    
                
                unset($json);                
            } 
            echo '<div class="clear"></div>';
        ?>
    </ul>
</div>
<!-- end media tab -->            