<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Station Form View
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

if (isset($print) && $print === false) {
    
    if (!empty($validation)) {          
        echo '<script type="text/javascript">$(document).ready(function(){ main_form_validate(); });</script>';      
    }
    return;
}

//Define action
if (empty($action)) {
    $p2 = Request::$initial->param('param2');
    $p3 = Request::$initial->param('param3');
    
    $fa = $type == 'create' ? 'save' : 'update/'.$p2.(isset($p3) && $p3 !== '' ? "/$p3" : "");
    $fa = $action === false
        ? URL::cms().'helper/transaction/'.$fa
        : URL::cms().$xmodule.'/'.$controller.'/'.$fa;
} else {
    $fa = $action;
}

$multilang = $multilang === true 
                && !empty(App::$module->language) 
                && !empty(Language::$active)
                && count(Language::$active) > 1;

if ($type == 'edit' && (empty($values) || $values == 'auto') && empty($manual)) {
    echo __('error.unauthorized');
    return;
}
#echo Debug::vars($values); return;
?>


<div id="station-form-wrapper">
    <!-- start form -->
    
    <?php
        echo !empty($title) ? '<div class="title-form">'.$title.'</div>' : "";
     
        $attributes['class'] = !empty($attributes['class']) ? "main-editor ".$attributes['class'] : "main-editor";
        $attributes['autocomplete'] = "off";
        echo Form::open($fa,$attributes);
        
        echo '<!-- start element -->'; 
        if ($multilang === true) {
            $language_tab = Kohana::find_file('widget/'.$_path.'/html/','language-tab');
            
            echo Form::hidden('app_transaction_multilanguage','true');          
            
            if (!empty($language_tab))
                echo Widget::capture($language_tab);
        } 
        
        
        
        
        if (isset($media) && !empty($media) && !empty(App::$module->medialibrary)) {
            if (empty($multilang)) {
                echo '<div class="tab-selector content-text"><ul id="content-tab"><li><span class="default">Content</span></li></ul></div>';
            }
            
            $media_tab = Kohana::find_file('widget/'.$_path.'/html/','media-tab');
            if (!empty($media_tab)) {
                echo Widget::capture($media_tab,array('media_items' => $media['type'],'tab-language'=>$multilang));
            }
            #echo Debug::vars($media);
        }
        
        if (!isset($language_tab) && !isset($media_tab))
            echo '<div class="line-break clear_left">&nbsp;</div>';
        
        
        if ($action === false) {
            echo Form::hidden('app_transaction_module',$xmodule);
            echo Form::hidden('app_transaction_controller',$controller);
        }
        
        /** Print packing module **/
        if (!empty($pack)) {      
            $p = '';  
            foreach ($pack as $k => $v) {
                $p .= empty($p) ? $v : ','.$v;
            }
            echo Form::hidden('app_transaction_pack',$p);
        }
        
        /** Print each table transaction column **/
        if (!empty($transaction)&& $pair_transaction === true) { 
            echo '<div style="visibility: hidden;position:absolute;top:-3000px;">';
            foreach ($transaction as $table => $pairs) {
                foreach ($pairs as $pair_column => $pair_form) {
                    if (is_array($pair_form)) {
                        foreach ($pair_form as $pair_form_key => $pair_form_value) {
                            echo Form::hidden("app_transaction_pairs[$table][$pair_column][$pair_form_key]",$pair_form_value);
                        }
                    } else {
                        echo Form::hidden("app_transaction_pairs[$table][$pair_column]",$pair_form);
                    }
                    
                }
            }
            echo '</div>';
        }
        /** End for each table transaction column **/
    ?>

    <?php echo !empty(App::$config->message->transaction) ? '<div id="transaction_message">'.App::$config->message->transaction.'</div>' : ''; ?>  
    <div id="content">
        <div class="media-container active" id="media-content-container">
        <?php         
            /** Print each form element **/
            foreach ($elements as $i => $object) {
                if (isset($object->special)) {
                    echo $object->special;
                    continue;
                }
                
                //Column table            
                $column_table = $object->tb_column;
                
                //Sub Title Closing
                if (!empty($object->subtitle) && !empty($sub_count)) {
                    echo '</div>';
                }
                
                //Sub Title Opening
                if (!empty($object->subtitle)) {
                    $subtitle  = __($object->subtitle);                    
                    $sub_count = empty($sub_count) ? 1 : $sub_count+1;                    
                    $sub_container = Helper_Kernel::name($object->subtitle);
                    
                    if ($sub_count > 1 || $i > 1)
                        echo '<div class="break20"></div>';
                    
                    echo "<div id='block-$sub_container'>";
                                            
                    echo "<div class='subtitle_container'>";
                    echo "<span>$subtitle</span>";
                    echo "</div>";
                    echo "<div class='break20'></div>";
                    
                    $sub_open  = true;
                    $sub_close = false;
                }
                    
                // Labeling
                if ($object->type != 'hidden') {
                    if (($object->type != 'checkbox' && $object->type != 'radio') || !empty($object->multiple)) {
                        //Form Label
                        echo '<div class="label';
                        echo $column == 2 ? " block_left" : "";
                        echo '">';
                        echo __($object->label);
                        echo '</div>';
                        
                        if ($column == 2)
                            echo '<div class="center block_separator">:</div>';
                        else
                            echo '<div class="break5">&nbsp;</div>';
                    } else {
                        if ($column == 2) {
                            echo '<div class="center no_label">&nbsp;</div>';
                        }
                    }
                }
            
                        
                //Form Element
                $hidden_class = $object->type == 'hidden' ? 'block_hidden ' : null;
                echo '<div class="',$hidden_class,' block_',$object->name,($column == 2 ? " block_right" : ""),'">';
                    if ($multilang === true && (isset($object->multilang) && $object->multilang === true)) {
                        foreach (Language::$active as $j => $lang) {
                            $tab_content_class = $j == 0 ? "active" : "hide";
                            
                            //Clone object for each of language
                            $cloning       = clone $object;
                            $cloning->id   = empty($cloning->id) 
                                                ? "$cloning->name-$lang->code"
                                                : "$cloning->id-$lang->code";
                            $cloning->name = "$cloning->name[$lang->code]";
                            $cloning->lang = $lang->code;    
                            $cloning->check_by_lang = $check_by_lang;
                            
                            //Get rid class require,old_data,is_available from secunder language
                            if ($check_by_lang === false)  {
                                if ($lang->code !== Language::$default && !empty($cloning->class)) {
                                    $_temp_checking_class = array(
                                                                "required",
                                                                "old_data",
                                                                "is_available",
                                                                );
                                    $cloning->class = trim(str_replace($_temp_checking_class,"",$cloning->class));    
                                } 
                            }
                                                                       
                            // Value overwrite
                            if (is_array($values) && isset($values[$column_table][$lang->code]) && empty($manual)) { 
                                $cloning->value =  $values[$column_table][$lang->code];
                            }
                        
                            if ($cloning->type == 'ordering' && isset($cloning->parenting)) {
                                foreach ($elements as $i2 => $element2) {
                                    if ($element2->name == $cloning->parenting) {                                
                                        $tmp['field'] = $cloning->parenting; 
                                        $tmp['value'] = empty($element2->value) ? 0 : $element2->value;
                                        $cloning->parenting = $tmp;
                                        unset($tmp);  
                                        break;
                                    }    
                                }
                            }
                                                        
                            echo '<div class="tab-content lang lang-'.$lang->code.' '.$tab_content_class.'" id="lang-'.$lang->code.'">';
                            echo Data_Form_Html::element(array(
                                    'element'   => $cloning 
                                ));
                            echo '</div>';
                        }
                    } else {
                        if ($multilang) {
                            //Change on Thurs, May 30, 2013. Should used system lang i/o default lang
                            $key_value = Language::$system; 
                                                   
                            // Value overwrite
                            if (is_array($values) && isset($values[$column_table][$key_value]) && empty($manual)) { 
                                $object->value =  $values[$column_table][$key_value];
                            }
                            echo '<div class="tab-content lang lang-'.Language::$default.'" id="lang-'.Language::$default.'">';
                        } else {                                               
                            // Value overwrite
                            if (is_array($values) && isset($values[$column_table]) && empty($manual)) { 
                                $object->value =  $values[$column_table];
                            }
                            echo '<div>';
                        } 
                        
                        if ($object->type == 'ordering' && isset($object->parenting)) {
                            foreach ($elements as $i2 => $element2) {
                                if ($element2->name == $object->parenting) {                                
                                    $tmp['field'] = $object->parenting; 
                                    $tmp['value'] = empty($element2->value) ? 0 : $element2->value;
                                    $object->parenting = $tmp;
                                    unset($tmp);  
                                    break;
                                }    
                            }
                        }                                             
                        
                        $object->row_id = $row_id;   
                        $object->job_type = $type;                  
                                            
                        echo Data_Form_Html::element(array(
                                'element'   => $object 
                            ));
                            
                        echo '</div>';
                    }
                    
                    //Error information
                    if ($object->type != 'hidden') {
                        echo '<div class="error_information"></div>'; 
                    }
                    
                    //Element preview
                    if (!empty($object->preview)) {
                        $preview_width  = isset($object->preview->width) ? $object->preview->width : 300;
                        $preview_height = isset($object->preview->height) ? $object->preview->height : 180;
                        echo '<img src="',$object->preview->src,'" style="width:',$preview_width,'px;height:',$preview_height,'px;border:2px solid #ccc;margin-top:10px;"/>';
                    }                
                    
                    //Form Information
                    if (!empty($object->info)) {
                        echo '<div class="element-information">',$object->info,'</div>';
                    }
                    
                    //Uploadify Calling
                    if (!empty($object->uploadify)) {
                        echo '<script type="text/javascript">',
                                '$(document).ready(function(){',
                                    'uploadify_maker($("#',$object->id,'"),{';
                                                        
                                        //Uploadify Attribute
                                        $dot = false;
                                        foreach ($object->uploadify as $ky => $va) {
                                            if (strtolower($ky) != 'formdata') {
                                                if ($dot === true) echo ',';
                                                $dot = true;
                                                echo '"',$ky,'" : "',$va,'"';                            
                                            } else {
                                                $formData = $va;
                                            }
                                        }   
                        
                                        //Uploadify form data
                                        echo ',"formData": {',
                                            //Token data
                                            '"token" : "',Cookie::get('webarqCms',Cookie::$salt),'"';
                                            if (!empty($formData)) {
                                                foreach ($formData as $ky => $va) {
                                                    echo ',"',$ky,'":"',$va,'"';
                                                } 
                                            }
                                        echo '}';                                         
                        echo '})';
                        echo '});</script>';
                    }
                echo '</div>';
                
                if (!empty($object->hint)) {
                    echo '<div class="frm-hint" data-hint="'.$object->hint.'"></div><div class="clear"></div>';
                    echo '<div class="clear"></div>';
                }
                
                //Line break
                if ($object->type != 'hidden') {
                    echo '<div class="line-break'; echo $column == 2 ? " clear_left" : ""; echo '">&nbsp;</div>';
                } 
                
                if ($object->type == 'submit' || $object->type == 'button')
                    $submit = $object;
                     
                $passed = true;
            } 
            /** End for each element **/
            
            //Sub Title Closing
            if (!empty($passed) && !empty($sub_count)) echo '</div>';
            
            if (empty($passed)) {
                echo 'no-data-form';
            } elseif (empty($submit) && $passed === true) {
                //Submit Button
                if ($column == 2) {
                    echo '<div class="center no_label">&nbsp;</div>';
                }
                
                $button_form  = '<div id="submit_wrapper" class="';
                $button_form .= $column == 2 ? " block_right" : "";
                $button_form .= '"><span>Submit</span>';
                #$button_form .= Form::submit("submit_form","Submit",array("class"=>"submit"));
                $button_form .= '</div>';
                
                if (!empty($cancel)) {
                    $button_form .= '<div id="cancel_wrapper" class="';
                    $button_form .= $column == 2 ? " block_right" : "";
                    $button_form .= '">';
                    $cancel_attrs['class'] = 'cancel-button';
                    if (is_bool($cancel)) {
                        if (!empty($xmodule) && !empty($controller)) {
                            $cancel_attrs['data-link'] = URL::cms().$xmodule.'/'.$controller;
                        } 
                    } else {
                        $cancel_attrs['data-link'] = $cancel;
                    }
                    $button_form .= Form::button("cancel_form","Cancel",$cancel_attrs);
                    $button_form .= '</div>';
                }
                    
                //Line break
                $button_form .= '<div class="break8'; 
                $button_form .= $column == 2 ? " clear_left" : ""; 
                $button_form .= '">&nbsp;</div>';                
            }
        ?>
        </div> <!-- End Media Content Container -->
        
        <?php 
            if (isset($media) && !empty($media) && !empty(App::$module->medialibrary)) { 
                echo '<div class="browseMedia">Add Media</div>';
                echo Form::hidden("media_application[table]",$media['table']);
                foreach ($media['type'] as $key => $item) {                                
                    //Get all application
                    list($type,$name) = $item;
                    
                    //Hidden previous application       
                    if (!empty($media['media'][$key][0])) {
                        foreach ($media['media'][$key] as $imedia) {                 
                            echo Form::hidden('current_media_applicant['.$name.']['.$imedia->id.']',$imedia->media_library_id);
                        }
                    }
                    
                    echo '<div class="media-container" id="media-',$name,'-container">';
                    
                    $arrType  = Helper_Kernel::object_to_array(Medialibrary::$config->extension->$type);
                     
                    $objMedia = Medialibrary::$config->media_library_application_types->$type->$name;
                    
                    echo '<div class="media-file-information"> Allowed type ',implode(', ',$arrType);
                    
                    if (isset($objMedia->width) || isset($objMedia->height)) {
                        echo '<p>';
                        echo 'And all file will be resize into';
                        echo isset($objMedia->width) ? " $objMedia->width px width" : "";
                        echo isset($objMedia->width) && isset($objMedia->height) ? " and" :"";
                        echo isset($objMedia->height) ? " $objMedia->height px height" : "";
                        echo '</p>';
                    }
                    echo '</div>';
                    
                    echo '<div class="isSortable">';
                                        
                    if (!empty($media['media'][$key][0])) {
                        foreach ($media['media'][$key] as $imedia) {  
                            $config = Medialibrary::$config;
                            echo '<div class="sortable media-item-container ',$type,'" id="item-container-',$imedia->media_library_id,'">';
                                echo '<div class="view-detail image icon';
                                if ($type == 'image') {
                                    $thumb_folder = URL::root(str_replace(Medialibrary::$_path,Medialibrary::$_thumb_path,$imedia->media_folder_name));
                                    echo '" style="margin-left:5px;width:',$config->thumb_width,'px;/**height:',($config->thumb_height+20),'px;**/">';
                                    echo '<img  src="',"$thumb_folder/$imedia->media_file_name",'" style="border:1px solid #ccc;width:',$config->thumb_width,'px;"/>';
                                    echo '<span style="margin-top:5px;" class="unlink">Unlink</span></div>';                                
                                } else {
                                    $extension  = Helper_File::type($imedia->media_file_name);
                                    $styles_bg  = file_exists("assets/__system/lib/images/files/".$extension.".png")
                                                    ? "assets/__system/lib/images/files/".$extension.".png"
                                                    : "assets/__system/lib/images/files/unknown.png";
                                    $styles_bg  = URL::root($styles_bg);    
        
                                    echo '" style="background-image:url('.$styles_bg.')"><span class="unlink">Unlink</span></div>';
                                }
                                
                                echo '<div class="view-detail text">';
                                    echo '<div class="inline-block label-section">Name</div>';
                                    echo '<div class="inline-block value-section"> : ',$imedia->media_file_name
                                            ,'<input type="hidden" name="media_application[',$name,'][',$imedia->media_library_id,'][type]" value="',$type,'"/>';
                                            
                                    if (in_array(strtolower(Helper_File::type($imedia->media_file_name)),Medialibrary::$image_type)) {
                                        echo '<input type="hidden" name="media_application[',$name,'][',$imedia->media_library_id,'][rel_path]" value="'
                                                        ,$imedia->media_folder_name,"/",$imedia->media_file_name,'"/>';
                                    }
                                    
                                    echo '</div>';
                                echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';                                    
                                
                                    if (isset($config->media_library_application_types->$type->$name->details)) {
                                        foreach ($config->media_library_application_types->$type->$name->details as $i => $item_detail) {
                                            
                                            if (isset($imedia->detail))
                                                $detail = json_decode($imedia->detail,true);
                                            
                                            echo '<div class="inline-block label-section">'
                                                    ,(isset($item_detail->label) ? __($item_detail->label) : __($item_detail->name))
                                                    ,'</div>';
                                                
                                            echo '<div class="inline-block value-section"> : ';
                                            
                                            //Manipulate attributes & join attributes
                                            $item_detail->type = empty($item_detail->type) ? 'text' : $item_detail->type;
                                            $dataAttr     = '';    
                                                                                    
                                            foreach ($item_detail as $attr_key => $attr_val) {
                                                
                                                if (strtolower($attr_key) == 'name') 
                                                    $dataAttr .= " $attr_key=\"media_application[$name][$imedia->media_library_id][detail][$item_detail->name]\"";
                                                elseif (strtolower($attr_key) == 'type')
                                                    $dataAttr .= " $attr_key=\"$item_detail->type\"";
                                                else    
                                                    $dataAttr .= " $attr_key=\"$attr_val\"";
                                            }
                                            //echo $dataAttr,'<br/>';
                                            $detailValue = !empty($detail[$item_detail->name]) ? $detail[$item_detail->name] : '';
                                            
                                            if ($item_detail->type != 'textarea') $dataAttr .= " value=\"".$detailValue."\"";
                                            
                                            switch($item_detail->type) {
                                                case "textarea" :
                                                    echo "<textarea",$dataAttr,">",$detailValue,"</textarea>";
                                                    break;
                                                default         :
                                                    echo "<input",$dataAttr,"/>";
                                            }   
                                            echo '</div>';
                                            echo '<div style="height:1px;line-height:1px;">&nbsp;</div>';
                                        }
                                    }
                                       
                                echo '</div>';
                                echo '<div class="clear"></div>';
                            echo '</div>';
                        }                        
                    }
                    
                    echo '</div>';           
                    
                    echo '</div>';                  
                }                
            }
        ?>
    </div> <!-- End ID Content -->
    
    <?php 
        if (isset($button_form)) echo $button_form;
        echo Form::close(); ?>
    <!-- end of form -->
</div>

<?php 
    if (!empty($validation)) {          
        echo '<script type="text/javascript">$(document).ready(function(){ main_form_validate(); });</script>';      
    }
?>