var selector_lang,selector_ctxt,selector_lang_button;

function init_ckeditor(id_element,config) {
    if (typeof id_element == 'undefined') { return false; }
    var editor = $('.'+id_element+'-editor');
    
    if (typeof config == 'undefined') {
        config = {
            'toolbar' : id_element,
            'width'   : 800,
            'height'  : id_element == 'intro' ? 120 : 300,
            'directory' : 'bobo'
        }
    }   
    editor.each(function(){
        $(this).ckeditor(function(){},config);
    }); 
}

function main_form_validate(id) {
    $("form").each(function () {
        $("#" + this.id).validate({
            // Multilang check error element
            invalidHandler : function(form, validator) {
                if (selector_lang.length > 0) {
                    var self_validate_errors = validator.numberOfInvalids();                
                    if (self_validate_errors) {
                        // Get active lang
                        var activeLang = selector_lang.children('ul').children('li.active').attr('id');
                        
                        // Get error field in active lang
                        var goToFirst = true;
                        for (var i=0;i<validator.errorList.length;i++){
                            var myBad = $(validator.errorList[i].element).parent('div');
                            
                            if (myBad.hasClass('lang-'+activeLang) === true || typeof myBad.attr('id') == 'undefined') {
                                goToFirst = false; break;    
                            }
                        }
                        if (goToFirst === true) {
                            myBad = $(validator.errorList[0].element).parent('div').attr('id').split('-')[1];
                            
                            selector_lang_button.removeClass('active');
                            selector_lang.children('ul').children('li#'+myBad).addClass('active');
                            
                            $('.tab-content.lang.active').removeClass('active').addClass('hide');
                            $('.tab-content.lang.hide.lang-'+myBad).removeClass('hide').addClass('active');                        
                        }
                    }
                }
            },     
            errorPlacement : function(error, element) {
                //$(element).parent('div').siblings('.error_information')
                $(element).parent('div').next('.error_information')
                          .html(error); 
            } 
        }); 
    });
     
    
    // Additional rules
    co = $('.old_data');    
    if (co.length > 0) {
        is_old_data_id = new Array();
        co.each(function(i){
            elem = $(this);
            data_ajax = $.parseJSON(elem.attr('data-ajax'));
            data_url  = typeof data_ajax.url !== 'undefined' ? data_ajax.url : bs_cms+'helper/ajax/old-data';
                
            is_old_data_id[i] = elem.attr('id');
                
            data_t = typeof data_ajax.t == 'undefined' ? 0 : data_ajax.t;       //table
            data_f = typeof data_ajax.f == 'undefined' ? 0 : data_ajax.f;       //field 
            data_c = typeof data_ajax.c == 'undefined' ? 'id' : data_ajax.c;    //condition field
            data_v = typeof data_ajax.v == 'undefined' ? 0 : data_ajax.v;       //condition value
                        
            elem.rules("add", {
                remote: {
                    url  : data_url,
                    type : "post",
                    data : {
                      t  : data_t,  
                      f  : data_f,
                      c  : data_c,
                      v  : data_v,
                      chk: function() {
                        return $('.old_data#'+is_old_data_id[i]).val();
                      }
                    }
                }
            });       
        });  
    }   
    
    //Additional rules
    ci = $('.is_available')
    if (ci.length > 0) {
        is_avaiable_id = new Array();
        ci.each(function(i){
            elem = $(this);
            data_ajax = $.parseJSON(elem.attr('data-ajax'));
            data_url  = typeof data_ajax.url !== 'undefined' ? data_ajax.url : bs_cms+'helper/ajax/check-exist';
            
            if (typeof data_ajax.t == 'undefined' || typeof data_ajax.f == 'undefined') return 0; //Skip current element
            
            is_avaiable_id[i] = elem.attr('id');
            
            data_t = data_ajax.t; //table
            data_f = data_ajax.f; //field 
            data_c = typeof data_ajax.c == 'undefined' ? 'id' : data_ajax.c; //condition field
            data_v = typeof data_ajax.v == 'undefined' ? null : data_ajax.v; //condition value
                        
            elem.rules("add", {
                remote: {
                    url  : data_url,
                    type : "post",
                    data : {
                      t  : data_t,  
                      f  : data_f,
                      c  : data_c,
                      v  : data_v,
                      chk: function() {
                        return $('.is_available#'+is_avaiable_id[i]).val();
                      }
                    }
                }
            });     
        });  
    }
}

function order_challenger(el) {
    
    form_type = $('#ordering').attr('data-form-type');
    
    if (el.attr('data-current-value') !== 'undefined' && el.attr('data-current-value') != el.val()) {
        form_type = 'create';
    }
    
    oca_post = {table:$('#ordering').attr('data-table'),parent_value:el.val(),form_type:form_type};
    
    if (typeof $('#ordering').attr('data-field') !== 'undefined') {
        oca_post.parent_field = $('#ordering').attr('data-field');
    }
    
    nous_process_message('Please wait for a while ...',true);
    
    setTimeout(function(){
        $.ajax({
            type    : 'POST',
            url     : bs_cms+'helper/ajax/order-challenge',
            data    : oca_post,
            async   : true,
            success : function(max) {
                max = $.trim(max);
                if (max !== 'false') {                        
                    $('#ordering').attr('max',max).removeAttr('readonly').val(max);
                    
                    if (parseInt(max) == 1) {
                        $('#ordering').attr('readonly','readonly');
                    }        
                }     
                nous_disable_message();                   
            }
        });  
    },10); 
}

function uri_sanitizer(res) {
    //default uri sanitizer value
    us = '';     
    
    //Check for tailing element
    if ($('.tailing-url').length > 0) {
        tailingUrl = $('.tailing-url option:selected').attr('data-permalink');
        if (typeof tailingUrl !== 'undefined')
            us = tailingUrl;                   
    }
    
    //Check for data referral
    if ($('.referrer').length > 0) {
        tR = $('.referrer');
        eR = tR.attr('data-referral');
        eR = $('.form-field.'+eR);
        
        if(typeof eR.attr('data-separator') == 'undefined')
            sp = '-'
        else
            sp = eR.attr('data-separator');
        
        if (us != '')
            us += '/';            
        us += cute_url(tR.val(),sp);
    } 
    
    if (typeof res !== 'undefined') {
        return us;    
    }
    
    if (eR.length > 0) {
        eR.val(us);
    }
}

function permalink_sanitizer() {    
    tP = $('.permalink');
    tV = tP.val();            
    sT = uri_sanitizer(true);
    
    if (tV == '') {
        tP.val(sT); return;        
    }
    
    if (tV.indexOf(sT) != 0) {
        console.log(sT+' vs '+tV);
        tP.val(sT+'/'+tV); return;
    }
        
}

$(document).ready(function(){
    // Language tab    
    selector_lang  = $('.tab-selector.lang');  
    if (selector_lang.length > 0) {
        selector_lang_button = selector_lang.children('ul').children('li'); 
        selector_lang_button.click(function(){
            var selector_lang_selected = $(this);            
            var selector_lang_sel_id   = selector_lang_selected.attr('id');
                        
            $('.media-container.active').removeClass('active');
            
            $('.media-container#media-content-container').addClass('active');  
                      
            selector_lang.children('ul').children('li.active').removeClass('active');  
            
            if (selector_media.length > 0) 
                selector_media.children('ul').children('li.active').removeClass('active'); 
                          
            selector_lang_selected.addClass('active');               
            $('.tab-content.lang.active').removeClass('active').addClass('hide');            
            $('.tab-content.lang.hide.lang-'+selector_lang_sel_id).removeClass('hide').addClass('active');            
            $('.tab-content.lang.lang-'+selector_lang_sel_id+' > *').stop(true,true).fadeOut(300).fadeIn(300);            
            $('form.form-generator .error').removeClass('error');            
            $('.error_information label').hide();
        });         
    } 
    
    // Normal content text
    selector_ctxt  = $('.tab-selector.content-text');  
    if (selector_ctxt.length > 0) {
        selector_ctxt_button = selector_ctxt.children('ul').children('li'); 
        selector_ctxt_button.click(function(){
            var selector_ctxt_selected = $(this);            
            var selector_ctxt_sel_id   = selector_ctxt_selected.attr('id');
                        
            $('.media-container.active').removeClass('active');
            
            $('.media-container#media-content-container').addClass('active');  
                      
            selector_ctxt.children('ul').children('li.active').removeClass('active');  
            
            if (selector_media.length > 0) 
                selector_media.children('ul').children('li.active').removeClass('active'); 
                          
            selector_ctxt_selected.addClass('active');            
            $('form.form-generator .error').removeClass('error');            
            $('.error_information label').hide();
        });         
    }
    
    // Media tab    
    selector_media = $('.tab-selector.media');  
    if (selector_media.length > 0) {
        selector_media_button = selector_media.children('ul').children('li');
        selector_media_button.click(function(){
            active_media    = $(this);
            active_container = active_media.attr('data-name');
            
            $('.media-container.active').removeClass('active');  
                          
            $('.media-container#media-'+active_container+'-container').addClass('active');
                      
            selector_media.children('ul').children('li.active').removeClass('active');  
            
            if (selector_lang.length > 0) 
                selector_lang.children('ul').children('li.active').removeClass('active'); 
            if (selector_ctxt.length > 0)
                selector_ctxt.children('ul').children('li.active').removeClass('active'); 
            
            $('.browseMedia').html('Add '+active_media.html())
            active_media.addClass('active');
        }); 
                
        //Media browsing
        
        $('.browseMedia').click(function(){
            active_media_type = selector_media.children('ul').children('li.active').attr('data-type');
            active_media_name = selector_media.children('ul').children('li.active').attr('data-name'); 
            
            enable_masking_layer('');
            
            $('#masking-html').css('opacity',1).html('Please wait while trying connect to the server, this may take a few seconds'); center_middle('#masking-html');
            
            setTimeout(function(){
                $.ajax({
                    type    : 'POST',
                    url     : bs_cms+"medialibrary/ajax/browser-media",
                    data    : {type:active_media_type, name:active_media_name},
                    async   : false,
                    success : function(response) {
                        enable_masking_layer(false);
                        $('#masking-html').css('opacity',0).addClass('browser-file').append(response);
                        center_middle('#masking-html');
                    }
                }); 
            },240);       
        }); 
    }
    
    // Common tab    
    selector_common  = $('.tab-selector');  
    if (selector_common.length > 0) {
        $.each(selector_common,function(){
            thisk = $(this);
            
            if (!(thisk.hasClass('lang')) && !(thisk.hasClass('media'))) {
                
                thisk_id = thisk.attr('id');
                
                if(typeof thisk_id !== 'undefined') {                    
                    //Hide all tab content
                    $('.tab-content.'+thisk_id).addClass('hide');
                    
                    selector_common_button = thisk.children('ul').children('li'); 
                    
                    if (thisk.children('ul').children('li.active').length <= 0) {
                        selector_common_button.eq(0).addClass('active');        
                    }
                     
                    selected_common_def_id = thisk.children('ul').children('li.active').attr('id');
                    $('.tab-content.'+thisk_id+'.hide#common-'+selected_common_def_id).removeClass('hide').addClass('active');
                    
                    selector_common_button.click(function(){
                        var selector_common_selected = $(this);            
                        var selector_common_sel_id   = selector_common_selected.attr('id');
                         
                        selector_common_selected.siblings('li.active').removeClass('active');            
                        selector_common_selected.addClass('active'); 
                                   
                        $('.tab-content.'+thisk_id+'.active').removeClass('active').addClass('hide');            
                        $('.tab-content.'+thisk_id+'.hide#common-'+selector_common_sel_id).removeClass('hide').addClass('active');            
                        $('.tab-content.'+thisk_id+'#common-'+selector_common_sel_id+' > *').stop(true,true).fadeOut(300).fadeIn(300);            
                    });  
                }
            }        
        });       
    }
    
    // CKEDITOR
    var ck_intro = $('.simple-editor');
    if (ck_intro.length > 0) init_ckeditor('simple');
    
    var ck_intro = $('.simplex-editor');
    if (ck_intro.length > 0) init_ckeditor('simplex');
    
    var ck_body = $('.full-editor');
    if (ck_body.length > 0) init_ckeditor('full');
    
    var ck_admin = $('.mega-editor');
    if (ck_admin.length > 0) init_ckeditor('mega');
    
    // Cancel button
    $('button.cancel-button').click(function(e){
        e.preventDefault();
        data_link = $(this).attr('data-link');
        if (typeof data_link !== 'undefined') {
            window.location.href = data_link;
        } else {
            history.go(-1);    
        }
    });
    
    // Bind submit wrapper click event to form
    $('#submit_wrapper').click(function(){
        $(this).parents('form').attr('id');
        $(this).parents('form').trigger('submit');  
    });
    $('.submit-form').click(function(){
        $(this).parents('form').attr('id');
        $(this).parents('form').trigger('submit');  
    });
    
    // Form element information
    var selector_inf = $('.frm-hint');
    if (selector_inf.length > 0) {
        $('.frm-hint').mouseover(function(e){                                                
            var selector_inf_cur = $(this);
            var inf_text_X = e.pageX+15;//(e.pageX+5);
            var inf_text_Y = e.pageY-10;//e.pageY-$(window).scrollTop();
            var selector_inf_txt = selector_inf_cur.attr('data-hint');
            
            var selector_inf_dir = $('<div class="frm-hint-text"/>');            
            $(selector_inf_dir).text(selector_inf_txt).css({
                //top     : inf_text_Y,
                left    : inf_text_X-40,
                display : 'none'
            });            
            selector_inf_cur.after($(selector_inf_dir)); 
            $(selector_inf_dir).fadeIn(300);
        }).mouseout(function(){
            $('.frm-hint-text').fadeOut(300,function(){
                $('.frm-hint-text').remove();
            })    
        });
    }
    
    if ($('.tailing-url').length > 0) {
        $('.tailing-url').change(function(){ 
            uri_sanitizer();
        }).blur(function(){
            uri_sanitizer();
        });   
    }
    
    if ($('.referrer').length > 0) {
        $('.referrer').keyup(function(){
            uri_sanitizer();
        }).blur(function(){
            uri_sanitizer();
        });
    }
    
    if ($('.permalink').length > 0) {
        $('.permalink').keydown(function(){
            permalink_sanitizer();                        
        }).blur(function(){
            permalink_sanitizer();
        });
    }
    
    if ($('#parent_id').length > 0 && $('#ordering').length > 0) {
        $('#parent_id').change(function(){
            order_challenger($(this));
        });
    }
    
    if ($('.challenge-order').length > 0 && $('#ordering').length > 0) {
        $('.challenge-order').each(function(){
            $(this).change(function(){                
                order_challenger($(this));                
            });
        });
    }
    
    if ($('.datepicker').length > 0) {
        $('.datepicker').each(function(){
            format_date = typeof $(this).attr('data-date-format') == 'undefined' ? 'yy-mm-dd' : $(this).attr('data-date-format');
            $(this).datepicker({
                dateFormat : format_date
            });
        });
    }
    
    if ($('.timepicker').length > 0) {
        $('.timepicker').each(function(){
            format_time = typeof $(this).attr('data-time-format') == 'undefined' ? 'hh:mm' : $(this).attr('data-time-format');
            $(this).timepicker({
                dateFormat : format_time
            });
        });
    }
    
    if ($('.media-container').length > 0) {
        if ($('.isSortable').length > 0) {
            $('.isSortable').sortable({
                connectWith         : ".isSortable",
                scrollSensitivity   : 100,
                cursor              : "move"
            });
        }
        
        $('.unlink')
        .unbind('click')
        .bind('click',function(){
            $(this).parents('.media-item-container').remove();
        }); 
    }
    
    //Filter file
    $('.filter-extension').change(function(){
        elvl = $(this).val(); 
        eldt = $(this).attr('data-type');
        elxt = elvl.substring(elvl.lastIndexOf('.') + 1).toLowerCase();
        
        if (typeof eldt !== 'undefined') {
            inObject = false;
            $.each($.parseJSON(eldt),function(i,key){
                if (elxt === key) {
                    inObject = true;
                    return;
                }
            });
                            
            if (inObject === false) {
                $(this).val('');   
                jAlert('You are selected forbidden file','Warning'); 
            }
                
        }           
    });
    
    $('.numeric').numeric();
});