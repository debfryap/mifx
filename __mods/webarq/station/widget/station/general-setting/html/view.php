<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

echo Data::form(array(
        'title'     => 'General Configuration',
        'action'    => URL::cms().'config/general-setting/update',
        'enctype'   => 'multipart/form-data',
        'type'      => 'update',
        'id'        => 'form_general_setting',
        'manual'    => true,
        //'cancel'    => false,
        'multilang' => false,        
        'elements'  => array (
            //Site configuration        
            array (
                'subtitle'  => 'Application Configuration',
                'label'     => 'status',
                'name'      => 'site[system][offline]',
                'type'      => 'select',
                'option'    => array ('0'=>'Off Line','1'=>'On Line'),
                'value'     => $setting->system->offline === true ? 0 : 1
            ),
            array (
                'label'     => 'environment',
                'name'      => 'site[system][environment]',
                'type'      => User::admin()->id == 1 ? 'select' : 'hidden',
                'option'    => array ('development'=>'Development','production'=>'Production'),
                'value'     => strtolower($setting->system->environment)
            ),
            array (
                'label'     => 'traversing-deep',
                'name'      => 'site[system][traversing_deep]',
                'type'      => User::admin()->id == 1 ? 'text' : 'hidden',
                'value'     => isset($setting->system->traversing_deep) ? $setting->system->traversing_deep : 2,
                'min'       => 0,
                'class'     => 'numeric'
            ),
            array (
                'label'     => 'image-alt',
                'name'      => 'site[system][image_alt]',
                'type'      => User::admin()->id == 1 ? 'text' : 'hidden',
                'value'     => isset($setting->system->image_alt) ? $setting->system->image_alt : 'Alternative Image'
            ),
            array (
                'label'     => 'perpage',
                'name'      => 'site[perpage]',
                'type'      => 'text',
                'class'     => 'required number',
                'value'     => empty($setting->perpage) ? 10 : $setting->perpage,
                'min'       => 10,
                'class'     => 'numeric'
            ),
            array (
                'label'     => 'logo',
                'name'      => 'logo',
                'type'      => 'file',
                'class'     => 'filter-extension',
                'data-type' => json_encode(array('gif','jpeg','jpg','png')),
                'id'        => 'logo-file',
                'preview'   => array (
                    'width'  => isset(App::$config->logo->width) ? App::$config->logo->width : 220,
                    'height' => isset(App::$config->logo->width) ? App::$config->logo->height : 67,
                    'src'    => isset(App::$config->logo->file)  
                                        ? URL::root(App::$config->logo->folder."/".App::$config->logo->file) 
                                        : URL::mediaImage('logo/logo.png')
                )
            ),
            array (
                'name'      => 'site[logo][width]',
                'type'      => 'hidden',
                'value'     => isset(App::$config->logo->width) ? App::$config->logo->width : 359
            ),
            array (
                'name'      => 'site[logo][height]',
                'type'      => 'hidden',
                'value'     => isset(App::$config->logo->width) ? App::$config->logo->height : 206
            ),
            array (
                'label'     => 'Footer Logo',
                'name'      => 'footerLogo',
                'type'      => 'file',
                'class'     => 'filter-extension',
                'data-type' => json_encode(array('gif','jpeg','jpg','png')),
                'id'        => 'footerLogo-file',
                'preview'   => array (
                    'width'  => 75,
                    'height' => 50,
                    'src'    => isset(App::$config->footerLogo->file)  
                                        ? URL::root(App::$config->footerLogo->folder."/".App::$config->footerLogo->file) 
                                        : URL::mediaImage('logo/footerLogo.png')
                )
            ),
            array (
                'label'     => 'Fav Icon',
                'name'      => 'favIcon',
                'type'      => 'file',
                'class'     => 'filter-extension',
                'data-type' => json_encode(array('ico')),
                'id'        => 'favIcon-file',
                'info'      => 'Only ico file.',
                'preview'   => array (
                    'width'  => 32,
                    'height' => 32,
                    'src'    => URL::root('favicon.ico')
                )
            ),
            array (
                'label'     => 'copy-right',
                'name'      => 'site[copyright]',
                'type'      => 'hidden',
                'value'     => empty($setting->copyright) ? (isset(App::$config->copyright) ? App::$config->copyright : ''): $setting->copyright
            ),
            
            //Meta configuration
            array (
                'subtitle'  => 'Meta Configuration',
                'label'     => 'title',
                'name'      => 'site[meta][title]',
                'type'      => 'text',
                'length'    => '200',
                'value'     => $setting->meta->title
            ),
            array (
                'label'     => 'keyword',
                'name'      => 'site[meta][keywords]',
                'type'      => 'text',
                'value'     => $setting->meta->keywords
            ),
            array (
                'label'     => 'description',
                'name'      => 'site[meta][description]',
                'type'      => 'textarea',
                'value'     => $setting->meta->description
            ),       
            
            //User Configuration
            array (
                'subtitle'  => 'User Configuration',
                'label'     => 'Allow Captcha',
                'name'      => 'allow_captcha',
                'type'      => 'hidden',
                'option'    => array (0=>'Off',1=>'On'),
                'value'     => User::$config->captcha === true ? 1 : 0 
            ),
            array (
                'label'     => 'Check IP Login',
                'name'      => 'allow_ip_check',
                'type'      => 'select',
                'option'    => array (0=>'Off',1=>'On'),
                'value'     => User::$config->check_ip === true ? 1 : 0 
            )                                  
        ) 
    ));

?>
