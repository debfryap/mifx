<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Station_GeneralSetting extends Populate {
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
    }
    
    public function data() {
        $general = $this->db
                              ->select('value')
                              ->from('configurations')
                              ->where('key = site')
                              ->execute()
                              ->current();
        if (!empty($general->value)) {
            return array ('setting'=>json_decode($general->value));
        }else {
            return array ('setting'=>App::$config);
        }
    }
}

?>