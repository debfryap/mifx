<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller Widget
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Widget_Station_BreadCrumb extends Populate {
    
    protected $permalink;
    
    protected $page;
    
    public function render() {
        if (empty($this->permalink)) {
            $this->permalink = App::tail_active_uri($this->page)->get('active_uri');
        }
        
        $a     = explode('/',$this->permalink);
        $html  = '<div class="bread-cumb" style="padding-top:80px;">';
        $html .= __('bread-crumb-position'); 
        
        if (isset($this->page) && $this->permalink != $this->page) {
            array_unshift($a,$this->page);
        }
            
        foreach ($a as $i => $item) {
            $h = !isset($h) ? $item : $h."/$item";
            
            $href = !empty(App::$module->language)
                        ? Translate::uri(Language::$current,Language::$current.'/'.$h)
                        : $h;
            
            $html .= HTML::anchor($i < count($a)- 1 ? $href : '#',ucwords(Inflector::decamelize($item,'-')),array(
                        'style'=>'width:auto;height:auto;display:inline-block;padding:5px 8px;text-align:center;background-color:rgb(219, 253, 210);border-radius:8px;-moz-border-radius:8px;-webkit-border-radius:8px;-o-border-radius:8px;margin-right:2px;'));
        }
        $html .= '</div>';
        
        return $html;
    }
    
}