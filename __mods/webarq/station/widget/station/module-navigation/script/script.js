$(document).ready(function(){
    //Shadow navigation margin
    var wg_open_navigation = $('.wg_base_module_navigation ul li.root#open');    
    var wg_st_mn_timer     = 100; 
    
    $('.wg_base_module_navigation ul li.root').hover(
        function(){
            $this = $(this);
            
            if ($this.attr('id') == 'open') return;  
            
            $('.wg_base_module_navigation ul li.root#open').stop().children('ul.child').fadeOut(wg_st_mn_timer)
                             
            $this
                .addClass('hover')
                .children('ul.child')
                .css('z-index',20)
                .fadeIn(wg_st_mn_timer);
        },
        function() {
            $this = $(this);
            
            if ($this.attr('id') == 'open') return;
                        
            $this.removeClass('hover').children('ul.child').stop().fadeOut(wg_st_mn_timer,function(){
                $(this).css('z-index',0);
            });
                
            $('.wg_base_module_navigation ul li.root#open').children('ul.child').stop().fadeIn(wg_st_mn_timer);
        }
    );            
});