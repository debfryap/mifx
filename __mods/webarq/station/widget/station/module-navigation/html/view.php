<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

if (!empty($app_menus[0])) {
    //echo Debug::vars($app_menus); return;
    //Necessary pre-define data
    $module = ''; 
    $open   = $close = false;
    $root_active  = !empty(App::$config->menu->group)  ? App::$config->menu->group  : App::$route_params['package'];
    $child_active = !empty(App::$config->menu->active) ? App::$config->menu->active : strtolower(Request::$initial->controller());
    
    echo '<div class="wg_base_module_navigation" id="'.strtolower($app_menus[0]->group).'">';
    echo '<ul id="list_container">';
    
        
                                                              
    echo '<li class="root" ' . ($root_active == 'dashboard' ? 'id="open"' : '') . '>';
        echo '<a class="dashboard" href="';
        #echo strtolower($menu->label) == $child_active ? '#" ' : URL::cms().$menu->module.'/'.$menu->permalink.'" ';
        echo URL::cms().'dashboard" ';
        echo $root_active == 'dashboard' ? 'id="active" ': '';
        echo '><span>Dashboard</span></a>';

         // add menu conversion
        echo '<ul class="child">';
        echo '<li>';
        echo HTML::anchor(URL::cms().'dashboard', 'History');
        echo '</li>';
                                                  
        echo '<li>';
        echo HTML::anchor(URL::cms().'dashboard/conversion', 'Conversion');
        echo '</li>';
        echo '</ul>';
        
    echo '</li>';
    
    foreach ($app_menus as $menu) {
        if ($module != $menu->module && $open === true && $close === false) {
            echo '</ul></li>';
            $close = true;
            $open  = false;
        }
        
        if ($module != $menu->module && $open === false) {
            $open = true;
            $close = false;
            
            echo '<li class="root" ';
            echo $menu->module == $root_active ? 'id="open" ': '';
            echo '>';
            echo '<a class="'.$menu->module.'" ';
            echo $menu->module == $root_active ? 'id="active" ' : '';
            echo '><span>'.__($menu->module,array('%s'=>'')).'</span></a>';
            echo '<ul class="child">';
        }
        
                                                              
        echo '<li>';
            echo '<a href="';
            #echo strtolower($menu->label) == $child_active ? '#" ' : URL::cms().$menu->module.'/'.$menu->permalink.'" ';
            echo URL::cms().$menu->module.'/'.$menu->permalink.'" ';
            echo strtolower($menu->label) == $child_active ? 'id="active" ': '';
            echo '>';
            echo __($menu->label,array('%s'=>'s'));
            echo '</a>';
        echo '</li>';
            
        $module = $menu->module;
    }
    
    echo '</ul>';
    echo '</div>';
}

?>