<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Station_ModuleNavigation extends Populate {
    protected $group = 1;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
    }
    
    public function data() {
        $superadmin  = User::admin()->role_id === 1 || User::admin()->role_id === '1';
        
        $builder = $this->db
                        ->select('mn.id','mn.label','mn.permalink',array('m.label','module'),array('mng.label','group'))
                        ->from(array('module_navigations','mn'))
                        ->join(array('modules','m'),'LEFT')
                            ->on('mn.module_id = m.id')
                        ->join(array('module_navigation_group_maps','mngm'),'LEFT')
                            ->on('mngm.module_navigation_id = mn.id')
                        ->join(array('module_navigation_groups','mng'),'LEFT')
                                    ->on('mng.id = mngm.module_navigation_group_id');
        
        //Join to permission?
        if (!$superadmin) {                                    
            $builder->join(array('module_navigation_permission_maps','mnpm'),'LEFT')
                        ->on('mnpm.module_navigation_id = mn.id')
                    ->join(array('module_permissions','mp'),'LEFT')
                        ->on('mp.id = mnpm.module_permission_id')
                    ->join(array('module_permission_role_maps','mprm'),'LEFT')
                        ->on('mprm.module_permission_id = mp.id');
        }
        
        $is_publish = $superadmin ? ">= 0" : "= 1";
       
        $builder->where('mngm.module_navigation_group_id = 1')
                ->and_where("mn.is_publish $is_publish")
                ->and_where("m.is_publish >= 0");
       
        //Validate role_id ?
        if (!$superadmin) {
            $builder->and_where('mprm.role_id','=',User::admin()->role_id);
        }
       
        $builder->group_by('mn.label')
                ->order_by('m.ordering','ASC')
               ->order_by('mn.ordering','ASC');
                     
        return array ('app_menus'=>$builder->execute());   
        
    } 
}

?>