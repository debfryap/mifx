<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


return array (

    "configurations" => array (
        array (
            "master" => "id"
        ),
        array (
            "field"     => "key",
            "type"      => "char",
            "primary"   => true
        ),
        array (
            "field"     => "value",
            "type"      => "text"
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    ),
    
    "histories" => array (
        array (
            "field"     => "row_id",
            "type"      => "int"
        ),
        array (
            "field"     => "table",
            "type"      => "varchar",
            "length"    => 100
        ),
        array (
            "field"     => "actor",     //Refer to username,
            "type"      => "varchar",
            "length"    => 100
        ),
        array (
            "master"    => "text",
            "field"     => "description"
        ),
        array (
            "master"    => "create_time"
        )
    ),
    
    "module_navigation_groups" => array (
        array (
            "master"    => "id",
            "type"      => "tinyint"
        ),
        array (
            "master"    => "label",
            "field"     => "label"
        ),
        array (
            "master"    => "is_something",
            "field"     => "is_publish"
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    ),
    
    "modules" => array (
        array (
            "master"    => "id",
            "type"      => "tinyint"
        ),
        array (
            "master"    => "label",
            "field"     => "label",
            "form"      => array (
                "group" => "module"
            )
        ),
        array (
            "master"    => "text",
            "field"     => "description",
            "form"      => array (
                "group" => "module",
                "order" => "after label"
            )
        ),
        array (
            "field"     => "ordering",
            "type"      => "tinyint",
            "form"      => array (
                "group" => "module"
            )
        ),
        array (
            "field"     => "is_system",
            "type"      => "boolean",
            "default"   => 0,
            "form"      => array (
                "group" => "module"                    
            )
        ),
        array (
            "master"    => "is_something",
            "field"     => "is_publish",
            "form"      => array (
                "group"     => "module"
            )
        ),
        array (
            "master"    => "create_time"
        ),
        array (
            "master"    => "update_time"
        )
    ),
    
    "module_navigations"  => array (
        array (
            "master"    => "id"
        ),
        array (
            "master"    => "int",
            "type"      => "tinyint",
            "field"     => "module_id"
        ),
        array (
            "master"    => "label",
            "field"     => "label"
        ),
        array (
            "master"    => "label",
            "field"     => "permalink"
        ),
        array (
            "master"    => "is_something",
            "field"     => "is_publish",
            "default"   => 1
        ),
        array (
            "master"    => "ordering"
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ),
    
    "module_navigation_group_maps" => array (
        array (
            "master"    => "id"
        ),
        array (
            "master"    => "int",
            "field"     => "module_navigation_id"                            
        ),
        array (
            "master"    => "int",
            "field"     => "module_navigation_group_id",
            "type"      => "tinyint"                            
        ),
        array (
            "master"    => "create_time"
        )
    ),
    
    "module_permissions"  => array (
        array (
            "master"    => "id",
        ),
        array (
            "master"    => "id",
            "field"     => "module_id",
            "type"      => "tinyint",
            "increment" => false
        ),
        array (
            "field"     => "permission",
            "type"      => "char",
            "length"    => 50,
            "primary"   => true
        ),
        /**
        array (
            "master"    => "label"
        ),
        **/
        array (
            "master"    => "text",
            "field"     => "description"                            
        ),
        array (
            "master"    => "create_time"
        )
    ),
    
    "module_permission_role_maps" => array (
        array (
            "master"    => "id"
        ),
        array (
            "master"    => "int",
            "field"     => "module_permission_id",
            "primary"   => true                            
        ),
        array (
            "master"    => "int",
            "field"     => "role_id",
            "type"      => "tinyint"                            
        ),
        array (
            "master"    => "create_time"
        )
    ),
    
    "module_navigation_permission_maps"  => array (
        array (
            "master"    => "id"
        ),
        array (
            "master"    => "int",
            "field"     => "module_navigation_id"
        ),
        array (
            "master"    => "int",
            "field"     => "module_permission_id"
        ),
        array (
            "master"    => "create_time"
        )
    ),

     // add conversions
    "conversions" => array (
        array (
            "field"     => "title",
            "type"      => "varchar",
            "length"    => 100
        ),
        array (
            "master"    => "text",
            "field"     => "value"
        ),
        array (
            "type"      => "varchar",
            "length"    => 100,
            "field"     => "ip"
        ),
        array (
            "master"    => "create_time"
        )
    )
);
        
?>