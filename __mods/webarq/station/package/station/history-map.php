<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    History item pointer
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'object' => array (
        "configuration"                     => "key",
        "module_navigation_group_map"       => "module_navigation->label"
    ),
    'item' => array (
        "module_navigation_group_map"       => "module_navigation_group->label"
    ),
    'action' => array (
        "module_navigation_group_map"       => "add"
    )
);