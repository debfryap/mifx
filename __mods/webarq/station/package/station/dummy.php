<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Dummy data
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


return array (
    "module_navigation_groups" => array (
        array (
            "id"            => 1,
            "label"         => "Main",
            "is_publish"    => 1,
            "create_time"   => "now"
        ),
        array (
            "id"            => 2,
            "label"         => "Left",
            "is_publish"    => 1,
            "create_time"   => "now"
        ),
        array (
            "id"            => 3,
            "label"         => "Footer",
            "is_publish"    => 1,
            "create_time"   => "now"
        )
    ),
    "modules" => array (
        array (
            "id"            => 1,
            "label"         => "config",
            "ordering"      => 1,
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "Maintain your application day after day",
            "create_time"   => "now"                              
        ),
        array (
            "id"            => 2,
            "label"         => "templanation",
            "ordering"      => 2,
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "Templating, which handle all of your themes",
            "create_time"   => "now"                              
        )
    ),
    "module_navigations"  => array (
        array (
            "id"            => "auto",
            "module_id"     => 1,
            "label"         => "general-setting",
            "permalink"     => "general-setting",
            "ordering"      => 3,
            "create_time"   => "now"                              
        )
    ),
    "module_navigation_group_maps"  => array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='general-setting' AND `permalink` = 'general-setting' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        )
    ),
    "module_permissions" => array (
        array (
            "id"            => "auto",
            "module_id"     => "1",
            "permission"    => "edit-configuration",
            "create_time"   => "now"                    
        )
    ),
    "module_navigation_permission_maps"  => array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='general-setting' AND `permalink` = 'general-setting' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` ='1' AND `permission` = 'edit-configuration' LIMIT 1",
            "create_time"   => "now" 
        )
    ),
    "module_permission_role_maps" => array (
        array (
            "id"            => "auto",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` ='1' AND `permission` = 'edit-configuration' LIMIT 1",
            "role_id"       => 2,
            "create_time"   => "now"                    
        )
    )
);

?>