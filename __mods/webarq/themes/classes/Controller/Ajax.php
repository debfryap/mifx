<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class Ajax System
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_Ajax extends Controller {
    
    protected $content   = NULL;
    
    protected $must_ajax = true;
    
    protected $controller,$action;
    
    protected $post,$get;
    
    protected $param1,$param2,$param3,$param4;
    
    public function before() {
        
        $this->init();        
        
        parent::before();
                        
        if (Kohana::$environment == Kohana::PRODUCTION && $this->must_ajax === true) {
            if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
                die(__('no_direct_access'));
            }
        }
    }
    
    public function after() {
        $this->response->body($this->content);
        // Run anything that needs to run after this.
        parent::after();        
    }   
    
    private function init() { 
        //Get param language
        I18n::lang($this->request->param('lang'));
        if (!empty(App::$module->language))
            Language::$current = I18n::$lang;
        
        //HTTP request data       
        $post = $this->request->post();         
        if (!empty($post))
            $this->post  = Helper_Kernel::arr_to_object($post,false);
        
        $get  = $this->request->query();
        if (!empty($get))
            $this->get   = Helper_Kernel::arr_to_object($get,false);
        
        //URL request
        $this->controller = $this->request->controller();
        $this->action     = $this->request->action();
        $this->param1 = $this->request->param('param1');
        $this->param2 = $this->request->param('param2');
        $this->param3 = $this->request->param('param3');
        $this->param4 = $this->request->param('param4');
    }
}