<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class HTML Space
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_Default_Template_Site extends Controller_System {
    
    //Active URL param package
    protected $package;
    
    public function before() {
        parent::before(__CLASS__);    
    
        //Cookie application check
        $app_coookie = App::cookie('webarqsite',str_replace('/','',Kohana::$base_url));
        
        //Styles
        $this->styles(URL::templateCss('flexslider','accordion','colorbox','bootstrap','bootstrap-responsive'));
        
        //Scripts
        $this->jquery = '1.7.2';
        $this->scripts(URL::templateJs('jquery.colorbox','jquery.flexslider','bootstrap','bootstrap-collapse','jquery.jcarousellite'));
    }
    
    public function after() {
        if (App::$config->system->offline) {
            //  
            $this->template = 'offline';
            
            parent::before();
        }
        
        parent::after();
    }

}