<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class HTML Space
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_System extends Controller_Template {
    
    /**
     * @var   string        template name
     */
    public    $template;
    
    /**
     * @var   string        template name
     */
    private   $_template;
    
    /**
     * @var   string        template folder
     */
    private   $template_folder = 'template';
    
    /**
     * @var   array         template variables holder
     */
    private   $template_vars = array();
    
    /**
     * @var   string        template variable current active key
     */
    private   $_temp_var = '';
    
    /**
     * @var   int           error leveling
     */
    private   $error_level = 0;
    
    /**
     * @var   array         template css files
     */
    private   $_styles = array();
    
    /**
     * @var   array         template scripts files
     */
    private   $_scripts = array();
    
    /**
     */
    private   $media_ui_custom = false;
    
    /**
     * @var   object        template variable collection
     */
    protected $site;
    
    /**
     * @var   array         browser data request post
     */
    protected $post;
    
    /**
     * @var   arrray        browser data request get                 
     */
    protected $get;
    
    /**
     * @var   string        kohana controller name
     */
    protected $controller;
    
    /**
     * @var   string        kohana action name
     */
    protected $action;
    
    /**
     * @var   bool          using css reset
     * @value string        css file name
     */
    protected $css_reset = 'reset';
    
    /**
     * @var   bool          using jquery
     * @value string        jquery file name
     */
    protected $jquery = '1.8.0';
    
    /**
     * @var   bool          active browser checking for both of css and javascript
     */
    protected $browser_function = true;
    
    protected $themes;
    
              
    public function before() {
        //Opening data
        $this->init();
        
        //Default template name 
        $this->_template = 'system';
        
        //Check for themes
        if (!isset($this->themes)) {
            $this->themes = "default";
        }
        
        //Check for template
        $param = func_get_args();
        if (!isset($this->template)) {
            $this->template = !empty($param) ? strtolower(Helper_File::type($param[0],'_')) : 'system';
        }
        
        Themes::instance(array(
            'name'      => $this->themes,
            'template'  => $this->template));
        
        //Here we check template overriding
        if (isset($this->template)) {            
            /**
             * Check file html on active themes
             * Please note, beside from config file, you could override this themes from each of your controller right in before function.
             * Still you need to remember calling again parent::before right after it, so system will calculate and use it
             * More info see your manual
             * 
             * $this->themes = 'something';
             * 
             */
            $in_template = App::check_file('views/'.$this->themes.'/'.$this->template_folder,$this->template,'php');
            
            $this->_template = $this->template;
            
            if ($in_template !== false) {
                $this->template = $this->themes.'/'.$this->template_folder.'/'.$this->template; //Make template in path mode
            } else {
                $in_system =  App::check_file('views/__system/'.$this->template_folder,$this->template,'php');
                if ($in_system !== false) {
                    $this->template = '__system/'.$this->template_folder.'/'.$this->template; //Make template in path mode
                } else {                    
                    $this->template = '__system/'.$this->template_folder.'/system'; //Will use system template
                    $this->error_level = 1;    
                }
            }
        }
        
        //Confirm with parent
        parent::before();
        
        $returnLang = false;
        $langActived = Language::$active;
        
        foreach($langActived as $item){ 
            if($item->code == Language::$current) $returnLang = true; 
        }
        if(!$returnLang){ 
            $this->redirect('errors/404', 301); 
            break; 
        }
        //end here

        //Convert site to template
        $this->convert();
    }
    
    public function after() {
        
        $this->collect_js_data();
        
        //Merge scripts
        $this->site->scripts = array_merge($this->site->scripts,$this->_scripts);
                
        $this->collect_css_data(); 
        
        //Merge styles
        $this->site->styles = array_merge($this->site->styles,$this->_styles);
        
        //Rendering
        $this->render();
        
        //Confirm with parent
        parent::after();
        
        //Destroy un-necessary data
        Kernel::destroy();
        
    }
    
    /**
        ===========================================================================================================================
        === Templating function section ===========================================================================================
        ===========================================================================================================================
    **/
    
    private function init() { 
        //Get param language
        $paramLang = $this->request->param('lang'); 
        
        if (!empty(App::$module->language)) {
            I18n::lang( empty($paramLang) ? Language::$default : $paramLang );
            Language::$current = I18n::$lang;
        }
        
        //HTTP request data       
        $post = $this->request->post();         
        if (!empty($post))
            $this->post  = Helper_Kernel::arr_to_object($post,false);
        
        $get  = $this->request->query();
        if (!empty($get))
            $this->get   = Helper_Kernel::arr_to_object($get,false);
        
        //URL request
        $this->controller = $this->request->controller();
        $this->action     = $this->request->action();
        $this->param1 = $this->request->param('param1');
        $this->param2 = $this->request->param('param2');
        $this->param3 = $this->request->param('param3');
        $this->param4 = $this->request->param('param4');
        
        $this->action = $this->request->action();
        
        $this->site   = json_decode('{
                            "menu"       : {"active":null,"group":null},
                            "search_key" : null,
                            "content"    : null,
                            "styles"     : [],
                            "scripts"    : []}'
                        );
    }
    
    private function convert() {
        //App configuration menu
        App::$config->menu = json_decode('{"active":null,"group":null}');
                                 
        //Overwrite template/space site
        $this->template->site = $this->site;
    } 
    
    private function collect_js_data() {
        //Using JQUERY
        if (!empty($this->jquery)) {
            $this->site->scripts[] = URL::libJs('jquery/core/'.$this->jquery);
        }
        
        if ($this->browser_function === true) {
            //Browser Javascript
            $js_file = strtolower(Request::user_agent('browser')); 
            $in_template  = App::check_file('../assets/'.$this->themes.'/'.$this->_template.'/js',$js_file,'js');
            $js_path      = null;
            if ($in_template !== false) {
                $js_path = URL::templateJs($js_file);
            } else {
                $in_default = App::check_file('../assets/'.$this->themes.'/default/js',$js_file,'js');            
                if ($in_default !== false) {
                    $js_path = URL::assets($js_file,URL::themes() . '/default/js');
                } else {
                    $in_lib = App::check_file('../assets/__system/lib/js',$js_file,'js');
                    if ($in_lib !== false) {
                        $js_path = URL::libJs($js_file);
                    } else {
                        $in_system = App::check_file('../assets/__system/index/js',$js_file,'js');
                        if ($in_system !== false) {
                            $js_path = URL::sysJs($js_file);
                        }
                    }
                }      
            }
            
            if ($js_path !== null) {
                $this->site->scripts[] = $js_path;
            }
        }
        
        //Template JS
        $in_template  = App::check_file('../assets/'.$this->themes.'/'.$this->_template.'/js','main','js');
        if ($in_template !== false) {
            $this->site->scripts[] = URL::templateJs('main');
        }
    }
    
    private function collect_css_data() {
        //Using CSS reset
        if (!empty($this->css_reset)) {
            $css_file     = $this->css_reset; 
            $in_template  = App::check_file('../assets/'.$this->themes.'/'.$this->_template.'/css',$css_file,'css');
            $css_path     = null;
            if ($in_template !== false) {
                $css_path = URL::templateCss($css_file);
            } else {
                $in_default = App::check_file('../assets/'.$this->themes.'/default/css',$css_file,'css');            
                if ($in_default !== false) {
                    $css_path = URL::assets($css_file,URL::themes() . '/default/css');
                } else {
                    $in_lib = App::check_file('../assets/__system/lib/css',$css_file,'css');
                    if ($in_lib !== false) {
                        $css_path = URL::libCss($css_file);
                    } else {
                        $in_system = App::check_file('../assets/__system/index/css',$css_file,'css');
                        if ($in_system !== false) {
                            $css_path = URL::sysCss($css_file);
                        }
                    }
                }    
            }
        
            if ($css_path !== null) {
                $this->site->styles[$css_path] = '';
            }
        }
         
        if ($this->browser_function === true) {
            //Browser CSS
            $css_file     = strtolower(Request::user_agent('browser')); 
            $in_template  = App::check_file('../assets/'.$this->themes.'/'.$this->_template.'/css',$css_file,'css');
            $css_path     = null;
            if ($in_template !== false) {
                $css_path = URL::templateCss($css_file);
            } else {
                $in_default = App::check_file('../assets/'.$this->themes.'/default/css',$css_file,'css');            
                if ($in_default !== false) {
                    $css_path = URL::assets($css_file,URL::themes() . '/default/css');
                } else {
                    $in_lib = App::check_file('../assets/__system/lib/css',$css_file,'css');
                    if ($in_lib !== false) {
                        $css_path = URL::libCss($css_file);
                    } else {
                        $in_system = App::check_file('../assets/__system/index/css',$css_file,'css');
                        if ($in_system !== false) {
                            $css_path = URL::sysCss($css_file);
                        }
                    }
                }    
            }
        
            if ($css_path !== null) {
                $this->site->styles[$css_path] = '';
            }
        }
        
        //Template CSS    
        $in_template  = App::check_file('../assets/'.$this->themes.'/'.$this->_template.'/css','main','css');
        if ($in_template !== false) {
            $this->site->styles[URL::templateCss('main')] = '';
        }
    }
    
    /**
     * Collect page css style files
     * @param   mixed   string css file name or array ($media_name=>$media_type)
     * @param   mixed   it can be everything you want to ;)
     * @return  object  
    */
    protected function styles($media_name,$media_type = 'screen,projection') {
        if (is_array($media_name)) {
            foreach ($media_name as $new_media_name => $media_type) {
                $is_int         = is_int($new_media_name);
                $new_media_name = $is_int ? $media_type : $new_media_name;
                $media_type     = $is_int ? 'screen,projection' : $media_type;
                $this->_styles[$new_media_name] = $media_type;
            }
        } else {
            $this->_styles[$media_name] = $media_type;
        }
        
        return $this;
    }
    
    /**
     * Collect page javascript files
     * @param   mixed   string css file name or array ($media_name)
     * @param   mixed   it can be everything you want to ;)
     * @return  object  
    */
    protected function scripts($media_name) {
        if (!is_array($media_name)) $media_name = func_get_args();
        foreach ($media_name as $new_media_name) 
            $this->_scripts[] = $new_media_name; 
        
        return $this;
    }
    
    /**
     * Collect html meta element 
     * @param   mixed   string meta name or array ($key=>$value)
     * @param   mixed   it can be everything you want to ;)
     * @return  object  
    */
    protected function meta($key,$value = null) {
        if (is_array($key)) {
            foreach ($key as $new_key=>$value) {
                App::$config->meta->$key = $value;
            }
        } else {
            App::$config->meta->$key = $value;
        } 
        
        return $this;
    }
    
    /**
        ===========================================================================================================================
        === Body filler function section ==========================================================================================
        ===========================================================================================================================
    **/
    
    protected function load($view = null,  array $vars = array()) {
        return empty($view) ? null : View::factory($view,$vars);
    }
    
    protected function render() {
        
        if (!empty($this->template_vars)) {
            foreach ($this->template_vars as $key => $value) {
                $this->site->$key = $value;
            }
        }
    
        if ($this->error_level == 1) {
            
            $this->site->styles  = array();
            
            $this->site->scripts = array();
            
            App::$config->meta->title = 'Error. Template nout found';
            
            $this->site->content = View::factory('__system/general/error',array(
                    'message' => "Even we could not be able to locate where is this very <span class='themes' style='font-size:130%;'>".
                                  $this->themes."/</span><span class='file' style='font-size:110%;'>".$this->_template."</span> html file hide."
                ));
        }
    }
    
    /**
     * Register essential very variable to template
     * @param   string      variable name
     * @param   string      variable value
    */
    protected function register($name,$value=null) {
        $this->_temp_var = $name;
        
        if (isset($value)) $this->template_vars[$this->_temp_var] = $value;
        
        return $this;
    }
    
    //Give a string value to previous registered template variable    
    protected function use_string($string='') {
        $this->template_vars[$this->_temp_var] = $string;
        return $this;
    }
    
    //Use a view as value of the previous registered template variable
    protected function use_view($path=null,$absolute=true) {
        
        $view_path = $absolute === true ? $this->themes.'/'.$path : $path;
        
        $this->template_vars[$this->_temp_var] = View::factory($view_path);
        
        return $this;
    }
    
    //Use a widget as value of the previous registered template variable
    protected function use_widget($path,array $data = null) {
        $this->template_vars[$this->_temp_var] = Widget::load($path); 
        if (!empty($data)) {
            $this->data($data);
        }
        return $this;
    }
    
     /**
     * @param   string      variable name
     * @param   string      variable value
     */
    protected function set($key,$value = null) {
        if (!empty($this->_temp_var)) {
            if (is_array($key)) {
                foreach ($key as $new_key => $value) {
                    $this->template_vars[$this->_temp_var]->set($new_key,$value);    
                }
            } else {
                $this->template_vars[$this->_temp_var]->set($key,$value);
            }
        }
                    
        return $this;
    }
    
     /**
     * @param   string      variable name
     * @param   string      variable value
     */
    protected function data($key,$value = null) {
        if (!empty($this->_temp_var)) {
            if (is_array($key)) {
                foreach ($key as $new_key => $value) {
                    $this->template_vars[$this->_temp_var]->data($new_key,$value);    
                }
            } else {
                $this->template_vars[$this->_temp_var]->data($key,$value);
            }
        }
                    
        return $this;
    }
    
    protected function jquery_ui_custom() {
        if ($this->media_ui_custom) return;        
        $this->media_ui_custom = true;     
        $this->scripts(URL::libJs('jquery/ui/jquery-ui.custom.min'));
        $this->styles(URL::libCss('jquery/themes/jquery.ui.all.css'));
    }
    
    protected function media_header() {
        
        //Append uploadify js
        $this->jquery_ui_custom();
        $this->scripts(URL::libJs(
                'jquery/ui/jquery.ui.sortable.min'
                ,'uploadify-3.2/jquery.uploadify.min.js'
                ,'jquery/plugin/jquery.contextMenu.js'
                ,'jquery/plugin/jquery.alphanumeric.js'
            ));
        
        //Append uploadify css
        $this->styles(URL::libJs('uploadify-3.2/uploadify'));
        $this->styles(URL::libCss('liquid-two-column'));
    }
    
    protected function media_uploadify() {     
        //Append js
        $this->scripts(URL::libJs('uploadify-3.2/jquery.uploadify.min.js'));
        
        //Append css
        $this->styles(URL::libJs('uploadify-3.2/uploadify.css'));
    }
    
    protected function media_sortable() {        
        //Append sortable jquery
        $this->jquery_ui_custom();
        $this->scripts(URL::libJs('jquery/ui/jquery.ui.sortable.min'));
    }
    
    protected function media_validate($alert = false) {
        $this->scripts(URL::libJs(
                                'jquery/plugin/jquery.validate'
                                ,'jquery/plugin/jquery.validate.additional'
                                ,'jquery/plugin/jquery.alphanumeric.js'));
        
        if ($alert === true) {
            $this->scripts(
                            URL::libJs('jquery/plugin/jquery.alert' 
                                , 'jquery/plugin/jquery.bpopup'
                                , 'function/function.js'));
            $this->styles(URL::libCss('jquery.alert','function'));
        }                                   
    }
    
    protected function media_ckeditor() {
        $this->scripts(URL::libJs('ckeditor/ckeditor','ckeditor/adapters/jquery'));
    }
    
    protected function media_tabular() {
        $this->styles(URL::libCss('tabular-css'));
    }
    
    protected function media_tree_tabular() {
        //Append style
        $this->styles(URL::libCss('tabular-css','jquery/tree-table/jquery.treetable.theme.default'));
            
        //Append script
        $this->scripts(URL::libJs('jquery/plugin/jquery.treetable'));
    }
    
    protected function media_datetimepicker() {      
        //Append sortable jquery        
        $this->jquery_ui_custom();
        $this->scripts(URL::libJs('jquery/ui/jquery.ui.datepicker.min','jquery/ui/jquery.ui.timepicker'));
    }
}