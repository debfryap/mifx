<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class HTML Space
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_Webarq_Template_Cms extends Controller_System {
    
    private $_verified = true;
        
    public function before() {
        parent::before(__CLASS__);       
    
        //Cookie application check
        $app_coookie = App::cookie('webarqcms');
        
        //User administrative
        User::current($app_coookie);
        
        if ((empty(User::admin()->is_login) || empty(User::admin()->is_admin)) && ($this->request->controller() != 'auth' && $this->request->controller() != 'Auth')) {
            $this->deflect('user/auth/login');
        }
        
        //Role permission
        User::role_permission();
        
        //Default jquery scripts  
        $this->scripts(URL::libJs(
                                'jquery/plugin/jquery.alert'
                                ,'jquery/plugin/jquery.cookie'
                                ,'function/function'));
        $this->scripts(URL::templateJs('jquery/factory/cms-scripting'));
        
        //Default jquery css
        $this->styles(URL::libCss('jquery.alert','function'));
    }
    
    public function after() {
        
        if ($this->_verified === false) {
            $this
                 ->register('title')->use_string(__('error.401'))
                 ->register('content')->use_view('error/401',false)
                 ;
        }
        
        parent::after();
    }
    
    protected function authorise($module,$permission=null) {
        $this->_verified = User::authorise($module,$permission);
        return $this;  
    }
    
    protected function deflect($permalink='',$code=302) { 
        $this->redirect(CMSPAGE.'/'.$permalink,$code);
    }

}