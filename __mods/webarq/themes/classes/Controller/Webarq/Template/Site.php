<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class HTML Space
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_Webarq_Template_Site extends Controller_System {
    
    public function before() {
        parent::before(__CLASS__);    
    
        //Cookie application check
        $app_coookie = App::cookie('webarqsite',str_replace('/','',Kohana::$base_url));
    }
    
    public function after() {
        
        if (App::$config->system->offline) {
            //  
            $this->template = 'offline';
            
            parent::before();
        }
        parent::after();
    }

}