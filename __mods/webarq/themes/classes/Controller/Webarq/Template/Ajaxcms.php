<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Class Ajax Cms
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Controller_Webarq_Template_Ajaxcms extends Controller_Ajax {
    
    public function before() {
        parent::before();
        
        User::current(App::cookie('webarqcms'));
        
        if ((empty(User::admin()->is_login) 
            || empty(User::admin()->is_admin)) 
            && ($this->request->controller() != 'auth' 
            && $this->request->controller() != 'Auth')) {
            die(__('error.403'));
        }
        
        //Role permission
        User::role_permission();
    }
}