<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Nouspeed
 * @Copyright 	2012
 * @Package	    Theme Api
 * @Module      Theme      
 * @License		Kohana ~ Nouspeed ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Themes {
    
    public static $instance;
    
    public $name;
    
    public $template;
    
    public static function instance(array $configs) {
        if (empty(self::$instance))
            self::$instance = new Themes; 
            
        if (!empty($configs))
            foreach ($configs as $k => $v)
                self::$instance->$k = $v;
    }
    
    public static function logo($type='logo') {

        if($type=='logo'){
            $width = 208;
            $height = 105;
        }else{
            $width = 99;
            $height = 50;
        }

        return HTML::anchor(
                //URL
                URL::front(),
                //Text
                HTML::image( URL::base(true) . App::$config->$type->folder . '/' .App::$config->$type->file, array('alt' => Contact::$config->office->name, 'title' => Contact::$config->office->name, 'width'=> $width, 'height'=> $height ) )
                //Attributes
                //,array('class'=>'logo','style'=>'background-image:none;')
            );
    }
}

?>