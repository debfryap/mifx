<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    
 * @Module      
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class URL extends Kohana_URL {
    
    protected static $admin_base = 'admin-cp';
    
    public static function cms($url) {
        return URL::site(self::$admin_base . '/' . $url,TRUE,TRUE);    
    }
    
}


?>