<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Space Html View
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
if ((int)(App::$config->version->html) <= 4) {
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
}elseif ((int)($site->html_version)  == 5) {
    echo '<!DOCTYPE html>'."\n";
}

?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<?php
        foreach (App::$config->meta as $name=>$value) {
            if ($name != 'title') {
                echo '<meta name="'.$name.'" content="'.$value.'"/>',"\r\n";
            }
        } 
        
        if (!empty($site->styles)) {
            #echo Debug::vars($site->styles);
            foreach($site->styles as $cpath => $ctype) {
                $ctype  = empty($ctype) ? 'screen,projection' : $ctype;  
                $file = Helper_File::type($cpath) == 'css' ? $cpath : $cpath.'.css';
                echo HTML::style($file, array('media' => $ctype)), "\n"; 
            }
        }
        
        echo "<script>
                    var bs_path = '",Kohana::$base_url,"';
                    var bs_root = '",URL::root(),"';
                    var bs_site = '",URL::front(),"';
                    var bs_cms  = '",URL::cms(),"';
        </script>","\r\n";
        
        if (!empty($site->scripts)) {
            foreach($site->scripts as $file) { 
                $file = Helper_File::type($file) == 'js' ? $file : $file.'.js';
                echo HTML::script($file, NULL, TRUE), "\n"; 
            } 
        }
    ?>
    
	<title><?php echo App::$config->meta->title;?></title>
</head>

<body>
    <div id="body-wrapper">
            
        <?php echo !empty($site->header) ? '<div id="wrapper-header">'.$site->header.'</div>' : '';?>
        
        <?php echo !empty($site->content) ? '<div id="wrapper-content">'.$site->content.'</div>' : '';?>
        
        <?php echo !empty($site->footer) ? '<div id="wrapper-footer">'.$site->footer.'</div>' : '';?>
        
    </div>
</body>
</html>