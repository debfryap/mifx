<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    HTML Error
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="" />

	<title>Offline mode</title>
</head>

<body>    
    <div 
        style="border:3px solid #ccc;padding:20px;width:300px;margin:30px auto;text-align:justify;letter-spacing:1.2px;color:#000;font-family:arial;font-size: 14px;" 
        class="system-error">
        <div style="height:10px;line-height:10px;"></div>
        <div style="opacity: .8;color:#333;font-size:11px;">
            We are in maintenance mode for more great service
        </div>
        <div style="height:10px;line-height:10px;"></div>
        See ya ...
    </div>
</body>
</html>