<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Space Html View
 * @Module      Templanation
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
if ((int)(App::$config->version->html) <= 4) {
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
}elseif ((int)($site->html_version)  == 5) {
    echo '<!DOCTYPE html>'."\n";
}

?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<?php
        foreach (App::$config->meta as $name=>$value) {
            if ($name != 'title') {
                echo '<meta name="'.$name.'" content="'.$value.'"/>',"\r\n";
            }
        } 
        
        if (!empty($site->styles)) {
            #echo Debug::vars($site->styles);
            foreach($site->styles as $cpath => $ctype) {
                $ctype  = empty($ctype) ? 'screen,projection' : $ctype;  
                $file = Helper_File::type($cpath) == 'css' ? $cpath : $cpath.'.css';
                echo HTML::style($file, array('media' => $ctype)), "\n"; 
            }
        }
        
        if (!empty($site->static_styles)) echo $site->static_styles;
        
        echo "<script>
                    var bs_path    = '",Kohana::$base_url,"';
                    var bs_root    = '",URL::root(),"';
                    var bs_site    = '",URL::front(),"';
                    var bs_cms     = '",URL::cms(),"';
                    var cms_cookie = '" . App::cookie_name('webarqcms') ."';
        </script>","\r\n";
        if (!empty(App::$module->language)) {
            echo "<script>var sys_lang = '",Language::$system,"';</script>","\r\n";
        }
        if (!empty($site->scripts)) {
            foreach($site->scripts as $file) { 
                $file = Helper_File::type($file) == 'js' ? $file : $file.'.js';
                echo HTML::script($file, NULL, TRUE), "\n"; 
            } 
        }
        
        if (!empty($site->static_scripts)) echo $site->static_scripts;
    ?>
    
	<title><?php echo App::$config->meta->title;?></title>
    
</head>

<body>
    <div id="wrapper">
        <?php if (!empty(User::admin()->is_admin)) {  ?>
            <div id="app_header">
                <div id="icon">WEBARQ Content Management System 2.1.0</div>
                <div id="welcome-message">
                    <div class="fl" id="message"><?php echo __('msg_welcome_user',array(':username'=>HTML::anchor(URL::cms().'user/profile',User::admin()->full_name))); ?></div>
                    <div class="fl" id="logout"><?php echo HTML::anchor(URL::cms().'user/auth/logout','');?></div>
                </div>
                <div id="inbox" class="hidden"></div>
                <div id="notification" class="hidden"></div>
                <div class="clear break1"></div>
            </div>
            
            <div id="app_shorcut">
                <div>
                    <div class="fl" style="margin:10px 0 0 30px;width: 230px;max-height: 120px;"><img src="<?php echo URL::templateImage('general/logo-client.png');?>" style="width: 145px;"/></div>    
                    <div class="clear break1"></div>
                </div>
            </div>
            
            <div id="app_navigation"><?php echo Widget::load('station/module-navigation',array('model'=>'dba','group'=>1)); ?></div>
            <div style="height: 21px;background-color:#fff;width:100%">&nbsp;</div>
            <div id="app_header_shadowing"></div>
            
            <div id="app_content">
                <div id="content_header"><h3<?php echo !empty(App::$route_params['package']) ? ' class="'.App::$route_params['package'].'"' : '';?>><?php echo isset($site->title) ? $site->title : __('belum-ada-judul'); ?></h3></div>
                <div id="content_body"><?php echo isset($site->content) ? $site->content : __('belum-ada-content'); ?></div>
                
            </div>
            
            <div id="app_footer">
                <div class="logo">Copyright &COPY; 2012 WEBARQ</div>
                <div class="clear"></div>
            </div>
        <?php } else { echo !empty($site->content) ? $site->content : __('belum-ada-content'); } ?>
    </div>
</body>
</html>