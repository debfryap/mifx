<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    View Layout 
 * @Module      Product
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

echo '<div class="two-column left">';
    if (isset($left)) echo $left; 
echo '</div>';

echo '<div class="two-column right">';
    if (isset($right)) echo $right;
echo '</div>';
        
?>