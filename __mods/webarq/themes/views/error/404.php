<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Error 404 page
 * @Module      Station
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<div class="error page-404">
    <h3 class="title"><?php echo __('error.404');?></h3>
    <div class="message">
        <p>
            We have searching 404 times, but still could not find anything and showing it in your monitor.<br/>
            Seems like the page you are looking for has been moved, or it was never be presented here.
        </p>
        <p>
            Please use the navigation at the top of this page, and hope you could find what you are looking for!
        </p>
    </div>
</div>
