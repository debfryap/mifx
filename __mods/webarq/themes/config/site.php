<?php defined('SYSPATH') or die('No direct script access.');


$add_slash = empty(Kohana::$index_file) ? '' : Kohana::$index_file.'/';
$full_url_address = 'http://'.$_SERVER['SERVER_NAME'].Kohana::$base_url;

return array  (
    "site" => array(
        "meta"              => array(
            //"author"        => "WEBARQ ~ Daniel Simangunsong",
            "title"         => "WEBARQ ~ CMS",
            "keywords"      => "WCMS Version 2.0.10",
            "description"   => "WCMS Version 2.0.10 Human Develop",
            "copyright"     => "&copy; WEBARQ",
            "creator"       => "WEBARQ ~ Team"
        ),
        "version"           => array("html" => 4, "php" => 5, "css" => 2), 
        "system"            => array("environment"=>"development","offline"=>false,"traversing_deep"=>2,"image_alt"=>"Alternative Image"),
        "salt"              => array("name" => "nouspeed" ,"key"=>"melon", "value"=> "green", "encrypted" => null),
        "cookie"            => array("cms" => null,"site" => null),
        "logo"              => array (
            "width"         => 165,
            "height"        => 105,
            "folder"        => "media/images/logo",
            "file"          => "logo.png"   
        ),
        "copyright"         => "@copyright"
    )
);
            