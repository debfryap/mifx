<?php defined('SYSPATH') or die('No direct script access.');

//Special user move
Route::set('user-token', 'user/token/(<action>)',array('action'=>'request-new-password|update-password'))
	->defaults(array(
        'lang'       => 'id',
        'package'    => 'user',
		'controller' => 'token',
        'directory'  => 'helper',
		'action'     => 'index',
	));