<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form supporting class
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Kohana_Userform {  
    
    public static function select_user_role_id($attribute) {
        $roles   = Model::factory('Role')->lower_role();
        $options = array(''=>__('select-one'));
        if (!empty($roles)) {
            foreach ($roles as $role) {
                $options[$role->id] = $role->label;
             }
        }
        $attrs   = isset($attribute->attributes) ? $attribute->attributes : array();
        $select  = isset($attribute->value) ? $attribute->value : '';
        return Form::select($attribute->name,$options,$select,$attrs);
    }
}

?>