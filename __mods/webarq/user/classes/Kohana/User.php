<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Handler Class
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Kohana_User extends Populate {
    //(object)  of current logged in user profile
    protected static $_current;
    
    //(boolean) enable check ip login
    protected $check_ip;
    
    //(boolean) enable captcha login
    protected $captcha;
    
    //(string)  hash algorithm method
    protected $hash;
    
    //(boolean) login using email permit
    protected $loginbyemail;
    
    //(string)  user id name
    protected $username;
    
    //(string)  hashing user password
    protected $password;
    
    /**
     * (object)  current user cookie
     *       1. cookie->root    : Root Application
     *       2. cookie->app     : cms or site
     *       3. cookie->salt    : Salt Application
     *       4. cookie->session : Current active session. User module will rewrite "App::$session->id" using this value. 
     */
    protected $cookie;    
    
    //(int)     user id
    protected $id; 
    
    //(string)  main handler method
    protected $handler;
    
    //(object)  module user configuration
    public static $config;
    
    //(array)   kohana user class configuration
    protected $_config = array (
                'check_ip'      => false,
                'captcha'       => false,
                'hash'          => 'ripemd160',
                'loginbyemail'  => true,
                'images'        => array (
                    "small"     => array (
                        'path'  => 'users/avatars/50x61/',  
                        'size'  => array(50,61)
                    ),
                    "medium"    => array (
                        'path'  => 'users/avatars/120x145/',
                        'size'  => array(120,145)
                    )
                )
            );  
    
    //(object)  database user model
    protected $db;
    
    //(object)  class instances
    protected static $_instance;
    
    public static function install() {
        if (!empty(User::$config->images)) {
            foreach (User::$config->images as $desc) {
                if (isset($desc->path))
                    Helper_File::create_folder($desc->path,URL::mediaImage(true));
            }    
        }    
    }
    
    //
    public static function admin() {
        return self::$_current;
    }
    
    //(string)  application cookie name 
    public static function current($cookie) {
        $user = new User(array('cookie' => $cookie,'handler' => 'current'));                
        return;                
    }
    
    //(string)  user name and (string) password    
    public static function login($user_name,$password) {
        return new User(array(
                'username' => $user_name,
                'password' => $password,
                'handler'  => 'login'
            ));                 
    }
    
    public static function logout($cookie) {
        return new User(array('cookie' => $cookie, 'handler' => 'logout'));
    }
    
    //(int)     user id
    public static function profile($id_user) {
        $user = new User( array('id'=> $id_user) );
        return $user->profile_handler();
    }
    
    //(array)   extend user configuration
    public static function factory(array $configs = array()) {
        return new User($configs);                 
    }
    
    public static function instance(array $configs = array()) {
        if (empty(self::$_instance))
            self::$_instance = new User($configs);
        
        return self::$_instance;
    }
    
    public static function make_password($string_password) {
        $user = new User();
        return $user->hashing_password($string_password);
    }
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        //Database model
        $this->db = Model::factory('User');
        
        //Cookie handler
        $this->override_config_session();
        
        //Calling handler
        $handler  = empty($this->handler) ? 'main' : $this->handler; $handler .= '_handler';
        $this->$handler();
    }
    
    protected function override_config_session() {    
        if (!empty($this->cookie)) {
            list($root,$app,$salt,$session) = explode('::',$this->cookie,4);
                        
            $this->cookie = new stdClass();        
            $this->cookie->root = $root;
            $this->cookie->app  = $app;
            $this->cookie->salt = $salt;
            $this->cookie->session = App::$session->id = $session;
        }
    }
    
    protected function hashing_password($string) {
        return hash($this->hash,$string); 
    }
    
    
    protected function main_handler() {
        return 'Main handler class user';    
    }
    
    protected function current_handler() {
        if (!empty($this->cookie)) {       
            if (App::$config->salt->encrypted == $this->cookie->salt) {
                $user = $this->db->session_scan(App::$session->id);
                $this->user_profiling($user);
            }
        }
    }
    
    protected function login_handler() {
        $password = $this->hashing_password($this->password);
        $user     =  $this->db->login_scan($this->username,$password);        
        if (!empty($user)) {
            $this->user_profiling($user);
            $this->db->session_register($user->id);
        }
    }
    
    protected function logout_handler() {
        if (!empty(self::$_current->id)){
            return $this->db->session_logout(self::$_current->id,App::$session->id);
        }
    }
    
    protected function profile_handler() {
        $user = $this->db->id_scan($this->id);
        if (!empty($user)) {            
            $user->full_name = $user->first_name
                                        .(empty($user->middle_name) ? '' : " ".$user->middle_name)
                                        .(empty($user->last_name) ? '' : " ".$user->last_name);
        }
        
        return $user;
    } 
    
    //(object) single user profile
    protected function user_profiling($user) {
        if (empty($user)) {
            self::$_current = new stdClass();
            self::$_current->is_login = false;
        }else{
            self::$_current = $user;
            self::$_current->full_name = $user->first_name
                                        .(empty($user->middle_name) ? '' : " ".$user->middle_name)
                                        .(empty($user->last_name) ? '' : " ".$user->last_name);
            self::$_current->is_login = true; 
        }
    }
    
    //Get current user activities
    public static function activities($user=NULL,$limit=NULL) {
        return $user === NULL
                ? $user
                : Model::factory('Dba')
                    ->select()
                    ->from('histories')
                    ->where("actor = $user")
                    ->order_by('create_time','DESC')
                    ->limit($limit)
                    ->execute();
    }
    
    //Get current or lower group activities
    public static function group_activities($limit = NULL) {
        $items = Model::factory('History')->latest_histories($limit);
        echo Debug::vars($list_users);
    }
    
    //Return client ip address
    public static function ip() {        
        $possible_ip = array(
                'HTTP_CLIENT_IP', 
                'HTTP_X_FORWARDED_FOR', 
                'HTTP_X_FORWARDED', 
                'HTTP_X_CLUSTER_CLIENT_IP', 
                'HTTP_FORWARDED_FOR', 
                'HTTP_FORWARDED', 
                'REMOTE_ADDR'
            );
        $ip = 'break';    
        foreach ($possible_ip as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    
                    /**
                     * filter_var($ip, FILTER_VALIDATE_IP) : check all possible ip
                     * filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) : check for private ip
                     */                                                             
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
    
    //(object) user role permission handler    
    public static function role_permission($module=null,$permission=null) {
        
        if (!empty(self::$_current->id)) {
            if (empty(self::$_current->permission)) {
                //Set permission as a standard class
                self::$_current->permission = array();
                
                //Get current user roles permission
                $roles = Model::factory('User')->mapping_role_permission();
                if (!empty($roles)) {
                    foreach ($roles as $role) {
                        self::$_current->permission[$role->module_name][$role->permission] = true;
                    }
                }
            }
            
            if (!empty($module) && !empty($permission))
                return !empty(self::$_current->permission[$module][$permission]);
            elseif (!empty($module))
                return !empty(self::$_current->permission[$module]) ? self::$_current->permission[$module] : false;
            
        }
        return false;
    }
    
    /**
     * Authorise role user based on module permission
     * @var mixed modules
     * @var mixed permissions
     */
    public static function authorise($modules,$permissions=null) {
        //No need to authorise first superadmin
        if (self::$_current->role_id === 1 || self::$_current->role_id === '1') return true;
        
        //Keep permissions as an array
        $permissions = !is_array($permissions) ? array($permissions) : $permissions;
        
        //Keep modules as an array
        $modules = !is_array($modules) ? array ($modules=>$permissions) : $modules;
        
        //Looping modules and permissions
        foreach ($modules as $ky => $va) {
            //Keep permissions as an array
            $va = !is_array($va) ? array($va) : $va;
            foreach ($va as $permission) {
                if (!empty(self::$_current->permission[$ky][$permission])) return true;                    
            }   
        }
        return false;
    }
    
    public static function delete_private_file($id,$username) {
        //Delete profile picture        
        $count    = count(json_decode(json_encode(User::$config->images),true));   
        foreach (User::$config->images as $k => $conf) {
            $dest = URL::mediaImage($conf->path,true);
            foreach (array('jpg','jpeg','gif','png') as $ext) {
                $file_image = $dest."/$username.$ext";
                if (file_exists($file_image)) {
                    unlink($file_image);
                }    
            }
        }
    }
    
    public static function token($density=10) {
        $aRand0 = array("7Man","Gun","Song","lEi","NaD","eIN","End","Hil","tIL","ABS");
        $aRand1 = array('aB','bC','cD','dE','eF','fG','gH','hI','iJ','jK','kL','lM','mN','nO','oP','pQ','qR','rS','sT','tU','uV','vW','wX','xY','yZ','xA');
        
        $token  = '';
        for ($i = 0; $i < $density; $i++) {
            $iRand0 = rand(0,9);
            $iRand1 = rand(0,25);
            $iRand3 = rand(0,1);
            $aRand  = "aRand$iRand3";
            $iRand  = "iRand$iRand3";            
            $arr    = $$aRand;
            $token .= $arr[$$iRand];
        }
        
        //Check for token
        $check = Model::factory('User')
                        ->select('user_id')
                        ->from('user_tokens')
                        ->where('is_use = 0')
                        ->and_where("token = $token")
                        ->execute()
                        ->get('user_id');
                            
        if (!empty($check)) {
            $token = User::token($density);    
        }
        return $token;
    }
    
    public function whoIsLoginAdmin() {
        //Data Query
        $query = "SELECT s.`user_id` FROM `user_sessions` s WHERE s.`login` >= '"  . date('Y-m-d 00:00:01'). "' AND s.`logout` IS NULL";                    
        
        //Total Query
        $total = "SELECT COUNT(`user_id`) AS `total` FROM ($query)xtotal";        
        $total = DB::query(Database::SELECT,$total)->execute()->get('total');
        
        $res   = array('total'=>$total);        
        
        $data  = DB::query(Database::SELECT,$query)->as_object()->execute();
        
        if ($data->valid()) {
            foreach ($data as $item) 
                $res[$item->user_id] = empty($res[$item->user_id]) ? 1 : $res[$item->user_id]+1;
        }
        
        return $res;
    }
}

?>