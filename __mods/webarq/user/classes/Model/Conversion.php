<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author      Daniel Simangunsong
 * @Company     Webarq
 * @copyright   2012
 * @Package     Database Model
 * @Module      User
 * @License     Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors  
**/ 
class Model_Conversion extends Model_Dba {
    /** Data listing purpose **/
    public function total() {
        $q = Data::total(array('conversions','h'),'actor',false);
                    //->join(array('users','u'))->on("u.username = h.actor")
                    //->join(array('roles','r'))->on("r.id = u.role_id")
                    //->where("r.id >= ".User::admin()->role_id);
        
        
        //if (!empty($this->data_tool['search'])) {
            //$search_key = $this->data_tool['search'];
            //$q->where_open()->and_where("h.actor like %$search_key%")->or_where("h.description like %$search_key%")->where_close();
        //}   
               
        return $q->execute()->current()->total; //App::$session->set('lst-tool',$data_tool);
    }
    
    public function tabular($limit=null,$offset=null) {
        
        $q = $this->select('h.title','h.element', 'h.value', 'h.ip', 'h.create_time','h.table')
                    ->from(array('conversions','h'))
                    ->order_by('h.create_time','DESC')
                    ->limit(isset($limit) ? $limit : App::$config->perpage)
                    ->offset($offset);
        //
//        if (!empty($this->data_tool['search'])) {
//            $search_key = $this->data_tool['search'];
//            $q->where_open()->and_where("actor like %$search_key%")->or_where("description like %$search_key%")->where_close();
//        }
        $q = $q->execute();
                
        
        $xitem = array();
         
        if (!empty($q)) {
            foreach ($q as $key => $item) {
                $xitem[$key] = $item;
                $xitem[$key]->activity = History::detail_activity($item,false);
            }
        }            
        return $xitem;
    }
    /** End Data listing purpose **/         
    
    protected function pre_select($field = NULL) {
        return $this->select('*')
                    ->from(array('conversions','h'));
    }
    
    //Get latest histories
    public function latest_histories($limit = NULL,$execute = TRUE) {
        if (is_bool($limit)) {
            $execute = $limit;
            $limit   = NULL;
        }
        
        $items = $this->pre_select()
                    //->join(array('users','u'))->on("u.username = h.actor")
                    //->join(array('roles','r'))->on("r.id = u.role_id")
                    ->order_by('h.create_time','DESC');
                    
        if (isset($limit)) $items->limit($limit);
        
        return $execute === TRUE ? $items->execute() : $items;
    }
    
    /**
     * Get latest histories depending on current role id
     * @param   int limit
     * @param   int role id
     * @return  Kohana Query Builder Execute
     */
    public function role_group_histories($limit = NULL, $role_id = NULL) {
        $role_id = is_numeric($role_id) ? $role_id : ($role_id === TRUE ? User::admin()->role_id : NULL);        
        $items   = $this->latest_histories($limit,FALSE);
        
        //if (isset($role_id))
        //    $items->where("r.id >= ".User::admin()->role_id);
                        
        return $items->execute();
    }
    
    /**
     * Get latest histories depending on current role id, and range date
     * @param   int   span days
     * @return  array result
     */
    public function dashboard($span = 7) {
        $role_id = User::admin()->role_id;        
        $post = (object) Request::current()->post(); 
        // set default year
        $start  = (isset($post->year) ? $post->year : date('Y'));
        $end    = (isset($post->year) ? ((int)$post->year - 1) : date("Y", strtotime("-1 year")));
        $month = array( 1 => 'Januari', 2 => 'Februari', 3 =>'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember' );   
       

        $items = DB::query(
                    Database::SELECT,
                    'select count(title) as activity, create_time, title  FROM `conversions` WHERE YEAR(create_time) = '. $start .' group by create_time')->as_object()->execute(); 
                    // 2 March 2015 => 'select count(title) as activity, create_time, title  FROM `conversions` WHERE YEAR(create_time) <= '. $start .' and YEAR(create_time) >='. $end .' group by create_time')->as_object()->execute();

        if (!$items->valid()) return null;
        
        $result['item'] = array();
        foreach($month as $key => $val){
            $items = DB::query(
                    Database::SELECT, 
                    'select count(title) as activity, create_time, title  FROM `conversions` WHERE MONTH(create_time) = '. $key .' and YEAR(create_time) = '. $start .' group by title')->as_object()->execute();
            
            foreach($items as $i){
                $result2[$i->title][$start .'-'. $key .'-01'] = (int) $i->activity;  
            }
        }
        
        foreach($month as $key => $val){
            foreach($result2 as $k => $v){
                if(!isset( $result2[$k][$start .'-'. $key .'-01']))
                    $result2[$k][$start .'-'. $key .'-01'] = 0;
            }
        }

        $result['item'] = $result2;
        $result['max'] = isset($max) ? $max : '';
        return $result;
    }
}