<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_User extends Model_Dba {
    
    protected function pre_select(){
        return $this->select('u.*',DB::expr("CASE WHEN s.user_id IS NULL THEN '' ELSE 'login' END AS `is_login`"),array('r.label','role_label'),'r.is_admin','us_ds.user_id','us_ds.first_name',
                                'us_ds.last_name','us_ds.middle_name','us_ds.sex','us_ds.address','us_ds.phone',
                                'us_ds.avatar_file_name',
                                DB::expr("CONCAT(us_ds.first_name,' ',us_ds.last_name) AS `full_name`"),
                                DB::expr("CASE u.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('users','u'))
                    ->join(array('roles','r'),'left')->on('u.role_id = r.id')
                    ->join(array('user_details','us_ds'),'left')->on('u.id = us_ds.user_id')
                    ->join(array('user_sessions','s'),'left')->on('u.id = s.user_id')->on('s.login','>=',DB::expr("'".date('Y-m-d 00:00:01'."'")))->on('s.logout','IS',DB::expr("NULL"))
                    ->group_by('u.id');
    }
    
    public function session_scan($session) {
        return $this->pre_select()
                    ->join(array('user_sessions','uss'))
                    ->on('u.id = uss.user_id')
                    ->where('uss.session = '.$session)
                    ->and_where('u.is_active = 1')
                    ->and_where('uss.logout = NULL')
                    ->limit(1)
                    ->execute()
                    ->current();
    }
    
    public function login_scan($username,$password) {
        $scan = $this->pre_select()
                     ->where_open()
                     ->where("u.username = $username")
                     ->or_where("u.email = $username")
                     ->where_close()
                     ->and_where("u.password = $password")
                     ->and_where('u.is_active = 1');
        //Ip check            
        if (!empty(User::$config->check_ip)) {
            $scan->and_where_open()
                 ->where("u.ip_addr = ".User::ip())
                 ->or_where("u.ip_addr IS NULL")
                 ->or_where("u.ip_addr","=","")
                 ->and_where_close();     
        }   
                         
        return $scan
                    ->limit(1)
                    ->execute()
                    ->current();
    }
    
    public function id_scan($id) {
        return $this->pre_select()
                    ->where("u.id = $id")
                    ->limit(1)
                    ->execute()
                    ->current();
    }
    
    public function session_register($user_id,$session=null) {
        $session = empty($session) ? App::$session->id : $session;
        return $this->insert('user_sessions')
                    ->row('user_id',$user_id)
                    ->row('session',$session)
                    ->row('login', App::date())
                    ->execute();
    }
    
    public function session_logout($user_id,$session=null) {
        $session = empty($session) ? App::$session->id() : $session;
        return $this->update('user_sessions')
                    ->set('logout',App::date())
                    ->where("user_id = $user_id")
                    ->and_where("session = $session")
                    ->execute();
    }
    
    public function mapping_role_permission() {
        return $this->select('mp.*',array('m.label','module_name'))
                    ->from(array('module_permissions','mp'))
                    ->join(array('module_permission_role_maps','mprm'))->on("mprm.module_permission_id = mp.id")
                    ->join(array('modules','m'))->on("m.id = mp.module_id")
                    ->join(array('users','u'))->on("u.role_id = mprm.role_id")
                    ->where("u.role_id","=",User::admin()->role_id)
                    ->execute();
    }

    public function token_owner($token,$email) {
        $owner = $this->select("user_id")
                      ->from("user_tokens")
                      ->where("token = $token")
                      ->and_where("email = $email")
                      ->and_where("is_use = 0")
                      ->and_where("expired",">=",date('Y-m-d H:i:s'))
                      ->execute()
                      ->get("user_id");
        return !isset($owner) ? false : $owner;                       
    }
    
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('users','u'),false)
                        ->join(array('roles','r'),'left')->on('u.role_id = r.id')
                        ->join(array('user_details','us_ds'))->on('u.id = us_ds.user_id')
                        ->where('u.role_id','>=',User::admin()->role_id);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select()->where('u.role_id','>=',User::admin()->role_id);
        
        return $model;
    }
    /** End Data listing purpose **/
}