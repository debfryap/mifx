<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_User_Cms_Permission extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
    }
    
    public function action_index() {
        
        $this->media_tabular();
        
        $this->authorise('user','permission')
             ->register('title')
             ->use_string(__('edit-permission'))
             ->register('content')
             ->use_view('user/cms/permission')
                ->set('bola','bola')  
                ;    
    }
}