<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_User_Cms_Auth extends Controller_Default_Template_Cms {
    
    public function before() {
        $this->template = 'system';
                
        parent::before();
    }
    
    public function action_index() {
        $this->action_login();            
    }    
    
    public function action_login() {
        //Media validate
        $this->media_validate();
        
        $this->meta('title',__('login_area'));
        if (!empty(App::$config->message->error)) {
            $this->meta('title',App::$config->message->error);    
        }
        
        //User administrative
        if (!(empty(User::admin()->is_login) || empty(User::admin()->is_admin))) {
            $this->deflect('user/profile');            
        }
        $this->scripts(URL::sysJs('function/function'))             
             ->register('content')
                ->use_widget('user/admin-webarq-login')
             //->register('content',View::factory('profiler/stats'))                
                ;
    }
    
    public function action_logout() {
        if (!empty(User::admin()->id)) {
            //Append history
            History::record(array(
                'row_id'  => User::admin()->id,
                'table'   => 'users',
                'actor'   => User::admin()->username,
                'action'  => "logout"
            ));
            
            //Do logout
            User::logout(App::cookie('webarqcms'));
            
            //Unset css cookie
            Cookie::set('webarqCms',null);
        }
        
        //Redirect login page
        $this->deflect('user/auth/login');
    }
    
    public function action_validate() {
        
        if (User::$config->captcha) {
            $response = Captcha::valid($this->post->captcha);
            
            if ($response === false) {
                App::$session->set('error_message',__('msg_error_captcha'));
                $this->deflect('user/auth/login/error');
                return;
            }
        }
        
        User::login($this->post->username,$this->post->password);
        
        if (!empty(User::admin()->is_admin)) {
            //Append history
            History::record(array(
                'row_id'  => User::admin()->id,
                'table'   => 'users',
                'actor'   => User::admin()->username,
                'action'  => "login"
            ));
            $this->deflect('user/profile');
        } else {
            App::$session->set('error_message',__('msg_error_login'));
            $this->deflect('user/auth/login/error');
        }
    }
    
    public function action_forgot_password() {
        $this->meta('title',__('login_area'));
        if (!empty(App::$config->message->error)) {
            $this->meta('title',App::$config->message->error);    
        }
        
        //User administrative
        if (!(empty(User::admin()->is_login) || empty(User::admin()->is_admin))) {
            $this->deflect('user/profile');            
        }
        $this->scripts(URL::sysJs('function/function'))
             
             ->register('content')
                ->use_widget('user/admin-webarq-login')
                ;
    }   
    
    public function action_change_password() {
        //Media validate
        $this->media_validate();
        
        $this->meta('title',__('change-password'));
        if (!empty(App::$config->message->error)) {
            $this->meta('title',App::$config->message->error);    
        }
        
        //User administrative
        if (!(empty(User::admin()->is_login) || empty(User::admin()->is_admin))) {
            $this->deflect('user/profile');            
        }
        
        $owner = isset($this->get->token) && isset($this->get->email)
                    ? Model::factory('User')->token_owner($this->get->token,$this->get->email)
                    : false;
        $this->scripts(URL::sysJs('function/function'))             
             ->register('content')
                ->use_widget('user/admin-change-password',array(
                                'user_id' => $owner,
                                'token'   => $this->get->token));
    }
    
}    