<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_User_Cms_History extends Controller_Default_Template_Cms {
    
    public function action_index() {
        //Authorisation
        $this->authorise('user','view-history');
        
        $this->meta('title',__('history'));
        
        //Append css styles
        $this->media_tabular();
        
        //Open listing
        $listing = Data::listing('history')
                        ->set('create_time','datetime') //Disable header create time
                        ->set('update_time',false);     //Disable header update time
        
        //Disable listing sortby
        $listing->tool('sort_by',false)->tool('search',array('actor','activity'));
        
        //Listing header
        $listing->set_header('actor',array('label'=>__('actor'),'style'=>'width:300px;'))                                                             
                ->set_header('activity');        
        
        //Assign to template
        $this
            ->register('title')->use_string(__('recent-activity')) //Title
            ->register('content')->use_string($listing)
                ;   
    }
}