<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_User_Cms_Ajax extends Controller_Default_Template_Ajaxcms {
    
    public function before() {
        parent::before();
    }
    
    public function action_is_old_password() {
        $x = !empty($this->post->chk) ? User::make_password($this->post->chk) : 'xxx';
        
        $s = Model::factory('Dba')
                        ->select('password')
                        ->from('users')
                        ->where("username = ".User::admin()->username)
                        ->and_where("password = $x")->execute()->current();
        $this->content = !empty($s) || $this->post->chk == '' ? 'true' : 'false';
    }
    
    public function action_publish() {
        
        if (User::authorise('user','publish-user')) {
            $id = $this->param1;
            $label = $this->param2;
            $do = Model::factory('Dba')->update('users')->set('is_active',1)->where("id = $id")->and_where('is_system = 0')->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,'users',$label,'publish');
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        } else {
            $this->content = 'true';
        }
    }
    
    public function action_unpublish() {
        
        if (User::authorise('user','publish-user')) {
            $id = $this->param1;
            $label = $this->param2;
            $do = Model::factory('Dba')->update('users')->set('is_active',0)->where("id = $id")->and_where('is_system = 0')->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,'users',$label,'unpublish');
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        } else {
            $this->content = 'true';
        }
    }
    
    public function action_delete() {
        
        if (User::authorise('user','delete-user')) {            
            $id = $this->param1;
            $label = $this->param2;        
            $do = Model::factory('Dba')->delete('users')->where("id = $id")->and_where('is_system = 0')->execute();
            
            if (!empty($do)) {
                //Delete detail
                Model::factory('Dba')->delete('user_details')->where("user_id = $id")->execute();
                
                //Delete user session
                Model::factory('Dba')->delete('user_sessions')->where("user_id = $id")->execute();                
                
                //Delete private file
                User::delete_private_file($id,$label);
                
                //Record history
                History::simple_record($id,'users',$label,'delete');
                
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        } else {
            $this->content = 'true';
        }
    }
    
}    