<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_User_Cms_AjaxRole extends Controller_Default_Template_Ajaxcms {
    
    public function action_publish() {
        
        if (User::authorise('user','publish-role')) {
            $id = $this->param1;
            $label = $this->param2;
            $do = Model::factory('Dba')->update('roles')->set('is_active',1)->where("id = $id")->and_where('is_system = 0')->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,'roles',$label,'publish');
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        } else {
            $this->content = 'true';
        }
        
    }
    
    public function action_unpublish() {
        
        if (User::authorise('user','publish-role')) {
            $id = $this->param1;
            $label = $this->param2;        
            
            $do = Model::factory('Dba')->update('roles')->set('is_active',0)->where("id = $id")->and_where('is_system = 0')->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,'roles',$label,'unpublish');
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        } else {
            $this->content = 'true';
        }
    }
    
    public function action_delete() {
        
        if (User::authorise('user','delete-role')) {            
            $id = $this->param1;
            $label = $this->param2;      
            
            //Check for user
            $do = Model::factory('Dba')->select('id')->from('users')->where("role_id = $id")->limit(1)->execute()->get('id');
            
            if (!empty($do)) {
                $this->content = 'false';
                return;
            }
            
            $do = Model::factory('Dba')->delete('roles')
                    ->where("id = $id")
                    ->and_where('is_system = 0')
                    ->execute();
            
            if (!empty($do)) {
                //Record history
                History::simple_record($id,'roles',$label,'delete');
                
                //Un-set all user from this group
                Model::factory('Dba')->update('users')->set('role_id',0)->where("role_id = $id")->execute();
                
                $this->content = 'true';
            } else {
                $this->content = 'false';
            }        
        } else {
            $this->content = 'true';
        }
    }
    
    public function action_role_setting() {
        $value      = $this->post->value;
        $role_id    = $this->post->role_id;
        $role_name  = $this->post->role_name;
        $pid        = $this->post->pid;  
        $permission = $this->post->permission;
         
        if ($value == 0) {
            //Delete from maps
            $delete = Model::factory('Dba')
                        ->delete('module_permission_role_maps')
                        ->where("role_id = $role_id")
                        ->and_where("module_permission_id = $pid")
                        ->execute();
            $action = empty($delete) ? null : 'dissallow';
        } else {
            //Insert in to maps
            $insert = Model::factory('Dba')
                        ->insert('module_permission_role_maps')
                        ->row('role_id',$role_id)
                        ->row('module_permission_id',$pid)
                        ->row('create_time',App::date())
                        ->execute();
            $action = empty($insert) ? null : 'allow';
        }
        
        if (isset($action)) {
            History::record(array(
                    'row_id'      => $role_id,
                    'table'       => 'roles',
                    'actor'       => User::admin()->username,
                    'description' => '{"object":"'.$role_name.'","action":"'.$action.'","item":"'.$permission.'","todo":"to","table":""}',
                ));
                
            $this->content = 'true';
        } else {
            $this->content = 'false';
        }
    }
}    