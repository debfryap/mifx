<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_User_Cms_Profile extends Controller_Default_Template_Cms {
    public function before() {
        parent::before();
        
        $this->meta('title','User Profile');
    }
    
    public function action_index() {
        
        $this->authorise('user','view-profile')
             ->register('content')
                ->use_widget('user/simple-profile')
             ->register('title')
                ->use_string(User::admin()->full_name);
    }
    
    public function action_edit() { 
        $uid = $this->param2;
        
        $this->media_validate();
                
        $this->authorise('user','edit-profile');
        
        if ($uid == User::admin()->id || User::admin()->role_id == 1) {   
            //Uploadify
            $this->media_uploadify();
            
            //Assign template
            $this->register('title')->use_string(__("edit-profile"))
                 ->register('content')->use_view('user/cms/frm-edit-profile');
        } else {
            $this
                ->register('title')->use_string(__('error.403'))
                ->register('content')->use_string(__('error.403'));
        }
    }
    
    public function action_update() {
        
        $uid  = $this->param1;
        $post = $this->post;
        
        if (($uid == User::admin()->id || User::admin()->role_id == 1) && !empty($this->post)) {
            
            $tables = array();
            
            foreach ($post->app_transaction_pairs as $table => $columns) {
                $tables[] = $table;
            }
            
            $parent_table = Data::check_parent($tables);
            $foreign_key  = Data::foreign_key($parent_table);
            $updated      = false;
            
            foreach ($post->app_transaction_pairs as $table => $columns) {
                $q = Model::factory('Dba')->update($table);
                foreach ($columns as $clm => $frm) {
                    if (!empty($post->$frm)) {
                        $va = $post->$frm;
                        
                        if ($clm === 'password') {
                            if (empty($post->old_password)) {
                                App::$session->set("transaction_message",__('msg_fill_old_password'));
                                $this->deflect("user/profile/edit/id/$uid");
                                return;
                            }
                            $va = User::make_password($va);
                        }
                        
                        if ($clm === 'avatar_file_name') {
                            $count    = count(json_decode(json_encode(User::$config->images),true));   
                            foreach (User::$config->images as $k => $conf) {
                                $w = $conf->size[0];
                                $h = $conf->size[1];
                                $i = empty($i) ? 1 : $i+1;
                                
                                $image = Picture::resize(array(
                                    'src'    => 'media/temp/'.$va,
                                    'dest'   => URL::mediaImage($conf->path,true),
                                    'name'   => User::admin()->username,
                                    'width'  => $w,
                                    'height' => $h,
                                    'delete_src' => $i >= $count ? true : false
                                )); 
                            }
                            
                            $va = isset($image) ? Helper_File::type($image,'/') : $va;     
                        }
                        
                        $q->set($clm,$va);    
                    } else {
                        if ($clm == 'ip_addr')
                            $q->set($clm,NULL);
                    }
                }  
                  
                if (strtolower($parent_table) == strtolower($table)) {
                    $q->where("id = $uid");
                } else {
                    $q->where("$foreign_key = $uid");
                }
                $e = $q->execute();
                
                if (!empty($e)) {                    
                    $updated = $updated === false ? true : $updated;
                    History::record(array(
                        'row_id'      => $uid,
                        'table'       => $table,
                        'actor'       => User::admin()->username,
                        'description' => '{"object":"'.$post->username.'","action":"update"}'
                    ));
                }            
            }
            
            if ($updated === true) {
                //Session message
                App::$session->set("transaction_message",__('success.transaction',array(
                    ':object'=>'Profile',':action'=>'updated')));
            } 
            $this->deflect("user/profile/edit/id/$uid");
            
        } else {
            $this
                ->register('title')->use_string(__('error.403'))
                ->register('content')->use_string(__('error.403'));
        }
    }
}