<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_User_Helper_Token extends Controller_Ajax {
    
    public function before() {
        parent::before();
    }
    
    public function action_request_new_password() {
        if (!empty($this->post->email)) {
            $user = Model::factory('User')
                                ->select('id','email')
                                ->from('users')
                                ->where("email","=",$this->post->email)
                                ->and_where("is_active = 1")
                                ->execute()
                                ->current();
            
            if (!empty($user->email)) {
                //Expired token
                $exp_hour = (int) User::$config->token_expired;                
                
                //Save token
                $token    = User::token();
                $exp_date = date('Y-m-d H:i:s',mktime(date('H')+$exp_hour));
                Model::factory('User')
                            ->insert('user_tokens')
                            ->row("user_id",$user->id)
                            ->row("email",$user->email)
                            ->row("token",$token)
                            ->row("expired",$exp_date)
                            ->execute();
                
                //Send to email
                $subject = 'Your new password';
                $message = View::factory('email/user-token',array(
                                'email'=>$user->email,'token'=>$token,'subject'=>$subject,'expired'=>$exp_date));
                //Data::mailer($user->email,$subject,$message);
                
                Email::factory($subject,$message,'text/html')
            				->from(Contact::$config->system_mail_from)
            				->to($user->email)
            				->send();    
                
                //Return
                $this->content = __('email-new-password',array(':expire'=>$exp_date));
                
                return;    
            }                                
        }
        $this->content = 'false';
    }
    
    public function action_update_password() {
        $post = $this->post;
        if (!empty($post->token) && !empty($post->password) && !empty($post->user_id)) {
            $update = Model::factory('Dba')
                        ->update('user_tokens')
                        ->set("is_use",1)
                        ->where("token = $post->token")
                        ->execute();
                          
            if (!empty($update)) {
                $update = Model::factory('Dba')
                                ->update('users')
                                ->set("password",User::make_password($post->password))
                                ->set("update_time",date('Y-m-d H:i:s'))
                                ->where("id = $post->user_id")
                                ->execute();
            }
            
            if (!empty($update)) {
                $user = Model::factory('User')
                            ->select('username')
                            ->from('users')
                            ->where("id = $post->user_id")
                            ->execute()
                            ->current();
                History::record(array(
                    "actor"       => "$user->username",
                    "table"       => "user",  
                    "row_id"      => $post->user_id,
                    "action"      => "update",
                    "object"      => "$user->username",
                    "item"        => "password"
                ));
                $this->content  = 'Congratulations. Your password have been changed. Now you can login with your new password';
                $this->content .= "<p/>".HTML::anchor(URL::cms().'user/auth/login','Go to login page')."</p>";
            } else {
                $this->content = "Error, while trying to update your password. Please contact your administrator".Model::factory('Dba')
                                ->update('users')
                                ->set("password",User::make_password($post->password))
                                ->set("update_time",date('Y-m-d H:i:s'))
                                ->where("id = $post->user_id");    
            }
            return;
        }   
        $this->content = __('error.411'); 
    }
    
}    