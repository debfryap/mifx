<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Email token
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$link = URL::cms("user/auth/change-password?token=$token&email=$email");

?>

<html>
    <title><?php echo $subject;?></title>
    <body>
    Hi there ...
    <p>
        Please be note, you get this email as per your requesting for a new password at "<?php echo URL::cms();?>" link.
        If you feel that you never requested for this, just ignore and delete permanently this email accordingly, to avoid unnecessary things.
    </p>
    <p>
        To change your password please click <?php echo HTML::anchor($link,'this link'); ?> or copy paste this "<?php echo $link;?>" to your browser address bar. 
    </p>
    <p>
        However that link only valid until <?php echo $expired;?>.
    </p>
    <p style="font:10px/11px calibri;">
        Thank you <br />
        <?php echo __('auto-generate-email-label'); ?>
    </p>
    </body>
</html>