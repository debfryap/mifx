<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Role ~ CMS
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
?>

<?php   
    $id    = Request::$initial->param('param2');
    
    $value = in_array($id,array(1)) || User::admin()->role_id > $id ? false : 'auto';
     
    echo Data::form(array(
        'action'    => false,
        'controller'=> 'manage-role',
        'title'     => $title,
        'id'        => 'form_add_role',
        'multilang' => false,  
        'pack'      => array ('user:'.Request::$initial->param('param1').'-role'),
        'row_id'    => array('id',$id),
        'values'    => $value
    ));
?>