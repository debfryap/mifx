<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

?>

<div id="wg-user-admin-webarq-login" class="<?php echo !isset($class) ? 'normal' : $class; echo User::$config->captcha ? ' with-captcha' : ''?>">
    <div class="wg-header header-left"><div class="wg-header header-right"><div class="wg-header header-center">
        <div id="inner-header-right">
            <div class="logo-client"></div>
            <div class="clear"></div>
        </div>
    </div></div></div>  
    
    <div class="wg-content">
        <div class="wording">
            <form id="frm-admin-webarq-login" 
                action="<?php echo URL::cms();?>user/auth/validate" 
                method="POST">
                <div class="fl label username"></div>
                <div class="fl input">
                    <input type="text" name="username" class="required" value="Username"/>
                </div>
                <div class="clear break15"></div>
                
                <div class="fl label password"></div>
                <div class="fl input">
                    <input type="password" name="password" class="required" value="Password"/>
                </div>
                
                <?php
                
                    if (User::$config->captcha) {
                        echo '<div class="clear break15"></div>';
                        echo '<div class="fl" style="width:120px;">',
                            Captcha::instance()
                                ->reconfig('width',110)
                                ->reconfig('height',39)
                                ->render(),
                            '</div>';
                        echo '<div class="fl input" style="width:125px;"><div class="shadow-left">',
                             '<input type="text" name="captcha" class="required captcha"/>',
                             '</div></div>';
                    }
                ?>                
                <div class="clear break15"></div>
                
                <div style="border-top: 1px solid #eaeaea;padding-top:15px;height:32px;">
                <?php
                    if (!empty(App::$config->message->error)) {
                        echo '<span class="error-message">'.App::$config->message->error,'</span>';
                        echo '</div>';
                        echo '<div>';   
                    } 
                ?>
                
                    <div class="fl">
                        <a class="forgot-password" style="color:#1076bc;font:11px/32px verdana;" href="<?php echo URL::cms(); ?>user/auth/forgot-password">
                            <?php echo __('forgot-password');?>
                        </a>
                    </div>
                    <input type="submit" class="submit" value=""/>
                    <div class="clear break1"></div>
                </div>
            </form>
        </div>
    </div> 
    
    <div class="wg-footer"><div class="wording">WEBARQ Content Management System</div></div>
    <div class="break10"></div>
    <div class="logo-webarq">
        Copyright &copy; 2012
    </div>
    </div>
</div>

<script>
    center_middle('#wg-user-admin-webarq-login');
    
    $(document).ready(function(){
        var username = $('input[name="username"]');
        var password = $('input[name="password"]');
        
        username.focus(function(){
            if ( $(this).val() == 'Username' ) $(this).val(''); 
        }).blur(function(){
            if ( $(this).val() == '' ) $(this).val('Username');
        });
        password.focus(function(){
            if ( $(this).val() == 'Password' ) $(this).val(''); 
        }).blur(function(){
            if ( $(this).val() == '' ) $(this).val('Password');
        });
        
        $('#frm-admin-webarq-login').validate({
            errorPlacement  : function(error,element) { },
            submitHandler: function(form) {
                if (username.val() == 'Username' || username.val() == '') { username.focus().val('').addClass('error'); return; false }
                if (password.val() == 'Password' || password.val() == '') { password.focus().val('').addClass('error'); return; false }
                
                form.submit(); //submit
            },
            showErrors: function(errorMap, errorList) {
                $.each(errorList, function(i,element) {
                    var element_id = errorList[i].element.id;
                    $('#'+element_id).parent().parent().prevAll('span.wording :first').addClass('red');
                });    
            },
            onkeyup : function(element) {
                var value = $.trim($(element).val());
                if (value != '') {
                    $(element).parent().parent().prevAll('span.wording :first').removeClass('red');    
                }
            }
        });
        
        $('.forgot-password').click(function(e){
            e.preventDefault();
            
            enable_masking_layer('');
            $('#masking-html').html('Please input your email : <div class="form-request-new-password-container"> \
                                        <form id="form-request-new-password" action="'+bs_cms+'user/request-new-password"> \
                                        <input type="text" class="required email" name="email"/> \
                                        <input type="submit" value="Request"/> \
                                        </form></div>');
            $('#form-request-new-password').validate({
                errorPlacement : function() {},
                submitHandler  : function(form) {
                    
                    requester_email = $('input[name="email"]').val();
                    
                    disable_masking_layer();
                    
                    nous_process_message('Please wait for a while ... ',true);
                    
                    center_middle('#masking-html');
                    
                    setTimeout(function(){
                        $.ajax({
                            url     : bs_site+'user/token/request-new-password',
                            async   : false,
                            type    : 'POST',
                            data    : {"email":requester_email},
                            success : function(response) {
                                
                                nous_disable_message();
                                
                                if ($.trim(response) == 'false')
                                {
                                    jAlert('We do not found any email registered in the system like <b>'+requester_email+'</b> ','Warning');
                                    return;
                                }  
                    
                                enable_masking_layer(response);
                            }
                        })    
                    },10);    
                }
            });                                        
            return false;
        })
        
    });
</script>