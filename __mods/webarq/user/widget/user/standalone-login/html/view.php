<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

?>

<div id="widget_user_standalone_login">
    <form id="standalone_login_form" 
          action="<?php echo URL::cms();?>user/auth/validate" 
          method="POST">
        <div class="widget_background"></div>
        <div class="widget_content">
            &nbsp;<span class="wording"><?php echo __('username');?></span>
            <div class="break5"></div>
            <div class="input_container_left">
                <div class="input_container_right">
                    <input id="username" name="username" type="text" class="required input_container_center"/>
                </div>
            </div>
            
            <div class="break5"></div>
            &nbsp;<span class="wording"><?php echo __('password');?></span>
            <div class="break5"></div>
            <div class="input_container_left">
                <div class="input_container_right">
                    <input id="password" name="password" type="password" class="required input_container_center"/>
                </div>
            </div>
        </div>
        <div class="clear"></div>
            
        <div class="widget_action">
            <div class="fr" style="margin-top:5px;margin-right:10px;">
                <input type="submit" value=""/>
            </div>
            <div class="fr" style="margin-top:8px;margin-right:26px;">
                <a href="<?php echo URL::cms();?>user/auth/forgot-password"><?php echo __('forgot_password');?></a>
            </div>
            <div class="clear"></div>
        </div>
    </form>
    

    <?php
        echo !empty(App::$config->message->error) 
                ? '<div class="widget_message">'.App::$config->message->error.'</div>'
                : '';
    ?>
</div>

<script>
    center_middle('#widget_user_standalone_login');
    $(document).ready(function(){
        $('#standalone_login_form').validate({
            errorPlacement  : function(error,element) { },
            showErrors: function(errorMap, errorList) {
                $.each(errorList, function(i,element) {
                    var element_id = errorList[i].element.id;
                    $('#'+element_id).parent().parent().prevAll('span.wording :first').addClass('red');
                });    
            },
            onkeyup : function(element) {
                var value = $.trim($(element).val());
                if (value != '') {
                    $(element).parent().parent().prevAll('span.wording :first').removeClass('red');    
                }
            }
        });
        
    });
</script>