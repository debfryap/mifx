<?php defined('SYSPATH') or die('No direct script access.');

return array  (
    'default' => array (
        'check_ip'      => false,
        'captcha'       => false,
        'hash'          => 'ripemd160',
        'loginbyemail'  => true,
        'images'        => array (
            "small"     => array (
                'path'  => 'users/avatars/50x61',  
                'size'  => array(50,61)
            ),
            "medium"    => array (
                'path'  => 'users/avatars/120x145',
                'size'  => array(120,145)
            )
        ),
        'token_expired' => 24                           //in Hour       
    )
);            