<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'news-category'                  => 'Category',
    'news-category-id'               => 'News Category ID',
    'create-news-category'           => 'Create News Category',
    'edit-news-category'             => 'Edit News Category',
    'manage-news-category'           => 'Manage News Category',
    'image-news-category'            => 'Image News Category',
    'parent-news-category'           => 'Parent News Category',
    
    'news-list'                      => 'News Listing',
    'create-news'                    => 'Create News',
    'edit-news'                      => 'Edit News',
    'manage-news'                    => 'Manage News',
    'image-news-thumbnail'           => 'Thumbnail',
    'image-news-large'               => 'Image News',
    'featured-news'                  => 'Featured News',


    'news-event'                      => 'News Event',
    
    'branch-events'                 => 'Branch Events',
    'create-branch-events'          => 'Create Branch Events',
    'manage-branch-events'          => 'Manage Branch Events',
    
    'create-brach-events'            => 'Create Branch Event',
    'edit-brach-events'              => 'Edit Branch Event',
    'delete-brach-events'            => 'Delete Branch Event',
    'publish-brach-events'           => 'Publis Branch Event',
    

    'image-monex-schedule'          => 'Image',
    'image-branch-event'            => 'Image',
);