<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'news'                           => 'Updates',
    'news-category'                  => 'Category',
    'news-category-id'               => 'News Category ID',
    'create-news-category'           => 'Create News Category',
    'edit-news-category'             => 'Edit News Category',
    'manage-news-category'           => 'Manage News Category',
    'image-news-category'            => 'Image News Category',
    'parent-news-category'           => 'Parent News Category',

    'news-list'                      => 'Article',
    'create-news'                    => 'Create Article',
    'edit-news'                      => 'Edit Article',
    'manage-news'                    => 'Manage Article',
    'image-news-thumbnail'           => 'Thumbnail',
    'image-news-large'               => 'Image Article',
    'featured-news'                  => 'Highlight Article',
    'publish-news'                   => 'Publish/Un-Publish Article',
    'delete-news'                    => 'Delete Article',
    'feature-news'                   => 'Featured Article',

    'news-event'                     => 'Monex Event',
    'create-news-event'              => 'Create Monex Event',
    'edit-news-event'                => 'Edit Monex Event',
    'manage-news-event'              => 'Manage Monex Event',
    'featured-news-event'            => 'Highlight Monex Event',
    'publish-news-event'             => 'Publish Monex Event',
    'delete-news-event'              => 'Delete Monex Event',

    'news-update'                    => 'Monex Update',
    'create-news-update'             => 'Create Monex Update',
    'edit-news-update'               => 'Edit Monex Update',
    'manage-news-update'             => 'Manage Monex Update',
    'featured-news-update'           => 'Highlight Monex Update',
    'publish-news-update'            => 'Publish Monex Update',
    'delete-news-update'             => 'Delete Monex Update',

    'monex-schedule'                 => 'Monex Schedule',
    'create-monex-schedule'          => 'Create Monex Schedule',
    'edit-monex-schedule'            => 'Edit Monex Schedule',
    'manage-monex-schedule'          => 'Manage Monex Schedule',
    'featured-monex-schedule'        => 'Highlight Monex Schedule',
    'publish-monex-schedule'         => 'Publish Monex Schedule',
    'delete-monex-schedule'          => 'Delete Monex Schedule',
    'start-hour'                     => 'Start Hour',
    'end-hour'                       => 'End Hour',
    'region'                         => 'Zona Waktu',
    'is_feature_schecule'            => 'Feature Schedule',
    'closing_info'                   => 'Closing/End Information',

    'monex-video'                    => 'Monex Education Video',
    'create-monex-video'             => 'Create Monex Video',
    'edit-monex-video'               => 'Edit Monex Video',
    'manage-monex-video'             => 'Manage Monex Video',
    'is_feature_video'               => 'Highlight Monex Video',
    'publish-monex-video'            => 'Publish Monex Video',
    'delete-monex-video'             => 'Delete Monex Video',
    'youtube-link'                   => 'Youtube Video ID',
    
    'branch-events'                 => 'Branch Events',
    'create-branch-events'          => 'Create Branch Events',
    'manage-brach-events'          => 'Manage Branch Events',
    
    'create-brach-events'            => 'Create Branch Event',
    'edit-brach-events'              => 'Edit Branch Event',
    'delete-brach-events'            => 'Delete Branch Event',
    'publish-brach-events'           => 'Publis Branch Event',

    'image-monex-schedule'          => 'Image',
    'image-branch-event'            => 'Image',
    
    'create-event'                  => 'Create Event',
    'manage-event'                  => 'Manage Event',

    'belajar-trading'               => 'Belajar Trading',
    'manage-belajar-trading'        => 'Manage Belajar Trading',
    'create-belajar-trading'        => 'Create Belajar Trading',
    'edit-belajar-trading'          => 'Edit Belajar Trading',
    'publish-belajar-trading'       => 'Publish Belajar Trading',
    'delete-belajar-trading'        => 'Delete Belajar Trading',
);