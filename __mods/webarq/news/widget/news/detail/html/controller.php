<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller Widget
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/


class Widget_News_Detail extends Populate {
    public function render() {
        
        $permalink = trim(Request::$initial->param('permalink'),'/');  
        
        if (empty($permalink)) {
            $item = Model::factory('News_List')->latest(1);
            if (!empty($item[0])) {
                $permalink = Data::get_category_uri('news_categories',$item[0]->news_category_id)."/".$item[0]->permalink;
            }
        } else {
            $c = trim(Request::$initial->param('permalink'),'/');
        
            $t = explode('/',$c);
            
            $a = array_pop($t);
        
            $item = Model::factory('News_List')->item($a);
        }
        
        if (empty($item[0]))
            return __('no-item-data',array(':item'=>__('news')));
        
        $item  = $item[0];
        
        $html  = '<div class="widget" id="widget-news-detail">';
        $html .= Widget::load('station/bread-crumb',array('permalink'=>$permalink,'page'=>'news'))->render();
        $html .= '<h3 class="title">'.$item->label.'</h3>';
        $html .= '<div class="decription">'.$item->description.'</div>';
        
        if (!empty($item->images[0])) {
            $html .= '<div class="image-news">';
            
            foreach ($item->images as $image)
                $html .= HTML::image(URL::root($image->folder.'/'.$image->file),array(
                                'alt'   => App::$config->system->image_alt,
                                'style' => 'width:677px;margin-bottom:10px;'
                            ));
                
            $html .= '</div>';
        }
        
        $html .= '</div>';
        return $html;
    }
}