<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/


class Widget_News_Category_SideBar extends Populate {
    
    private $object;
    
    private $arrCrumb;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        $this->object = Data::traversing(false)
                    ->__set('db',Model::factory('news_category')->simple())
                    ->__set('order_by','cat.ordering')
                    ->__set('column_traverse','cat.parent_id')
                    ->__set('next_column_traverse_value','id')
                    ->__set('traverse_start',0)
                    ->__set('deep',App::$config->system->traversing_deep)
                    ->response();
        
        if (!empty(App::$module->language))
            $this->object = Translate::item('news_categories',$this->object);
        
        $this->arrCrumb = App::tail_active_uri('news')->get('tail');
    }
    
    public function render() {
        if (empty($this->object))
            return null;
        
        $html  = '<div class="widget" id="widget-news-category">';        
        $html .= '<div class="block-title">'.__('news-category').'</div>';
        $html .= $this->navigation_ul($this->object);
        $html .= '</div>';
        
        return $html;
    }
    
    private function navigation_ul($object,$parent_uri=null) {   
        $html = '<ul';
        $html .= '>';
        
        foreach ($object as $item) {
            $is_active = $item->label == 0;
            
            $html .= '<li';
            $html .= ' class="level-'.$item->level;
            $html .= $item->permalink == App::$config->menu->active 
                        || $item->permalink == App::$config->menu->group 
                            || (!empty($this->arrCrumb) && in_array($item->permalink,$this->arrCrumb)) ? ' active' : ''; 
            $html .= '"';
            $html .= '>';
            
            $uri   = !isset($parent_uri) ? $item->permalink : $parent_uri.'/'.$item->permalink;
            
            if (!empty(App::$module->language))
                $href  = Translate::uri(Language::$current,Language::$current."/news/$uri");
            else
                $href = URL::front("news/$uri");
            
            
                            
            $html .= '<a href="'.$href.'">';
            $html .= $item->label;
            $html .= '</a>';            
            
            if (!empty($item->child))
                $html .= $this->navigation_ul($item->child,$uri);
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
}