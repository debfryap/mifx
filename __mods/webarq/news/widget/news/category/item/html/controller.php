<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller Widget
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Widget_News_Category_Item {
    
    public function render() {
        $c = trim(Request::$initial->param('permalink'),'/');
        
        $t = explode('/',$c);
        
        $a = array_pop($t);
        
        $news = Model::factory('News_Category')->get_news($a);
        
        $html = Widget::load('station/bread-crumb',array('page'=>'news'))->render();
        
        if (empty($news)) { 
           $html .= 'No news found for this category';
           return;
        }
           
        $html  = '<div class="widget" id="widget-news-category-item">';
        
        $prefix_uri = "/news/".App::tail_active_uri('news')->get('active_uri');
        
        $html .= Widget::load('station/bread-crumb',array('page'=>'news'))->render();
        
        foreach ($news as $i => $news) {
            $uri   = Translate::uri(Language::$current,Language::$current.$prefix_uri.'/'.$news->permalink.'-detail');
            $html .= '<a href="'.$uri.'">';
            if (isset($news->thumbnail)) {
                $alt   = isset($news->thumbnail->detail->title) ? $news->thumbnail->detail->title : App::$config->system->image_alt;
                $html .= HTML::image(URL::root($news->thumbnail->folder.'/'.$news->thumbnail->file),array('alt'=>$alt));
            } else {
                $html .= '<div style="width:71px;height:90px;"></div>';    
            }
            $html .= '<div style="text-align:center;padding-top:10px;">'.substr($news->label,0,12).'</div>';
            $html .= '</a>';    
        }
        
        $html .= "</div>";
        
        return $html;
    }
}