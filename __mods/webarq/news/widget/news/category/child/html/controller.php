<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller Widget
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

class Widget_News_Category_Child {
    
    public function render() {
        $c = trim(Request::$initial->uri(),'/');
        
        $t = explode('/',$c);
        
        $a = array_pop($t);
        
        $p = substr($c,stripos($c,'news/')+5);
                
        $childs = Model::factory('News_Category')->get_child($a);
        
        if ($childs === false)
            return View::factory('error/404');
        
        if ($childs === null) 
           return Widget::load('news/category/item');
           
        $html  = '<div class="widget" id="widget-news-category-child">';
        $html .= Widget::load('station/bread-crumb',array('permalink'=>$p,'page'=>'news'))->render();
        
        foreach ($childs as $i => $child) {
            $uri   = Translate::uri(Language::$current,Language::$current.'/news/'.$child->permalink);
            $html .= '<a href="'.$uri.'">';
            if (isset($child->cover)) {
                $alt   = isset($child->cover->detail->title) ? $child->cover->detail->title : App::$config->system->image_alt;
                $html .= HTML::image(URL::root($child->cover->folder.'/'.$child->cover->file),array('alt'=>$alt));
            } else {
                $html .= '<div style="width:71px;height:90px;"></div>';    
            }
            $html .= '<div style="text-align:center;padding-top:10px;">'.substr($child->label,0,12).'</div>';
            $html .= '</a>';    
        }
        
        $html .= "</div>";
        
        return $html;
    }
}