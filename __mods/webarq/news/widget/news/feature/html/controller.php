<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller Widget
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/


class Widget_News_Feature extends Populate {
    
    protected $limit = 3;
    
    private $items;
    
    public function __construct() {
        $this->items = Model::factory('news_list')->feature($this->limit);
    }
    
    public function render () {
            
        $html  = '<div class="widget" id="widget-news-feature">';
        $html .= '<div class="block-title">'.__('featured-news').'</div>';
        
        if (empty($this->items[0])) {
            $html .= '<div style="padding:5px 0">'.__('no-item-data',array(':item'=>__('feature-news'))).'</div>';
            $html .= '</div>';
            return $html;
        }
        
        $html .= '<ul>';
        foreach ($this->items as $item) {
            $html .= '<li>';
            if (isset($item->thumbnail)) {
                $alt   = isset($item->thumbnail->detail->title) ? $item->thumbnail->detail->title : App::$config->system->image_alt;
                $html .= HTML::image(URL::root($item->thumbnail->folder.'/'.$item->thumbnail->file),array('alt'=>$alt));
            }        
            $html .= HTML::anchor($item->uri,$item->label);
            $html .= '</li>';        
        }
        $html .= '</ul>';
        $html .= '</div>';
        
        return $html;
    }
}