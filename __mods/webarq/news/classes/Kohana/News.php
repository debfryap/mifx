<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Api
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Kohana_News {    
    public static $config;
    
     /**
     * Module up (working both on development and production mode)
     */
    public static function up($config) {
                
    }
    
    /**
     * Module install (working on development mode only)
     */
    public static function install() {
        
        if (!self::$config->use_category) {
            //Drop table categories
            Data::drop_table('news_categories');
            
            //Drop navigations 
            Data::undo_module_navigation('news',array('category'));
            
            //Navigation list ordering
            $order_list = 1;
        } else {
            //Navigation list ordering
            $order_list = 2;
        }
        
        //Update navigation list ordering
        DB::update('module_navigations')->set(array('ordering'=>$order_list))->where('label','=','news-list')->and_where('permalink','=','list')->execute();
    }
}