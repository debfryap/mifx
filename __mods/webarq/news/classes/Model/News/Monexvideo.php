<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_News_Monexvideo extends Model_Dba {    
    public function pre_select() {
        return $this->select('mv.*',
                                DB::expr("CASE mv.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('monex_videos','mv'));
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('monex_videos','mv'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/    
    
    public function append_media($o,$medias = array()) {
    
        if (!$o->valid()) return null;
        
        foreach ($o as $k => $i) {
            
            $i     = Translate::item('monex_videos',$i);
        
            $x[$k] = $i;      
            
            //$uri   = Data::get_category_uri('news_categories',$i->news_category_id); 
            
            //$x[$k]->uri = Translate::uri(Language::$current,Language::$current."/news/$uri/$i->permalink"."-detail");
            
            if (!empty($medias)) {
                foreach ($medias as $type) {
                    $gm = Model::factory('Medialibrary')->simple_application($type,$i->id,'monex_videos')->current();
                    if (!empty($gm)) $medLib[$type] = $gm; 
                }
                
                $kn = "media_library";
                
                if (!empty($medLib)) $x[$k]->$kn = $medLib;
                unset($medLib);
            }
        }
        
        return count($x) == 1 ? $x[0] : $x; 
    }
    
    public function xdata($limit = null) {
        $get = $this->select('mv.*')
                    ->from(array('monex_videos','mv'))
                    //->join(array('news_categories','cat'))
                    //->on('cat.id = news_category_id')
                    ->where('mv.is_active = 1')
                    ->order_by('mv.ordering','ASC');
                    //->and_where('cat.is_active = 1');
                    
        return isset($limit) ? $get->limit($limit) : $get; 
    }
    
    public function feature($limit=5,$medias = array()) {
        $f = $this->xdata($limit)->and_where('mv.is_feature = 1')->execute();
        
        return $this->append_media($f,$medias);
    }
    
    
    public function latest($limit=5,$medias = array()) {
        $f = $this->xdata($limit)->execute();
        
        return $this->append_media($f,$medias);
    }
    
    public function item($permalink,$medias = array()) {
        $f = $this->xdata(1)->and_where("mv.permalink = $permalink")->execute();
        
        return $this->append_media($f,$medias);
    }
    
    public function others($current_active,$medias = array()) {
        $f = $this->xdata()->and_where("mv.permalink <> $current_active")->execute();
        
        return $this->append_media($f,$medias);
    }
}