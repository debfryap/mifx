<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_News_Category extends Model_Dba {        
    public function pre_select() {
        return $this->select('cat.*',
                                DB::expr("(SELECT COUNT(`nw`.`id`) FROM `news` AS `nw` WHERE `nw`.`news_category_id` = `cat`.`id`) AS `total_news`"),
                                DB::expr("CASE cat.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('news_categories','cat'));
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('news_categories','cat'),false)->where('cat.parent_id = 0');
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/   
    
    public function simple() {
        return $this->select('cat.id','cat.label','cat.permalink')
                    ->from(array('news_categories','cat'))
                    ->where('cat.is_active = 1');
    }
    
    private function append_media($o) {
    
        if (empty($o[0]))
            return null;
            
        foreach ($o as $k => $i) {
            $x[$k] = $i;            
            $x[$k]->uri = Translate::uri(Language::$current,Language::$current.'/'.'news/'.$i->permalink);
            
            $t = Model::factory('Medialibrary')->simple_application('news-category',$i->id,'news_categories');
                        
            if (isset($t[0]->folder)) {
                $x[$k]->cover  = $t[0];
                $x[$k]->images = $t;
            }
        }
        
        return $x;
    }
    
    public function get_child($value,$field='permalink') {
        $c = $this->select('cat.id')
                ->from(array('news_categories','cat'))
                ->where("cat.$field = $value")
                ->and_where("cat.is_active = 1")
                ->execute()
                ->current();
        
        if (empty($c->id))
            return false;
            
        $d = $this->pre_select()
                  ->where("cat.parent_id = $c->id")
                  ->and_where("cat.is_active = 1")
                  ->order_by('cat.ordering')
                  ->execute();
        
        return $this->append_media($d);
    }
    
    public function get_news($value,$field='permalink') {
        $p = Model::factory('news_list')
                    ->pre_select()
                    ->join(array('news_categories','cat'),'left')
                    ->on("nw.news_category_id = cat.id")
                    ->where("cat.$field = $value")
                    ->and_where('nw.is_active = 1')
                    ->execute();
        
        return Model::factory('news_list')->append_media($p);
    }
}