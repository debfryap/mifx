<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_News_List extends Model_Dba {    
    public function pre_select() {
        return $this->select('nw.*',
                                DB::expr("CASE nw.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function_publish`"),
                                DB::expr("CASE nw.`is_feature` WHEN 1 THEN 'unfeature' WHEN 0 THEN 'feature' END `function_feature`"))
                    ->from(array('news','nw'));
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('news','nw'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/   
    
    public function append_media($o,$medias = array()) {
    
        if ($o === null || !$o->valid()) return null;
        
        foreach ($o as $k => $i) {
            
            $i     = Translate::item('news',$i);
            
            $x[$k] = $i;      
            
            //$uri   = Data::get_category_uri('news_categories',$i->news_category_id); 
            
            //$x[$k]->uri = Translate::uri(Language::$current,Language::$current."/news/$uri/$i->permalink"."-detail");
            
            if (!empty($medias)) {
                foreach ($medias as $type) {
                    $gm = Model::factory('Medialibrary')->simple_application($type,$i->id,'news')->current();
                    if (!empty($gm)) $medLib[$type] = $gm; 
                }
                
                $kn = "media_library";
                
                if (!empty($medLib)) $x[$k]->$kn = $medLib;
                unset($medLib);
            }
        }
        
        return count($x) == 1 ? $x[0] : $x; 
    }
    
    public function xdata($limit) {
        return $this->select('nw.*')
                    ->from(array('news','nw'))
                    //->join(array('news_categories','cat'))
                    //->on('cat.id = news_category_id')
                    ->where('nw.is_active = 1')
                    ->order_by('nw.date','DESC')
                    //->and_where('cat.is_active = 1')
                    ->limit($limit);
    }
    
    public function feature($limit=5,$medias = array()) {
        $f = $this->xdata($limit)->and_where('nw.is_feature = 1')->execute();
        
        return $this->append_media($f,$medias);
    }
    
    
    public function latest($limit=5,$medias = array()) {
        $f = $this->xdata($limit)->execute();
        
        return $this->append_media($f,$medias);
    }
    
    /**
    public function item($permalink,$medias = array()) {
        if (!empty(App::$module->language))
        {
            $acLang = Request::$initial->param('lang');
            $dfLang = Language::$default;
        }
       
        $f = $this->xdata(1)->and_where("nw.permalink = $permalink")->execute();  
       
        $f = $this->append_media($f,$medias);  
        
        if (isset($acLang)  && $acLang != $dfLang)
        {
            if (!empty($f) && $f->permalink != $permalink)
            {
                return null;
            }
        }
        
        return $f;
    }
    **/
    
    
    public function xdata2($limit) {
        return $this->select('nw.*')
                    ->from(array('news','nw'))
                    ->where('nw.is_active','=','1')
                    ->order_by('nw.date','DESC')
                    ->limit($limit);
    }
    
    public function item($permalink,$medias = array()) {
        if (!empty(App::$module->language))
        {
            $acLang = Request::$initial->param('lang');
            $dfLang = Language::$default;
            $sLang  = Language::$system;
        }
       
        $f = clone $this->xdata2(1)->_instance;
        
        $f = $f->as_object()->and_where("nw.permalink","=",$permalink);
        
        if (isset($acLang)  && $acLang != $sLang)
        {
            $c = clone $f;
            
            $c->join(array('translations','t'),'left')
              ->on('t.row_id','=','nw.id')
              ->on('t.row_column','=',DB::expr("'permalink'"))
              ->and_where('t.row_table','=','news')
              ->or_where_open()
              ->where('t.row_value','=',$permalink)
              ->or_where_close();
             
            $d = clone $c; 
            $d = $d->execute()->current();
            
            if (!empty($d))
            {
                $f = $c;
            }
        }
        
        $f = $f->execute();
        
        if ($f->valid())
        {
            $df = $f[0];
        }  
               
        $f = $this->append_media($f,$medias);  
        
        if (!empty($f)) $f->default_permalink = $df->permalink; 
        
        if (isset($acLang)  && $acLang != $dfLang)
        {
            if (!empty($f) && $f->permalink != $permalink)
            {
                return null;
            } 
        }
        
        return $f;
    }
}