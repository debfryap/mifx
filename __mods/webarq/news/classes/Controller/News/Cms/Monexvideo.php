<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_News_Cms_MonexVideo extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'monex-video';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('news',array(
                            'create-monex-video','edit-monex-video',
                            'delete-monex-video','publish-monex-video'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('news_monexvideo');
        
        //HTML table style listing
        $listing->style('table');
        
        //Default listing ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('label')->set_header('source',array('label'=>'youtube-link'));        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('news','create-monex-video')) {
            $listing->create_action(__('create-monex-video'));
        }
        
        if (User::authorise('news','edit-monex-video')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('news','delete-monex-video')) {
            $delete_link = URL::cms().'helper/ajax/delete/monex_videos/{id}/{label}';
                
            $listing->delete_action(array(
                'ajax'      => true,                
                'href'      => $delete_link
            ));
        }
        
        if (User::authorise('news','publish-monex-video')) {
            $listing->status_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/monex_videos/{id}/{label}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-monex-video')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //CK Editor
        $this->media_ckeditor();
        
        //Date time picker
        $this->media_datetimepicker();
        
        //Check for authorisation
        $permission = $this->param1."-monex-video";
        $this->authorise('news',$permission);
        
        //Support variable        
        $title = __('create-monex-video');
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('news/cms/frm-monex-video')
                    ->set('title',$title)
                ;   
    }
}