<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_News_Cms_ListUpdate extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'news-update';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('news',array(
                            'create-news-update','edit-news-update',
                            'delete-news-update','publish-news-update'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('news_listupdate');
        
        //HTML table style listing
        $listing->style('table');
        
        //Default ordering
        $listing->order_by('date','ASC');
        
        //Enable traversing listing
        //$listing->traversing();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink')->set_header('date');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('news','create-news-update')) {
            $listing->create_action(__('create-update'));
        }
        
        if (User::authorise('news','edit-news-update')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('news','delete-news-update')) {
            $delete_link = URL::cms().'helper/ajax/delete/news_updates/{id}/{label}';
                
            $listing->delete_action(array(
                'ajax'      => true,                
                'href'      => $delete_link
            ));
        }
        
        if (User::authorise('news','publish-news-update')) {
            $listing->status_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/news_updates/{id}/{label}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-update')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //CK Editor
        $this->media_ckeditor();
        
        //Date time picker
        $this->media_datetimepicker();
        
        //Check for authorisation
        $permission = $this->param1."-news-update";
        $this->authorise('news',$permission);
        
        //Support variable        
        $title = __('create-update');
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('news/cms/frm-update')
                    ->set('title',$title)
                ;   
    }
}