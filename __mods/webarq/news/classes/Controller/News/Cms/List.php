<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_News_Cms_List extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'news-list';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('news',array(
                            'create-news','edit-news',
                            'delete-news','publish-news'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('news_list');
        
        //HTML table style listing
        $listing->style('table');
        
        //Default ordering
        $listing->order_by('date','DESC');
        
        //Enable traversing listing
        //$listing->traversing();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink')->set_header('date');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('news','create-news')) {
            $listing->create_action(__('create-news'));
        }
        
        if (User::authorise('news','edit-news')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('news','delete-news')) {
            $delete_link = URL::cms().'helper/ajax/delete/news/{id}/{label}';
            
            if (News::$config->use_category === true)
                $delete_link .= '&news_category_id={news_category_id}';
                
            $listing->delete_action(array(
                'ajax'      => true,                
                'href'      => $delete_link
            ));
        }
        
        if (User::authorise('news','publish-news')) {
            $listing->status_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function_publish}/news/{id}/{label}'
            ));
        }
        
        if (User::authorise('news','feature-news')) {
            $listing->feature_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function_feature}/news/{id}/{label}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-news')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //CK Editor
        $this->media_ckeditor();
        
        //Date time picker
        $this->media_datetimepicker();
        
        //Check for authorisation
        $permission = $this->param1."-news";
        $this->authorise('news',$permission);
        
        //Support variable        
        $title = __('create-news');
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('news/cms/frm-news')
                    ->set('title',$title)
                ;   
    }
    
    public function action_hidden()
    {
        
        $this->register('title','Hidden');
        
        $content = '';
        
        $gets = DB::select('row_value','row_id','language_code')->from('translations')->where('row_table','=','news')->and_where('row_column','=','label')->execute()->as_array();
        
        if (!empty($gets))
        {
            //DB::delete('translations')->where('row_table','=','news')->and_where('row_column','=','permalink')->execute();
            foreach ($gets as $get)
            {
                $ins = DB::insert('translations',array('row_table','row_id','row_column','row_value','language_code'))
                       ->values(array('translations',$get['row_id'],'permalink',Helper_Kernel::name($get['row_value']),$get['language_code']));
                
                $content .= $ins . '<br/>'; 
            }
        }
        
        $this->register('content',$content);
    }
}