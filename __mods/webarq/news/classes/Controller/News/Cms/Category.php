<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_News_Cms_Category extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'news-category';
    }
    
    public function action_index() {
        
        if (empty(News::$config->use_category)) {
            $this->authorise(false);
            return;
        };
        
        //Check for authorisation
        $this->authorise('news',array(
                            'create-category','edit-category',
                            'delete-category','publish-category'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('news_category');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        $listing->traversing();
        
        //Default ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('news','create-category')) {
            $listing->create_action(__('create-news-category'));
        }
        
        if (User::authorise('news','edit-category')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('news','delete-category')) {
            $listing->delete_action(array(
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/news_categories/{id}/{label}?ordering={ordering}&parent={parent_id}'
            ));
        }
        
        if (User::authorise('news','publish-category')) {
            $listing->status_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/news_categories/{id}/{label}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-news-category')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        
        if (empty(News::$config->use_category)) {
            $this->authorise(false);
            return;
        };
        
        //Check for authorisation
        $permission = $this->param1."-category";
        $this->authorise('news',$permission);
        
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //Support variable        
        $title = __('create-news-category');
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('news/cms/frm-category')
                    ->set('title',$title)
                ;   
    }
}