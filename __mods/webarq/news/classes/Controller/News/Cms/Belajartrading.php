<?php defined('SYSPATH') or die('No direct script access.'); 

 

/**

 * @Author 		Daniel Simangunsong

 * @Company		Webarq

 * @copyright 	2012

 * @Package	    Controller (For Application Administrator) 

 * @Module      News

 * @License		Kohana ~ Webarq ~ Daniel Simangunsong

 * 

 * Calm seas, never make skillfull sailors	

**/ 





class Controller_News_Cms_BelajarTrading extends Controller_Default_Template_Cms {

    

    public function before() {

        parent::before();

        

        //Active navigation based on label

        App::$config->menu->active = 'belajar-trading';

    }

    

    public function action_index() {

        //Check for authorisation

        $this->authorise('news',array(

                            'create-belajar-trading','edit-belajar-trading',

                            'delete-belajar-trading','publish-belajar-trading'));

        

        //Append css styles

        $this->media_tree_tabular();

        

        //Open listing using model

        $listing = Data::listing('news_belajartrading');

        

        //HTML table style listing

        $listing->style('table');

        

        //Default listing ordering

        $listing->order_by();

        

        //Listing header

        $listing->set_header('label')->set_header('source',array('label'=>'youtube-link'));        

        

        //Listing action, we have several ways to make this fun

        if (User::authorise('news','create-belajar-trading')) {

            $listing->create_action(__('create-belajar-trading'));

        }

        

        if (User::authorise('news','edit-belajar-trading')) {

            $listing->edit_action(array(

                'href'      => 'form/edit/{id}'

            ));

        }

        

        if (User::authorise('news','delete-belajar-trading')) {

            $delete_link = URL::cms().'helper/ajax/delete/belajar_trading/{id}/{label}';

                

            $listing->delete_action(array(

                'ajax'      => true,                

                'href'      => $delete_link

            ));

        }

        

        if (User::authorise('news','publish-belajar-trading')) {

            $listing->status_action(array(

                'ajax'      => true,

                'href'      => URL::cms().'helper/ajax/{function}/belajar_trading/{id}/{label}'

            ));

        }

        

        //Assign to template

        $this

            ->register('title')->use_string(__('manage-belajar-trading')) 

            ->register('content')->use_string($listing->render())

                ;

    }

    

    public function action_form() {

        //Activate media validate

        $this->media_validate();

        

        //Load media script

        $this->media_header();

        

        //CK Editor

        $this->media_ckeditor();

        

        //Date time picker

        $this->media_datetimepicker();

        

        //Check for authorisation

        $permission = $this->param1."-belajar-trading";

        $this->authorise('news',$permission);

        

        //Support variable        

        $title = __('create-belajar-trading');

        

        //Assign to template

        $this

            ->register('title')

                ->use_string($title)

            ->register('content')

                ->use_view('news/cms/frm-belajar-trading')

                    ->set('title',$title)

                ;   

    }

}