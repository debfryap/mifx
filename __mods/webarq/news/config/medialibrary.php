<?php defined('SYSPATH') or die('No direct script access.');

return array  (
    'default' => array (
        'media_library_application_types'  => array (
            'image'         => array (
                'news-thumbnail' => array (
                    'label'     => 'image-news-thumbnail',
                    'width'     => 220,
                    'height'    => 145,
                    'limit'     => 1
                ),

                 
                'image-monex-schedule' => array (
                    'label'     => 'image-monex-schedule',
                    'width'     => 260,
                    'limit'     => 1
                ),
                 'image-branch-event' => array (
                    'label'     => 'image-branch-event',
                    'width'     => 260,
                    'limit'     => 1
                )
            )
        ) 
    )
);            