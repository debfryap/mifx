<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$is_superadmin = empty(User::admin()->id)   ? null : (User::admin()->role_id != 1 ? false : true);

if (News::$config->use_category === true) {
    $news_installer['news_categories'] = array (
        array (
            "master" => "id"
        ),
        array (
            "type"   => "int",
            "field"  => "parent_id",
            "form"   => array (
                "label"     => "parent-news-category",
                "group"     => array('create-category','edit-category'),
                "traverse"  => array (
                    "field"     => "parent_id",
                    "value"     => "id",
                    "permalink" => true,
                    "label"     => "label" 
                )
            )
        ),
        array (
            "master" => "label",
            "form"   => array (
                "group"       => array('create-category','edit-category'),
                "class"       => "required is_used",
                "multilang"   => true,
                "minlength"   => "3",
                "referrer"    => array('permalink',false)
            )
        ),
        array (
            "master" => "label",
            "field"  => "permalink",
            "form"   => array (
                "group"       => array('create-category','edit-category'),
                "class"       => "required permalink",
                "minlength"   => "3",
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group"       => array('create-category','edit-category')
            )
        ),
        array (
            "master"    => "ordering",
            "form"   => array (
                "group"     => array('create-category','edit-category'),
                "type"      => "ordering",
                "parenting" => "parent_id",
                "class"     => "required",
                "order"     => "after permalink"
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    );
    
    $news_category = array (
        "type"   => "int",
        "field"  => "news_category_id",
        "default"=> 0,
        "form"   => array (
            "label"     => "news-category-id",
            "group"     => array('create-news','edit-news'),
            "class"     => "required",
            "traverse"  => array (
                "table"     => "news_categories",
                "field"     => "parent_id",
                "value"     => "id",
                "label"     => "label",
                "top_level" => false,
                "permalink" => true 
            )
        ) 
    );
} else {
    $news_category = null;
}

$news_installer['news'] = array (
    array (
        "master" => "id",
    ),
    $news_category,
    array(
        "master"    => "label",
        "form"   => array (
            "group"         => array('create-news','edit-news'),
            "class"         => "required",
            "multilang"     => true,
            "referrer"      => array('permalink',$is_superadmin ? 'permalink' : false),
            "multireferrer" => true,
            "order"         => "first"
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-news',$is_superadmin ? 'edit-news' : '---'),
            "multilang" => true,
            "class"     => "permalink required"
        ) 
    ),
    array (
        "type"   => "date",
        "field"  => "date",
        "form"   => array (
            "group"     => array('create-news','edit-news'),
            "class"     => "datepicker required",
            "type"      => "text"
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "meta_title",
        "form"   => array (
            "label"     => 'Meta Title',
            "group"     => array('create-news','edit-news'),
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "meta_description",
        "form"   => array (
            "label"     => 'Meta Description',
            "group"     => array('create-news','edit-news'),
            "type"      => 'textarea',
            "cols"      => 50,
            "maxlength" => 160,
            "rows"      => 10
        ) 
    ),
    array (
        "field"     => "intro",
        "type"      => "mediumtext",
        "form"   => array (
            "group"     => array('create-news','edit-news'),
            "class"     => "simple-editor",
            "multilang" => true
        ) 
    ),
    array (
        "field"     => "description",
        "type"      => "text",
        "form"   => array (
            "group"     => array('create-news','edit-news'),
            "class"     => "full-editor",
            "multilang" => true
        ) 
    ),
    array (
        "master" => "is_something",
        "field"  => "is_feature",
        "default" => 0,
        "form"   => array (
            "label"     => "featured-news",
            "group"     => array('create-news','edit-news')
        )
    ),
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 0,
        "form"   => array (
            "group"     => array('create-news','edit-news')
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

/**
 * installer news event monex
 */
$news_installer['news_events'] = array (
    array (
        "master" => "id",
    ),
    array(
        "master"    => "label",
        "form"   => array (
            "group"       => array('create-event','edit-event'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',$is_superadmin ? 'permalink' : false),
            "order"       => "first"
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-event',$is_superadmin ? 'edit-event' : '---'),
            "class"     => "permalink required"
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "meta_title",
        "form"   => array (
            "label"     => 'Meta Title',
            "group"     => array('create-event','edit-event'),
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "meta_description",
        "form"   => array (
            "label"     => 'Meta Description',
            "type"      => 'textarea',
            "cols"      => 50,
            "rows"      => 10,
            "group"     => array('create-event','edit-event'),
        ) 
    ),
    array (
        "type"   => "date",
        "field"  => "date",
        "form"   => array (
            "group"     => array('create-event','edit-event'),
            "class"     => "datepicker required",
            "type"      => "text"
        ) 
    ),
    array (
        "field"     => "intro",
        "type"      => "mediumtext",
        "form"   => array (
            "group"     => array('create-event','edit-event'),
            "class"     => "simple-editor",
            "multilang" => true
        ) 
    ),
    array (
        "field"     => "description",
        "type"      => "text",
        "form"   => array (
            "group"     => array('create-event','edit-event'),
            "class"     => "full-editor",
            "multilang" => true
        ) 
    ),
    /**
    array (
        "master" => "is_something",
        "field"  => "is_feature",
        "default" => 0,
        "form"   => array (
            "label"     => "featured-event",
            "group"     => array('create-event','edit-event')
        )
    ),
    **/
    array (
        "master" => "is_something",
        "field"  => "is_active",
        " " => 1,
        "form"   => array (
            "group"     => array('create-event','edit-event'),
            "default"   => 1
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);


/**
 * installer news update monex
 */
$news_installer['news_updates'] = array (
    array (
        "master" => "id",
    ),
    array(
        "master"    => "label",
        "form"   => array (
            "group"       => array('create-update','edit-update'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',$is_superadmin ? 'permalink' : false),
            "order"       => "first"
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-update',$is_superadmin ? 'edit-update' : '---'),
            "class"     => "permalink required"
        ) 
    ),
    array (
        "type"   => "date",
        "field"  => "date",
        "form"   => array (
            "group"     => array('create-update','edit-update'),
            "class"     => "datepicker required",
            "type"      => "text"
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "meta_title",
        "form"   => array (
            "label"     => 'Meta Title',
            "group"     => array('create-update','edit-update'),
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "meta_description",
        "form"   => array (
            "label"     => 'Meta Description',
            "group"     => array('create-update','edit-update'),
            "type"      => 'textarea',
            "cols"      => 50,
            "rows"      => 10
        ) 
    ),
    array (
        "field"     => "intro",
        "type"      => "mediumtext",
        "form"   => array (
            "group"     => array('create-update','edit-update'),
            "class"     => "simple-editor",
            "multilang" => true
        ) 
    ),
    array (
        "field"     => "description",
        "type"      => "text",
        "form"   => array (
            "group"     => array('create-update','edit-update'),
            "class"     => "full-editor",
            "multilang" => true
        ) 
    ),
    /**
    array (
        "master" => "is_something",
        "field"  => "is_feature",
        "default" => 0,
        "form"   => array (
            "label"     => "featured-update",
            "group"     => array('create-update','edit-update')
        )
    ),
    **/
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 0,
        "form"   => array (
            "group"     => array('create-update','edit-update')
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

$news_installer['monex_schedules'] = array (
    array (
        "master" => "id",
    ),
    array(
        "master"    => "label",
        "form"   => array (
            "group"       => array('create-monex-schedule','edit-monex-schedule'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',$is_superadmin ? 'permalink' : false),
            "order"       => "first"
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-monex-schedule',$is_superadmin ? 'edit-monex-schedule' : '---'),
            "class"     => "permalink required"
        ) 
    ),
    array (
        "type"   => "date",
        "field"  => "date",
        "form"   => array (
            "group"     => array('create-monex-schedule','edit-monex-schedule'),
            "class"     => "datepicker required",
            "type"      => "text"
        ) 
    ),
    array (
        "type"   => "time",
        "field"  => "start",
        "form"   => array (
            "label"     => "start-hour",
            "group"     => array('create-monex-schedule','edit-monex-schedule'),
            "class"     => "required timepicker",
            "type"      => "text",
            "style"     => "width:60px;"
        ) 
    ),
    array (
        "type"   => "time",
        "field"  => "end",
        "form"   => array (
            "label"     => "end-hour",
            "group"     => array('create-monex-schedule','edit-monex-schedule'),
            "class"     => "required timepicker",
            "type"      => "text",
            "style"     => "width:60px;"
        ) 
    ),
    array (
        "type"   => "varchar",
        "field"  => "closing_info",
        "form"   => array (
            "group"     => array('create-monex-schedule','edit-monex-schedule'),
            "class"     => "required",
            "multilang" => true
        ) 
    ),
    array (
        "type"   => "enum",
        "field"  => "region",
        "option" => 'WIB,WIT,WITA',
        "form"   => array (
            "group"     => array('create-monex-schedule','edit-monex-schedule'),
            "class"     => "required"
        ) 
    ),
    array (
        "type"   => "text",
        "field"  => "description",
        "form"   => array (
            "group"     => array('create-monex-schedule','edit-monex-schedule'),
            "class"     => "simple-editor",
            'multilang' => true
        ) 
    ),
    array (
        "type"   => "int",
        "field"  => "registration_user"
    ), 
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 0,
        "form"   => array (
            "group"     => array('create-monex-schedule','edit-monex-schedule')
        )
    ),
    array (
        "master" => "is_something",
        "field"  => "is_feature",
        "default" => 0,
        "form"   => array (
            "label"     => "is_feature_schecule",
            "group"     => array('create-monex-schedule','edit-monex-schedule')
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

$news_installer['monex_videos'] = array (
    array (
        "master" => "id",
    ),
    array(
        "master"    => "label",
        "form"   => array (
            "group"       => array('create-monex-video','edit-monex-video'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',$is_superadmin ? 'permalink' : false),
            "order"       => "first",
            "style"       => "width:500px",
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-monex-video',$is_superadmin ? 'edit-monex-video' : '---'),
            "class"     => "permalink required",
            "type"      => "hidden",
            "multilang"   => true
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "source",
        "form"   => array (
            "label"     => "youtube-link",
            "group"     => array('create-monex-video',$is_superadmin ? 'edit-monex-video' : '---'),
            "class"     => "required",
            "info"      => "Please enter your youtube video id",
            "style"     => "width:500px",
        ) 
    ),
    array (
        "field"     => "description",
        "type"      => "text",
        "form"   => array (
            "group"     => array('create-monex-video','edit-monex-video'),
            "class"     => "full-editor",
            "multilang"   => true
        ) 
    ),
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 1,
        "form"   => array (
            "group"     => array('create-monex-video','edit-monex-video')
        )
    ),
    array (
        "master" => "is_something",
        "field"  => "is_feature",
        "default" => 0,
        "form"   => array (
            "label"     => "is_feature_video",
            "group"     => array('create-monex-video','edit-monex-video')
        )
    ),
    array (
        "master"    => "ordering",
        "form"   => array (
            "group"     => array('create-monex-video','edit-monex-video'),
            "type"      => "ordering",
            "class"     => "required"
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

// Branch Events
$news_installer['branch_events'] = array (
    array (
        "master" => "id",
    ),
    array(
        "master"    => "label",
        "form"   => array (
            "group"       => array('create-brach-events','edit-brach-events'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',$is_superadmin ? 'permalink' : false),
            "order"       => "first"
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-brach-events',$is_superadmin ? 'edit-brach-events' : '---'),
            "class"     => "permalink required"
        ) 
    ),
    array (
        "type"   => "date",
        "field"  => "date",
        "form"   => array (
            "group"     => array('create-brach-events','edit-brach-events'),
            "class"     => "datepicker required",
            "type"      => "text"
        ) 
    ),
    array (
        "type"   => "time",
        "field"  => "start",
        "form"   => array (
            "label"     => "start-hour",
            "group"     => array('create-brach-events','edit-brach-events'),
            "class"     => "required timepicker",
            "type"      => "text",
            "style"     => "width:60px;"
        ) 
    ),
    array (
        "type"   => "time",
        "field"  => "end",
        "form"   => array (
            "label"     => "end-hour",
            "group"     => array('create-brach-events','edit-brach-events'),
            "class"     => "required timepicker",
            "type"      => "text",
            "style"     => "width:60px;"
        ) 
    ),
    array (
        "type"   => "varchar",
        "field"  => "location",
        "form"   => array (
            "group"     => array('create-brach-events','edit-brach-events'),
            "class"     => "required",
            "label"     => "location"
        ) 
    ),
    array (
        "type"   => "enum",
        "field"  => "region",
        "option" => 'WIB,WIT,WITA',
        "form"   => array (
            "group"     => array('create-brach-events','edit-brach-events'),
            "class"     => "required"
        ) 
    ),
    array (
        "type"   => "text",
        "field"  => "description",
        "form"   => array (
            "group"     => array('create-brach-events','edit-brach-events'),
            "class"     => "simple-editor",
            'multilang' => true
        ) 
    ),
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 0,
        "form"   => array (
            "group"     => array('create-brach-events','edit-brach-events')
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

// Branch Events
$news_installer['belajar_trading'] = array (
    array (
        "master" => "id",
    ),
    array(
        "master"    => "label",
        "form"   => array (
            "group"       => array('create-belajar-trading','edit-belajar-trading'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',$is_superadmin ? 'permalink' : false),
            "order"       => "first",
            "style"       => "width:500px",
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-belajar-trading',$is_superadmin ? 'edit-belajar-trading' : '---'),
            "class"     => "permalink required",
            "type"      => "hidden",
            "multilang"   => true
        ) 
    ),
    array (
        "master" => "label",
        "field"  => "source",
        "form"   => array (
            "label"     => "youtube-link",
            "group"     => array('create-belajar-trading',$is_superadmin ? 'edit-belajar-trading' : '---'),
            "class"     => "required",
            "info"      => "Please enter your youtube video id",
            "style"     => "width:500px",
        ) 
    ),
    array (
        "field"     => "description",
        "type"      => "text",
        "form"   => array (
            "group"     => array('create-belajar-trading','edit-belajar-trading'),
            "class"     => "full-editor",
            "multilang"   => true
        ) 
    ),
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 1,
        "form"   => array (
            "group"     => array('create-belajar-trading','edit-belajar-trading')
        )
    ),
    /*array (
        "master" => "is_something",
        "field"  => "is_feature",
        "default" => 0,
        "form"   => array (
            "label"     => "is_feature_video",
            "group"     => array('create-belajar-trading','edit-belajar-trading')
        )
    ),*/
    array (
        "master"    => "ordering",
        "form"   => array (
            "group"     => array('create-belajar-trading','edit-belajar-trading'),
            "type"      => "ordering",
            "class"     => "required"
        )
    ),
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

return $news_installer;