<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Dummy data
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$news_module_navigations = array(
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "news-list",
            "permalink"     => "list",
            "ordering"      => 1,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "news-event",
            "permalink"     => "list-event",
            "ordering"      => 2,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "news-update",
            "permalink"     => "list-update",
            "ordering"      => 3,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "monex-schedule",
            "permalink"     => "monex-schedule",
            "ordering"      => 4,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "monex-video",
            "permalink"     => "monex-video",
            "ordering"      => 5,
            "create_time"   => "now"
        ),
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "branch-events",
            "permalink"     => "branch-events",
            "ordering"      => 6,
            "create_time"   => "now"
        ),

    );

$news_module_navigation_group_maps =  array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-list' AND `permalink` = 'list' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-event' AND `permalink` = 'list-event' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-update' AND `permalink` = 'list-update' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-schedule' AND `permalink` = 'monex-schedule' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-video' AND `permalink` = 'monex-video' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='branch-events' AND `permalink` = 'branch-events' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        )
    );

$news_module_permissions = array ( 
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-news",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-news",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-news",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-news",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "feature-news",
            "create_time"   => "now"                    
        ),


        // create permission event
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-news-event",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-news-event",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-news-event",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-news-event",
            "create_time"   => "now"                    
        ),

        // create permission update
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-news-update",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-news-update",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-news-update",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-news-update",
            "create_time"   => "now"                    
        ),

        // create permission monex schedule
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-monex-schedule",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-monex-schedule",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-monex-schedule",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-monex-schedule",
            "create_time"   => "now"                    
        ),

        // create permission monex video
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-monex-video",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-monex-video",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-monex-video",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-monex-video",
            "create_time"   => "now"                    
        ),

        // create permission brach events
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-brach-events",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-brach-events",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-brach-events",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-brach-events",
            "create_time"   => "now"                    
        ),
    );

$news_module_navigation_permission_maps = array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-news' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-news' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-news' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-news' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'feature-news' LIMIT 1",
            "create_time"    => "now" 
        ),

        // create permission maps event
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-event' AND `permalink` = 'list-event' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-news-event' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-event' AND `permalink` = 'list-event' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-news-event' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-event' AND `permalink` = 'list-event' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-news-event' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-event' AND `permalink` = 'list-event' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-news-event' LIMIT 1",
            "create_time"    => "now" 
        ),

        // create permission maps update
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-update' AND `permalink` = 'list-update' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-news-update' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-update' AND `permalink` = 'list-update' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-news-update' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-update' AND `permalink` = 'list-update' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-news-update' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-update' AND `permalink` = 'list-update' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-news-update' LIMIT 1",
            "create_time"    => "now" 
        ),

        // create permission maps monex schedule
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-schedule' AND `permalink` = 'monex-schedule' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-monex-schedule' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-schedule' AND `permalink` = 'monex-schedule' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-monex-schedule' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-schedule' AND `permalink` = 'monex-schedule' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-monex-schedule' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-schedule' AND `permalink` = 'monex-schedule' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-monex-schedule' LIMIT 1",
            "create_time"    => "now" 
        ),

        // create permission maps monex video
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-video' AND `permalink` = 'monex-video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-monex-video' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-video' AND `permalink` = 'monex-video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-monex-video' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-video' AND `permalink` = 'monex-video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-monex-video' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='monex-video' AND `permalink` = 'monex-video' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-monex-video' LIMIT 1",
            "create_time"    => "now" 
        ),

        // create permission maps branch events
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='branch-events' AND `permalink` = 'branch-events' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-brach-events' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='branch-events' AND `permalink` = 'branch-events' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-brach-events' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='branch-events' AND `permalink` = 'branch-events' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-brach-events' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='branch-events' AND `permalink` = 'branch-events' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-brach-events' LIMIT 1",
            "create_time"    => "now" 
        ),
    );
    
$news_categories = null;

if (News::$config->use_category === true) {
    array_push(
        $news_module_navigations,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news'",
            "label"         => "news-category",
            "permalink"     => "category",
            "ordering"      => 1,
            "create_time"   => "now"
        )
    );
    array_push(
        $news_module_navigation_group_maps,
        array (
                "id"             => "auto",
                "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-category' AND `permalink` = 'category' LIMIT 1",
                "module_navigation_group_id"  => 1,
                "create_time"   => "now" 
            )
    );
    array_push(
        $news_module_permissions,        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "create-category",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "edit-category",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "delete-category",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1",
            "permission"    => "publish-category",
            "create_time"   => "now"                    
        )
    );
    array_push(
        $news_module_navigation_permission_maps,
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-category' AND `permalink` = 'category' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'create-category' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-category' AND `permalink` = 'category' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'edit-category' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-category' AND `permalink` = 'category' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'delete-category' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='news-category' AND `permalink` = 'category' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'news' LIMIT 1) AND `permission` = 'publish-category' LIMIT 1",
            "create_time"    => "now" 
        )
    );
    
    $news_categories = array (
        array (
            "id"                => 1,
            "parent_id"         => 0, 
            "label"             => "Sport",
            "permalink"         => "sport",
            "is_active"         => 1,
            "ordering"          => 1,
            "create_time"       => "now"
        ),
        array (
            "id"                => 2,
            "parent_id"         => 0, 
            "label"             => "Technology",
            "permalink"         => "technology",
            "is_active"         => 1,
            "ordering"          => 2,
            "create_time"       => "now"
        ), 
        array (
            "id"                => 3,
            "parent_id"         => 2, 
            "label"             => "Space Bound",
            "permalink"         => "space-bound",
            "is_active"         => 1,
            "ordering"          => 1,
            "create_time"       => "now"
        ),
        array (
            "id"                => 4,
            "parent_id"         => 2, 
            "label"             => "Wi Robo",
            "permalink"         => "wi-robo",
            "is_active"         => 1,
            "ordering"          => 2,
            "create_time"       => "now"
        )
    );
}

return array (    
    //Do not delete this section
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "news",
            "ordering"      => "total-row",
            "is_publish"    => 1,
            "description"   => "News Modules",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations"                => $news_module_navigations,
    "module_navigation_group_maps"      => $news_module_navigation_group_maps,    
    "module_permissions"                => $news_module_permissions,
    "module_navigation_permission_maps" => $news_module_navigation_permission_maps,   
    
    //Frontend section 
    "news_categories"                   => $news_categories,    
    "news" => array (
        array (
            "id"                => 1,
            "news_category_id"  => News::$config->use_category === true ? 4 : false,
            "label"             => "Robotica Capsul Launched",
            "permalink"         => "robotica-capsul-launched",
            "date"              => App::date(0),
            "intro"             => "It has been waiting for 7 years, and finally we can relax now and proudly to pronounce that Robotica Capsul will be launched in this quartal opening said Mr. Pulechenko",
            "description"       => ".....",
            "is_active"         => 1,
            "create_time"       => "now"
        )
    )
);

?>