<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Item ~ CMS
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

echo Data::form(array(
    'action'    => false,
    'media'     => array('branch_events',array('image:image-branch-event')),
    'title'     => $title,
    'id'        => 'form_branch_events',
    'multilang' => true,  
    'pack'      => array ('news:'.Request::$initial->param('param1').'-brach-events'),
    'type'      => strtolower(Request::$initial->param('param1')),
    'row_id'    => array('id',Request::$initial->param('param2')),
));
?>