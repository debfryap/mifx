<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Category ~ CMS
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    echo Data::form(array(
        'action'    => false,
        'media'     => array('news_categories',array('image:news-category','image:slide')),
        'title'     => $title,
        'id'        => 'form_news_category',
        'multilang' => true,  
        'pack'      => array ('news:'.Request::$initial->param('param1').'-category'),
        'type'      => strtolower(Request::$initial->param('param1')),
        'row_id'    => array('id',Request::$initial->param('param2')),
    ));
?>