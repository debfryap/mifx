<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Item ~ CMS
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    echo Data::form(array(
        'action'    => false,
        //'media'     => array('news_updates',array('image:news-thumbnail','image:news-large')),
        'title'     => $title,
        'id'        => 'form_belajar_trading',
        'multilang' => true,  
        'pack'      => array ('news:'.Request::$initial->param('param1').'-belajar-trading'),
        'type'      => strtolower(Request::$initial->param('param1')),
        'row_id'    => array('id',Request::$initial->param('param2')),
    ));
?>