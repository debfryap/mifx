<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Translator Handler
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Kohana_Language {
    /** System Default Language **/
    public static $system;
    
    /** Current Used Language **/
    public static $current;
    
    /** Default Application Language **/
    public static $default;
    
    /** Registered Language **/
    public static $active;
    
    public static function factory() {
        
        Language::$system  = 'id';
        
        Language::$active  = Model::factory('Language')->mode('mini')->get_active();
        
        Language::$default = !empty(Language::$active->current()->code) 
                                    ? Language::$active->current()->code 
                                    : Language::$system;
    }
}