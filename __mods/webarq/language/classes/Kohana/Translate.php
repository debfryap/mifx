<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Translator
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Kohana_Translate extends Populate{
    // Translation table
    public static $_table = 'translations';
        
    
    public static function delete($row_table,$row_id,$execute=true) {
        $builder = Model::factory('Dba')
                        ->delete(Translate::$_table)
                        ->where("row_table = $row_table")
                        ->and_where("row_id = $row_id");
            
        return $execute === true ? $builder->execute() : $builder;                  
    }
    
    /**
     * @var string table name
     * @var mixed  
     *      int    table row id
     *      array  (table field id,table row id)
     * @var mixed
     *      string language code
     *      array  field to select
     * @var array  field to select
     * auto translation table
     */
    public static function table($row_table,$row_id=null,$lang=null,$fields=null) {
        if (!isset($lang)) {
            $lang = Language::$current;
        }
        
        if (is_array($lang)) {
            $fields = $lang;
            $lang   = Language::$current;
        }
        
        if (isset($row_id)) {
            $x_fd = is_array($row_id) ? $row_id[0] : (is_numeric($row_id) ? 'id' : $row_id);
            $x_id = is_array($row_id) ? $row_id[0] : (is_numeric($row_id) ? $row_id : null);
        } else {
            $x_fd = 'id';
        }
        
        //Set default field
        if (!isset($fields))
            $fields = '*';
        
        //Make sure table is plurar
        $row_table = Inflector::plural($row_table);
        
        //Get item
        $items = Model::factory('Dba')->select($fields)->from($row_table);
        
        if (isset($x_id)) {
            $items->where("$x_fd = $x_id");
        }
        
        $items = $items->execute();
        
        if (empty($items[0]))
            return null;
                
        foreach ($items as $index => $item) {        
            $translation = Model::factory('Language')->translate($row_table,$item->id,$lang);
        
            if (!empty($translation[0])) {
                foreach ($translation as $t_item) {
                    if (is_array($item)) {
                        $item[$t_item->row_column] = $t_item->row_value;    
                    } elseif (is_object($item)) {
                        $item_column = $t_item->row_column;
                        $item->$item_column = $t_item->row_value;
                    }
                }
            }
            $results[$index] = $item;
        }
        
        return count($results) == 1 ? $results[0] : $results;
    }
    
    /**
     * @param mixed info 
     *      str   table name 
     *      array table name,field name
     * @param DB::select()->execute();
     */
    public static function item($table,$object,$filter = true, $set_db = null) {
        if (empty($object))
            return;
            
        if (!is_array($object) && !is_a($object,'Database_MySQL_Result'))
            $object = array($object);
        
        $t = new Translate(array(
                    'row_table'     => is_array($table) ? $table[0] : $table,
                    'row_field_id'  => is_array($table) ? $table[1] : 'id'
                ));
                
        return $t->do_translate($object,$filter, $set_db);                        
    } 
    
    /**
     * @var   string row table name
     */
    protected $row_table;
    
    /**
     * @var   numeric row table field id
     */
    protected $row_field_id;
    
    
    
    /**
     * @param   array(object) item to translate 
     */
    private function do_translate($object,$filter = true, $set_db = null) {
        if (!empty($object[0])) {
            foreach ($object as $index => $item) {     
                $row_field_id = $this->row_field_id;
                $row_id       = is_array($item) ? $item[$row_field_id] : $item->$row_field_id;
                
                $translation  = Model::factory('Language')->translate($this->row_table,$row_id,  Language::$current, $set_db );
                if (!empty($translation[0])) {
                    foreach ($translation as $t_item) {
                        if (is_array($item)) {                            
                            if (trim($item[$t_item->row_column]) != "") $item[$t_item->row_column] = $t_item->row_value;
                        } elseif (is_object($item)) {                            
                            $item_column = $t_item->row_column;
                            if (trim($t_item->row_value) != "") $item->$item_column = $t_item->row_value;
                        }
                    }
                }
                
                if (is_array($item)) {               
                    if (!empty($item['child'])) {
                       // $item['child'] = $this->do_translate($item['child'],$filter);
                       $this->do_translate($item['child'],$filter);
                    }  
                } elseif (is_object($item)) {    
                    if (!empty($item->child)) {
                        //$item->child = $this->do_translate($item->child,$filter);
                        $this->do_translate($item->child,$filter);
                    }  
                }
                
                $results[$index] = $item;
            }
        } else {
            return null;
        }
        return $index == 0 && $filter ? $results[$index] : $results;
    }
    
    public static function uri($lang,$uri=null) {
        $uri = !isset($uri) ? Request::$initial->uri() : $uri;
        $uri = trim($uri,'/');
        
        if ($uri == '' || $uri == Language::$current) {
            $href = $lang;
        } else {
            $href = strpos($uri,Language::$current) === 0 
                        ? ($lang == Language::$default && App::$show_default_language_in_uri === false
                                ? preg_replace("/".Language::$current."\//","",$uri,1)
                                : preg_replace("/".Language::$current."\//","$lang/",$uri,1)) 
                        : ($lang == Language::$default && App::$show_default_language_in_uri === false 
                                ? $uri 
                                : "$lang/$uri");
        }
        
        return URL::front($href);
    }
}