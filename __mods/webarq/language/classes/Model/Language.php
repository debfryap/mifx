<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Language extends Model_Dba {
    // Model table
    protected $table = 'languages';
    
    // Model mode
    protected $_mode = 'admin';
    
    protected function pre_select() {
        if ($this->_mode == 'admin')
            $x = $this->select();
        elseif ($this->_mode == 'mini')
            $x = $this->select('id','label','code');
        
        return $x->from($this->table);
    }
    
    public function mode($mode='admin') {
        $this->_mode = $mode; return $this;
    }
    
    public function get_active($execute = true) {
        $get = $this->pre_select()->where("is_active = 1");
        
        if ($execute) {
            $get = $get->order_by("ordering")->execute();
            return $get->valid() ? $get : null;   
        }
        
        return $get;
    }
    
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array($this->table,'l'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->select('l.*',
                            DB::expr("CASE l.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                      ->from(array($this->table,'l'));
        
        return $model;
    }
    
    public function translate($row_table,$row_id,$lang=null, $set_db = null) {
        $q = $this->select()
                    ->from('translations')
                    ->where("row_table = $row_table")
                    ->and_where("row_id = $row_id");
        
        if (isset($lang))
            $q->and_where("language_code = $lang");                    
                    
        return $q->execute($set_db);
    }
    /** End Data listing purpose **/
}