<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) (Language Manage) 
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Language_Cms_Manage extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('manage-language'));
        
        App::$config->menu->active = 'manage-language';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('language',array('create-language','edit-language','publish-language','view-language','delete-language'));
        
        //Append css styles
        $this->media_tabular();
        $this->styles(URL::libCss('jquery/themes/jquery.ui.all'));
        
        //Append js
        $this->media_sortable();
        
        
        //Open listing
        $listing = Data::listing('language');
        
        //Disable tool
        $listing->tool('sort_by',false);
        
        //Enable listing sortable
        $listing->sortable('languages');
        
        //Default ordering
        $listing->order_by('ordering');
        
        //Listing header
        $listing->set_header('label',array('label'=>__('language')))     
                ->set_header('code',array('class'=>'code'))
                ->set_header('is_active',array('label'=>__('is_active'),'class'=>'is-active'))
                ;        
        
        //Listing button
        if (User::authorise('language','create-language')) {
            $listing->create_action(__('create-language'));
        }
        
        if (User::authorise('language','edit-language')) {
            $listing->edit_action(array(
                'exception' => array('is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        } 
        //Another ways for listing button       
        $listing->action(array(
                "delete" => array(
                    'exception' => array('is_system'=>1),
                    "authorise" => array("language"=>"delete-language"),
                    'ajax'      => 'ajax',                
                    'href'      => URL::cms().'helper/ajax/delete/languages/{id}/{label}?is_system=0'
                ),
                "status" => array(
                    "exception" => array('is_system'=>1),
                    "authorise" => array("user"=>"publish-language"),
                    'ajax'      => 'ajax',
                    'href'      => URL::cms().'helper/ajax/{function}/languages/{id}/{label}?is_system=0'
                )                    
            ));
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-language')) 
            ->register('content')->use_string($listing->render())
                ;   
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Check for authorisation
        $permission = $this->param1."-language";
        $this->authorise('language',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('language/cms/frm-language')
                    ->set('title',$title)
                ;   
    }
}