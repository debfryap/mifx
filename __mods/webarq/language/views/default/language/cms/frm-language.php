<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Edit Profile ~ CMS
 * @Module      User
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    $id    = Request::$initial->param('param2');
    $job   = strtolower(Request::$initial->param('param1'));
    
    echo Data::form(array(
        'action'    => false,
        'title'     => $title,
        'id'        => 'form_language',
        'multilang' => false,  
        'pack'      => array ('language:'.Request::$initial->param('param1').'-language'),
        'values'    => 'auto',
        'row_id'    => array('id',$id),
        'values'    => $job === 'create' ? false : 'auto',
    ));
?>