<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget viewer
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

if (empty(Language::$active->current()->code)) return;

echo '<div id="language-selector-wrapper">',
        '<ul id="language-selector-ul">';
    
foreach (Language::$active as $lang) {
    echo '<li><a';    
    if ($lang->code == Language::$current) {
        echo ' class="active"'; $href = '#'; 
    } else {
        $href = Translate::uri($lang->code);
    }
    
    echo ' href="',$href,'">';
    echo $lang->label;    
    echo '</a></li>';
}            
        
echo '</ul></div>';
