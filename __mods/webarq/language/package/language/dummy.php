<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    "languages" => array (
        array (
            "id"         => 1,
            "label"      => "Bahasa Indonesia",
            "code"       => "id",
            "is_system"  => 1,
            "is_active"  => 1,
            "ordering"   => 1,
            "create_time"   => "now"
        ),
        array (
            "id"         => 2,
            "label"      => "English",
            "code"       => "en",
            "is_system"  => 1,
            "is_active"  => 1,
            "ordering"   => 2,
            "create_time"   => "now"
        ),
        /**
        array (
            "id"         => 3,
            "label"      => "Deutch",
            "code"       => "de",
            "is_system"  => 1,
            "is_active"  => 1,
            "ordering"   => 3,
            "create_time"   => "now"
        )
        /** **/
    ),
    
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "language",
            "ordering"      => "total-row",
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "Language module. Enabling multilanguge",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations" => array (
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'language'",
            "label"         => "manage-language",
            "permalink"     => "manage",
            "ordering"      => 1,
            "create_time"   => "now"
        )
    ),
    
    "module_navigation_group_maps" => array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-language' AND `permalink` = 'manage' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now"
        )
    ) 
);  