<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Language
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array ( 
    "languages" => array (
        array (
            "master" => "id",
            "type"   => "smallint"
        ),
        array (
            "master" => "label",
            "form"   => array (
                "group"     => array("create-language","edit-language"),
                "minlength" => "4",
                "class"     => "required is_available",
                "data-ajax" => array (
                        "t" => (base64_encode('encode_languages')),
                        "f" => (base64_encode('encode_label')),
                        "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                    )                                
            )
        ),
        array (
            "field"  => "code",
            "type"   => "char",
            "length" => 5,
            "form"   => array (
                "group"     => array("create-language","edit-language"),
                "class"     => "required is_available",
                "data-ajax" => array (
                        "t" => (base64_encode('encode_languages')),
                        "f" => (base64_encode('encode_code')),
                        "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                    )                                
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_system",
            "default" => 0
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group"     => array("create-language","edit-language")
            )
        ),
        array (
            "master" => "ordering",
            "form"   => array (
                "group"     => array("create-language","edit-language"),
                "class"     => "required integer",
                "order"     => "after code",
                "type"      => "ordering"
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ),
    
    "translations" => array (
        array (
            "master"  => "id",
            "type"    => "bigint"
        ),
        array (
            "field"  => "language_code",
            "type"   => "char",
            "length" => 5
        ),
        array (
            "field"  => "row_table",
            "type"   => "varchar",
            "length" => 50
        ),
        array (
            "field" => "row_id",
            "type"  => "int"
        ),
        array (
            "field"  => "row_column",
            "type"   => "varchar",
            "length" => 50
        ),
        array (
            "field"  => "row_value",
            "type"   => "longtext"
        )
    )
);

?>