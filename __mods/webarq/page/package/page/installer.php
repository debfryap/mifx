<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$is_superadmin = empty(User::admin()->id)   ? null : (User::admin()->role_id != 1 ? false : true);


return array (
    "site_navigations" => array (
        array (
            "master" => "id",
            "type"   => "smallint"
        ),  
        array (
            "field"  => "parent_id",
            "type"   => "smallint",
            "form"   => array (
                "label"     => array("parent-navigation","parent-navigation"),
                "group"     => array("create-navigation","edit-navigation"),
                "class"     => "required",
                "traverse"  => array (
                    "field"     => "parent_id",
                    "value"     => "id",
                    "permalink" => true,
                    "label"     => "label",
                    //"top_level" => false 
                ),
                "order"     => "first"
            )
        ),
        array (
            "master" => "label",
            "form"   => array (
                "label"     => "site-menu",
                "group"     => array("create-navigation","edit-navigation"),
                "referrer"  => array("targetLink",false),
                "class"     => "required",
                "multilang" => true,
            )
        ),
        array (
            "master" => "label",
            "field"  => "judul_icon",
            "form"   => array (
                "label"     => "judul-icon",
                "group"     => array("create-navigation","edit-navigation"),
                "multilang" => true,
            )
        ),
        array(
            "type"   => "varchar",
            "field"  => "permalink",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-navigation", $is_superadmin ? "edit-navigation" : null),
                "readonly"  => array(false,!$is_superadmin),
                "class"     => "required targetLink is_available",
                "data-ajax" => array (
                        "t" => (base64_encode('encode_site_navigations')),
                        "f" => (base64_encode('encode_permalink')),
                        "v" => isset(App::$route_params['param2']) ? App::$route_params['param2'] : null
                    ),           
                "info"      => "For internal link please use character, numbers and dash only for internal link. And for external link please use complete URL address, like http://aabbcc.com"
            )
        ), 
        array (
            "master" => "label",
            "field"  => "meta_title",
            "form"   => array (
                "label"     => "meta-title",
                "group"     => array("create-navigation","edit-navigation"),
                "multilang" => true
            )
        ), 
        array (
            "type"   => "mediumtext",
            "field"  => "meta_description",
            "form"   => array (
                "label"     => "meta-description",
                "group"     => array("create-navigation","edit-navigation"),
                "rows"      => 3,
                "style"     => "height:100px;",
                "multilang" => true
            )
        ), 
        array (
            "field"  => "ordering",
            "type"   => "smallint",
            "form"   => array (
                "group"     => array("create-navigation","edit-navigation"),
                "type"      => "ordering",
                "parenting" => "parent_id",
                "class"     => "required",
                "order"     => "after description",
                "parenting_start"  => 0
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_system",
            "default" => 0,
            "form"   => array (
                "group" => !empty($is_superadmin) ? array("create-navigation","edit-navigation") : false
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-navigation","edit-navigation"),
                "default" => 1
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ) ,
    
    "site_navigation_pages" => array (
        array (
            "master"    => "id"
        ),
        array (
            "field"     => "site_navigation_id",
            "type"      => "int",
            "form"      => array (
                "label"     => "select-navigation",
                "group"     => array("create-page","edit-page"),
                "class"     => "required challenge-order",
                "traverse"  => array (
                    "table"          => "site_navigations",
                    "field"          => "parent_id",
                    "value"          => "id",
                    "label"          => "label",
                    "top_level"      => false,
                    "multi_relation" => array(14,4,42,69,70,33,34,array('parent_id',18),array('parent_id',35)),
                    "field_relation" => "site_navigation_id",
                    "exception"      => array("is_system" => "1"),
                    "deep"           => App::$config->system->traversing_deep+1
                ),
            )
        ),
        array (
            "field"     => "title",
            "type"      => "varchar",
            "length"    => 300,
            "form"   => array (
                "label"     => "page-title",
                "group"     => array("create-page","edit-page"),
                "class"     => "required",
                "multilang" => true
            )
        ),
        array (
            "field"     => "intro",
            "type"      => "varchar",
            "length"    => 3000,
            "form"   => array (
                "label"     => "page-intro",
                "class"     => "full-editor",
                "group"     => array("create-page","edit-page"),
                "type"      => "textarea",
                "multilang" => true
            )
        ),
        array (
            "field"     => "description",
            "type"      => "text",
            "form"   => array (
                "label"     => "page-description",
                "class"     => "full-editor",
                "group"     => array("create-page","edit-page"),
                "type"      => "textarea",
                "multilang" => true
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_system",
            "default" => 0,
            "form"   => array (
                //"group" => !empty($is_superadmin) ? array("create-page","edit-page") : false,
                "group" => array("create-page","edit-page"),
                "default" => 0
            )
        ),  
        array (
            "field"  => "ordering",
            "type"   => "smallint",
            "form"   => array (
                "group"     => array("create-page","edit-page"),
                "type"      => "ordering",
                "parenting" => "site_navigation_id",
                "class"     => "required",
                "data-field"=> "site_navigation_id",
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )       
    ),
    
    "site_navigation_position_maps" => array (
        array (
            "master"    => "id"
        ),
        array (
            "type"      => "int",              
            "field"     => "site_navigation_id" 
        ),
        array (
            "type"      => "char",
            "length"    => 9,
            "field"     => "position",
            "form"   => array (
                "label"     => "navigation-position",
                "group"     => array("create-navigation","edit-navigation"),
                "type"      => "checkbox",
                "multiple"  => true,
                "option"    => isset(Page::$config->site_navigation_positions) ? Page::$config->site_navigation_positions : array('main'=>'Main Menu','footer'=>'Footer Menu')
            )
        )
    ),
    
    
    "tutorials" => array (
        array (
            "master" => "id",
            "type"   => "smallint"
        ),  
        array (
            "field"  => "parent_id",
            "type"   => "smallint",
            "form"   => array (
                "label"     => array("parent-tutorial","parent-tutorial"),
                "group"     => array("create-tutorial","edit-tutorial"),
                "class"     => "required",
                "traverse"  => array (
                    "field"     => "parent_id",
                    "value"     => "id",
                    "permalink" => true,
                    "label"     => "label",
                    //"top_level" => false 
                ),
                "order"     => "first"
            )
        ),
        array (
            "master" => "label",
            "form"   => array (
                "label"     => "tutorial-title",
                "group"     => array("create-tutorial","edit-tutorial"),
                "referrer"  => array("permalink",false),
                "class"     => "required",
                "multilang" => true,
            )
        ),
        array(
            "type"   => "varchar",
            "field"  => "permalink",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-tutorial", $is_superadmin ? "edit-tutorial" : null),
                "readonly"  => array(false,true),
                "class"     => "required permalink",
                "info"      => "Permalink should be use character, numbers and dash only'"
            )
        ),
        array (
            "field"     => "description",
            "type"      => "text",
            "form"   => array (
                "label"     => "tutorial-description",
                "class"     => "full-editor",
                "group"     => array("create-tutorial","edit-tutorial"),
                "type"      => "textarea",
                "multilang" => true
            )
        ),  
        array (
            "field"  => "ordering",
            "type"   => "smallint",
            "form"   => array (
                "group"     => array("create-tutorial","edit-tutorial"),
                "type"      => "ordering",
                "parenting" => "parent_id",
                "class"     => "required",
                "order"     => "after description",
                "parenting_start"  => 0
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-tutorial","edit-tutorial"),
                "default" => 1
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ) ,
    
    "investment_clinics" => array (
        array (
            "master" => "id",
            "type"   => "smallint"
        ),  
        array (
            "field"  => "parent_id",
            "type"   => "smallint",
            "form"   => array (
                "label"     => array("parent-investmentclinic","parent-investmentclinic"),
                "group"     => array("create-investmentclinic","edit-investmentclinic"),
                "class"     => "required",
                "traverse"  => array (
                    "field"     => "parent_id",
                    "value"     => "id",
                    "permalink" => true,
                    "label"     => "label",
                    //"top_level" => false 
                ),
                "order"     => "first"
            )
        ),
        array (
            "master" => "label",
            "form"   => array (
                "label"     => "investmentclinic-title",
                "group"     => array("create-investmentclinic","edit-investmentclinic"),
                "referrer"  => array("permalink",false),
                "class"     => "required",
                "multilang" => true,
            )
        ),
        array(
            "type"   => "varchar",
            "field"  => "permalink",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-investmentclinic", $is_superadmin ? "edit-investmentclinic" : null),
                "readonly"  => array(false,true),
                "class"     => "required permalink",
                "info"      => "Permalink should be use character, numbers and dash only'"
            )
        ),
        array (
            "field"     => "description",
            "type"      => "text",
            "form"   => array (
                "label"     => "investmentclinic-description",
                "class"     => "full-editor",
                "group"     => array("create-investmentclinic","edit-investmentclinic"),
                "type"      => "textarea",
                "multilang" => true
            )
        ),  
        array (
            "field"  => "ordering",
            "type"   => "smallint",
            "form"   => array (
                "group"     => array("create-investmentclinic","edit-investmentclinic"),
                "type"      => "ordering",
                "parenting" => "parent_id",
                "class"     => "required",
                "order"     => "after description",
                "parenting_start"  => 0
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-investmentclinic","edit-investmentclinic"),
                "default" => 1
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ) ,
    
    "glossaries" => array (
        array (
            "master" => "id",
            "type"   => "int"
        ),  
        array (
            "master" => "label",
            "form"   => array (
                "label"     => "glossary-title",
                "group"     => array("create-glossary","edit-glossary"),
                "referrer"  => array("permalink",false),
                "class"     => "required",
                "multilang" => true,
            )
        ),
        array(
            "type"   => "varchar",
            "field"  => "permalink",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-glossary", $is_superadmin ? "edit-glossary" : null),
                "readonly"  => array(false,true),
                "class"     => "required permalink",
                "info"      => "Permalink should be use character, numbers and dash only'"
            )
        ),
        array (
            "master" => "label",
            "field"  => "meta_title",
            "form"   => array (
                "label"     => 'Meta Title',
                "group"     => array("create-glossary","edit-glossary"),
            ) 
        ),
        array (
            "master" => "label",
            "field"  => "meta_description",
            "form"   => array (
                "label"     => 'Meta Description',
                "group"     => array("create-glossary","edit-glossary"),
                "type"      => 'textarea',
                "cols"      => 50,
                "maxlength" => 160,
                "rows"      => 10
            ) 
        ),
        array (
            "field"     => "description",
            "type"      => "text",
            "form"   => array (
                "label"     => "glossary-description",
                "class"     => "full-editor",
                "group"     => array("create-glossary","edit-glossary"),
                "type"      => "textarea",
                "multilang" => true
            )
        ),  
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-glossary","edit-glossary"),
                "default" => 1
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ),
    
    // add installer ebook
    "ebook" => array (
        array (
            "master" => "id",
            "type"   => "int"
        ),  
        array (
            "master" => "label",
            "form"   => array (
                "label"     => "ebook-title",
                "group"     => array("create-ebook","edit-ebook"),
                "class"     => "required",
                "referrer"  => array("permalink",false),
                "multilang" => true,
            )
        ),
         array(
            "type"   => "varchar",
            "field"  => "permalink",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-ebook", $is_superadmin ? "edit-ebook" : null),
                "readonly"  => array(false,true),
                "class"     => "required permalink",
                "info"      => "Permalink should be use character, numbers and dash only'"
            )
        ),

        array(
            "type"   => "varchar",
            "field"  => "url-demo",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-ebook", "edit-ebook"),
                "class"     => "required",
            )
        ),
        array(
            "type"   => "varchar",
            "field"  => "url-full",
            "length" => 500,
            "form"   => array (
                "group"     => array("create-ebook", "edit-ebook"),
                "class"     => "required"
            )
        ),
        array (
            "field"     => "description",
            "type"      => "text",
            "form"   => array (
                "label"     => "ebook-description",
                "class"     => "simple-editor",
                "group"     => array("create-ebook","edit-ebook"),
                "type"      => "textarea",
                "multilang" => true
            )
        ),
        array (
            "field"  => "ordering",
            "type"   => "smallint",
            "form"   => array (
                "group"     => array("create-ebook","edit-ebook"),
                "type"      => "ordering",
                "class"     => "required",
                "order"     => "after description",
            )
        ),  
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group" => array("create-ebook","edit-ebook"),
                "default" => 1
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    ),
    
    // add installer ebook
    "email_formats" => array (
        array (
            "master" => "id",
            "type"   => "smallint"
        ),   
        array (
            "master" => "label",
            "field"  => "type",
            "form"   => array (
                "label"     => "email-type",
                "group"     => array("create-email-format"),
                "class"     => "required"
            )
        ), 
        array (
            "master" => "label",
            "field"  => "subject",
            "form"   => array (
                "label"     => "email-subject",
                "group"     => array("create-email-format","edit-email-format"),
                "class"     => "required",
                "multilang" => true,
            )
        ),
        array (
            "field"     => "description",
            "type"      => "text",
            "form"   => array (
                "label"     => "email-body",
                "group"     => array("create-email-format","edit-email-format"),
                "class"     => "required simple-editor",
                "multilang" => true,
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    )
);