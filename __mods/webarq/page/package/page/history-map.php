<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    History item pointer
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'object' => array (
        'site_navigation'           => 'label',
        'site_navigation_page'      => 'title',
        'email_format'              => 'type'
    ),
    'action' => array (
        'site_navigation'           => 'add'
        
    )
);