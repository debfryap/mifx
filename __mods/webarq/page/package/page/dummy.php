<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Dummy data
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (    
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "page",
            "ordering"      => "total-row",
            "is_system"     => 1,
            "is_publish"    => 1,
            "description"   => "Pages Module",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations" => array (
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "site-navigation",
            "permalink"     => "site-navigation",
            "ordering"      => 1,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "manage-page",
            "permalink"     => "manage-page",
            "ordering"      => 2,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "tutorial",
            "permalink"     => "tutorial",
            "ordering"      => 3,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "investmentclinic",
            "permalink"     => "investmentclinic",
            "ordering"      => 4,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "glossary",
            "permalink"     => "glossary",
            "ordering"      => 5,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "ebook",
            "permalink"     => "ebook",
            "ordering"      => 6,
            "create_time"   => "now"
        ),
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page'",
            "label"         => "email-format",
            "permalink"     => "email-format",
            "ordering"      => 7,
            "create_time"   => "now"
        )
    ),
    
    "module_navigation_group_maps" => array (
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='site-navigation' AND `permalink` = 'site-navigation' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        ),
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-page' AND `permalink` = 'manage-page' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        ),
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='tutorial' AND `permalink` = 'tutorial' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        ),
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='investmentclinic' AND `permalink` = 'investmentclinic' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        ),
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='glossary' AND `permalink` = 'glossary' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        ),
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='ebook' AND `permalink` = 'ebook' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        ),
        array (
            "id"                            => "auto",
            "module_navigation_id"          => "SELECT `id` FROM `module_navigations` WHERE `label` ='email-format' AND `permalink` = 'email-format' LIMIT 1",
            "module_navigation_group_id"    => 1,
            "create_time"                   => "now"
        )
    ),
    
    "module_permissions" => array (
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-navigation",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-navigation",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "delete-navigation",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "publish-navigation",
            "create_time"   => "now"                    
        ),
        
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-page",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-page",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "delete-page",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-tutorial",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-tutorial",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "delete-tutorial",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "publish-tutorial",
            "create_time"   => "now"                    
        ),
                
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-investmentclinic",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-investmentclinic",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "delete-investmentclinic",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "publish-investmentclinic",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-glossary",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-glossary",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "delete-glossary",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "publish-glossary",
            "create_time"   => "now"                    
        ),

        // add module permission ebook
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-ebook",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-ebook",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "delete-ebook",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "publish-ebook",
            "create_time"   => "now"                    
        ),

        // add module permission email format
         array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "create-email-format",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1",
            "permission"    => "edit-email-format",
            "create_time"   => "now"                    
        )
    ),
    
    
    "module_navigation_permission_maps"  => array (
        //Navigation Maps
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='site-navigation' AND `permalink` = 'site-navigation' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-navigation' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='site-navigation' AND `permalink` = 'site-navigation' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-navigation' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='site-navigation' AND `permalink` = 'site-navigation' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'delete-navigation' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='site-navigation' AND `permalink` = 'site-navigation' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'publish-navigation' LIMIT 1",
            "create_time"   => "now" 
        ),        
        
        //Page maps
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-page' AND `permalink` = 'manage-page' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-page' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-page' AND `permalink` = 'manage-page' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-page' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='manage-page' AND `permalink` = 'manage-page' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'delete-page' LIMIT 1",
            "create_time"   => "now" 
        ),
        
        //Tutorial Maps
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='tutorial' AND `permalink` = 'tutorial' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-tutorial' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='tutorial' AND `permalink` = 'tutorial' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-tutorial' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='tutorial' AND `permalink` = 'tutorial' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'delete-tutorial' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='tutorial' AND `permalink` = 'tutorial' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'publish-tutorial' LIMIT 1",
            "create_time"   => "now" 
        ),
        
        //Investment Clinic Maps 
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='investmentclinic' AND `permalink` = 'investmentclinic' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-investmentclinic' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='investmentclinic' AND `permalink` = 'investmentclinic' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-investmentclinic' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='investmentclinic' AND `permalink` = 'investmentclinic' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'delete-investmentclinic' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='investmentclinic' AND `permalink` = 'investmentclinic' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'publish-investmentclinic' LIMIT 1",
            "create_time"   => "now" 
        ),
        
        //Glossary Maps
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='glossary' AND `permalink` = 'glossary' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-glossary' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='glossary' AND `permalink` = 'glossary' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-glossary' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='glossary' AND `permalink` = 'glossary' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'delete-glossary' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='glossary' AND `permalink` = 'glossary' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'publish-glossary' LIMIT 1",
            "create_time"   => "now" 
        ),

        //Ebook Maps
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='ebook' AND `permalink` = 'ebook' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-ebook' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='ebook' AND `permalink` = 'ebook' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-ebook' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='ebook' AND `permalink` = 'ebook' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'delete-ebook' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='ebook' AND `permalink` = 'ebook' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'publish-ebook' LIMIT 1",
            "create_time"   => "now" 
        ),

        //Email Format Maps
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='email-format' AND `permalink` = 'email-format' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'create-email-format' LIMIT 1",
            "create_time"   => "now" 
        ),        
        array (
            "id"            => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='email-format' AND `permalink` = 'email-format' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'page' LIMIT 1) AND `permission` = 'edit-email-format' LIMIT 1",
            "create_time"   => "now" 
        ) 
    ),
  
    "site_navigations" => array (
        array (
            "id"            => 1,
            "parent_id"     => 0,
            "label"         => 'Home',
            "permalink"     => 'home',
            "ordering"      => 1,
            "is_system"     => 1,
            "is_active"     => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"            => 2,
            "parent_id"     => 0,
            "label"         => 'About Us',
            "permalink"     => 'about-us',
            "ordering"      => 2,
            "is_system"     => 1,
            "is_active"     => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"            => 3,
            "parent_id"     => 2,
            "label"         => 'History',
            "permalink"     => 'history',
            "ordering"      => 1,
            "is_system"     => 0,
            "is_active"     => 1,
            "create_time"   => "now" 
        ),
        array (
            "id"            => 4,
            "parent_id"     => 2,
            "label"         => 'Our Team',
            "permalink"     => 'our-team',
            "ordering"      => 2,
            "is_system"     => 1,
            "is_active"     => 1,
            "create_time"   => "now" 
        )
    )
);

?>