<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Edit Page ~ CMS
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

    $id = Request::$initial->param('param2');
    $ac = Request::$initial->param('param1');
    
    $md = null;
    
    if ($ac == 'edit') {
        if ( $id == 8 || $id == 11)
            $md = array('site_navigation_pages',array('image:page-left-bnnr'));
        if ($id == 24)
            $md = array('site_navigation_pages',array('image:banner-account'));
    }

    if($md !=null)
        array_push($md, 'image:banner-page');
    else
        $md = array('site_navigation_pages', 'image:banner-page');
    
    echo Data::form(array(
        'action'    => false,
        'title'     => $title,
        'media'     => $md ,//array('site_navigation_pages',array('image:widget-icon')),
        'id'        => 'form_navigation',
        'multilang' => true,  
        'pack'      => array ('page:'.Request::$initial->param('param1').'-page'),
        #'row_id'    => Request::$initial->param('param2'),
    ));
?>