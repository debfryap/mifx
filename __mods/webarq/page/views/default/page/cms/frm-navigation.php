<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Edit Page ~ CMS
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

    
    $id = Request::$initial->param('param2');
    $ac = Request::$initial->param('param1');
    $ma = null;
    
    $frm = Data::form(array(
        'action'    => false,
        'title'     => $title,
        'id'        => 'form_navigation',
        'multilang' => true,  
        'pack'      => array ('page:'.$ac.'-navigation'),
        'type'      => strtolower($ac),
        'row_id'    => array('id',$id),
    ));
    
    $ma = array();

    if($id !=1){
        //Default media library
        $ma = array('image:main-banner-id', 'image:main-banner-en', 'image:main-banner-cn','image:main-banner-tw', 'image:widget-icon');
    }

    //Filtering ....
    if ($ac == 'edit') {
        if ($id == 1) {
           // array_push($ma, 'image:slide', 'image:logo-partner');
           array_push($ma, 'image:logo-partner', 'image:slide-id', 'image:slide-en', 'image:slide-cn', 'image:slide-tw');
        } 
        if ($id == 43) {
            
        }
        if($id == 36 or $id==41){
            array_push($ma, 'image:banner-trading');      
        }
        if($id == 38 or $id == 39 or $id == 40){
            array_push($ma, 'image:widget-trading');           
        }
        if($id==41){
            array_push($ma, 'image:banner-310x172');
        }
        if($id==43){
            array_push($ma, 'image:banner-410x375');
        }
        
        //if ($id == 1 || $id == 3) {
            //array_push($ma, 'image:home-banner');    
        //}
        if ($id == 72) {
            array_push($ma, 'image:page-left-bnnr');
        }
    
        //Get form transaction values
        $values = $frm->get('_values');
        
        //Get parent id
        $parent_id = $values['parent_id'][Language::$system];
        
        //Get position menu
        $position  = isset($values['position']) ? $values['position'][Language::$system] : null;        
    }
    
    if ( isset($parent_id) ) {
        if ($parent_id == 55)
            array_push($ma, 'image:tool-video');
        if ($parent_id == 47)
            array_push($ma, 'image:widget-account');
    }
    
    if (isset($position)) {
        if (is_array($position))
        {
            if (in_array('shortcut',$position)) {
                array_push($ma,'image:shortcut-icon'); 
            }    
        } 
        elseif ($position == 'shortcut')
        {
            array_push($ma, 'image:main-banner','image:shortcut-icon');
        }
    }
    
    $md = array('site_navigations', $ma);
    
    $frm->set('media',$md);
    
    echo $frm;
?>