<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Edit Page ~ CMS
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

    $id = Request::$initial->param('param2');
    $ac = Request::$initial->param('param1');
    
    echo Data::form(array(
        'action'    => false,
        'title'     => $title,
        'id'        => 'form_navigation',
        'multilang' => true,  
        'pack'      => array ('page:'.Request::$initial->param('param1').'-email-format'),
        #'row_id'    => Request::$initial->param('param2'),
    ));
?>