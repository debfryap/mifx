<?php defined('SYSPATH') or die('No direct script access.');

return array  (
    'default' => array (
        'media_library_application_types'  => array (
            'image'         => array (
                'slide'         => array (
                    'label'     => 'image-slider',
                    'width'     => 800,
                    'height'    => 300,
                    'details'   => array (
                        array (
                            "label" => 'Title',
                            "name"  => 'title',
                        ),
                        array (
                            "label" => 'Description',
                            "name"  => 'description',
                            "type"  => 'textarea',
                            "rows"  => 2,
                            "style" => "margin-top:1px;width:800px"
                        ),
                        array (
                            "label" => 'Banner Link',
                            "name"  => 'permalink',
                            "class" => 'url'
                        )
                    )
                ),
                
                'main-banner'   => array (
                    'width'     => 800,
                    'height'    => 300,
                    'limit'     => 1
                )
            ),
            'doc'           => array ('attachment'=>'document'),
            'video'         => array ('video'=>'video')
        ),
    )
);            