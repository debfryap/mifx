<?php defined('SYSPATH') or die('No direct script access.');

return array (
        'default'   => array (
            'breadCrumbSeparator'       => array('file'=>'material/ico_breadcrumb.jpg'),
            'site_navigation_positions' => array (
                'main'                  => "Main Menu",
                'footer'                => "Footer Menu",
                'shortcut'              => "Shortcut"
            ),
            'nstt'                      => array (
                'nstt', 'breadCrumbSeparator'
            )
        )
    );