<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'page'                      => 'Page',
    'manage-page'               => 'Static Page Administration',
    'create-page'               => 'Create Page',
    'edit-page'                 => 'Edit Page',
    'delete-page'               => 'Delete Page',
    'publish-page'              => 'Publish/Unpublish Page',
    'site-menu'                 => 'Menu Title',
    'page-title'                => 'Page Title',
    'page-intro'                => 'Page Introduction',
    'page-description'          => 'Page Description',
    'parent-page'               => 'Parent Page',
    'navigation'                => 'Navigation',
    'site-navigation'           => 'Site Navigation Administration',
    'manage-navigation'         => 'Site Navigation Administration',
    'create-navigation'         => 'Create Navigation',
    'edit-navigation'           => 'Edit Navigation',
    'delete-navigation'         => 'Delete Navigation',
    'publish-navigation'        => 'Publish/Unpublish Navigation',
    'parent-navigation'         => 'Parent Navigation',
    'select-navigation'         => 'Select Navigation',
    
    'image-slider'              => 'Image Slider',
    'main-banner'               => 'Main Banner',
    
    'tutorial'                  => 'Tutorial',
    'create-tutorial'           => 'Add Tutorial',
    'edit-tutorial'             => 'Edit Tutorial',
    'publish-tutorial'          => 'Publish/Unpublish Tutorial',
    'delete-tutorial'           => 'Delete Tutorial',
    'manage-tutorial'           => 'Manage Tutorial',
    'parent-tutorial'           => 'Parent',
    'tutorial-description'      => 'Description',
    'tutorial-title'            => 'Title',
    
    'investmentclinic'                  => 'Investment Clinic',
    'create-investmentclinic'           => 'Add Investment Clinic',
    'edit-investmentclinic'             => 'Edit Investment Clinic',
    'publish-investmentclinic'          => 'Publish/Unpublish Investment Clinic',
    'delete-investmentclinic'           => 'Delete Investment Clinic',
    'manage-investmentclinic'           => 'Manage Investment Clinic',
    'parent-investmentclinic'           => 'Parent',
    'investmentclinic-description'      => 'Description',
    'investmentclinic-title'            => 'Title',
    
    'glossary'                  => 'Glossary',
    'create-glossary'           => 'Add Glossary',
    'edit-glossary'             => 'Edit Glossary',
    'publish-glossary'          => 'Publish/Unpublish Glossary',
    'delete-glossary'           => 'Delete Glossary',
    'manage-glossary'           => 'Manage Glossary',
    'parent-glossary'           => 'Parent',
    'glossary-description'      => 'Description',
    'glossary-title'            => 'Title',
    
    'email-format'              => 'Email Format',
    'create-email-format'       => 'Create Email Format',
    'edit-email-format'         => 'Edit Email Format',
    'manage-email-format'       => 'Manage Email Format',
    'email-subject'             => 'Email Subject',
    'email-body'                => 'Email Body',
    'email-type'                => 'Type',
    'subject'                   => 'Subject',
    
    'edit-ebook'                => 'Edit Ebook',
    'delete-ebook'              => 'Delete Ebook',
    'publish-ebook'             => 'Publish Ebook',
);