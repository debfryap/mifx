<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 
 
return array(
    'page'                      => 'Page',
    'manage-page'               => 'Page Administration',
    'create-page'               => 'Create Page',
    'edit-page'                 => 'Edit Page',
    'delete-page'               => 'Delete Page',
    'publish-page'              => 'Publish/Unpublish Page',
    'site-menu'                 => 'Menu Title',
    'page-title'                => 'Page Title',
    'page-intro'                => 'Page Introduction',
    'page-description'          => 'Page Description',
    'parent-page'               => 'Parent Page',
    
    'site-navigation'           => 'Site Navigation Administration',
    'manage-navigation'         => 'Site Navigation Administration',
    'create-navigation'         => 'Create Navigation',
    'edit-navigation'           => 'Edit Navigation',
    'delete-navigation'         => 'Delete Navigation',
    'publish-navigation'        => 'Publish/Unpublish Navigation',
    'parent-navigation'         => 'Parent Navigation',
    'select-navigation'         => 'Select Navigation',
);