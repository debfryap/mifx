<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_FooterNavigation extends Populate {
    
    private $_object;
    
    private $_active_uri;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        $this->_object = Data::traversing(false)
                    ->__set('db',Model::factory('Navigation')->footer())
                    ->__set('order_by','sn.ordering')
                    ->__set('column_traverse','sn.parent_id')
                    ->__set('next_column_traverse_value','id')
                    ->__set('traverse_start',0)
                    ->__set('deep',1)
                    ->response();
        
        if (!empty(App::$module->language))
            $this->_object = Translate::item('site_navigations',$this->_object);
            
        $this->_active_uri = App::tail_active_uri()->get('tail');
    }
    
    public function render() {
        if (empty($this->_object))
            return null;
        
        $html  = '<div id="widget-page_footer-navigation">';        
        $html .= $this->navigation_ul($this->_object);
        $html .= '</div>';
        
        return $html;
    }
    
    private function navigation_ul($object) {        
        $html = '<ul';
        $html .= '>';
        
        foreach ($object as $item) {
            $is_active = $item->permalink == App::$config->menu->active 
                            || $item->permalink == App::$config->menu->group
                                || (!empty($this->_active_uri) && in_array($item->permalink,$this->_active_uri));
            
            $html .= '<li';
            $html .= ' class="level-'.$item->level;
            $html .= $is_active ? ' active' : ''; 
            $html .= '"';
            $html .= '>';
            $html .= '<a';
            if (!empty(App::$module->language) && Language::$current != Language::$default)
                $html .= ' href="'.URL::front(Language::$current.'/'.$item->permalink).'"';
            else
                $html .= ' href="'.URL::front($item->permalink).'"';
            $html .= '>';
            $html .= $item->label;
            $html .= '</a>';            
            
            if (!empty($item->child))
                $html .= $this->navigation_ul($item->child);
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
}