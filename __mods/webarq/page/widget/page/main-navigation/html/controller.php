<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_MainNavigation extends Populate {
    
    protected $navigation;
    
    private $_active_uri;
    
    public function __construct(array $configs = array()) {
        parent::__construct($configs);
        
        if (!empty(App::$module->language))
            $this->navigation = Translate::item('site_navigations',$this->navigation);
                        
        $this->_active_uri = App::tail_active_uri()->get('tail');
    }
    
    public function render() {
        if (empty($this->navigation))
            return null;
            
        $html  = '<div id="widget-page_site-navigation">';        
        $html .= $this->navigation_ul($this->navigation);
        $html .= '</div>';
        
        return $html;
    }
    
    private function navigation_ul($object,$parent_uri=null) {
        
        $html = '<ul';
        $html .= '>';
        
        foreach ($object as $item) {
            
            $is_active = $item->permalink == App::$config->menu->active 
                            || $item->permalink == App::$config->menu->group
                                || (!empty($this->_active_uri) && in_array($item->permalink,$this->_active_uri));
            
            $html .= '<li';
            $html .= ' class="level-'.$item->level;
            $html .= $is_active ? ' active' : '';
            $html .= '"';
            $html .= '>';
            $html .= '<a';
            
            $uri   = !isset($parent_uri) ? $item->permalink : $parent_uri.'/'.$item->permalink;
                        
            if (!empty(App::$module->language) && Language::$current != Language::$default)
            {
                $href  = Translate::uri(Language::$current,Language::$current.'/'.$uri);
                $html .= ' href="'.$href.'"';
            }                
            else
            {
                $html .= ' href="'.URL::front($uri).'"';
            }
            $html .= '>';
            $html .= $item->label;
            $html .= '</a>';            
            
            if (!empty($item->child))
                $html .= $this->navigation_ul($item->child,$uri);
                
            $html .= '</li>';
        }
        
        $html .= '</ul>';
        
        return $html;
    }
}