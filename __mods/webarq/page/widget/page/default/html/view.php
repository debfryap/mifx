<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget View
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/

if (!empty($page)) {
    echo Widget::load('station/bread-crumb');
    echo '<div id="common-page-wrapper">';
    echo '<h3 class="big-header">',$page->title,'</h3>';
    echo '<div class="wording">',$page->description,'</div>';
    echo '</div>';
} else {
    echo View::factory('error/404');
}