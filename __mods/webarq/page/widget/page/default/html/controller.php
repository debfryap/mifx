<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Widget Controller
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/  

class Widget_Page_Default extends Populate {
    
    public function data() {
        $permalink = App::tail_active_uri()->get('small_uri');
        
        $page      = Model::factory('Page')->navigation_page($permalink);
            
        $translate = Translate::item('site_navigation_pages',$page);
        
        return empty($translate[0]) ? null : array('page'=>$translate[0]);
    }   
    
}