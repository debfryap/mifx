<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Handler Class
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Kohana_Page { 
    
    public static $config;
    
    /**
     * Page instance .... does not know is this the right way ... need to learn more about OOP
     */
    public static $instance;
    
    protected $navigationArray = array();           //Store all navigation in array         (array) 
    
    protected $completedNavigationUri = array();    //Store each navigation completed URI   (array)   
    
    protected $navigationObject;                    //Store all active navigation           (object), only main navigation
    
    protected $navigationObjectComplete;            //Store all active navigation           (object), regardless position
    
    protected $navigationActive;                    //Current active navigation             (string)
        
    protected $navigationRoot;                      //Current active navigation root        (string, depend on traversing)
    
    protected $_meta;                                //Current active meta
    
    protected $navigationAll;
    
    /**
     * @var     html    $_navigation page main navigation html 
     */
    protected $navigationHtml;
    
    /**
     */
    protected $simpleBreadCrumb = array();
    
    protected $fullBreadCrumb = array();
    
    public static function up($config) {
        self::$instance = new Page;    
    }
    
    /**
     * Navigation various function
     */
    public function navigationCollector() {
        
        $n = Data::traversing(false)
                    ->__set('db',Model::factory('Navigation')->main())
                    ->__set('order_by','sn.ordering')
                    ->__set('column_traverse','sn.parent_id')
                    ->__set('next_column_traverse_value','id')
                    ->__set('traverse_start',0)
                    ->response();
        
        $a = Data::traversing(false)
                    ->__set('db',Model::factory('Navigation')->active())
                    ->__set('order_by','sn.ordering')
                    ->__set('column_traverse','sn.parent_id')
                    ->__set('next_column_traverse_value','id')
                    ->__set('traverse_start',0)
                    ->response();
        
        $c = Data::traversing(false)
                    ->__set('db', Model::factory('Navigation')->all_nav())
                    ->__set('order_by','sn.ordering')
                    ->__set('column_traverse','sn.parent_id')
                    ->__set('next_column_traverse_value','id')
                    ->__set('traverse_start',0)
                    ->response();
        
        //Translation checking
        if (!empty(App::$module->language) && Language::$default !== Language::$current) {
            $n = Translate::item('site_navigations',$n);
            $a = Translate::item('site_navigations',$a);
            $c = Translate::item('site_navigations',$c);
        }
        
        self::$instance->navigationAll = $c;
        
        //Navigation array
        //self::make_array_navigation($n);
        
        //Navigation Object
        self::$instance->navigationObject = $n;
        
        self::$instance->navigationObjectComplete = $a;
        
        //Navigation html            
        self::$instance->navigationHtml = Widget::load('page/main-navigation')->set('navigation',$n);
        
        //Tailing URL
        $x = App::tail_active_uri();
        self::$instance->navigationActive = empty($x->small_uri) ? null : $x->small_uri;
        self::$instance->navigationRoot   = empty($x->parent_uri) ? self::$instance->navigationActive  : $x->parent_uri;
        
        //Application site simple bread-crumb
        //self::$instance->loadSimpleBreadCrumb($a);        
        
        //Application site full bread-crumb
        self::$instance->loadFullBreadCrumb($a);
        
        //Application complete navigation uri
        self::$instance->loadCompletedNavigationUri($a);
    }
    
    protected function loadCompletedNavigationUri($object, $parent_uri = null) {   
        foreach ($object as $idx => $item) {
            
            //$tmpCompletedNavigationUri = Page::get('completedNavigationUri');   
            
            //Check for query Source
            $qSource = Request::$initial->query('Source');
            
            //Check for session Source
            if (empty($qSource)) {
                $qSource = Session::instance()->get('Source');                
            } else {
                //Session check
                $qSessionSource = Session::instance()->get('Source');
                
                //Only set session source for once
                if (empty($qSessionSource)) {
                    //Set to session
                    Session::instance()->set('Source',$qSource);
                }
            } 
            
            if ( strpos($item->permalink,'http') === 0
                    || $item->permalink == 'open-live-account.php' || $item->permalink == 'open-demo-account.php' )
            {
                $href = $item->permalink;
                self::$instance->completedNavigationUri["normal-$item->id"]        = $href;
                self::$instance->completedNavigationUri["normal-$item->permalink"] = $href;
                
                if (!empty($qSource)) {
                    //Append to href
                    $href .= "?Source=".$qSource;
                }  
                //Directly push 
                self::$instance->completedNavigationUri[$item->id]        = $href;
                self::$instance->completedNavigationUri[$item->permalink] = $href;
                continue;    
            }
            
            $uri   = !isset($parent_uri) ? $item->permalink : $parent_uri.'/'.$item->permalink;
            
            if (!empty(App::$module->language) && Language::$current != Language::$default)
            {
                $href = Translate::uri(Language::$current,Language::$current.'/'.$uri);
            }                
            else
            {
                $href = URL::front($uri);
            }
            
            self::$instance->completedNavigationUri["normal-$item->id"]        = $href;
            self::$instance->completedNavigationUri["normal-$item->permalink"] = $href;
                
            if (!empty($qSource)) {
                
                //Append to href
                $href .= "?Source=".$qSource;
            }
            
            //Push array
            self::$instance->completedNavigationUri[$item->id]        = $href;
            self::$instance->completedNavigationUri[$item->permalink] = $href;
            
            //Push meta
            if (!empty($item->meta_title))          $meta['title'] = $item->meta_title;
            
            if (!empty($item->meta_description))    $meta['desc']  = $item->meta_description;
            
            if (isset($meta)) {
                self::$instance->_meta[$item->id]        = $meta;
                                
                self::$instance->_meta[$item->permalink] = $meta;
                
                //Make sure meta is cleared
                unset($meta);               
            }
            
            
            if (!empty($item->child))
            {
                $this->loadCompletedNavigationUri($item->child,$uri);
            }                
        }
    }
    
    /**
     * @param   object  $n  navigation array object
     */
    protected function loadSimpleBreadCrumb($n) {
        foreach ($n as $i) {
            //$i = Translate::item('site_navigations',$i);
            //Push into simple bread-crumb
            self::$instance->simpleBreadCrumb[$i->permalink] = $i->label;      
            
            //Child checking                          
            if (!empty($i->child)) $this->loadSimpleBreadCrumb($i->child);
        }
    }
    
    /**
     * @param   object  $n  navigation array object
     * @param   string  $p  navigation parent uri
     */    
    protected function loadFullBreadCrumb($n,$p=null) {
        foreach ($n as $i) {
            //Translate Item
            //$i = Translate::item('site_navigations',$i);
            
            //Push into simple bread-crumb
            self::$instance->simpleBreadCrumb[$i->permalink] = $i->label;    
            self::$instance->simpleBreadCrumb[$i->id]        = $i->label;
            
            //Check for http
            if (strpos($i->permalink,'http') === 0) 
            {
                //Push into full bread-crumb @as parent            
                self::$instance->fullBreadCrumb["as-parent-$i->permalink"] = $i->permalink;      
                self::$instance->fullBreadCrumb["as-parent-$i->id"]        = $i->permalink;
                continue;
            }  
            
            //Parent checking
            $u = !isset($p) ? $i->permalink : $p.'/'.$i->permalink;
            $s = explode('/',trim($u,'/')); array_pop($s);
            
            $c = $l = '';
                        
            $cs = is_string(Page::$config->breadCrumbSeparator) 
                        ? Page::$config->breadCrumbSeparator 
                        : HTML::image(URL::templateImage(Page::$config->breadCrumbSeparator->file));
                        
            $b  = !empty(App::$module->language) && Language::$current != Language::$default ? Language::$current . '/' : '';
            
            //Check for query Source
            $qSource = Request::$initial->query('Source');
            
            if (!empty($s)) {
                foreach ($s as $j => $k) {
                    $l .= $j == 0 ? $k : "/$k";
                    
                    $href = $b.$l;
                    
                    if (!empty($qSource)) {
                        $href .= "?Source=".$qSource;
                    }
                         
                    $c .= HTML::anchor($href,self::$instance->simpleBreadCrumb[$k]) . '&nbsp;' . $cs . '&nbsp;';
                }
            }
            
                    
            $href = $b.$l."/".$i->permalink;
                    
            if (!empty($qSource)) {
                $href .= "?Source=".$qSource;
            }
                    
            //Push into full bread-crumb @as parent            
            self::$instance->fullBreadCrumb["as-parent-$i->permalink"] = $c . HTML::anchor($b.$l."/".$i->permalink,self::$instance->simpleBreadCrumb[$i->permalink]) . '&nbsp;' . $cs . '&nbsp;';      
            self::$instance->fullBreadCrumb["as-parent-$i->id"]        = self::$instance->fullBreadCrumb["as-parent-$i->permalink"];
            
            $c .= $i->label;
            
            //Push into full bread-crumb            
            self::$instance->fullBreadCrumb[$i->permalink] = $c;
            self::$instance->fullBreadCrumb[$i->id]        = $c;            
                          
            //Child checking';                          
            if (!empty($i->child)) $this->loadFullBreadCrumb($i->child,$u);
        }
    }
    
    public static function meta($link = null,$type = null) {
        $link = !isset($link) ? self::$instance->navigationActive : $link;
        
        $link = !isset($link) ? 'home' : $link;
        
        $meta = self::$instance->_meta;
        
        //Get default meta
        if (empty($meta) || !isset($meta[$link])) {
            $xmeta = array (
                'title' => App::$config->meta->title,
                'desc'  => App::$config->meta->description
            );
            
            if (isset($link))
                $meta[$link] = $xmeta;
            else
                $meta        = $xmeta;
        }
        
        if (!isset($type)) return $meta;
        
        if (!isset($meta[$link][$type])) return  isset(App::$config->meta->$type) ? App::$config->meta->$type : "There is no meta for $link with $type type";
        
        return $meta[$link][$type];
    }
    
    public static function breadCrumb($mode = 'full', $key = null) {
        
        if ($mode !== 'full' && $mode !== 'simple') {
            $key  = $mode;
            $mode = 'full';
        }
        
        if (!isset($key)) {            
            $uri = App::tail_active_uri();
            $key = !empty($uri->small_uri) ? $uri->small_uri : null;
        }
        
        if (!isset($key)) return null;
        
        $b = !empty(App::$module->language) && Language::$current != Language::$default ? Language::$current . '/' : '';
        $h = HTML::anchor($b . 'home', self::$instance->simpleBreadCrumb['home']) . '&nbsp;' . HTML::image(URL::templateImage(Page::$config->breadCrumbSeparator->file)) . '&nbsp;'; 
        
        if ($mode == 'full')            
            return !empty(self::$instance->fullBreadCrumb[$key]) ? $h . self::$instance->fullBreadCrumb[$key] : null;
        else                           
            return !empty(self::$instance->simpleBreadCrumb[$key]) ? $h . self::$instance->simpleBreadCrumb[$key] : null;
        
    }
    
    public static function set($key,$value) {
        self::$instance->$key = $value;
    }
    
    public static function get($key,$default = null) {
        if ( ($x = strpos($key,':')) === false && !isset(self::$instance->$key) ) return $default;
        
        if ( $x !== false ) { $argument = substr($key,$x+1); $key = substr($key,0,$x); } 
        
        $get = self::$instance->$key;
        
        if (is_array($get) && $x !== false) {
            return !empty($get[$argument]) ? $get[$argument] : $default;
        }
        
        return $get;
    }
    
    protected static $day_indonesia   = array ('Monday'=>'Senin','Tuesday'=>'Selasa','Wednesday'=>'Rabu','Thursday'=>'Kamis','Friday'=>'Jum\'at','Saturday'=>'Sabtu','Sunday'=>'Minggu');
    
    protected static $month_indonesia = array ('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
    
    public static function tanggalIndonesia($date) {
        $strtotime       = strtotime($date);
        $day_int         = date('l',$strtotime);
        $month_int       = date('n',$strtotime); 
        $date            = date('j',$strtotime);
        $year            = date('Y',$strtotime);
        
        return self::$day_indonesia[$day_int] . ", $date ". self::$month_indonesia[$month_int] . " $year";    
    }
    
    public static function rangeTanggalIndonesia($start,$end,$glue = '-') {
        $strtotime1     = strtotime($start);
        $day_int1       = date('l',$strtotime1);
        $month_int1     = date('n',$strtotime1); 
        $date1          = date('j',$strtotime1);
        $year1          = date('Y',$strtotime1);        
        
        $strtotime2     = strtotime($end);
        $day_int2       = date('l',$strtotime2);
        $month_int2     = date('n',$strtotime2); 
        $date2          = date('j',$strtotime2);
        $year2          = date('Y',$strtotime2);
        
        $html           = '';
        if( $strtotime1 < $strtotime2) {
            $fd              = true;
            $html           .= $date1;
        
            if (self::$month_indonesia[$month_int1] != self::$month_indonesia[$month_int2])
                $html      .= " ".self::$month_indonesia[$month_int1];
                
            if ($year1 != $year2)
                $html      .= " $year1";
        }
        
        if (!empty($fd)) {
            $html      .= " $glue ";
        }
        $html          .= "$date2 " . self::$month_indonesia[$month_int2] ." $year2";
        
        return $html;
    }   
    
    public static function datetime($date,$time = '',$code = '') {
        $date = Language::$current == 'id'
                    ? self::tanggalIndonesia($date)
                    : date('D, d M',strtotime($date));
        
        return "$date $time $code";                            
    }
    
    public static function youtubeImage($videoId,$index = 0,array $attrs = array()) {
        if (is_array($index)) {
            $attrs = $index;
            $index = 0;
        }
        /**
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/0.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/1.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/2.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/3.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/default.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/hqdefault.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/mqdefault.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/sddefault.jpg
            http://img.youtube.com/vi/<insert-youtube-video-id-here>/maxresdefault.jpg
        **/
        return HTML::image("http://img.youtube.com/vi/$videoId/$index.jpg",$attrs);
    }
    public static function video($v,$w,$h) {
        /**
         * $files   = simplexml_load_file('https://gdata.youtube.com/feeds/api/videos/3GA1i8UdYbM?v=2');
         * echo Debug::vars($files);
         */
        $e = strpos($v,'http') !== 0 ? 'http://www.youtube.com/embed/'.$v : $v;
        $v = strpos($v,'http') !== 0 ? 'http://www.youtube.com/v/'.$v : $v;
        //return '<iframe width="'.$w.'" height="'.$h.'" src="'.$v.'" frameborder="0" allowfullscreen></iframe>';
        return '<object width="'.$w.'" height="'.$h.'" data="'.$v.'?rel=0&showinfo=0" type="application/x-shockwave-flash"><param name="src" value="'.$v.'?rel=0&showinfo=0"></object>';
    }
    
    public static function sosialMedia($link, $title=null) {
        return View::factory('general/sos-med')->set('permalink',$link)->set('title', $title)->render();
    }
    
    public static function searchNavigationChild($nav,$searchPermalink) {
        
        foreach ($nav as $n) {
            if ($n->permalink !== $searchPermalink) {
                if (!empty($n->child))
                    $item = self::searchNavigationChild($n->child , $searchPermalink);
                else
                    continue;    
            } else {
                $item = $n->child;
                break;
            }
        }
        
        return isset($item) ? $item : null;
    }
    
    public static function getPages($permalink, $mediaTypes = null,$filter = false) {
        $pages = DB::select('snp.id','snp.title','snp.description')
                    ->from(array('site_navigation_pages','snp'))  
                    ->join(array('site_navigations','sn'))
                        ->on('sn.id','=','snp.site_navigation_id')
                    ->where('sn.permalink','=',$permalink)
                    ->order_by('snp.ordering')
                    ->as_object()
                    ->execute();
        
        if (!$pages->valid()) return null;
        
        
               
        foreach ($pages as $i => $p) {
            $p  = Translate::item('site_navigation_pages',$p);
            
            if (!empty($mediaTypes)) {
                $mediaTypes = is_array($mediaTypes) ? $mediaTypes : array($mediaTypes);
                $mediaObj   = new stdClass();
                foreach ($mediaTypes as $type) {
                    $mediaObj->$type = Model::factory('Medialibrary')->simple_application($type,$p->id,'site_navigation_pages');
                }
                $p->mediaObject = $mediaObj;
            }
            $tmp[$i] = $p;
        }
        
        return $tmp;
    }
    
    public static function sourceCorrection($root,$item) {
  
        $normalUrl = self::$instance->completedNavigationUri["normal-$root"];
        
        if (empty($normalUrl)) return '';
        
        $url = $normalUrl;
        
        if (isset($item) && $item !== '')        
          $url .= '/' . $item;
        
        //Check for query Source
        $qSource = Request::$initial->query('Source');
        
        //Check for session Source
        if (empty($qSource)) {
            $qSource = Session::instance()->get('Source');                
        }
        
        if (!empty($qSource)) {
            $url .= "?Source=$qSource";
        }
            
        return $url;
    }
}

?>