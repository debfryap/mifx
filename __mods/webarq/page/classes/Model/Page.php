<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Page extends Model_Dba {        
    public function pre_select() {
         
        $builder = $this
                        ->select(array('sn.id','navigation_id'),'sn.parent_id','sn.ordering','sn.label','sn.permalink',
                            DB::expr("CASE sn.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"),
                            'snp.*')                                          
                        ->from(array('site_navigation_pages','snp'))                  
                        ->join(array('site_navigations','sn'))->on('sn.id = snp.site_navigation_id');
        
        return $builder;
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('site_navigation_pages','snp'),false)
                        ->join(array('site_navigations','sn'))->on('sn.id = snp.site_navigation_id');//->where('sn.parent_id = 0');
        
        return $model;
    }
    
    public function tabular() {         
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/ 
    
    public function navigation_page ($permalink = NULL, $mediaTypes = null) {
        $permalink = empty($permalink) ? Page::get('navigationActive') : $permalink;
        
        $page = $this->select('snp.id','snp.title','snp.intro','snp.description', 'sn.judul_icon', 'sn.permalink')
                    ->from(array('site_navigation_pages','snp'))  
                    ->join(array('site_navigations','sn'))->on('sn.id = snp.site_navigation_id')
                    ->where('sn.permalink','=',$permalink)
                    ->order_by('snp.ordering', 'ASC')
                    ->execute()
                    ->current();
                    
        if (empty($page)) return null;
        
        if (!empty(App::$module->language) && Language::$current != Language::$default) {
            $page = Translate::item('site_navigation_pages',$page);
        }
        
        if (!empty($mediaTypes)) {
            $mediaTypes = is_array($mediaTypes) ? $mediaTypes : array($mediaTypes);
            $mediaObj   = new stdClass();
            foreach ($mediaTypes as $type) {
                $mediaObj->$type = Model::factory('Medialibrary')->simple_application($type,$page->id,'site_navigations');
            }
            $page->mediaObject = $mediaObj;
        }
        
        return $page;
    }

    /** End Data listing purpose **/ 
     
    public function navigation_page_array ($permalink = NULL, $mediaTypes = null,  $limit = null) {
        $permalink = empty($permalink) ? Page::get('navigationActive') : $permalink;
        
        $page = $this->select('snp.id','snp.title','snp.intro','snp.description', 'sn.judul_icon')
                    ->from(array('site_navigation_pages','snp'))  
                    ->join(array('site_navigations','sn'))->on('sn.id = snp.site_navigation_id')
                    ->where('sn.permalink','=',$permalink)
                    ->order_by('snp.ordering', 'ASC');

        // check for limit
        if(is_numeric($limit)) $page->limit($limit);

        // execute query
        $page = $page->execute();
                    
        if (empty($page) || !$page->valid()) return null;
        
        foreach ($page as $i => $p) {
            if (!empty(App::$module->language) && Language::$current != Language::$default) {
                $tmp[$i] = Translate::item('site_navigation_pages',$p);
            } else {
                $tmp[$i] = $p;
            }
            
            if (!empty($mediaTypes)) {
                $mediaTypes = is_array($mediaTypes) ? $mediaTypes : array($mediaTypes);
                $mediaObj   = new stdClass();
                foreach ($mediaTypes as $type) {
                    $mediaObj->$type = Model::factory('Medialibrary')->simple_application($type,$p->id,'site_navigation_pages');
                }
                $tmp[$i]->mediaObject = $mediaObj;
            }
        }
        
        return isset($tmp) ? $tmp : null;
    }
}