<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Ebook extends Model_Dba {  
    public function basic($fields=null) {
        $items = 'sn.*';
        if (isset($fields)) {
            $fields = is_array($fields) ? $fields : func_get_args();
            
            unset($items);
            
            foreach ($fields as $field) {
                $items[] = "sn.$field";
            }
        }
        return $this->select($items)->from(array('ebook','sn'));    
    }
    
    public function pre_select() {
        return $this->select('sn.*',
                                DB::expr("CASE sn.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('ebook','sn'));
    }
    
    
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('ebook','sn'),false);//->where('sn.parent_id = 0');
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/   
    
    public function active() {
        return $this->select('sn.id','sn.label','sn.permalink','sn.folder')
                    ->from(array('ebook','sn'))
                    ->where('sn.is_active = 1')
                    ->group_by('sn.id');
    } 
    
   
    
    public function getBy($by_field,$value,$limit = 1) {
        //Check for operator
        $operator = substr($value,0,1) == '%' && substr($value,-1) == '%' ? 'LIKE' : '=';
        
        //Start select
        $get = $this->select()->from('ebook')->where($by_field,$operator,$value);
        
        //Check for limit
        if (is_numeric($limit) && $limit > 0) $get->limit($limit);
        
        //Return
        return $get->execute();                        
    }

     public function current_ebook ($permalink = NULL) {
        
        $page = $this->select('snp.*')
                    ->from(array('ebook','snp'))
                    ->where('snp.is_active = 1')
                    ->order_by('snp.ordering', 'DESC');
        
        if($permalink !=null or !empty($permalink))
            $page = $page->where('snp.permalink','=',$permalink);

        
        $page = $page->execute()->current();

        if (empty($page)) return null;
        
        if (!empty(App::$module->language) && Language::$current != Language::$default) {
            $page = Translate::item('ebook',$page);
        }
       
        
        return $page;
    }

    public function all_ebook() {

        $page = $this->select('snp.*')
                    ->from(array('ebook','snp'))
                    ->where('snp.is_active = 1')
                    ->order_by('snp.ordering', 'DESC')
                    ->execute();


        return $page;

    }
}