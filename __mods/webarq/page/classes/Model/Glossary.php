<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Glossary extends Model_Dba {  
    public function basic($fields=null) {
        $items = null;
        if (isset($fields)) {
            $fields = is_array($fields) ? $fields : func_get_args();
            
            unset($items);
            
            foreach ($fields as $field) {
                $items[] = "g.$field";
            }
        }
        return $this->select($items)->from(array('glossaries','g'));    
    }
    
    public function pre_select() {
        return $this->select('g.*',
                                DB::expr("CASE g.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('glossaries','g'));
    }
    
    
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('glossaries','g'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
}