<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Navigation extends Model_Dba {  
    public function basic($fields=null) {
        $items = 'sn.*';
        if (isset($fields)) {
            $fields = is_array($fields) ? $fields : func_get_args();
            
            unset($items);
            
            foreach ($fields as $field) {
                $items[] = "sn.$field";
            }
        }
        return $this->select($items)->from(array('site_navigations','sn'));    
    }
    
    public function pre_select() {
        return $this->select('sn.*',
                                DB::expr("CASE sn.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('site_navigations','sn'));
    }
    
    
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('site_navigations','sn'),false)->where('sn.parent_id = 0');
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/   
    
    public function active() {
        return $this->select('sn.id','sn.label','sn.permalink','sn.judul_icon','sn.meta_title','sn.meta_description', 'snpm.position')
                    ->from(array('site_navigations','sn'))
                    ->join(array('site_navigation_position_maps','snpm'))
                    ->on('snpm.site_navigation_id = sn.id')
                    ->where('sn.is_active = 1')
                    ->group_by('sn.id');
    } 
    
    public function main() {
        return $this->select('sn.id','sn.label','sn.permalink','sn.judul_icon')
                    ->from(array('site_navigations','sn'))
                    ->join(array('site_navigation_position_maps','snpm'))
                    ->on('snpm.site_navigation_id = sn.id')
                    ->where('sn.is_active = 1')
                    ->and_where('snpm.position = main')
                    ->group_by('sn.id');
    } 
    
    public function footer() {
        return $this->select('sn.id','sn.label','sn.permalink','sn.judul_icon')
                    ->from(array('site_navigations','sn'))
                    ->join(array('site_navigation_position_maps','snpm'))
                    ->on('snpm.site_navigation_id = sn.id')
                    ->where('sn.is_active = 1')
                    ->and_where('snpm.position = footer')
                    ->group_by('sn.id');
    } 
    
     public function all_nav() {
        return $this->select('sn.id','sn.label','sn.permalink','sn.judul_icon')
                    ->from(array('site_navigations','sn'))
                    ->join(array('site_navigation_position_maps','snpm'))
                    ->on('snpm.site_navigation_id = sn.id')
                    ->where('sn.is_active = 1')
                    ->group_by('sn.id');
    }  
    
    public function shortcut() {
        return $this->select('sn.id','sn.label','sn.permalink','sn.judul_icon')
                    ->from(array('site_navigations','sn'))
                    ->join(array('site_navigation_position_maps','snpm'))
                    ->on('snpm.site_navigation_id = sn.id')
                    ->where('sn.is_active = 1')
                    ->and_where('snpm.position = shortcut');
    }
    
    public function getBy($by_field,$value,$limit = 1) {
        //Check for operator
        $operator = substr($value,0,1) == '%' && substr($value,-1) == '%' ? 'LIKE' : '=';
        
        //Start select
        $get = $this->select()->from('site_navigations')->where($by_field,$operator,$value);
        
        //Check for limit
        if (is_numeric($limit) && $limit > 0) $get->limit($limit);
        
        //Return
        return $get->execute();                        
    }
}