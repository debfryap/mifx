<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Emailformat extends Model_Dba { 
    
    /** Data listing purpose **/
    public function total() {
        return Data::total(array('email_formats','ef'),false);
    }
    
    public function tabular() {                     
        return $this->select()->from('email_formats');
    }
    /** End Data listing purpose **/ 
    
    
    public function item($type) {
        return $this->tabular()->where('type','=',$type)->execute()->current();
    }
}