<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Tutorial extends Model_Dba {  
    public function basic($fields=null) {
        $items = null;
        if (isset($fields)) {
            $fields = is_array($fields) ? $fields : func_get_args();
            
            unset($items);
            
            foreach ($fields as $field) {
                $items[] = "t.$field";
            }
        }
        return $this->select($items)->from(array('tutorials','t'));    
    }
    
    public function pre_select() {
        return $this->select('t.*',
                                DB::expr("CASE t.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('tutorials','t'));
    }
    
    
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('tutorials','t'),false)->where('t.parent_id = 0');
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
}