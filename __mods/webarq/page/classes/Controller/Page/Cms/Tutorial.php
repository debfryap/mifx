<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Page_Cms_Tutorial extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('tutorial'));
        
        App::$config->menu->active = 'tutorial';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('page',array(
                            'create-tutorial','edit-tutorial',
                            'delete-tutorial','publish-tutorial'));
        
        //Append media tree tabular
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('tutorial');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        $listing->traversing();
        
        //Default ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('page','create-tutorial')) {
            $listing->create_action(__('create-tutorial'));
        }
        
        if (User::authorise('page','publish-tutorial')) {
            $listing->status_action(array(
                //'exception' => array('is_system'=>1),
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/tutorials/{id}/{label}'
            ));
        }
        
        if (User::authorise('page','edit-tutorial')) {
            $listing->edit_action(array(
                //'exception' => array('is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('page','delete-tutorial')) {
            $listing->delete_action(array(
                'exception' => array(
                        'i_absolute'=>true),
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/tutorials/{id}/{label}?ordering={ordering}&parent={parent_id}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-tutorial')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //Append CK Editor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-tutorial";
        $this->authorise('page',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('page/cms/frm-tutorial')
                    ->set('title',$title)
                ;   
    }
}