<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Page_Cms_Glossary extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('glossary'));
        
        App::$config->menu->active = 'glossary';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('page',array(
                            'create-glossary','edit-glossary',
                            'delete-glossary','publish-glossary'));
        
        //Append media tree tabular
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('glossary');
        
        //HTML table style listing
        $listing->style('table');
        
        //Default ordering
        $listing->order_by('label');
        
        //Listing header
        $listing->set_header('label')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('page','create-glossary')) {
            $listing->create_action(__('create-glossary'));
        }
        
        if (User::authorise('page','publish-glossary')) {
            $listing->status_action(array(
                //'exception' => array('is_system'=>1),
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/glossaries/{id}/{label}'
            ));
        }
        
        if (User::authorise('page','edit-glossary')) {
            $listing->edit_action(array(
                //'exception' => array('is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('page','delete-glossary')) {
            $listing->delete_action(array(
                'exception' => array(
                        'i_absolute'=>true),
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/glossaries/{id}/{label}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-glossary')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //Append CK Editor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-glossary";
        $this->authorise('page',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('page/cms/frm-glossary')
                    ->set('title',$title)
                ;   
    }
}