<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Page_Cms_Managepage extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('manage-page'));
        
        App::$config->menu->active = 'manage-page';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('page',array(
                            'create-page','edit-page',
                            'delete-page','publish-page'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('page');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        //$listing->traversing('parent_id','site_navigation_id');
        
        //Default ordering
        $listing->order_by(array('sn.parent_id','sn.ordering','snp.ordering'));
        
        //Listing header
        $listing->set_header(array(
                'title',
                'label' => array( 'label' => __('navigation') )
            )) ;        
        
        $listing->tool('search',array('title','label'));
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('page','create-page')) {
            $listing->create_action(__('create-page'));
        }
        
        if (User::authorise('page','edit-page')) {
            $listing->edit_action(array(
                //'exception' => array('is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('page','delete-page')) {
            $listing->delete_action(array(
                'exception' => array('is_system'=>1,'i_absolute'=>true),
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/site_navigation_pages/{id}/{title}?site_navigation_id={site_navigation_id}&ordering={ordering}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-page')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Activate ckeditor
        $this->media_ckeditor();
        
        //Load media script
        $this->media_header();
        
        //Check for authorisation
        $permission = $this->param1."-page";
        $this->authorise('page',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Append js
        $this->scripts(URL::libJs('uploadify-3.2/jquery.uploadify.min'));
       
        //Append css
        $this->styles(URL::libJs('uploadify-3.2/uploadify.css'));
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('page/cms/frm-page')
                    ->set('title',$title)
                ;   
    }
}