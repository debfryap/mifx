<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Page_Cms_Investmentclinic extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('investmentclinic'));
        
        App::$config->menu->active = 'investmentclinic';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('page',array(
                            'create-investmentclinic','edit-investmentclinic',
                            'delete-investmentclinic','publish-investmentclinic'));
        
        //Append media tree tabular
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('investmentclinic');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        $listing->traversing();
        
        //Default ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('page','create-investmentclinic')) {
            $listing->create_action(__('create-investmentclinic'));
        }
        
        if (User::authorise('page','publish-investmentclinic')) {
            $listing->status_action(array(
                //'exception' => array('is_system'=>1),
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/investment_clinics/{id}/{label}'
            ));
        }
        
        if (User::authorise('page','edit-investmentclinic')) {
            $listing->edit_action(array(
                //'exception' => array('is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('page','delete-investmentclinic')) {
            $listing->delete_action(array(
                'exception' => array(
                        'i_absolute'=>true),
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/investment_clinics/{id}/{label}?ordering={ordering}&parent={parent_id}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-investmentclinic')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //Append CK Editor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-investmentclinic";
        $this->authorise('page',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('page/cms/frm-investmentclinic')
                    ->set('title',$title)
                ;   
    }
}