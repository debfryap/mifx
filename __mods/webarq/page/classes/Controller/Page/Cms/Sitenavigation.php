<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Page_Cms_Sitenavigation extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('manage-navigation'));
        
        App::$config->menu->active = 'site-navigation';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('page',array(
                            'create-navigation','edit-navigation',
                            'delete-navigation','publish-navigation'));
        
        //Append media tree tabular
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('navigation');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        $listing->traversing();
        
        //Default ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('page','create-navigation')) {
            $listing->create_action(__('create-navigation'));
        }
        
        if (User::authorise('page','publish-navigation')) {
            $listing->status_action(array(
                //'exception' => array('is_system'=>1),
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/site_navigations/{id}/{label}'
            ));
        }
        
        if (User::authorise('page','edit-navigation')) {
            $listing->edit_action(array(
                //'exception' => array('is_system'=>1),
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('page','delete-navigation')) {
            $related_table = http_build_query(array('relation_table' => array('site_navigation_pages','site_navigation_position_maps','media_library_applications')));
            $listing->delete_action(array(
                'exception' => array(
                        'is_system' => 1,
                        //'id'        => '<=100', //Predefined navigation could not be deleted
                        'i_absolute'=>true),
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/site_navigations/{id}/{label}?ordering={ordering}&parent={parent_id}&'.$related_table
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-navigation')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //Check for authorisation
        $permission = $this->param1."-navigation";
        $this->authorise('page',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('page/cms/frm-navigation')
                    ->set('title',$title)
                ;   
    }
        
    public function action_save() {
        
        $transaction = Data::create();        
        
        $transaction->save(true);
    }
    
    
    public function action_update() {
        Data::update()->save();
    }
}