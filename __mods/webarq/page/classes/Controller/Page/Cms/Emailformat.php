<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Page_Cms_Emailformat extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        $this->meta('title',__('manage-email-format'));
        
        App::$config->menu->active = 'email-format';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('page',array('create-email-format','edit-email-format'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('emailformat');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        //$listing->traversing('parent_id','site_navigation_id');
        
        //Default ordering
        $listing->order_by(array('id'));
        
        //Listing header
        $listing->set_header('type',array('label'=>'email-type'))
                ->set_header('subject');        
        
        $listing->tool('search',array('subject','type'));
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('page','create-email-format')) {
            $listing->create_action(__('create-email-format'));
        }
        
        if (User::authorise('page','edit-email-format')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-email-format')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Activate ckeditor
        $this->media_ckeditor();
        
        //Load media script
        $this->media_header();
        
        //Check for authorisation
        $permission = $this->param1."-email-format";
        $this->authorise('page',$permission);
        
        //Support variable        
        $title = __($permission);
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('page/cms/frm-email-format')
                    ->set('title',$title)
                ;   
    }
}