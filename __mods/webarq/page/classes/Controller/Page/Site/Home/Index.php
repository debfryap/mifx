<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page_Site_Home_Index extends Controller_Default_Template_Site {
    
    public function before() {
        parent::before();
        
        App::$config->menu->group = 'home';   
    }
    
    public function action_index() {
        $this->meta('title','Webarq CMS Front View')
             ->register('banner')
                ->use_widget('medialibrary/basic-slider')
                    ->set('table','site_navigations')
                    ->set('permalink','home')
             ->register('footer','');
	}

}

//class Controller_Page_Site_Home_Index extends Controller {
//    
//    public function action_index() {
//        $this->response->body('Welcome');
//	}
//
//} 
