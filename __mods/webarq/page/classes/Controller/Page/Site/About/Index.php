<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page_Site_About_Index extends Controller_Default_Template_Site {
    
    public function before() {
        parent::before();
        
        #App::$config->menu->group = 'about-us';
    }
    
    public function action_index() {
        $this->meta('title','Webarq CMS Front View')
             ->register('banner',Widget::load('medialibrary/basic-banner'))
             ->register('content')
                ->use_view('site/sample')
                    ->set('content','<p>But i was calling from controller</p>')
                ;
	}

} //End Page Site Home
