<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Front End) 
 * @Module      Page
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Controller_Page_Site_Default_Index extends Controller_Default_Template_Site {
    
    public function before() {
        parent::before();
    }
    
    public function action_index() {
        
        $link = trim(Request::$initial->uri(),'/');
        $link = stripos($link,Language::$current) === 0
                    ? substr($link,strlen(Language::$current)+1)
                    : $link;
                    
        App::$config->menu->group = $link;
        
        $this->meta('title','Webarq CMS Front View')
             ->register('banner',Widget::load('medialibrary/basic-banner'))
             ->register('content',Widget::load('page/default'));
	}

} //End Page Site Home
