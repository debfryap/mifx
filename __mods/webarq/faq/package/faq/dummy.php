<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Dummy data
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

$faq_module_navigations = array(
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq'",
            "label"         => "faq-list",
            "permalink"     => "list",
            "ordering"      => Faq::$config->use_subject === true ? 2 : 1,
            "create_time"   => "now"
        )
    );

$faq_module_navigation_group_maps =  array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-list' AND `permalink` = 'list' LIMIT 1",
            "module_navigation_group_id"  => 1,
            "create_time"   => "now" 
        )
    );

$faq_module_permissions = array ( 
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "create-faq",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "edit-faq",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "delete-faq",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "publish-faq",
            "create_time"   => "now"                    
        ),
    );

$faq_module_navigation_permission_maps = array (
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'create-faq' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'edit-faq' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'delete-faq' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-list' AND `permalink` = 'list' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'publish-faq' LIMIT 1",
            "create_time"    => "now" 
        )
    );
    
$faq_subjects = null;

if (Faq::$config->use_subject === true) {
    array_push(
        $faq_module_navigations,
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq'",
            "label"         => "faq-subject",
            "permalink"     => "subject",
            "ordering"      => 1,
            "create_time"   => "now"
        )
    );
    array_push(
        $faq_module_navigation_group_maps,
        array (
                "id"             => "auto",
                "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-subject' AND `permalink` = 'subject' LIMIT 1",
                "module_navigation_group_id"  => 1,
                "create_time"   => "now" 
            )
    );
    array_push(
        $faq_module_permissions,        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "create-subject",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "edit-subject",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "delete-subject",
            "create_time"   => "now"                    
        ),
        
        array (
            "id"            => "auto",
            "module_id"     => "SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1",
            "permission"    => "publish-subject",
            "create_time"   => "now"                    
        )
    );
    array_push(
        $faq_module_navigation_permission_maps,
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-subject' AND `permalink` = 'subject' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'create-subject' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-subject' AND `permalink` = 'subject' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'edit-subject' LIMIT 1",
            "create_time"   => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-subject' AND `permalink` = 'subject' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'delete-subject' LIMIT 1",
            "create_time"    => "now" 
        ),
        array (
            "id"             => "auto",
            "module_navigation_id" => "SELECT `id` FROM `module_navigations` WHERE `label` ='faq-subject' AND `permalink` = 'subject' LIMIT 1",
            "module_permission_id" => "SELECT `id` FROM `module_permissions` WHERE `module_id` =(SELECT `id` FROM `modules` WHERE `label` = 'faq' LIMIT 1) AND `permission` = 'publish-subject' LIMIT 1",
            "create_time"    => "now" 
        )
    );
}

return array (    
    //Do not delete this section
    "modules" => array (
        array (
            "id"            => "auto",
            "label"         => "faq",
            "ordering"      => "total-row",
            "is_publish"    => 1,
            "description"   => "FAQ Modules",
            "create_time"   => "now"                              
        )
    ),
    
    "module_navigations"                => $faq_module_navigations,
    "module_navigation_group_maps"      => $faq_module_navigation_group_maps,    
    "module_permissions"                => $faq_module_permissions,
    "module_navigation_permission_maps" => $faq_module_navigation_permission_maps,  
);

?>