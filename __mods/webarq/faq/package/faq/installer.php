<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Module Package Installation
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

if (Faq::$config->use_subject === true) {
    $faq_installer['faq_subjects'] = array (
        array (
            "master" => "id"
        ),
        array (
            "master" => "label",
            "form"   => array (
                "group"       => array('create-subject','edit-subject'),
                "class"       => "required is_used",
                "multilang"   => true,
                "minlength"   => "3",
                "referrer"    => array('permalink',false)
            )
        ),
        array (
            "master" => "label",
            "field"  => "permalink",
            "form"   => array (
                "group"       => array('create-subject','edit-subject'),
                "class"       => "required permalink",
                "minlength"   => "3",
            )
        ),
        array (
            "master" => "is_something",
            "field"  => "is_active",
            "default" => 0,
            "form"   => array (
                "group"       => array('create-subject','edit-subject')
            )
        ),
        array (
            "master"    => "ordering",
            "form"   => array (
                "group"     => array('create-subject','edit-subject'),
                "type"      => "ordering",
                "class"     => "required",
                "order"     => "after permalink"
            )
        ),
        array (
            "master"     => "create_time"
        ),
        array (
            "master"     => "update_time"
        )
    );
    
    $faq_subject = array (
        "type"   => "int",
        "field"  => "faq_subject_id",
        "default"=> 0,
        "form"   => array (
            "label"     => "faq-subject-id",
            "group"     => array('create-faq','edit-faq'),
            "class"     => "required challenge-order",
            "query"     => array (
                "table"     => "faq_subjects",
                "value"     => "id",
                "label"     => "label",
                "order"     => true
            )
        ) 
    );
    $faq_ordering = array (
        "master"    => "ordering",
        "form"   => array (
            "group"      => array('create-faq','edit-faq'),
            "type"       => "ordering",
            "data-field" => "faq_subject_id",
            "class"      => "required",
            "order"      => "after is_feature",
            "parenting"  => "faq_subject_id"
        )
    );
} else {
    $faq_subject = null; 
    $faq_ordering = array (
        "master"    => "ordering",
        "form"   => array (
            "group"      => array('create-faq','edit-faq'),
            "type"       => "ordering",
            "class"      => "required",
            "order"      => "after is_feature",
        )
    );   
}

$faq_installer['faqs'] = array (
    array (
        "master" => "id",
    ),
    $faq_subject,
    array(
        "master"    => "label",
        "field"     => "question",
        "form"   => array (
            "group"       => array('create-faq','edit-faq'),
            "class"       => "required",
            "multilang"   => true,
            "referrer"    => array('permalink',false),
            "order"       => "first"
        )
    ),
    array (
        "master" => "label",
        "field"  => "permalink",
        "form"   => array (
            "group"     => array('create-faq','edit-faq'),
            "class"     => "permalink required"
        ) 
    ),
    array (
        "field"     => "answer",
        "type"      => "mediumtext",
        "form"   => array (
            "group"     => array('create-faq','edit-faq'),
            "class"     => "simple-editor"
        ) 
    ),
    array (
        "master" => "is_something",
        "field"  => "is_active",
        "default" => 0,
        "form"   => array (
            "group"     => array('create-faq','edit-faq'),
            "class"     => "simple-editor"
        ) 
    ),
    $faq_ordering,
    array (
        "master"     => "create_time"
    ),
    array (
        "master"     => "update_time"
    )
);

return $faq_installer;