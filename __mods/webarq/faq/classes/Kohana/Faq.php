<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Api
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Kohana_Faq {    
    public static $config;
    
    /**
     * Return uri for current active news
     */
    public static function uri() {
        $c = trim(Request::$initial->param('permalink'),'/');
        
        $t = explode('/',$c);
        
        $a = array_pop($t);
        
        return $a;
    }
    
    /**
     */
    public static function up() {
             
    }
    
    public static function install() {
        
        if (!self::$config->use_subject) {
            //Drop table categories
            Data::drop_table('faq_subjects');
            
            //Drop navigations 
            Data::undo_module_navigation('faq',array('faq-subject'));
            
            //Navigation list ordering
            $order_list = 1;
        } else {
            //Navigation list ordering
            $order_list = 2;
        }
        
        //Update navigation list ordering
        DB::update('module_navigations')->set(array('ordering'=>$order_list))->where('label','=','faq-list')->and_where('permalink','=','list')->execute();
    }
}