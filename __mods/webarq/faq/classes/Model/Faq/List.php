<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Faq_List extends Model_Dba {    
    public function pre_select() {
        $builder = $this->select('fq.*',
                                DB::expr("CASE fq.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('faqs','fq'));
        
        if (Faq::$config->use_subject)
            $builder->join(array('faq_subjects','fs'))
                    ->on('fq.faq_subject_id = fs.id');
        
        return $builder;
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('faqs','fq'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/
    
    
    /** Front end data **/
    
    /**
     */
    public function basic() {
        return $this->pre_select()->order_by('fq.ordering')->execute();
    }  
}