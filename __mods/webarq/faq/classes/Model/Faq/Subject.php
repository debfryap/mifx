<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Database Model 
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

class Model_Faq_Subject extends Model_Dba {        
    public function pre_select() {
        return $this->select('sbj.*',
                                DB::expr("(SELECT COUNT(`fq`.`id`) FROM `faqs` AS `fq` WHERE `fq`.`faq_subject_id` = `sbj`.`id`) AS `total_faqs`"),
                                DB::expr("CASE sbj.`is_active` WHEN 1 THEN 'unpublish' WHEN 0 THEN 'publish' END `function`"))
                    ->from(array('faq_subjects','sbj'));
    }
    
    /** Data listing purpose **/
    public function total() {
        $model = Data::total(array('faq_subjects','sbj'),false);
        
        return $model;
    }
    
    public function tabular() {        
        $model = $this->pre_select();
                    
        return $model;
    }
    /** End Data listing purpose **/   
    
    public function simple() {
        return $this->select('sbj.id','sbj.label','sbj.permalink')
                    ->from(array('faq_subjects','cat'))
                    ->where('sbj.is_active = 1');
    }
    
    public function get_child($value,$field='permalink') {
        $c = $this->select('sbj.id')
                ->from(array('faq_subjects','cat'))
                ->where("sbj.$field = $value")
                ->and_where("sbj.is_active = 1")
                ->execute()
                ->current();
        
        if (empty($c->id))
            return false;
            
        $d = $this->pre_select()
                  ->where("sbj.parent_id = $c->id")
                  ->and_where("sbj.is_active = 1")
                  ->order_by('sbj.ordering')
                  ->execute();
        
        return $d;
    }
    
    public function get_faq($value,$field='permalink') {
        $p = Model::factory('faq_list')
                    ->pre_select()
                    ->join(array('faq_subjects','cat'),'left')
                    ->on("fq.faq_subject_id = sbj.id")
                    ->where("sbj.$field = $value")
                    ->and_where('fq.is_active = 1')
                    ->execute();
        
        return $p;
    }
}