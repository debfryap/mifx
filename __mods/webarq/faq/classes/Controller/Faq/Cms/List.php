<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Faq_Cms_List extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'faq-list';
    }
    
    public function action_index() {
        //Check for authorisation
        $this->authorise('faq',array(
                            'create-faq','edit-faq',
                            'delete-faq','publish-faq'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('faq_list');
        
        //HTML table style listing
        $listing->style('table');
        
        //Enable traversing listing
        //$listing->traversing();
        
        //Default ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('question')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('faq','create-faq')) {
            $listing->create_action(__('create-faq'));
        }
        
        if (User::authorise('faq','edit-faq')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('faq','delete-faq')) {
            $delete_link = URL::cms().'helper/ajax/delete/faqs/{id}/{question}?ordering={ordering}';
            
            if (Faq::$config->use_subject === true)
                $delete_link .= '&faq_subject_id={faq_subject_id}';
                
            $listing->delete_action(array(
                'ajax'      => true,                
                'href'      => $delete_link
            ));
        }
        
        if (User::authorise('faq','publish-faq')) {
            $listing->status_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/faqs/{id}/{question}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-faq')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //CK Editor
        $this->media_ckeditor();
        
        //Check for authorisation
        $permission = $this->param1."-faq";
        $this->authorise('faq',$permission);
        
        //Support variable        
        $title = __('create-faq');
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('faq/cms/frm-item')
                    ->set('title',$title)
                ;   
    }
}