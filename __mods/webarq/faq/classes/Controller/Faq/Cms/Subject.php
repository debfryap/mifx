<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller (For Application Administrator) 
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_Faq_Cms_Subject extends Controller_Default_Template_Cms {
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'faq-subject';
    }
    
    public function action_index() {
        
        if (empty(Faq::$config->use_subject)) {
            $this->authorise(false);
            return;
        };
        
        //Check for authorisation
        $this->authorise('faq',array(
                            'create-subject','edit-subject',
                            'delete-subject','publish-subject'));
        
        //Append css styles
        $this->media_tree_tabular();
        
        //Open listing using model
        $listing = Data::listing('faq_subject');
        
        //HTML table style listing
        $listing->style('table');
        
        //Default ordering
        $listing->order_by();
        
        //Listing header
        $listing->set_header('label')->set_header('permalink');        
        
        //Listing action, we have several ways to make this fun
        if (User::authorise('faq','create-subject')) {
            $listing->create_action(__('create-faq-subject'));
        }
        
        if (User::authorise('faq','edit-subject')) {
            $listing->edit_action(array(
                'href'      => 'form/edit/{id}'
            ));
        }
        
        if (User::authorise('faq','delete-subject')) {
            $listing->delete_action(array(
                'ajax'      => true,                
                'href'      => URL::cms().'helper/ajax/delete/faq_subjects/{id}/{label}?ordering={ordering}'
            ));
        }
        
        if (User::authorise('faq','publish-subject')) {
            $listing->status_action(array(
                'ajax'      => true,
                'href'      => URL::cms().'helper/ajax/{function}/faq_subjects/{id}/{label}'
            ));
        }
        
        //Assign to template
        $this
            ->register('title')->use_string(__('manage-faq-subject')) 
            ->register('content')->use_string($listing->render())
                ;
    }
    
    public function action_form() {
        
        if (empty(Faq::$config->use_subject)) {
            $this->authorise(false);
            return;
        };
        
        //Check for authorisation
        $permission = $this->param1."-subject";
        $this->authorise('faq',$permission);
        
        //Activate media validate
        $this->media_validate();
        
        //Load media script
        $this->media_header();
        
        //Support variable        
        $title = __('create-faq-subject');
        
        //Assign to template
        $this
            ->register('title')
                ->use_string($title)
            ->register('content')
                ->use_view('faq/cms/frm-subject')
                    ->set('title',$title)
                ;   
    }
}