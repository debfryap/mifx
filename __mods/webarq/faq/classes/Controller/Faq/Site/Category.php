<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Controller
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 


class Controller_News_Site_Category extends Controller_Default_Template_Site {
    
    private $permalink;
    
    
    public function before() {
        parent::before();
        
        //Active navigation based on label
        App::$config->menu->active = 'news';
        
        $this->permalink = Request::$current->param('permalink');
    }
    
    public function action_index() {
        $this->register('content')
             ->use_view('layout/basic-two-column')
                ->set('left',Widget::load('news/category/side-bar').Widget::load('news/feature'))
                ->set('right',Widget::load('news/category/child'));
    }    
}