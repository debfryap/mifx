<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    i18n
 * @Module      News
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

return array (
    'faq'                           => 'FAQ',
    'faq-subject'                   => 'Subject',
    'faq-subject-id'                => 'FAQ Subject ID',
    'create-faq-subject'            => 'Create FAQ Subject',
    'edit-faq-subject'              => 'Edit FAQ Subject',
    'manage-faq-subject'            => 'Manage FAQ Subject',
    'image-faq-subject'             => 'Image FAQ Subject',
    'parent-faq-subject'            => 'Parent FAQ Subject',
    'faq-list'                      => 'FAQ Listing',
    'create-faq'                    => 'Create FAQ',
    'edit-faq'                      => 'Edit FAQ',
    'manage-faq'                    => 'Manage FAQ',
    'image-faq-thumbnail'           => 'Thumbnail',
    'image-faq-large'               => 'Image FAQ',
    'featured-faq'                  => 'Featured FAQ',
    'publish-faq'                   => 'Publish/Un-Publish FAQ',
    'delete-faq'                    => 'Delete FAQ',
);