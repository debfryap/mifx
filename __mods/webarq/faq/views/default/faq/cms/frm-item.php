<?php defined('SYSPATH') or die('No direct script access.'); 
 
/**
 * @Author 		Daniel Simangunsong
 * @Company		Webarq
 * @copyright 	2012
 * @Package	    Form Item ~ CMS
 * @Module      FAQ
 * @License		Kohana ~ Webarq ~ Daniel Simangunsong
 * 
 * Calm seas, never make skillfull sailors	
**/ 

?>

<?php
    echo Data::form(array(
        'action'    => false,
        'title'     => $title,
        'id'        => 'form_faq',
        'multilang' => true,  
        'pack'      => array ('faq:'.Request::$initial->param('param1').'-faq'),
        'type'      => strtolower(Request::$initial->param('param1')),
        'row_id'    => array('id',Request::$initial->param('param2')),
    ));
?>